{
    "id": "756c6d76-bee1-45d0-9d1e-b5eff76a77c3",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "font04",
    "AntiAlias": 0,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "VCR OSD Mono",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "2a12f9f4-aab8-4b98-8ef7-b6e7f8494d5b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 21,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 17,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "29d1c2af-f97c-407a-bc27-40374f0763eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 21,
                "offset": 4,
                "shift": 12,
                "w": 3,
                "x": 105,
                "y": 94
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "595bce7b-45ec-4f05-adba-e5778ae77c56",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 21,
                "offset": 2,
                "shift": 12,
                "w": 9,
                "x": 191,
                "y": 71
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "6194d65d-ec11-47dc-b04e-dfd628059245",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 67,
                "y": 48
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "2a1f1fe8-4a43-4d6f-8591-c4af03614897",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 80,
                "y": 48
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "e53fb25a-28d0-43e9-a147-6adf2f629ff6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 93,
                "y": 48
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "fa3d8bda-1175-4df0-a896-7d0798346377",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 106,
                "y": 48
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "8c4f3dcf-2138-4253-858c-9aac03570aeb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 21,
                "offset": 4,
                "shift": 12,
                "w": 4,
                "x": 94,
                "y": 94
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "4d516aca-c194-457e-9542-6c0e9d96b8ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 21,
                "offset": 3,
                "shift": 12,
                "w": 6,
                "x": 64,
                "y": 94
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "0e6f1040-34ea-44bc-9f01-29e327f7cf70",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 21,
                "offset": 4,
                "shift": 12,
                "w": 6,
                "x": 56,
                "y": 94
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "2e013611-3771-41fe-9bbf-3d08130e2640",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 21,
                "offset": 2,
                "shift": 12,
                "w": 9,
                "x": 158,
                "y": 71
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "a773b0c7-e516-47b0-8ce5-2bf1be01aa6a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 119,
                "y": 48
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "753c1cb0-b7aa-4ef8-bee6-d2b015289d9a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 21,
                "offset": 3,
                "shift": 12,
                "w": 5,
                "x": 87,
                "y": 94
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "25915f41-0233-419c-9e7f-c80291dd22e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 21,
                "offset": 2,
                "shift": 12,
                "w": 9,
                "x": 180,
                "y": 71
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "3d4c6cc1-ddd3-4b7d-9e0b-8587037a4430",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 21,
                "offset": 4,
                "shift": 12,
                "w": 3,
                "x": 110,
                "y": 94
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "3ccbdb62-79fc-464a-836b-cfad5bb90965",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 132,
                "y": 48
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "10f469c8-ebe9-47c8-96aa-4837b6a1ee77",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 145,
                "y": 48
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "4acf09dd-7e89-4446-8007-7fcb99abe279",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 21,
                "offset": 3,
                "shift": 12,
                "w": 7,
                "x": 2,
                "y": 94
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "8bfa187a-3c91-4902-8dfb-bd62785e5795",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 2,
                "y": 71
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "2b07a5b2-9796-4e32-b736-19ddab2cac38",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 158,
                "y": 48
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "63e54222-20a4-44e9-9ccc-acb9dc2ca39d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 171,
                "y": 48
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "3b531d5d-7d31-413f-9b51-11cc28a9f68f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 184,
                "y": 48
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "f43f4650-2c92-4a01-95df-cfc6213f004e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 197,
                "y": 48
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "f4cce8f1-fd31-438e-ac5a-66053653d598",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 210,
                "y": 48
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "a6f46183-30c3-4163-acd8-364e4e207cfe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 223,
                "y": 48
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "71e848c6-24b6-4b0a-b8b3-917cdd1f33d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 236,
                "y": 48
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "c4e7a574-21dd-40a2-9671-fb37330a8e24",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 21,
                "offset": 4,
                "shift": 12,
                "w": 3,
                "x": 100,
                "y": 94
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "4ab7de35-f307-4a21-9538-1f3022db5517",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 21,
                "offset": 2,
                "shift": 12,
                "w": 5,
                "x": 80,
                "y": 94
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "a8a95ebe-8b35-4fd8-9978-16edc0ce8cae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 21,
                "offset": 2,
                "shift": 12,
                "w": 9,
                "x": 169,
                "y": 71
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "07f679e8-1361-4963-af5a-602e532c5cfb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 145,
                "y": 71
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "93380099-639b-4085-a584-2c2854defd04",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 21,
                "offset": 2,
                "shift": 12,
                "w": 9,
                "x": 213,
                "y": 71
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "7ec3daec-5bd0-4de5-978e-55e30e150cd8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 132,
                "y": 71
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "c51868d8-3633-49f4-bac7-07d67974b05e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 93,
                "y": 71
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "7dd1b170-3daa-492a-9b79-a340904a99f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 80,
                "y": 71
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "b8820449-4db8-4d52-9c9d-b2bf1d34fa19",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 67,
                "y": 71
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "b52db57e-ec4c-46d7-aa37-89ba19ac4780",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 106,
                "y": 71
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "dd8f2853-8c3a-4f43-acab-9013a2464e8d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 54,
                "y": 71
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "57008310-25c2-4d02-a76c-596fedb9c3b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 119,
                "y": 71
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "1a0fd279-20dd-40d4-9ba7-d99eb09179c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 28,
                "y": 71
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "2dc5d235-7e5f-46e6-969a-bf718fdbea9b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 41,
                "y": 71
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "b551db6f-173a-4006-9ca4-7ce46771e8ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 54,
                "y": 48
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "a79dbfba-d659-4d1d-a414-6500c4eac4ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 21,
                "offset": 3,
                "shift": 12,
                "w": 7,
                "x": 11,
                "y": 94
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "b575faa9-e0e0-4677-b607-d6912ad2d349",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 28,
                "y": 48
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "91bbe915-699a-47ed-8d5e-7cc8c61fa383",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 2,
                "y": 25
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "ed1415d6-8cc0-481e-a440-8424b616d70a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 226,
                "y": 2
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "a959f269-c1e2-4098-939f-5745bf56ca4e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 213,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "f21ff669-f061-4447-a2e8-1ede574f035d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 200,
                "y": 2
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "2084cae5-77bb-4620-ac9d-3699c07410bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 41,
                "y": 48
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "27ff586c-5d29-4e74-8cde-019c6abadf05",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 187,
                "y": 2
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "b7737c0d-5b01-433c-ab07-46734271d959",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 174,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "94c203cd-0c80-4856-a9f5-ac68a606b8ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 161,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "79a5a7ed-9267-4657-af0e-ac3a9af12322",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 148,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "62d904a1-474c-494b-84d3-5d24393cd088",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 135,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "c98519ed-d6b5-46a5-aab2-7b60732b49e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 122,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "76525a0b-edc4-490a-b3fc-292a3dcea7e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 109,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "0bb5b6ac-673d-439a-958e-64d4fed97ff6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 96,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "789056ab-c807-4dba-8e66-f5f7cb0bdd9a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 83,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "d0b64c54-8be7-419f-b4ea-ab780a06bd81",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 70,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "44db46ac-ae3d-44b5-ba1b-713247e55c6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 57,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "1b34c499-064d-48c5-845c-1490647c7f66",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 21,
                "offset": 4,
                "shift": 12,
                "w": 7,
                "x": 29,
                "y": 94
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "724c1a07-0945-40ea-8f2b-6362b7443a67",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 44,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "a904e68d-3a53-4e2c-beed-d9f571dd4e18",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 21,
                "offset": 2,
                "shift": 12,
                "w": 7,
                "x": 47,
                "y": 94
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "28a809d0-05c4-4034-9a54-c4a05d46f6b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 31,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "e508c010-3819-4689-abf7-e95ac259144d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 21,
                "offset": 0,
                "shift": 12,
                "w": 13,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "3f15b8f0-7abc-4323-9bb0-036a4fe22eeb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 21,
                "offset": 3,
                "shift": 12,
                "w": 6,
                "x": 72,
                "y": 94
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "98981793-5fdb-4656-b4da-5e0d2593b2b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 239,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "a89027ad-962f-4bad-a0eb-93d7180b2231",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 15,
                "y": 25
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "42d7cc9a-b3ba-4c0e-ab46-fb87916148e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 15,
                "y": 48
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "7979cb81-e27d-4231-b255-c6baec2486b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 28,
                "y": 25
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "25be471c-fbbb-4fa1-8384-7c7c86e886db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 2,
                "y": 48
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "28b52f83-1e9c-44ac-8765-ee46fe4f9a66",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 21,
                "offset": 2,
                "shift": 12,
                "w": 9,
                "x": 202,
                "y": 71
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "70c954d7-f816-4415-9cbb-84ea60436459",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 236,
                "y": 25
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "2b299b8d-dbc2-425c-ae06-66397aec7634",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 223,
                "y": 25
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "f548bb37-065a-4f04-b7c8-9600e78269b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 21,
                "offset": 3,
                "shift": 12,
                "w": 7,
                "x": 224,
                "y": 71
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "3d47565f-a322-4122-bcf5-6c0f4534c28d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 21,
                "offset": 3,
                "shift": 12,
                "w": 7,
                "x": 233,
                "y": 71
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "e0309ff4-5384-4f14-8adf-63ffb4c12a8f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 210,
                "y": 25
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "0eeefc7d-f443-4624-aba6-09007422828c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 21,
                "offset": 5,
                "shift": 12,
                "w": 3,
                "x": 120,
                "y": 94
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "15b18e44-23c4-4699-8c9d-c3a298103e0b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 197,
                "y": 25
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "673cc1b0-406e-4dae-8532-8cbc43f5daa2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 184,
                "y": 25
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "35552d2f-d0c2-4bcd-9dc2-cd9f2fe11c7b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 171,
                "y": 25
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "9b218490-1ddc-4c2f-b646-ddf4b205a5e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 158,
                "y": 25
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "4112f141-80c0-48c2-89c7-2285970eb1c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 145,
                "y": 25
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "a9b02186-5d24-40ba-871a-2404a00a3744",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 132,
                "y": 25
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "8f0d4e44-bc37-4a8f-a906-77cbed215d70",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 119,
                "y": 25
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "7922bd7d-8b29-4435-adc6-5d650cec9ae4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 21,
                "offset": 3,
                "shift": 12,
                "w": 7,
                "x": 38,
                "y": 94
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "d7cbd34a-b7ff-4051-85e1-7a4872994804",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 106,
                "y": 25
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "4e870bce-fbfe-49a6-984c-54406a72eca3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 93,
                "y": 25
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "25c862e2-29fc-41ed-b3c4-7f300add3326",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 80,
                "y": 25
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "4955207e-dcf5-4da4-8330-7dd0356f965c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 67,
                "y": 25
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "dac35cdf-0d08-4680-8e1f-c0a18bd4ff81",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 54,
                "y": 25
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "55400950-4981-4d1f-be08-5cc1281ac6c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 41,
                "y": 25
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "6be7e50f-41c9-473f-9c24-0b5a9e5f93bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 21,
                "offset": 3,
                "shift": 12,
                "w": 7,
                "x": 242,
                "y": 71
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "1ae79896-b934-4cac-ab34-1d2258dea94d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 21,
                "offset": 5,
                "shift": 12,
                "w": 3,
                "x": 115,
                "y": 94
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "3844cb80-43f7-4f7a-a874-d15e4e6d7a6e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 21,
                "offset": 3,
                "shift": 12,
                "w": 7,
                "x": 20,
                "y": 94
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "d4e53127-85cd-426b-a71c-675f4d8d934d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 15,
                "y": 71
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 16,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}