{
    "id": "a4876167-784e-49a3-b95b-7d045173dbaf",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "font01",
    "AntiAlias": 0,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "8BIT WONDER",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "95d479e9-27e4-42d9-8872-213fe6ef494e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 13,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 179,
                "y": 47
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "fb02f209-d648-4aab-8adb-9e4353f7cca0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 13,
                "offset": 3,
                "shift": 8,
                "w": 2,
                "x": 134,
                "y": 62
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "8551ee57-14b0-4477-b4ed-49e819eba6f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 13,
                "offset": 2,
                "shift": 8,
                "w": 5,
                "x": 91,
                "y": 62
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "311d2bde-1a12-40f9-99ad-bbfbe363d61f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 14,
                "x": 82,
                "y": 2
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "41effba8-e242-4ca4-86e3-a74a4c23b840",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 45,
                "y": 62
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "026884e0-91ab-4735-8235-7821f7dd63f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 13,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 149,
                "y": 47
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "f78bafa4-355c-4a38-8c24-f7fdf3721ada",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 13,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 139,
                "y": 47
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "50f07565-2729-43e1-b18a-5a780bc1a835",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 13,
                "offset": 3,
                "shift": 8,
                "w": 2,
                "x": 130,
                "y": 62
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "8dab9469-04c5-42f6-9c82-5d4ce0536366",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 13,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 197,
                "y": 47
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "2295d453-688f-4759-b011-cccc221d6681",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 13,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 206,
                "y": 47
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "43514e76-f917-4cda-8705-02251dfe7c1c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 86,
                "y": 32
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "363ff20e-a87a-4625-a025-7414528659e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 215,
                "y": 47
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "e79588b9-30ca-471e-951d-d466763545d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 13,
                "offset": 3,
                "shift": 8,
                "w": 3,
                "x": 111,
                "y": 62
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "9772019c-50a6-47b4-b5ec-c456233aaddd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 13,
                "offset": 2,
                "shift": 8,
                "w": 5,
                "x": 77,
                "y": 62
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "80346f48-0ffb-424e-952f-e4b369df33db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 13,
                "offset": 3,
                "shift": 8,
                "w": 3,
                "x": 121,
                "y": 62
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "89b8e812-43f7-4617-8365-c321b64f0897",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 61,
                "y": 62
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "81843e8c-92dd-447f-b26c-bdbf2decc8c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 170,
                "y": 32
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "b6e18dc6-432a-454c-9d50-4ecfcf83b191",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 13,
                "offset": -1,
                "shift": 7,
                "w": 7,
                "x": 224,
                "y": 47
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "d08454cd-d15b-40d7-ab8a-a0217dfb7ddd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 198,
                "y": 32
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "69e183c0-207f-4fee-b11f-75a6d32839d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 100,
                "y": 47
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "1532de1a-2649-4296-a913-b590109aa0ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 86,
                "y": 47
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "acdb7d4d-b691-4f67-8abe-d57ea9339d53",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 72,
                "y": 47
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "36ce6eb8-266b-45fc-9038-dec539e1c5e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 58,
                "y": 47
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "88d92b72-690a-49e8-9ebf-7eeb49e9fba6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 44,
                "y": 47
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "b7f6b2d6-7b3f-4093-b23e-d4d1ba724724",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 114,
                "y": 47
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "f7943b11-fcd3-4f8d-bb5e-aa4e029bb89c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 16,
                "y": 47
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "482c923d-60ce-4555-aa30-3291a93cad76",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 13,
                "offset": 3,
                "shift": 8,
                "w": 2,
                "x": 126,
                "y": 62
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "9eb8ff13-b2db-4a25-8808-84523060b19e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 13,
                "offset": 3,
                "shift": 8,
                "w": 3,
                "x": 116,
                "y": 62
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "b67141c0-0955-49c4-9f68-94746479ac52",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 233,
                "y": 47
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "b815b18a-f805-4d74-ba35-dd1c2237693f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 242,
                "y": 47
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "fb8d444a-e9a6-4937-ac59-fa9cccc12f77",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 20,
                "y": 62
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "9694cca6-64e8-46a3-93c7-76d134d02b9d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 69,
                "y": 62
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "48df04eb-2de3-4474-821a-27f069e79da3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 13,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 169,
                "y": 47
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "e886a291-3c05-4a91-ab3e-ecfe2861a126",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 226,
                "y": 17
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "4ccd328d-3905-4e98-9078-e2ef8e130f03",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 30,
                "y": 17
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "929cb37c-07e8-4c50-afb0-cc6673ee2771",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 224,
                "y": 2
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "c39616c9-3982-4e16-8217-4b35bc84754b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 210,
                "y": 2
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "0551f286-9fe5-4c3e-9f6d-3577361148f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 196,
                "y": 2
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "53ba5c6d-f0bd-4225-910b-cd4624c09287",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 182,
                "y": 2
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "6592e205-6706-49c9-a1b9-4f5110e46660",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 168,
                "y": 2
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "f4822de9-dd55-4b45-a9aa-f3da1a5d3e48",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 238,
                "y": 2
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "3a31a9bc-8481-46dc-9164-dc249c729f14",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 13,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 84,
                "y": 62
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "0be3e186-2b7e-4ac5-bcea-4562aa14d3ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 140,
                "y": 2
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "942e3c02-8f0b-4b8c-8751-e4bcbe5150f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 126,
                "y": 2
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "a99645ab-6700-4eb5-81d2-46081fca62e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 112,
                "y": 2
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "9a82509a-91ac-4253-81df-cda084b8f609",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 13,
                "offset": 0,
                "shift": 20,
                "w": 18,
                "x": 42,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "2ca383a8-0ae9-4044-b099-130178115a3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 2,
                "y": 17
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "cf95ade6-bd1f-43f9-8093-24ad14f18a41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 98,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "49dae683-569b-493f-9f90-ad5a8b1cd847",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 16,
                "y": 17
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "065e67ac-197d-4108-b41f-c514f6cae6c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 44,
                "y": 17
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "e5ce36fe-6c9f-4c51-aaf5-7f22255dcd2a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 58,
                "y": 17
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "483e7fdd-44d9-4d06-a5da-27a65cd9b7e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 72,
                "y": 17
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "62c862c5-c954-412d-901e-7219380da496",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 86,
                "y": 17
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "14f21c52-d529-41a8-802f-89e3d7c9985d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 100,
                "y": 17
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "9f2dd364-c741-4186-aa45-a34889affefa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 114,
                "y": 17
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "4e122bbb-69fb-47df-a338-b03221d1202e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 13,
                "offset": 0,
                "shift": 20,
                "w": 18,
                "x": 62,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "9a7ca274-b734-4cef-95ac-45769244ea61",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 128,
                "y": 17
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "e4601cfe-cd09-45d5-9378-9262144e76a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 142,
                "y": 17
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "f15ad612-702f-463b-a61f-e9cf7a954fa5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 156,
                "y": 17
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "207dd68f-7848-4f77-ad75-b7bf821324a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 13,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 11,
                "y": 62
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "a2857f29-bc6a-4cb1-ab08-2a74327184c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 53,
                "y": 62
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "c383bbe2-ff97-4586-b45a-39c8594cb52e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 13,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 188,
                "y": 47
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "4c60253b-d7f8-4618-be36-254dc5f20f96",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 8,
                "x": 159,
                "y": 47
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "7a584273-905e-45cc-8234-1de4b64b70a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 13,
                "offset": 0,
                "shift": 8,
                "w": 9,
                "x": 128,
                "y": 47
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "488082ca-366e-4dd9-9870-dc0b6a012d48",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 13,
                "offset": 3,
                "shift": 8,
                "w": 4,
                "x": 105,
                "y": 62
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "7eb2a2cc-6d32-4b56-87b0-a24a62c352d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 240,
                "y": 17
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "a0f3f687-201b-4c98-b827-20d91ac3584c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 156,
                "y": 32
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "e974423a-7815-4a6c-845c-4b8954ec4e38",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 198,
                "y": 17
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "6b50b93d-2a88-4f39-a1ac-a38697076af5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 184,
                "y": 17
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "14b0d4ed-ee3b-4cd4-a8cd-adfe3b4b5d37",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 170,
                "y": 17
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "e9f9f90c-418f-428c-9f64-e6889a5e7282",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 212,
                "y": 17
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "9e094eb5-542c-4a3d-bb9e-ddb372323c9d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 154,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "c381803a-7769-4041-81df-75e050efae2e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 2,
                "y": 32
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "c706c3a9-4a0d-4fa1-908c-73ad41381862",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 13,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 98,
                "y": 62
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "8e23f47c-f471-49bf-b979-54562af29264",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 212,
                "y": 32
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "3bb23954-ae37-453c-a075-d59b8151f98b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 226,
                "y": 32
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "0f790b47-0ab1-4c55-94f7-5daec20fb7ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 240,
                "y": 32
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "cfa35bb8-a165-4965-9ede-9e43efdfc515",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 13,
                "offset": 0,
                "shift": 20,
                "w": 18,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "a42e72cb-93ec-4fec-a8bc-fc40de419531",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 30,
                "y": 47
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "721f07e6-ed0a-4fc8-857d-3144f3265581",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 2,
                "y": 47
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "8c3635fa-bd64-46b2-98b3-2d33abd3b698",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 184,
                "y": 32
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "07920c29-7774-496c-b653-aa580a1df04f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 142,
                "y": 32
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "0d3c61d1-f12f-4356-8a9b-a8c96b0d8e30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 128,
                "y": 32
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "d1189847-cd80-4166-96f4-f6ddc1189bca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 114,
                "y": 32
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "b5119531-4834-440d-b26e-314f91783e88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 100,
                "y": 32
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "4bca9c4c-c754-488e-9236-c367483c51b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 72,
                "y": 32
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "6ca347bc-3bbd-4265-aedb-dab8ca48323a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 58,
                "y": 32
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "94541486-0736-4f1d-9c1f-c586c9b2d8bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 13,
                "offset": 0,
                "shift": 20,
                "w": 18,
                "x": 22,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "76519338-5a30-4f4f-9f56-753c35c25e55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 44,
                "y": 32
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "d08a740a-ae07-4456-a51e-ddf1a1737a11",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 30,
                "y": 32
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "7554f5e4-0e1c-4775-9626-801fa0737751",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 16,
                "y": 32
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "d10d2cce-cd91-4696-ab2f-a5343a1ebb48",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 37,
                "y": 62
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "fb65197e-c565-4318-8413-1d58b9cba44b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 13,
                "offset": 3,
                "shift": 8,
                "w": 2,
                "x": 138,
                "y": 62
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "ca1a13df-0fee-45e4-ac58-667ceb918956",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 29,
                "y": 62
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "653efd85-22d0-4df8-b2a0-d5f4202c23e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 2,
                "y": 62
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 10,
    "styleName": "Nominal",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}