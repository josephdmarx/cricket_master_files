{
    "id": "f8ecb762-d579-483f-ad39-4b07a921b07b",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "font02",
    "AntiAlias": 0,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "8-bit pusab",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "e10903b7-3ada-46ad-bb21-5ed3a25e9636",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 9,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 90,
                "y": 46
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "803ac68c-dc2f-45bd-851f-ddf002b5b594",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 9,
                "offset": 2,
                "shift": 5,
                "w": 2,
                "x": 14,
                "y": 57
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "7e6ae893-0e38-4ffe-839f-ecdb66e884e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 9,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 80,
                "y": 46
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "df456081-4be9-4d9a-9488-bc6eb40962e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 9,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 26,
                "y": 13
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "de541230-bb8c-41bb-aeae-f0048d3c9851",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 51,
                "y": 35
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "e1b0c671-244b-4418-a9f2-17218f05dc49",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 9,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 12,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "78d93617-3a96-4022-9e24-fcd1ab1d2358",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 9,
                "y": 35
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "d69a1868-95bc-41d5-b7f8-72992db05775",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 9,
                "offset": 1,
                "shift": 4,
                "w": 4,
                "x": 86,
                "y": 35
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "1ce6b0b2-128b-4982-8040-15901fe7ee45",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 9,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 18,
                "y": 57
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "3f1ddfc4-8f5a-4f91-a0c6-8ac7045f273c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 9,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 120,
                "y": 46
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "15187c84-a16a-4cbd-856f-e8150f2d6d0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 9,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 42,
                "y": 13
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "0c80be1b-2114-4940-8e79-2b31baf9e157",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 2,
                "y": 35
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "f874a8ae-a938-494f-b26f-9ed8a56a3c29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 9,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 6,
                "y": 57
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "387febc3-9d84-4831-9a0a-58baa20b0efd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 107,
                "y": 24
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "c3ca9aed-59d5-406c-be7b-ea79ba6d082d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 9,
                "offset": 0,
                "shift": 2,
                "w": 1,
                "x": 25,
                "y": 57
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "61889710-47c2-40cb-9415-9ec693e66c08",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 68,
                "y": 46
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "7ef22b44-f084-4536-9245-b210248b445d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 114,
                "y": 24
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "90ebae35-792c-4a65-a50d-a4c76748d1eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 9,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 14,
                "y": 46
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "f632ab4d-78d1-4ae0-a1cf-af327d0847d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 16,
                "y": 35
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "fb3d0b8e-1ec0-4729-9159-8a8e1348de6a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 9,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 117,
                "y": 2
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "0c9b12f1-b504-4f02-8a65-ca136f6d14e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 9,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 109,
                "y": 2
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "44c1191a-3869-4535-bd60-38a5c7f35c72",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 23,
                "y": 35
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "4985b1ca-0ca9-4d77-9f76-b12fc5594510",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 44,
                "y": 35
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "f8a251c1-b776-46e3-8046-21a790efcef9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 93,
                "y": 2
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "b7450c79-28b7-45a8-80d0-3c77f3d792f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 37,
                "y": 35
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "560d68e5-7248-4892-9621-b4573b08a22c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 79,
                "y": 35
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "f19789c4-b40f-466e-95d4-9c7353a40253",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 9,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 10,
                "y": 57
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "ab5416c9-4c31-4bf8-917a-7b5c406b19f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 110,
                "y": 46
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "a12fa15c-9a0a-40a4-a198-0b160e929587",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 56,
                "y": 46
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "332f8306-a3e1-452c-a7d0-efb5fc605771",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 58,
                "y": 35
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "8893f2ba-ce94-4ac7-8051-08c45ab4a3b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 62,
                "y": 46
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "88f19280-6a9a-4a47-a664-80b19faeb757",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 9,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 77,
                "y": 2
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "75a71420-e477-4ee8-b048-bc3dd2959fdd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 9,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "54b7b75c-7d7b-44e0-b262-b808b58fa76e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 65,
                "y": 35
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "5b9f0f6d-e4dc-4751-9287-0bec00e8a07f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 72,
                "y": 35
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "37eb7989-99e5-492f-9fd6-994fee8ecef6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 69,
                "y": 2
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "ad9e890d-2ba9-4feb-9dac-d36edb7acd36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 100,
                "y": 24
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "a9c25165-c913-478a-b966-53466c55bbe7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 61,
                "y": 2
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "103cf30d-7092-498e-8755-9b4443002e14",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 53,
                "y": 2
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "0b005ab7-9d79-48f0-976f-112f4b44d78a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 45,
                "y": 2
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "89002fbf-3462-4176-a3e4-70df4617a5de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 30,
                "y": 35
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "2a61bb93-431a-4cb3-bf66-b846ddf88c9b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 9,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 29,
                "y": 2
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "200f6edb-14d3-4407-ac66-3186da8d6025",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 9,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 101,
                "y": 2
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "dd807cc1-b8a4-41fe-adc1-ad612247015e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 10,
                "y": 13
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "e123ff59-9f82-484b-9427-64ef889537d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 37,
                "y": 2
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "d57a5da1-35f0-4c4a-b18c-c4ef6723cd8c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 79,
                "y": 13
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "7b98c16a-450f-4a61-b029-336971c37cc2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 93,
                "y": 13
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "764a1b12-d4c9-4849-aff3-7e34dc8086b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 93,
                "y": 24
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "765fba95-b97a-4340-bf6a-5de496b92b92",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 100,
                "y": 13
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "48bf7550-1447-4090-8e66-a0b1245a1175",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 2,
                "y": 24
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "ba29e460-ee70-49d0-8be4-f50d9ba64d73",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 79,
                "y": 24
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "bf7dd2cd-9d08-4725-8535-ef50b223ec6e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 9,
                "y": 24
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "43844684-1811-482f-88d1-67d0a7b722fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 9,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 85,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "0c301696-a43c-4da7-a770-63bb852a616e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 16,
                "y": 24
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "0c45bcfe-c0f2-40e5-973b-91d4ef30430a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 23,
                "y": 24
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "a6da0d7c-1af1-45fc-ad52-4c4005691211",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 30,
                "y": 24
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "20dfcbf7-8108-4b77-985b-7e0edbdc1f90",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 44,
                "y": 24
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "190e9d28-12ae-4935-a007-54d81f395994",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 86,
                "y": 24
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "18491468-831e-4be6-bcdf-0cc585d38f5b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 2,
                "y": 13
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "c04929c8-5b66-4940-99a5-d3241c54e523",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 105,
                "y": 46
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "52097b5e-60f8-4a9d-9e9f-fc602fcb61b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 50,
                "y": 46
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "8ff40557-e970-43c1-b911-7eeb2f9809ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 100,
                "y": 46
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "1efc13d7-8b60-4217-b0a9-f5e852358e59",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 58,
                "y": 13
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "8248da74-4746-4983-bdb6-ecb424534b90",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 50,
                "y": 13
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "a0ede8c1-322b-4c9d-b530-cd271bec446e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 9,
                "offset": 1,
                "shift": 4,
                "w": 4,
                "x": 32,
                "y": 46
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "781455bd-15e6-454c-9019-f67d6e30843e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 26,
                "y": 46
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "bcb40af2-0e24-4a9e-806f-f6bb9a166da8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 20,
                "y": 46
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "3b11990f-03fb-4167-bb01-8752f197b0ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 72,
                "y": 24
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "93d7c9a5-11f8-40db-b0b3-9315c486b01e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 8,
                "y": 46
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "55300e5b-6f37-4026-8328-4c8e54e2902e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 65,
                "y": 24
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "81a7b5c7-b976-4f7a-835b-f30666389c02",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 58,
                "y": 24
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "14692d8a-a61d-4b66-aa2e-626fef6a6e03",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 51,
                "y": 24
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "6fdd9cda-b3bc-4670-9162-3f8b05c7ce83",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 110,
                "y": 35
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "d976e022-c08e-446d-9ec6-859ecd86af72",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 9,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 2,
                "y": 57
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "8efe5887-d214-4ace-9c4e-590e6fc30af8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 37,
                "y": 24
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "78dcaa26-5768-4fd6-82fb-917beae422b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 114,
                "y": 13
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "f73eb1d8-1a83-4a8b-ae57-4780bc93ee9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 95,
                "y": 46
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "b5173b37-6ee3-4ccb-8726-a98d97f9323c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 107,
                "y": 13
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "85f6df22-6ead-4249-8d07-e12d64df3669",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 116,
                "y": 35
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "e6363484-6d96-4f54-825a-ba250aa2acc7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 38,
                "y": 46
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "cad8bf9e-a9b7-4d96-a84a-bab4dd6a94ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 74,
                "y": 46
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "fc6ffe83-8df0-48b5-af21-dea3ded820a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 98,
                "y": 35
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "768e86f1-e279-4968-8767-3740f93748aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 92,
                "y": 35
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "680dd806-aa42-4159-a547-619c5beb6c8c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 86,
                "y": 13
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "e7499ffb-d6f5-4a12-bf57-2d271124a83c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 104,
                "y": 35
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "86fccab7-4df4-4174-a192-d831504fd00b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 2,
                "y": 46
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "252f3bd3-fbe9-448b-8812-1e0bcd4ba28d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 44,
                "y": 46
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "580f1645-dc84-4ee1-957a-b8b6eff11202",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 72,
                "y": 13
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "c1cebd49-c1cc-4149-8188-205bf9422fc8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 9,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 34,
                "y": 13
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "ca82c465-02fe-4975-a94b-9c3fc2869e2e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 65,
                "y": 13
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "272120f3-118b-41c9-a344-0cf33dd27a8a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 18,
                "y": 13
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "b3dbd6e0-ca80-48a5-aa21-09ffeb8909b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 85,
                "y": 46
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "13ceca35-ed0d-4426-9c80-43be02766583",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 9,
                "offset": 0,
                "shift": 2,
                "w": 1,
                "x": 22,
                "y": 57
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "0c1098d3-a8ab-47f4-ab2d-679202528da5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 115,
                "y": 46
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "d0f73475-1668-4477-83dd-9ebf221172e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 21,
                "y": 2
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 4,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}