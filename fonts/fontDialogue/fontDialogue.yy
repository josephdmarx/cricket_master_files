{
    "id": "05900f93-03ea-4d85-ab01-21e322a34be9",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fontDialogue",
    "AntiAlias": 0,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "8-bit pusab",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "861f8c25-af36-4b93-a821-8422da869975",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 13,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 40,
                "y": 107
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "cc00169d-9c66-4bc9-aca4-d36076ad82b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 13,
                "offset": 3,
                "shift": 7,
                "w": 3,
                "x": 56,
                "y": 107
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "64ebb273-9b7c-4d67-8cc2-a0d9d4d372dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 13,
                "offset": 2,
                "shift": 7,
                "w": 6,
                "x": 65,
                "y": 92
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "7da21816-f920-406f-ab60-7f60e2cfc65b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 13,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 68,
                "y": 47
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "562b4885-ea8d-400b-9489-48a3238ff30a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 13,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 2,
                "y": 62
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "25151fb9-1e24-4001-806c-933badf883db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 13,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 18,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "68209e6b-9645-4cc3-8e4e-52ee3fd205a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 13,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 92,
                "y": 2
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "9de63616-fac9-48b9-a95f-208cc409c4b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 13,
                "offset": 2,
                "shift": 7,
                "w": 5,
                "x": 116,
                "y": 92
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "b8239881-fd03-48c5-aafc-42015efb350a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 13,
                "offset": 0,
                "shift": 3,
                "w": 4,
                "x": 28,
                "y": 107
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "4a06b4ca-ee10-43eb-90ec-b3ecee3eaabe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 13,
                "offset": 0,
                "shift": 3,
                "w": 4,
                "x": 22,
                "y": 107
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "265d0c3a-c805-4ff8-b075-b77765a84420",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 13,
                "offset": 2,
                "shift": 11,
                "w": 10,
                "x": 68,
                "y": 2
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "7c21cf70-f374-4abe-b2ec-9a197ff8233e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 13,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 38,
                "y": 92
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "8b57e5c9-2669-40d6-a623-c4d0c3359f3d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 13,
                "offset": 0,
                "shift": 3,
                "w": 4,
                "x": 16,
                "y": 107
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "994ae291-e70f-4106-9ddc-666687c63164",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 13,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 56,
                "y": 92
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "7590a6b3-252d-48d2-8614-33b1297b13da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 13,
                "offset": 0,
                "shift": 2,
                "w": 3,
                "x": 66,
                "y": 107
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "f8e4a82c-b4cb-453d-9389-812dd282b4a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 2,
                "y": 92
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "cc6d8668-5e5f-49cc-a596-b5c055af73b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 13,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 13,
                "y": 47
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "952f83b8-ff0c-40d6-9e89-e52ed96562c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 13,
                "offset": 2,
                "shift": 8,
                "w": 6,
                "x": 73,
                "y": 92
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "dc62bb5d-38ea-496d-affc-86e5bc74dd4c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 13,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 35,
                "y": 47
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "4556a599-102e-4ae3-b990-373e4f8d71c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 13,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 22,
                "y": 77
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "8abdd368-6a8e-4f65-a961-1e63f8e3a09b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 13,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 56,
                "y": 2
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "0dc31153-21d4-41f7-bdad-83cb5fd5d1fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 13,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 44,
                "y": 2
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "5b51ad39-aaa5-4560-8da1-b318a9bc6102",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 13,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 57,
                "y": 47
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "81501f58-baec-4d58-a22b-c6592e70a1d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 13,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 2,
                "y": 47
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "f3d15c3e-1bd7-45fb-87a7-c8a4acfcd25c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 13,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 46,
                "y": 47
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "34f7ef48-4748-418e-b415-7ceb5d515d27",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 13,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 32,
                "y": 2
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "ba6902bd-089d-4f93-b7a2-f0d33b24b877",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 13,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 46,
                "y": 107
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "1a25b5b9-d5f7-4805-8c38-2e19c3447ba5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 13,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 34,
                "y": 107
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "8f52cf84-ed51-4ba8-a915-cfd119d5b731",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 29,
                "y": 92
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "d3f843d1-d0c4-444b-874c-8d27a1c8b2ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 13,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 20,
                "y": 92
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "6857a9e7-ced1-4d20-95ef-8f97d5919d47",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 116,
                "y": 77
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "292605b7-c1fb-47a5-ba7f-7efa0163749d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 13,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 24,
                "y": 47
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "89cd2e4d-266a-4379-af44-0f0780608505",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 14,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "7c185203-daee-4bd2-aa54-608e586ecb09",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 13,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 2,
                "y": 32
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "2f14a10e-612f-4efe-b28d-596b4896a722",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 13,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 115,
                "y": 2
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "8eb7a057-40cc-4f69-9e7b-13309e185fb4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 13,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 102,
                "y": 62
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "fca3d536-0610-4265-9132-3a37b23b6878",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 13,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 2,
                "y": 17
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "6cdc1411-a0b8-44b9-8101-849ea28475fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 13,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 13,
                "y": 17
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "80183c26-9186-4e18-b773-a1071f8a58d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 13,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 35,
                "y": 17
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "3331b049-3304-435b-9302-e50e97848b63",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 13,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 2,
                "y": 77
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "29f2d6ec-639a-4d1c-b076-a53d2dee2f8e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 13,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 46,
                "y": 17
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "8473710f-0040-4104-b842-c4bd77ba6cf7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 13,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 52,
                "y": 77
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "b34f1a57-f3ed-48e5-ba48-c49d009503ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 13,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 57,
                "y": 17
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "ec133d41-6856-47fe-bfd8-1f64421b0b74",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 13,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 90,
                "y": 17
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "73089f0d-1ed6-443a-902e-797b0da5543a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 13,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 101,
                "y": 17
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "7e496305-a339-410b-a9e9-6db1f868c7fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 13,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 90,
                "y": 32
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "a89dc049-d067-4fb4-a405-8b39ea7c5d35",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 13,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 112,
                "y": 17
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "5c5d528e-4e45-4dcc-935a-7363f0418100",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 13,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 112,
                "y": 32
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "d336f045-6637-4a71-b95c-5162e5df36a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 13,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 101,
                "y": 32
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "9bd8ab8a-da5b-4db8-af82-ce7d5512edda",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 13,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 13,
                "y": 32
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "9f53da10-91cb-426e-b0c7-eecedb7dd293",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 13,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 24,
                "y": 32
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "dfba923f-8a4a-4862-a689-aed04971d063",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 13,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 35,
                "y": 32
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "6f875c5f-623d-4aef-97d3-0e0e4df74796",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 13,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 42,
                "y": 62
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "90d15322-a801-4db5-ad04-2d8071a80ec0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 13,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 46,
                "y": 32
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "f94a5d5e-6c6b-4d08-8ea5-34de09dfad74",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 13,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 68,
                "y": 32
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "2a5d3375-b6fc-438d-8f86-2e513112acfd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 13,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 79,
                "y": 32
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "137dec41-49af-43f2-a361-0c8da1df7dc9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 13,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 104,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "7a02720e-0bca-4560-a0ec-65a0048edbd3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 13,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 57,
                "y": 32
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "c5e69511-024a-42b2-8b95-4db00cc15c5d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 13,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 12,
                "y": 62
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "ca87d4bb-d6af-4199-9b36-65758f8b86c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 13,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 2,
                "y": 107
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "f92f8e24-a204-41d4-b0bb-51c08a21ff3a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 11,
                "y": 92
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "2a5af5db-739b-4bbf-a5e1-173b76607f11",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 13,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 9,
                "y": 107
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "c03f9da4-63ab-4bd1-b109-914ebd128853",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 13,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 22,
                "y": 62
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "b16415c0-f7de-4ca7-b5fb-57b0d2d53aa2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 13,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 98,
                "y": 47
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "8a5a2b6c-08f9-4ee9-9a3e-da14f8cf97ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 13,
                "offset": 2,
                "shift": 7,
                "w": 5,
                "x": 102,
                "y": 92
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "65b68cac-136f-4c8a-a1e7-2870953010ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 13,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 47,
                "y": 92
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "544bf620-05a6-4ae3-a688-0c8bad7117d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 13,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 12,
                "y": 77
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "d5e02e65-4def-4c23-9d6f-1fc605f70cca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 13,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 62,
                "y": 77
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "407976b9-6978-4f5a-aa06-d103eee08246",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 13,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 32,
                "y": 62
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "06e05e39-f452-47f9-a184-d7ad027cb66e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 13,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 89,
                "y": 77
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "e2d3c6cf-deee-4ffd-bc7b-819553569f93",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 13,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 80,
                "y": 77
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "7b59b70b-a8a7-4452-91aa-f95f8ccb76c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 13,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 71,
                "y": 77
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "e8112f8f-43e1-47b8-9695-c4068f1e4ffa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 13,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 108,
                "y": 47
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "94f86fed-ce6b-44a6-8ab9-9573ec638167",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 13,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 51,
                "y": 107
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "22daceda-d7a5-44b0-bf29-c07455797b47",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 13,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 88,
                "y": 47
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "1056a834-e7c5-4dcb-8b3a-5863cf8815bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 13,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 78,
                "y": 47
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "9dfb1bdb-0a8d-4077-934e-87f2b165f786",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 109,
                "y": 92
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "f61e6fbb-50f6-43c9-bdab-921ebc697fc2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 13,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 79,
                "y": 17
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "e2df6dc3-6294-4af3-84da-e564e4a55329",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 13,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 52,
                "y": 62
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "d4a0a98c-c773-4b3d-a14d-5fdb10c4b056",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 13,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 107,
                "y": 77
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "21a1d8de-2a17-4970-a4c5-c8ecac61ae6a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 13,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 62,
                "y": 62
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "244c43d7-35f3-4516-8a10-99103679bcc1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 13,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 42,
                "y": 77
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "46ed1490-f2e3-4be9-aa6c-1bac1010f8fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 13,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 32,
                "y": 77
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "5602b732-67e3-4091-a91c-3c52b3e65e19",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 13,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 98,
                "y": 77
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "96b56875-88ff-4f2f-ae4d-5a3a35342b52",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 13,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 88,
                "y": 92
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "6b099735-e3cd-4d51-bca1-1f289c64f0f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 13,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 112,
                "y": 62
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "136b0739-1150-43df-ba94-e957a033e1b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 13,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 72,
                "y": 62
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "80824243-48af-44c8-bea0-afd23a881711",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 13,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 68,
                "y": 17
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "e0c77041-e3ac-4f41-9f6e-25d8ac948097",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 13,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 24,
                "y": 17
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "f2be90c0-b8db-4e0a-a58c-9f210e8422d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 13,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 92,
                "y": 62
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "8eaab83a-a7d7-49aa-af05-841cfb7b6a26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 13,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 82,
                "y": 62
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "cc601dc2-09c2-48c4-b869-f80f281862a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 13,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 81,
                "y": 92
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "17119659-6c11-45f2-bfab-64633a63f075",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 13,
                "offset": 0,
                "shift": 2,
                "w": 3,
                "x": 61,
                "y": 107
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "157a4530-a4fc-434b-ba7e-ee2066ce7842",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 13,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 95,
                "y": 92
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "be38599c-31ae-4fc8-9c93-05759f9f48d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 13,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 80,
                "y": 2
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 6,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}