{
    "id": "27ae2e65-0975-4930-a015-2e9dfe662ed8",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "font03",
    "AntiAlias": 0,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "3Dventure",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "0a0493de-d50b-4874-9e52-5b2bbacc87d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 25,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 217,
                "y": 137
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "a34212b5-a60f-4cd7-8556-769dce516b6f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 25,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 157,
                "y": 137
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "9c02c361-c713-43eb-b0b7-edd0d69762df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 25,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 78,
                "y": 2
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "dd22de68-1cb7-426e-95e1-6eec38011239",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 25,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 2,
                "y": 110
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "dfc8b0ec-584f-4439-a7b4-838823dd5270",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 25,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 226,
                "y": 83
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "f338e259-7035-4ee4-aaae-ee4f326191b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 25,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 210,
                "y": 83
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "715f4de1-121b-4f9e-831b-880bb42b2dfb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 25,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 194,
                "y": 83
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "2b289307-b6e2-4db8-b44a-908ca0635ff4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 25,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 207,
                "y": 137
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "7737ba4f-7cb9-4720-b5b2-d986e2135c81",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 25,
                "offset": 0,
                "shift": 11,
                "w": 9,
                "x": 124,
                "y": 137
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "e84dfbf2-974c-4d15-a8c3-709b3454574b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 25,
                "offset": 0,
                "shift": 11,
                "w": 9,
                "x": 113,
                "y": 137
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "c9239fc0-2c57-43d6-9bde-497f3199087b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 25,
                "offset": 0,
                "shift": 11,
                "w": 9,
                "x": 102,
                "y": 137
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "e1dbe431-fb3b-4299-8e69-3cb6ead8a0db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 25,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 178,
                "y": 83
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "abb3f589-917a-4681-a315-082572e741d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 25,
                "offset": 0,
                "shift": 11,
                "w": 9,
                "x": 80,
                "y": 137
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "d2740b8e-444b-4191-84a2-48213b663d12",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 25,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 162,
                "y": 83
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "3f807288-bcfe-44d9-9784-a0c6a857e641",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 25,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 187,
                "y": 137
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "1ea0bf20-c733-4671-b716-c8758151c345",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 25,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 146,
                "y": 83
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "ab72b1ce-a78f-4bc4-9474-52da9f0a5697",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 25,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 130,
                "y": 83
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "8bc411d3-c98f-4035-af36-e604d2b22dff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 25,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 114,
                "y": 83
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "399c51a2-65d7-4e74-8ef5-968577a7def9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 25,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 98,
                "y": 83
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "f3d1cf35-0ccb-4bef-a3bb-62bd82590fbe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 25,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 82,
                "y": 83
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "ea3ef6cf-a2fe-4427-afaa-958ab7619034",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 25,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 66,
                "y": 83
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "fb22112a-4a7a-4e77-a60a-19db2eca2f7f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 25,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 50,
                "y": 83
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "6e930c14-7d47-4e32-9ce1-455045218c68",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 25,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 34,
                "y": 83
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "8d4905af-6061-4fd2-afd5-3149ece50ac7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 25,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 50,
                "y": 110
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "33144975-5b50-4c05-922c-5ed22fa0ade7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 25,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 18,
                "y": 110
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "f5fc2f92-f7d8-4bab-833a-1a5106c08447",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 25,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 34,
                "y": 110
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "eb98ce8a-26b2-48bc-9c12-b59e27f6a87d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 25,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 197,
                "y": 137
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "281eace2-9ad7-466f-8087-1ee5f8d6ec30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 25,
                "offset": 0,
                "shift": 11,
                "w": 9,
                "x": 135,
                "y": 137
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "83f2967f-875c-481f-93e4-e06e4a25a54b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 25,
                "offset": 0,
                "shift": 11,
                "w": 9,
                "x": 69,
                "y": 137
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "bba61a13-75f4-4c3c-9a06-5c24af964474",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 25,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 2,
                "y": 137
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "24af0303-33bc-4752-a1ee-a2ce320df358",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 25,
                "offset": 0,
                "shift": 11,
                "w": 9,
                "x": 146,
                "y": 137
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "a353908a-687a-46c5-bd42-cce3626ba0c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 25,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 226,
                "y": 110
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "808a5ee8-9179-4831-9ace-3f4cad56e973",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 25,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 178,
                "y": 110
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "cb8dc180-8a17-4b68-a62a-6e6ea9dcc91d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 25,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 162,
                "y": 110
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "bfc02dcf-4baf-4a28-8399-bc6283b4ec84",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 25,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 146,
                "y": 110
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "52e4bf64-df07-4d8e-aee2-5cb3804bf9ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 25,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 194,
                "y": 110
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "7f6c37e9-a704-44b5-967b-7265861699e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 25,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 130,
                "y": 110
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "26443268-72c2-4dfb-bc83-9c23fb073a18",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 25,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 66,
                "y": 110
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "de8d5c55-da6f-4a77-a646-75dceda4236d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 25,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 114,
                "y": 110
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "3ab02b6d-0517-4a1d-8c6d-ea41aebde453",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 25,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 98,
                "y": 110
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "e392d2d3-97bb-42c5-99b3-dfecd35ef3e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 25,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 82,
                "y": 110
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "361bbae8-8b96-45d1-a3eb-12489e06edd3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 25,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 210,
                "y": 110
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "da34d2e5-a91e-4c48-8495-c9b274aee363",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 25,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 2,
                "y": 83
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "ae45cf7e-daa0-4659-b8a5-958c27c3540f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 25,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 18,
                "y": 83
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "98275e83-94bb-4086-846c-7ed70df989a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 25,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 210,
                "y": 56
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "21ac2d43-d8fa-4897-ab79-e6ecb52efab5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 25,
                "offset": 0,
                "shift": 18,
                "w": 17,
                "x": 59,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "1144a526-4a87-46a6-8644-8aae94260124",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 25,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 82,
                "y": 29
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "508bd07f-2095-49a0-8d31-f2b9695d180c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 25,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 207,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "3521a0d2-5622-41b5-a591-a647a9dcefa0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 25,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 66,
                "y": 29
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "6a561278-885c-40d7-a3a2-debd8254292f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 25,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 50,
                "y": 29
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "de699a6b-f864-4f56-97e1-af8685f22757",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 25,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 34,
                "y": 29
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "ce028ad1-ebc1-48ac-8794-d208d387619c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 25,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 18,
                "y": 29
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "7a641ec1-a55a-4e25-b2b3-30eae8c08085",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 25,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 2,
                "y": 29
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "47f20d5d-011c-4418-a275-60087f12683f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 25,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 239,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "fe282357-cd08-4f07-9acc-f753ff8f6f2c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 25,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 223,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "f26279ad-2bce-444e-9604-b15ce9713782",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 25,
                "offset": 0,
                "shift": 18,
                "w": 17,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "ef2fc5cb-6555-4827-80ac-7319f8597e25",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 25,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 98,
                "y": 29
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "80bd2526-70fa-426b-86d0-d6b1c6e3d472",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 25,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 191,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "c01107fe-4204-43c7-b9aa-c37d3b3efffb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 25,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 175,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "c7f094dd-7d0e-458c-976f-e67f38771ae3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 25,
                "offset": 0,
                "shift": 11,
                "w": 9,
                "x": 91,
                "y": 137
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "2ae1a315-b1ac-4eaa-a41d-98a3deee0849",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 25,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 159,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "b30e6802-2d9a-4e7f-b136-0a59d3c251e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 25,
                "offset": 0,
                "shift": 11,
                "w": 9,
                "x": 58,
                "y": 137
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "7c127c72-05da-46d7-9d3c-90ebe04e0b70",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 12,
                "x": 18,
                "y": 137
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "3ca55f22-d367-4876-bc3e-c3ee68cf8931",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 25,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 143,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "fcdfd4b8-3f32-445f-8bc0-68de07a16bdb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 25,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 177,
                "y": 137
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "af44d643-0f23-457c-aef9-a21140962876",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 25,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 127,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "5d2fd7de-90f1-49b7-876f-ea5caf7f85a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 25,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 111,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "d32d5cb8-367a-4645-a052-373b00d65df4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 25,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 95,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "e979c5c4-bae1-4aec-a3c5-108f289a454b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 25,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 114,
                "y": 29
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "2b7bc0dd-e759-4814-8a28-87f40b74e6c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 25,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 194,
                "y": 56
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "33ae3d69-a493-4b4d-b72c-4bd1ba6d0bfc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 25,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 146,
                "y": 29
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "577a0895-1199-446f-a278-f46b5f8222e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 25,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 130,
                "y": 29
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "453446e3-85ad-436b-9b72-28bbe82ceeee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 25,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 178,
                "y": 56
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "4bf0df6f-b481-45c2-902a-f934fa274eac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 25,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 162,
                "y": 56
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "4d3345e7-6976-4dc3-bcb9-011fee62e44e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 25,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 146,
                "y": 56
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "7469e747-a605-4af9-8de2-7898447347fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 25,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 130,
                "y": 56
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "e97b6085-15c8-41ac-a292-d4a5d8c2010c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 25,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 114,
                "y": 56
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "62e6e6a5-6b72-4a0c-92b0-8e46cb0ffb6f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 25,
                "offset": 0,
                "shift": 18,
                "w": 17,
                "x": 40,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "a2c76cd4-6a1f-42ec-886c-da3cef264d06",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 25,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 98,
                "y": 56
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "5a591190-7006-401e-a6a4-04e818e4c182",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 25,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 82,
                "y": 56
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "0bf10bcf-5c8f-4f93-be8c-4d70f215ee33",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 25,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 66,
                "y": 56
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "0c61a2a9-d5de-4ba3-9c07-c0138bd493a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 25,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 50,
                "y": 56
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "0b67231e-86c6-4f65-aef0-6ad3e58b7071",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 25,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 34,
                "y": 56
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "a72f3fc3-fb8a-40eb-8a74-4399dd2c00e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 25,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 18,
                "y": 56
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "7aa460bb-acda-4c34-b5ab-f9a3a440cb33",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 25,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 2,
                "y": 56
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "9203ae10-cdb6-4101-a0f8-915f20110b79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 25,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 226,
                "y": 29
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "3247e2c2-56e6-4a2a-9aac-dcf96acc01a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 25,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 210,
                "y": 29
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "87c3264c-68f4-42c6-8bf7-ece9fda43628",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 25,
                "offset": 0,
                "shift": 18,
                "w": 17,
                "x": 21,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "b1e9a2bb-c0cb-4a48-8631-1b3d80cab2cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 25,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 194,
                "y": 29
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "6a8ff9e9-ca29-4966-9f5a-1d8a46c583a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 25,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 178,
                "y": 29
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "f508cee3-7eb6-4c3b-8e04-b23b37cac59b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 25,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 162,
                "y": 29
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "55ef82bc-d5ae-4d5c-afb9-f37b4f41523a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 32,
                "y": 137
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "544c803d-320c-4584-84ee-322802ae5944",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 25,
                "offset": 3,
                "shift": 12,
                "w": 8,
                "x": 167,
                "y": 137
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "2bd8cf24-28dd-4bf4-9d6a-f29bb09f3d24",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 45,
                "y": 137
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "17e4a181-31b8-4b91-b8ad-1f1da8315fb3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 25,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 226,
                "y": 56
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 18,
    "styleName": "Medium",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}