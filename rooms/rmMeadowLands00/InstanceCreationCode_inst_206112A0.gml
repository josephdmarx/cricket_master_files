//stars, sound triggers, Edana, Meteor

t_scene_info = 
[
		[cutsceneCamFocus, false, 100, oCamTarget01, true],
		
		//
		// in this section we will have several shooting stars in the distance
		// that appear to sound cues
		//
		[cutsceneWait, 4],
		[cutsceneInstanceCreateSoundCue, 1390, 106, "Effects", oStarShine, noone],
		[cutsceneWait, 3],
		[cutsceneInstanceCreateSoundCue, 1054, 220, "Effects", oStarShine, noone],
		[cutsceneWait, 3],
		
		//Meteor-----
			[cutsceneInstanceCreateSoundCue, 1248, -800, "Effects", oMeteor, sfx_meteor01],
			[cutsceneWait, 1],
			[cutsceneCamFocus, false, 50, oMeteor, false],
			[cutsceneWait, 2],
			[cutsceneInstanceCreateSoundCue, 1248, -800, "Effects", WhiteOverlay, sfxSoftBoom01],
			[cutsceneWait, 2],
	
		//------------
		
		[cutsceneMoveCharacter, oEdana, 1120, 1392, false, 1],
		[cutsceneWait, 1],
		[cutsceneChangeVariable, oEdana, "sprite_index", sEdanaInjuredLookUp],
		[cutsceneWait, 1],
		
		[cutscenePortraitDialogueBox, 
				["A fallen star?"],
				[oEdana],
				["Edana"],
				[sEdanaPort01]
		],
		
		[cutsceneWait, 1],
		
		[cutsceneChangeVariable, oProgressTracker, "chapter", 1],
		[cutsceneTransition, rmChapter, 0, 0],
		
		
		//[cutsceneCamFocus, false, 100, oPlayer, false],
		//[cutsceneWait, 4],
		//[cutsceneSpeechBubble, "z z z", oPlayer],
		//[cutsceneWait, 4],	
		//[cutsceneInstanceDestroy, oSpeechBubbleShort],
		//[cutsceneAnimateCharacter, oPlayer, WAKEUP1],
		//[cutsceneWait, 1],
		//[cutscenePlaySound, sfxCricketYawn,1,0],
		//[cutsceneWait, 4],
		//[cutscenePortraitDialogueBox, 
		//		["Oh no! I'm going to be late!", "I need to go see Edana right away!"],
		//		[oPlayer, oPlayer],
		//		["Cricket", "Cricket"],
		//		[sCricketPort, sCricketPort]
		//	],
		//[cutsceneCamFocus, true, 25, oPlayer, false],
		//[cutsceneAdvanceMainQuest],
		//[cutsceneActivateQuest, QUEST.meet_edana],
		////[cutsceneActivateQuest, QUEST.meadow_town],
		//[cutsceneChangeCharacterState, oPlayer, -1, StateIDLE],
		//[cutsceneActivateRecipe, RECIPETYPE.CUREFEVER],
		//[cutsceneActivateRecipe, RECIPETYPE.HEALTHPOTION],
		////[cutsceneActivateEquipment, WEAPONTYPE.CRITTERNET],
		////[cutsceneActivateEquipment, WEAPONTYPE.WPNCANE],
		//[cutsceneUnlockPlayer]
		//[cutsceneInstanceDestroy, oPlayer]

];