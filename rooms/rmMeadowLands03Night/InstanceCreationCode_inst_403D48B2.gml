trigger_id = 0;
quest = QUEST.go_home;

t_scene_info = 
[
		
		[cutsceneLockPlayer],
		[cutsceneCamFocus, false, 25, oCamTarget01, false],
		
		[cutsceneWait, 2],
		
		[cutsceneCamFocus, false, 25, oPlayer, false],
		
		[cutsceneWait, 1],
		
		[cutscenePortraitDialogueBox, 
				["What are those things?", "I need to get back Edana right away."],
				[oPlayer, oPlayer],
				["Cricket", "Cricket"],
				[sCricketPort02, sCricketPort02]
		],
		
		[cutsceneAdvanceQuest, QUEST.go_home],

		[cutsceneUnlockPlayer]
]
