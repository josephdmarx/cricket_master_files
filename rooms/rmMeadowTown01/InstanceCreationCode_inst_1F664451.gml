trigger_id = 1;
quest = QUEST.meadow_town;

t_scene_info = 
[
		
		[cutsceneLockPlayer],
		[cutsceneCamFocus, false, 25, oPlayer, false],
		
		[cutsceneHidePlayer],
		[cutsceneChangePlayerPosition, 250, 273],
		[cutsceneInstanceCreate, oPlayer.x, oPlayer.y, "Player", oPlayer_CS],
		[cutsceneMoveCharacter, oPlayer_CS, 250, 273, false, 1.5],
		[cutsceneRevealPlayer],
		[cutsceneInstanceDestroy, oPlayer_CS],
		
		[cutscenePortraitDialogueBox, 
				["Meadowtown.", "There's the Mayor!"],
				[oPlayer,oPlayer],
				["Cricket","Cricket"],
				[sCricketPort, sCricketPort02]
		],
		
		[cutsceneCamFocus, false, 25, oMayor, false],
		
		[cutsceneWait, 4],
		
		[cutsceneCamFocus, true, 25, oPlayer, false],
		
		[cutsceneAdvanceQuest, QUEST.meadow_town],  //quest stage is now 2
		[cutsceneAdvanceMainQuest],
		[cutsceneUnlockPlayer]
		
		
];