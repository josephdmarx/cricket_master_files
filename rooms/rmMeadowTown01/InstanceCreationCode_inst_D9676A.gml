trigger_id = 2;
quest = QUEST.meadow_town;

t_scene_info = 
[
		[cutsceneLockPlayer],
		[cutsceneCamFocus, false, 25, oPlayer, false],
		[cutsceneCamFocus, false, 25, oMayor, false],
		
		[cutscenePlaySound, sfxCricketHello, 1, false],
		
		[cutscenePortraitDialogueBox, 
				[	"Hello!", 
					"May I help you?", 
					"Edana sent me! I'm here to help!", 
					"Oh.. I see.", 
					"She couldn't come herself?", 
					"She was in the middle of something important, so she sent me!", 
					"And you are?",
					"Cricket!",
					"Very well, Cricket. Follow me," 
				],
				[oPlayer, oMayor, oPlayer, oMayor, oMayor, oPlayer, oMayor, oPlayer, oMayor],
				["Cricket","Mayor","Cricket", "Mayor", "Mayor", "Cricket", "Mayor", "Cricket", "Mayor"],
				[sCricketPort, sPortraitMayor, sCricketPort02, sPortraitMayor, sPortraitMayor, sCricketPort, sPortraitMayor, sCricketPort, sPortraitMayor]
		],
		
				
		[cutsceneMoveCharacter, oMayor, 2178, 273, false, 3],
		[cutscenePlaySound, sfxWoodDoorOpen, 1, 0],
		[cutsceneInstanceDestroy, oMayor],
		
		[cutsceneCamFocus, true, 25, oPlayer, false],
		[cutsceneAdvanceQuest, QUEST.meadow_town],
		[cutsceneAdvanceMainQuest],
		[cutsceneUnlockPlayer],
];