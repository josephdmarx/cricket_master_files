trigger_id = 10;
quest = QUEST.meadow_town;

t_scene_info = 
[
		
		[cutsceneLockPlayer],

		
			
		[cutscenePortraitDialogueBox, 
				["I have the potion!"],
				[oPlayer],
				["Cricket"],
				[sCricketPort]
		],
		
		[cutsceneChangeXscale, oMayor, -1],
		[cutsceneWait, 1],
		
		[cutscenePortraitDialogueBox, 
				[	"Wonderful!", 
					"Quickly! Hand it to me, and I'll give it to Hurley."
				],
				[oMayor, oMayor],
				["Mayor","Mayor"],
				[sPortraitMayor,sPortraitMayor]
		],
		
		[cutsceneChangeXscale, oMayor, 1],
		//[cutsceneMoveCharacter, oMayor, 20, 0, true, 1], 
		
		[cutsceneWait, 2],
		
		[cutscenePortraitDialogueBox, 
			[	"Is it working?", 
				"I'm not sure..."
			],
			[oPlayer, oMayor],
			["Cricket","Mayor"],
			[sCricketPort02,sPortraitMayor] 
		
		],
		
		//----------create one script for entire sequence--------------//
		[cutsceneInstanceCreate, 448, 288, "Effects_BG", oBed],
		[cutsceneInstanceCreate, 490, 235, "NPCs", oHurley],  
		[cutsceneChangeXscale, oHurley, -1],
		[cutsceneInstanceDestroy, oSickBed],
		//----------create one script for entire sequence--------------//
		
		[cutsceneChangeVariable, oMayor, "IdleSprite", sMayorSurprise],
	
		[cutsceneChangeVariable, oHurley, "vsp", -8],
		
		[cutsceneWait, 1],
		
		//[cutsceneChangeVariable, oHurley, "sprite_index", sHurleyIdle],
		
		[cutscenePortraitDialogueBox, 
			[	"Hurley!", 
				"Wow! I feel like a million bucks!"
			],
			[oMayor, oHurley],
			["Mayor","Hurley"],
			[sPortraitMayor,sPortraitHurley] 
		],
	
		[cutsceneChangeVariable, oMayor, "IdleSprite", sMayorIdle],
		[cutsceneChangeXscale, oMayor, -1],
		[cutsceneWait, 1],
		
		[cutscenePortraitDialogueBox, 
			[	"Well, it certainly seems to have done the trick!", 
				"Be sure to give Edana my thanks when you head home.",
				"Meadowtown is in your debt!"
			],
			[oMayor, oMayor, oMayor],
			["Mayor","Mayor","Mayor"],
			[sPortraitMayor,sPortraitMayor,sPortraitMayor] 
		],
	
		[cutsceneCompleteQuest, QUEST.meadow_town],
		[cutsceneActivateQuest, QUEST.go_home], 
		[cutsceneUnlockPlayer]
]
		
		



