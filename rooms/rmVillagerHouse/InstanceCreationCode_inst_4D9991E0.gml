trigger_id = 5;
quest = QUEST.meadow_town;

t_scene_info = 
[
		
		[cutsceneLockPlayer],
		[cutsceneCamFocus, false, 25, oPlayer, false],
		
			
		[cutscenePortraitDialogueBox, 
				["Jellycap!"],
				[oPlayer],
				["Cricket"],
				[sCricketPort]
		],
		
		[cutsceneChangeXscale, oMayor, -1],
		[cutsceneWait, 1],
		
		[cutscenePortraitDialogueBox, 
				[	"Jellywhat?", 
					"Jellycap! It should cure his fever!", 
					"It grows in caves!", 
					"Hmm.. There are some caves in the bog to the east.", 
					"Okay. I'll be back as soon as I can!"
				],
				[oMayor, oPlayer, oPlayer,oMayor,oPlayer],
				["Mayor","Cricket","Cricket","Mayor","Cricket"],
				[sPortraitMayor,sCricketPort,sCricketPort,sPortraitMayor,sCricketPort]
		],
		
		[cutsceneAdvanceQuest, QUEST.meadow_town],

		[cutsceneUnlockPlayer]
]
		
		



