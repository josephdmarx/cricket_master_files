trigger_id = 3;
quest = QUEST.meadow_town;

t_scene_info = 
[
		
		[cutsceneLockPlayer],
		[cutsceneCamFocus, false, 25, oPlayer, false],
		
		[cutsceneHidePlayer],
		[cutsceneChangePlayerPosition, 390, 257],
		[cutsceneInstanceCreate, oPlayer.x, oPlayer.y, "Player", oPlayer_CS],
		[cutsceneMoveCharacter, oPlayer_CS, 390, 257, false, 1.5],
		[cutsceneRevealPlayer],
		[cutsceneInstanceDestroy, oPlayer_CS],
		
		[cutscenePortraitDialogueBox, 
				["This is Hurley.", "Three days ago he fell ill with a terrible fever.", "He's not getting better.","I'll look in my herbalism guide! [ START ]"],
				[oMayor,oMayor,oMayor,oPlayer],
				["Mayor","Mayor","Mayor","Cricket"],
				[sEmpty, sPortraitMayor, sPortraitMayor, sCricketPort02]
		],
		
		[cutsceneAdvanceQuest, QUEST.meadow_town],
		[cutsceneAdvanceMainQuest],
		[cutsceneUnlockPlayer]
		
		
];