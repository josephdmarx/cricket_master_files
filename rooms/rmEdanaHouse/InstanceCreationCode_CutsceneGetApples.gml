trigger_id = 7;

	t_scene_info = 
	[		
	
			[cutsceneCamFocus, false, 25, oPlayer, false],
			
			[cutsceneWait, 1],
						
			[cutscenePortraitDialogueBox, 
				["Done!", "Good! One last errand for you today.", "I need a few apples. You can use your cane to smack the apple tree outside.", "Bring me three nice ripe ones!"],		
				[oPlayer, oEdana, oEdana, oEdana],
				["Cricket", "Edana", "Edana", "Edana"],
				[sCricketPort, sEdanaPort02, sEdanaPort01, sEdanaPort01]
			],
			
			[cutsceneActivateEquipment, WEAPONTYPE.WPNCANE],
			
			[cutsceneCompleteQuest, QUEST.frog_collecting],
			[cutsceneActivateQuest, QUEST.collect_apples],	
			[cutsceneAdvanceMainQuest],
			
			[cutsceneCamFocus, true, 25, oPlayer, false],
			[cutsceneUnlockPlayer],
			[cutsceneAdvanceTutorial]
	];