trigger_id = 9;

	t_scene_info = 
	[		
		[cutsceneCamFocus, false, 25, oPlayer, false],
			[cutsceneWait, 1],
		
			[cutsceneDialogueSelectionBox, 
		
				sCricketPort, 
				"Throw the frogs in the cauldron?", 
				"Yes", 
				"No", 
				QUEST.collect_apples],
		
			[cutsceneCamFocus, true, 25, oPlayer, false],
			[cutsceneUnlockPlayer]
	];