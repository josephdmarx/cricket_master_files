trigger_id = 6;

	t_scene_info = 
	[		
		[cutsceneCamFocus, false, 25, oPlayer, false],
			[cutsceneWait, 1],
		
			[cutsceneDialogueSelectionBox, 
		
				sCricketPort, 
				"Put the frog in the box?", 
				"Yes", 
				"No", 
				QUEST.frog_collecting],

			[cutsceneCamFocus, true, 25, oPlayer, false],
			[cutsceneUnlockPlayer]
	];