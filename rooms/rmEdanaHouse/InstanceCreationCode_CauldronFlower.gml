trigger_id = 3;

	t_scene_info = 
	[		
			[cutsceneCamFocus, false, 25, oPlayer, false],
			[cutsceneWait, 1],
		
		
			[cutsceneDialogueSelectionBox, 
		
				sCricketPort, 
				"Throw the flowers in the cauldron?", 
				"Yes", 
				"No", 
				QUEST.collect_flowers],
				
			[cutsceneCamFocus, true, 25, oPlayer, false],
			[cutsceneUnlockPlayer]
	];