trigger_id = 4;

	t_scene_info = 
	[		
	
			[cutsceneCamFocus, false, 25, oPlayer, false],
			
			[cutsceneWait, 1],
						
			[cutscenePortraitDialogueBox, 
				["Done!", "Good. Now for your next errand.", "Take this net and catch three frogs, then bring them to me.", "You should be able to find some hopping around near the pond to the east."],
				[oPlayer, oEdana, oEdana, oEdana],
				["Cricket", "Edana", "Edana", "Edana"],
				[sCricketPort, sEdanaPort02, sEdanaPort01, sEdanaPort01]
			],
			
			[cutsceneActivateEquipment, WEAPONTYPE.CRITTERNET],
			
			[cutsceneCompleteQuest, QUEST.collect_flowers],
			[cutsceneActivateQuest, QUEST.frog_collecting],	
			[cutsceneAdvanceMainQuest],
			
			[cutsceneCamFocus, true, 25, oPlayer, false],
			[cutsceneUnlockPlayer],
			[cutsceneAdvanceTutorial]
	];