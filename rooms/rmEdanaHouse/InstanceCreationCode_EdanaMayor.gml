trigger_id = 10;

	t_scene_info = 
	[		
	
			[cutsceneCamFocus, false, 25, oPlayer, false],
			
			[cutscenePortraitDialogueBox, 
				[
					"Edana, who was that guy I just saw leaving the tower?",
					"That was the mayor of Meadow Town. He needs our help.",  
					"There is a villager who is gravely ill, and the Mayor needs a healer.", 
					"You will have to go alone. The potion I'm preparing can't be interrupted.", 
					"Do you remember the spell I taught you?",
					"Uh, yes! I think so!",
					"The road to town shouldn't be dangerous, but just in case.",
					"The mayor is expecting you now. There is no time to waste.",
					"Just continue to the east beyond the pond. The road isn't long."
				],
				[oPlayer, oEdana, oEdana, oEdana, oEdana, oPlayer, oEdana, oEdana, oEdana],
				["Cricket", "Edana", "Edana", "Edana", "Edana", "Cricket", "Edana", "Edana","Edana"],
				[sCricketPort, sEdanaPort01, sEdanaPort01, sEdanaPort01, sEdanaPort01, sCricketPort, sEdanaPort01, sEdanaPort01, sEdanaPort01]
			],
			[cutsceneChangeVariable, oPlayer, "cancast", true],
			[cutsceneCompleteQuest, QUEST.collect_apples],
			[cutsceneActivateQuest, QUEST.meadow_town],
			[cutsceneAdvanceMainQuest],
			
			[cutsceneCamFocus, true, 25, oPlayer, false],
			
			[cutsceneUnlockPlayer],
			[cutsceneAdvanceTutorial]
	];