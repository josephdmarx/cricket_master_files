trigger_id = 1;

t_scene_info = 
[		
		[cutsceneCamFocus, false, 25, oPlayer, false],
		[cutsceneWait, 1],
		
		
		[cutscenePortraitDialogueBox,
				["Cricket. You're Late.", "I'm sorry, I fell asleep under the tree again.", "It's okay. Just get your errands done quickly.", "Go out and gather three coneflowers for me. I'll be waiting."], 
				[oEdana, oPlayer, oEdana, oEdana,],
				["Edana", "Cricket", "Edana", "Edana"],
				[sEdanaPort01, sCricketPort, sEdanaPort02, sEdanaPort02]
		],
		
		[cutsceneCamFocus, true, 25, oPlayer, false],
				
		[cutsceneChangeVariable, oPlayer, "canpickup", true],
		[cutsceneAdvanceMainQuest],
		[cutsceneActivateQuest, QUEST.collect_flowers],
		[cutsceneAdvanceTutorial],
		[cutsceneUnlockPlayer]
	
];