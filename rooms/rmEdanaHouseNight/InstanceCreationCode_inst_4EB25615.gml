trigger_id = 1;
quest = QUEST.go_home;

t_scene_info = 
[
		//[cutsceneInstanceCreate, oPlayer.x, oPlayer.y, "Player", oPlayer_CS],
		//[cutsceneChangeVariable, oPlayer_CS, "sprite_index", sPlayerCrouchHurt],
		
		[cutsceneLockPlayer],
		[cutsceneCamFocus, false, 25, oCamTarget01, false],
		
		[cutsceneCompleteQuest, QUEST.go_home],
				
		[cutsceneWait, 1],
		
		[cutscenePortraitDialogueBox, 
				["Edana!!"],
				[oPlayer],
				["Cricket"],
				[sCricketPort02]
		],
		
		//[cutsceneChangeVariable, oEdana, "sprite_index", sEdanaInjuredLookUp],
		
		[cutsceneWait, 1],
		
		[cutscenePortraitDialogueBox, 
				["No...  Run.."],
				[oEdana],
				["Edana"],
				[sEdanaPort01]
		],
		
		[cutsceneChangeXscale, oAntag, 1],
		
		[cutsceneWait, 1],
		
		[cutscenePortraitDialogueBox, 
				["There you are."],
				[oAntag],
				["???"],
				[sEmpty]
		],
		
		
		[cutsceneChangeVariable, oAntag, "sprite_index", sAntag_Cast],
		
		//[cutsceneChangePlayerPosition, 390, 257],
		[cutsceneHidePlayer],
		
		[cutsceneInstanceCreate, 428, 436, "Player", oPlayer_CS],
		[cutsceneChangeVariable, oPlayer_CS, "sprite_index", sPlayerCrouchHurt],
		
		//[cutsceneInstanceCreate, 510,365, "Effects", oEnergyDark_Cast],
		[cutsceneInstanceCreate, 428,436, "Effects_BG", oEnergyDark_Void],
		//[cutsceneInstanceDestroy, oPlayer_CS],
		
		[cutsceneWait, 3],
		
		[cutscenePortraitDialogueBox, 
				["What's happening!?", "I can't move!"],
				[oPlayer, oPlayer],
				["Cricket","Cricket"],
				[sCricketPort02, sCricketPort02]
		],
		
		[cutsceneWait, 1],
		
		[cutscenePortraitDialogueBox, 
				["Cricket, no!", "Let her go, monster!"],
				[oEdana, oEdana],
				["Edana","Edana"],
				[sEdanaPort02, sEdanaPort02]
		],
		
		[cutscenePortraitDialogueBox, 
				["Cricket? What a foolish name.", "I'm afraid she's coming with me."],
				[oAntag, oAntag],
				["???","???"],
				[sEmpty, sEmpty]
		],
		
		[cutscenePortraitDialogueBox, 
				["Cricket...", "I'm sorry."],
				[oEdana, oEdana],
				["Edana","Edana"],
				[sEdanaPort02, sEdanaPort02]
		],
		
		[cutsceneWait, 2],
		[cutsceneInstanceCreate, oEdana.x, oEdana.y, "Effects", oStaffIgnite],
		[cutsceneChangeVariable, oEdana, "sprite_index", sEdanaInjuredLookUp],
		[cutsceneWait, 1],
		[cutsceneInstanceCreate, oEdana.x, oEdana.y, "Effects", oStaffParticle],
		[cutsceneInstanceCreate, oEdana.x - 40, oEdana.y + 12, "Effects", oMagicBlast01],
		[cutsceneChangeVariable, oEdana, "sprite_index", sEdanaInjuredCast],
		[cutsceneWait, 2],
		
		//Cast effect on Antag
				
		[cutsceneInstanceDestroy, oEnergyDark_Void],
		
		
		
		
];