if (oPlayer.MQstage < 11) trigger_id = oPlayer.MQstage;

{
	
	t_scene_info = 
	[		
			
			[cutscenePortraitDialogueBox, 
				[
					"I should turn back. It could be dangerous out there."
				],
				[oPlayer],
				["Cricket"],
				[sCricketPort]
			],
			
			[cutsceneCamFocus, true, 25, oPlayer, false],
			[cutsceneUnlockPlayer]
	];
	
}