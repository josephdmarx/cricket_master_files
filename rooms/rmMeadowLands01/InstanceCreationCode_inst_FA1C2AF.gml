trigger_id = 9;

t_scene_info = 
[
		[cutsceneCamFocus, false, 25, oPlayer, false],
		[cutsceneInstanceCreate, 360, 289, "Enemies", oMayor],
		[cutsceneMoveCharacter, oMayor, 1526, 289, false, 5],
		[cutsceneInstanceDestroy, oMayor],
		[cutsceneChangeXscale, oPlayer, 1],
		[cutscenePortraitDialogueBox, 
				["Who was that guy?", "I should ask Edana!"],
				[oPlayer,oPlayer],
				["Cricket","Cricket"],
				[sCricketPort02, sCricketPort02]
			],
		[cutsceneCamFocus, true, 25, oPlayer, false],
		[cutsceneAdvanceMainQuest],
		[cutsceneUnlockPlayer]
	
];