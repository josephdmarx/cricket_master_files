trigger_id = 0;

t_scene_info = 
[
		//[cutsceneCamFocus, false, 25, oCamTarget01, true],
		//[cutsceneChangeCharacterState, oPlayer, -1, StateSLEEPTREE],
		//[cutsceneCamFocus, false, 100, oPlayer, false],
		//[cutsceneWait, 4],
		//[cutsceneSpeechBubble, "z z z", oPlayer],
		//[cutsceneWait, 4],	
		//[cutsceneInstanceDestroy, oSpeechBubbleShort],
		//[cutsceneAnimateCharacter, oPlayer, WAKEUP1],
		//[cutsceneWait, 1],
		//[cutscenePlaySound, sfxCricketYawn,1,0],
		//[cutsceneWait, 4],
		//[cutscenePortraitDialogueBox, 
		//		["Oh no! I'm going to be late!", "I need to go see Edana right away!"],
		//		[oPlayer, oPlayer],
		//		["Cricket", "Cricket"],
		//		[sCricketPort, sCricketPort]
		//	],
		//[cutsceneCamFocus, true, 25, oPlayer, false],
		[cutsceneAdvanceMainQuest],
		[cutsceneActivateQuest, QUEST.meet_edana],
		//[cutsceneActivateQuest, QUEST.meadow_town],
		[cutsceneChangeCharacterState, oPlayer, -1, StateIDLE],
		[cutsceneActivateRecipe, RECIPETYPE.CUREFEVER],
		[cutsceneActivateRecipe, RECIPETYPE.HEALTHPOTION],
		//[cutsceneActivateEquipment, WEAPONTYPE.CRITTERNET],
		//[cutsceneActivateEquipment, WEAPONTYPE.WPNCANE],
		[cutsceneUnlockPlayer],
		[cutsceneAdvanceTutorial]

];