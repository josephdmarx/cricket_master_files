trigger_id = 1;

t_scene_info = 
[
		[cutsceneLockPlayer],
		[cutscenePortraitDialogueBox, 
				["Edana's Tower is the other direction!"],
				[oPlayer],
				["Cricket"],
				[sCricketPort02]
			],
		[cutsceneChangeCharacterState, oPlayer, 1, StateWALKPLAYER],
		[cutsceneLockPlayer],
		[cutsceneMoveCharacter, oPlayer, -128, 0,  true, 1],
		[cutsceneChangeCharacterState, oPlayer, -1, StateIDLE],
		[cutsceneUnlockPlayer]
	
];