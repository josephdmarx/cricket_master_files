trigger_id = 7;
quest = QUEST.meadow_town;

t_scene_info = 
[
		
		[cutsceneLockPlayer],
		[cutsceneCamFocus, false, 25, oBossShamblerStill, false],
		[cutsceneWait, 1],
		
		[cutscenePlaySound, sfxRumble, 1, 0],
		
		[cutsceneScreenShake, 3],

		[cutsceneMoveCharacter, oBossShamblerStill, 728, 2948, false, 1],
		[cutsceneInstanceCreate, 728, 2948, "Enemies", oBossShambler],
		[cutsceneInstanceDestroy, oBossShamblerStill],
		
		
		
		//[cutsceneHidePlayer],
		//[cutsceneInstanceCreate, 2960, 705, "Player", oPlayer_CS],
		//[cutsceneMoveCharacter, oPlayer_CS, 3520, 705, false, 1.5],
	
		
		//[cutsceneInstanceCreate, 3520, 705, "Player", oPlayer_Dummy_Vis],
		//[cutsceneInstanceDestroy, oPlayer_CS],
		
		//[cutscenePlaySound, sfxRumble, 1, 0],
		//[cutsceneScreenShake, 1],
		//[cutscenePortraitDialogueBox, 
		//		["Uh oh..."],
		//		[oPlayer],
		//		["Cricket"],
		//		[sCricketPort02]
		//],
	
		//[cutsceneChangePlayerPosition, 3520, 705],
		//[cutsceneRevealPlayer],
		//[cutsceneInstanceDestroy, oPlayer_Dummy_Vis],

		//[cutsceneWait, 1],
		//[cutscenePlaySound, sfxRocksCollapse, 1, 0],
		//[cutsceneWait, 1],
		
		//[cutscenePlaySound, sfxRockyLanding, 1, 0],
		//[cutscenePlaySound, sfxCricketOof, 1, 0],
		//[cutsceneWait, 1],
		
		//[cutsceneCamFocus, false, 25, oPlayer, false],
		//[cutsceneWait, 2],
		
		//[cutscenePlaySound, sfxCricketWhine, 1, 0],
		//[cutscenePortraitDialogueBox, 
		//		["Now where am I?", "I guess I have to find my way back up there."],
		//		[oPlayer, oPlayer],
		//		["Cricket", "Cricket"],
		//		[sCricketPort02, sCricketPort02]
		//],
		[cutsceneAdvanceQuest, QUEST.meadow_town],
		[cutsceneCamFocus, false, 25, oPlayer, false],
		[cutsceneUnlockPlayer]
]
		
		

