{
    "id": "76110fbd-3c0f-48fa-8e25-242f1be6a5c4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oWater",
    "eventList": [
        {
            "id": "9639fbea-703f-408f-b879-db9055fd0f6e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "76110fbd-3c0f-48fa-8e25-242f1be6a5c4"
        },
        {
            "id": "cb1b63d9-7115-4ae4-8d3e-58b7e88b58c2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "76110fbd-3c0f-48fa-8e25-242f1be6a5c4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "9ef076df-e2d4-4c0d-8d64-4340e468d024",
    "visible": true
}