{
    "id": "08901575-66e1-4369-b81a-32f3f0de6272",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oCollider",
    "eventList": [
        {
            "id": "be86ce07-b093-4f14-9e27-32eaea26e99c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "08901575-66e1-4369-b81a-32f3f0de6272"
        }
    ],
    "maskSpriteId": "240da789-6827-442b-8776-82c6c85d75a4",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "240da789-6827-442b-8776-82c6c85d75a4",
    "visible": true
}