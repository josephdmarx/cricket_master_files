{
    "id": "652cbc71-09b2-40c2-a3ff-2465f40ab3cb",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oEssence",
    "eventList": [
        {
            "id": "ef6a495a-56a4-4978-b544-fd0363e99af9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "652cbc71-09b2-40c2-a3ff-2465f40ab3cb"
        },
        {
            "id": "7639d00a-c3ce-4032-a542-6c2189af36af",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "652cbc71-09b2-40c2-a3ff-2465f40ab3cb"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "85e3bbea-8c67-4dfc-87cb-5d787bf16d87",
    "visible": true
}