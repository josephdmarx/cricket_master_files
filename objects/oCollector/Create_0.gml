/// @description Insert description here
// You can write your code in this editor
event_inherited();

interactable = true;
on = false;

name = "Big Luke"
portrait = sPortraitBigLuke;

text = ["Wanna buy something?"];
speakers = [id];

items = [itemtype.healpot01, itemtype.manapot01];
price = [20,30];

IdleSprite = sCollectorIdle;
MoveSprite = sHurleyJump;
JumpSprite = sHurleyJump;

grounded = false;
moving = false;
vsp = 0;

