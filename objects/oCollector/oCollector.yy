{
    "id": "9192b3b7-3bf0-4ee4-a862-3f22acdd017a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oCollector",
    "eventList": [
        {
            "id": "9d1c46d9-b7d1-4e02-ac8f-1097261e00f6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9192b3b7-3bf0-4ee4-a862-3f22acdd017a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "f24d8264-b85d-4e5a-b632-26d55b8117fe",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f8c0816c-e7c3-40e8-94c1-f3a617834a02",
    "visible": true
}