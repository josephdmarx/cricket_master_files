{
    "id": "8ddb84be-7c62-47e2-9f95-3d76169ba762",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "parProjectileEnemy",
    "eventList": [
        {
            "id": "eba5523b-fa7b-420c-9b3b-a1765ad938e4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8ddb84be-7c62-47e2-9f95-3d76169ba762"
        },
        {
            "id": "ef3389d9-449e-4025-b56d-1512745d7617",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8ddb84be-7c62-47e2-9f95-3d76169ba762"
        },
        {
            "id": "97835e8d-917d-4ee7-9b02-67fac4304120",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "5ab0bdeb-238d-489a-9844-fbab736cb673",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "8ddb84be-7c62-47e2-9f95-3d76169ba762"
        },
        {
            "id": "a9879842-a19a-43e7-a1ba-f61890e713fb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "8ddb84be-7c62-47e2-9f95-3d76169ba762"
        },
        {
            "id": "08444671-d435-4b69-ae40-19076f4e9072",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 40,
            "eventtype": 7,
            "m_owner": "8ddb84be-7c62-47e2-9f95-3d76169ba762"
        },
        {
            "id": "9ad66759-b1ff-4ff6-b669-7fde0a244ae2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "e181d273-45da-482e-8383-65bf4b53c74b",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "8ddb84be-7c62-47e2-9f95-3d76169ba762"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f435b0ca-46e2-4f77-bfe9-833178d31b38",
    "visible": true
}