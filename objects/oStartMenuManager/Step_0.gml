/// @description Insert description here
// You can write your code in this editor


if (menu_active)
{	
	oPlayer.hascontrol = false;
	MasterMenuOpen = true;
	GetMenuInput();
	
	if (key_down)
	{
		audio_play_sound(sfxMenuClick, 1, 0);
		active_option ++;
		if (active_option > 7) active_option = 0;
	}
	
	if (key_up)
	{
		audio_play_sound(sfxMenuClick, 1, 0);
		active_option --;
		if (active_option < 0) active_option = 7;
	}

	
	if (key_accept)  //if we press the accept button
	{
		menu_active = false;
								
		#region Inventory
				
		if (active_option == ITEMS)
		{
			audio_play_sound(sfxMenuAccept,1,0);
			with(oInventoryManager) menu_active = true;
		}
		
		#endregion
		
		#region Equipment
		
		if (active_option == EQUIPMENT)
		{
			audio_play_sound(sfxMenuAccept,1,0);
			with (oEquipmentManager) menu_active = true;	
		}
		
		#endregion
		
		#region Magic
		
		if (active_option == SPELLS)
		{
			audio_play_sound(sfxMenuAccept,1,0);
			with (oMagicManager) menu_active = true;	
		}
		
		#endregion
		
		#region Bestiary
		
		if (active_option == BESTIARY)
		{	
			audio_play_sound(sfxMenuCancel,1,0);
			MasterMenuOpen = false;
			active_option = 0;
			
			with(oPlayer)
			{
				inputdelay = 10;
				hascontrol = true;
			}
		}
		
		#endregion
		
		#region Recipes
		
		if (active_option == RECIPES)
		{
			audio_play_sound(sfxMenuAccept,1,0);
			with (oRecipeManager) menu_active = true;
		}
		
		#endregion
		
		#region Go to Map
		
		if (active_option == MAP)
		{	
			audio_play_sound(sfxMenuAccept,1,0);
			oWorldMapManager.menu_active = true;
		}
		
		#endregion
		
		#region Pets
		if (active_option == PETS)
		{	
			audio_play_sound(sfxMenuCancel,1,0);
			MasterMenuOpen = false;
			active_option = 0;
			
			with(oPlayer)
			{
				inputdelay = 10;
				hascontrol = true;
			}
		}
		
		
		#endregion
		
		#region Exit the Menu
		
		if (active_option == EXIT)
		{
			audio_play_sound(sfxMenuCancel,1,0);
			MasterMenuOpen = false;
			active_option = 0;
			
			with(oPlayer)
			{
				inputdelay = 10;
				hascontrol = true;
			}
		}
		
		#endregion
		
	}
	
	if 	(key_start)
	{
						
			audio_play_sound(sfxMenuCancel,1,0);
			MasterMenuOpen = false;
			active_option = 0;
			
			with(oPlayer)
			{
				inputdelay = 10;
				hascontrol = true;
			}
			
			menu_active = false;
	}
	
}