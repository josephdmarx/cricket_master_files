/// @description Insert description here
// You can write your code in this editor

if !(menu_active)
	exit;

DrawStartMenu();

draw_circle_color(40,103 + (active_option * 20), 5,c_black,c_black,false);
draw_circle_color(40,103 + (active_option * 20), 4,c_white,c_white,false);

#region draw buttons

if (gamepad_is_connected(0))
	{
		draw_sprite(sAPad,0,33,335-24);
		draw_text_outlined(50, 322-24, "select");
	}

else if (!gamepad_is_connected(0))
	{
		draw_sprite(sSPACEKey,0,60,335-24);
		draw_text_outlined(100, 322-24, "select");
	}
	
if (gamepad_is_connected(0))
	{
		draw_sprite(sBPad,0,40,335);
		draw_text_outlined(50, 322, "exit");
	}

else if (!gamepad_is_connected(0))
	{
		draw_sprite(sENTERKey,0,60,335);
		draw_text_outlined(100, 322, "exit");
	}



	
#endregion