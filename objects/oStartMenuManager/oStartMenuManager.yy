{
    "id": "adf8c0f9-5778-45e3-a295-1a3d1a593af1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oStartMenuManager",
    "eventList": [
        {
            "id": "cb26371e-4d7c-4a10-b6eb-3d4710386bbe",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "adf8c0f9-5778-45e3-a295-1a3d1a593af1"
        },
        {
            "id": "ed0e746b-635c-4dce-8c7e-e3ba9e451da6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "adf8c0f9-5778-45e3-a295-1a3d1a593af1"
        },
        {
            "id": "ccd6e65d-5d2f-490f-ae82-640d9a92b5e9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "adf8c0f9-5778-45e3-a295-1a3d1a593af1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}