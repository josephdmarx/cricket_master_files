{
    "id": "ec4a9f79-31d7-482d-aa80-fc2b0b90dff8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oPurpleSparkle",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "36be9821-624b-4ec6-b86e-9e38f02e885f",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c601a00b-4579-48f7-8587-2e7509f1e71d",
    "visible": true
}