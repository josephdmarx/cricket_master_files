{
    "id": "038d3214-4b03-43a5-a729-a9499779ff23",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oStaffIgnite",
    "eventList": [
        {
            "id": "e0164910-620e-4d14-b747-20faf3954636",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "038d3214-4b03-43a5-a729-a9499779ff23"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "dc4b7e93-63a0-4151-a4d9-544d3402b43d",
    "visible": true
}