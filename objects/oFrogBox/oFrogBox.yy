{
    "id": "211403d0-74fe-4a0c-b3bc-c4ca0ad15fc1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oFrogBox",
    "eventList": [
        {
            "id": "f8107d31-92cf-4524-836d-5b1ee90fc5ad",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "211403d0-74fe-4a0c-b3bc-c4ca0ad15fc1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "618b439e-aa46-4fcc-a6dc-f62e8c015b7d",
    "visible": true
}