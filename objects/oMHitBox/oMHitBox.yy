{
    "id": "81f4253d-298e-4705-a55d-878dcd152a01",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oMHitBox",
    "eventList": [
        {
            "id": "73b99999-6e00-4375-8d4f-97c869b4de9a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 77,
            "eventtype": 8,
            "m_owner": "81f4253d-298e-4705-a55d-878dcd152a01"
        }
    ],
    "maskSpriteId": "0ea93cc1-617d-462e-b090-d305a68082dc",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "0ea93cc1-617d-462e-b090-d305a68082dc",
    "visible": false
}