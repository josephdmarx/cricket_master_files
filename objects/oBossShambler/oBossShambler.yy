{
    "id": "b6914406-7cf4-465a-baee-9b6225b4cc8e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oBossShambler",
    "eventList": [
        {
            "id": "c837bbd7-e3df-4d8f-98c3-63f3729c707a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b6914406-7cf4-465a-baee-9b6225b4cc8e"
        },
        {
            "id": "e5683fab-4818-4574-a5ac-f932ce6aab92",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "b6914406-7cf4-465a-baee-9b6225b4cc8e"
        },
        {
            "id": "41a1db95-583c-4704-8b34-0af58e9c4125",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b6914406-7cf4-465a-baee-9b6225b4cc8e"
        },
        {
            "id": "cd6b8bf6-4080-44e0-adca-4c8f0e70129d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "3269c2bb-a7cc-47cc-84a0-19141b893294",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "b6914406-7cf4-465a-baee-9b6225b4cc8e"
        },
        {
            "id": "f8845503-1c0c-4618-973a-5883bde2247c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "b6914406-7cf4-465a-baee-9b6225b4cc8e"
        },
        {
            "id": "1b7b2f69-8887-43ba-83b2-62dc1ddb7c3a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "b6914406-7cf4-465a-baee-9b6225b4cc8e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4a6e639a-9057-47d3-904c-c6ce5224f566",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "7dcde0f8-e73e-4d92-9366-465e8ab1cd70",
    "visible": true
}