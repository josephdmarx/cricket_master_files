/// @description Insert description here
// You can write your code in this editor
gui_height = camera_get_view_height(view_camera[0]);
gui_width = camera_get_view_width(view_camera[0]);

spd = 1;
vsp = 0;
grv = 0;
def = 0;
life = 100;
flash = 0;
death = sImpD;
dying = false;
item = oGremlinKnifePU;
rareitem = oGremlinCapPU;   
immune = false;
atkpower = 2;
knockbackforce = 6;
hostile = true;
injured = false;
shootframe = 10;
range = 200;
attackrange = 50
alert = false;
DeathSound = sfx_impDeath;
MoveSprite = sShambler_Walk;
IdleSprite = sShambler_Idle;
AttackSprite = sShambler_Slam;
Attacks = 2;
myItemType = itemtype.none;
grounded = false;
flying = false;
flyaway = false;
wander = false;
wandertimer = 0;
alertimer = (room_speed * 2)
hop = false;
jumper = false;
hider = false;
turret = false;
wake_sprite = noone;
hide_sprite = noone;
sound_wakeup = noone;
egold = 35;
rocks = false;
projectile = oShamblerSpit;

DeathSound = sfx_impDeath;
death = sImpD;
hurt = sShambler_Idle;
stay = false;

State = ShamblerStateIDLE;