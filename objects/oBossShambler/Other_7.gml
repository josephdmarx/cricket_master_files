/// @description switch state when the current animation ends

if (life <= 0)
{
	
}


switch (State)
{
	case ShamblerStateRoar:
		TargetX = oPlayer.x - x;
		TargetY = oPlayer.y - y;
		image_index = 0;
		canattack = true;
		State = ShamblerStateALERT;
		break;
	
	case ShamblerStateSlam:
		rocks = false;
		canattack = true;
		State = ShamblerStatePause;
		break;
		
	case ShamblerStateIDLE:
		break;
		
	case ShamblerStateALERT:
		TargetX = oPlayer.x - x;
		TargetY = oPlayer.y - y;
		break;

	case ShamblerStateATTACK:
		canattack = true;
		State = ShamblerStateIDLE;
		break;
	
	case eStateDEATH:
		if (stay)
		{
			instance_create_layer(x,y,"Enemies", stayframe);
			stayframe.image_xscale = sign(TargetX);
		}
		instance_destroy();
		break;
		
	case ShamblerStateHURT:
		injured = false;
		State = ShamblerStateIDLE;
		break;
		
	case ShamblerStateWAKEUP:
		State = ShamblerStateALERT;
		break;
	
	case ShamblerStatePause:
		State = ShamblerStateMoveBack;
		break;
		
	case ShamblerStateMoveBack:
		TargetX = oPlayer.x - x;
		TargetY = oPlayer.y - y;
		State = ShamblerStateRoar;
		break;
}