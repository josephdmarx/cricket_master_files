if (injured)
	{
		State = ShamblerStateHURT;
	}

if (life <= 0) and (dying = false)
	{
		dying = true;
	
		_targetx = 0;
		_targety = 0;

		alert = false;
		hostile = false;
	
		DropItem(item, rareitem);
		DropGold(egold);
		image_index = 0;
		image_speed = 1;
		sprite_index = death;
		
		playDeath(death,x,y);
		
		if (instance_exists(oSlamBox)) instance_destroy(oSlamBox);	
		audio_play_sound(DeathSound,1,0);
		
		instance_destroy();
				
		State = eStateDEATH;
		
}

script_execute(State);

