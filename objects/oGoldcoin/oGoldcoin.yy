{
    "id": "bf88eafb-ca6e-41c5-bad9-9eaa21680cb1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oGoldcoin",
    "eventList": [
        {
            "id": "008038d1-c89f-4e01-97d1-9d544bd60e5a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "bf88eafb-ca6e-41c5-bad9-9eaa21680cb1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": [
        {
            "id": "4d46fbf2-89a9-43ca-ac0c-7abf918ae7d1",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "6c37065d-eb91-4300-a809-5b569349aa0c",
            "propertyId": "c4210375-1472-4e6a-bd60-cb042778825e",
            "value": "itemtype.redapple"
        }
    ],
    "parentObjectId": "10bdb088-bb60-40db-a9e0-29d6ad8545d5",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "590f2f87-2779-4daa-8967-f372674c8ae2",
    "visible": true
}