{
    "id": "4071088c-45d5-404c-8b14-a543d3254ea4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oBirdIndigo",
    "eventList": [
        {
            "id": "bb773c6f-5cdd-4d4f-968f-02c4a66d81b2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4071088c-45d5-404c-8b14-a543d3254ea4"
        },
        {
            "id": "6d98657d-5899-420d-b537-7cf22af72c3f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "4071088c-45d5-404c-8b14-a543d3254ea4"
        },
        {
            "id": "e947dd9c-97d5-40c6-b16f-8b47d3506349",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 40,
            "eventtype": 7,
            "m_owner": "4071088c-45d5-404c-8b14-a543d3254ea4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "c397ffe4-21bb-4149-a4d9-9d44c5be99c0",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "739aa8c2-caac-4117-ba8d-13cc71474be9",
    "visible": true
}