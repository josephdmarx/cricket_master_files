/// @description Insert description here
// You can write your code in this editor
#region Pause Check

if (global.paused)
{
	image_speed = 0;
	exit;
}

image_speed = 1;

#endregion

TargetX = oPlayer.x - x;
TargetY = oPlayer.y - y;

var _targetx = -(sign(TargetX)*spd);  

if (distance_to_object(oPlayer) < range) and (grounded)
	{
		alert = true;
		grounded = false;
		var _pitch = (random_range(1, 1.2));
		audio_sound_pitch(sfxBird01, _pitch);
		audio_play_sound(sfxBird01,0,false);
	}
	
if (alert)
{

	State = eStateFLYAWAY;
}

else if !(alert)
{
		wandertimer += random_range(0,5);
		if (wandertimer >= alertimer)
		{
			if (wander)
				State = eStateWANDER;
		}
}

script_execute(State);
