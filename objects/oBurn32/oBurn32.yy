{
    "id": "3056e29b-186b-47e9-a1f6-b3906d149a3e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oBurn32",
    "eventList": [
        {
            "id": "8dde86d6-c452-4df0-a5f7-01fdd87fe719",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "3056e29b-186b-47e9-a1f6-b3906d149a3e"
        },
        {
            "id": "4f953cba-1f64-4ba3-9006-966f1f86dba9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3056e29b-186b-47e9-a1f6-b3906d149a3e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "76c57b56-289e-4193-ba82-a441f5064e9b",
    "visible": true
}