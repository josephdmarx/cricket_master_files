{
    "id": "36be9821-624b-4ec6-b86e-9e38f02e885f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "parDust",
    "eventList": [
        {
            "id": "7c951261-3fd1-451c-8b03-ff08d5234737",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "36be9821-624b-4ec6-b86e-9e38f02e885f"
        },
        {
            "id": "d2c4c054-1208-489b-b27c-a29bc7328807",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "36be9821-624b-4ec6-b86e-9e38f02e885f"
        },
        {
            "id": "d5c416c4-aab9-4e5e-ab90-ce32730cc2c9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "36be9821-624b-4ec6-b86e-9e38f02e885f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "608a8ea7-ab67-4c06-91e0-5366d59b2f28",
    "visible": true
}