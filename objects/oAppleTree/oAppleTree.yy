{
    "id": "2f1b7c4e-4fe8-48a5-a773-af4fb7de4084",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oAppleTree",
    "eventList": [
        {
            "id": "52f21378-1361-441d-bd52-a79fa47f2d4b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2f1b7c4e-4fe8-48a5-a773-af4fb7de4084"
        },
        {
            "id": "0776bbc9-b8c6-4e52-b72d-13d9b075cef5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "2f1b7c4e-4fe8-48a5-a773-af4fb7de4084"
        },
        {
            "id": "78295e09-5fb3-4cae-9867-831fec9bfe90",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "2f1b7c4e-4fe8-48a5-a773-af4fb7de4084"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "7c87f15c-1f32-4642-af33-cab711e3015b",
    "visible": true
}