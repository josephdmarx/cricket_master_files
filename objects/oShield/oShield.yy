{
    "id": "e181d273-45da-482e-8383-65bf4b53c74b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oShield",
    "eventList": [
        {
            "id": "1c029e1b-bb8d-423f-81ce-f187abd342b5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e181d273-45da-482e-8383-65bf4b53c74b"
        },
        {
            "id": "465d9b8e-ce71-4b82-a998-12822fae7d06",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e181d273-45da-482e-8383-65bf4b53c74b"
        }
    ],
    "maskSpriteId": "0ea93cc1-617d-462e-b090-d305a68082dc",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "599df1e9-9f93-43ed-9391-349e3d0d5dcf",
    "visible": true
}