{
    "id": "f398d483-a4a1-4892-8fd5-2c3e5c6b0104",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oRecipeManager",
    "eventList": [
        {
            "id": "26e1f485-04e7-407f-9028-af7a1a4ce940",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f398d483-a4a1-4892-8fd5-2c3e5c6b0104"
        },
        {
            "id": "c77673df-de1f-4f36-b1e1-bb41c9ceee84",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "f398d483-a4a1-4892-8fd5-2c3e5c6b0104"
        },
        {
            "id": "d44043c0-80eb-4860-838f-17aa6e5ce052",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f398d483-a4a1-4892-8fd5-2c3e5c6b0104"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "432d7032-d117-44ed-910f-1cb4d16b538f",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}