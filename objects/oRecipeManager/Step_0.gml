if (recipe[activeslot] == RECIPETYPE.NONE) and (activeslot != 0)
	activeslot--;

if ((activeslot) > 2)
		activerow = 1;
		
else if ((activeslot) > 5)
		activerow = 2;
		
else if ((activeslot) > 8)
		activerow = 3;
		
else if ((activeslot) > 11)
		activerow = 4;	
	
if (menu_active) 
{
	if (oQuestManager.ds_quests[# 1, QUEST.meadow_town] == 4)
	{
		oQuestManager.ds_quests[# 1, QUEST.meadow_town] = 5;
	}
	
	GetMenuInput();
	
	if (key_right) and (activeslot != (MAX_ACTIVE_RECIPES -1))
	{
		audio_play_sound(sfxMenuClick,1,0);
		
		activeslot++;
	}
	if (key_left) and (activeslot != 0)
	{
		audio_play_sound(sfxMenuClick,1,0);
		
		activeslot--;
	}
	//if (key_down) and (activeslot < 12)
	//{
	//	audio_play_sound(sfxMenuClick,1,0);
	//	activeslot += 3;
	//}

	//if (key_up) and (activeslot > 2)
	//{
	//	audio_play_sound(sfxMenuClick,1,0);
	//	activeslot -= 3;
	//}
	



	//----INGREDIENT CHECK---------------------------------------------------------//
	var _activerecipetype = recipe[activeslot];
	
	show_debug_message("activeslot");
	show_debug_message(activeslot);

	var _ingtype0 = recipeDefinitions[_activerecipetype, RECIPEPROPERTIES.INGTYPE0];
	var _ingtype1 = recipeDefinitions[_activerecipetype, RECIPEPROPERTIES.INGTYPE1];
	var _ingtype2 = recipeDefinitions[_activerecipetype, RECIPEPROPERTIES.INGTYPE2];
	var _itemtocraft = recipeDefinitions[_activerecipetype, RECIPEPROPERTIES.CRAFTEDITEM];
	
	ingamount0 = oInventoryManager.itemDefinitions[_ingtype0, itemproperties.amount];
	ingamount1 = oInventoryManager.itemDefinitions[_ingtype1, itemproperties.amount];
	ingamount2 = oInventoryManager.itemDefinitions[_ingtype2, itemproperties.amount];
	//----INGREDIENT CHECK---------------------------------------------------------//


	if (key_accept) //craft if all the ingredients are present
	{
		audio_play_sound(sfxMenuAccept,1,0);
		
		AttemptCraftItem(_itemtocraft, _ingtype0, _ingtype1, _ingtype2); 
		
	}
	
	if (key_back)
	{
		audio_play_sound(sfxMenuCancel,1,0);
		
		menu_active = false;
		
		with (oStartMenuManager) menu_active = true;	
	}
}

cursor = activeslot;

