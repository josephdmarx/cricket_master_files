/// @description Insert description here
// You can write your code in this editor

half_w = (display_get_gui_width() * 0.5 );

menu_active = false;

depth = -50;
scale = 2;

activecolumn = 0;
activerow = 0;

ingredient_display = false;

inventory_row = 0;

menu_active = false;
guiHolderWidth = sprite_get_width(sInventoryLarge);
guiHolderPosX = 200;
guiHolderPosY = 95;
guiHolderPad = 5;

guiHolderOffsetX = 31 + guiHolderPad;
guiHolderOffsetY = 31 + guiHolderPad;

ingtype0 = 0;

ingamount0 = 0;
ingamount1 = 0;
ingamount2 = 0;

activeslot = 0; //the slot that we currently have selected with our selector sprite

cursor = 0;
activerecipe = RECIPETYPE.NONE;
maxrecipe = (RECIPETYPE.LENGTH - 1);

menu_active = false;

recipeInitialize();