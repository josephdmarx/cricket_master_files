
if (menu_active)
{

	image_speed = 0;
	
	DrawStartMenu();
	drawMenuButtons();
	
	var _recipetype		= recipe[activeslot];
	var _details		= recipeDefinitions[_recipetype, RECIPEPROPERTIES.DESCRIPTION];
	var _name			= recipeDefinitions[_recipetype, RECIPEPROPERTIES.NAME];
	var _sprite			= recipeDefinitions[_recipetype, RECIPEPROPERTIES.SPRITE];
	
	var _ing0sprite		= recipeDefinitions[_recipetype,RECIPEPROPERTIES.INGSPRITE0];
	var _ing1sprite		= recipeDefinitions[_recipetype,RECIPEPROPERTIES.INGSPRITE1];
	var _ing2sprite		= recipeDefinitions[_recipetype,RECIPEPROPERTIES.INGSPRITE2];
	
	var _ing0name		= recipeDefinitions[_recipetype,RECIPEPROPERTIES.INGNAME0];
	var _ing1name		= recipeDefinitions[_recipetype,RECIPEPROPERTIES.INGNAME1];
	var _ing2name		= recipeDefinitions[_recipetype,RECIPEPROPERTIES.INGNAME2];
	

	#region Draw Everything
	
	draw_sprite(sEqFrame, -1, 190, 250);
	draw_sprite(_sprite, -1, 194, 254);
	
	draw_sprite(sEqFrame, -1, 340, 250);
	draw_sprite(sEqFrame, -1, 390, 250);  
	draw_sprite(sEqFrame, -1, 440, 250);
	
	#region Draw the first ingredient
	
	if (ingamount0 == 0)
	{
		draw_sprite_ext(_ing0sprite, 0, 344, 254,1,1,0,c_black,1);
	}
	
	else if (ingamount0 > 0)
	{
		draw_sprite_ext(_ing0sprite, 0, 344, 254,1,1,0,c_white,1);
	}
	
	
	
	#endregion
	
	#region Draw the second ingredient
	
	if (ingamount1 == 0) 
	{
		draw_sprite_ext(_ing1sprite, 0, 394, 254,1,1,0,c_black,1);
		
	}
	
	else if (ingamount1 > 0)
	{
		draw_sprite_ext(_ing1sprite, 0, 394, 254,1,1,0,c_white,1);
		
	}
	
	
	
	#endregion
	
	#region Draw the third ingredient
	
if (ingamount2 == 0)
	{
		
		draw_sprite_ext(_ing2sprite, 0, 444, 254,1,1,0,c_black,1);
		
	}
	
	else if (ingamount2 > 0)
	{
		
		draw_sprite_ext(_ing2sprite, 0, 444, 254,1,1,0,c_white,1);
		
	}
	
	
	
	#endregion

	draw_text_outlined(500, 255, _ing0name);
	
	draw_text_outlined(500, 265, _ing1name);
	
	draw_text_outlined(500, 275, _ing2name);
	
	draw_text_outlined(190, 310, _details);
	
	#endregion
	
	#region Draw the Recipe entries
	
	for (var i = 0; i < MAX_ACTIVE_RECIPES; i++)
	{
		itemDefIndex = recipe[i];
		recipeName = recipeDefinitions[itemDefIndex, RECIPEPROPERTIES.NAME];
		
			if (itemDefIndex != RECIPETYPE.NONE) and (activerow == 0)
			{
				
				draw_text_outlined((guiHolderPosX + guiHolderPad) + (120 * i),
									(guiHolderPosY + guiHolderPad),
									recipeName);
			}
		
	}
	
	#endregion
	
	#region Draw the Cursor
	
	draw_text_outlined((guiHolderPosX - guiHolderPad) + (120 * cursor) - (120 * (activerow * 3)),
						(guiHolderPosY + guiHolderPad) + (activerow * 15), ">");
	#endregion
	
	
}
	