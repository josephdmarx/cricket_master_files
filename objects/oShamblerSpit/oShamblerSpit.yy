{
    "id": "b5e2a2e8-8f04-49c6-b471-01950af1e324",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oShamblerSpit",
    "eventList": [
        {
            "id": "d2f632b8-ce8f-4246-b9f1-8d3e95774fa4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b5e2a2e8-8f04-49c6-b471-01950af1e324"
        },
        {
            "id": "7f822190-9fa1-4ba1-a6ff-d9b3aad81efc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b5e2a2e8-8f04-49c6-b471-01950af1e324"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "26c8828f-ee73-4d28-86a0-20ad3afe6c11",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b1079b03-3ff9-48e9-bbb9-9a148279e851",
    "visible": true
}