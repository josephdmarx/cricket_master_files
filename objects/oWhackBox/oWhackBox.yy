{
    "id": "3c3791e7-afc0-45bb-87b6-7dc41f955d2a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oWhackBox",
    "eventList": [
        {
            "id": "22a81fb1-8a47-48d0-bfd3-c8c2d5704cca",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 77,
            "eventtype": 8,
            "m_owner": "3c3791e7-afc0-45bb-87b6-7dc41f955d2a"
        },
        {
            "id": "7b93795f-02c5-4f18-8063-4a308c2b703b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3c3791e7-afc0-45bb-87b6-7dc41f955d2a"
        }
    ],
    "maskSpriteId": "d24de41f-eed9-4fa6-9a45-fb36fdeb3ebe",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}