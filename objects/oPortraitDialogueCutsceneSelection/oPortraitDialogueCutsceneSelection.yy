{
    "id": "9e45dee7-ba10-4c56-92c2-3f92f7cb839c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oPortraitDialogueCutsceneSelection",
    "eventList": [
        {
            "id": "9fe5776c-bc7c-418b-866b-f50d86f71d26",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "9e45dee7-ba10-4c56-92c2-3f92f7cb839c"
        },
        {
            "id": "90af7bde-182b-4d1c-a23d-829831828904",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9e45dee7-ba10-4c56-92c2-3f92f7cb839c"
        },
        {
            "id": "1815aff0-0f6c-4e11-b5ee-4cfbafcc0528",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "9e45dee7-ba10-4c56-92c2-3f92f7cb839c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}