/// @description Insert description here
// You can write your code in this editor

//Draw the Box

//draw9Slice(s9slice, 20, 120, 260, 180);

if !(alreadyplayed)
{
	draw_sprite(sDialogueBox,-1,20,120);
	
	if (gamepad_is_connected(0))
	{
		draw_sprite(sYPad,0,255,190);
	}

	else if (!gamepad_is_connected(0))
	{
		draw_sprite(sEKey,0,255,190);
	}

	
	
	draw_sprite(sDialogueSelectionBox, -1, 20, 190);
	draw_sprite_stretched(portrait, -1, 22, 83, 96, 96);
	//DrawSetText(c_white,font04,fa_left,fa_top);

	
	draw_text_outlined_wrap(100+border, 106+border, text_current);

	draw_text_outlined(40+border, 200, option1);
	draw_text_outlined(140+border, 200, option2);

	if (activeoption == 1)
	{
		draw_text_outlined(40, 200, ">");	
	}
	else if (activeoption == 2)
	{
		draw_text_outlined(140, 200, ">");
	}
	
}