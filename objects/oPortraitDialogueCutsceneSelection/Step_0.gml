/// @desc progress text

oPlayer.hascontrol = false;

letters += spd;
soundbuffer ++;

text_current = string_copy(text,1,floor(letters));

if (h == 0)
	{
		h = string_height(text);
	}
	
w = string_width(text_current);


#region SELECTION


show_debug_message(quest);

if (letters >= length) and !(alreadyplayed)
	{
		if (keyboard_check_pressed(ord("A"))) or (gamepad_button_check_pressed(0, gp_padl))
				activeoption = 1;
				//move cursor left
	
		if (keyboard_check_pressed(ord("D"))) or (gamepad_button_check_pressed(0, gp_padr))
				activeoption = 2;
				//move cursor right

		if (keyboard_check_pressed(ord("E"))) or (gamepad_button_check_pressed(0, gp_face4))
			{	
				if (activeoption == 1)
				{
					oQuestManager.ds_quests[# 1, quest] += 1;
					audio_play_sound(sfxPickup01, 1, 0);
					alreadyplayed = true;
					oPlayer.hascontrol = true;
					instance_destroy();
				}
				
				else if (activeoption == 2)
				{
					audio_play_sound(sfxPickup01, 1, 0);	
					alreadyplayed = false;
					oPlayer.hascontrol = true;
					instance_destroy();
				}
			}	
	}
	
#endregion