{
    "id": "37e54b4c-62b9-4d71-b728-17e09862d17c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oScreenShake",
    "eventList": [
        {
            "id": "fd1d28fa-e53b-4c30-9323-8bd4e1ff9250",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "37e54b4c-62b9-4d71-b728-17e09862d17c"
        },
        {
            "id": "a057b7ec-8bbf-4193-ac94-387ad7772e53",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "37e54b4c-62b9-4d71-b728-17e09862d17c"
        },
        {
            "id": "6b672c93-9fa6-4582-8b54-08924e275832",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "37e54b4c-62b9-4d71-b728-17e09862d17c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}