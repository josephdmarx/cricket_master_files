//@description Easy random particle effect!


image_speed = random_range(0.5,1);
image_index = random_range(0,2)
hsp = 0;
vsp = random_range(-1,-2);
image_xscale = choose(1,-1);
image_yscale = choose(1,-1);