{
    "id": "2e7e3331-b2f8-4fad-b7bf-b61314a5087b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "parDustBlack",
    "eventList": [
        {
            "id": "4cd7b965-813a-410f-b481-260473b74151",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "2e7e3331-b2f8-4fad-b7bf-b61314a5087b"
        },
        {
            "id": "97be93d3-d23e-4343-9cb8-cc2979ad62b3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2e7e3331-b2f8-4fad-b7bf-b61314a5087b"
        },
        {
            "id": "65913e3b-3c93-4633-8039-a71a44c445b4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "2e7e3331-b2f8-4fad-b7bf-b61314a5087b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "31c1969d-5686-4cd5-9b0f-445c4588a5e7",
    "visible": true
}