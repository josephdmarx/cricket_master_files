/// @description Insert description here
// You can write your code in this editor

#region Facing Direction

	TargetX = oPlayer.x - x;  
	TargetY = oPlayer.y - y;

	

	if (TargetX > 0)
		image_xscale = 1
	
	if (TargetX < 0)
		image_xscale = -1

#endregion

if (interactable = true)
{
	if (point_in_circle(oPlayer.x,oPlayer.y,x,y,64)) and (on = false)
	{
		on = true; 
		with (instance_create_layer(x + 15, y - 35, "Effects", oSpeechBubbleShort))
		{
			text = "   $   ";	
		}
	}

	if !(point_in_circle(oPlayer.x,oPlayer.y,x,y,64)) and (on = true)
	{
		on = false;
		instance_destroy(oSpeechBubbleShort);
	}
}