//This is now a one time QUEST trigger

if (oQuestManager.ds_quests[# 1, quest] == trigger_id) and (active)
{
	if (!instance_exists(oCutscene)) and !(triggered)
	{
		if (place_meeting(x,y,oPlayer)) 
		{	
			triggered = true;
			createCutscene(t_scene_info);
		}

	}
}

	if !(place_meeting(x,y,oPlayer)) 
		triggered = false;