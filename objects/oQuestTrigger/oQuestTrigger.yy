{
    "id": "7f21f142-95c8-4d22-bbf7-46b62f38bd69",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oQuestTrigger",
    "eventList": [
        {
            "id": "3cc7aaf2-e044-4acf-a61b-a00c7c01d934",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7f21f142-95c8-4d22-bbf7-46b62f38bd69"
        },
        {
            "id": "a729106f-05f1-4a78-82fa-ecbc0270451b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "7f21f142-95c8-4d22-bbf7-46b62f38bd69"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "47123730-a81d-43e3-8eeb-f8e082aaeac6",
    "visible": true
}