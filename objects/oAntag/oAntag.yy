{
    "id": "b280db3f-78a1-4fde-ac5d-1d71761cb907",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oAntag",
    "eventList": [
        {
            "id": "c1f4e981-afb7-4aaa-98af-621e24b052f2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b280db3f-78a1-4fde-ac5d-1d71761cb907"
        },
        {
            "id": "30857596-c516-4d78-8261-e7b9cf7d1269",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b280db3f-78a1-4fde-ac5d-1d71761cb907"
        },
        {
            "id": "6303f188-0d4d-4b97-8d10-4ba220ca95b2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "b280db3f-78a1-4fde-ac5d-1d71761cb907"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ee9c3021-31bc-4cb9-98ae-db20c9eae1b1",
    "visible": true
}