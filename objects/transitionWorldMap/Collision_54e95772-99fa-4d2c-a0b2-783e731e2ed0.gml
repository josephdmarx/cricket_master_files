/// @desc Move to the next room
if (active)
{
	with(oPlayer)
	{
		if (hascontrol)
		{
			hascontrol = false;
		}
	}
	if (oWorldMapManager.menu_active == false) oWorldMapManager.menu_active = true;
}