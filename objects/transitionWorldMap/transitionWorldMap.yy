{
    "id": "1f162153-16ee-4304-abfc-cd0a8e66c4dd",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "transitionWorldMap",
    "eventList": [
        {
            "id": "54e95772-99fa-4d2c-a0b2-783e731e2ed0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "5ab0bdeb-238d-489a-9844-fbab736cb673",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "1f162153-16ee-4304-abfc-cd0a8e66c4dd"
        },
        {
            "id": "0fdf5863-d32e-44e5-a069-4805b1624fb6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1f162153-16ee-4304-abfc-cd0a8e66c4dd"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "98c8dcd6-5f6a-49fe-906f-2de7aba5aa9b",
    "visible": false
}