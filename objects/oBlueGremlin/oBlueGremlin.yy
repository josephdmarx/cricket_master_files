{
    "id": "b8a7dda0-6b8d-45be-b690-44971a18b29a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oBlueGremlin",
    "eventList": [
        {
            "id": "d3361bf7-2d62-4843-bcc6-9a0086d5313a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b8a7dda0-6b8d-45be-b690-44971a18b29a"
        },
        {
            "id": "2a812f45-c4b0-4a84-893f-fb8af6aafd28",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "3c3791e7-afc0-45bb-87b6-7dc41f955d2a",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "b8a7dda0-6b8d-45be-b690-44971a18b29a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4a6e639a-9057-47d3-904c-c6ce5224f566",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "516ff03d-5532-46cd-b918-a0f5b4b33588",
    "visible": true
}