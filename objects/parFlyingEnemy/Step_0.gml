TargetX = oPlayer.x - x;
TargetY = oPlayer.y - y;

image_xscale = sign(TargetX);

if !(hurt)
{	
	image_speed = 1;
	whackable = true;
}

if (life <= 0) and (dying = false)
{
	dying = true;
	spd = 0;
	image_index = 0;
	sprite_index = death;
	
	playDeath(death, x, y);
	
	DropItem(item, rareitem);
	instance_destroy();
}

if (attacking)
MoveLUNGE(oWall);

if (hurt)
MoveHURT(oWall);

script_execute(State);