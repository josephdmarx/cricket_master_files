{
    "id": "674e4a10-15be-454e-a541-a349c20f44a3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "parFlyingEnemy",
    "eventList": [
        {
            "id": "a9f5634a-06fa-4336-b475-7c57da156371",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "674e4a10-15be-454e-a541-a349c20f44a3"
        },
        {
            "id": "b40a8757-678a-4aa6-beb5-0e92aa2fdda9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "674e4a10-15be-454e-a541-a349c20f44a3"
        },
        {
            "id": "220684fe-7165-43c8-b2b4-f91d8ef73fc6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "674e4a10-15be-454e-a541-a349c20f44a3"
        },
        {
            "id": "2376dcb7-7bca-4b5a-9292-2743531a0476",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "674e4a10-15be-454e-a541-a349c20f44a3"
        },
        {
            "id": "db1cdd29-dc11-4256-b8fa-369dd2fa601c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "3269c2bb-a7cc-47cc-84a0-19141b893294",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "674e4a10-15be-454e-a541-a349c20f44a3"
        },
        {
            "id": "2ce7557a-3668-4c03-b4c3-b8ac29346d34",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "3c3791e7-afc0-45bb-87b6-7dc41f955d2a",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "674e4a10-15be-454e-a541-a349c20f44a3"
        }
    ],
    "maskSpriteId": "0ea93cc1-617d-462e-b090-d305a68082dc",
    "overriddenProperties": null,
    "parentObjectId": "e41eb36a-da0c-468f-82ad-8d63c346e7e9",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "0ea93cc1-617d-462e-b090-d305a68082dc",
    "visible": true
}