
if (instance_exists(oPlayer))  //safety check for player Exist
{
	
	var _playery = oPlayer.y;
	
	
	if (_playery + 30 > y)
	{
		sprite_index = -1;  //no collision mask
	}
	
	else if (oPlayer.key_down)
	{
		if (oPlayer.key_jump) sprite_index = -1;
	}
	
	else
	{
		sprite_index = sFloorOneWay;  //collision mask	
	}
}

