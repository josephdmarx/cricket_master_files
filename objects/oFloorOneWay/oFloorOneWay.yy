{
    "id": "8258286e-bd1c-4f93-93db-9e389c57d7ed",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oFloorOneWay",
    "eventList": [
        {
            "id": "71251adf-32aa-4d30-a48b-991e6969b3f3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8258286e-bd1c-4f93-93db-9e389c57d7ed"
        },
        {
            "id": "3808ea29-0ad0-4d63-8c83-05996d3bcc5e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "8258286e-bd1c-4f93-93db-9e389c57d7ed"
        },
        {
            "id": "580332e6-ae2d-4ca1-975c-a419fb4c2925",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8258286e-bd1c-4f93-93db-9e389c57d7ed"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "1e5b4d39-1484-49d0-97bb-d4436467508a",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f3f9d9d7-1e6f-4379-922a-9f1737423724",
    "visible": true
}