{
    "id": "7c190c06-c3cf-485b-bcb8-b10e5ee56d2c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oFireBurn",
    "eventList": [
        {
            "id": "30b48c73-1005-424b-866b-2f2a916faed8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7c190c06-c3cf-485b-bcb8-b10e5ee56d2c"
        },
        {
            "id": "504116f4-d3b4-493e-bde4-730ccd7a6499",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "7c190c06-c3cf-485b-bcb8-b10e5ee56d2c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "34f8bb68-0dd8-461d-ab3c-684320fad985",
    "visible": true
}