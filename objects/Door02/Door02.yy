{
    "id": "45eed800-4260-4f7e-8909-133b84b728a3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Door02",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "503953ce-3280-44c8-bb6e-175dfea7cbdc",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "86472daa-4aa3-46f4-a2ae-1cb92edff45e",
    "visible": true
}