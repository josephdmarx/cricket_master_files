TargetX = oPlayer.x - x;
TargetY = oPlayer.y - y;

spd = 1;
hsp = 0;
vsp = 0;
grv = 0;
def = 0;
life = 1;
flash = 0;

knockbackforce = 5;
dying = false;
item = oManaGem;
rareitem = oManaGem;
immune = false;
atkpower = 1;
hostile = true;
range = 200;
attackrange = 100;
alert = false;
attacking = false;
DeathSound = sfx_impDeath;
MoveSprite = sImp;
IdleSprite = sImp;
AttackSprite = sImpWindup;
myItemType = itemtype.none;
grounded = false;
flying = true;
flyaway = false;
injured = false;
wander = false;
wandertimer = 0;
alertimer = (room_speed * 2)
hop = false;
jumper = false;
hider = false;
turret = false;
wake_sprite = noone;
hide_sprite = noone;
sound_wakeup = noone;
stay = false;
egold = 1;
whackable = true;
hurt = false;

attacktype = LUNGE;

death = sImpD;

State = eStateIDLE;