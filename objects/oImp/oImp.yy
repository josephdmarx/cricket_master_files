{
    "id": "2754ad60-2b0b-4e34-9a5a-877763465815",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oImp",
    "eventList": [
        {
            "id": "e56d12f2-b35f-42a1-9a1d-d3ac60429489",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2754ad60-2b0b-4e34-9a5a-877763465815"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "674e4a10-15be-454e-a541-a349c20f44a3",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "609f408e-380e-40aa-a352-ae82c044ef17",
    "visible": true
}