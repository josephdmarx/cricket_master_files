/// @description Insert description here
// You can write your code in this editor

for (var i = 0; i < oInstanceManager.no_of_chests; i++)
{
	if (id == oInstanceManager.ids_of_opened_chests[i])
	{
		open = true;
	}
}


if (open)
{
	image_speed = 0;	
	image_index = 5;
	interactable = false;
}

if (room != location)   //turns of the instance if it's not in the corrent room.
{
	interactable = false;
}

#region Open the Chest 

if (interactable = true) and (oPlayer.canpickup = true)
{
	if (point_in_circle(oPlayer.x,oPlayer.y,x,y,56)) and !(open)
	{
		on = true;
		image_index = 1;
		//instance_create_layer(x,y-40,"GUI",oBubbleTail);
		//instance_create_layer(x,y-40,"GUI",oEKey);
	}

	if !(point_in_circle(oPlayer.x,oPlayer.y,x,y,56)) or (open)
	{
		on = false;
		image_index = 0;
		//instance_destroy(oBubbleTail);
		//instance_destroy(oEKey);
	}
}

if (on) and (oPlayer.key_interact)
{	
		image_speed = 1;
		interactable = false;
}

if (image_index >= 5) 
{

	image_speed = 0;
}

#endregion

#region Drop Item


if (image_index >= 3) and !(open)
{
	open = true;
	
	oInstanceManager.ids_of_opened_chests[oInstanceManager.no_of_chests] = id;
	oInstanceManager.no_of_chests ++;
	
	if (gold)
	{
		DropGold(goldamount);
	}
	
	else
	{
		ChestItem(item);
	}
	
}

#endregion