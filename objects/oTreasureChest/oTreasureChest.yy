{
    "id": "e38f57b4-b03a-4b6d-ad52-a3b3b2850cb7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oTreasureChest",
    "eventList": [
        {
            "id": "d8d0ee40-14fc-4e90-90a6-46af60bc126b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e38f57b4-b03a-4b6d-ad52-a3b3b2850cb7"
        },
        {
            "id": "7e1e3812-0f47-48b8-95ca-5884f29f1e84",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e38f57b4-b03a-4b6d-ad52-a3b3b2850cb7"
        },
        {
            "id": "bbe25d2d-7dc2-4ab0-8f56-f6cfb3836b3c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "e38f57b4-b03a-4b6d-ad52-a3b3b2850cb7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "fc99f2f2-ed73-416c-98fc-fc240d40f017",
    "visible": true
}