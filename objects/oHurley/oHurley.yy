{
    "id": "64b3a668-c373-4350-bce4-e9c8ace8b533",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oHurley",
    "eventList": [
        {
            "id": "123c15db-969b-4df5-9ebe-4cfdf7de4021",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "64b3a668-c373-4350-bce4-e9c8ace8b533"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "5d4b5c14-2c31-4dff-9cc2-b8b8bc2d63dc",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "9d8a56db-8824-4404-82dd-ec8101f6dc21",
    "visible": true
}