/// @description Insert description here
// You can write your code in this editor
interactable = true;
on = false;

name = "Hurley"
portrait = sPortraitHurley;

text = ["This person has nothing to say", "You should probably leave."]
speakers = [id, id];

IdleSprite = sHurleyIdle;
MoveSprite = sHurleyJump;
JumpSprite = sHurleyJump;


grounded = false;
moving = false;
vsp = 0;