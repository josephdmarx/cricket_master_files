{
    "id": "9c55066c-10e5-43a7-83d8-ae64a1ee1988",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oFire01D",
    "eventList": [
        {
            "id": "1f83e2de-1ca6-4c71-823f-2e812daa5b1b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9c55066c-10e5-43a7-83d8-ae64a1ee1988"
        },
        {
            "id": "4f784200-f2ee-404a-9605-d89d3964ad2b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "9c55066c-10e5-43a7-83d8-ae64a1ee1988"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "714d530b-af52-4f53-913a-864a9456810a",
    "visible": true
}