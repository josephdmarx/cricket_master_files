{
    "id": "9ef9a7fb-88d9-4ca9-bbe9-736bf37e8024",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oEnergy01",
    "eventList": [
        {
            "id": "80d3f8cb-9e83-4e39-95cc-5abc1f1db629",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9ef9a7fb-88d9-4ca9-bbe9-736bf37e8024"
        },
        {
            "id": "31ea338b-252c-43ef-9900-39eab7d18cf4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "9ef9a7fb-88d9-4ca9-bbe9-736bf37e8024"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "36be9821-624b-4ec6-b86e-9e38f02e885f",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "dc4b7e93-63a0-4151-a4d9-544d3402b43d",
    "visible": true
}