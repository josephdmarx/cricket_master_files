{
    "id": "72edf04a-743c-4eb5-a01f-1a5ee703e434",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "FallingPlatform01",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "96b2439d-813b-4553-8172-16d3a30b4f51",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "bee10a14-a349-4827-b63b-d3d9cbc48a5c",
    "visible": true
}