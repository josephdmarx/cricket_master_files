{
    "id": "bdfec41a-45c5-4f8c-a717-e67bb97ce72b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "parSplashGreen",
    "eventList": [
        {
            "id": "c495678e-8fe8-481b-adec-e583e0d1669c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "bdfec41a-45c5-4f8c-a717-e67bb97ce72b"
        },
        {
            "id": "02622c04-5a34-42e2-8f4e-5f72f3a86d72",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "bdfec41a-45c5-4f8c-a717-e67bb97ce72b"
        },
        {
            "id": "8148b7b7-49a4-4054-8a14-775221de158b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "bdfec41a-45c5-4f8c-a717-e67bb97ce72b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "43df5b12-fe3d-4cba-b902-a8494e4836fb",
    "visible": true
}