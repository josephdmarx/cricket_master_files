{
    "id": "e18ccc66-7d9e-4c8e-a37d-069344f8ea58",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "parJumpingEnemy",
    "eventList": [
        {
            "id": "642dd92f-f9da-46be-9637-8072f6e2ce02",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e18ccc66-7d9e-4c8e-a37d-069344f8ea58"
        },
        {
            "id": "c8690da2-34b0-48a7-b61a-3ca8ed2321f7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e18ccc66-7d9e-4c8e-a37d-069344f8ea58"
        },
        {
            "id": "c5808b04-af83-46f9-b725-2b79ee17ddfa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "e18ccc66-7d9e-4c8e-a37d-069344f8ea58"
        },
        {
            "id": "7730cb55-63ed-4d74-bf92-ab1ff33938a2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "e18ccc66-7d9e-4c8e-a37d-069344f8ea58"
        },
        {
            "id": "8170499d-9cef-4d99-bb83-6bf6fa3f67f3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "3269c2bb-a7cc-47cc-84a0-19141b893294",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "e18ccc66-7d9e-4c8e-a37d-069344f8ea58"
        }
    ],
    "maskSpriteId": "0ea93cc1-617d-462e-b090-d305a68082dc",
    "overriddenProperties": null,
    "parentObjectId": "4a6e639a-9057-47d3-904c-c6ce5224f566",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "0ea93cc1-617d-462e-b090-d305a68082dc",
    "visible": true
}