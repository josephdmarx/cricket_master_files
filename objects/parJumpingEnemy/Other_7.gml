/// @description switch state when the current animation ends

switch (State)
{
	case eStateIDLE:
		break;
		
	case eStateALERT:
		break;
		
	case eStateWANDER:
		State = eStateIDLE;
		break;
		
	case eStateATTACK:
		State = eStateALERT;
		break;
	
	case eStateDEATH:
		break;
		
	case eStateHURT:
		injured = false;
		State = eStateIDLE;
		break;
		
	case eStateWINDUP:
		State = eStateJUMP;
		break;
		
	case eStateJUMP:
		break;
		
	case eStateJUMPINBOI:
		if (grounded) 
			State = eStateIDLE;
		break;
}