{
    "id": "c823a4bb-7077-4d8f-b821-f069e0066914",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oPauseManager",
    "eventList": [
        {
            "id": "2b2d0044-206d-4914-9f2f-2968c12fe066",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c823a4bb-7077-4d8f-b821-f069e0066914"
        },
        {
            "id": "5cb60ce4-04ea-41be-938c-e7e955c64822",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c823a4bb-7077-4d8f-b821-f069e0066914"
        },
        {
            "id": "ca3fe3b4-c4d7-43b8-af5e-2ad8ac48b9b3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "c823a4bb-7077-4d8f-b821-f069e0066914"
        },
        {
            "id": "1061e118-1b8b-4eda-abce-285d41c0a48b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "c823a4bb-7077-4d8f-b821-f069e0066914"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}