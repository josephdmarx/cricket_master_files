/// @description switch state when the current animation ends

if (life <= 0)
{
	
}


switch (State)
{
	case eStateIDLE:
		break;
		
	case eStateALERT:
		TargetX = oPlayer.x - x;
		TargetY = oPlayer.y - y;
		break;
		
	case eStateWANDER:
		State = eStateIDLE;
		break;
		
	case eStateATTACK:
		canattack = true;
		State = eStateALERT;
		break;
	
	case eStateDEATH:
		if (stay)
		{
			instance_create_layer(x,y,"Enemies", stayframe);
			stayframe.image_xscale = sign(TargetX);
		}
		instance_destroy();
		break;
		
	case eStateHURT:
		injured = false;
		if (turret) State = eStateALERT
		else State = eStateIDLE;
		break;
		
	case eStateSTUN:
		State = eStateIDLE;
		break;
		
	case eStateWAKEUP:
		State = eStateALERT;
		break;
}