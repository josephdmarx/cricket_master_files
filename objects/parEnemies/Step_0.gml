if (stunned)
	{	
		if (stuntimer <= 0) 
		{
			whackable = true;
			stunned = false;
			instance_destroy(oStatusStun);
		}
		
		stuntimer --;
		State = eStateSTUN;
	}

if (injured)
	{
		State = eStateHURT;
	}

if (life <= 0) and (dying = false)
	{
		dying = true;
	
		_targetx = 0;
		_targety = 0;

		alert = false;
		hostile = false;
		
		instance_destroy(oStatusStun);
		DropItem(item, rareitem);
		DropGold(egold);
		image_index = 0;
		image_speed = 1;
		sprite_index = deathSprite;
		
		playDeath(deathSprite,x,y);
		
		audio_play_sound(DeathSound,1,0);
		
		instance_destroy();
				
		State = eStateDEATH;
		
}

script_execute(State);