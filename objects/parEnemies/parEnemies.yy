{
    "id": "4a6e639a-9057-47d3-904c-c6ce5224f566",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "parEnemies",
    "eventList": [
        {
            "id": "3b3e7b13-aed8-456d-9d57-74084c35283a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4a6e639a-9057-47d3-904c-c6ce5224f566"
        },
        {
            "id": "ac45f30e-f3b4-4481-a7b4-4c72bba5c9b1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "4a6e639a-9057-47d3-904c-c6ce5224f566"
        },
        {
            "id": "1be03fed-48e8-472f-a26f-236e7d12bf29",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "4a6e639a-9057-47d3-904c-c6ce5224f566"
        },
        {
            "id": "3f70909a-f2d9-48fb-8d98-30eafcf22d9a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "4a6e639a-9057-47d3-904c-c6ce5224f566"
        },
        {
            "id": "c0e73353-1229-46e8-9bca-5d566f287844",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "3269c2bb-a7cc-47cc-84a0-19141b893294",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "4a6e639a-9057-47d3-904c-c6ce5224f566"
        }
    ],
    "maskSpriteId": "0ea93cc1-617d-462e-b090-d305a68082dc",
    "overriddenProperties": null,
    "parentObjectId": "e41eb36a-da0c-468f-82ad-8d63c346e7e9",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "0ea93cc1-617d-462e-b090-d305a68082dc",
    "visible": true
}