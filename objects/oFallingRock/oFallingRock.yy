{
    "id": "1f559761-ffb3-441d-bc10-a7222598640a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oFallingRock",
    "eventList": [
        {
            "id": "0680e642-d4fc-4c15-a549-ee862d9c9eb6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1f559761-ffb3-441d-bc10-a7222598640a"
        },
        {
            "id": "3336576c-4b9e-40b7-a39e-b08b09b5508c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "1f559761-ffb3-441d-bc10-a7222598640a"
        },
        {
            "id": "4283cea1-6308-42ef-aabe-3c158c483c57",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "1f559761-ffb3-441d-bc10-a7222598640a"
        }
    ],
    "maskSpriteId": "0ea93cc1-617d-462e-b090-d305a68082dc",
    "overriddenProperties": null,
    "parentObjectId": "3d68bb9b-56fd-4a7a-b31d-a29bfafe755b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "0ea93cc1-617d-462e-b090-d305a68082dc",
    "visible": true
}