/// @description Check whether the player is on the platform for  more than 1 second

event_inherited();

if (place_meeting(x,y-1,oPlayer))
{
	triggered = true;
}

if (triggered)
{
	count ++;
}

if (count > (room_speed * 1))
{
	falling = true;
}

if (falling)
{
	vsp = vsp + 0.4;
}

y += vsp * spd;