{
    "id": "96b2439d-813b-4553-8172-16d3a30b4f51",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oFallingPlatform",
    "eventList": [
        {
            "id": "73e1a9d8-72f6-44a9-874e-49d5f378a80b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "96b2439d-813b-4553-8172-16d3a30b4f51"
        },
        {
            "id": "b69ff829-cdc6-4ed3-9735-bdc887b4b5f4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "96b2439d-813b-4553-8172-16d3a30b4f51"
        },
        {
            "id": "072acfbf-119b-4384-b5ac-59821c8c2484",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 40,
            "eventtype": 7,
            "m_owner": "96b2439d-813b-4553-8172-16d3a30b4f51"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "1e5b4d39-1484-49d0-97bb-d4436467508a",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f3f9d9d7-1e6f-4379-922a-9f1737423724",
    "visible": true
}