{
    "id": "3d7c6484-8033-41e4-a2fb-02343f99d0bf",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oTextBox",
    "eventList": [
        {
            "id": "7f8296e3-b2ac-47c6-88b2-58f6fef94b85",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3d7c6484-8033-41e4-a2fb-02343f99d0bf"
        },
        {
            "id": "b5bfef9c-7427-43e1-9bab-12dafc431f3b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "3d7c6484-8033-41e4-a2fb-02343f99d0bf"
        },
        {
            "id": "dbf8fbdf-f86a-4793-a937-5a15001c099f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "3d7c6484-8033-41e4-a2fb-02343f99d0bf"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}