/// @description Insert description here
// You can write your code in this editor

#region room music declarations

if (room == rmMainMenu)			roomSound = BGM00;
if (room == rmChapter)			roomSound = noone;
if (room == rmMeadowLands00)	roomSound = sfx_amb_crickets;
if (room == rmMeadowLands01)	roomSound = BGM01;
if (room == rmMeadowLands02)	roomSound = BGM01;
if (room == rmMeadowLands03)	roomSound = BGM01;
if (room == rmEdanaHouse)		roomSound = BGM01;
if (room == rmCricketHouse)		roomSound = BGM01;
if (room == rmMeadowTown01)		roomSound = BGMMeadowTown;
if (room == rmBogCaves01)		roomSound = BGMSwampCaves;
if (room == rmBogCaves02)		roomSound = BGMSwampCaves;

#endregion

if (!playing)
{
	current_song = roomSound;
	location = room;
	playing = true;
	audio_play_sound(roomSound,1,true);
}


if (room != location) and (current_song != roomSound)
{
	audio_stop_all();
	playing = false;
}
	
