{
    "id": "f05d21d6-9dcc-482f-9ac9-afa3146d1c28",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oCureFeverPU",
    "eventList": [
        {
            "id": "93caf8cb-87ed-4f22-8c2f-73c0067f28fd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f05d21d6-9dcc-482f-9ac9-afa3146d1c28"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": [
        {
            "id": "a8529a3c-97eb-4967-9820-d4ee5f32c5be",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "6c37065d-eb91-4300-a809-5b569349aa0c",
            "propertyId": "c4210375-1472-4e6a-bd60-cb042778825e",
            "value": "itemtype.curefever"
        }
    ],
    "parentObjectId": "6c37065d-eb91-4300-a809-5b569349aa0c",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f69891e2-a677-4345-a3f6-3b8d406d1795",
    "visible": true
}