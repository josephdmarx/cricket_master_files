/// @description Insert description here
// You can write your code in this editor
interactable = false;
on = false;

name = "Mayor";
portrait = sPortraitMayor;

text[0] = "This person has nothing to say.";
speakers[0] = id;

IdleSprite = sMayorIdle;
MoveSprite = sMayorRun;
JumpSprite = sHurleyJump;

grounded = false;
moving = false;
vsp = 0;