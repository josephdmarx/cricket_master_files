{
    "id": "10bdb088-bb60-40db-a9e0-29d6ad8545d5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "parCoin",
    "eventList": [
        {
            "id": "86098dd8-9931-439e-8fc0-710977489955",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "5ab0bdeb-238d-489a-9844-fbab736cb673",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "10bdb088-bb60-40db-a9e0-29d6ad8545d5"
        },
        {
            "id": "44cbb4df-6813-436f-bbc6-292eca1142c7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "10bdb088-bb60-40db-a9e0-29d6ad8545d5"
        },
        {
            "id": "e984dfb2-c9d4-4285-b406-5b730526ba4e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "10bdb088-bb60-40db-a9e0-29d6ad8545d5"
        }
    ],
    "maskSpriteId": "868428b9-3b74-491f-87cf-87497f119190",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "6894192f-431e-4ced-b0a4-b7f0b55aa068",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "myItemType",
            "varType": 1
        }
    ],
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}