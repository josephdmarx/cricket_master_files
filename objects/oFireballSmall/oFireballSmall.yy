{
    "id": "11bcfd02-cabb-4310-a4b8-f10e88993e2a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oFireballSmall",
    "eventList": [
        {
            "id": "a6ea032c-0a25-41b5-8549-a7c390da031c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "11bcfd02-cabb-4310-a4b8-f10e88993e2a"
        },
        {
            "id": "7ffd6702-d0de-4dc5-9798-d3ba3c22cb86",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "11bcfd02-cabb-4310-a4b8-f10e88993e2a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "3269c2bb-a7cc-47cc-84a0-19141b893294",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "5ddbc78f-d84c-4817-8fef-fe14ac1a4335",
    "visible": true
}