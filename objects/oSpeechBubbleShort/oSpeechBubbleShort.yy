{
    "id": "dd23747a-1a90-4912-bb60-fbe79e50f29f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oSpeechBubbleShort",
    "eventList": [
        {
            "id": "2401027f-b9f8-4190-ba09-9798ecfc3d25",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "dd23747a-1a90-4912-bb60-fbe79e50f29f"
        },
        {
            "id": "221e9b85-1e41-4a37-9a7e-bfb7d2691935",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "dd23747a-1a90-4912-bb60-fbe79e50f29f"
        },
        {
            "id": "4bc4e257-62d1-4e7a-9fe5-968ed7eef4d7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "dd23747a-1a90-4912-bb60-fbe79e50f29f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f22286bb-4167-4d08-a2a5-b8152238303a",
    "visible": true
}