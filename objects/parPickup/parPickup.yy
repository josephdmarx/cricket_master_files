{
    "id": "6c37065d-eb91-4300-a809-5b569349aa0c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "parPickup",
    "eventList": [
        {
            "id": "8a0b3ade-ee2d-4afd-95c5-106650e1b81e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "5ab0bdeb-238d-489a-9844-fbab736cb673",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "6c37065d-eb91-4300-a809-5b569349aa0c"
        },
        {
            "id": "5aefd2f8-2eba-4587-99a3-648b0ad7b062",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "6c37065d-eb91-4300-a809-5b569349aa0c"
        },
        {
            "id": "060ed2c0-c719-4c21-b417-3618254a5f01",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6c37065d-eb91-4300-a809-5b569349aa0c"
        }
    ],
    "maskSpriteId": "868428b9-3b74-491f-87cf-87497f119190",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "c4210375-1472-4e6a-bd60-cb042778825e",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "myItemType",
            "varType": 1
        }
    ],
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}