{
    "id": "c278b09d-8233-4c75-acd1-522d59b606ed",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oFireBook",
    "eventList": [
        {
            "id": "38f8798b-8ef6-4640-b6ff-2d7997580ec9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c278b09d-8233-4c75-acd1-522d59b606ed"
        },
        {
            "id": "5b3650f5-a482-4b06-84bd-de3471c19d34",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c278b09d-8233-4c75-acd1-522d59b606ed"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "e08abc38-3d58-492f-b98a-a71aff27b009",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "87fc7cb7-818d-4ecd-a3cf-5fe27e5498a2",
    "visible": true
}