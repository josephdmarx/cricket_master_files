{
    "id": "8fe74693-4a6d-4f96-86e1-5d571344d3d3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "transitionButtonPress",
    "eventList": [
        {
            "id": "a5330788-ddcf-49fa-b9c9-f8bfba66ae96",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "5ab0bdeb-238d-489a-9844-fbab736cb673",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "8fe74693-4a6d-4f96-86e1-5d571344d3d3"
        },
        {
            "id": "1881b1f3-6497-4117-80bc-3fa543072ce2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8fe74693-4a6d-4f96-86e1-5d571344d3d3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "98c8dcd6-5f6a-49fe-906f-2de7aba5aa9b",
    "visible": false
}