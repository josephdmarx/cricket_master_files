{
    "id": "de79eee9-c178-4b49-8f52-dd9c76bfb9ba",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oMagicBlast01",
    "eventList": [
        {
            "id": "74994003-e34a-42ae-9bdd-44000e791aa2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "de79eee9-c178-4b49-8f52-dd9c76bfb9ba"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "9c76e071-3e57-4abb-9ddb-ea47badc0a73",
    "visible": true
}