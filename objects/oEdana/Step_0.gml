/// @description Insert description here
// You can write your code in this editor

if (instance_exists(oQuestManager))
{
	if (oQuestManager.ds_quests[# 1, QUEST.go_home] == 1)
	{
		sprite_index = sEdanaInjured;
	}
}


#region Facing Direction


if (instance_exists(oPlayer))
{
	TargetX = oPlayer.x - x;  
	TargetY = oPlayer.y - y;	
}

	if (TargetX > 0)
		image_xscale = -1
	
	if (TargetX < 0)
		image_xscale = 1

#endregion

#region Bubble Popup

if (interactable = true) and (instance_exists(oPlayer))
{
	if (point_in_circle(oPlayer.x,oPlayer.y,x,y,64)) and (on = false)
	{
		on = true; 
		with (instance_create_layer(x + 15, y - 35, "Effects", oSpeechBubbleShort))
		{
			text = "  . . .";	
		}
	}

	if !(point_in_circle(oPlayer.x,oPlayer.y,x,y,64)) and (on = true)
	{
		on = false;
		instance_destroy(oSpeechBubbleShort);
	}
}

#endregion

#region Quest Dialogue and Code
if (instance_exists(oQuestManager))
{
	if (oQuestManager.ds_quests[# 1, QUEST.collect_flowers ] == 1) 
	
		{
			x = oEdanaLoc02.x;
			y = oEdanaLoc02.y;
			text = ["You brought the flowers?", "Yep! I have all three of them, just like you asked.", "Good. Put them in the Cauldron."];
			speakers = [id, oPlayer, id];
		}

	if (oQuestManager.ds_quests[# 1, QUEST.frog_collecting ] == 0) 
	
		{
			x = oEdanaLoc02.x;
			y = oEdanaLoc02.y;
			text = ["You can find some frogs near the pond to the east.", "Run along now."];
			speakers = [id, id];
		}
	
	if (oQuestManager.ds_quests[# 1, QUEST.frog_collecting ] == 1) 
	
		{
			x = oEdanaLoc02.x;
			y = oEdanaLoc02.y;
			text = ["You have the frog? Put it in the box behind you please."];
			speakers = [id];
		}	

	if (oQuestManager.ds_quests[# 1, QUEST.collect_apples ] == 0) 
	
		{
			x = oEdanaLoc02.x;
			y = oEdanaLoc02.y;
			text = ["Just give that tree a good whack.", "Off with you!"];
			speakers = [id, id];
		}
	
	if (oQuestManager.ds_quests[# 1, QUEST.collect_apples ] == 1) 
	
		{
			x = oEdanaLoc02.x;
			y = oEdanaLoc02.y;
			text = ["Keep the apples. You might need a snack on the way!"];
			speakers = [id];
		}
}


#endregion