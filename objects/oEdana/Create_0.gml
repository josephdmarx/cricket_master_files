/// @description Insert description here
// You can write your code in this editor
interactable = true;
on = false;

name = "Edana"
portrait = sEdanaPort01;

text = ["Cricket, don't you have some chores to do?"];
speakers = [id];

IdleSprite = sEdanaIdle;
MoveSprite = sEdanaWalk;
JumpSprite = sHurleyJump;


grounded = false;
moving = false;
vsp = 0;
TargetX = 1;
TargetY = 0;