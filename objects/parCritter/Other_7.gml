/// @description switch state when the current animation ends

switch (State)
{
	case eStateIDLE:
		break;
		
	case eStateALERT:
		if !(flying)
			State = eStateIDLE;
			
		break;
		
	case eStateWANDER:
		State = eStateIDLE;
		break;		
}