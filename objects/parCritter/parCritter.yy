{
    "id": "c397ffe4-21bb-4149-a4d9-9d44c5be99c0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "parCritter",
    "eventList": [
        {
            "id": "c2b7947f-ed06-47ad-a2ed-701170ccc28a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c397ffe4-21bb-4149-a4d9-9d44c5be99c0"
        },
        {
            "id": "0e8889b4-ed22-4b82-90e9-18b0ab041d4e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c397ffe4-21bb-4149-a4d9-9d44c5be99c0"
        },
        {
            "id": "559bbb57-af8a-45c1-bb02-4ae672e9b8f1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "c397ffe4-21bb-4149-a4d9-9d44c5be99c0"
        },
        {
            "id": "689e5b68-3d2a-485a-9c96-5c8b53e9921f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "c397ffe4-21bb-4149-a4d9-9d44c5be99c0"
        }
    ],
    "maskSpriteId": "0ea93cc1-617d-462e-b090-d305a68082dc",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "0ea93cc1-617d-462e-b090-d305a68082dc",
    "visible": true
}