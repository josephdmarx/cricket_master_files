/// @description Draw Inventory

if (menu_active)
{

	DrawStartMenu();
	drawMenuButtons();

	var slot = 0;  //each slot has it's own position 0-7
	var _selectorrow = 0; //row position for the selector

	draw_sprite(sInventoryLarge, -1, guiHolderPosX, guiHolderPosY)

	#region Item description

		var _itemtype = inventory[activeslot];
		var _details = oInventoryManager.itemDefinitions[_itemtype, itemproperties.details];
		var _name = oInventoryManager.itemDefinitions[_itemtype, itemproperties.name];
		
		draw_text_outlined(guiHolderPosX + (guiHolderPad * 2), 290,_name);
		
		draw_set_halign(fa_left);
		draw_set_color(c_white);
		draw_set_font(font04);
		draw_text_transformed((guiHolderPosX + (guiHolderPad * 2)),305,_details,.5,.5,0);

	#endregion


	#region Draw Inventory

	if (inventory_row == 0)
	{
		draw_sprite(sSelector, -1, 
				(guiHolderPosX + guiHolderPad) + (guiHolderOffsetX * activeslot), 
				(guiHolderPosY + guiHolderPad));
	}

	else if (inventory_row > 0)
	{
		draw_sprite(sSelector, -1, 
				(guiHolderPosX + guiHolderPad) + (guiHolderOffsetX * activeslot) - (guiHolderOffsetX * (inventory_row * 12)), 
				(guiHolderPosY + guiHolderPad) + (guiHolderOffsetY * inventory_row));
	}



	for (var i = 0; i < MAX_ACTIVE_INV_ITEMS; i++)
	{
		itemDefIndex = inventory[i];

		if (i <= 12)
			{
				slot = 0;
			}
		else if (i > 12)
			{
				slot = 1;
			}

		if (itemDefIndex != itemtype.none) and (inventory_row == 0)
			{
				image_speed = 0;
				image_index = 0;
				draw_sprite(itemDefinitions[itemDefIndex, itemproperties.sprite], -1, 
							(guiHolderPosX + guiHolderPad) + (guiHolderOffsetX * i),
							(guiHolderPosY + guiHolderPad));
			
				draw_text_transformed((guiHolderPosX + guiHolderPad) + (guiHolderOffsetX * i),
						(guiHolderPosY + guiHolderPad),
						string(itemDefinitions[itemDefIndex, itemproperties.amount]),.5,.5,0);
			
			}
		else if (itemDefIndex != itemtype.none) and (inventory_row > 0 )
			{
				image_speed = 0;
				image_index = 0;
				draw_sprite(itemDefinitions[itemDefIndex, itemproperties.sprite], -1, 
						(guiHolderPosX + guiHolderPad) + (guiHolderOffsetX * i),
						(guiHolderPosY + guiHolderPad));		
			
				draw_text_transformed((guiHolderPosX + guiHolderPad) + (guiHolderOffsetX * i),
						(guiHolderPosY + guiHolderPad),
						string(itemDefinitions[itemDefIndex, itemproperties.amount]),.5,.5,0);
		
		
			}
		
	}
	
	#endregion
	
	
	
	
}