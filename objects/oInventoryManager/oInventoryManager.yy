{
    "id": "257720c3-5ff8-4c0a-82c6-61d4a5dca943",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oInventoryManager",
    "eventList": [
        {
            "id": "c0789808-adce-47d4-8f10-a443af1b4bc6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "257720c3-5ff8-4c0a-82c6-61d4a5dca943"
        },
        {
            "id": "fc9be1c4-ea85-4b49-9725-22002e79064e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "257720c3-5ff8-4c0a-82c6-61d4a5dca943"
        },
        {
            "id": "ffbcaa64-7fc4-4511-8a27-a8f6aa40d0d0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "257720c3-5ff8-4c0a-82c6-61d4a5dca943"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "432d7032-d117-44ed-910f-1cb4d16b538f",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}