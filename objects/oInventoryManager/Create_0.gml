/// @description Initialize Inventory

depth = -50;
scale = 2;

show_inventory = false;

inventory_row = 0;

menu_active = false;
guiHolderWidth = sprite_get_width(sInventoryLarge);
guiHolderPosX = 130;
guiHolderPosY = 90;
guiHolderPad = 5;

guiHolderOffsetX = 31 + guiHolderPad;
guiHolderOffsetY = 31 + guiHolderPad;

activeslot = 0; //the slot that we currently have selected with our selector sprite

InvInitialize();
