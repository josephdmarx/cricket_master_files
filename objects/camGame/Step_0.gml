if (global.paused) exit;


if (instance_exists(follow))  //changes the camera destination to the follow the object
{
	xTo = follow.x;
	yTo = follow.y;
}

//Update object position
x += (xTo - x) / camspd;          //calculates the different between the current position and the destination position, and divides it for smooth movement.
y += (yTo - y) / camspd;


//Update camera view
camera_set_view_pos(cam,x-view_w_half,y-view_h_half);  //sets the position so the camera will center on the target based on half the view distance


x = clamp(x,view_w_half+50,room_width-view_w_half-50);

y = clamp(y,view_h_half,room_height-view_h_half);



if (layer_exists("Meadow"))
{
		layer_x("Meadow",x/4);
		//layer_y("Meadow",y/4);
}

if (layer_exists("NearTrees"))
{
		layer_x("NearTrees",x/3);
		//layer_y("NearTrees",y/3);
}


if (layer_exists("FarTrees"))
{
		layer_x("FarTrees",x/1.8);
		//layer_y("FarTrees",y/1.8);
}


if (layer_exists("Background"))
{
		layer_x("Background",x/1.2);
		//layer_y("Background",y/1.2);
}



