/// @description Setup Cam

display_set_gui_size(640,360);

//Set the window size
//var _windowW = 1280;
//var _windowH = 720;

//window_set_size (_windowW, _windowH);

////center the window
//window_set_position(display_get_gui_width()/2, display_get_gui_height()/2);
					
// OLD CODE ABOVE

if (instance_exists(oPlayer))
{
	xstart = oPlayer.x;
	ystart = oPlayer.y;
	follow = oPlayer;
}
else 
{
	follow = oCamTarget01;
}



x = follow.x;
y = follow.y;

// NEW CODE BELOW


cam = view_camera[0];

view_w_half = camera_get_view_width(cam) * 0.5;
view_h_half = camera_get_view_height(cam) * 0.5;

xTo = xstart;  //camera destination is set to the starting position of the camera object
yTo = ystart;

camspd = 25;

