{
    "id": "0a421eb8-886b-424a-ae6b-ec1fecfba936",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oGoldManager",
    "eventList": [
        {
            "id": "63113efe-6d99-434a-b591-9371d3de63cf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0a421eb8-886b-424a-ae6b-ec1fecfba936"
        },
        {
            "id": "27f1fe67-73de-45d2-a0f8-b451f5135ccd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "0a421eb8-886b-424a-ae6b-ec1fecfba936"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "432d7032-d117-44ed-910f-1cb4d16b538f",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}