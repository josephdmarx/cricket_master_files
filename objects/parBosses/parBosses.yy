{
    "id": "ce4cdaa5-0c4a-4101-8fbf-d0543c31e9dc",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "parBosses",
    "eventList": [
        {
            "id": "ad26a4ad-aa84-4e92-b78c-85c8c01c5b28",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ce4cdaa5-0c4a-4101-8fbf-d0543c31e9dc"
        },
        {
            "id": "fdae6a8f-839a-40c6-9fdd-6272ced37b0b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ce4cdaa5-0c4a-4101-8fbf-d0543c31e9dc"
        },
        {
            "id": "90b6e7a4-f7f2-4a90-a370-33665f6a3ca8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "ce4cdaa5-0c4a-4101-8fbf-d0543c31e9dc"
        },
        {
            "id": "41fa2b40-9c87-48ea-8692-22a3807c1808",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "ce4cdaa5-0c4a-4101-8fbf-d0543c31e9dc"
        },
        {
            "id": "85bdc155-6320-45dd-97b5-3c336648424e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "3269c2bb-a7cc-47cc-84a0-19141b893294",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "ce4cdaa5-0c4a-4101-8fbf-d0543c31e9dc"
        }
    ],
    "maskSpriteId": "0ea93cc1-617d-462e-b090-d305a68082dc",
    "overriddenProperties": null,
    "parentObjectId": "e41eb36a-da0c-468f-82ad-8d63c346e7e9",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "0ea93cc1-617d-462e-b090-d305a68082dc",
    "visible": true
}