/// @description switch state when the current animation ends

if (life <= 0)
{
	
}


switch (State)
{
	case BossStateIDLE:
		break;
		
	case BossStateALERT:
	
		break;

	case BossStateATTACK:
		canattack = true;
		State = BossStateALERT;
		break;
	
	case eStateDEATH:
		if (stay)
		{
			instance_create_layer(x,y,"Enemies", stayframe);
			stayframe.image_xscale = sign(TargetX);
		}
		instance_destroy();
		break;
		
	case BossStateHURT:
		injured = false;
		State = BossStateIDLE;
		break;
		
	case BossStateWAKEUP:
		State = BossStateALERT;
		break;
		
	case AttackStates:
		State = BossStateALERT;
		break;
}