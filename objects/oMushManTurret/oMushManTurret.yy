{
    "id": "82885536-b0c4-4b86-95d2-9243b6969267",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oMushManTurret",
    "eventList": [
        {
            "id": "3f3112f9-b044-4408-b3bb-68f57861de14",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "82885536-b0c4-4b86-95d2-9243b6969267"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4a6e639a-9057-47d3-904c-c6ce5224f566",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d06561d3-3f59-491e-9c58-22ec7940847e",
    "visible": true
}