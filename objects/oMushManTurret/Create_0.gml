spd = 0;
vsp = 0;
grv = 0;
def = 0;
life = 4;
flash = 0;
death = sImpD;
dying = false;
item = oManaGem;
rareitem = oApplePU;
immune = false;
atkpower = 2;
knockbackforce = 5;
hostile = false;
injured = false;
range = 150;
attackrange = 150;
alert = false;
DeathSound = noone;
MoveSprite = sMushman_Idle;
IdleSprite = sMushman_Idle;
AttackSprite = sMushman_Attack;
myItemType = itemtype.none;
grounded = false;
flying = false;
flyaway = false;
wander = false;
wandertimer = 0;
alertimer = (room_speed * 2)
hop = false;
jumper = false;
hider = true;
turret = true;
wake_sprite = sMushman_Wake;
hide_sprite = sMushman_Wiggle;
projectile = oMushmanSpit;
canattack = true;
egold = 0;
stunned = false;

//turret only variables
shootframe = 3;

sound_wakeup = sfxJump01;
DeathSound = sfx_impDeath;
deathSprite = sImpD;
hurtSprite = sMushman_Idle;
stunSprite = sMushman_Idle;
stay = false;

attacktype = NOTHING;

State = eStateIDLE;