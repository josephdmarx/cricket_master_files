{
    "id": "6cf7ef50-0bf5-4d0d-b787-3015cfd61334",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oSpit01D",
    "eventList": [
        {
            "id": "75dda1b1-d4d7-41f4-a2f1-d0f5a65def6f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6cf7ef50-0bf5-4d0d-b787-3015cfd61334"
        },
        {
            "id": "df4d04ad-b2c9-40f8-812b-9744c22e239f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "6cf7ef50-0bf5-4d0d-b787-3015cfd61334"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6da10993-05d0-42e3-a2d6-aa1e2d197fd3",
    "visible": true
}