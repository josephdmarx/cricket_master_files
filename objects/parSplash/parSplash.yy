{
    "id": "5fdcb410-e0eb-407c-abca-81ed7ec23711",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "parSplash",
    "eventList": [
        {
            "id": "ad1429a9-03f5-40b2-8913-eec6bf9ac92d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "5fdcb410-e0eb-407c-abca-81ed7ec23711"
        },
        {
            "id": "847a0426-b87a-4746-9775-ad27ec883adc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5fdcb410-e0eb-407c-abca-81ed7ec23711"
        },
        {
            "id": "3328963d-f198-4d7c-a023-5d59a87c7b8d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "5fdcb410-e0eb-407c-abca-81ed7ec23711"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c3ce15a6-9a5e-4f74-a8c7-1f83776a178a",
    "visible": true
}