{
    "id": "4bb11b38-afab-4b92-943b-44db64c1db94",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oBreakable",
    "eventList": [
        {
            "id": "c9e771fa-0437-443d-a1c4-8563a5b9a4a9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4bb11b38-afab-4b92-943b-44db64c1db94"
        },
        {
            "id": "33bf52d9-3849-419a-8fb6-8f742899ef8c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "4bb11b38-afab-4b92-943b-44db64c1db94"
        }
    ],
    "maskSpriteId": "0ea93cc1-617d-462e-b090-d305a68082dc",
    "overriddenProperties": null,
    "parentObjectId": "1e5b4d39-1484-49d0-97bb-d4436467508a",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "599df1e9-9f93-43ed-9391-349e3d0d5dcf",
    "visible": true
}