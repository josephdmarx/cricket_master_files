{
    "id": "fd8c4a34-9887-4e11-be91-a744d8aab67d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oSkullDemon",
    "eventList": [
        {
            "id": "3d72f4d0-53de-4316-b929-dcced4233aee",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "fd8c4a34-9887-4e11-be91-a744d8aab67d"
        },
        {
            "id": "53b9ce1d-aef7-49bf-9120-cf2c49f4574c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "fd8c4a34-9887-4e11-be91-a744d8aab67d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "e18ccc66-7d9e-4c8e-a37d-069344f8ea58",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "caa31944-0ea7-469b-8ec4-5ff707e7f7ec",
    "visible": true
}