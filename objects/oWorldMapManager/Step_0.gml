if !(menu_active) exit;

else if (menu_active)
{
	GetMenuInput();
	
	if (key_back)
	{
		audio_play_sound(sfxMenuCancel,1,0);
		
		menu_active = false;
		
		if (atSignPost)
		{
			with (oStartMenuManager)
			{			
				menu_active = false;
				key_back = false;
				oPlayer.hascontrol = true;
				atSignPost = false;
			}
		}
		
		else if !(atSignPost)
		{
			with (oStartMenuManager)
			{			
				menu_active = true;
				key_back = false;
			}
		}
		
		
	}
	
	if (key_right)
	{
		audio_play_sound(sfxMenuClick,1,0);
		
		if (meadowtown_active)
		{
			if (active_location < locationsUnlocked) active_location ++;
		}
		
	}


	if (key_left)
	{
		audio_play_sound(sfxMenuClick,1,0);
		if (active_location > 0) active_location --;
	}


	if (key_accept) and (atSignPost)
	{
		audio_play_sound(sfxMenuAccept,1,0);
		if (active_location = MEADOWLANDS) and (oPlayer.location != MEADOWLANDS)   //if the cursor is on Meadowlands and the player is not IN the meadowlands
		{
			if (oQuestManager.ds_quests[# 1, QUEST.go_home] != -1)
			{
				menu_active = false;
				oStartMenuManager.MasterMenuOpen = false;
				atSignPost = false;
			
				with(oPlayer)
				{	
					image_xscale = -1;
				
			
					SlideTransition(TRANS_MODE.GOTO, rmMeadowLands03Night, 5072, 288);			//take us there
				
				}
				
				
			}
			
			
			else 
			{
				menu_active = false;
				oStartMenuManager.MasterMenuOpen = false;
				atSignPost = false;
			
				with(oPlayer)
				{	
					image_xscale = -1;
				
			
					SlideTransition(TRANS_MODE.GOTO, rmMeadowLands01, 2496, 272);			//take us there
				
				}
			}
		}
		else if (active_location = MEADOWTOWN) and (oPlayer.location != MEADOWTOWN)
		{
			
			menu_active = false;
			oStartMenuManager.MasterMenuOpen = false;
			atSignPost = false;
			
			with(oPlayer)
			{
				image_xscale = 1;
				
				
				SlideTransition(TRANS_MODE.GOTO, rmMeadowTown01, 65, 272);
			}
		}
	
		else if (active_location = BOGCAVES) and (oPlayer.location != BOGCAVES)
		{
			
			
			menu_active = false;
			oStartMenuManager.MasterMenuOpen = false;
			atSignPost = false;
			
			with(oPlayer)
			{	
				image_xscale = 1;
								
				SlideTransition(TRANS_MODE.GOTO, rmBogCaves01, 65, 272);
				//SlideTransition(TRANS_MODE.GOTO, rmBogCaves02, 1070, 3009);
			}
		}
		
	}
}