
if (menu_active)
{

	DrawStartMenu();
	drawMenuButtons();

	var slot = 0;  //each slot has it's own position 0-7
	var _selectorrow = 0; //row position for the selector

	draw_sprite(sInventoryLarge, -1, guiHolderPosX, guiHolderPosY)

	#region Item description

		var _eqtype = magic[activemagic];
		var _details = oMagicManager.magicDefinitions[_eqtype, MAGICPROPERTIES.DESCRIPTION];
		var _name = oMagicManager.magicDefinitions[_eqtype, MAGICPROPERTIES.NAME];
		
		draw_text_outlined(guiHolderPosX + (guiHolderPad * 2), 290, _name);
		
		draw_set_halign(fa_left);
		draw_set_color(c_white);
		draw_set_font(font04);
		draw_text_transformed((guiHolderPosX + (guiHolderPad * 2)),305,_details,.5,.5,0);

	#endregion

	//#region Draw the Equipped Weapon

	//draw_sprite(sEqFrame, -1, 470, 280);  //draws the frame
	//draw_text_outlined(520, 300, magicDefinitions[magic[equippedmagic], MAGICPROPERTIES.NAME]);
	//draw_sprite(magicDefinitions[magic[equippedmagic], MAGICPROPERTIES.SPRITE], -1, 474, 284);
	

	//#endregion

		
	if (inventory_row == 0)
	{
		draw_sprite(sSelector, -1, 
				(guiHolderPosX + guiHolderPad) + (guiHolderOffsetX * activeslot), 
				(guiHolderPosY + guiHolderPad));
	}

	else if (inventory_row > 0)
	{
		draw_sprite(sSelector, -1, 
				(guiHolderPosX + guiHolderPad) + (guiHolderOffsetX * activeslot) - (guiHolderOffsetX * (inventory_row * 12)), 
				(guiHolderPosY + guiHolderPad) + (guiHolderOffsetY * inventory_row));
	}

	
	for (var i = 0; i < MAX_ACTIVE_MAGIC_ITEMS; i++)
	{
		magDefIndex = magic[i];

		if (i <= 12)
			{
				slot = 0;
			}
		else if (i > 12)
			{
				slot = 1;
			}

		if (magDefIndex != MAGICTYPE.NONE) and (inventory_row == 0)
			{
				draw_sprite(magicDefinitions[magDefIndex, MAGICPROPERTIES.SPRITE], -1, 
							(guiHolderPosX + guiHolderPad) + (guiHolderOffsetX * i),
							(guiHolderPosY + guiHolderPad));
			}
			
		else if (magDefIndex != MAGICTYPE.NONE) and (inventory_row > 0 )
			{
				draw_sprite(magicDefinitions[magDefIndex, MAGICPROPERTIES.SPRITE], -1, 
						(guiHolderPosX + guiHolderPad) + (guiHolderOffsetX * i),
						(guiHolderPosY + guiHolderPad));		
		
		
			}

	}
}



	draw_sprite(sEqFrame, -1, 170, 10);
	draw_sprite(magicDefinitions[magic[equippedmagic], MAGICPROPERTIES.SPRITE], -1, 172+2, 12+2);
	
	if (gamepad_is_connected(0))
	{
		draw_sprite(sBPad, 0, 210, 58);		
	}
	else if !(gamepad_is_connected(0))
	{
		draw_sprite(sJKey, 0, 210, 58);
	}
