var _itemtype = magic[activemagic];

if (activeslot <= 11)
	inventory_row = 0;

if (activeslot > 11)
	inventory_row = 1;
	
if (activeslot > 23)
	inventory_row = 2;
	
if (activeslot > 35)
	inventory_row = 3;
	
if (activeslot > 47)
	inventory_row = 4;


if (menu_active)
{
	
	GetMenuInput();
	
	if (key_right) and (activeslot != 59)
	{
		audio_play_sound(sfxMenuClick,1,0);
		activeslot++;
	}
	if (key_left) and (activeslot != 0)
	{
		audio_play_sound(sfxMenuClick,1,0);
		activeslot--;
	}
	if (key_down) and (activeslot != 59)
	{
		audio_play_sound(sfxMenuClick,1,0);
		if (activeslot < 47) activeslot += 12;
	}
	
	if (key_up) and (activeslot != 0)
	{
		audio_play_sound(sfxMenuClick,1,0);
		if (activeslot > 11) activeslot -= 12;		
	}

}
if (menu_active)	
{
	
	activemagic = activeslot;
	
	if (key_accept) //use item
	{
		audio_play_sound(sfxMenuAccept,1,0);
		
		equippedmagic = activemagic;
		
	}
	
	if (key_back)
	{
		audio_play_sound(sfxMenuCancel,1,0);
		
		menu_active = false;
		
		with (oStartMenuManager) menu_active = true;	
	}
}


