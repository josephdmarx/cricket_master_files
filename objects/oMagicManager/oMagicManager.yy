{
    "id": "a8419477-ef4c-4af2-a354-f567f3ca2d12",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oMagicManager",
    "eventList": [
        {
            "id": "9a085689-e92d-46ed-9d4b-ccea25420182",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a8419477-ef4c-4af2-a354-f567f3ca2d12"
        },
        {
            "id": "35052f75-be2c-4184-b571-4b1fdce32d34",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "a8419477-ef4c-4af2-a354-f567f3ca2d12"
        },
        {
            "id": "5250663a-4912-429d-a57e-523d88d00d00",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "a8419477-ef4c-4af2-a354-f567f3ca2d12"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "432d7032-d117-44ed-910f-1cb4d16b538f",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}