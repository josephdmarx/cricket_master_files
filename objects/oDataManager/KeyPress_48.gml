/// @desc Save

//Create a root list
var _root_list = ds_list_create();

with (parSaveMe)
{
	
	var _noOfChests = 0;
	
	var _map = ds_map_create();	
	ds_list_add(_root_list, _map);
	ds_list_mark_as_map(_root_list, ds_list_size(_root_list)-1);
	
	var _obj = object_get_name(object_index);
	
	
	ds_map_add(_map, "obj", _obj);  //add the object in the parSaveMe
		
	
	if (_obj == "oInstanceManager")
	{
		ds_map_add(_map, "no_of_chests", no_of_chests);
		
		for (var tt = 0; tt < no_of_chests; tt++)
		{
			ds_map_add(  _map, "chest"+string(tt), ids_of_opened_chests[tt]  );
		}
	}
	
	if (_obj == "oPlayer")
	{
		ds_map_add(_map, "y", y);
		ds_map_add(_map, "x", x);
		ds_map_add(_map, "room", room);
		ds_map_add(_map, "location", location);
		ds_map_add(_map, "cancast", cancast);
		ds_map_add(_map, "canpickup", canpickup);
		ds_map_add(_map, "layer", layer);
		ds_map_add(_map, "MQstage", MQstage);
		ds_map_add(_map, "TutorialStage", TutorialStage);
		ds_map_add(_map, "manamax", manamax);
		ds_map_add(_map, "mana", mana);
		ds_map_add(_map, "lifemax", lifemax);
		ds_map_add(_map, "life", life);
	}
	
	if (_obj == "oInventoryManager")
	{
		//ds_map_add(_map, "inventory", inventory);
		var _numberOfEntries = 0;
		
		for (var i = 0; i < MAX_ACTIVE_INV_ITEMS; i++)
		{
			if 	(inventory[i] != itemtype.none)
			{
				ds_map_add(_map, "inventory"+string(i)+"type", inventory[i]);
				ds_map_add(_map, "inventory"+string(i)+"amount", itemDefinitions[inventory[i], itemproperties.amount]);
				_numberOfEntries ++;
			}	
		}
		
		ds_map_add(_map, "numberOfEntries", _numberOfEntries);
	}
	
	if (_obj == "oQuestManager")
	{
		var _numberOfQuestEntries = 0;
		
		for (var j = 0; j < ds_quests_number; j++)
		{
				ds_map_add(_map, "ds_quest"+string(j), ds_quests[# 1, j]);
				_numberOfQuestEntries ++;	
		}
		
		ds_map_add(_map, "numberOfQuestEntries", _numberOfQuestEntries);
		
		ds_map_add(_map, "froginbox", froginbox);
	}
		
		
	if (_obj = "oEquipmentManager")
	{
		
		var _numberOfEquipmentEntries = 0;
		
		ds_map_add(_map, "equippedweapon", equippedweapon);
		ds_map_add(_map, "activeweapon", activeweapon);
		
		for (var p = 0; p < MAX_ACTIVE_EQ_ITEMS; p++)  //search all possible equipment entries
		{
			if 	(equipment[p] != WEAPONTYPE.NONE)  //store the type in each slot
			{
				ds_map_add(_map, "equipment"+string(p)+"type", equipment[p]);
				_numberOfEquipmentEntries ++;
			}	
		}
		
		ds_map_add(_map, "_numberOfEquipmentEntries", _numberOfEquipmentEntries);
	}
	
	if (_obj == "oProgressTracker")
	{
		ds_map_add(_map, "chapter", chapter);
	}
	
	if (_obj == "oGoldManager")
	{
		ds_map_add(_map, "gold", gold);
	}
		
	if (_obj == "oMagicManager")
	{
		var _numberOfMagicEntries = 0;
		
		ds_map_add(_map, "equippedmagic", equippedmagic);
		ds_map_add(_map, "activemagic", activemagic);
		
		for (var f = 0; f < MAX_ACTIVE_MAGIC_ITEMS; f++)  //search all possible equipment entries
		{
			if 	(magic[f] != MAGICTYPE.NONE)  //store the type in each slot
			{
				ds_map_add(_map, "magic"+string(f)+"type", magic[f]);
				_numberOfMagicEntries ++;
			}	
		}	
		
		ds_map_add(_map, "_numberOfMagicEntries", _numberOfMagicEntries);
	}
	
	if (_obj == "oRecipeManager")
	{
		
		var _numberOfRecipeEntries = 0;		
		
		for (var q = 0; q < MAX_ACTIVE_RECIPES; q++)  //search all possible equipment entries
		{
			if 	(recipe[q] != RECIPETYPE.NONE)  //store the type in each slot
			{
				ds_map_add(_map, "recipe"+string(q)+"type", recipe[q]);
				_numberOfRecipeEntries ++;
			}	
		}	
		
		ds_map_add(_map, "_numberOfRecipeEntries", _numberOfRecipeEntries);
	}
	
	
}

//Wrap the root LIST up in a MAP
var _wrapper = ds_map_create();
ds_map_add_list(_wrapper, "ROOT", _root_list);

//save all of this to a string
var _string = json_encode(_wrapper);
savestringtofile("savedgame.sav", _string);

//nuke the data
ds_map_destroy(_wrapper);

show_message("GAME SAVED!");