/// @desc Load Game
//with (oPlayer) instance_destroy();  //deletes are present player instance (this should be obsolete with save rooms


if (file_exists("savedgame.sav"))
{
	var _wrapper = loadJsonfromfile("savedgame.sav");
	var _list = _wrapper[? "ROOT"];
	
	for (var i = 0; i < ds_list_size(_list); i++)
	{
		var _map = _list[| i];
		
		var _obj = _map[? "obj"];
		
		
		if (_obj == "oInstanceManager")
		{
			with (oInstanceManager)
			{
				no_of_chests = _map[? "no_of_chests"];
				
				for (var tt = 0; tt < no_of_chests; tt++)
				{
					ids_of_opened_chests[tt] = _map[? "chest"+string(tt)];  //recreate the open chest array	
				}
								
			}
			
		}
	
	
		if (_obj == "oPlayer")
		{
			with (oPlayer)  //doesn't create a new player object but instead changes its values to be correct
			{
				y = _map[? "y"];
				x = _map[? "x"];
				layer = _map[? "layer"];
				room = _map[? "room"];
				cancast = _map[? "cancast"];
				canpickup = _map[? "canpickup"];
				MQstage = _map[? "MQstage"];
				TutorialStage = _map[? "TutorialStage"];
				manamax = _map[? "manamax"];
				mana = _map[? "mana"];
				lifemax = _map[? "lifemax"];
				life = _map[? "life"];
			}
		}
		
		if (_obj == "oInventoryManager")
		{
			with (oInventoryManager)
			{
				
				var _numberOfEntries = _map[? "numberOfEntries"];  //find how many slots were full
				
				inventory[_numberOfEntries] = 0;  //initialize all entries to none.
				
				
				for (var j = 0; j < _numberOfEntries; j++) //loop this for each filled slot
				{
					inventory[j] = _map[? "inventory"+string(j)+"type"];
					itemDefinitions[inventory[j], itemproperties.amount] = _map[? "inventory"+string(j)+"amount"];	
				}
			}
		}
		
		if (_obj == "oGoldManager")
		{
			with (oGoldManager)
			{
				gold = _map[? "gold"];	
			}
		}
		
		if (_obj == "oProgressTracker")
		{
			with (oProgressTracker)
			{
				chapter = _map[? "chapter"];	
			}
			
		}
		
		
		if (_obj == "oEquipmentManager")
		{
			with (oEquipmentManager)
			{
				equipment[MAX_ACTIVE_EQ_ITEMS] = 0;
				
				activeweapon = _map[? "activeweapon"];
				equippedweapon = _map[? "equippedweapon"];
				
				for (var p = 0; p < MAX_ACTIVE_EQ_ITEMS; p++)
				{
					equipment[p] = _map[? "equipment"+string(p)+"type"];
				}
			}
		}
		
		if (_obj == "oMagicManager")
		{
			with (oMagicManager)
			{
				magic[MAX_ACTIVE_MAGIC_ITEMS] = 0;
			
				activemagic = _map[? "activemagic"];
				equippedmagic = _map[? "equippedmagic"];
			
				for (var q = 0; q < MAX_ACTIVE_MAGIC_ITEMS; q++)
					{
						magic[q] = _map[? "magic"+string(q)+"type"];
					}
			}
		}
		
	
		if (_obj == "oRecipeManager")
		{
			with (oRecipeManager)
			{
				recipe[MAX_ACTIVE_RECIPES] = RECIPETYPE.NONE;
			
				for (var f = 0; f < MAX_ACTIVE_RECIPES; f++)
					{
						recipe[f] = _map[? "recipe"+string(f)+"type"];
					}
			}
		}
		
		
		
				
		if (_obj == "oQuestManager")
		{
			with (oQuestManager)
			{
				var _numberOfQuestEntries = _map[? "numberOfQuestEntries"];
				
				for (var kk = 0; kk < ds_quests_number; kk++)
				{
					ds_quests[# 1, kk] = _map[? "ds_quest"+string(kk)];
				}
				
				froginbox = _map[? "froginbox"];
			}
		}
		
		
			
	}
	
	
	ds_map_destroy(_wrapper);
	show_message("GAME LOADED!");
	
}