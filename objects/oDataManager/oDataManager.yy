{
    "id": "b159f3eb-1ca1-4102-ae6b-d82157e2807b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oDataManager",
    "eventList": [
        {
            "id": "5ec759e8-a2b5-4913-9c02-12093b5be5c7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 48,
            "eventtype": 9,
            "m_owner": "b159f3eb-1ca1-4102-ae6b-d82157e2807b"
        },
        {
            "id": "4a4d75d8-b45b-4bd2-80b0-5b8e47505928",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 55,
            "eventtype": 9,
            "m_owner": "b159f3eb-1ca1-4102-ae6b-d82157e2807b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}