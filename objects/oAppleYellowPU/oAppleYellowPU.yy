{
    "id": "89e4e153-f0b1-4197-86d7-bf42c69fc0fa",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oAppleYellowPU",
    "eventList": [
        {
            "id": "0900b701-db9d-4ad9-a9b0-f9a87ebd45b2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "89e4e153-f0b1-4197-86d7-bf42c69fc0fa"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": [
        {
            "id": "b3808880-a054-4d79-8e93-1221dc7009ca",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "6c37065d-eb91-4300-a809-5b569349aa0c",
            "propertyId": "c4210375-1472-4e6a-bd60-cb042778825e",
            "value": "itemtype.yellowapple"
        }
    ],
    "parentObjectId": "6c37065d-eb91-4300-a809-5b569349aa0c",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a4c6e877-f34d-4d74-85de-c1bacc8bf275",
    "visible": true
}