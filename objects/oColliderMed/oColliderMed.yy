{
    "id": "ec1169c6-c5eb-449c-9c5e-78ef46e55c21",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oColliderMed",
    "eventList": [
        
    ],
    "maskSpriteId": "240da789-6827-442b-8776-82c6c85d75a4",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "240da789-6827-442b-8776-82c6c85d75a4",
    "visible": true
}