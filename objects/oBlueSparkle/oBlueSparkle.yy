{
    "id": "78fe52d1-aed7-49d2-ad57-f556d89c35e8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oBlueSparkle",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "36be9821-624b-4ec6-b86e-9e38f02e885f",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8b149d3f-5513-4039-827f-8ce4ff4243d1",
    "visible": true
}