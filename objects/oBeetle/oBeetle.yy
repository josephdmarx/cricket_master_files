{
    "id": "143732f7-e139-445d-9cb6-5349bace3a21",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oBeetle",
    "eventList": [
        {
            "id": "ab8557d1-3cd4-4925-81bf-c202b26bc896",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "143732f7-e139-445d-9cb6-5349bace3a21"
        },
        {
            "id": "4fefbffc-28ba-4063-995a-2ba6a4366245",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "143732f7-e139-445d-9cb6-5349bace3a21"
        },
        {
            "id": "e287b7c1-f56a-4f95-8780-a1ba3e9af1cc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "143732f7-e139-445d-9cb6-5349bace3a21"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "5d4b5c14-2c31-4dff-9cc2-b8b8bc2d63dc",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ed4e48d1-6143-46ea-b847-cd3a27f528b1",
    "visible": true
}