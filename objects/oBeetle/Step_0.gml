/// @description Insert description here
// You can write your code in this editor
if (interactable = true)
{
	if (point_in_circle(oPlayer.x,oPlayer.y,x,y,64)) and (on = false)
	{
		on = true;
		
		with (instance_create_layer(x + 15, y - 35, "Effects", oSpeechBubbleShort))
		{
			text = "  . . .";	
		}		
		
		image_speed = 1;
		sprite_index = sBeetleLookup;
		State = NPCStateWake;
		
		//instance_create_layer(x+16,y-40,"GUI",oBubbleTail);
		//instance_create_layer(x+16,y-40,"GUI",oEKey);
	}
	
	

	else if !(point_in_circle(oPlayer.x,oPlayer.y,x,y,64)) and (on = true)
	{
		on = false;
		
		instance_destroy(oSpeechBubbleShort);
		
		image_speed = 1;
		sprite_index = sBeetleLookdown;
		State = NPCStateUnWake;

		//instance_destroy(oBubbleTail);
		//instance_destroy(oEKey);
	}
}


script_execute(State);