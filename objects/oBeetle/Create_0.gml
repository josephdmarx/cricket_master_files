/// @description Insert description here
// You can write your code in this editor
interactable = true;
on = false;

name = "Beetle"
portrait = sPortraitBeetle;

text = ["This person has nothing to say", "You should probably leave."]
speakers = [id, id];

IdleSprite = sBeetleIdle;
MoveSprite = sHurleyJump;
JumpSprite = sHurleyJump;

grounded = false;
moving = false;
vsp = 0;

image_speed = 0;
sprite_index = sBeetleLean;
State = NPCStateIdle;