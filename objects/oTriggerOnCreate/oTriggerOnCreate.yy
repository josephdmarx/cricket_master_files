{
    "id": "d941c42e-ec5d-4f25-8786-7d93207d7336",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oTriggerOnCreate",
    "eventList": [
        {
            "id": "5dd0a4b4-a420-45b9-838a-17928f11d10c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d941c42e-ec5d-4f25-8786-7d93207d7336"
        },
        {
            "id": "122f9a2c-a1c9-491e-9644-141511ca441d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d941c42e-ec5d-4f25-8786-7d93207d7336"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "90a9d325-0622-4243-9c19-69c5701c9e61",
    "visible": true
}