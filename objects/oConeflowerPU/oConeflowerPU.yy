{
    "id": "98f5d7ec-7ebd-4caa-93c3-612351d4d29f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oConeflowerPU",
    "eventList": [
        {
            "id": "43105688-2f3f-40f4-ba73-3f180c0446bc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "98f5d7ec-7ebd-4caa-93c3-612351d4d29f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": [
        {
            "id": "5586848c-e3ab-4e3e-bbe4-203c3d11ff5d",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "6c37065d-eb91-4300-a809-5b569349aa0c",
            "propertyId": "c4210375-1472-4e6a-bd60-cb042778825e",
            "value": "itemtype.redapple"
        },
        {
            "id": "00c4c4f2-393c-4fd5-98ef-f4cfde468c2f",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "d0b10fe6-3c23-4463-93d0-85557011c17c",
            "propertyId": "efe70b75-18af-4a52-838a-7790d19cb2f7",
            "value": "itemtype.coneflower"
        }
    ],
    "parentObjectId": "d0b10fe6-3c23-4463-93d0-85557011c17c",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8c7d38ba-ede2-4ffe-b48b-4f919d969762",
    "visible": true
}