{
    "id": "e8dbe644-a377-4afb-acd2-35b4d7d1dcb1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oEnergyBubble",
    "eventList": [
        {
            "id": "17c3910c-4952-4333-ab7e-4c31446a5322",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e8dbe644-a377-4afb-acd2-35b4d7d1dcb1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "75399ee4-bf6e-4a8a-8853-0440fa1bd579",
    "visible": true
}