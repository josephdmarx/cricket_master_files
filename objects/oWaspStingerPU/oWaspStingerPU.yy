{
    "id": "26b269cb-957b-42dc-9b52-a23b00754ca2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oWaspStingerPU",
    "eventList": [
        {
            "id": "34d72fd0-63c9-4ff8-b7a2-05c0df0779dd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "26b269cb-957b-42dc-9b52-a23b00754ca2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": [
        {
            "id": "a492b3dc-6632-4758-8be4-c6ae5bf4ea89",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "6c37065d-eb91-4300-a809-5b569349aa0c",
            "propertyId": "c4210375-1472-4e6a-bd60-cb042778825e",
            "value": "itemtype.waspstinger"
        }
    ],
    "parentObjectId": "6c37065d-eb91-4300-a809-5b569349aa0c",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d01f282a-5cdc-4f43-a1bb-ec278015fdb8",
    "visible": true
}