// Orbital motion
Angle += Speed;
if(Angle >= 360) Angle -= 360;

// Update position
x = lengthdir_x(Orbit, Angle) + Center_X;
y = lengthdir_y(Orbit, Angle) + Center_Y;

ParticleBurstSmall(x,y,oBlueSparkle);