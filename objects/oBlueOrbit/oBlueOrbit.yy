{
    "id": "979b7f82-335e-4985-b9c4-46db2c768dc5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oBlueOrbit",
    "eventList": [
        {
            "id": "ee133543-44b8-4477-87f9-b5b2e03f7d2b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "979b7f82-335e-4985-b9c4-46db2c768dc5"
        },
        {
            "id": "02b84ad2-88cf-4cb0-8b07-862ed81b97b4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "979b7f82-335e-4985-b9c4-46db2c768dc5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8b149d3f-5513-4039-827f-8ce4ff4243d1",
    "visible": true
}