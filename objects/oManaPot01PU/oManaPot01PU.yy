{
    "id": "17f795c1-18dc-4d34-aebe-2dedb142c9ea",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oManaPot01PU",
    "eventList": [
        {
            "id": "d708b6c4-15fb-4e36-b7c0-76903d19e543",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "17f795c1-18dc-4d34-aebe-2dedb142c9ea"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": [
        {
            "id": "ae2a378a-a6ab-4f6e-981d-958ebd6da1dc",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "6c37065d-eb91-4300-a809-5b569349aa0c",
            "propertyId": "c4210375-1472-4e6a-bd60-cb042778825e",
            "value": "itemtype.manapot01"
        }
    ],
    "parentObjectId": "6c37065d-eb91-4300-a809-5b569349aa0c",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f7ec87e2-1de5-41c1-85b2-472b05480db5",
    "visible": true
}