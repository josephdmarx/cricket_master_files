var _dmg = Mpower - other.def;

with(other)
	{
		if (immune = false)
		{
			flash = 3;
			life = life - _dmg
		}
	}

//Draw damage


audio_play_sound(sfxHit01,1,0);

instance_create_layer(x,y+15,"Effects",oDMG);
oDMG.value = _dmg;

ParticleBurstSmall(x,y,particle);

instance_change(deathAnim, true);


//instance_destroy();