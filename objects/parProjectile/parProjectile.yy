{
    "id": "3269c2bb-a7cc-47cc-84a0-19141b893294",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "parProjectile",
    "eventList": [
        {
            "id": "27ab1475-181c-4bff-8775-07a6b29ce8c9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3269c2bb-a7cc-47cc-84a0-19141b893294"
        },
        {
            "id": "f2b41582-5d7d-44db-a913-5c225fe7a02e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "3269c2bb-a7cc-47cc-84a0-19141b893294"
        },
        {
            "id": "bad61f3b-428e-45b6-bbb2-f28ec7e49f8b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "e41eb36a-da0c-468f-82ad-8d63c346e7e9",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "3269c2bb-a7cc-47cc-84a0-19141b893294"
        },
        {
            "id": "89cb8533-6543-490d-ba55-90cdd171b75d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "3269c2bb-a7cc-47cc-84a0-19141b893294"
        },
        {
            "id": "6790a3e7-712c-4306-a9be-48dd4c37a977",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 40,
            "eventtype": 7,
            "m_owner": "3269c2bb-a7cc-47cc-84a0-19141b893294"
        },
        {
            "id": "d19e03cd-dcca-44bc-a97d-7a1d8b5a52f7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "e181d273-45da-482e-8383-65bf4b53c74b",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "3269c2bb-a7cc-47cc-84a0-19141b893294"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": [
        
    ],
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f435b0ca-46e2-4f77-bfe9-833178d31b38",
    "visible": true
}