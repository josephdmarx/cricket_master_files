TargetX = oPlayer.x - x;
TargetY = oPlayer.y - y;


spd = 1;
hsp = 0;
vsp = 0;
grv = 0;
def = 0;
life = 2;
flash = 0;

knockbackforce = 5;
dying = false;
item = oInsectWingPU;
rareitem = oWaspStingerPU;
immune = false;
atkpower = 1;
hostile = true;
range = 100;
attackrange = 20;
alert = false;
attacking = false;
DeathSound = sfx_impDeath;
MoveSprite = sWasp_Idle;
IdleSprite = sWasp_Idle;
AttackSprite = sWasp_Idle;
myItemType = itemtype.none;
grounded = false;
flying = true;
flyaway = false;
injured = false;
wander = false;
wandertimer = 0;
alertimer = (room_speed * 2)
hop = false;
jumper = false;
hider = false;
turret = false;
wake_sprite = noone;
hide_sprite = noone;
sound_wakeup = noone;
stay = false;
egold = 1;
whackable = true;
hurt = false;

attacktype = NOTHING;


death = sWasp_D;

State = eStateIDLE;