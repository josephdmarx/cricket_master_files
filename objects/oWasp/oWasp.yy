{
    "id": "abeff35f-ff87-4e08-9773-ebb0b9b49d1b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oWasp",
    "eventList": [
        {
            "id": "1202d4ef-eac7-4791-b438-eb47cc84823f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "abeff35f-ff87-4e08-9773-ebb0b9b49d1b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "674e4a10-15be-454e-a541-a349c20f44a3",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "921cccda-9f40-479c-aab5-3040e58b58c4",
    "visible": true
}