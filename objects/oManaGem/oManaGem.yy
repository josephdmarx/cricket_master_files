{
    "id": "b6ee857c-ff39-4e23-826c-49f77b14a74d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oManaGem",
    "eventList": [
        {
            "id": "296a14a0-f55c-481a-9886-f159988cd606",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b6ee857c-ff39-4e23-826c-49f77b14a74d"
        },
        {
            "id": "dbc9939a-4f97-4314-8000-166cbf8b9d06",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "5ab0bdeb-238d-489a-9844-fbab736cb673",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "b6ee857c-ff39-4e23-826c-49f77b14a74d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "798c4d6f-4a22-4d0d-8f44-11e705cae995",
    "visible": true
}