/// @description Insert description here
// You can write your code in this editor

if !(burning) draw_self();

if (burning)
{
	DissolveAmount -= 0.05;	
	DissolveShader(sprite_index, image_index, x, y, DissolveAmount);
}

if (DissolveAmount <= 0)
{
	instance_destroy();
}