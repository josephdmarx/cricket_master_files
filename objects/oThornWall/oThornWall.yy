{
    "id": "87d4d4d6-2540-4995-bf70-4b338893d03b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oThornWall",
    "eventList": [
        {
            "id": "7e600fbe-d91a-4c8a-9860-fcd4667bbf77",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "87d4d4d6-2540-4995-bf70-4b338893d03b"
        },
        {
            "id": "f4bb5364-0ee1-4210-aab3-e12e23da6b40",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "11bcfd02-cabb-4310-a4b8-f10e88993e2a",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "87d4d4d6-2540-4995-bf70-4b338893d03b"
        },
        {
            "id": "df80b1fa-83be-45bf-803b-07745a723c7d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "87d4d4d6-2540-4995-bf70-4b338893d03b"
        },
        {
            "id": "a6ed6036-faad-4a4a-8729-9ecad60d21d0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "87d4d4d6-2540-4995-bf70-4b338893d03b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e9cf9f17-7512-4b7c-bb5a-dd6146d6c2f5",
    "visible": true
}