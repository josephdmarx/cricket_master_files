/// @description Insert description here
// You can write your code in this editor
	if (point_in_circle(oPlayer.x,oPlayer.y,x,y,64)) and (on = false)
	{
		on = true;
		instance_create_layer(x,y-40,"GUI",oBubbleTail);
		instance_create_layer(x,y-40,"GUI",oUPKey);
	}
	
	
	if !(point_in_circle(oPlayer.x,oPlayer.y,x,y,64)) and (on = true)
	{
		on = false;
		instance_destroy(oBubbleTail);
		instance_destroy(oUPKey);
	}
	
	if (instance_exists(oCutscene))
	{
		instance_destroy(oBubbleTail);
		instance_destroy(oUPKey);
	}
