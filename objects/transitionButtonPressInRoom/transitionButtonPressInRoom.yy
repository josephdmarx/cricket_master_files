{
    "id": "fb6afcb9-d17f-4947-88e2-55c700e181dc",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "transitionButtonPressInRoom",
    "eventList": [
        {
            "id": "f3ae50a2-1e02-4cd1-ae47-8fddf3c63012",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "5ab0bdeb-238d-489a-9844-fbab736cb673",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "fb6afcb9-d17f-4947-88e2-55c700e181dc"
        },
        {
            "id": "036c1b0f-30dd-4917-b643-b8a909d3ed7d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "fb6afcb9-d17f-4947-88e2-55c700e181dc"
        },
        {
            "id": "7dfd2121-1d36-4eb5-9b10-088522c75d82",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "fb6afcb9-d17f-4947-88e2-55c700e181dc"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "98c8dcd6-5f6a-49fe-906f-2de7aba5aa9b",
    "visible": false
}