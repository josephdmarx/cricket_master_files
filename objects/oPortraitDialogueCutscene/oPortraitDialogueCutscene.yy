{
    "id": "79a02aec-48c8-4640-96e1-edca41f67dbf",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oPortraitDialogueCutscene",
    "eventList": [
        {
            "id": "96b5e24a-f463-4d39-8a90-01c3252db847",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "79a02aec-48c8-4640-96e1-edca41f67dbf"
        },
        {
            "id": "36623400-654a-4a00-8f6e-5d8fea5ec605",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "79a02aec-48c8-4640-96e1-edca41f67dbf"
        },
        {
            "id": "a071fc56-f398-4261-8d26-539641da86b1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "79a02aec-48c8-4640-96e1-edca41f67dbf"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}