{
    "id": "ec422416-2d93-4d05-b128-7164eb99c1be",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oJellycapPU",
    "eventList": [
        {
            "id": "5d179779-af06-42e0-a26a-8a668a0b4fc8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ec422416-2d93-4d05-b128-7164eb99c1be"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": [
        {
            "id": "e7d6c118-7d4e-4476-8cc9-e2df8852c14c",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "6c37065d-eb91-4300-a809-5b569349aa0c",
            "propertyId": "c4210375-1472-4e6a-bd60-cb042778825e",
            "value": "itemtype.redapple"
        },
        {
            "id": "69fd8471-93ec-426a-a461-376fc488637c",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "d0b10fe6-3c23-4463-93d0-85557011c17c",
            "propertyId": "efe70b75-18af-4a52-838a-7790d19cb2f7",
            "value": "itemtype.jellycap"
        }
    ],
    "parentObjectId": "d0b10fe6-3c23-4463-93d0-85557011c17c",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "4b4920dc-5d81-44e7-9a10-fe0f388a69ab",
    "visible": true
}