{
    "id": "6f16630f-45f7-4437-b3ea-9b39ca36e37c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_light_mouse",
    "eventList": [
        {
            "id": "3042952d-2c11-4e11-b5bc-93c6ad9ffc22",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "6f16630f-45f7-4437-b3ea-9b39ca36e37c"
        },
        {
            "id": "492cc2d0-4e70-42a7-9c5c-0a454413d958",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "6f16630f-45f7-4437-b3ea-9b39ca36e37c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": [
        {
            "id": "e24c703b-0106-499d-9f25-aea6a056f672",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "554c7ae1-ce9e-4dcd-b03c-3fc06f694594",
            "propertyId": "219572d8-1198-45c2-8df6-ebec5b27a3d4",
            "value": "False"
        }
    ],
    "parentObjectId": "554c7ae1-ce9e-4dcd-b03c-3fc06f694594",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}