/// @desc Move to the next room
if (active)
{
	with(oPlayer)
	{
		if (hascontrol)
		{
			hascontrol = false;
			SlideTransition(TRANS_MODE.GOTO, other.target, other.targetX, other.targetY);
		}
	}
}