{
    "id": "6942afab-69a0-48f6-9ca8-08f7f181ac90",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "transitionCollision",
    "eventList": [
        {
            "id": "4bd5e43c-4cdf-41e4-99bb-5b2b4eaaaf15",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "5ab0bdeb-238d-489a-9844-fbab736cb673",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "6942afab-69a0-48f6-9ca8-08f7f181ac90"
        },
        {
            "id": "fa7394b8-680e-4663-b558-94c371fe758a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6942afab-69a0-48f6-9ca8-08f7f181ac90"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "98c8dcd6-5f6a-49fe-906f-2de7aba5aa9b",
    "visible": false
}