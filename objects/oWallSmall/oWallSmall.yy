{
    "id": "d78695fd-8abd-466d-ac65-25a36641bc98",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oWallSmall",
    "eventList": [
        {
            "id": "ad53f4ae-c678-4ac5-953b-d7173fbea397",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d78695fd-8abd-466d-ac65-25a36641bc98"
        },
        {
            "id": "6194ed96-a564-40a5-882b-9252b5523596",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d78695fd-8abd-466d-ac65-25a36641bc98"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "1e5b4d39-1484-49d0-97bb-d4436467508a",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d7561f40-c0b0-4a95-86d7-c961be5e88c0",
    "visible": true
}