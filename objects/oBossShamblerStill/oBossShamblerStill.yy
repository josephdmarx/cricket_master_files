{
    "id": "39061a89-7c88-4882-904e-65e25e25f422",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oBossShamblerStill",
    "eventList": [
        {
            "id": "9a81fa5d-ab82-44f3-a7f5-fde71186bc82",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "39061a89-7c88-4882-904e-65e25e25f422"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "342749da-1d52-4c63-9e9f-75607fb3c9aa",
    "visible": true
}