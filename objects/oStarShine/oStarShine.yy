{
    "id": "0b857bdf-f17d-426f-b99f-be37290f093f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oStarShine",
    "eventList": [
        {
            "id": "0150af67-0be9-4e47-bcdb-8f78b2269768",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "0b857bdf-f17d-426f-b99f-be37290f093f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6ac9e69d-a8b0-4f81-9157-529d1775335c",
    "visible": true
}