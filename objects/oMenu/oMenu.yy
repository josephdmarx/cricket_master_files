{
    "id": "b05d26a3-c86c-4979-a64f-4c016a34fdd3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oMenu",
    "eventList": [
        {
            "id": "f2fbb635-77a4-4ccb-bf71-35db19cd7ec1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "b05d26a3-c86c-4979-a64f-4c016a34fdd3"
        },
        {
            "id": "b7060dd3-f2ed-4bd4-be73-5069051556c7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b05d26a3-c86c-4979-a64f-4c016a34fdd3"
        },
        {
            "id": "8e7a3543-3b4e-40ae-a5b7-edc0417ca787",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b05d26a3-c86c-4979-a64f-4c016a34fdd3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}