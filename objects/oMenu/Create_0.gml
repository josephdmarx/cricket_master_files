/// @desc Menu Setup

display_set_gui_size(640,360);

gui_width = display_get_gui_width();
gui_height = display_get_gui_height();
gui_margin = 64;

menu_x = gui_width * 0.5;
menu_y = (gui_height * 0.5) - gui_margin;

menu_font = font01;
menu_itemheight = font_get_size(font01);
menu_committed = -1;
menu_control = true;

//menu array
menu[2] = "New Game";
menu[1] = "Continue";
menu[0] = "Quit Game";

menu_items = array_length_1d(menu); //gives us the length of the menu array
menu_cursor = 2; //decides which option we're looking at in the array


