/// @description Control Menu


#region keyboard controls

if (menu_control)

	if (instance_exists(oPlayer)) 
	{
		oPlayer.hascontrol = false;
	}

	if (keyboard_check_pressed(vk_up)) or (gamepad_button_check_pressed(0, gp_padu))
	{
		audio_play_sound(sfxMenuClick, 1, 0);
		menu_cursor ++;
		if (menu_cursor >= menu_items) menu_cursor = 0;
	}
	
	if (keyboard_check_pressed(vk_down)) or (gamepad_button_check_pressed(0, gp_padd))
	{
		audio_play_sound(sfxMenuClick, 1, 0);
		menu_cursor --;
		if (menu_cursor <  0) menu_cursor = menu_items - 1;
	}

	if (keyboard_check_pressed(vk_enter)) or (gamepad_button_check_pressed(0, gp_start))
	{
		audio_play_sound(sfxMenuAccept, 1, 0);
		menu_committed = menu_cursor;  //our selected item is our current cursor position
		menu_control = false;
	}

#endregion

if (menu_committed != -1)
{
	switch (menu_committed)
	{
		case 2: default:  //This is the NEW GAME condition
			SlideTransition(TRANS_MODE.NEXT);
		break
						
		case 1: //This is the CONTINUE condition
		{
			if (!file_exists("savegame.sav"))	
			{
				show_message("NO GAME DATA");	
			}
		
		
			else
			{
				// load the savegame.sav
				
				with (oPlayer) instance_destroy();
				
				
				if (file_exists("savegame.sav"))
				{
					
					var _wrapper = loadJsonfromfile("savegame.sav");
					var _list = _wrapper[? "ROOT"];
					
					for (var i = 0; i < ds_list_size(_list); i++)
					{
						var _map = _list[| i];
						
						var _obj = _map[? "obj"];
						
					   
						with (instance_create_layer(0,0,layer,asset_get_index(_obj)))
						{
							room = _map[? "room"];
							x = _map[? "x"];
							y = _map[? "y"];
							layer = _map[? "layer"];
							canpickup = _map[? "canpickup"];
							life = _map[? "life"];
							lifemax = _map[? "lifemax"];
							mana = _map[? "mana"];
							manamax = _map[? "manamax"];
							MQstage = _map[? "MQstage"];	
							TutorialStage = _map[? "TutorialStage"];
						}
						
					
					
					}
					
					ds_map_destroy(_wrapper);
					
				}
				
			}
		}
		break;
						
		case 0:
			game_end();
		break
		
		
	}
}