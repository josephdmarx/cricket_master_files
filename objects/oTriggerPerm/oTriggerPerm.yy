{
    "id": "701757be-4d6e-4040-8025-be7ef33f5be3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oTriggerPerm",
    "eventList": [
        {
            "id": "9c82f092-7886-428e-98e5-a77d5c97e696",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "701757be-4d6e-4040-8025-be7ef33f5be3"
        },
        {
            "id": "ff653dfe-f67f-45d5-bdeb-362dd8c3d868",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "701757be-4d6e-4040-8025-be7ef33f5be3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "bff59029-5b08-4e8d-bd8b-3103e7f47140",
    "visible": true
}