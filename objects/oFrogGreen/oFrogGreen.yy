{
    "id": "80e89ab3-d0b6-4135-b5e2-90a2a8338ba7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oFrogGreen",
    "eventList": [
        {
            "id": "48d34a9c-77aa-40fb-a9bf-0383a64ee3d3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "80e89ab3-d0b6-4135-b5e2-90a2a8338ba7"
        },
        {
            "id": "6b11f215-bbd1-4b64-9741-a2e80c8183af",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "3c3791e7-afc0-45bb-87b6-7dc41f955d2a",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "80e89ab3-d0b6-4135-b5e2-90a2a8338ba7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "c397ffe4-21bb-4149-a4d9-9d44c5be99c0",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "618b439e-aa46-4fcc-a6dc-f62e8c015b7d",
    "visible": true
}