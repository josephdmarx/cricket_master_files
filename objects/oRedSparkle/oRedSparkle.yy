{
    "id": "4b4be42c-4248-4ff3-8949-87e07d5adeaf",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oRedSparkle",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "36be9821-624b-4ec6-b86e-9e38f02e885f",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "76c57b56-289e-4193-ba82-a441f5064e9b",
    "visible": true
}