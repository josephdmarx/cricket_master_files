{
    "id": "01b97589-151a-4898-9318-0c4b70b2410d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oHealthBar",
    "eventList": [
        {
            "id": "1f8d1f12-87af-48e8-a639-6d38b0190a5d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "01b97589-151a-4898-9318-0c4b70b2410d"
        },
        {
            "id": "bdf6974d-0cc5-401d-ba54-16538ba2fa66",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "01b97589-151a-4898-9318-0c4b70b2410d"
        },
        {
            "id": "ca2185d6-447d-4c7d-b105-60aaa28fb82a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "01b97589-151a-4898-9318-0c4b70b2410d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "56c7c67e-c688-4c7a-8d51-1b97c339264a",
    "visible": true
}