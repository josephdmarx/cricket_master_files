draw_sprite(sHealthBar, -1, 10, 12);

if !(instance_exists(oPlayer)) exit;   //if the player object isn't here, exit

drawhealth = lerp(drawhealth, oPlayer.life, .5);

draw_set_alpha(1);
draw_set_color(c_red);
draw_rectangle(15, 17, 10+88 * (drawhealth / oPlayer.lifemax)  , 10+13,false);
draw_set_color(c_white);