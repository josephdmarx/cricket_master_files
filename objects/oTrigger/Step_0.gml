//one time trigger based on MQ Stage
if (oPlayer.MQstage == trigger_id) and (active)
{
	if (!instance_exists(oCutscene)) and !(triggered)
	{
		if (place_meeting(x,y,oPlayer)) 
		{	
			triggered = true;
			createCutscene(t_scene_info);
		}

	}
}

	if !(place_meeting(x,y,oPlayer)) 
		triggered = false;