{
    "id": "5cc5506b-3dbd-496a-ae29-d396e39effd0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oTrigger",
    "eventList": [
        {
            "id": "cb332c42-11e9-47d4-ad78-e2a34b6ecc3a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5cc5506b-3dbd-496a-ae29-d396e39effd0"
        },
        {
            "id": "a0339180-07e1-4c82-98b9-9bb98ed01c16",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "5cc5506b-3dbd-496a-ae29-d396e39effd0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "47123730-a81d-43e3-8eeb-f8e082aaeac6",
    "visible": true
}