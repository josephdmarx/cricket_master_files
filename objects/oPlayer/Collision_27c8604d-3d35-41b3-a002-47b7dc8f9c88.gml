
if (immunedelay <= 0)
{
	
	knockbackforce =  other.knockbackforce;
	life = life - other.atkpower;
	flash = 3;
	
	immunedelay = room_speed;  //.5 second immunity to being hit
	
	var _direction = point_direction(other.x, other.y, x, y);

	var _xforce = -(lengthdir_x(knockbackforce, _direction));
	var _yforce = (lengthdir_y(knockbackforce, _direction));
	var _pitch = (random_range(1, 1.2));
	
	audio_sound_pitch(sfxCricketOof, _pitch);
	audio_play_sound(sfxCricketOof, 1, 0);
	
	hsp = _xforce * 1;
	vsp = -4;
	hascontrol = false;
	hurt = true;
}