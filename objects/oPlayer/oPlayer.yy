{
    "id": "5ab0bdeb-238d-489a-9844-fbab736cb673",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oPlayer",
    "eventList": [
        {
            "id": "ff4cec95-b042-40ae-ad47-6fa48b9cf5cb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5ab0bdeb-238d-489a-9844-fbab736cb673"
        },
        {
            "id": "f4815ead-9eef-42c3-ada5-265866c78d51",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "5ab0bdeb-238d-489a-9844-fbab736cb673"
        },
        {
            "id": "7679ae85-c041-4393-92c3-d708af01a570",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "5ab0bdeb-238d-489a-9844-fbab736cb673"
        },
        {
            "id": "3caa9513-bd1d-4be1-9959-64c6fb990265",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "5ab0bdeb-238d-489a-9844-fbab736cb673"
        },
        {
            "id": "72628e54-2747-451e-ac6a-d72137bc7013",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "e41eb36a-da0c-468f-82ad-8d63c346e7e9",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "5ab0bdeb-238d-489a-9844-fbab736cb673"
        },
        {
            "id": "27c8604d-3d35-41b3-a002-47b7dc8f9c88",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "3d68bb9b-56fd-4a7a-b31d-a29bfafe755b",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "5ab0bdeb-238d-489a-9844-fbab736cb673"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "432d7032-d117-44ed-910f-1cb4d16b538f",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "271e9ea0-f725-4c4a-9bd5-a73a4fbe6d5d",
    "visible": true
}