/// @description Initialize

//MQ Stage for testing

//location unlocks
//unlock_meadowtown = false; 
//unlock_bogcaves = false;

//ability unlocks

cancast = true;
canpickup = true;

// initialization
MQstage = 0;
TutorialStage = -1;

legframe = 0;

animtoplay = 0;

Horiz = 0
hsp = 0;
vsp = 0;
SlowSpeed = 1;
WalkSpeed = 4;
grv = 0.4;
grounded = true;
underwater = false;

hascontrol = false;
immunedelay = 0;
inputdelay = 0;

//gold = 0;

manamax = 5;
mana = 0;

def = 0;
lifemax = 5;
life = lifemax;

charge_count =0;
location = 0;

name = "Cricket";
portrait = sCricketPort;
radius = 64;
chapter = 0;

stun = false;
charging = false;
flash = 0;

knockbackforce = 0;
hitby = noone;
hurt = false;

MeleeAttack = false;
CanAttack = true;
XOffset = -CELL;
YOffset = 0;
flying = false;

Direction = NOKEY;
Action = IDLE;
SequenceCount = 0;  //combo counter

State = StateIDLE;

IdleSprite = sPlayer;
MoveSprite = sPlayerWalk;

//Combo Array
NoKeyCombo[0]=sPlayerM0;
NoKeyCombo[1]=sPlayerM1;
//NoKeyCombo[2]=sPlayerM2;

//Horizontal Combo
HorizCombo[0]=sPlayerM0;
HorizCombo[1]=sPlayerM1;

//Horizontal Running Combo
HorizWCombo[0]=sPlayerM0Arms;
HorizWCombo[1]=sPlayerM0Arms;

//HorizCombo[2]=sPlayerM2;

//Air Combo
AirCombo[0]=sPlayerA0;
AirCombo[1]=sPlayerA1;
//AirCombo[2]=sPlayerA2;

//Charge Attack
ChargeAttack[0] = sPlayerM2;
ChargingUp[0] = sPlayerCharge;

NetAttack[0] = sPlayerNet;

Sprite[NOKEY,IDLE]			= sPlayer;
Sprite[HORIZONTAL,IDLE]		= sPlayerWArms;
Sprite[AIR,IDLE]			= sPlayerA;
Sprite[UP,IDLE]				= sPlayerA;
Sprite[DOWN,IDLE]			= sPlayerD;
Sprite[CROUCH,IDLE]			= sPlayerCrouch;
Sprite[WALK, IDLE]			= sPlayerWalk;

Sprite[NOKEY,NET]			= sPlayerNet; 
Sprite[HORIZONTAL,NET]		= sPlayerNet; 
Sprite[AIR,NET]				= sPlayerNet;  
Sprite[UP,NET]				= sPlayerNet;
Sprite[DOWN,NET]			= sPlayerNet;
Sprite[CROUCH,NET]			= sPlayerCrouch;

Sprite[NOKEY,CANE]			= sPlayerCane;  
Sprite[HORIZONTAL,CANE]		= sPlayerCane; 
Sprite[AIR,CANE]			= sPlayerCane;  
Sprite[UP,CANE]				= sPlayerCane;
Sprite[DOWN,CANE]			= sPlayerCane;
Sprite[CROUCH,CANE]			= sPlayerCrouch;

Sprite[NOKEY,ATTACK]		= sPlayerM0;  
Sprite[HORIZONTAL,ATTACK]	= sPlayerM0; 
Sprite[AIR,ATTACK]			= sPlayerA0;  
Sprite[UP,ATTACK]			= sPlayerA0;
Sprite[DOWN,ATTACK]			= sPlayerA0;
Sprite[CROUCH,ATTACK]		= sPlayerCrouch;

Sprite[NOKEY, FIRESPELL]		= sPlayerCast01;
Sprite[HORIZONTAL, FIRESPELL]	= sPlayerCast01;
Sprite[AIR, FIRESPELL]			= sPlayerCast01Air;
Sprite[UP, FIRESPELL]			= sPlayerCast01Air;
Sprite[DOWN, FIRESPELL]			= sPlayerCast01Air;
Sprite[CROUCH, FIRESPELL]		= sPlayerCrouch;

Sprite[NOKEY,CHARGING]		= sPlayerCharge; 
Sprite[HORIZONTAL,CHARGING] = sPlayerCharge; 
Sprite[AIR,CHARGING]		= sPlayerChargeA;
Sprite[UP,CHARGING]			= sPlayerChargeA;
Sprite[DOWN,CHARGING]		= sPlayerChargeA;
Sprite[CROUCH,CHARGING]		= sPlayerCrouch;

Sprite[NOKEY,SLEEPTREE]		= sPlayerSleep;
Sprite[NOKEY,ANIMATE]		= sPlayer;
Sprite[NOKEY,WAKEUP1]		= sPlayerWakeup;

Sprite[NOKEY,WALK]			= sPlayerWalk;