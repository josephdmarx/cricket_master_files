/// @desc checks life, executes, the current state, changes the animation, and calls the move script

////DEBUGS
//show_debug_message("MQStage:");
//show_debug_message(MQstage);
//show_debug_message("TutorialStage:");
//show_debug_message(TutorialStage);

if (global.paused)
{
	image_speed = 0;	
	exit;
}

image_speed = 1;

LocationCheck();

#region OPEN MENU
inputdelay--;

if (inputdelay <= 0) //prevent jumping on menu exit
{
	GetInput();
}


if (key_start)
{
	audio_play_sound(sfxMenuAccept, 1, 0);
	oPlayer.hascontrol = false;
	oStartMenuManager.menu_active = true;
}

#endregion

#region UNDERWATER



if (place_meeting(x,y,oWater)) and !(underwater)
	{	
		splash();
		underwater = true;
	}
	
if !(place_meeting(x,y,oWater)) and (underwater) 
{
		splash();
		underwater = false;	
}

#endregion

immunedelay --;

if (life <= 0)
	game_restart();

#region legframe calculation for attacking while running

legframe += 0.25;   //15 fps

if (legframe > 8)
	legframe = 0;
	
#endregion

#region 3 Shot Attack

var _shotcount = instance_number(parProjectile);

if (_shotcount < 3) and (State != StateCROUCH)//keeps player from spamming bullets and not casting while crouching
{
			if (key_cast) and (cancast)
			{
	
				image_index = 0;  //reset the animation	
				Sprite[@ AIR, ATTACK] = AirCombo[SequenceCount];
				Sprite[@ NOKEY, ATTACK] = NoKeyCombo[SequenceCount];
				Sprite[@ HORIZONTAL, ATTACK] = HorizCombo[SequenceCount];
				Sprite[@ UP, ATTACK] = AirCombo[SequenceCount];
				Sprite[@ DOWN, ATTACK] = AirCombo[SequenceCount];			
				
				State = StateATTACK;
				
				CanAttack = true;
								
				SequenceCount++;
				if (SequenceCount >= 2)
				{
						SequenceCount = 0;  //resets the attack sequence
				}
	
	
			}
}

#endregion

script_execute(State);

sprite_index = Sprite[Direction,Action];

if (hurt)
MoveHURT(oWall);

else
MovePlayer(oWall);