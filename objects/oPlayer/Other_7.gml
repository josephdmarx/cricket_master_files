/// @description switch state when the current animation ends

switch (State)
{
	case StateIDLE:
		image_speed = 1;
		break;
		
	case StateATTACK:
		Action = IDLE;
		CanAttack = true;
		State = StateIDLE;
		break;
		
	case StateFIRE:
		CanAttack = true;
		Action = IDLE;
		State = StateIDLE;
		break;
		
	case StateCHARGEATTACK:
		CanAttack = true;
		stun = false;
		charge_count = 0;
		SequenceCount = 0;
		Action = IDLE;
		State = StateIDLE;
		break;
		
	case StateHURT:
		
		State = StateIDLE;
		break;
		
		
		
	case StateNET:
		CanAttack = true;
		MeleeAttack = false;
		State = StateIDLE;
		break;
	
	case StateCANE:
		CanAttack = true;
		MeleeAttack = false;
		State = StateIDLE;
		break;
		
	case StateSLEEPTREE:
		break;
		
	case StateMOVE:
		break;
		
	case StateCROUCH:
		if  (key_down)
		{
			State = StateCROUCH;	
		}
		else
		{
			CanAttack = true;
			State = StateIDLE;
		}
		break;
		
	case StateANIMATION:
		image_speed = 0;						//causes the called animation to pause on the final frame
		image_index = image_number - 1;
		break;
		
	case StateWALKPLAYER:
		break;
}