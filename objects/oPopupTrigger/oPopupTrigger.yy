{
    "id": "8b04bb91-094e-44d0-b60b-ddfc962fb836",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oPopupTrigger",
    "eventList": [
        {
            "id": "9f09a9d6-499f-431e-9eee-bda650a89026",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8b04bb91-094e-44d0-b60b-ddfc962fb836"
        },
        {
            "id": "ce84ae44-1878-47c1-b678-c206dce7d591",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8b04bb91-094e-44d0-b60b-ddfc962fb836"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "47123730-a81d-43e3-8eeb-f8e082aaeac6",
    "visible": true
}