//one time trigger based on MQ Stage
if (oPlayer.TutorialStage == tutorial_id)
{
	if (place_meeting(x,y,oPlayer)) 
	{	
		triggered = true;
		with (instance_create_layer(0,0,"GUI",oTutorialBox))
		{
				text1 = other.text1;
				text2 =	other.text2;
				text3 = other.text3;
				text4 = other.text4;
				key = other.key;
				keyx = other.keyx;
				keyy = other.keyy;
		}
		instance_destroy();
	}
}
