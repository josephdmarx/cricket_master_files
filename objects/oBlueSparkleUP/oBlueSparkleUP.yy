{
    "id": "3e2590a4-7b89-4b7d-88ac-a1827d278779",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oBlueSparkleUP",
    "eventList": [
        {
            "id": "88ec4f73-af11-4a1f-8d91-8996e8dded0b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3e2590a4-7b89-4b7d-88ac-a1827d278779"
        },
        {
            "id": "3d11a692-9159-4a8d-adb6-42ff754f681c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "3e2590a4-7b89-4b7d-88ac-a1827d278779"
        },
        {
            "id": "a587b613-915e-44cb-b8dc-00df67c028d1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "3e2590a4-7b89-4b7d-88ac-a1827d278779"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8b149d3f-5513-4039-827f-8ce4ff4243d1",
    "visible": true
}