{
    "id": "b2f27953-f08f-4d44-b14c-8753e192f4b0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oCaveBreakWallBlackout",
    "eventList": [
        {
            "id": "9f206a13-3656-4570-9845-fa40daafdb19",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b2f27953-f08f-4d44-b14c-8753e192f4b0"
        },
        {
            "id": "7f1e618d-33d5-4d4f-9b29-cdec3f088246",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b2f27953-f08f-4d44-b14c-8753e192f4b0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6ff411b9-ac76-49ed-aa03-b8b358b832da",
    "visible": true
}