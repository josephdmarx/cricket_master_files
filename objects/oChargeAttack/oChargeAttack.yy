{
    "id": "3600b68b-5f11-4920-8d51-1f64bbb83499",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oChargeAttack",
    "eventList": [
        {
            "id": "d9b09476-9782-45ca-92b2-9b2068f079b3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3600b68b-5f11-4920-8d51-1f64bbb83499"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "3269c2bb-a7cc-47cc-84a0-19141b893294",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f435b0ca-46e2-4f77-bfe9-833178d31b38",
    "visible": true
}