/// @description Insert description here
// You can write your code in this editor
if (interactable = true)
{
	if (point_in_circle(oPlayer.x,oPlayer.y,x,y,64)) and (on = false)
	{
		on = true; 
		with (instance_create_layer(x + 15, y - 35, "Effects", oSpeechBubbleShort))
		{
			text = "  . . .";	
		}
	}

	if !(point_in_circle(oPlayer.x,oPlayer.y,x,y,64)) and (on = true)
	{
		on = false;
		instance_destroy(oSpeechBubbleShort);
	}
}

if (place_meeting(x,y+1,oWall))
{
	 grounded = true;
	 
	 if (moving) sprite_index = MoveSprite;
	 
	 else sprite_index = IdleSprite;
}


if !(grounded) 
{
	GroundMe(oWall);
	sprite_index = JumpSprite;
}
