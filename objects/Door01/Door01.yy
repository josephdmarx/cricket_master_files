{
    "id": "e0db093f-d815-4ad6-ba8e-18db1e593165",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Door01",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "503953ce-3280-44c8-bb6e-175dfea7cbdc",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "913d6ced-d333-437d-89fa-fd61ad89a906",
    "visible": true
}