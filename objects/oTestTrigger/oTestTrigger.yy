{
    "id": "6ef6b534-6953-450d-898e-e5537fec5110",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oTestTrigger",
    "eventList": [
        {
            "id": "55b8ddff-bc33-4bca-870e-44c76ba152c2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6ef6b534-6953-450d-898e-e5537fec5110"
        },
        {
            "id": "7cf1d584-cd81-4fca-b304-2eccbc287ab2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "5ab0bdeb-238d-489a-9844-fbab736cb673",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "6ef6b534-6953-450d-898e-e5537fec5110"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "47123730-a81d-43e3-8eeb-f8e082aaeac6",
    "visible": true
}