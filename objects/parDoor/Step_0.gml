/// @description Insert description here
// You can write your code in this editor
/// @description Insert description here
// You can write your code in this editor

image_speed = 0;

if (interactable = true)
{
	if (point_in_circle(oPlayer.x,oPlayer.y,x+20,y,32)) and (on = false)
	{
		on = true;
		image_index = 1;
		//instance_create_layer(x+16,y-40,"GUI",oBubbleTail);
		//instance_create_layer(x+16,y-40,"GUI",oEKey);
	}
	
	

	else if !(point_in_circle(oPlayer.x,oPlayer.y,x+20,y,32)) and (on = true)
	{
		on = false;
		image_index = 0;
		//instance_destroy(oBubbleTail);
		//instance_destroy(oEKey);
	}
}

if (on) and (oPlayer.key_interact)
{	
	oPlayer.image_xscale = facing;
	
	with(oPlayer)
	{
		
		
		if (hascontrol)
		{
			hascontrol = false;
			audio_play_sound(sfxWoodDoorOpen,1,0);
			SlideTransition(TRANS_MODE.GOTO, other.target, other.targetX, other.targetY);	
		}
	}
}
