{
    "id": "b94f8c3b-237c-49d0-a822-af5cef204b56",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oSlamBox",
    "eventList": [
        {
            "id": "f77124e6-580d-496b-af5a-e36088f1bfbd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b94f8c3b-237c-49d0-a822-af5cef204b56"
        }
    ],
    "maskSpriteId": "366dbb84-305d-45f4-a321-f4cf4030c7b5",
    "overriddenProperties": null,
    "parentObjectId": "3d68bb9b-56fd-4a7a-b31d-a29bfafe755b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "366dbb84-305d-45f4-a321-f4cf4030c7b5",
    "visible": true
}