/// @description Insert description here
// You can write your code in this editor
spd = 2;
vsp = 0;
grv = 0;
def = 0;
life = 5;
flash = 0;

knockbackforce = 5;
dying = false;
item = oManaGem;
rareitem = oManaGem;
immune = false;
atkpower = 1;
hostile = true;
injured = false;
range = 200;
attackrange = 20;
alert = false;
DeathSound = sfx_impDeath;
MoveSprite = sMuck_Idle;
IdleSprite = sMuck_Idle;
AttackSprite = sMuck_Idle;
myItemType = itemtype.none;
grounded = false;
flying = false;
flyaway = false;
wander = false;
wandertimer = 0;
alertimer = (room_speed * 2)
hop = false;
jumper = false;
hider = false;
turret = false;
wake_sprite = noone;
hide_sprite = noone;
sound_wakeup = noone;

DeathSound = sfx_impDeath;
deathSprite = sMuck_Death;
hurtSprite = sMuck_Idle;
stunSprite = sMuck_Idle;
stay = true;
stayframe = oDeadMuck;
egold = 2;

TargetX = 0;

attacktype = NOTHING;

State = eStateMOVEBOUNCE;