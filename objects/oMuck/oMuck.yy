{
    "id": "e9bbda25-af53-44c7-84a7-9a22b789ecfd",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oMuck",
    "eventList": [
        {
            "id": "a64b5cef-dae7-45f8-b758-463d56454de3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e9bbda25-af53-44c7-84a7-9a22b789ecfd"
        },
        {
            "id": "2a5d546e-6192-4968-92b0-92d3582fcad6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e9bbda25-af53-44c7-84a7-9a22b789ecfd"
        },
        {
            "id": "98358968-ae5d-4459-b31d-94b2b837be7b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 50,
            "eventtype": 7,
            "m_owner": "e9bbda25-af53-44c7-84a7-9a22b789ecfd"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4a6e639a-9057-47d3-904c-c6ce5224f566",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "9c113f90-cea3-403f-af4b-41b1f1a1808f",
    "visible": true
}