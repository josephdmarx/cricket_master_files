{
    "id": "2fa999b7-5598-406b-ae10-fb684d4e47d7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_light_player",
    "eventList": [
        {
            "id": "6e15c019-eb86-4d1a-a6a0-42160c9200fd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "2fa999b7-5598-406b-ae10-fb684d4e47d7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": [
        {
            "id": "37c8db9c-7ec6-4cae-a517-964512b10a30",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "554c7ae1-ce9e-4dcd-b03c-3fc06f694594",
            "propertyId": "219572d8-1198-45c2-8df6-ebec5b27a3d4",
            "value": "False"
        }
    ],
    "parentObjectId": "554c7ae1-ce9e-4dcd-b03c-3fc06f694594",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}