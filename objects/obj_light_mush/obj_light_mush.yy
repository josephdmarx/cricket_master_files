{
    "id": "5a9be243-1509-4f68-bcd1-291003b3971d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_light_mush",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": [
        {
            "id": "9e8f6fd5-d2e7-429f-a7b9-9ca7003663a6",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "554c7ae1-ce9e-4dcd-b03c-3fc06f694594",
            "propertyId": "219572d8-1198-45c2-8df6-ebec5b27a3d4",
            "value": "False"
        }
    ],
    "parentObjectId": "554c7ae1-ce9e-4dcd-b03c-3fc06f694594",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}