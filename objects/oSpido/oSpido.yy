{
    "id": "e78f35a5-1ad6-4c17-82d3-6c17fda310f3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oSpido",
    "eventList": [
        {
            "id": "80930fb6-ace7-4230-a0b0-b562d3a5d68f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e78f35a5-1ad6-4c17-82d3-6c17fda310f3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a5bda1d6-6803-49d2-a893-e3cbd4d7507f",
    "visible": true
}