/// @desc progress text

if (instance_exists(oPlayer)) oPlayer.hascontrol = false;

letters += spd;

name = names[page];
portrait = portraits[page];

text_current = string_copy(text[page],1,floor(letters));

draw_set_font(fontDialogue);

if (h == 0)
	h = string_height(text[page]);
	
w = string_width(text_current);

//destroy when done

if (letters >= length)
{
	if (keyboard_check_pressed(ord("E"))) or (gamepad_button_check_pressed(0, gp_face4))
		{
			if(page < array_length_1d(text) -1)
			{
				page ++;
				letters = 0;
			}
		
			else 
			{
				alreadyplayed = true;
				oPlayer.hascontrol = true;
				oPlayer.inputdelay = 10;
				instance_destroy();
			}
		}
}