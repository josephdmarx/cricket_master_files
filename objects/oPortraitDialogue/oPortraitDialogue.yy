{
    "id": "b30d5a2a-f798-4e8b-ae61-696302f6090b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oPortraitDialogue",
    "eventList": [
        {
            "id": "5352189e-c20c-4173-94d7-2a3fa888b58d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "b30d5a2a-f798-4e8b-ae61-696302f6090b"
        },
        {
            "id": "948cd44a-219f-4aeb-adc5-bc39ddc221c1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b30d5a2a-f798-4e8b-ae61-696302f6090b"
        },
        {
            "id": "c0c4a103-507b-4dfe-b886-2844ee1a11c0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b30d5a2a-f798-4e8b-ae61-696302f6090b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}