{
    "id": "26c8828f-ee73-4d28-86a0-20ad3afe6c11",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "parProjevtileEnemyAlt",
    "eventList": [
        {
            "id": "a615312e-36ec-4600-bbb8-bfeebfe92bb0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "26c8828f-ee73-4d28-86a0-20ad3afe6c11"
        },
        {
            "id": "5a285ace-e73b-49c8-8ffa-68a0cc1f6c6b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "26c8828f-ee73-4d28-86a0-20ad3afe6c11"
        },
        {
            "id": "1a8e9540-4fd2-4f8b-9161-3935fc1d5219",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "5ab0bdeb-238d-489a-9844-fbab736cb673",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "26c8828f-ee73-4d28-86a0-20ad3afe6c11"
        },
        {
            "id": "cb610fb8-8635-4a34-be88-f09cd0f19725",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "26c8828f-ee73-4d28-86a0-20ad3afe6c11"
        },
        {
            "id": "582c72ad-8650-4200-9fcd-603a2ae480c7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 40,
            "eventtype": 7,
            "m_owner": "26c8828f-ee73-4d28-86a0-20ad3afe6c11"
        },
        {
            "id": "77637043-7400-4cca-9414-dd31ba6e6628",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "e181d273-45da-482e-8383-65bf4b53c74b",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "26c8828f-ee73-4d28-86a0-20ad3afe6c11"
        },
        {
            "id": "64ec703d-18b8-45a7-be0c-162154a48f4d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "26c8828f-ee73-4d28-86a0-20ad3afe6c11"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f435b0ca-46e2-4f77-bfe9-833178d31b38",
    "visible": true
}