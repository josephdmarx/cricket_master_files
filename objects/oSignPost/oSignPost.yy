{
    "id": "e048a63f-7ad7-40cd-9b5c-8f6caa1b8e04",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oSignPost",
    "eventList": [
        {
            "id": "f0ed51e8-1818-4296-b576-19c2357b55ad",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e048a63f-7ad7-40cd-9b5c-8f6caa1b8e04"
        },
        {
            "id": "07aa2d43-14ba-43f3-8529-07655250458c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e048a63f-7ad7-40cd-9b5c-8f6caa1b8e04"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "7caa83b0-4e00-4370-8ff5-46d6bcfbfe15",
    "visible": true
}