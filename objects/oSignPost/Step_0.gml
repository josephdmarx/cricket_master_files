/// @description Insert description here
// You can write your code in this editor
image_speed = 0;

if (interactable = true) and (oPlayer.canpickup = true)
{
	if (point_in_circle(oPlayer.x,oPlayer.y,x,y,64)) and (on = false)
	{
		on = true;
		image_index = 1;
		//instance_create_layer(x+16,y-40,"GUI",oBubbleTail);
		//instance_create_layer(x+16,y-40,"GUI",oEKey);
	}
	
	

	else if !(point_in_circle(oPlayer.x,oPlayer.y,x,y,64)) and (on = true)
	{
		on = false;
		image_index = 0;
		//instance_destroy(oBubbleTail);
		//instance_destroy(oEKey);
	}
}

if (on) and (oPlayer.key_interact)
{
		//if location is not added to the WorldMapManager
			//add it
			
	
		//MEADOWLANDS
		if (location = MEADOWLANDS)
		{
			if !(oWorldMapManager.meadowlands_active)
			{
					with (instance_create_layer(0,0,"GUI",oTutorialBox))
					{
							text1 = "- Meadowlands -";
							text2 =	"Has been added to your map";
							text3 = "- Meadowlands -";
							text4 =	"Has been added to your map";
					}
				oWorldMapManager.meadowlands_active = true;
				oWorldMapManager.locationsUnlocked ++;
			}
			
			else if !(instance_exists(oTutorialBox))
			{
				if !(oWorldMapManager.menu_active)
				{
					oPlayer.hascontrol = false;
					oWorldMapManager.menu_active = true;
					oWorldMapManager.atSignPost = true;
				}
			}
			
		}
		
		

		//MEADOWTOWN
		if (location = MEADOWTOWN)
		{
			if !(oWorldMapManager.meadowtown_active)
			{
					with (instance_create_layer(0,0,"GUI",oTutorialBox))
					{
							text1 = "- Meadowtown -";
							text2 =	"Has been added to your map";
							text3 = "- Meadowtown -";
							text4 =	"Has been added to your map";
					}
				oWorldMapManager.meadowtown_active = true;
				oWorldMapManager.locationsUnlocked ++;
			}
			
			else if !(instance_exists(oTutorialBox))
			{
				if !(oWorldMapManager.menu_active)
				{
					oPlayer.hascontrol = false;
					oWorldMapManager.menu_active = true;
					oWorldMapManager.atSignPost = true;
				}
			}
		}
			
		if (location = BOGCAVES)
		{
			if !(oWorldMapManager.bogcaves_active)
			{
					with (instance_create_layer(0,0,"GUI",oTutorialBox))
					{
							text1 = "- Bog Caverns -";
							text2 =	"Has been added to your map";
							text3 = "- Bog Caverns -";
							text4 =	"Has been added to your map";
					}
				oWorldMapManager.bogcaves_active = true;
				oWorldMapManager.locationsUnlocked ++;
			}
			
			else if !(instance_exists(oTutorialBox))
			{
				if !(oWorldMapManager.menu_active)
				{
					oPlayer.hascontrol = false;
					oWorldMapManager.menu_active = true;
					oWorldMapManager.atSignPost = true;
				}
			}
			
		
		}
			
		//if location is added
			//open the fast travel menu
		
		
		
		
}