/// @description Insert description here
// You can write your code in this editor

//salePrice = salePrices[page];
//saleItemName = saleItemNames[page];
//saleItemSprite = saleItemSprites[page];
//saleItemType = saleItemTypes[page];


if (page < (numberOfSaleItems - 1))
{
	page++;
}

else
{
	GetMenuInput();
	if (key_back) 
	{
		audio_play_sound(sfxMenuCancel,1,false);
		instance_destroy();	
	}
	
	if (key_accept)
	{
		//if we don't have enough money
		if (oGoldManager.gold < salePrices[activeSelection])
		{
			audio_play_sound(sfx_impDeath,1,false);
		}
		
		
		//if we have enough money
		else 
		{	
			oGoldManager.gold -= salePrices[activeSelection];
			audio_play_sound(sfxGoldPickup01,1,false);
			InvAddItem(saleItemTypes[activeSelection]);
		}
		
		
		
	}
	
	if (key_up) and (activeSelection > 0)
	{
		activeSelection --;
		audio_play_sound(sfxMenuClick,1,false);
	}
	
	if (key_down) and (activeSelection < (numberOfSaleItems -1))
	{
		activeSelection ++;
		audio_play_sound(sfxMenuClick,1,false);
	}
}


