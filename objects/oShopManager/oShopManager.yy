{
    "id": "6dbf06af-4c21-4f8a-8226-ccf2dd08259d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oShopManager",
    "eventList": [
        {
            "id": "b8a7aa08-8df9-441e-906c-9ddbae8170d9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6dbf06af-4c21-4f8a-8226-ccf2dd08259d"
        },
        {
            "id": "1cc91222-00ce-435c-baf1-fc854fbbb1e6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "6dbf06af-4c21-4f8a-8226-ccf2dd08259d"
        },
        {
            "id": "3fbf8e4b-5afa-4c6e-a313-fec9a999c0f8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "6dbf06af-4c21-4f8a-8226-ccf2dd08259d"
        },
        {
            "id": "8dac0407-5a6b-4c26-a5f5-9fe2781bdd3c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "6dbf06af-4c21-4f8a-8226-ccf2dd08259d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}