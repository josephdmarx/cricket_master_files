{
    "id": "83d08fc9-c3cd-4a25-b071-5930d4fd8196",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oSecretTrigger",
    "eventList": [
        {
            "id": "24fad2a6-1e58-4373-adf6-1821ec430138",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "5ab0bdeb-238d-489a-9844-fbab736cb673",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "83d08fc9-c3cd-4a25-b071-5930d4fd8196"
        },
        {
            "id": "ebbb127d-1849-4856-93a5-60e364609c94",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "83d08fc9-c3cd-4a25-b071-5930d4fd8196"
        },
        {
            "id": "a17b9ef7-0d59-4648-9765-a1156d7c2256",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "83d08fc9-c3cd-4a25-b071-5930d4fd8196"
        },
        {
            "id": "a0326756-5fc5-4bcc-9c99-723ae339c854",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "83d08fc9-c3cd-4a25-b071-5930d4fd8196"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "240da789-6827-442b-8776-82c6c85d75a4",
    "visible": true
}