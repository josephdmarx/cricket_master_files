{
    "id": "29a8dc91-97d4-40a8-9878-4041fab0020e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oCaveBreakWallLeft",
    "eventList": [
        {
            "id": "261ffc77-04fa-4070-aeff-94480ae3b88f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "29a8dc91-97d4-40a8-9878-4041fab0020e"
        },
        {
            "id": "6d126670-9e43-4509-a8af-5f951902104e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "3269c2bb-a7cc-47cc-84a0-19141b893294",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "29a8dc91-97d4-40a8-9878-4041fab0020e"
        },
        {
            "id": "3d74eb44-f339-45d8-b784-32bd10319d59",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "29a8dc91-97d4-40a8-9878-4041fab0020e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "29d362df-98e6-4732-8705-588429403239",
    "visible": true
}