{
    "id": "d46b742b-d4c4-4abb-93ac-d32a26dc0e75",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oDMG",
    "eventList": [
        {
            "id": "4bd0af6f-0469-46a5-a0dc-0b9596e6aaa1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d46b742b-d4c4-4abb-93ac-d32a26dc0e75"
        },
        {
            "id": "e7e2806d-7799-4f95-8dae-a926cfc35ae2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d46b742b-d4c4-4abb-93ac-d32a26dc0e75"
        },
        {
            "id": "8e9557c7-3c43-4f39-95f5-2ac54ed32952",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "d46b742b-d4c4-4abb-93ac-d32a26dc0e75"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}