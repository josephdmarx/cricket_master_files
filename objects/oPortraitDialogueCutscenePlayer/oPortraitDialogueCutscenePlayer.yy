{
    "id": "4255908d-50ad-4b29-bf28-64f5a0a83e6e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oPortraitDialogueCutscenePlayer",
    "eventList": [
        {
            "id": "561fd94a-8a5b-48c1-8a81-fd8b09eaf3a9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "4255908d-50ad-4b29-bf28-64f5a0a83e6e"
        },
        {
            "id": "d32503de-549c-4aad-b51b-92753ee9c55c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4255908d-50ad-4b29-bf28-64f5a0a83e6e"
        },
        {
            "id": "39fc825d-4380-46c6-8ab1-fb84218cf37e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "4255908d-50ad-4b29-bf28-64f5a0a83e6e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}