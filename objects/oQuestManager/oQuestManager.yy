{
    "id": "246c067f-ae26-4874-8dc9-78b30eac91b8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oQuestManager",
    "eventList": [
        {
            "id": "69e3a7a6-a54e-4e13-9789-b817443dc549",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "246c067f-ae26-4874-8dc9-78b30eac91b8"
        },
        {
            "id": "17f25d4c-18e2-4315-a980-d129b72ccd8a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "246c067f-ae26-4874-8dc9-78b30eac91b8"
        },
        {
            "id": "909ee3ac-cf8b-4f5d-b46a-ba5716432da4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "246c067f-ae26-4874-8dc9-78b30eac91b8"
        },
        {
            "id": "5d4c1ccb-da82-419a-8ba3-efd3db6621e1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "246c067f-ae26-4874-8dc9-78b30eac91b8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "432d7032-d117-44ed-910f-1cb4d16b538f",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}