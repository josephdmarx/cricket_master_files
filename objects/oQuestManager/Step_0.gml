// Quest Architecture
var grid = ds_quests;  //this grid represents the data structure we have created with all our quests
var i = 0;  //our position on the grid (VERTICALLY - different quests)

//show_debug_message("MEADOWTOWN STAGE:");
//show_debug_message(ds_quests[# 1, QUEST.meadow_town]);
//show_debug_message("FROGS STAGE:");
//show_debug_message(ds_quests[# 1, QUEST.frog_collecting]);
//show_debug_message("APPLES STAGE:");
//show_debug_message(ds_quests[# 1, QUEST.collect_apples]);

repeat(ds_quests_number)  //repeat this check once for each number of quests determined by the height of our grid

{
	switch(i)
		{
		
			#region Meet Edana
			case QUEST.meet_edana:
		
				switch(grid[# 1, i])
				{
					case -1: break;
					case 0:
					{
						if (oPlayer.MQstage == 2)
						{
							grid[# 1, i] = -1;  //complete this quest
						}
					
						break;
					}
				}
		
			break;
			#endregion
		
			#region Collect Flowers
			case QUEST.collect_flowers:
				{
					
					
					switch(grid[# 1, i])
					{
						case -1: break;
						case 0:  //objective: gather the flowers
						{
								var _flowers = oInventoryManager.itemDefinitions[itemtype.coneflower, itemproperties.amount];
								
							 	if (_flowers >= 3)  //check inventory for at least 3 coneflowers
								{
									grid[# 1, i] += 1; //advance the quest
									oPlayer.MQstage = 3;
								}
					
						break;
						}
				
						case 1:  //objective: bring the flowers to Edana
						{
								var _flowers = oInventoryManager.itemDefinitions[itemtype.coneflower, itemproperties.amount];
							
								if (_flowers < 3)  // Less than 3 flowers?
								{
									grid[# 1, i] -= 1; //regress the Quest
									oPlayer.MQstage = 2;
								}
								
						break;
						}
						
				
						case 2:  //objective = return to Edana again after putting them in the cauldron
						{
								grid[# 1, i] += 1; //advance the Quest
								with (oInventoryManager)
								{
									InvRemoveItem(itemtype.coneflower);
									InvRemoveItem(itemtype.coneflower);
									InvRemoveItem(itemtype.coneflower);
								}
						break;								
						}
						
						case 3:
						{
								oPlayer.MQstage = 4;
						break;		
						}
						
					}
				break;
				}
			#endregion
					
			#region Apple Collecting
			case QUEST.collect_apples:
				{
					switch(grid[# 1, i])
					{
						case -1: break;
						case 0:  //objective: gather the flowers
						{
							 var _apples = oInventoryManager.itemDefinitions[itemtype.redapple, itemproperties.amount];
							 	if (_apples >= 3)  //check inventory for at least 3 coneflowers
								{
									grid[# 1, i] += 1; //advance the quest
									oPlayer.MQstage = 9;
								}
					
						break;
						}
				
						case 1:  //objective: bring the flowers to Edana
						{
					
							var _apples = oInventoryManager.itemDefinitions[itemtype.redapple, itemproperties.amount];
								if (_apples < 3)  // Less than 3 flowers?
								{
									grid[# 1, i] -= 1; //regress the Quest
									oPlayer.MQstage = 8;
								}
								
						break;
						}
						
				
						case 2:  //objective = return to Edana again after putting them in the cauldron
						{
								oPlayer.MQstage = 10;
								
						break;								
						}
						
					}
				break;
				}


			
			
			
			#endregion
					
			#region Frog Collecting
			
			case QUEST.frog_collecting:
				{
					switch(grid[# 1, i])
					{
						case -1: break;
						case 0:  //objective: gather the flowers
						{
							 var _frogs = oInventoryManager.itemDefinitions[itemtype.greenfrog, itemproperties.amount];
							 	if (_frogs >= 1)  //check inventory for at least 3 coneflowers
								{
									grid[# 1, i] += 1; //advance the quest
									oPlayer.MQstage = 6;
								}
					
						break;
						}
				
						case 1:  //objective: bring the flowers to Edana
						{
					
							var _frogs = oInventoryManager.itemDefinitions[itemtype.greenfrog, itemproperties.amount];
								if (_frogs < 1)  // Less than 3 flowers?
								{
									grid[# 1, i] -= 1; //regress the Quest
									oPlayer.MQstage = 5;
								}
								
						break;
						}
						
				
						case 2:  //objective = return to Edana again after putting them in the cauldron
						{
								grid[# 1, i] += 1; //advance the Quest
								with (oInventoryManager)
								{
									oQuestManager.froginbox = true;
									instance_create_layer(291, 449,"NPCs", oFrogBox);
									InvRemoveItem(itemtype.greenfrog);
								}
								
								
						break;								
						}
						case 3:
						{
							oPlayer.MQstage = 7;
							
						break;
						}
						
					}
				break;
				}



			#endregion
			
			#region MEADOW TOWN
			
			case QUEST.meadow_town:
				{
					switch(grid[# 1, i])
					{
						case -1: 
						break;
						
						case 0:  //objective: go to Meadowtown
						{
							if (room = rmMeadowTown01)
								grid[# 1, i] += 1;
						}
						break;
						
						case 1: //objective: find the Mayor
						break;
						
						break;
						
						case 2: 
						break;
						
						case 3: 
						break;
						
						case 4: 
						break;
						
						case 5:
						break;
						
						case 6:
						break;
						
						case 7:
						break;
						
						case 8:
						{
							var _ingredient = oInventoryManager.itemDefinitions[itemtype.jellycap, itemproperties.amount];
							
							if (_ingredient > 0)
								grid[# 1, i] += 1;
						}
						break;
						
						case 9:
						{
							var _ingredient = oInventoryManager.itemDefinitions[itemtype.curefever, itemproperties.amount];
							
							if (_ingredient > 0)
								grid[# 1, i] += 1;
							
						}
						break;
						
						case 10:
						break;
												
						break;
						
						
						
					}
				}
			break;
			
			#endregion
		
		}					

	
	i++;  //check the next quest
	
	}
