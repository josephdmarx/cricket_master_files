// creating data structure
//quest stage -1 means the quest is NOT runnning
//quest stage 0 means the quest is running the first entry in the array eg: Catch three frogs

enum QUEST
{
	meet_edana = 0,
	collect_flowers = 1,
	collect_apples = 2,
	frog_collecting = 3,
	meadow_town = 4,
	go_home = 5
}

conditionmet = false;
conditioncount = 0;

var quest_array = 
[
	[ "Meet Edana", -1, ["Meet Edana at her tower"] ],
	[ "Gather Flowers", -1, ["Gather three coneflowers", "Return to Edana", "Return to Edana again", "Return to Edana again"] ],
	[ "Gather Apples", -1, ["Gather three apples", "Return to Edana"] ],
	[ "Frog Collecting", -1, ["Catch a frog", "Bring the frog to Edana", "Return to Edana again", "Return to Edana again"] ],
	[ "Meadow Town", -1, ["Travel to Meadow Town", "Find the Mayor", "Talk to the Mayor", "Follow the Mayor", "Consult your Herbalism Guide", "Return to the Mayor","Find a Jellycap in the Bog Caverns", "Find the way back up to the Jellycap", "Find the way back up to the Jellycap", "Create the Cure Fever potion", "Bring the potion back to the Mayor"] ],
	[ "Go Home", -1, ["Return to Edana", "Return to Edana"] ]
];

ds_quests = create_ds_grid_from_array(quest_array);  
ds_quests_number = ds_grid_height(ds_quests);  

//Specific World Toggles
froginbox = false;