{
    "id": "b4024d9a-9f43-4fc7-9c2f-0075352aca25",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oPlayer_CS",
    "eventList": [
        {
            "id": "ed9486ba-181b-45ed-b904-69bb61b1f3f0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b4024d9a-9f43-4fc7-9c2f-0075352aca25"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "271e9ea0-f725-4c4a-9bd5-a73a4fbe6d5d",
    "visible": true
}