draw_sprite(sHealthBar, -1, 10, 30);

if !(instance_exists(oPlayer)) exit;   //if the player object isn't here, exit

drawmana = lerp(drawmana, oPlayer.mana, .5);

draw_set_alpha(1);
draw_set_color(c_blue);

if(oPlayer.mana > 0)
	{
		draw_rectangle(15, 35, 10+88 * (drawmana / oPlayer.manamax), 30+11,false);
	}
	
draw_set_color(c_white);