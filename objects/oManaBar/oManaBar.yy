{
    "id": "c5b03f64-ae18-4fe3-a28d-908a47fddbd7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oManaBar",
    "eventList": [
        {
            "id": "4c7f15f9-3e5b-476b-a519-4262ee74549d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c5b03f64-ae18-4fe3-a28d-908a47fddbd7"
        },
        {
            "id": "1004dc75-c763-46f2-b553-acf6a6337165",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "c5b03f64-ae18-4fe3-a28d-908a47fddbd7"
        },
        {
            "id": "320d90ab-e76c-46ee-8a86-66c928d8e6a6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "c5b03f64-ae18-4fe3-a28d-908a47fddbd7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "56c7c67e-c688-4c7a-8d51-1b97c339264a",
    "visible": true
}