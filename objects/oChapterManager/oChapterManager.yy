{
    "id": "3135a44f-a2cb-4593-83ed-2aed071759b1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oChapterManager",
    "eventList": [
        {
            "id": "a9d45423-5fe6-4d72-89a0-525edb0fe49e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3135a44f-a2cb-4593-83ed-2aed071759b1"
        },
        {
            "id": "f0c13bd0-b1f4-42a3-b0fd-f9b284656e93",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "3135a44f-a2cb-4593-83ed-2aed071759b1"
        },
        {
            "id": "8c6f6418-059d-4e7e-80dd-5e82a664c55c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "3135a44f-a2cb-4593-83ed-2aed071759b1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}