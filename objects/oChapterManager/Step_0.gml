/// @description Insert description here
// You can write your code in this editor

timer++;

chapter = oProgressTracker.chapter; 

#region Sound Effect

if (chapter == 0)
{
	text = "Prologue:";
	subtext = "Stars";
	sound = sfx_amb_crickets;
}

if (chapter == 1)
{
	text = "Chapter One";
	subtext = "Cricket";
	sound = noone;
}

if !(sound_isplaying)
{
	sound_isplaying = true;
	audio_play_sound(sound, 1, false);
	audio_sound_gain(sound, 0, 0);
	audio_sound_gain(sound, 1, 3000);
}

#endregion


if (timer >= timerlength)
{
	timer = 0;
	
	switch(chapter)  //switch based on the chapter we are currently IN
	{
		case 0:
			SlideTransition(TRANS_MODE.NEXT)
		break;
			
		case 1: 
			SlideTransition(TRANS_MODE.GOTO, rmMeadowLands01, 1744, 272);
		break;
			
	break;
	}
}