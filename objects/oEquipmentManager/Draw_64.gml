/// @description Insert description here
// You can write your code in this editor

if (menu_active)
{

	DrawStartMenu();
	drawMenuButtons();

	var slot = 0;  //each slot has it's own position 0-7
	var _selectorrow = 0; //row position for the selector

	draw_sprite(sInventoryLarge, -1, guiHolderPosX, guiHolderPosY)

	#region Item description

		var _eqtype = equipment[activeweapon];
		var _details = oEquipmentManager.eqDefinitions[_eqtype, WEAPONPROPERTIES.DESCRIPTION];
		var _name = oEquipmentManager.eqDefinitions[_eqtype, WEAPONPROPERTIES.NAME];
		
		draw_text_outlined(guiHolderPosX + (guiHolderPad * 2), 290, _name);
		
		draw_set_halign(fa_left);
		draw_set_color(c_white);
		draw_set_font(font04);
		draw_text_transformed((guiHolderPosX + (guiHolderPad * 2)),305,_details,.5,.5,0);

	#endregion

	//#region Draw the Equipped Weapon

	//draw_sprite(sEqFrame, -1, 470, 280);  //draws the frame
	//draw_text_outlined(520, 300, eqDefinitions[equipment[equippedweapon], WEAPONPROPERTIES.NAME]);
	//draw_sprite(eqDefinitions[equipment[equippedweapon], WEAPONPROPERTIES.SPRITE], -1, 473, 282);
	

	//#endregion

		
	if (inventory_row == 0)
	{
		draw_sprite(sSelector, -1, 
				(guiHolderPosX + guiHolderPad) + (guiHolderOffsetX * activeslot), 
				(guiHolderPosY + guiHolderPad));
	}

	else if (inventory_row > 0)
	{
		draw_sprite(sSelector, -1, 
				(guiHolderPosX + guiHolderPad) + (guiHolderOffsetX * activeslot) - (guiHolderOffsetX * (inventory_row * 12)), 
				(guiHolderPosY + guiHolderPad) + (guiHolderOffsetY * inventory_row));
	}

	
	for (var i = 0; i < MAX_ACTIVE_EQ_ITEMS; i++)
	{
		eqDefIndex = equipment[i];

		if (i <= 12)
			{
				slot = 0;
			}
		else if (i > 12)
			{
				slot = 1;
			}

		if (eqDefIndex != WEAPONTYPE.NONE) and (inventory_row == 0)
			{
				draw_sprite(eqDefinitions[eqDefIndex, WEAPONPROPERTIES.SPRITE], -1, 
							(guiHolderPosX + guiHolderPad) + (guiHolderOffsetX * i),
							(guiHolderPosY + guiHolderPad));
			}
			
		else if (eqDefIndex != WEAPONTYPE.NONE) and (inventory_row > 0 )
			{
				draw_sprite(eqDefinitions[eqDefIndex, WEAPONPROPERTIES.SPRITE], -1, 
						(guiHolderPosX + guiHolderPad) + (guiHolderOffsetX * i),
						(guiHolderPosY + guiHolderPad));		
		
		
			}
			

		
	
	}
}





	draw_sprite(sEqFrame, -1, 117, 10);  //draws the frame
	draw_sprite(eqDefinitions[equipment[equippedweapon], WEAPONPROPERTIES.SPRITE], -1, 120, 12); //draws the item
	
	if (gamepad_is_connected(0))
	{
		draw_sprite(sRightBumperPad, 0, 157, 58);		
	}
	else if !(gamepad_is_connected(0))
	{
		draw_sprite(sLKey, 0, 157, 58);
	}
	
	



