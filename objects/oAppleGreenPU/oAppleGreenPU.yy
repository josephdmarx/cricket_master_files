{
    "id": "660fadaf-8dc8-4afc-a345-58ab94f47967",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oAppleGreenPU",
    "eventList": [
        {
            "id": "0d5f1d5c-21d8-4c15-8f9e-b4e8a2f75872",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "660fadaf-8dc8-4afc-a345-58ab94f47967"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": [
        {
            "id": "dfd7a08e-8ae8-41ea-a2d9-bab9268f48b6",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "6c37065d-eb91-4300-a809-5b569349aa0c",
            "propertyId": "c4210375-1472-4e6a-bd60-cb042778825e",
            "value": "itemtype.greenapple"
        }
    ],
    "parentObjectId": "6c37065d-eb91-4300-a809-5b569349aa0c",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "4e9dfba7-3374-4df6-8645-2282b3b7ecaf",
    "visible": true
}