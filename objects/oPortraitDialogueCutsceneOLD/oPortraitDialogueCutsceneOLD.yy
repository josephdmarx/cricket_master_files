{
    "id": "df4d377d-306c-462d-93f6-60943c6b65c3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oPortraitDialogueCutsceneOLD",
    "eventList": [
        {
            "id": "39fcee6a-615a-4ba7-a47b-827cc7caac0f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "df4d377d-306c-462d-93f6-60943c6b65c3"
        },
        {
            "id": "a3bc24df-d78a-42d2-94a4-4922fdc313e8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "df4d377d-306c-462d-93f6-60943c6b65c3"
        },
        {
            "id": "1d6b9af8-5cb6-457c-9f09-cedbd92d965b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "df4d377d-306c-462d-93f6-60943c6b65c3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}