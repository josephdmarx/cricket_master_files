{
    "id": "97c278f7-efb9-4dd2-b1ef-85530dda808d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_light_torch",
    "eventList": [
        {
            "id": "b3049206-d8aa-40c9-a3f0-c395b492e444",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "97c278f7-efb9-4dd2-b1ef-85530dda808d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "554c7ae1-ce9e-4dcd-b03c-3fc06f694594",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}