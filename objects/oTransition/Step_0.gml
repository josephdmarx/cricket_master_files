/// @desc Progress the transition



if (alpha != 0) and (fade == -1)
		{
			alpha = clamp(alpha + (fade*0.02),0,1);
		}
		
if (alpha != 1) and (fade == 1)
		{
			alpha = clamp(alpha + (fade*0.02),0,1);
		}
		
//alpha = clamp(alpha + (fade*0.05),0,1);

if (mode != TRANS_MODE.OFF)
{
	//if (mode == TRANS_MODE.INTRO)
	//{
	//	percent = max(0,percent - max((percent/10),0.005));
	//}
	//else
	//{
	//	percent = min(1,percent + max(((1 - percent)/10),0.005));
	//}
	
	
		switch (mode)
		{
			case TRANS_MODE.INTRO:
			{
				mode = TRANS_MODE.OFF;
				fade = -1;
				
				if(instance_exists(oPlayer)) 
					oPlayer.hascontrol = true;
				
				
				break;
			}
			case TRANS_MODE.NEXT:
			{
				
					
					
					
						fade = 1;
					
					
					if (alpha == 1)
					{
						room_goto_next();
						mode = TRANS_MODE.INTRO;
					}
					
				
				break;
			}
			case TRANS_MODE.GOTO:
			{
				
					
					
					
						fade = 1;
						
					
					
					if (alpha == 1)
					{
						room_goto(target);
					
						if(instance_exists(oPlayer))
								{
									oPlayer.x = targetX;
									oPlayer.y = targetY;
								}
								
						mode = TRANS_MODE.INTRO;
					}
				break;
							}
			case TRANS_MODE.RESTART:
				{
					game_restart();
				}		
							
				break;
			}
		}
	

