/// @desc Size variables and mode setup
depth = -5000


display_set_gui_size(640,360);

w = display_get_gui_width();
h = display_get_gui_height();

w_half = w * 0.5;

h_half = h * 0.5;

alpha = 1;
fade = -1;


enum TRANS_MODE
{
	OFF,
	NEXT,
	GOTO,
	RESTART,
	INTRO
}

mode = TRANS_MODE.INTRO;

percent = 1;

target = room;
targetX = 0;
targetY = 0;