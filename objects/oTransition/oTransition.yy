{
    "id": "c72f5e84-4f9d-488a-ab86-e7748ad163c6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oTransition",
    "eventList": [
        {
            "id": "54c9fe36-b03a-48c8-bd85-7ffd03320762",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c72f5e84-4f9d-488a-ab86-e7748ad163c6"
        },
        {
            "id": "d904803f-5406-4b15-9466-f1e986b6d00e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c72f5e84-4f9d-488a-ab86-e7748ad163c6"
        },
        {
            "id": "2f7fa4b7-05dd-472c-acf3-ba18d5151cf6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "c72f5e84-4f9d-488a-ab86-e7748ad163c6"
        },
        {
            "id": "1bcb6f4e-b4b7-4881-9f77-0abef2d52f1f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 82,
            "eventtype": 9,
            "m_owner": "c72f5e84-4f9d-488a-ab86-e7748ad163c6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}