/// @description Insert description here
// You can write your code in this editor
if (i <= 0) and (exiting)
{
	oPlayer.TutorialStage ++;
	oPlayer.inputdelay = 10;
	instance_destroy();
}

if (i < 1) and !(exiting)
{
	oPlayer.hascontrol = false;	
	 i += 0.1;
}

if (i > 0) and (exiting)
{
	oPlayer.hascontrol = true;	
	 i -= 0.1;
}

xx1 = lerp(x1, x1_t, i);
xx2 = lerp(x2, x2_t, i);
yy1 = lerp(y1, y1_t, i);
yy2 = lerp(y2, y2_t, i);

if (timer < room_speed) and !(exiting)
{
	timer++;
}

else if (timer >= room_speed)
{
	if (keyboard_check(ord("E")) or gamepad_button_check(0, gp_face4))
	{
		audio_play_sound(sfxMenuAccept,0,false);
		timer = 0;
		exiting = true;
	}
}