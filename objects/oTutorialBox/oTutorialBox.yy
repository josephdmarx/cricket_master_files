{
    "id": "20357f6c-6542-4567-af7f-d84d81d3f575",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oTutorialBox",
    "eventList": [
        {
            "id": "4cd7460d-5ad3-4122-8550-a8ebaf94c483",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "20357f6c-6542-4567-af7f-d84d81d3f575"
        },
        {
            "id": "ba099161-1d2d-43bb-a2ac-28f0c0824e25",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "20357f6c-6542-4567-af7f-d84d81d3f575"
        },
        {
            "id": "4216bb38-83a8-4034-863c-c5e9d6976c53",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "20357f6c-6542-4567-af7f-d84d81d3f575"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}