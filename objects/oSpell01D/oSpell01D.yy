{
    "id": "9efb4aa8-8cd5-4cdb-9958-c81d3f7dc7c2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oSpell01D",
    "eventList": [
        {
            "id": "712c5d01-c584-46f3-9059-18120091ae4b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9efb4aa8-8cd5-4cdb-9958-c81d3f7dc7c2"
        },
        {
            "id": "3030400a-d007-4b67-b0ab-77e06b476b61",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "9efb4aa8-8cd5-4cdb-9958-c81d3f7dc7c2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "68d563a8-4037-46ac-9540-d7a2f6983d15",
    "visible": true
}