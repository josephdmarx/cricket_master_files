{
    "id": "beae981b-2b3d-4e31-a2d7-179e2c760a73",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayerD",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 94,
    "bbox_left": 58,
    "bbox_right": 72,
    "bbox_top": 42,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "44b5adbe-e5fa-45de-9ea6-3a22a2e5cba5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "beae981b-2b3d-4e31-a2d7-179e2c760a73",
            "compositeImage": {
                "id": "bbf1f90b-2459-4413-8bd9-25f52a07f5cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "44b5adbe-e5fa-45de-9ea6-3a22a2e5cba5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9d54b42-6c85-4912-829c-86d0a76464e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "44b5adbe-e5fa-45de-9ea6-3a22a2e5cba5",
                    "LayerId": "9cfcadc5-c7d8-48c0-a123-274404fde9a8"
                }
            ]
        },
        {
            "id": "9592e659-63f6-4eef-914b-839221785e8b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "beae981b-2b3d-4e31-a2d7-179e2c760a73",
            "compositeImage": {
                "id": "a9227431-7a52-49bb-9b70-6762bab9b012",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9592e659-63f6-4eef-914b-839221785e8b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b89fb125-e3e0-45ae-8357-68a7e22a57f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9592e659-63f6-4eef-914b-839221785e8b",
                    "LayerId": "9cfcadc5-c7d8-48c0-a123-274404fde9a8"
                }
            ]
        },
        {
            "id": "4d2fe025-7def-4119-b169-90531774685f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "beae981b-2b3d-4e31-a2d7-179e2c760a73",
            "compositeImage": {
                "id": "21474ce8-b550-4c8d-8be6-1cae787526c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d2fe025-7def-4119-b169-90531774685f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bca932fd-c527-46f4-8aa1-bb2cfb9ec2cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d2fe025-7def-4119-b169-90531774685f",
                    "LayerId": "9cfcadc5-c7d8-48c0-a123-274404fde9a8"
                }
            ]
        },
        {
            "id": "28fa7376-f736-4cff-a21e-b93cbe3c7ec2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "beae981b-2b3d-4e31-a2d7-179e2c760a73",
            "compositeImage": {
                "id": "3193caae-9d30-484d-8669-5e8ebd965b14",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "28fa7376-f736-4cff-a21e-b93cbe3c7ec2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51794a66-12e9-442e-88b3-1913fce7e2d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "28fa7376-f736-4cff-a21e-b93cbe3c7ec2",
                    "LayerId": "9cfcadc5-c7d8-48c0-a123-274404fde9a8"
                }
            ]
        },
        {
            "id": "4942da24-35d6-4c2c-86ef-0e09fb457646",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "beae981b-2b3d-4e31-a2d7-179e2c760a73",
            "compositeImage": {
                "id": "1d28c72e-d502-44d2-a3f4-5167dba86bc8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4942da24-35d6-4c2c-86ef-0e09fb457646",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "400b9af8-113d-497b-9169-72766e9a4ec1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4942da24-35d6-4c2c-86ef-0e09fb457646",
                    "LayerId": "9cfcadc5-c7d8-48c0-a123-274404fde9a8"
                }
            ]
        },
        {
            "id": "f12911ed-16d9-4a77-a98f-f3c63413590e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "beae981b-2b3d-4e31-a2d7-179e2c760a73",
            "compositeImage": {
                "id": "8f565970-752d-495c-abe0-db8bea1402cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f12911ed-16d9-4a77-a98f-f3c63413590e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "195a8f92-448f-47af-9a8f-0a32e4ea0893",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f12911ed-16d9-4a77-a98f-f3c63413590e",
                    "LayerId": "9cfcadc5-c7d8-48c0-a123-274404fde9a8"
                }
            ]
        },
        {
            "id": "c600c014-186c-4169-a076-5a3920e970a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "beae981b-2b3d-4e31-a2d7-179e2c760a73",
            "compositeImage": {
                "id": "dd2744d4-1c54-4b1e-8fe8-22fd7eac24d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c600c014-186c-4169-a076-5a3920e970a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c4d6e8c5-1232-433b-842f-99d36cd36dda",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c600c014-186c-4169-a076-5a3920e970a8",
                    "LayerId": "9cfcadc5-c7d8-48c0-a123-274404fde9a8"
                }
            ]
        },
        {
            "id": "8c1764ec-d886-4a88-a8ad-fe60e224c21b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "beae981b-2b3d-4e31-a2d7-179e2c760a73",
            "compositeImage": {
                "id": "f05a7935-0bb1-4bfc-9084-be3a31300970",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c1764ec-d886-4a88-a8ad-fe60e224c21b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cda6fdaa-2baa-49a5-8493-e6feb064cf19",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c1764ec-d886-4a88-a8ad-fe60e224c21b",
                    "LayerId": "9cfcadc5-c7d8-48c0-a123-274404fde9a8"
                }
            ]
        },
        {
            "id": "f1d270a6-998e-48fb-8953-18307eaaed3b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "beae981b-2b3d-4e31-a2d7-179e2c760a73",
            "compositeImage": {
                "id": "356b366b-55ab-404f-8f28-0ca2db536ad9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1d270a6-998e-48fb-8953-18307eaaed3b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82b2e072-3a3b-4970-9441-2d9e903962a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1d270a6-998e-48fb-8953-18307eaaed3b",
                    "LayerId": "9cfcadc5-c7d8-48c0-a123-274404fde9a8"
                }
            ]
        },
        {
            "id": "e5bdd387-c9d3-47f1-bd0f-44d672ec6958",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "beae981b-2b3d-4e31-a2d7-179e2c760a73",
            "compositeImage": {
                "id": "ae4cf4b8-6d8c-4169-831d-a2ec0d6d0c49",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5bdd387-c9d3-47f1-bd0f-44d672ec6958",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f7fcdd8-e43e-461c-b129-68b757e591bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5bdd387-c9d3-47f1-bd0f-44d672ec6958",
                    "LayerId": "9cfcadc5-c7d8-48c0-a123-274404fde9a8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "9cfcadc5-c7d8-48c0-a123-274404fde9a8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "beae981b-2b3d-4e31-a2d7-179e2c760a73",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}