{
    "id": "342749da-1d52-4c63-9e9f-75607fb3c9aa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sShambler_Still",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 198,
    "bbox_left": 75,
    "bbox_right": 155,
    "bbox_top": 110,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3a562aa5-1630-43d4-802b-606ee7eaf5d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "342749da-1d52-4c63-9e9f-75607fb3c9aa",
            "compositeImage": {
                "id": "854086f9-cdbc-4511-8082-4d0762c01714",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a562aa5-1630-43d4-802b-606ee7eaf5d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1bdc9c2b-1f29-49b8-85f4-d83206f9e0ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a562aa5-1630-43d4-802b-606ee7eaf5d2",
                    "LayerId": "f1b0fc9a-e4f4-4ff2-bce6-05b8a921be4a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 215,
    "layers": [
        {
            "id": "f1b0fc9a-e4f4-4ff2-bce6-05b8a921be4a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "342749da-1d52-4c63-9e9f-75607fb3c9aa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 258,
    "xorig": 129,
    "yorig": 107
}