{
    "id": "bff59029-5b08-4e8d-bd8b-3103e7f47140",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTriggerPerm",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "801baeb8-d1f4-49f9-b75f-f4ebbc170286",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bff59029-5b08-4e8d-bd8b-3103e7f47140",
            "compositeImage": {
                "id": "db190f40-e688-41f7-9f79-51a0857c2a12",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "801baeb8-d1f4-49f9-b75f-f4ebbc170286",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bde215cd-6a06-43cd-b84d-5fc7922fa0c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "801baeb8-d1f4-49f9-b75f-f4ebbc170286",
                    "LayerId": "bc796032-583a-4039-be39-b689329191a0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "bc796032-583a-4039-be39-b689329191a0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bff59029-5b08-4e8d-bd8b-3103e7f47140",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}