{
    "id": "87bdf8fc-787f-421e-a41d-d8a03230bed7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBed",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 5,
    "bbox_right": 91,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d8399cf3-3fa9-4a70-b626-77d2b749abf4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "87bdf8fc-787f-421e-a41d-d8a03230bed7",
            "compositeImage": {
                "id": "94099e37-78a9-4f7d-83ca-c80c1d0fd071",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8399cf3-3fa9-4a70-b626-77d2b749abf4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "175dff68-c0cd-4df0-83c0-cb251f1fd410",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8399cf3-3fa9-4a70-b626-77d2b749abf4",
                    "LayerId": "75bf2bfe-0970-4d16-884e-62f447d40074"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "75bf2bfe-0970-4d16-884e-62f447d40074",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "87bdf8fc-787f-421e-a41d-d8a03230bed7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 5,
    "yorig": 47
}