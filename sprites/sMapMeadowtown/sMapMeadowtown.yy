{
    "id": "5cd2597b-5eac-48c0-913f-ddbc078ccfe6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sMapMeadowtown",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 286,
    "bbox_left": 179,
    "bbox_right": 310,
    "bbox_top": 239,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c6556a20-a24c-462a-8e21-615bdef09b51",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5cd2597b-5eac-48c0-913f-ddbc078ccfe6",
            "compositeImage": {
                "id": "9580041d-0d1d-4a0e-85c8-409fbbf142d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6556a20-a24c-462a-8e21-615bdef09b51",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f1a169e7-b0a7-4c68-9d54-aef9a7a3c6de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6556a20-a24c-462a-8e21-615bdef09b51",
                    "LayerId": "f48da54f-8a44-4297-b629-b39128a23532"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 360,
    "layers": [
        {
            "id": "f48da54f-8a44-4297-b629-b39128a23532",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5cd2597b-5eac-48c0-913f-ddbc078ccfe6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 0,
    "yorig": 0
}