{
    "id": "34f8bb68-0dd8-461d-ab3c-684320fad985",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sFireBurn",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 67,
    "bbox_left": 0,
    "bbox_right": 80,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "84facc02-4a15-4bed-a12e-75110ddba640",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34f8bb68-0dd8-461d-ab3c-684320fad985",
            "compositeImage": {
                "id": "71338bac-bbf7-441e-9a6a-cdf5f92be59e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84facc02-4a15-4bed-a12e-75110ddba640",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f8268db-e8be-44a7-801d-ed1e7ce028d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84facc02-4a15-4bed-a12e-75110ddba640",
                    "LayerId": "01b48ac0-4028-46cd-8459-96a615e984dd"
                }
            ]
        },
        {
            "id": "d58f5d73-13b1-4896-b2b6-dbe10ed067c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34f8bb68-0dd8-461d-ab3c-684320fad985",
            "compositeImage": {
                "id": "6ea85da6-43d2-4458-b95d-77434c9c537a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d58f5d73-13b1-4896-b2b6-dbe10ed067c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "acd3ecc7-9dd6-499e-91a5-dd87990667f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d58f5d73-13b1-4896-b2b6-dbe10ed067c0",
                    "LayerId": "01b48ac0-4028-46cd-8459-96a615e984dd"
                }
            ]
        },
        {
            "id": "b9516d28-60e2-4d81-9307-879915c927a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34f8bb68-0dd8-461d-ab3c-684320fad985",
            "compositeImage": {
                "id": "06fa6b78-cebe-4a2b-b730-effd91b637a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b9516d28-60e2-4d81-9307-879915c927a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f57450bb-b378-4d56-9a23-3c3f0b822bea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9516d28-60e2-4d81-9307-879915c927a8",
                    "LayerId": "01b48ac0-4028-46cd-8459-96a615e984dd"
                }
            ]
        },
        {
            "id": "f8eca0f9-a782-4b98-8077-a0e3dcea3090",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34f8bb68-0dd8-461d-ab3c-684320fad985",
            "compositeImage": {
                "id": "b346707a-b0fd-4fbc-9b4a-1d9025212608",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f8eca0f9-a782-4b98-8077-a0e3dcea3090",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "06b78813-9009-4ae2-a216-2faba78be639",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f8eca0f9-a782-4b98-8077-a0e3dcea3090",
                    "LayerId": "01b48ac0-4028-46cd-8459-96a615e984dd"
                }
            ]
        },
        {
            "id": "070c57ff-7e31-4e15-9709-6f9bd216c942",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34f8bb68-0dd8-461d-ab3c-684320fad985",
            "compositeImage": {
                "id": "fc9bf30c-06ad-40ab-b5b6-2ccc1d1be611",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "070c57ff-7e31-4e15-9709-6f9bd216c942",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9079e742-4ee7-4674-9371-6e2d130133ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "070c57ff-7e31-4e15-9709-6f9bd216c942",
                    "LayerId": "01b48ac0-4028-46cd-8459-96a615e984dd"
                }
            ]
        },
        {
            "id": "bdd03c49-1d1b-403d-9408-449984ae5a2f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34f8bb68-0dd8-461d-ab3c-684320fad985",
            "compositeImage": {
                "id": "49b84063-4470-4a47-ac64-5ee599ef3b5e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bdd03c49-1d1b-403d-9408-449984ae5a2f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9fd6fff8-8f0a-434d-aa39-fcdd8c4dac66",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bdd03c49-1d1b-403d-9408-449984ae5a2f",
                    "LayerId": "01b48ac0-4028-46cd-8459-96a615e984dd"
                }
            ]
        },
        {
            "id": "0518ea73-f9eb-4dec-96a3-c29651882806",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34f8bb68-0dd8-461d-ab3c-684320fad985",
            "compositeImage": {
                "id": "8c001298-c4b2-4401-92d2-5a09cf2ede24",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0518ea73-f9eb-4dec-96a3-c29651882806",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d67c8ff7-f2fc-4e1f-8af8-85bcfdea7810",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0518ea73-f9eb-4dec-96a3-c29651882806",
                    "LayerId": "01b48ac0-4028-46cd-8459-96a615e984dd"
                }
            ]
        },
        {
            "id": "8dbdafd0-0cd9-419f-ba9d-5c077e47d86d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34f8bb68-0dd8-461d-ab3c-684320fad985",
            "compositeImage": {
                "id": "0cd3be50-b251-42bd-8f8d-68ef9be491b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8dbdafd0-0cd9-419f-ba9d-5c077e47d86d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "810f0706-0c34-42f2-9e10-f250af87c4ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8dbdafd0-0cd9-419f-ba9d-5c077e47d86d",
                    "LayerId": "01b48ac0-4028-46cd-8459-96a615e984dd"
                }
            ]
        },
        {
            "id": "bac9aa85-6c8b-4c01-96f7-317366d6d202",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34f8bb68-0dd8-461d-ab3c-684320fad985",
            "compositeImage": {
                "id": "213699b0-b260-4ba9-a547-f8500ca0bc0e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bac9aa85-6c8b-4c01-96f7-317366d6d202",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1359b925-cf1d-4f90-bdb2-49575ad19cb2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bac9aa85-6c8b-4c01-96f7-317366d6d202",
                    "LayerId": "01b48ac0-4028-46cd-8459-96a615e984dd"
                }
            ]
        },
        {
            "id": "c2b10a3c-b9f4-4fb8-ae1f-674fccc01a05",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34f8bb68-0dd8-461d-ab3c-684320fad985",
            "compositeImage": {
                "id": "34055546-6725-4684-a9e9-fb2239685832",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2b10a3c-b9f4-4fb8-ae1f-674fccc01a05",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bbcd7bab-8bc8-4315-993c-61cb4ba14dbd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2b10a3c-b9f4-4fb8-ae1f-674fccc01a05",
                    "LayerId": "01b48ac0-4028-46cd-8459-96a615e984dd"
                }
            ]
        },
        {
            "id": "59c2fb8a-2e6e-4282-a777-ce1e7b58f2d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34f8bb68-0dd8-461d-ab3c-684320fad985",
            "compositeImage": {
                "id": "bd8ea5fc-8b88-49b0-871e-f428c7b6417e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59c2fb8a-2e6e-4282-a777-ce1e7b58f2d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32320077-5ea7-4202-bc89-9ece9b2c63f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59c2fb8a-2e6e-4282-a777-ce1e7b58f2d8",
                    "LayerId": "01b48ac0-4028-46cd-8459-96a615e984dd"
                }
            ]
        },
        {
            "id": "71b12897-c846-4db1-b96b-5c948a9d54eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34f8bb68-0dd8-461d-ab3c-684320fad985",
            "compositeImage": {
                "id": "10cf85cb-8a67-40a5-80a0-2579b3b5d0bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "71b12897-c846-4db1-b96b-5c948a9d54eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "149148f0-c171-467e-91a7-d8df9ad15372",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71b12897-c846-4db1-b96b-5c948a9d54eb",
                    "LayerId": "01b48ac0-4028-46cd-8459-96a615e984dd"
                }
            ]
        },
        {
            "id": "fa46255c-b777-4ee2-851b-70a40df09fbc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34f8bb68-0dd8-461d-ab3c-684320fad985",
            "compositeImage": {
                "id": "5d275be7-794f-4115-89f5-dbbdb6321861",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa46255c-b777-4ee2-851b-70a40df09fbc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b6a3f9fb-dace-4786-b201-5fb47b338107",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa46255c-b777-4ee2-851b-70a40df09fbc",
                    "LayerId": "01b48ac0-4028-46cd-8459-96a615e984dd"
                }
            ]
        },
        {
            "id": "4d16db61-1ddc-4957-a11f-ddfe8b48471e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34f8bb68-0dd8-461d-ab3c-684320fad985",
            "compositeImage": {
                "id": "fd9db5af-9b4d-47ff-9fd8-13cfd5fb5352",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d16db61-1ddc-4957-a11f-ddfe8b48471e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5bb130d5-7ea6-454d-a438-0b9c8f3e8c7b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d16db61-1ddc-4957-a11f-ddfe8b48471e",
                    "LayerId": "01b48ac0-4028-46cd-8459-96a615e984dd"
                }
            ]
        },
        {
            "id": "dda13324-cc29-4216-ade4-9ae8427c21dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34f8bb68-0dd8-461d-ab3c-684320fad985",
            "compositeImage": {
                "id": "5c3a3ac8-3f84-4e25-a854-ac7ef84dbfc8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dda13324-cc29-4216-ade4-9ae8427c21dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "173b6654-c275-4a41-88c6-5002c54ba065",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dda13324-cc29-4216-ade4-9ae8427c21dc",
                    "LayerId": "01b48ac0-4028-46cd-8459-96a615e984dd"
                }
            ]
        },
        {
            "id": "1e4f224b-c960-48d0-8267-82511408dc50",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34f8bb68-0dd8-461d-ab3c-684320fad985",
            "compositeImage": {
                "id": "546a93d0-98cf-40c3-b36e-2c9af378734c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e4f224b-c960-48d0-8267-82511408dc50",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "03147638-adb0-4fff-a006-004b0732e089",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e4f224b-c960-48d0-8267-82511408dc50",
                    "LayerId": "01b48ac0-4028-46cd-8459-96a615e984dd"
                }
            ]
        },
        {
            "id": "2136aef3-e106-485c-9554-9b3b7e360651",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34f8bb68-0dd8-461d-ab3c-684320fad985",
            "compositeImage": {
                "id": "5cc517e2-f61b-4e8d-88cd-6c577f429a1f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2136aef3-e106-485c-9554-9b3b7e360651",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "97bd52fd-d223-4a50-b39b-d7ceb22b53f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2136aef3-e106-485c-9554-9b3b7e360651",
                    "LayerId": "01b48ac0-4028-46cd-8459-96a615e984dd"
                }
            ]
        },
        {
            "id": "8a1067d4-09f0-4706-bf67-a034c6cf783f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34f8bb68-0dd8-461d-ab3c-684320fad985",
            "compositeImage": {
                "id": "e301a4f3-4890-4e89-b0a4-673822471bea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a1067d4-09f0-4706-bf67-a034c6cf783f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "990a81ea-7a7a-46e3-8894-4be0c636738c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a1067d4-09f0-4706-bf67-a034c6cf783f",
                    "LayerId": "01b48ac0-4028-46cd-8459-96a615e984dd"
                }
            ]
        },
        {
            "id": "f051d3ed-bc28-4a8f-aa78-9f640fa79b5c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34f8bb68-0dd8-461d-ab3c-684320fad985",
            "compositeImage": {
                "id": "10ffb93b-423a-475e-a92b-6ccd5fc2c869",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f051d3ed-bc28-4a8f-aa78-9f640fa79b5c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f4518542-e71c-452a-9920-81302fdcbd45",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f051d3ed-bc28-4a8f-aa78-9f640fa79b5c",
                    "LayerId": "01b48ac0-4028-46cd-8459-96a615e984dd"
                }
            ]
        },
        {
            "id": "6863ec5b-45ac-477d-a5b5-01c8e742fab0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34f8bb68-0dd8-461d-ab3c-684320fad985",
            "compositeImage": {
                "id": "5a3acd88-528f-4148-af0d-f125d1bc35c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6863ec5b-45ac-477d-a5b5-01c8e742fab0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6cc7e33c-718e-4d13-a793-4900a740a955",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6863ec5b-45ac-477d-a5b5-01c8e742fab0",
                    "LayerId": "01b48ac0-4028-46cd-8459-96a615e984dd"
                }
            ]
        },
        {
            "id": "2c37d839-93de-4bc1-a596-10c93b7feb68",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34f8bb68-0dd8-461d-ab3c-684320fad985",
            "compositeImage": {
                "id": "2d538907-c2f2-4973-953c-6dad374ab02b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c37d839-93de-4bc1-a596-10c93b7feb68",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "234b397d-5905-4309-918c-3586eb06b2ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c37d839-93de-4bc1-a596-10c93b7feb68",
                    "LayerId": "01b48ac0-4028-46cd-8459-96a615e984dd"
                }
            ]
        },
        {
            "id": "c5c9772f-8d1c-4350-a9dc-fb83d02edb6f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34f8bb68-0dd8-461d-ab3c-684320fad985",
            "compositeImage": {
                "id": "c1f1b6da-cbbe-4a5a-a387-0c48f859a2f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5c9772f-8d1c-4350-a9dc-fb83d02edb6f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a397f769-d87c-4ba2-b46d-75908814adfa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5c9772f-8d1c-4350-a9dc-fb83d02edb6f",
                    "LayerId": "01b48ac0-4028-46cd-8459-96a615e984dd"
                }
            ]
        },
        {
            "id": "2cacaf45-0b0a-407a-a5e1-8a93f45198df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34f8bb68-0dd8-461d-ab3c-684320fad985",
            "compositeImage": {
                "id": "8039b3ac-cb27-4c1c-a30c-da55ba670a78",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2cacaf45-0b0a-407a-a5e1-8a93f45198df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ebe9340-9aaa-4392-9f8c-bde2ac41876b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2cacaf45-0b0a-407a-a5e1-8a93f45198df",
                    "LayerId": "01b48ac0-4028-46cd-8459-96a615e984dd"
                }
            ]
        },
        {
            "id": "91822e56-240e-456e-a70e-825c207dceea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34f8bb68-0dd8-461d-ab3c-684320fad985",
            "compositeImage": {
                "id": "01c77f69-ce52-4204-8db8-4cb32af59fef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91822e56-240e-456e-a70e-825c207dceea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13634bd5-fae3-43d7-a487-d095d4d3b20a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91822e56-240e-456e-a70e-825c207dceea",
                    "LayerId": "01b48ac0-4028-46cd-8459-96a615e984dd"
                }
            ]
        },
        {
            "id": "40b164ef-def0-4af5-afad-32bcbc79f43e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34f8bb68-0dd8-461d-ab3c-684320fad985",
            "compositeImage": {
                "id": "bff7cd61-ecf9-46a5-8067-54220e88ab59",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "40b164ef-def0-4af5-afad-32bcbc79f43e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5bd79d0-9eba-450b-a4bf-5a9f3bd56e02",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "40b164ef-def0-4af5-afad-32bcbc79f43e",
                    "LayerId": "01b48ac0-4028-46cd-8459-96a615e984dd"
                }
            ]
        },
        {
            "id": "8d260b7c-6e97-45f3-a015-12b249d23aa8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34f8bb68-0dd8-461d-ab3c-684320fad985",
            "compositeImage": {
                "id": "bda56afb-fde4-4162-860a-31a0210cade8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d260b7c-6e97-45f3-a015-12b249d23aa8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6008f4ed-205a-4e6a-9fa5-e5b9b16c5637",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d260b7c-6e97-45f3-a015-12b249d23aa8",
                    "LayerId": "01b48ac0-4028-46cd-8459-96a615e984dd"
                }
            ]
        },
        {
            "id": "2b3a4d48-5258-41ab-9d81-a06bb62b402e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34f8bb68-0dd8-461d-ab3c-684320fad985",
            "compositeImage": {
                "id": "1835ed08-0dbd-4455-9e0d-5e5df20001f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b3a4d48-5258-41ab-9d81-a06bb62b402e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "942479db-25e2-4e5d-befe-c412467f8278",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b3a4d48-5258-41ab-9d81-a06bb62b402e",
                    "LayerId": "01b48ac0-4028-46cd-8459-96a615e984dd"
                }
            ]
        },
        {
            "id": "6a45efd9-8c21-404e-838a-7b742c46cf02",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34f8bb68-0dd8-461d-ab3c-684320fad985",
            "compositeImage": {
                "id": "b55bd43f-e365-4257-952a-173b27b89ea3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a45efd9-8c21-404e-838a-7b742c46cf02",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58234eae-ef68-4fb3-9708-633c12b3f7fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a45efd9-8c21-404e-838a-7b742c46cf02",
                    "LayerId": "01b48ac0-4028-46cd-8459-96a615e984dd"
                }
            ]
        },
        {
            "id": "5e43b241-f4a6-4d24-a4a4-195441a3a474",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34f8bb68-0dd8-461d-ab3c-684320fad985",
            "compositeImage": {
                "id": "e05b4599-2d23-4a6b-b5d3-c12b3ef0cf97",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e43b241-f4a6-4d24-a4a4-195441a3a474",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33bb423d-dd1b-406f-bf01-f5dd9239e771",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e43b241-f4a6-4d24-a4a4-195441a3a474",
                    "LayerId": "01b48ac0-4028-46cd-8459-96a615e984dd"
                }
            ]
        },
        {
            "id": "8cd17175-a336-4f79-91c2-ee7147bc0c7d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34f8bb68-0dd8-461d-ab3c-684320fad985",
            "compositeImage": {
                "id": "f64cb1f9-3e6b-40e9-92ce-d4994d9ed1e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8cd17175-a336-4f79-91c2-ee7147bc0c7d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ffe44495-fd60-4576-b20d-6ea14b1390af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8cd17175-a336-4f79-91c2-ee7147bc0c7d",
                    "LayerId": "01b48ac0-4028-46cd-8459-96a615e984dd"
                }
            ]
        },
        {
            "id": "f6f63437-74d0-45e9-989e-d073e355d08f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34f8bb68-0dd8-461d-ab3c-684320fad985",
            "compositeImage": {
                "id": "561cd174-776f-4a06-9402-0a00b80ac540",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6f63437-74d0-45e9-989e-d073e355d08f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "90f57bf6-a27f-47e4-b3f3-b2a5aefb49d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6f63437-74d0-45e9-989e-d073e355d08f",
                    "LayerId": "01b48ac0-4028-46cd-8459-96a615e984dd"
                }
            ]
        },
        {
            "id": "d7f4a738-ec7a-4701-bdea-859969104367",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34f8bb68-0dd8-461d-ab3c-684320fad985",
            "compositeImage": {
                "id": "a4207235-1e06-44de-9acc-dc45e39b7873",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d7f4a738-ec7a-4701-bdea-859969104367",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20469476-0830-4af9-9762-36acfdb832b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d7f4a738-ec7a-4701-bdea-859969104367",
                    "LayerId": "01b48ac0-4028-46cd-8459-96a615e984dd"
                }
            ]
        },
        {
            "id": "f10a6219-2b30-4ec7-92ba-8bd865fa41ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34f8bb68-0dd8-461d-ab3c-684320fad985",
            "compositeImage": {
                "id": "03163831-6825-4165-9abb-26b92ddfce84",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f10a6219-2b30-4ec7-92ba-8bd865fa41ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c1acf9f-859b-4391-b1d9-c83bc5b6473c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f10a6219-2b30-4ec7-92ba-8bd865fa41ca",
                    "LayerId": "01b48ac0-4028-46cd-8459-96a615e984dd"
                }
            ]
        },
        {
            "id": "302f5c79-4642-4ade-a366-040cefe9cc08",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34f8bb68-0dd8-461d-ab3c-684320fad985",
            "compositeImage": {
                "id": "80d7e088-d6e1-41e0-87f7-b12810f3341f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "302f5c79-4642-4ade-a366-040cefe9cc08",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "96ef2d58-c335-492a-878a-20006b9bc5a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "302f5c79-4642-4ade-a366-040cefe9cc08",
                    "LayerId": "01b48ac0-4028-46cd-8459-96a615e984dd"
                }
            ]
        },
        {
            "id": "430c87ad-6d32-4208-89e1-fb3c65e8ed4f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34f8bb68-0dd8-461d-ab3c-684320fad985",
            "compositeImage": {
                "id": "b692f634-17e6-485a-a159-94e678968226",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "430c87ad-6d32-4208-89e1-fb3c65e8ed4f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5de49fe-973e-4558-8531-3462c05e7783",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "430c87ad-6d32-4208-89e1-fb3c65e8ed4f",
                    "LayerId": "01b48ac0-4028-46cd-8459-96a615e984dd"
                }
            ]
        },
        {
            "id": "103ea668-b42c-4e9a-a46e-993ef6ca5bbb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34f8bb68-0dd8-461d-ab3c-684320fad985",
            "compositeImage": {
                "id": "7d7fc426-c680-4a99-bb1b-073734eca191",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "103ea668-b42c-4e9a-a46e-993ef6ca5bbb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f4e9a812-5d10-498f-b017-85333cb7fb19",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "103ea668-b42c-4e9a-a46e-993ef6ca5bbb",
                    "LayerId": "01b48ac0-4028-46cd-8459-96a615e984dd"
                }
            ]
        },
        {
            "id": "093cd6e5-daaf-4312-b045-5c7f8c2f0094",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34f8bb68-0dd8-461d-ab3c-684320fad985",
            "compositeImage": {
                "id": "5b4c1d5e-3ad5-44a4-bf75-a7c5d0985998",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "093cd6e5-daaf-4312-b045-5c7f8c2f0094",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f55f5056-b687-4d71-b37f-4219719ca4dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "093cd6e5-daaf-4312-b045-5c7f8c2f0094",
                    "LayerId": "01b48ac0-4028-46cd-8459-96a615e984dd"
                }
            ]
        },
        {
            "id": "47873286-66cb-419f-a7ea-2a51d95b7dda",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34f8bb68-0dd8-461d-ab3c-684320fad985",
            "compositeImage": {
                "id": "8a3bb219-d428-4d38-9d89-ebf255c71453",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "47873286-66cb-419f-a7ea-2a51d95b7dda",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "907168de-4e9f-4f54-b623-b94a987aa2ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47873286-66cb-419f-a7ea-2a51d95b7dda",
                    "LayerId": "01b48ac0-4028-46cd-8459-96a615e984dd"
                }
            ]
        },
        {
            "id": "cc744d22-7130-49a5-a485-ed3abe2eeb63",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34f8bb68-0dd8-461d-ab3c-684320fad985",
            "compositeImage": {
                "id": "449a3b4a-24bd-4db6-94bf-5603815a7093",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc744d22-7130-49a5-a485-ed3abe2eeb63",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ca7f3fd-0873-4999-b4a0-297ce944bfcd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc744d22-7130-49a5-a485-ed3abe2eeb63",
                    "LayerId": "01b48ac0-4028-46cd-8459-96a615e984dd"
                }
            ]
        },
        {
            "id": "a44203e7-4a01-4de0-90ff-04bfb8d34468",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34f8bb68-0dd8-461d-ab3c-684320fad985",
            "compositeImage": {
                "id": "ce1db303-c58a-415d-88b1-72850eb6d093",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a44203e7-4a01-4de0-90ff-04bfb8d34468",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "77ef07bf-b0c8-4c69-a695-04f37b62f5bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a44203e7-4a01-4de0-90ff-04bfb8d34468",
                    "LayerId": "01b48ac0-4028-46cd-8459-96a615e984dd"
                }
            ]
        },
        {
            "id": "e09b0616-1b74-4454-a8a2-c0a5989b656f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34f8bb68-0dd8-461d-ab3c-684320fad985",
            "compositeImage": {
                "id": "84e3a5be-4be2-4b30-9d09-20e22d37ef2f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e09b0616-1b74-4454-a8a2-c0a5989b656f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3fc42070-184c-4241-9f33-03cb7f2e34fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e09b0616-1b74-4454-a8a2-c0a5989b656f",
                    "LayerId": "01b48ac0-4028-46cd-8459-96a615e984dd"
                }
            ]
        },
        {
            "id": "6ad27de1-fdc2-4aba-bf8d-df1f6ab1eb7d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34f8bb68-0dd8-461d-ab3c-684320fad985",
            "compositeImage": {
                "id": "4dfd529f-bc24-4597-88a6-600346a49cbc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ad27de1-fdc2-4aba-bf8d-df1f6ab1eb7d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc5c0975-1b5e-483a-bceb-d665cf24886c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ad27de1-fdc2-4aba-bf8d-df1f6ab1eb7d",
                    "LayerId": "01b48ac0-4028-46cd-8459-96a615e984dd"
                }
            ]
        },
        {
            "id": "fa5c80b3-fdce-44d2-b39d-f7078a6ea728",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34f8bb68-0dd8-461d-ab3c-684320fad985",
            "compositeImage": {
                "id": "d943e9f8-f18a-4a2c-83ac-d37b59fdade1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa5c80b3-fdce-44d2-b39d-f7078a6ea728",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "015fc0e1-1a0a-4912-b287-2f87166d3f7d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa5c80b3-fdce-44d2-b39d-f7078a6ea728",
                    "LayerId": "01b48ac0-4028-46cd-8459-96a615e984dd"
                }
            ]
        },
        {
            "id": "b7710bd0-7efa-4fcf-9697-25915e02efdb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34f8bb68-0dd8-461d-ab3c-684320fad985",
            "compositeImage": {
                "id": "51e5ad90-0fc0-4628-bceb-a004d1214e80",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7710bd0-7efa-4fcf-9697-25915e02efdb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "92cf6222-7ce2-4573-8db4-bf3a4152a18b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7710bd0-7efa-4fcf-9697-25915e02efdb",
                    "LayerId": "01b48ac0-4028-46cd-8459-96a615e984dd"
                }
            ]
        },
        {
            "id": "3d11a2a8-a34a-4f28-9193-3a51d6a58664",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34f8bb68-0dd8-461d-ab3c-684320fad985",
            "compositeImage": {
                "id": "1fced376-2355-414c-8984-bc3c7a80194e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d11a2a8-a34a-4f28-9193-3a51d6a58664",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8103f3e0-0ee6-4490-9bfb-1c2f9c5d0340",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d11a2a8-a34a-4f28-9193-3a51d6a58664",
                    "LayerId": "01b48ac0-4028-46cd-8459-96a615e984dd"
                }
            ]
        },
        {
            "id": "e9caef0a-a5ac-4282-a48d-e9405cff7a8d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34f8bb68-0dd8-461d-ab3c-684320fad985",
            "compositeImage": {
                "id": "220f08ed-9349-4274-a22c-a10a5770c76a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9caef0a-a5ac-4282-a48d-e9405cff7a8d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9fcfc5d4-4edb-4c1a-b80f-1d04dc426ff8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9caef0a-a5ac-4282-a48d-e9405cff7a8d",
                    "LayerId": "01b48ac0-4028-46cd-8459-96a615e984dd"
                }
            ]
        },
        {
            "id": "e6f300d5-c15e-4b3c-8d47-81ad31690330",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34f8bb68-0dd8-461d-ab3c-684320fad985",
            "compositeImage": {
                "id": "276ac244-b22a-4fc7-bea2-a641341d7f4f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6f300d5-c15e-4b3c-8d47-81ad31690330",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c69ee7e-1f40-427a-93ff-a6b5db62500c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6f300d5-c15e-4b3c-8d47-81ad31690330",
                    "LayerId": "01b48ac0-4028-46cd-8459-96a615e984dd"
                }
            ]
        },
        {
            "id": "148cddab-4cd3-4480-a5b6-6b73afc4de7d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34f8bb68-0dd8-461d-ab3c-684320fad985",
            "compositeImage": {
                "id": "19fc5a4b-ad32-489d-a09b-9d4f380673a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "148cddab-4cd3-4480-a5b6-6b73afc4de7d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "79e8be36-24e3-47c1-9d90-631d846be6b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "148cddab-4cd3-4480-a5b6-6b73afc4de7d",
                    "LayerId": "01b48ac0-4028-46cd-8459-96a615e984dd"
                }
            ]
        },
        {
            "id": "b6189f01-2ea5-43fd-8672-cdd9adc20cca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34f8bb68-0dd8-461d-ab3c-684320fad985",
            "compositeImage": {
                "id": "1ae752ad-394b-4739-a406-711d6febdbac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6189f01-2ea5-43fd-8672-cdd9adc20cca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "17c5de0b-db6c-4318-911e-945c017053ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6189f01-2ea5-43fd-8672-cdd9adc20cca",
                    "LayerId": "01b48ac0-4028-46cd-8459-96a615e984dd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "01b48ac0-4028-46cd-8459-96a615e984dd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "34f8bb68-0dd8-461d-ab3c-684320fad985",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 50,
    "yorig": 65
}