{
    "id": "ae9e0710-ee16-4944-abfa-4e9139de9d47",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sAPad",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 7,
    "bbox_right": 24,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e23309ea-8d61-499a-ad73-f5d2d06af8a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae9e0710-ee16-4944-abfa-4e9139de9d47",
            "compositeImage": {
                "id": "b481b596-4752-4388-83fb-8da48f655cda",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e23309ea-8d61-499a-ad73-f5d2d06af8a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a51f28e2-f4dc-4aa5-87ad-017ee62fe0f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e23309ea-8d61-499a-ad73-f5d2d06af8a1",
                    "LayerId": "f73100a8-9085-4278-85f9-e90b4f95fe9c"
                }
            ]
        },
        {
            "id": "290c25a7-ca4c-46b2-aa22-e5c7dcfd074f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae9e0710-ee16-4944-abfa-4e9139de9d47",
            "compositeImage": {
                "id": "60b3113b-899f-486f-8a3e-55217c86fdf7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "290c25a7-ca4c-46b2-aa22-e5c7dcfd074f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4366a8ea-8477-4421-8904-d001bf2cf28d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "290c25a7-ca4c-46b2-aa22-e5c7dcfd074f",
                    "LayerId": "f73100a8-9085-4278-85f9-e90b4f95fe9c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "f73100a8-9085-4278-85f9-e90b4f95fe9c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ae9e0710-ee16-4944-abfa-4e9139de9d47",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 15,
    "yorig": 25
}