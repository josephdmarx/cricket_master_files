{
    "id": "515d311b-ed75-44f6-b127-3c3092acd613",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sShambler_Walk",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 198,
    "bbox_left": 75,
    "bbox_right": 155,
    "bbox_top": 110,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "50619270-c985-4a70-8c5f-11b4ce270e7e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "515d311b-ed75-44f6-b127-3c3092acd613",
            "compositeImage": {
                "id": "fd52f377-2e3d-487e-a7ae-3dac9fe83577",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "50619270-c985-4a70-8c5f-11b4ce270e7e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "59807419-10a2-4b28-80b5-0c42e1d33f7e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "50619270-c985-4a70-8c5f-11b4ce270e7e",
                    "LayerId": "3ae48b87-89de-4211-a1a5-9e4f0b7ee26d"
                }
            ]
        },
        {
            "id": "5d285151-0602-4b27-8137-28b4ed0fc194",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "515d311b-ed75-44f6-b127-3c3092acd613",
            "compositeImage": {
                "id": "3abe3b02-1b31-4d0f-a596-f33bbcee3d41",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d285151-0602-4b27-8137-28b4ed0fc194",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f06d263f-dff1-463f-9877-3d83ddb1eb29",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d285151-0602-4b27-8137-28b4ed0fc194",
                    "LayerId": "3ae48b87-89de-4211-a1a5-9e4f0b7ee26d"
                }
            ]
        },
        {
            "id": "77af712c-be24-46af-b890-6ed69b5c9f16",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "515d311b-ed75-44f6-b127-3c3092acd613",
            "compositeImage": {
                "id": "a1af562c-f8be-44e5-8cfb-1069f1b73e1c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77af712c-be24-46af-b890-6ed69b5c9f16",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "814ffbb0-d0a8-4080-98f3-de1835ce6690",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77af712c-be24-46af-b890-6ed69b5c9f16",
                    "LayerId": "3ae48b87-89de-4211-a1a5-9e4f0b7ee26d"
                }
            ]
        },
        {
            "id": "6167f051-e1b3-47c9-8932-c3a8494a4986",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "515d311b-ed75-44f6-b127-3c3092acd613",
            "compositeImage": {
                "id": "d95ca428-9acc-4ce9-96c8-2d8a61d3d979",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6167f051-e1b3-47c9-8932-c3a8494a4986",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "37466a7e-5ead-48ae-99ac-3d43d324d432",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6167f051-e1b3-47c9-8932-c3a8494a4986",
                    "LayerId": "3ae48b87-89de-4211-a1a5-9e4f0b7ee26d"
                }
            ]
        },
        {
            "id": "6c3b3903-a645-405e-8bd6-2e450372db0f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "515d311b-ed75-44f6-b127-3c3092acd613",
            "compositeImage": {
                "id": "f9283db5-3180-47ad-9cfd-b2c0d6b7f842",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c3b3903-a645-405e-8bd6-2e450372db0f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e51980ca-4112-405c-bcd6-d4fa303bf51b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c3b3903-a645-405e-8bd6-2e450372db0f",
                    "LayerId": "3ae48b87-89de-4211-a1a5-9e4f0b7ee26d"
                }
            ]
        },
        {
            "id": "ccad3522-46f6-46ba-a667-0c367677e896",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "515d311b-ed75-44f6-b127-3c3092acd613",
            "compositeImage": {
                "id": "6c1592b8-ae31-4e64-8237-b41d1fd403f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ccad3522-46f6-46ba-a667-0c367677e896",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c46a364-c0ae-4e74-b165-b38ce89a3945",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ccad3522-46f6-46ba-a667-0c367677e896",
                    "LayerId": "3ae48b87-89de-4211-a1a5-9e4f0b7ee26d"
                }
            ]
        },
        {
            "id": "8520723f-a6b8-4fe0-a285-c80d90019098",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "515d311b-ed75-44f6-b127-3c3092acd613",
            "compositeImage": {
                "id": "400898c8-d7d9-4789-b59e-ef317bd09542",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8520723f-a6b8-4fe0-a285-c80d90019098",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dfd1002d-75ae-4c68-8929-8e2f86025bb6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8520723f-a6b8-4fe0-a285-c80d90019098",
                    "LayerId": "3ae48b87-89de-4211-a1a5-9e4f0b7ee26d"
                }
            ]
        },
        {
            "id": "378f81eb-6419-4ca3-ad0b-6e78d6792237",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "515d311b-ed75-44f6-b127-3c3092acd613",
            "compositeImage": {
                "id": "c55cbd2b-95ea-4eb4-95ea-f3f45ac8039b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "378f81eb-6419-4ca3-ad0b-6e78d6792237",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "36a3d4bd-b3c0-4afa-bd84-4a98d759e569",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "378f81eb-6419-4ca3-ad0b-6e78d6792237",
                    "LayerId": "3ae48b87-89de-4211-a1a5-9e4f0b7ee26d"
                }
            ]
        },
        {
            "id": "cd6f966a-ea5f-425b-9245-bb6969a1e25e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "515d311b-ed75-44f6-b127-3c3092acd613",
            "compositeImage": {
                "id": "6de70267-4c04-4589-97dd-df8afb3dd626",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd6f966a-ea5f-425b-9245-bb6969a1e25e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00246324-a938-4290-bb4c-90e99cb201f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd6f966a-ea5f-425b-9245-bb6969a1e25e",
                    "LayerId": "3ae48b87-89de-4211-a1a5-9e4f0b7ee26d"
                }
            ]
        },
        {
            "id": "e2d65771-fea6-4fce-90ef-748582159129",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "515d311b-ed75-44f6-b127-3c3092acd613",
            "compositeImage": {
                "id": "d5897bcc-7531-4643-9640-0be2e9deebe1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2d65771-fea6-4fce-90ef-748582159129",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8989ea70-1e65-4071-a14f-ee99e25a7c22",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2d65771-fea6-4fce-90ef-748582159129",
                    "LayerId": "3ae48b87-89de-4211-a1a5-9e4f0b7ee26d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 215,
    "layers": [
        {
            "id": "3ae48b87-89de-4211-a1a5-9e4f0b7ee26d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "515d311b-ed75-44f6-b127-3c3092acd613",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 258,
    "xorig": 129,
    "yorig": 107
}