{
    "id": "48e253de-c2a5-4f36-9b5d-039ae0068f94",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTrees02_dark",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 359,
    "bbox_left": 0,
    "bbox_right": 639,
    "bbox_top": 120,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9e96dff0-cdef-474c-bbd9-2015b1617ab9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48e253de-c2a5-4f36-9b5d-039ae0068f94",
            "compositeImage": {
                "id": "afdf3b69-cc19-4f8c-940a-9b4ef3ab3bd0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e96dff0-cdef-474c-bbd9-2015b1617ab9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5f261f2-d5bf-423c-a37a-ccd4922a8fa3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e96dff0-cdef-474c-bbd9-2015b1617ab9",
                    "LayerId": "60daf709-bca1-45de-a82c-a5e111e11f33"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 360,
    "layers": [
        {
            "id": "60daf709-bca1-45de-a82c-a5e111e11f33",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "48e253de-c2a5-4f36-9b5d-039ae0068f94",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 0,
    "yorig": 0
}