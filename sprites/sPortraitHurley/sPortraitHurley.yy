{
    "id": "42858ab8-fd1e-4798-9492-acbfdf774841",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPortraitHurley",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 9,
    "bbox_right": 88,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2f21347a-f810-4648-8e9a-7769b471b416",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "42858ab8-fd1e-4798-9492-acbfdf774841",
            "compositeImage": {
                "id": "a3e41d33-094e-480e-9cca-cf6d75e2db5e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f21347a-f810-4648-8e9a-7769b471b416",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "72617fba-aca5-4f13-964d-eae861598c06",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f21347a-f810-4648-8e9a-7769b471b416",
                    "LayerId": "92776ded-eee6-47a0-8094-80686e0e7ff1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "92776ded-eee6-47a0-8094-80686e0e7ff1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "42858ab8-fd1e-4798-9492-acbfdf774841",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 0,
    "yorig": 0
}