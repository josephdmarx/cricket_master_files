{
    "id": "7c87f15c-1f32-4642-af33-cab711e3015b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sAppleTree",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 149,
    "bbox_left": 15,
    "bbox_right": 108,
    "bbox_top": 20,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "37962dd6-600d-495d-822e-c33c4cf7c345",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c87f15c-1f32-4642-af33-cab711e3015b",
            "compositeImage": {
                "id": "7cbff1dd-ba11-4d46-9202-034df92ea12e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "37962dd6-600d-495d-822e-c33c4cf7c345",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f684808-5aaf-496a-8b23-f245839f3d7c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37962dd6-600d-495d-822e-c33c4cf7c345",
                    "LayerId": "a98d725b-a555-4b1c-887a-823fddcd2b9f"
                }
            ]
        },
        {
            "id": "e3e1c05e-4dea-40d0-b3a1-ef4ded9009a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c87f15c-1f32-4642-af33-cab711e3015b",
            "compositeImage": {
                "id": "0dc0e8bb-6c80-45bb-a484-1bf252ae1573",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3e1c05e-4dea-40d0-b3a1-ef4ded9009a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6cee07f9-0fc8-4ab7-80ea-9d2687ea8eac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3e1c05e-4dea-40d0-b3a1-ef4ded9009a4",
                    "LayerId": "a98d725b-a555-4b1c-887a-823fddcd2b9f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 167,
    "layers": [
        {
            "id": "a98d725b-a555-4b1c-887a-823fddcd2b9f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7c87f15c-1f32-4642-af33-cab711e3015b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 112,
    "xorig": 54,
    "yorig": 149
}