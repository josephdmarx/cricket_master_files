{
    "id": "7caa83b0-4e00-4370-8ff5-46d6bcfbfe15",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSignPost",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 52,
    "bbox_left": 10,
    "bbox_right": 53,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9ebee5cd-9ef3-4291-9506-cbd02749f64c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7caa83b0-4e00-4370-8ff5-46d6bcfbfe15",
            "compositeImage": {
                "id": "3bd10d1f-1e1a-4491-b2f5-f485f835d4be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ebee5cd-9ef3-4291-9506-cbd02749f64c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2dc57caa-e21d-4e14-9044-f312d6c36666",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ebee5cd-9ef3-4291-9506-cbd02749f64c",
                    "LayerId": "61df3aab-77af-431c-b350-e644101f7aa4"
                }
            ]
        },
        {
            "id": "b6252e84-bb64-4f75-89d0-120df60a2d88",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7caa83b0-4e00-4370-8ff5-46d6bcfbfe15",
            "compositeImage": {
                "id": "390c6bf6-53b1-4f07-a031-b7b24ec18646",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6252e84-bb64-4f75-89d0-120df60a2d88",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c7132bd-18a2-4663-aed5-a73d20c334a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6252e84-bb64-4f75-89d0-120df60a2d88",
                    "LayerId": "61df3aab-77af-431c-b350-e644101f7aa4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "61df3aab-77af-431c-b350-e644101f7aa4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7caa83b0-4e00-4370-8ff5-46d6bcfbfe15",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 31,
    "yorig": 52
}