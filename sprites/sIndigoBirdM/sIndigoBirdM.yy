{
    "id": "7ca91976-9554-4a8f-964f-9683e9547d20",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sIndigoBirdM",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 7,
    "bbox_right": 25,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c511ea32-f55b-4060-a518-94c22bb70f6e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7ca91976-9554-4a8f-964f-9683e9547d20",
            "compositeImage": {
                "id": "4b8b9f00-4877-4fb8-a9fe-c78160f9516e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c511ea32-f55b-4060-a518-94c22bb70f6e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "75917de7-c7f3-4448-8b35-3803855c3bed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c511ea32-f55b-4060-a518-94c22bb70f6e",
                    "LayerId": "d1c4ef0e-2599-49d4-a1a4-963c8ecf3105"
                }
            ]
        },
        {
            "id": "8bfb45cf-1920-4881-ae62-847adee8e025",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7ca91976-9554-4a8f-964f-9683e9547d20",
            "compositeImage": {
                "id": "1e6a6a35-908e-407b-acde-738c280e05af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8bfb45cf-1920-4881-ae62-847adee8e025",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b739e977-6e9a-4f73-954a-00b9042774ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8bfb45cf-1920-4881-ae62-847adee8e025",
                    "LayerId": "d1c4ef0e-2599-49d4-a1a4-963c8ecf3105"
                }
            ]
        },
        {
            "id": "b20512fd-2ad9-46de-83d7-43c096bbec2c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7ca91976-9554-4a8f-964f-9683e9547d20",
            "compositeImage": {
                "id": "f03d2f3d-0f96-4876-b113-b544bdbfd7fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b20512fd-2ad9-46de-83d7-43c096bbec2c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b63e4f94-ba56-49b5-8657-f58020eb71dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b20512fd-2ad9-46de-83d7-43c096bbec2c",
                    "LayerId": "d1c4ef0e-2599-49d4-a1a4-963c8ecf3105"
                }
            ]
        },
        {
            "id": "78928067-1bc8-48ab-9260-fbecf5a0163c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7ca91976-9554-4a8f-964f-9683e9547d20",
            "compositeImage": {
                "id": "4378fec3-db8a-4bb0-b65f-e480094f0a20",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78928067-1bc8-48ab-9260-fbecf5a0163c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "844d2125-cd9a-4b99-8321-29ee4b033a1a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78928067-1bc8-48ab-9260-fbecf5a0163c",
                    "LayerId": "d1c4ef0e-2599-49d4-a1a4-963c8ecf3105"
                }
            ]
        },
        {
            "id": "96bb00ed-4f90-4f14-aa06-a4f265a329db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7ca91976-9554-4a8f-964f-9683e9547d20",
            "compositeImage": {
                "id": "5f5719cb-417c-4b2b-8270-76f3c5d76b12",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96bb00ed-4f90-4f14-aa06-a4f265a329db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "16dac7f4-2454-44a4-a323-33b61b69d827",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96bb00ed-4f90-4f14-aa06-a4f265a329db",
                    "LayerId": "d1c4ef0e-2599-49d4-a1a4-963c8ecf3105"
                }
            ]
        },
        {
            "id": "baa0aa5a-3f8e-4083-8348-0b1e0a2d752d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7ca91976-9554-4a8f-964f-9683e9547d20",
            "compositeImage": {
                "id": "38c84e1d-a8a6-4827-83c1-34ed59865955",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "baa0aa5a-3f8e-4083-8348-0b1e0a2d752d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c23ad028-4422-4fe4-a999-b781aa0f7ed0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "baa0aa5a-3f8e-4083-8348-0b1e0a2d752d",
                    "LayerId": "d1c4ef0e-2599-49d4-a1a4-963c8ecf3105"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "d1c4ef0e-2599-49d4-a1a4-963c8ecf3105",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7ca91976-9554-4a8f-964f-9683e9547d20",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 15,
    "yorig": 25
}