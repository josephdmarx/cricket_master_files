{
    "id": "6da10993-05d0-42e3-a2d6-aa1e2d197fd3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSpit01D",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 8,
    "bbox_right": 23,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f2b17cd4-8b5d-4a36-9aab-72bb5980406f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6da10993-05d0-42e3-a2d6-aa1e2d197fd3",
            "compositeImage": {
                "id": "b2035dd7-30e5-4d7c-9561-794af4bc340e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f2b17cd4-8b5d-4a36-9aab-72bb5980406f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "131c2772-7b3c-4fde-90ec-52c0474283ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f2b17cd4-8b5d-4a36-9aab-72bb5980406f",
                    "LayerId": "990e89fb-b443-43dd-b175-caf145fe25fe"
                }
            ]
        },
        {
            "id": "11916fa8-e663-4848-9a91-d9ce54530c77",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6da10993-05d0-42e3-a2d6-aa1e2d197fd3",
            "compositeImage": {
                "id": "f5026cc4-6087-45ac-b02d-3876e2c49936",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "11916fa8-e663-4848-9a91-d9ce54530c77",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c43e0a0e-09c1-4dfc-878a-0e70d80718ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "11916fa8-e663-4848-9a91-d9ce54530c77",
                    "LayerId": "990e89fb-b443-43dd-b175-caf145fe25fe"
                }
            ]
        },
        {
            "id": "ff210fb3-03c1-4a15-a862-a4452a6f2ca0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6da10993-05d0-42e3-a2d6-aa1e2d197fd3",
            "compositeImage": {
                "id": "044e711c-81b2-4cd2-a2e6-1a8b8c3e75e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff210fb3-03c1-4a15-a862-a4452a6f2ca0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c61e49e9-882d-4229-802a-5a34edf0e48e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff210fb3-03c1-4a15-a862-a4452a6f2ca0",
                    "LayerId": "990e89fb-b443-43dd-b175-caf145fe25fe"
                }
            ]
        },
        {
            "id": "9c4dc4aa-b007-4081-b448-63e0a3ca0d98",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6da10993-05d0-42e3-a2d6-aa1e2d197fd3",
            "compositeImage": {
                "id": "3695feb9-c137-45bc-8157-910100f3a449",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c4dc4aa-b007-4081-b448-63e0a3ca0d98",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "255afe5a-1d1a-40f5-9db5-6d6fa16d3568",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c4dc4aa-b007-4081-b448-63e0a3ca0d98",
                    "LayerId": "990e89fb-b443-43dd-b175-caf145fe25fe"
                }
            ]
        },
        {
            "id": "7f23b506-09e8-4bf6-90d8-3bbdae285ad9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6da10993-05d0-42e3-a2d6-aa1e2d197fd3",
            "compositeImage": {
                "id": "e782d0c5-6b8a-4979-ac8a-2c250d241fc2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f23b506-09e8-4bf6-90d8-3bbdae285ad9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6fdca96e-f1e9-494a-8f81-8c5d914b0222",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f23b506-09e8-4bf6-90d8-3bbdae285ad9",
                    "LayerId": "990e89fb-b443-43dd-b175-caf145fe25fe"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "990e89fb-b443-43dd-b175-caf145fe25fe",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6da10993-05d0-42e3-a2d6-aa1e2d197fd3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}