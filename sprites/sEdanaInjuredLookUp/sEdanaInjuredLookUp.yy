{
    "id": "b19eafbd-c9dc-4866-b36b-95e44fbae5f5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sEdanaInjuredLookUp",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 110,
    "bbox_left": 35,
    "bbox_right": 79,
    "bbox_top": 23,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b0f5fa4b-0ac7-4424-ba1b-a7a67a12eda0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b19eafbd-c9dc-4866-b36b-95e44fbae5f5",
            "compositeImage": {
                "id": "a752dfa6-1db5-44d8-a0b7-e22e9d0a44e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b0f5fa4b-0ac7-4424-ba1b-a7a67a12eda0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd31a7b4-038f-439f-a864-c8a94af5d928",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0f5fa4b-0ac7-4424-ba1b-a7a67a12eda0",
                    "LayerId": "c682fa18-8c27-4380-b4b3-fe5fed1a8ecd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "c682fa18-8c27-4380-b4b3-fe5fed1a8ecd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b19eafbd-c9dc-4866-b36b-95e44fbae5f5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}