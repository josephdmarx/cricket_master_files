{
    "id": "8960f105-8042-421f-8dc6-b96642a685b9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sFloatyMotes",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 283,
    "bbox_left": 0,
    "bbox_right": 303,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fffd3291-598f-4862-9083-1fe61d748127",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8960f105-8042-421f-8dc6-b96642a685b9",
            "compositeImage": {
                "id": "2c9d30a0-7faf-4161-8cc9-8d972377ac1f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fffd3291-598f-4862-9083-1fe61d748127",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eab89e38-573f-4ddd-8752-4dcbde35bb67",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fffd3291-598f-4862-9083-1fe61d748127",
                    "LayerId": "403c5345-b65f-4b6c-83ba-dddd34db0650"
                }
            ]
        },
        {
            "id": "32de7873-ff12-44bc-8284-878b736303b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8960f105-8042-421f-8dc6-b96642a685b9",
            "compositeImage": {
                "id": "2615660f-1775-4174-9ea1-11f85615f274",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "32de7873-ff12-44bc-8284-878b736303b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "add9162a-c333-4df1-86f6-41de7d87d151",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32de7873-ff12-44bc-8284-878b736303b0",
                    "LayerId": "403c5345-b65f-4b6c-83ba-dddd34db0650"
                }
            ]
        },
        {
            "id": "8dc2aed1-7579-4f7d-9271-cbe210f824ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8960f105-8042-421f-8dc6-b96642a685b9",
            "compositeImage": {
                "id": "eda57baa-c257-4ce1-9b12-b1f84f4080eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8dc2aed1-7579-4f7d-9271-cbe210f824ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9a779d3-289d-4f56-bf63-b63d6aab6da5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8dc2aed1-7579-4f7d-9271-cbe210f824ac",
                    "LayerId": "403c5345-b65f-4b6c-83ba-dddd34db0650"
                }
            ]
        },
        {
            "id": "a35985c2-9124-48ef-8e10-9649b8234815",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8960f105-8042-421f-8dc6-b96642a685b9",
            "compositeImage": {
                "id": "89586085-1e52-4aa8-b850-03e318006073",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a35985c2-9124-48ef-8e10-9649b8234815",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ef5a8d6-afeb-4fc1-9640-5d70d808f61f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a35985c2-9124-48ef-8e10-9649b8234815",
                    "LayerId": "403c5345-b65f-4b6c-83ba-dddd34db0650"
                }
            ]
        },
        {
            "id": "10766740-a164-41b2-a172-61d07250d811",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8960f105-8042-421f-8dc6-b96642a685b9",
            "compositeImage": {
                "id": "a2691948-42b5-4e67-b779-c6955cbeb5bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10766740-a164-41b2-a172-61d07250d811",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e56484a-4989-4a5a-9e8a-396368da0f42",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10766740-a164-41b2-a172-61d07250d811",
                    "LayerId": "403c5345-b65f-4b6c-83ba-dddd34db0650"
                }
            ]
        },
        {
            "id": "fd300b45-3b5c-42cf-9375-6ce86fc6b703",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8960f105-8042-421f-8dc6-b96642a685b9",
            "compositeImage": {
                "id": "a0b84f9d-f020-4286-933e-f860ac4eb488",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd300b45-3b5c-42cf-9375-6ce86fc6b703",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fcb26114-ac4f-41cf-83f5-c49da4cafefc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd300b45-3b5c-42cf-9375-6ce86fc6b703",
                    "LayerId": "403c5345-b65f-4b6c-83ba-dddd34db0650"
                }
            ]
        },
        {
            "id": "f38d3c09-5ff7-41ee-9a79-30aa0d3c3f38",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8960f105-8042-421f-8dc6-b96642a685b9",
            "compositeImage": {
                "id": "f2c7c05b-7285-47db-8eb5-699456cce166",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f38d3c09-5ff7-41ee-9a79-30aa0d3c3f38",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a71512b5-3526-4320-ad90-21441d24f41c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f38d3c09-5ff7-41ee-9a79-30aa0d3c3f38",
                    "LayerId": "403c5345-b65f-4b6c-83ba-dddd34db0650"
                }
            ]
        },
        {
            "id": "22d24146-db7e-4767-9a4f-13147a47f378",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8960f105-8042-421f-8dc6-b96642a685b9",
            "compositeImage": {
                "id": "8617ae8a-a52e-4438-9277-3b647c701083",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22d24146-db7e-4767-9a4f-13147a47f378",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a1a18f7-3aba-401a-bbd7-f9e14af34be3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22d24146-db7e-4767-9a4f-13147a47f378",
                    "LayerId": "403c5345-b65f-4b6c-83ba-dddd34db0650"
                }
            ]
        },
        {
            "id": "a65f9155-223a-40e1-b3aa-0d61990522d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8960f105-8042-421f-8dc6-b96642a685b9",
            "compositeImage": {
                "id": "91e6e1ec-b3e6-437e-95eb-8b2db3a6d07b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a65f9155-223a-40e1-b3aa-0d61990522d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc7bb644-7d02-46dd-9677-72180dfbd491",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a65f9155-223a-40e1-b3aa-0d61990522d0",
                    "LayerId": "403c5345-b65f-4b6c-83ba-dddd34db0650"
                }
            ]
        },
        {
            "id": "2acffec1-7eff-4fb8-92f4-cbc2a8a71490",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8960f105-8042-421f-8dc6-b96642a685b9",
            "compositeImage": {
                "id": "89a724ae-36d7-4503-a60d-42f51e9ba05e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2acffec1-7eff-4fb8-92f4-cbc2a8a71490",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f034eca-0f0c-413b-a18a-825c46002a41",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2acffec1-7eff-4fb8-92f4-cbc2a8a71490",
                    "LayerId": "403c5345-b65f-4b6c-83ba-dddd34db0650"
                }
            ]
        },
        {
            "id": "419ff99d-5b00-49a7-9a39-5ebf98d64287",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8960f105-8042-421f-8dc6-b96642a685b9",
            "compositeImage": {
                "id": "6ca441b9-1662-4da1-a6b1-bd677c341da8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "419ff99d-5b00-49a7-9a39-5ebf98d64287",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf782b61-4fce-4006-9bfb-c70ea1a64f24",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "419ff99d-5b00-49a7-9a39-5ebf98d64287",
                    "LayerId": "403c5345-b65f-4b6c-83ba-dddd34db0650"
                }
            ]
        },
        {
            "id": "8ea9fb05-4532-4055-aaa9-3191298dd13d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8960f105-8042-421f-8dc6-b96642a685b9",
            "compositeImage": {
                "id": "79cc9e3c-2eb8-473e-b5a3-9b8aee8e1443",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ea9fb05-4532-4055-aaa9-3191298dd13d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2694d2ea-47b1-4285-aa61-79b07a9dce4e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ea9fb05-4532-4055-aaa9-3191298dd13d",
                    "LayerId": "403c5345-b65f-4b6c-83ba-dddd34db0650"
                }
            ]
        },
        {
            "id": "e39a4d8f-ebb2-4e60-95db-7db857b61c0f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8960f105-8042-421f-8dc6-b96642a685b9",
            "compositeImage": {
                "id": "b88dfcd2-d498-4d08-b15c-8af81f35ac67",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e39a4d8f-ebb2-4e60-95db-7db857b61c0f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "93f3d30f-1331-4e02-8155-94c2f757635d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e39a4d8f-ebb2-4e60-95db-7db857b61c0f",
                    "LayerId": "403c5345-b65f-4b6c-83ba-dddd34db0650"
                }
            ]
        },
        {
            "id": "be5f4cb1-c116-4bc8-a9c7-7f8535fd5ece",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8960f105-8042-421f-8dc6-b96642a685b9",
            "compositeImage": {
                "id": "4d727ce6-88d1-4999-b402-32e6b809074c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be5f4cb1-c116-4bc8-a9c7-7f8535fd5ece",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f99e94c-0c4b-4f36-9448-dd3c7b9beeb1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be5f4cb1-c116-4bc8-a9c7-7f8535fd5ece",
                    "LayerId": "403c5345-b65f-4b6c-83ba-dddd34db0650"
                }
            ]
        },
        {
            "id": "847b1e6e-2f57-424c-8216-e6fe67fe8170",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8960f105-8042-421f-8dc6-b96642a685b9",
            "compositeImage": {
                "id": "6841d3e2-e1a0-417b-b765-a3e4612e6d97",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "847b1e6e-2f57-424c-8216-e6fe67fe8170",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2fa932d1-19f9-4aa3-a97e-d5ce270078ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "847b1e6e-2f57-424c-8216-e6fe67fe8170",
                    "LayerId": "403c5345-b65f-4b6c-83ba-dddd34db0650"
                }
            ]
        },
        {
            "id": "dbe31f2f-edfe-4cca-b45a-7e8b90d15fad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8960f105-8042-421f-8dc6-b96642a685b9",
            "compositeImage": {
                "id": "7dc65644-d536-43ba-a2f2-d81af08ee66e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dbe31f2f-edfe-4cca-b45a-7e8b90d15fad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cedf9944-dda3-436d-a741-b4c3886d36a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dbe31f2f-edfe-4cca-b45a-7e8b90d15fad",
                    "LayerId": "403c5345-b65f-4b6c-83ba-dddd34db0650"
                }
            ]
        },
        {
            "id": "d4cbb1c1-264c-4edc-a82e-dfd45c48edd3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8960f105-8042-421f-8dc6-b96642a685b9",
            "compositeImage": {
                "id": "0072e6b4-9bbe-41a4-8c5e-0d0440e231ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4cbb1c1-264c-4edc-a82e-dfd45c48edd3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b0f1235-98ea-4af9-bf16-d3c18ff8f175",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4cbb1c1-264c-4edc-a82e-dfd45c48edd3",
                    "LayerId": "403c5345-b65f-4b6c-83ba-dddd34db0650"
                }
            ]
        },
        {
            "id": "dca498bb-057b-46b1-bd9e-c4c7797df066",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8960f105-8042-421f-8dc6-b96642a685b9",
            "compositeImage": {
                "id": "7d0ecb5f-06cf-44fa-b8dc-c1f7386e1fa3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dca498bb-057b-46b1-bd9e-c4c7797df066",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f3b32e9c-8d0e-4cb7-8eee-192577a3eb6f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dca498bb-057b-46b1-bd9e-c4c7797df066",
                    "LayerId": "403c5345-b65f-4b6c-83ba-dddd34db0650"
                }
            ]
        },
        {
            "id": "c63d1ff3-bb0b-4b4c-84ab-d077d2040f33",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8960f105-8042-421f-8dc6-b96642a685b9",
            "compositeImage": {
                "id": "469e0c24-b7eb-4979-a1b9-51af39a2d169",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c63d1ff3-bb0b-4b4c-84ab-d077d2040f33",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8df5aa67-26ae-470b-b37a-0a3a95723eac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c63d1ff3-bb0b-4b4c-84ab-d077d2040f33",
                    "LayerId": "403c5345-b65f-4b6c-83ba-dddd34db0650"
                }
            ]
        },
        {
            "id": "732c4f2e-0db4-4325-a13d-9cc7585f4be9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8960f105-8042-421f-8dc6-b96642a685b9",
            "compositeImage": {
                "id": "892977c3-13e4-4262-9108-0d73a26ee753",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "732c4f2e-0db4-4325-a13d-9cc7585f4be9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b1ac94d-3774-4669-a17a-1f2fd0ff230e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "732c4f2e-0db4-4325-a13d-9cc7585f4be9",
                    "LayerId": "403c5345-b65f-4b6c-83ba-dddd34db0650"
                }
            ]
        },
        {
            "id": "d1812dab-c883-4364-bded-1b681f8fd610",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8960f105-8042-421f-8dc6-b96642a685b9",
            "compositeImage": {
                "id": "2e2ac5b5-4db2-426d-af69-171600b363d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1812dab-c883-4364-bded-1b681f8fd610",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9cac1adc-5539-436b-b84a-ce07280cd14a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1812dab-c883-4364-bded-1b681f8fd610",
                    "LayerId": "403c5345-b65f-4b6c-83ba-dddd34db0650"
                }
            ]
        },
        {
            "id": "ae945fc8-796b-4360-b7ad-296ca4069d67",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8960f105-8042-421f-8dc6-b96642a685b9",
            "compositeImage": {
                "id": "f257c3d5-4c18-4195-96d5-d02cc8bbbd75",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae945fc8-796b-4360-b7ad-296ca4069d67",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d500281a-8fdd-476f-aedc-4a9260bf3c6c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae945fc8-796b-4360-b7ad-296ca4069d67",
                    "LayerId": "403c5345-b65f-4b6c-83ba-dddd34db0650"
                }
            ]
        },
        {
            "id": "2f4c7d0f-cbc0-4667-a3a4-032a01ca39ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8960f105-8042-421f-8dc6-b96642a685b9",
            "compositeImage": {
                "id": "5cfdf5a1-266d-4024-ac7e-f13f170cc22c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f4c7d0f-cbc0-4667-a3a4-032a01ca39ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7f14afb-013e-4cbc-8552-689d03e401ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f4c7d0f-cbc0-4667-a3a4-032a01ca39ba",
                    "LayerId": "403c5345-b65f-4b6c-83ba-dddd34db0650"
                }
            ]
        },
        {
            "id": "31a94453-4a95-4b3a-a060-7331613990fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8960f105-8042-421f-8dc6-b96642a685b9",
            "compositeImage": {
                "id": "8d81cfcd-7640-4baa-a488-f86310398ff4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31a94453-4a95-4b3a-a060-7331613990fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6411cae-f0ee-4b69-86d1-8ade216afcb7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31a94453-4a95-4b3a-a060-7331613990fd",
                    "LayerId": "403c5345-b65f-4b6c-83ba-dddd34db0650"
                }
            ]
        },
        {
            "id": "81319f4a-2a96-4eea-b5c1-de5aad19e131",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8960f105-8042-421f-8dc6-b96642a685b9",
            "compositeImage": {
                "id": "0b038dc9-2915-44f8-99e0-3df08aae8813",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81319f4a-2a96-4eea-b5c1-de5aad19e131",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "258ffb56-741b-46ee-9a83-d25d65ea016d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81319f4a-2a96-4eea-b5c1-de5aad19e131",
                    "LayerId": "403c5345-b65f-4b6c-83ba-dddd34db0650"
                }
            ]
        },
        {
            "id": "c394c34c-eb0c-4bd1-98b1-5d3a4a94d2f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8960f105-8042-421f-8dc6-b96642a685b9",
            "compositeImage": {
                "id": "33a76234-7232-4897-8692-d1ef3ebcb1d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c394c34c-eb0c-4bd1-98b1-5d3a4a94d2f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9984e24-c968-40bf-b2df-4af70ab30c25",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c394c34c-eb0c-4bd1-98b1-5d3a4a94d2f8",
                    "LayerId": "403c5345-b65f-4b6c-83ba-dddd34db0650"
                }
            ]
        },
        {
            "id": "0429c7d1-d1fa-458b-a91e-9f3a35b51dce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8960f105-8042-421f-8dc6-b96642a685b9",
            "compositeImage": {
                "id": "7b65bdf1-968b-452e-bef8-3e850ad0afaa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0429c7d1-d1fa-458b-a91e-9f3a35b51dce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e44394d-b4aa-4e41-9d02-01e3970293a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0429c7d1-d1fa-458b-a91e-9f3a35b51dce",
                    "LayerId": "403c5345-b65f-4b6c-83ba-dddd34db0650"
                }
            ]
        },
        {
            "id": "c27c7871-8074-4c57-88fb-a201808d29cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8960f105-8042-421f-8dc6-b96642a685b9",
            "compositeImage": {
                "id": "004b2bcf-7484-4495-8a99-044b2dee5a84",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c27c7871-8074-4c57-88fb-a201808d29cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd718f0b-f5ed-4997-b1fa-db1676ac4e1d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c27c7871-8074-4c57-88fb-a201808d29cf",
                    "LayerId": "403c5345-b65f-4b6c-83ba-dddd34db0650"
                }
            ]
        },
        {
            "id": "b8370c60-d29a-4c46-b3a8-bada6d6756b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8960f105-8042-421f-8dc6-b96642a685b9",
            "compositeImage": {
                "id": "9bc78bed-3cf4-44ec-bde6-2cb48a55f90a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b8370c60-d29a-4c46-b3a8-bada6d6756b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "996df74a-b50a-4585-8612-d634a2f4f8c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b8370c60-d29a-4c46-b3a8-bada6d6756b2",
                    "LayerId": "403c5345-b65f-4b6c-83ba-dddd34db0650"
                }
            ]
        },
        {
            "id": "1c42b8a9-2fab-4b9d-ad32-1f9e031aac26",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8960f105-8042-421f-8dc6-b96642a685b9",
            "compositeImage": {
                "id": "5e09f7c7-9f1f-4892-8a15-a916c129d8ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c42b8a9-2fab-4b9d-ad32-1f9e031aac26",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "febbc2a3-d5a2-4fd6-971d-518e0f2e9ae2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c42b8a9-2fab-4b9d-ad32-1f9e031aac26",
                    "LayerId": "403c5345-b65f-4b6c-83ba-dddd34db0650"
                }
            ]
        },
        {
            "id": "909ea897-2eed-4f58-bb5c-f86da4389b95",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8960f105-8042-421f-8dc6-b96642a685b9",
            "compositeImage": {
                "id": "5a6f190e-0b2a-497c-9332-b7cae857db74",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "909ea897-2eed-4f58-bb5c-f86da4389b95",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d64dbe70-f0c1-4acc-8551-eaf655c764ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "909ea897-2eed-4f58-bb5c-f86da4389b95",
                    "LayerId": "403c5345-b65f-4b6c-83ba-dddd34db0650"
                }
            ]
        },
        {
            "id": "570cd2dd-2a09-426b-bc06-a22926cda971",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8960f105-8042-421f-8dc6-b96642a685b9",
            "compositeImage": {
                "id": "1852e57c-4b14-4085-8c0e-2af61321be52",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "570cd2dd-2a09-426b-bc06-a22926cda971",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c8b16b3-b684-45fc-8a4f-bf928ab0be14",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "570cd2dd-2a09-426b-bc06-a22926cda971",
                    "LayerId": "403c5345-b65f-4b6c-83ba-dddd34db0650"
                }
            ]
        },
        {
            "id": "9fa7ea56-09e0-4f37-a8f5-acd809b159c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8960f105-8042-421f-8dc6-b96642a685b9",
            "compositeImage": {
                "id": "9ef8d2ce-3f44-45e9-9137-de476038b902",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9fa7ea56-09e0-4f37-a8f5-acd809b159c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b2447afd-3e07-4b50-8ee5-3421a6cc2a0d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9fa7ea56-09e0-4f37-a8f5-acd809b159c6",
                    "LayerId": "403c5345-b65f-4b6c-83ba-dddd34db0650"
                }
            ]
        },
        {
            "id": "a0d2963e-7671-43b4-8629-45506757cbb4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8960f105-8042-421f-8dc6-b96642a685b9",
            "compositeImage": {
                "id": "3874ac77-3e42-47c7-aff5-55902cfd5ef9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a0d2963e-7671-43b4-8629-45506757cbb4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f6069fe-7dac-4ebf-902f-10f42f44e4bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0d2963e-7671-43b4-8629-45506757cbb4",
                    "LayerId": "403c5345-b65f-4b6c-83ba-dddd34db0650"
                }
            ]
        },
        {
            "id": "9fb49f55-1971-4e9d-a1cb-ea1b6ac9f8b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8960f105-8042-421f-8dc6-b96642a685b9",
            "compositeImage": {
                "id": "1353e3af-9a1a-439a-8c52-ece99755158f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9fb49f55-1971-4e9d-a1cb-ea1b6ac9f8b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f40d8573-57d5-4e93-bfbc-4c618aa86470",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9fb49f55-1971-4e9d-a1cb-ea1b6ac9f8b0",
                    "LayerId": "403c5345-b65f-4b6c-83ba-dddd34db0650"
                }
            ]
        },
        {
            "id": "8e80cd84-899d-4723-8e85-e21ad5c8bf35",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8960f105-8042-421f-8dc6-b96642a685b9",
            "compositeImage": {
                "id": "850e309e-31e8-4d20-a3e6-a858a0b446cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e80cd84-899d-4723-8e85-e21ad5c8bf35",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c66ea7f5-0059-4b92-a919-b9df7c3ba36b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e80cd84-899d-4723-8e85-e21ad5c8bf35",
                    "LayerId": "403c5345-b65f-4b6c-83ba-dddd34db0650"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 400,
    "layers": [
        {
            "id": "403c5345-b65f-4b6c-83ba-dddd34db0650",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8960f105-8042-421f-8dc6-b96642a685b9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 400,
    "xorig": 0,
    "yorig": 0
}