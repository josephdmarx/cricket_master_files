{
    "id": "7c11392c-25ca-4ca6-964a-e9a1dfa8378c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sMayorSurprise",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 94,
    "bbox_left": 46,
    "bbox_right": 78,
    "bbox_top": 29,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1d3055a9-a3a7-4d42-831f-44414a5ad273",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c11392c-25ca-4ca6-964a-e9a1dfa8378c",
            "compositeImage": {
                "id": "637ca2af-dc6d-4acf-a384-a512d79cf3f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d3055a9-a3a7-4d42-831f-44414a5ad273",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8001020b-2d68-47f0-bd3e-f3c8866d0eb9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d3055a9-a3a7-4d42-831f-44414a5ad273",
                    "LayerId": "a1913e54-5c7d-47f3-93b0-5de6d75d3f31"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "a1913e54-5c7d-47f3-93b0-5de6d75d3f31",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7c11392c-25ca-4ca6-964a-e9a1dfa8378c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}