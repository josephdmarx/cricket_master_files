{
    "id": "25c88ecd-c78b-4760-aa98-f2b0cf82650f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sMeadow01",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 359,
    "bbox_left": 0,
    "bbox_right": 639,
    "bbox_top": 227,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d6c62b5a-9b9b-48e1-8515-999cf3568bd8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "25c88ecd-c78b-4760-aa98-f2b0cf82650f",
            "compositeImage": {
                "id": "8690641e-2308-4213-9876-44139099cbb9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6c62b5a-9b9b-48e1-8515-999cf3568bd8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "06154ecd-2ddd-4600-99cc-380d911ad0b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6c62b5a-9b9b-48e1-8515-999cf3568bd8",
                    "LayerId": "b3ecb9d7-b5f4-4e6c-a609-46708d65e512"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 360,
    "layers": [
        {
            "id": "b3ecb9d7-b5f4-4e6c-a609-46708d65e512",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "25c88ecd-c78b-4760-aa98-f2b0cf82650f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 0,
    "yorig": 0
}