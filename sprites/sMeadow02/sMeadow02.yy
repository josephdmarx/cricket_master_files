{
    "id": "07c8c0f9-2276-4cb2-b54e-2ce58a330f90",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sMeadow02",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 359,
    "bbox_left": 0,
    "bbox_right": 639,
    "bbox_top": 226,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "06b39249-8a87-4923-9b49-10e5d534cc72",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07c8c0f9-2276-4cb2-b54e-2ce58a330f90",
            "compositeImage": {
                "id": "e8bc3954-12ea-495b-b921-712d46e64616",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06b39249-8a87-4923-9b49-10e5d534cc72",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99e6cc54-f96f-45c8-b75b-0da4e600abee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06b39249-8a87-4923-9b49-10e5d534cc72",
                    "LayerId": "9299e399-5762-4b71-a41e-915702de7e1e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 360,
    "layers": [
        {
            "id": "9299e399-5762-4b71-a41e-915702de7e1e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "07c8c0f9-2276-4cb2-b54e-2ce58a330f90",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 0,
    "yorig": 0
}