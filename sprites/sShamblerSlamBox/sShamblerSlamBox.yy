{
    "id": "366dbb84-305d-45f4-a321-f4cf4030c7b5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sShamblerSlamBox",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1f4e6f28-d44e-48a2-85a8-7563df2df5a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "366dbb84-305d-45f4-a321-f4cf4030c7b5",
            "compositeImage": {
                "id": "45111d69-ce59-44e7-b1d1-e34ee166fcf6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f4e6f28-d44e-48a2-85a8-7563df2df5a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a97d8c55-0a1f-4cb2-81a0-879fc6af0910",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f4e6f28-d44e-48a2-85a8-7563df2df5a7",
                    "LayerId": "6f5dc9f3-f0f6-4888-a436-185ec34a33c0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "6f5dc9f3-f0f6-4888-a436-185ec34a33c0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "366dbb84-305d-45f4-a321-f4cf4030c7b5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 16
}