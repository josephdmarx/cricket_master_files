{
    "id": "6db20f7f-2c2d-42fd-bd42-022d92456709",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSkullDemon_Crouch",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 48,
    "bbox_right": 80,
    "bbox_top": 65,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ebb6e6c9-792a-4867-922a-361e7d5d1aed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6db20f7f-2c2d-42fd-bd42-022d92456709",
            "compositeImage": {
                "id": "67c1b45d-4259-4c6e-80b3-3c857db140de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ebb6e6c9-792a-4867-922a-361e7d5d1aed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "03c36ff2-e1f1-4c28-ba4b-13b16dd71118",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ebb6e6c9-792a-4867-922a-361e7d5d1aed",
                    "LayerId": "bf350481-9f4d-465a-a92a-64779dae8e69"
                }
            ]
        },
        {
            "id": "8a5f8967-5d62-4a72-8110-44f8ca538edf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6db20f7f-2c2d-42fd-bd42-022d92456709",
            "compositeImage": {
                "id": "fc412a9d-7ca2-47a1-a93b-4cca7f903606",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a5f8967-5d62-4a72-8110-44f8ca538edf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4149aca6-8117-4202-a3f9-b623f00cf95d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a5f8967-5d62-4a72-8110-44f8ca538edf",
                    "LayerId": "bf350481-9f4d-465a-a92a-64779dae8e69"
                }
            ]
        },
        {
            "id": "6f5ba343-af6a-45c5-ac59-6f7d81b21bdd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6db20f7f-2c2d-42fd-bd42-022d92456709",
            "compositeImage": {
                "id": "0dd35aca-753f-4c84-9f4e-93e133274a85",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f5ba343-af6a-45c5-ac59-6f7d81b21bdd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0109f319-4b5b-4c5c-911f-67b605b06b91",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f5ba343-af6a-45c5-ac59-6f7d81b21bdd",
                    "LayerId": "bf350481-9f4d-465a-a92a-64779dae8e69"
                }
            ]
        },
        {
            "id": "14d4d4a9-af2b-4a4a-abc1-df0339b483a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6db20f7f-2c2d-42fd-bd42-022d92456709",
            "compositeImage": {
                "id": "6c39ca62-e712-455a-b45f-898f0d6e67ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "14d4d4a9-af2b-4a4a-abc1-df0339b483a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0eb500e1-3424-4c3b-97e3-84980ddda112",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14d4d4a9-af2b-4a4a-abc1-df0339b483a4",
                    "LayerId": "bf350481-9f4d-465a-a92a-64779dae8e69"
                }
            ]
        },
        {
            "id": "4235a17a-cd65-431c-9f28-dc012606591d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6db20f7f-2c2d-42fd-bd42-022d92456709",
            "compositeImage": {
                "id": "87e6cdd8-9b21-47b3-950a-d54d4ef7a5a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4235a17a-cd65-431c-9f28-dc012606591d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f11e002-bd13-45aa-b540-acd0ab9b3ed8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4235a17a-cd65-431c-9f28-dc012606591d",
                    "LayerId": "bf350481-9f4d-465a-a92a-64779dae8e69"
                }
            ]
        },
        {
            "id": "7331b7b8-2049-4dfe-b5ee-3b131dc0a1d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6db20f7f-2c2d-42fd-bd42-022d92456709",
            "compositeImage": {
                "id": "3dff907b-44fa-4317-9131-34173d605333",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7331b7b8-2049-4dfe-b5ee-3b131dc0a1d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a576d75f-985b-4b28-a859-0b9d768cb3c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7331b7b8-2049-4dfe-b5ee-3b131dc0a1d9",
                    "LayerId": "bf350481-9f4d-465a-a92a-64779dae8e69"
                }
            ]
        },
        {
            "id": "492209a9-1b65-4719-bdf4-dce5d4fe42d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6db20f7f-2c2d-42fd-bd42-022d92456709",
            "compositeImage": {
                "id": "dec5edbd-54e0-45b3-9ddf-aabc6baadfa0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "492209a9-1b65-4719-bdf4-dce5d4fe42d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "67723f39-228b-4573-9fcd-7551a02d7099",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "492209a9-1b65-4719-bdf4-dce5d4fe42d9",
                    "LayerId": "bf350481-9f4d-465a-a92a-64779dae8e69"
                }
            ]
        },
        {
            "id": "35b66ff7-d99d-4b61-b288-d98ec40faa2e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6db20f7f-2c2d-42fd-bd42-022d92456709",
            "compositeImage": {
                "id": "6b2e0551-25eb-4ce9-8407-2b935ba18028",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35b66ff7-d99d-4b61-b288-d98ec40faa2e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd1a7bb7-54e2-4d06-8626-3a13a1664971",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35b66ff7-d99d-4b61-b288-d98ec40faa2e",
                    "LayerId": "bf350481-9f4d-465a-a92a-64779dae8e69"
                }
            ]
        },
        {
            "id": "dca8556e-f61a-46bc-a39b-efc131d20293",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6db20f7f-2c2d-42fd-bd42-022d92456709",
            "compositeImage": {
                "id": "320d1d56-8049-4e0a-aa1a-cc83138bd28e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dca8556e-f61a-46bc-a39b-efc131d20293",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "958b16a4-a72f-423f-be1a-efc54f132a04",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dca8556e-f61a-46bc-a39b-efc131d20293",
                    "LayerId": "bf350481-9f4d-465a-a92a-64779dae8e69"
                }
            ]
        },
        {
            "id": "b772b31a-b9cc-442e-a740-9225bad6c5e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6db20f7f-2c2d-42fd-bd42-022d92456709",
            "compositeImage": {
                "id": "fdf03528-f8dc-4f1e-bf1c-6d8d41f15419",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b772b31a-b9cc-442e-a740-9225bad6c5e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a5c5e0c9-b23a-417c-8165-5f9b59558cd5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b772b31a-b9cc-442e-a740-9225bad6c5e3",
                    "LayerId": "bf350481-9f4d-465a-a92a-64779dae8e69"
                }
            ]
        },
        {
            "id": "58a8e6e7-5183-4abc-bca1-93a7653326bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6db20f7f-2c2d-42fd-bd42-022d92456709",
            "compositeImage": {
                "id": "a6276767-3cc6-4ef9-b12f-01f06293e204",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "58a8e6e7-5183-4abc-bca1-93a7653326bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "93fb8279-f077-4766-88b6-42da43161565",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "58a8e6e7-5183-4abc-bca1-93a7653326bf",
                    "LayerId": "bf350481-9f4d-465a-a92a-64779dae8e69"
                }
            ]
        },
        {
            "id": "ee4e4e33-d06a-40a4-91f6-e6e68dbf435f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6db20f7f-2c2d-42fd-bd42-022d92456709",
            "compositeImage": {
                "id": "f1f7ab55-b3a9-4cf1-9a1b-f3be252fd5af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee4e4e33-d06a-40a4-91f6-e6e68dbf435f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ede70818-4923-4b91-81db-e67a543888a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee4e4e33-d06a-40a4-91f6-e6e68dbf435f",
                    "LayerId": "bf350481-9f4d-465a-a92a-64779dae8e69"
                }
            ]
        },
        {
            "id": "e797e607-d771-4008-906b-ca5a77cf012e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6db20f7f-2c2d-42fd-bd42-022d92456709",
            "compositeImage": {
                "id": "d9fc9415-1be8-49ca-9f97-8a91d5ef04a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e797e607-d771-4008-906b-ca5a77cf012e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b57afaa6-2efd-4e99-9caf-72ba3f39ffe4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e797e607-d771-4008-906b-ca5a77cf012e",
                    "LayerId": "bf350481-9f4d-465a-a92a-64779dae8e69"
                }
            ]
        },
        {
            "id": "6b1da13f-70d0-47d9-bce0-4da2b096d48b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6db20f7f-2c2d-42fd-bd42-022d92456709",
            "compositeImage": {
                "id": "13971735-dcf9-4e25-b1b6-5cd5f23a8dae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b1da13f-70d0-47d9-bce0-4da2b096d48b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84f97a74-ad26-4b9f-ae7a-98b8c50cfc13",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b1da13f-70d0-47d9-bce0-4da2b096d48b",
                    "LayerId": "bf350481-9f4d-465a-a92a-64779dae8e69"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "bf350481-9f4d-465a-a92a-64779dae8e69",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6db20f7f-2c2d-42fd-bd42-022d92456709",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}