{
    "id": "3cd0a5e7-2b28-4a0f-b3f5-59902b9a46db",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bloco",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 5,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "769c2ddf-5a3c-45c8-b4c1-ee056d3878b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3cd0a5e7-2b28-4a0f-b3f5-59902b9a46db",
            "compositeImage": {
                "id": "c5b31c51-637c-41f3-973e-06173b3d9404",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "769c2ddf-5a3c-45c8-b4c1-ee056d3878b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5577fb9e-4eac-42e1-9ace-827406fc10b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "769c2ddf-5a3c-45c8-b4c1-ee056d3878b6",
                    "LayerId": "825aeffb-c71b-4cbb-880e-ff12b957dd48"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "825aeffb-c71b-4cbb-880e-ff12b957dd48",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3cd0a5e7-2b28-4a0f-b3f5-59902b9a46db",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}