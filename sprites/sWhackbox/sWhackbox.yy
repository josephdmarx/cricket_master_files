{
    "id": "d24de41f-eed9-4fa6-9a45-fb36fdeb3ebe",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sWhackbox",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "72997dc0-2eca-40de-802f-527f5e7402f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d24de41f-eed9-4fa6-9a45-fb36fdeb3ebe",
            "compositeImage": {
                "id": "e779a984-55c5-4761-9cd7-506942b9bdc2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72997dc0-2eca-40de-802f-527f5e7402f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f0689fb9-ce2f-4cf7-b0a3-20b5fb61da74",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72997dc0-2eca-40de-802f-527f5e7402f7",
                    "LayerId": "6976bd08-3336-4bb5-8760-99a0e708b27b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "6976bd08-3336-4bb5-8760-99a0e708b27b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d24de41f-eed9-4fa6-9a45-fb36fdeb3ebe",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}