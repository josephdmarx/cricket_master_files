{
    "id": "8f45f3fc-6269-4e25-be06-de55f5e999fb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sIndigoBirdPU",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 7,
    "bbox_right": 25,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b30d3905-2d23-4aaa-98e5-139e4aefe3a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8f45f3fc-6269-4e25-be06-de55f5e999fb",
            "compositeImage": {
                "id": "b8d8e283-4200-4cda-a0e4-00997e0b20a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b30d3905-2d23-4aaa-98e5-139e4aefe3a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "598ae9f9-43f5-4d10-a252-91cf5db764b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b30d3905-2d23-4aaa-98e5-139e4aefe3a1",
                    "LayerId": "18fc07ef-1b97-4334-b5de-6ccf408b30e6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "18fc07ef-1b97-4334-b5de-6ccf408b30e6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8f45f3fc-6269-4e25-be06-de55f5e999fb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 3,
    "yorig": 3
}