{
    "id": "9dd9e44d-a6b2-4b85-9070-98e945967cc1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Ground01_Flat",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 111,
    "bbox_left": 16,
    "bbox_right": 111,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "eb180860-643c-4fed-b554-2111db8fd803",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9dd9e44d-a6b2-4b85-9070-98e945967cc1",
            "compositeImage": {
                "id": "67914ef2-3de6-4cc6-b64b-3adcef6627e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb180860-643c-4fed-b554-2111db8fd803",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c604ff16-9ae6-4de8-99c2-9c57a217ddcf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb180860-643c-4fed-b554-2111db8fd803",
                    "LayerId": "bda1ea56-917a-449b-8ede-f13ead1454fe"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "bda1ea56-917a-449b-8ede-f13ead1454fe",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9dd9e44d-a6b2-4b85-9070-98e945967cc1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 512,
    "xorig": 0,
    "yorig": 0
}