{
    "id": "90a9d325-0622-4243-9c19-69c5701c9e61",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSlope01",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "39915dda-0371-453d-b412-8aec1cf5c319",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "90a9d325-0622-4243-9c19-69c5701c9e61",
            "compositeImage": {
                "id": "ca33e265-85bf-4ebb-afa8-7ac33436a8e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39915dda-0371-453d-b412-8aec1cf5c319",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "27201f2d-faa2-49e6-ae5e-91495b4fc996",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39915dda-0371-453d-b412-8aec1cf5c319",
                    "LayerId": "0e9260fe-5516-4c15-a839-c5bf7a6df8ee"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "0e9260fe-5516-4c15-a839-c5bf7a6df8ee",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "90a9d325-0622-4243-9c19-69c5701c9e61",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 3,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 16
}