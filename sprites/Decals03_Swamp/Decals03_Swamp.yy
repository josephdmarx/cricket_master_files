{
    "id": "0c207bdd-eeb0-4fd2-8d0c-d27445573f33",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Decals03_Swamp",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 337,
    "bbox_left": 53,
    "bbox_right": 1130,
    "bbox_top": 19,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1bb9f949-22e3-4a90-b5d7-3b0e3674f4a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0c207bdd-eeb0-4fd2-8d0c-d27445573f33",
            "compositeImage": {
                "id": "9248fdbb-867e-485c-9ee7-eabfc8b01ede",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1bb9f949-22e3-4a90-b5d7-3b0e3674f4a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d02f5f85-7bc0-460c-8a16-4ef62d96385b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1bb9f949-22e3-4a90-b5d7-3b0e3674f4a2",
                    "LayerId": "4854e789-c2cb-441e-b73b-409ca8325c6b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 360,
    "layers": [
        {
            "id": "4854e789-c2cb-441e-b73b-409ca8325c6b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0c207bdd-eeb0-4fd2-8d0c-d27445573f33",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1280,
    "xorig": 0,
    "yorig": 0
}