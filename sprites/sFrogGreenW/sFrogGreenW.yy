{
    "id": "9d0fe532-88c4-46a8-a486-c338e475e4d9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sFrogGreenW",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 25,
    "bbox_left": 21,
    "bbox_right": 45,
    "bbox_top": 4,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "40f843e2-4dbd-4df7-aa60-33d652ba70f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9d0fe532-88c4-46a8-a486-c338e475e4d9",
            "compositeImage": {
                "id": "255ab69a-0bc6-4a28-b7cf-cc01da988aa8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "40f843e2-4dbd-4df7-aa60-33d652ba70f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f3d03635-77e8-4230-b4af-125d5e698456",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "40f843e2-4dbd-4df7-aa60-33d652ba70f3",
                    "LayerId": "2aa2b5bd-d0e8-4946-9376-9ad692f8e8bb"
                }
            ]
        },
        {
            "id": "5e872f43-fe0a-4daf-ae42-2da5a3b11622",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9d0fe532-88c4-46a8-a486-c338e475e4d9",
            "compositeImage": {
                "id": "b7772921-bf2e-4d2a-a7c2-aa580f4f5540",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e872f43-fe0a-4daf-ae42-2da5a3b11622",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1536eddd-99c6-42ec-8025-2ace944f3873",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e872f43-fe0a-4daf-ae42-2da5a3b11622",
                    "LayerId": "2aa2b5bd-d0e8-4946-9376-9ad692f8e8bb"
                }
            ]
        },
        {
            "id": "35d42be3-a861-4354-b072-9d8789cc0a8d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9d0fe532-88c4-46a8-a486-c338e475e4d9",
            "compositeImage": {
                "id": "851ade78-3d48-4b61-93b5-2285c2b828a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35d42be3-a861-4354-b072-9d8789cc0a8d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2995d636-2010-4678-b845-d2a6e9be49e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35d42be3-a861-4354-b072-9d8789cc0a8d",
                    "LayerId": "2aa2b5bd-d0e8-4946-9376-9ad692f8e8bb"
                }
            ]
        },
        {
            "id": "ca480f4f-7fe1-47a0-bddd-81cf7ca7f58d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9d0fe532-88c4-46a8-a486-c338e475e4d9",
            "compositeImage": {
                "id": "102a4e11-cf3c-4135-87c6-2711ab38b772",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca480f4f-7fe1-47a0-bddd-81cf7ca7f58d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4c078a2-411e-4236-9fc2-17383a911036",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca480f4f-7fe1-47a0-bddd-81cf7ca7f58d",
                    "LayerId": "2aa2b5bd-d0e8-4946-9376-9ad692f8e8bb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "2aa2b5bd-d0e8-4946-9376-9ad692f8e8bb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9d0fe532-88c4-46a8-a486-c338e475e4d9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 16
}