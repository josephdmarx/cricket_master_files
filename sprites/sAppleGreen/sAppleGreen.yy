{
    "id": "4e9dfba7-3374-4df6-8645-2282b3b7ecaf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sAppleGreen",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 9,
    "bbox_right": 22,
    "bbox_top": 9,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ec5a5494-ab41-4deb-8335-3a9c7379ed36",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e9dfba7-3374-4df6-8645-2282b3b7ecaf",
            "compositeImage": {
                "id": "2ba8a855-f96c-4794-8a2e-d8387b0a9ffb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec5a5494-ab41-4deb-8335-3a9c7379ed36",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d8359319-ad09-422f-babf-04e22e67a0a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec5a5494-ab41-4deb-8335-3a9c7379ed36",
                    "LayerId": "48ccb22a-2575-4fcb-8092-b91233dcd88f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "48ccb22a-2575-4fcb-8092-b91233dcd88f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4e9dfba7-3374-4df6-8645-2282b3b7ecaf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}