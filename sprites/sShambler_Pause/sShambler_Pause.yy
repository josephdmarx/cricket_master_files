{
    "id": "d7771781-1560-40e8-9200-c425b0cfa779",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sShambler_Pause",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 198,
    "bbox_left": 75,
    "bbox_right": 155,
    "bbox_top": 110,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1290b61c-ec40-40da-b79a-658601f1b9d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7771781-1560-40e8-9200-c425b0cfa779",
            "compositeImage": {
                "id": "968e5063-1f66-41ef-9665-de0d9b524fac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1290b61c-ec40-40da-b79a-658601f1b9d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55137ba9-b76f-48ec-89e8-6506691263ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1290b61c-ec40-40da-b79a-658601f1b9d8",
                    "LayerId": "d71b845d-d6fb-4b32-b573-f437d9dc4e85"
                }
            ]
        },
        {
            "id": "9ad12577-dbab-4422-96e4-84f99f316f6a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7771781-1560-40e8-9200-c425b0cfa779",
            "compositeImage": {
                "id": "dd2115ea-98df-4a06-bdd5-2c2142584461",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ad12577-dbab-4422-96e4-84f99f316f6a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "96b8a1e0-b0fe-4ac4-8953-f5835a381038",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ad12577-dbab-4422-96e4-84f99f316f6a",
                    "LayerId": "d71b845d-d6fb-4b32-b573-f437d9dc4e85"
                }
            ]
        },
        {
            "id": "0b603810-97d3-4572-b995-6a31d00c1343",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7771781-1560-40e8-9200-c425b0cfa779",
            "compositeImage": {
                "id": "af2888da-67c5-42a7-bfc5-92cef5a59397",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b603810-97d3-4572-b995-6a31d00c1343",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b715b3e-088a-402f-83db-3f6c369fbe5d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b603810-97d3-4572-b995-6a31d00c1343",
                    "LayerId": "d71b845d-d6fb-4b32-b573-f437d9dc4e85"
                }
            ]
        },
        {
            "id": "054532fc-4011-4d0b-88cc-108bb7d34f0f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7771781-1560-40e8-9200-c425b0cfa779",
            "compositeImage": {
                "id": "ccbb4094-6369-4766-84bc-058507a2fa5d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "054532fc-4011-4d0b-88cc-108bb7d34f0f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c03ac186-1079-4a33-bdd5-8aaa5cd91e56",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "054532fc-4011-4d0b-88cc-108bb7d34f0f",
                    "LayerId": "d71b845d-d6fb-4b32-b573-f437d9dc4e85"
                }
            ]
        },
        {
            "id": "470df4e9-f71c-44cb-9cd8-a49b3651698e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7771781-1560-40e8-9200-c425b0cfa779",
            "compositeImage": {
                "id": "8a11eb1b-8b85-4cd9-bc7d-8d20ccf085c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "470df4e9-f71c-44cb-9cd8-a49b3651698e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a05a780-657d-41c7-b18b-d51642a27cfa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "470df4e9-f71c-44cb-9cd8-a49b3651698e",
                    "LayerId": "d71b845d-d6fb-4b32-b573-f437d9dc4e85"
                }
            ]
        },
        {
            "id": "a5c2c790-305b-41c5-933f-28d486a61956",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7771781-1560-40e8-9200-c425b0cfa779",
            "compositeImage": {
                "id": "8a779914-aba2-4134-b6fc-40c5f93a3b6d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a5c2c790-305b-41c5-933f-28d486a61956",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85c0da3a-fc11-40e5-80a9-421a2fa38d8f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5c2c790-305b-41c5-933f-28d486a61956",
                    "LayerId": "d71b845d-d6fb-4b32-b573-f437d9dc4e85"
                }
            ]
        },
        {
            "id": "103afc41-e340-485d-8f29-2b0c342980b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7771781-1560-40e8-9200-c425b0cfa779",
            "compositeImage": {
                "id": "688fe30a-7ce9-4ab3-b6d7-142c57979eee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "103afc41-e340-485d-8f29-2b0c342980b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb3c67c0-521e-49b6-a5fb-c13f0b005b52",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "103afc41-e340-485d-8f29-2b0c342980b5",
                    "LayerId": "d71b845d-d6fb-4b32-b573-f437d9dc4e85"
                }
            ]
        },
        {
            "id": "4e76d09d-1cce-423e-b945-743908486bb7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7771781-1560-40e8-9200-c425b0cfa779",
            "compositeImage": {
                "id": "898edd45-645b-4735-a482-78c92d354c14",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4e76d09d-1cce-423e-b945-743908486bb7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d632a5e-a1c3-4cc2-8aee-13eba8bd1dc4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e76d09d-1cce-423e-b945-743908486bb7",
                    "LayerId": "d71b845d-d6fb-4b32-b573-f437d9dc4e85"
                }
            ]
        },
        {
            "id": "e5678fa7-e042-460c-bce7-bc230b3a6d06",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7771781-1560-40e8-9200-c425b0cfa779",
            "compositeImage": {
                "id": "c9e8d1e8-4a2c-4058-8f15-281a1d109e83",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5678fa7-e042-460c-bce7-bc230b3a6d06",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e84e61f5-23c0-4718-b45a-cdf47938337b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5678fa7-e042-460c-bce7-bc230b3a6d06",
                    "LayerId": "d71b845d-d6fb-4b32-b573-f437d9dc4e85"
                }
            ]
        },
        {
            "id": "a7f3f6f8-31ee-46a1-be09-b504ee533634",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7771781-1560-40e8-9200-c425b0cfa779",
            "compositeImage": {
                "id": "c051f0ff-17b7-4b4d-bf53-11155970ca61",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7f3f6f8-31ee-46a1-be09-b504ee533634",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cbd94e27-d966-47a6-83d4-efce91ad0a69",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7f3f6f8-31ee-46a1-be09-b504ee533634",
                    "LayerId": "d71b845d-d6fb-4b32-b573-f437d9dc4e85"
                }
            ]
        },
        {
            "id": "063e8a97-c8b6-48b3-8f0d-257e653723c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7771781-1560-40e8-9200-c425b0cfa779",
            "compositeImage": {
                "id": "ac8b0ad0-63d7-4b6f-abda-c0e1222ddc4c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "063e8a97-c8b6-48b3-8f0d-257e653723c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54085c29-e567-4c68-9ad0-d3bb9b9dc3ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "063e8a97-c8b6-48b3-8f0d-257e653723c9",
                    "LayerId": "d71b845d-d6fb-4b32-b573-f437d9dc4e85"
                }
            ]
        },
        {
            "id": "0bae3435-c41c-423c-a631-0111c77582ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7771781-1560-40e8-9200-c425b0cfa779",
            "compositeImage": {
                "id": "144f5c61-1b40-4f54-9ea2-60eafe2ce624",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0bae3435-c41c-423c-a631-0111c77582ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ba5d8c5-16db-4a6d-b63c-1a92936677bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0bae3435-c41c-423c-a631-0111c77582ba",
                    "LayerId": "d71b845d-d6fb-4b32-b573-f437d9dc4e85"
                }
            ]
        },
        {
            "id": "aae1470e-416e-4cf7-9348-6ab2fd14fa5c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7771781-1560-40e8-9200-c425b0cfa779",
            "compositeImage": {
                "id": "f00336e1-08fb-4369-8986-1de2ff6ee178",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aae1470e-416e-4cf7-9348-6ab2fd14fa5c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d73a3922-05e6-4ab6-998f-7be1c676ab60",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aae1470e-416e-4cf7-9348-6ab2fd14fa5c",
                    "LayerId": "d71b845d-d6fb-4b32-b573-f437d9dc4e85"
                }
            ]
        },
        {
            "id": "4dc4d035-c5a9-4620-b273-500a01e7e085",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7771781-1560-40e8-9200-c425b0cfa779",
            "compositeImage": {
                "id": "326fd3a3-ca25-4880-aa60-7a67073deef3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4dc4d035-c5a9-4620-b273-500a01e7e085",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c5aed4d-d71b-406b-8469-b3267af09df3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4dc4d035-c5a9-4620-b273-500a01e7e085",
                    "LayerId": "d71b845d-d6fb-4b32-b573-f437d9dc4e85"
                }
            ]
        },
        {
            "id": "1538c110-1dbf-4390-a384-1c25a8e542a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7771781-1560-40e8-9200-c425b0cfa779",
            "compositeImage": {
                "id": "0fd5229d-415b-41e9-8c8a-749f943fc894",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1538c110-1dbf-4390-a384-1c25a8e542a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b19a5c0e-24fb-4a4d-9963-308832c7d14e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1538c110-1dbf-4390-a384-1c25a8e542a4",
                    "LayerId": "d71b845d-d6fb-4b32-b573-f437d9dc4e85"
                }
            ]
        },
        {
            "id": "45318d2c-380c-4d46-b3fd-6204daeebaa1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7771781-1560-40e8-9200-c425b0cfa779",
            "compositeImage": {
                "id": "c8b4023a-0e17-409c-843c-26d8ed386fa6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "45318d2c-380c-4d46-b3fd-6204daeebaa1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a25729b-b423-4678-b26b-87ae27739027",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "45318d2c-380c-4d46-b3fd-6204daeebaa1",
                    "LayerId": "d71b845d-d6fb-4b32-b573-f437d9dc4e85"
                }
            ]
        },
        {
            "id": "8cc22f66-2a10-4c4c-97ea-c9fccc984cfa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7771781-1560-40e8-9200-c425b0cfa779",
            "compositeImage": {
                "id": "70298d8e-2345-4b49-ad44-4b979754af08",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8cc22f66-2a10-4c4c-97ea-c9fccc984cfa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "beba44de-2427-4c3b-9ada-69fdb5f80bde",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8cc22f66-2a10-4c4c-97ea-c9fccc984cfa",
                    "LayerId": "d71b845d-d6fb-4b32-b573-f437d9dc4e85"
                }
            ]
        },
        {
            "id": "3bb03b7f-fcb3-452b-935a-3c9047b85f10",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7771781-1560-40e8-9200-c425b0cfa779",
            "compositeImage": {
                "id": "ba0d6f8c-d2c5-4242-9418-3a1ecf633998",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3bb03b7f-fcb3-452b-935a-3c9047b85f10",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "24c2b387-dcfa-4192-b41a-a902d4a1c3bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3bb03b7f-fcb3-452b-935a-3c9047b85f10",
                    "LayerId": "d71b845d-d6fb-4b32-b573-f437d9dc4e85"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 215,
    "layers": [
        {
            "id": "d71b845d-d6fb-4b32-b573-f437d9dc4e85",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d7771781-1560-40e8-9200-c425b0cfa779",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 258,
    "xorig": 129,
    "yorig": 107
}