{
    "id": "50b99c0f-8305-48f7-86aa-f6fe1a729de4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBPad",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 7,
    "bbox_right": 24,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f4a2d2c7-d82b-4a74-a65c-5a60bf4ae1d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "50b99c0f-8305-48f7-86aa-f6fe1a729de4",
            "compositeImage": {
                "id": "2f6a067a-c604-4d31-b677-5be0adc91c22",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f4a2d2c7-d82b-4a74-a65c-5a60bf4ae1d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc719c7c-e495-41e9-9731-7eeb05878035",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4a2d2c7-d82b-4a74-a65c-5a60bf4ae1d1",
                    "LayerId": "641cbacb-05c3-482e-a3de-7fb1d099676c"
                }
            ]
        },
        {
            "id": "e06df1aa-7184-488e-8582-74016705dd3b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "50b99c0f-8305-48f7-86aa-f6fe1a729de4",
            "compositeImage": {
                "id": "7e67f775-d47d-404c-b29c-1dab6185f4ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e06df1aa-7184-488e-8582-74016705dd3b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ead22681-02b8-4eda-965b-85532d95f5b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e06df1aa-7184-488e-8582-74016705dd3b",
                    "LayerId": "641cbacb-05c3-482e-a3de-7fb1d099676c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "641cbacb-05c3-482e-a3de-7fb1d099676c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "50b99c0f-8305-48f7-86aa-f6fe1a729de4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 15,
    "yorig": 25
}