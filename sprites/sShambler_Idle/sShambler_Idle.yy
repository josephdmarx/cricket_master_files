{
    "id": "7dcde0f8-e73e-4d92-9366-465e8ab1cd70",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sShambler_Idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 198,
    "bbox_left": 75,
    "bbox_right": 155,
    "bbox_top": 110,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c92dd6a7-ddf3-4ca2-bee9-6bfde39157f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7dcde0f8-e73e-4d92-9366-465e8ab1cd70",
            "compositeImage": {
                "id": "46967aa1-48ca-495d-a138-4fc00ddd699e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c92dd6a7-ddf3-4ca2-bee9-6bfde39157f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "998847a9-9458-4b3c-803d-bbb7e00729e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c92dd6a7-ddf3-4ca2-bee9-6bfde39157f1",
                    "LayerId": "4d718bca-0505-4d05-a611-034bf2278a6b"
                }
            ]
        },
        {
            "id": "0fc9e578-f833-4150-8575-d28b86b6fe3d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7dcde0f8-e73e-4d92-9366-465e8ab1cd70",
            "compositeImage": {
                "id": "bc75f636-0691-4e41-b44d-969a643f9527",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0fc9e578-f833-4150-8575-d28b86b6fe3d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f8dd966-88b9-4eba-af69-22fb2d722960",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0fc9e578-f833-4150-8575-d28b86b6fe3d",
                    "LayerId": "4d718bca-0505-4d05-a611-034bf2278a6b"
                }
            ]
        },
        {
            "id": "f9c9bb17-b07f-4a8e-9922-478ab46cb0f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7dcde0f8-e73e-4d92-9366-465e8ab1cd70",
            "compositeImage": {
                "id": "9dc7f1c6-67b7-4b97-bb54-da5f39ff8fd0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f9c9bb17-b07f-4a8e-9922-478ab46cb0f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d51ebaf2-71f6-45f7-99b7-90d5f9ff54f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9c9bb17-b07f-4a8e-9922-478ab46cb0f4",
                    "LayerId": "4d718bca-0505-4d05-a611-034bf2278a6b"
                }
            ]
        },
        {
            "id": "c0452fe3-a9b5-47dc-91fe-6d54ec313baf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7dcde0f8-e73e-4d92-9366-465e8ab1cd70",
            "compositeImage": {
                "id": "19b84278-1878-41cb-a932-3c498278c972",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0452fe3-a9b5-47dc-91fe-6d54ec313baf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b018c8c0-d606-4754-afc8-575b5b094515",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0452fe3-a9b5-47dc-91fe-6d54ec313baf",
                    "LayerId": "4d718bca-0505-4d05-a611-034bf2278a6b"
                }
            ]
        },
        {
            "id": "76089b53-3dda-48ed-91c2-0502b4e48966",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7dcde0f8-e73e-4d92-9366-465e8ab1cd70",
            "compositeImage": {
                "id": "6a0393e9-6309-437a-8fb1-2b61b9409945",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76089b53-3dda-48ed-91c2-0502b4e48966",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e3a12ee-4cde-461f-a878-7eeefdc18711",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76089b53-3dda-48ed-91c2-0502b4e48966",
                    "LayerId": "4d718bca-0505-4d05-a611-034bf2278a6b"
                }
            ]
        },
        {
            "id": "50123abf-ac85-4e02-915c-be5b7f03763e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7dcde0f8-e73e-4d92-9366-465e8ab1cd70",
            "compositeImage": {
                "id": "e6d8ca90-4144-4cd1-b35a-2bf645f65c2d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "50123abf-ac85-4e02-915c-be5b7f03763e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44d26758-f8c8-49d7-8b74-2566b19e8b51",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "50123abf-ac85-4e02-915c-be5b7f03763e",
                    "LayerId": "4d718bca-0505-4d05-a611-034bf2278a6b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 215,
    "layers": [
        {
            "id": "4d718bca-0505-4d05-a611-034bf2278a6b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7dcde0f8-e73e-4d92-9366-465e8ab1cd70",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 258,
    "xorig": 129,
    "yorig": 107
}