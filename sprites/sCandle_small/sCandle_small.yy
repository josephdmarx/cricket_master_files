{
    "id": "4f7a53bc-f6ee-411d-9ae2-c86c8f6182ae",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCandle_small",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 77,
    "bbox_left": 49,
    "bbox_right": 54,
    "bbox_top": 65,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ee9a801a-2e5a-49a6-9b4a-e5b8d5f18dcf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f7a53bc-f6ee-411d-9ae2-c86c8f6182ae",
            "compositeImage": {
                "id": "5ab3c24a-9a0d-4391-8f20-771988c6cd10",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee9a801a-2e5a-49a6-9b4a-e5b8d5f18dcf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a437b547-c83e-42bc-adac-c7ef0e479644",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee9a801a-2e5a-49a6-9b4a-e5b8d5f18dcf",
                    "LayerId": "01e4fba6-3f81-41f2-9294-c1f5fe15d720"
                }
            ]
        },
        {
            "id": "573f49d7-3617-468e-b4e3-27f608ae4f5c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f7a53bc-f6ee-411d-9ae2-c86c8f6182ae",
            "compositeImage": {
                "id": "5ca011c2-71f9-4844-a2b6-ef5e978ae3ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "573f49d7-3617-468e-b4e3-27f608ae4f5c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad658c38-3ad9-4b23-bd19-1c3340323260",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "573f49d7-3617-468e-b4e3-27f608ae4f5c",
                    "LayerId": "01e4fba6-3f81-41f2-9294-c1f5fe15d720"
                }
            ]
        },
        {
            "id": "5f9b4bea-4ffd-4653-bad2-c5d343e0e878",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f7a53bc-f6ee-411d-9ae2-c86c8f6182ae",
            "compositeImage": {
                "id": "81e23def-422c-4ffa-bb23-4b51ecb5537f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f9b4bea-4ffd-4653-bad2-c5d343e0e878",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d826a60-59fe-4d3f-b734-87036be7ba44",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f9b4bea-4ffd-4653-bad2-c5d343e0e878",
                    "LayerId": "01e4fba6-3f81-41f2-9294-c1f5fe15d720"
                }
            ]
        },
        {
            "id": "fe397eda-e53f-4fc5-83df-2c8cb5245f5d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f7a53bc-f6ee-411d-9ae2-c86c8f6182ae",
            "compositeImage": {
                "id": "8de6dd96-0813-4d86-b3d7-a3ca5ad73c97",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe397eda-e53f-4fc5-83df-2c8cb5245f5d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "31df8087-d3a2-41d3-bee9-99f051b4fa49",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe397eda-e53f-4fc5-83df-2c8cb5245f5d",
                    "LayerId": "01e4fba6-3f81-41f2-9294-c1f5fe15d720"
                }
            ]
        },
        {
            "id": "e6c0600c-9889-4db0-af56-828dddec2419",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f7a53bc-f6ee-411d-9ae2-c86c8f6182ae",
            "compositeImage": {
                "id": "eb28b2ca-bdf1-4735-b6f1-81b055b939b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6c0600c-9889-4db0-af56-828dddec2419",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "98134305-4dc7-47b4-9256-7be8c2c4581a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6c0600c-9889-4db0-af56-828dddec2419",
                    "LayerId": "01e4fba6-3f81-41f2-9294-c1f5fe15d720"
                }
            ]
        },
        {
            "id": "a7e77da3-559e-4fb8-ab48-88f41300f42a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f7a53bc-f6ee-411d-9ae2-c86c8f6182ae",
            "compositeImage": {
                "id": "c0581844-43c4-4e19-8de6-4cc38fa4e843",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7e77da3-559e-4fb8-ab48-88f41300f42a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f28844fa-df49-43c0-9331-b8d393887c64",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7e77da3-559e-4fb8-ab48-88f41300f42a",
                    "LayerId": "01e4fba6-3f81-41f2-9294-c1f5fe15d720"
                }
            ]
        },
        {
            "id": "f14659df-38a9-4e12-bf1d-8b854829a553",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f7a53bc-f6ee-411d-9ae2-c86c8f6182ae",
            "compositeImage": {
                "id": "92f418c8-74a5-44e9-b6c1-1a8816c6d505",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f14659df-38a9-4e12-bf1d-8b854829a553",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a7bb80ab-19df-4adf-a685-317edb79c824",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f14659df-38a9-4e12-bf1d-8b854829a553",
                    "LayerId": "01e4fba6-3f81-41f2-9294-c1f5fe15d720"
                }
            ]
        },
        {
            "id": "fbb7e923-09f3-4438-b41a-6f7f7a9a99ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f7a53bc-f6ee-411d-9ae2-c86c8f6182ae",
            "compositeImage": {
                "id": "f247c1d6-3121-486a-ab82-c4b1dcf57def",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fbb7e923-09f3-4438-b41a-6f7f7a9a99ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "66d110b1-cdb5-435e-876c-28d889f113f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fbb7e923-09f3-4438-b41a-6f7f7a9a99ef",
                    "LayerId": "01e4fba6-3f81-41f2-9294-c1f5fe15d720"
                }
            ]
        },
        {
            "id": "0bdd61b2-0bc9-45e7-8fb2-95df5ebb0cc3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f7a53bc-f6ee-411d-9ae2-c86c8f6182ae",
            "compositeImage": {
                "id": "c0762011-acb7-4dc6-b605-eac2132dfa90",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0bdd61b2-0bc9-45e7-8fb2-95df5ebb0cc3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09b50243-5f40-45f3-8722-0d06ce799f29",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0bdd61b2-0bc9-45e7-8fb2-95df5ebb0cc3",
                    "LayerId": "01e4fba6-3f81-41f2-9294-c1f5fe15d720"
                }
            ]
        },
        {
            "id": "4cb68cf3-bb60-4740-8d87-e95b4e24900e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f7a53bc-f6ee-411d-9ae2-c86c8f6182ae",
            "compositeImage": {
                "id": "3b098f6a-e883-4d49-9f92-d85e0bf58c73",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4cb68cf3-bb60-4740-8d87-e95b4e24900e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5eec76dc-73e9-4422-9a70-fa6410c878ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4cb68cf3-bb60-4740-8d87-e95b4e24900e",
                    "LayerId": "01e4fba6-3f81-41f2-9294-c1f5fe15d720"
                }
            ]
        },
        {
            "id": "7e1c205c-1113-44ad-8d12-af75c6f443f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f7a53bc-f6ee-411d-9ae2-c86c8f6182ae",
            "compositeImage": {
                "id": "17ac6736-690b-477b-b086-3c2771dc1954",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e1c205c-1113-44ad-8d12-af75c6f443f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6341ce46-6a04-42a1-b64e-c15116aa16bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e1c205c-1113-44ad-8d12-af75c6f443f4",
                    "LayerId": "01e4fba6-3f81-41f2-9294-c1f5fe15d720"
                }
            ]
        },
        {
            "id": "0ecfa78e-4426-4514-9225-5a8b0d4949e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f7a53bc-f6ee-411d-9ae2-c86c8f6182ae",
            "compositeImage": {
                "id": "4e2fb9b1-aa12-4a22-9cde-ea71a1316277",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ecfa78e-4426-4514-9225-5a8b0d4949e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1a12cc8-8370-4b30-ac4d-e00d5cca3e58",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ecfa78e-4426-4514-9225-5a8b0d4949e1",
                    "LayerId": "01e4fba6-3f81-41f2-9294-c1f5fe15d720"
                }
            ]
        },
        {
            "id": "4cd491ca-5dd9-4aa5-adbc-5042147db59b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f7a53bc-f6ee-411d-9ae2-c86c8f6182ae",
            "compositeImage": {
                "id": "5482ca9c-916b-4d00-9377-7b5768b47a3d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4cd491ca-5dd9-4aa5-adbc-5042147db59b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e3261ef-9909-4995-b0d0-1491a77420bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4cd491ca-5dd9-4aa5-adbc-5042147db59b",
                    "LayerId": "01e4fba6-3f81-41f2-9294-c1f5fe15d720"
                }
            ]
        },
        {
            "id": "fd990984-1a24-42d0-bec0-57bec4abd10d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f7a53bc-f6ee-411d-9ae2-c86c8f6182ae",
            "compositeImage": {
                "id": "81e3c40b-46c5-4b7f-9bcc-03658846e5e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd990984-1a24-42d0-bec0-57bec4abd10d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19d0e651-23dc-4b90-b9d8-cd9fdc45f682",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd990984-1a24-42d0-bec0-57bec4abd10d",
                    "LayerId": "01e4fba6-3f81-41f2-9294-c1f5fe15d720"
                }
            ]
        },
        {
            "id": "a2115230-1016-4848-b504-73dab07fa382",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f7a53bc-f6ee-411d-9ae2-c86c8f6182ae",
            "compositeImage": {
                "id": "da012114-5deb-43a3-b9b0-20b1ff75beaf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a2115230-1016-4848-b504-73dab07fa382",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4fc9c6ee-d057-4014-bcf0-3a1ebeb0dc84",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2115230-1016-4848-b504-73dab07fa382",
                    "LayerId": "01e4fba6-3f81-41f2-9294-c1f5fe15d720"
                }
            ]
        },
        {
            "id": "79db26b2-f957-4743-ab29-795c576fcce2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f7a53bc-f6ee-411d-9ae2-c86c8f6182ae",
            "compositeImage": {
                "id": "db2b5b28-7237-4aae-8f3c-3cdd81c27492",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "79db26b2-f957-4743-ab29-795c576fcce2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c7586896-d7b9-4f02-9e88-ab2e8de2de76",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79db26b2-f957-4743-ab29-795c576fcce2",
                    "LayerId": "01e4fba6-3f81-41f2-9294-c1f5fe15d720"
                }
            ]
        },
        {
            "id": "33a53058-a18b-4284-8353-f254cfbb9f90",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f7a53bc-f6ee-411d-9ae2-c86c8f6182ae",
            "compositeImage": {
                "id": "b2ca5466-a2a9-41a2-8912-154cb2906c72",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33a53058-a18b-4284-8353-f254cfbb9f90",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e7dcc684-13f6-4a30-906f-f09492bdd7c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33a53058-a18b-4284-8353-f254cfbb9f90",
                    "LayerId": "01e4fba6-3f81-41f2-9294-c1f5fe15d720"
                }
            ]
        },
        {
            "id": "4813a596-6ca3-4d6d-89c1-aef9fe929cef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f7a53bc-f6ee-411d-9ae2-c86c8f6182ae",
            "compositeImage": {
                "id": "91af97b0-f42c-4e7f-813c-4d451c09971c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4813a596-6ca3-4d6d-89c1-aef9fe929cef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b392f364-99de-4458-9c20-d1133910f493",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4813a596-6ca3-4d6d-89c1-aef9fe929cef",
                    "LayerId": "01e4fba6-3f81-41f2-9294-c1f5fe15d720"
                }
            ]
        },
        {
            "id": "0dfd171a-fc7b-4551-a35c-327912dd1d76",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f7a53bc-f6ee-411d-9ae2-c86c8f6182ae",
            "compositeImage": {
                "id": "a53679ca-841b-4628-8f8f-de73671557f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0dfd171a-fc7b-4551-a35c-327912dd1d76",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "06d36b0c-e248-447a-8180-4eb9eceede26",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0dfd171a-fc7b-4551-a35c-327912dd1d76",
                    "LayerId": "01e4fba6-3f81-41f2-9294-c1f5fe15d720"
                }
            ]
        },
        {
            "id": "4263b398-b820-423d-8d63-0311f477d6cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f7a53bc-f6ee-411d-9ae2-c86c8f6182ae",
            "compositeImage": {
                "id": "9404c629-2d3f-41bf-a6cc-eb5dca6f50db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4263b398-b820-423d-8d63-0311f477d6cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "112614ea-f54d-4649-af1c-099d34526f0e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4263b398-b820-423d-8d63-0311f477d6cc",
                    "LayerId": "01e4fba6-3f81-41f2-9294-c1f5fe15d720"
                }
            ]
        },
        {
            "id": "34221634-0989-4329-b90b-31337bffd85b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f7a53bc-f6ee-411d-9ae2-c86c8f6182ae",
            "compositeImage": {
                "id": "c8579867-4f23-4d42-8f38-8d26ab20bac2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34221634-0989-4329-b90b-31337bffd85b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bdb0347f-2014-4516-8f17-003c9294fc89",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34221634-0989-4329-b90b-31337bffd85b",
                    "LayerId": "01e4fba6-3f81-41f2-9294-c1f5fe15d720"
                }
            ]
        },
        {
            "id": "dc729753-3ff6-49b2-9645-e19ad27f3802",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f7a53bc-f6ee-411d-9ae2-c86c8f6182ae",
            "compositeImage": {
                "id": "c622a499-90bc-4f62-92ee-d883eb9d3c03",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dc729753-3ff6-49b2-9645-e19ad27f3802",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "10142b69-f6f7-4037-96d0-6843372c4405",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc729753-3ff6-49b2-9645-e19ad27f3802",
                    "LayerId": "01e4fba6-3f81-41f2-9294-c1f5fe15d720"
                }
            ]
        },
        {
            "id": "6156dd0d-f7c2-4dcf-9c8b-87bd88d6f0e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f7a53bc-f6ee-411d-9ae2-c86c8f6182ae",
            "compositeImage": {
                "id": "c425afd4-164b-42c5-a7d0-63914dd075d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6156dd0d-f7c2-4dcf-9c8b-87bd88d6f0e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f8bc747-6928-4a8e-98c1-53a83ea2638c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6156dd0d-f7c2-4dcf-9c8b-87bd88d6f0e7",
                    "LayerId": "01e4fba6-3f81-41f2-9294-c1f5fe15d720"
                }
            ]
        },
        {
            "id": "9e3e7a21-0fcf-491b-a020-fc6a32a52bdd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f7a53bc-f6ee-411d-9ae2-c86c8f6182ae",
            "compositeImage": {
                "id": "545c9fd1-82b4-44f0-a474-dcef67bf3510",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e3e7a21-0fcf-491b-a020-fc6a32a52bdd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db655a21-b623-4ec1-a071-0d45ed39343c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e3e7a21-0fcf-491b-a020-fc6a32a52bdd",
                    "LayerId": "01e4fba6-3f81-41f2-9294-c1f5fe15d720"
                }
            ]
        },
        {
            "id": "7cf5cc85-cb62-406c-baef-52f6a333da9e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f7a53bc-f6ee-411d-9ae2-c86c8f6182ae",
            "compositeImage": {
                "id": "7bfa31de-e3ad-472a-8fd9-340acd42499a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7cf5cc85-cb62-406c-baef-52f6a333da9e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab60d56b-c480-46b2-94fe-87155a7887b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7cf5cc85-cb62-406c-baef-52f6a333da9e",
                    "LayerId": "01e4fba6-3f81-41f2-9294-c1f5fe15d720"
                }
            ]
        },
        {
            "id": "8b0197ff-589f-4c8b-9fe9-2d96835624e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f7a53bc-f6ee-411d-9ae2-c86c8f6182ae",
            "compositeImage": {
                "id": "4fc388ad-64b1-436e-8511-9efe748ef0a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b0197ff-589f-4c8b-9fe9-2d96835624e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56989989-4a52-4c3b-a357-1179087eccce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b0197ff-589f-4c8b-9fe9-2d96835624e3",
                    "LayerId": "01e4fba6-3f81-41f2-9294-c1f5fe15d720"
                }
            ]
        },
        {
            "id": "ec8a25ab-1f64-4a95-a9a3-971e1fb8138e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f7a53bc-f6ee-411d-9ae2-c86c8f6182ae",
            "compositeImage": {
                "id": "5549967b-b6c6-465a-bf8f-242b1af53e03",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec8a25ab-1f64-4a95-a9a3-971e1fb8138e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eae82926-86ad-4b34-823e-27fc3086ed4e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec8a25ab-1f64-4a95-a9a3-971e1fb8138e",
                    "LayerId": "01e4fba6-3f81-41f2-9294-c1f5fe15d720"
                }
            ]
        },
        {
            "id": "4ab86ce0-881f-4011-9376-9ce3a2dc58e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f7a53bc-f6ee-411d-9ae2-c86c8f6182ae",
            "compositeImage": {
                "id": "05513540-5547-4485-8438-37d1699305d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ab86ce0-881f-4011-9376-9ce3a2dc58e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f99db478-feee-40bf-b4d3-9b8c2da32f7b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ab86ce0-881f-4011-9376-9ce3a2dc58e2",
                    "LayerId": "01e4fba6-3f81-41f2-9294-c1f5fe15d720"
                }
            ]
        },
        {
            "id": "4f2be124-3685-4506-aaec-81433826962e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f7a53bc-f6ee-411d-9ae2-c86c8f6182ae",
            "compositeImage": {
                "id": "164b8697-1b2b-4570-8954-46ff6ca84793",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f2be124-3685-4506-aaec-81433826962e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "322ede95-01a8-4f34-8649-0974222effb9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f2be124-3685-4506-aaec-81433826962e",
                    "LayerId": "01e4fba6-3f81-41f2-9294-c1f5fe15d720"
                }
            ]
        },
        {
            "id": "d282c44b-fb4a-46fb-9ab9-1c765edba33a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f7a53bc-f6ee-411d-9ae2-c86c8f6182ae",
            "compositeImage": {
                "id": "cb9e98e9-fce0-4d31-a9de-d07f7c450767",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d282c44b-fb4a-46fb-9ab9-1c765edba33a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a46115e3-090b-451e-a381-f00d5ddc7a0e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d282c44b-fb4a-46fb-9ab9-1c765edba33a",
                    "LayerId": "01e4fba6-3f81-41f2-9294-c1f5fe15d720"
                }
            ]
        },
        {
            "id": "e3faaace-09a2-4f56-95ab-6be4674f55c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f7a53bc-f6ee-411d-9ae2-c86c8f6182ae",
            "compositeImage": {
                "id": "571bf7f9-7301-4c55-8f28-b7ece4867b36",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3faaace-09a2-4f56-95ab-6be4674f55c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f8952da-c543-491e-bcba-5ef5ec979943",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3faaace-09a2-4f56-95ab-6be4674f55c6",
                    "LayerId": "01e4fba6-3f81-41f2-9294-c1f5fe15d720"
                }
            ]
        },
        {
            "id": "0aceebc5-5dd0-4f9e-8b93-c6430ec97893",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f7a53bc-f6ee-411d-9ae2-c86c8f6182ae",
            "compositeImage": {
                "id": "62763062-33a8-45b5-ba5d-d11b191c5d1e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0aceebc5-5dd0-4f9e-8b93-c6430ec97893",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "772db23c-1c44-43cc-bd9d-50f2e209a832",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0aceebc5-5dd0-4f9e-8b93-c6430ec97893",
                    "LayerId": "01e4fba6-3f81-41f2-9294-c1f5fe15d720"
                }
            ]
        },
        {
            "id": "fd71ef0f-aae1-45c3-bb51-878aeb467a12",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f7a53bc-f6ee-411d-9ae2-c86c8f6182ae",
            "compositeImage": {
                "id": "1f0aa42f-4ad2-471c-a5b7-fd649112b04c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd71ef0f-aae1-45c3-bb51-878aeb467a12",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30eadf20-04e5-42cf-a268-7d1a338dc506",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd71ef0f-aae1-45c3-bb51-878aeb467a12",
                    "LayerId": "01e4fba6-3f81-41f2-9294-c1f5fe15d720"
                }
            ]
        },
        {
            "id": "3d4c5b0c-3d52-4624-8c9d-70b88e9ee2d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f7a53bc-f6ee-411d-9ae2-c86c8f6182ae",
            "compositeImage": {
                "id": "1cd8518a-bbae-4ac4-b387-1bc837186437",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d4c5b0c-3d52-4624-8c9d-70b88e9ee2d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3867eb2c-a7b6-479a-a34b-bef02c354ee7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d4c5b0c-3d52-4624-8c9d-70b88e9ee2d4",
                    "LayerId": "01e4fba6-3f81-41f2-9294-c1f5fe15d720"
                }
            ]
        },
        {
            "id": "974050ea-86d9-46cc-be28-61afd79dd706",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f7a53bc-f6ee-411d-9ae2-c86c8f6182ae",
            "compositeImage": {
                "id": "cb3ef91f-dd13-472c-9005-9ff4d451b65d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "974050ea-86d9-46cc-be28-61afd79dd706",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3002b47-53e0-445a-bce9-60b08d28d21c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "974050ea-86d9-46cc-be28-61afd79dd706",
                    "LayerId": "01e4fba6-3f81-41f2-9294-c1f5fe15d720"
                }
            ]
        },
        {
            "id": "010528f8-8c49-4eff-86aa-32554316d2be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f7a53bc-f6ee-411d-9ae2-c86c8f6182ae",
            "compositeImage": {
                "id": "b75da3e3-25da-4d1f-bfae-1fecb15fd2bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "010528f8-8c49-4eff-86aa-32554316d2be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cdeb848c-dc71-4672-9882-867b3afac5dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "010528f8-8c49-4eff-86aa-32554316d2be",
                    "LayerId": "01e4fba6-3f81-41f2-9294-c1f5fe15d720"
                }
            ]
        },
        {
            "id": "4b041b29-6fa4-4320-9dfa-db641bb2662f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f7a53bc-f6ee-411d-9ae2-c86c8f6182ae",
            "compositeImage": {
                "id": "1ac6eb15-cff8-4b5c-b2e3-b8ba42468e8f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b041b29-6fa4-4320-9dfa-db641bb2662f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "36152c65-5fbe-449f-9ab3-291905020623",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b041b29-6fa4-4320-9dfa-db641bb2662f",
                    "LayerId": "01e4fba6-3f81-41f2-9294-c1f5fe15d720"
                }
            ]
        },
        {
            "id": "27a243bd-4ad5-4b47-abd0-c069e29b0cba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f7a53bc-f6ee-411d-9ae2-c86c8f6182ae",
            "compositeImage": {
                "id": "b7331892-4f55-4e62-9e8e-7b26ef55d187",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "27a243bd-4ad5-4b47-abd0-c069e29b0cba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "77acbacc-e7ca-4281-899a-1ef8b35f6cad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "27a243bd-4ad5-4b47-abd0-c069e29b0cba",
                    "LayerId": "01e4fba6-3f81-41f2-9294-c1f5fe15d720"
                }
            ]
        },
        {
            "id": "448184ee-4a12-45dd-a6a5-3d5b5c219ec0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f7a53bc-f6ee-411d-9ae2-c86c8f6182ae",
            "compositeImage": {
                "id": "8248e094-ffa3-4929-8378-a6f3712ae7dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "448184ee-4a12-45dd-a6a5-3d5b5c219ec0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "38def16a-2c48-44cc-a672-8fd932ac45dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "448184ee-4a12-45dd-a6a5-3d5b5c219ec0",
                    "LayerId": "01e4fba6-3f81-41f2-9294-c1f5fe15d720"
                }
            ]
        },
        {
            "id": "b660c434-4c9d-4a6d-a713-a492cdbc74f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f7a53bc-f6ee-411d-9ae2-c86c8f6182ae",
            "compositeImage": {
                "id": "f8214cec-da56-43a5-b12a-b9d80603a398",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b660c434-4c9d-4a6d-a713-a492cdbc74f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee280535-1f86-4af7-9de0-9619ec434bd9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b660c434-4c9d-4a6d-a713-a492cdbc74f9",
                    "LayerId": "01e4fba6-3f81-41f2-9294-c1f5fe15d720"
                }
            ]
        },
        {
            "id": "ba77dd88-6629-4817-8d2b-0d2bdeeb4b4a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f7a53bc-f6ee-411d-9ae2-c86c8f6182ae",
            "compositeImage": {
                "id": "d0452f91-d5fb-4ad6-94c5-ef1205aae04c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba77dd88-6629-4817-8d2b-0d2bdeeb4b4a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "112577ad-519c-4d27-bfa2-55753fa17f47",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba77dd88-6629-4817-8d2b-0d2bdeeb4b4a",
                    "LayerId": "01e4fba6-3f81-41f2-9294-c1f5fe15d720"
                }
            ]
        },
        {
            "id": "a00f8637-357b-4e3c-9c07-a28190ebb0fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f7a53bc-f6ee-411d-9ae2-c86c8f6182ae",
            "compositeImage": {
                "id": "a823c79a-0052-41ea-a7e8-b4a0782c077c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a00f8637-357b-4e3c-9c07-a28190ebb0fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7ad70ff-37cb-438a-b10b-fafe98f8756f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a00f8637-357b-4e3c-9c07-a28190ebb0fb",
                    "LayerId": "01e4fba6-3f81-41f2-9294-c1f5fe15d720"
                }
            ]
        },
        {
            "id": "81ae212a-355b-47fa-949b-e36ae7ed2389",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f7a53bc-f6ee-411d-9ae2-c86c8f6182ae",
            "compositeImage": {
                "id": "cfd7c0d7-3425-4771-a9fb-dffeeec7a049",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81ae212a-355b-47fa-949b-e36ae7ed2389",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b6c20c0e-572f-43a7-9894-9c81964d55e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81ae212a-355b-47fa-949b-e36ae7ed2389",
                    "LayerId": "01e4fba6-3f81-41f2-9294-c1f5fe15d720"
                }
            ]
        },
        {
            "id": "d11cd765-7f98-4f2c-b503-b821b2b54f68",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f7a53bc-f6ee-411d-9ae2-c86c8f6182ae",
            "compositeImage": {
                "id": "fb38da5f-d581-4876-8bca-428577122775",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d11cd765-7f98-4f2c-b503-b821b2b54f68",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd287617-82f1-4201-ad46-7a03faf7ddb9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d11cd765-7f98-4f2c-b503-b821b2b54f68",
                    "LayerId": "01e4fba6-3f81-41f2-9294-c1f5fe15d720"
                }
            ]
        },
        {
            "id": "8695612d-e564-4cc9-b216-0c9a5365d686",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f7a53bc-f6ee-411d-9ae2-c86c8f6182ae",
            "compositeImage": {
                "id": "06953381-5124-4a5c-a1d9-173ce1d5816a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8695612d-e564-4cc9-b216-0c9a5365d686",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "77fd83f9-06dc-484a-b288-6ae6cb89278f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8695612d-e564-4cc9-b216-0c9a5365d686",
                    "LayerId": "01e4fba6-3f81-41f2-9294-c1f5fe15d720"
                }
            ]
        },
        {
            "id": "423d6cac-d981-479d-b2ca-1281653f3ae9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f7a53bc-f6ee-411d-9ae2-c86c8f6182ae",
            "compositeImage": {
                "id": "72af496c-4a93-493c-ae96-1057ca61c3ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "423d6cac-d981-479d-b2ca-1281653f3ae9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "402de2ae-9b99-4893-b136-b713007e8626",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "423d6cac-d981-479d-b2ca-1281653f3ae9",
                    "LayerId": "01e4fba6-3f81-41f2-9294-c1f5fe15d720"
                }
            ]
        },
        {
            "id": "c1d878d0-978e-45fa-80f0-e3f8858b14c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f7a53bc-f6ee-411d-9ae2-c86c8f6182ae",
            "compositeImage": {
                "id": "67ebe7c3-2f51-41da-9ad5-a452c4408f96",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1d878d0-978e-45fa-80f0-e3f8858b14c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c7bd7c76-2b2e-4e99-b075-0c497ecd2e3a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1d878d0-978e-45fa-80f0-e3f8858b14c0",
                    "LayerId": "01e4fba6-3f81-41f2-9294-c1f5fe15d720"
                }
            ]
        },
        {
            "id": "e9a09810-fb30-43bb-a377-400387f53d46",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f7a53bc-f6ee-411d-9ae2-c86c8f6182ae",
            "compositeImage": {
                "id": "e6356c98-889e-43b7-909a-08f66526d3ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9a09810-fb30-43bb-a377-400387f53d46",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "165ae109-cc59-44a7-a875-6b1d4d29dcb1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9a09810-fb30-43bb-a377-400387f53d46",
                    "LayerId": "01e4fba6-3f81-41f2-9294-c1f5fe15d720"
                }
            ]
        },
        {
            "id": "4c0e9072-3bc6-4ef9-a939-ec86991dea90",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f7a53bc-f6ee-411d-9ae2-c86c8f6182ae",
            "compositeImage": {
                "id": "f4dd3427-d1b4-4bf6-95e0-90d003e406df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c0e9072-3bc6-4ef9-a939-ec86991dea90",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b2286c8f-eb0d-4034-8e62-3d73da87b25d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c0e9072-3bc6-4ef9-a939-ec86991dea90",
                    "LayerId": "01e4fba6-3f81-41f2-9294-c1f5fe15d720"
                }
            ]
        },
        {
            "id": "f3767b75-90a3-4c7a-a2b8-0d522b6d50f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f7a53bc-f6ee-411d-9ae2-c86c8f6182ae",
            "compositeImage": {
                "id": "a077de8c-90e7-4816-b7d3-8489b79c646f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3767b75-90a3-4c7a-a2b8-0d522b6d50f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a44738f0-2c80-4421-ab68-c7cf57697f00",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3767b75-90a3-4c7a-a2b8-0d522b6d50f8",
                    "LayerId": "01e4fba6-3f81-41f2-9294-c1f5fe15d720"
                }
            ]
        },
        {
            "id": "2199ed9d-73e0-48f9-9355-23ce9199b802",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f7a53bc-f6ee-411d-9ae2-c86c8f6182ae",
            "compositeImage": {
                "id": "8391e65d-40f7-4482-ba8d-fd861acf4d12",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2199ed9d-73e0-48f9-9355-23ce9199b802",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aca391b4-2a47-4e5a-80f6-16df5e405ea3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2199ed9d-73e0-48f9-9355-23ce9199b802",
                    "LayerId": "01e4fba6-3f81-41f2-9294-c1f5fe15d720"
                }
            ]
        },
        {
            "id": "58548f88-8e8a-4692-8834-ddadb1769ba2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f7a53bc-f6ee-411d-9ae2-c86c8f6182ae",
            "compositeImage": {
                "id": "db12f572-3454-4541-9a89-94566864105e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "58548f88-8e8a-4692-8834-ddadb1769ba2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5caa1a7a-cc79-4f7c-ae37-9af71768f6d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "58548f88-8e8a-4692-8834-ddadb1769ba2",
                    "LayerId": "01e4fba6-3f81-41f2-9294-c1f5fe15d720"
                }
            ]
        },
        {
            "id": "d9e432f4-d4c5-4d4e-a482-e9c74bfd0d2b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f7a53bc-f6ee-411d-9ae2-c86c8f6182ae",
            "compositeImage": {
                "id": "df64c162-17fe-4cb0-88a3-1eeca9c21514",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d9e432f4-d4c5-4d4e-a482-e9c74bfd0d2b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac6debbb-d300-4949-bf18-ad05dc5886c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9e432f4-d4c5-4d4e-a482-e9c74bfd0d2b",
                    "LayerId": "01e4fba6-3f81-41f2-9294-c1f5fe15d720"
                }
            ]
        },
        {
            "id": "a3e72c7e-4257-47eb-88dd-c69b1fd7376a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f7a53bc-f6ee-411d-9ae2-c86c8f6182ae",
            "compositeImage": {
                "id": "f04d534b-6552-42f0-b461-5d4757ad1fb3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3e72c7e-4257-47eb-88dd-c69b1fd7376a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91616a08-5cfb-43d9-a9e4-de42c5e0bac5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3e72c7e-4257-47eb-88dd-c69b1fd7376a",
                    "LayerId": "01e4fba6-3f81-41f2-9294-c1f5fe15d720"
                }
            ]
        },
        {
            "id": "afe0de48-ed0f-4ca4-9c60-038d4afb6f24",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f7a53bc-f6ee-411d-9ae2-c86c8f6182ae",
            "compositeImage": {
                "id": "b170ba4b-b7e1-4c1f-87f1-36145b9fb912",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "afe0de48-ed0f-4ca4-9c60-038d4afb6f24",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58d33d2d-eaa6-43a8-8cf3-8c873f7a08ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "afe0de48-ed0f-4ca4-9c60-038d4afb6f24",
                    "LayerId": "01e4fba6-3f81-41f2-9294-c1f5fe15d720"
                }
            ]
        },
        {
            "id": "e2a9c8c4-a46f-4b11-bf2e-73c74fe6f593",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f7a53bc-f6ee-411d-9ae2-c86c8f6182ae",
            "compositeImage": {
                "id": "20e481cd-425b-4f16-be89-c4c8e6116b9f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2a9c8c4-a46f-4b11-bf2e-73c74fe6f593",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c3f947b-3826-4b6f-8cc9-e264b9b0ea35",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2a9c8c4-a46f-4b11-bf2e-73c74fe6f593",
                    "LayerId": "01e4fba6-3f81-41f2-9294-c1f5fe15d720"
                }
            ]
        },
        {
            "id": "c433fb56-43ee-4b66-8e1c-0b0cf5cbfdc6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f7a53bc-f6ee-411d-9ae2-c86c8f6182ae",
            "compositeImage": {
                "id": "1a73b279-0d18-4762-99d6-d0ed148703d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c433fb56-43ee-4b66-8e1c-0b0cf5cbfdc6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de19f26c-063e-46f0-a191-44fe896ded45",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c433fb56-43ee-4b66-8e1c-0b0cf5cbfdc6",
                    "LayerId": "01e4fba6-3f81-41f2-9294-c1f5fe15d720"
                }
            ]
        },
        {
            "id": "c0427432-12ab-4201-b2f9-31445cf31718",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f7a53bc-f6ee-411d-9ae2-c86c8f6182ae",
            "compositeImage": {
                "id": "d9249841-b17f-4760-bec2-af535e3e6973",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0427432-12ab-4201-b2f9-31445cf31718",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8b692b4-9bf3-4894-970b-75a7d58d36a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0427432-12ab-4201-b2f9-31445cf31718",
                    "LayerId": "01e4fba6-3f81-41f2-9294-c1f5fe15d720"
                }
            ]
        },
        {
            "id": "db52c402-b678-4935-9526-57fde56bad41",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f7a53bc-f6ee-411d-9ae2-c86c8f6182ae",
            "compositeImage": {
                "id": "477de093-fff0-4cb5-a319-f99c7219ca5f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db52c402-b678-4935-9526-57fde56bad41",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ad5c9c3-3ab9-4d9a-ae02-dc2d30969de2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db52c402-b678-4935-9526-57fde56bad41",
                    "LayerId": "01e4fba6-3f81-41f2-9294-c1f5fe15d720"
                }
            ]
        },
        {
            "id": "96f95a45-faa3-40fb-a737-31011c63e907",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f7a53bc-f6ee-411d-9ae2-c86c8f6182ae",
            "compositeImage": {
                "id": "a2919e5d-3076-4df3-a23f-5feadb324f35",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96f95a45-faa3-40fb-a737-31011c63e907",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "593a5dab-c684-4bab-b0db-55bc68a3e899",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96f95a45-faa3-40fb-a737-31011c63e907",
                    "LayerId": "01e4fba6-3f81-41f2-9294-c1f5fe15d720"
                }
            ]
        },
        {
            "id": "80a9ed39-beb2-4092-b614-87a9e273a1b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f7a53bc-f6ee-411d-9ae2-c86c8f6182ae",
            "compositeImage": {
                "id": "8a713600-d845-4aca-be3e-5b8a0074d268",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "80a9ed39-beb2-4092-b614-87a9e273a1b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6d3e305-cbca-4e10-807a-3ec9a066569c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80a9ed39-beb2-4092-b614-87a9e273a1b6",
                    "LayerId": "01e4fba6-3f81-41f2-9294-c1f5fe15d720"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "01e4fba6-3f81-41f2-9294-c1f5fe15d720",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4f7a53bc-f6ee-411d-9ae2-c86c8f6182ae",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 0,
    "yorig": 0
}