{
    "id": "4bc4f7af-edfa-4764-b9fc-84e51e24debb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSpit01",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 7,
    "bbox_right": 22,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "eadeb3be-c5ca-4abb-b3da-8043d3318e09",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4bc4f7af-edfa-4764-b9fc-84e51e24debb",
            "compositeImage": {
                "id": "352931d9-26ff-4316-8027-7e3a9a655452",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eadeb3be-c5ca-4abb-b3da-8043d3318e09",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd6353c4-0afc-4a0f-a610-7339fd10e337",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eadeb3be-c5ca-4abb-b3da-8043d3318e09",
                    "LayerId": "0f0c9b41-9e3f-4c88-9854-27f06a4d89eb"
                }
            ]
        },
        {
            "id": "cb4e07c4-8df5-4154-8a9d-e2d14ac69728",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4bc4f7af-edfa-4764-b9fc-84e51e24debb",
            "compositeImage": {
                "id": "b78a7ad5-c066-46da-8f1a-c11aabdf8ad8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb4e07c4-8df5-4154-8a9d-e2d14ac69728",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8283b5d-90e8-4df2-b167-316f8ee2ae90",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb4e07c4-8df5-4154-8a9d-e2d14ac69728",
                    "LayerId": "0f0c9b41-9e3f-4c88-9854-27f06a4d89eb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "0f0c9b41-9e3f-4c88-9854-27f06a4d89eb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4bc4f7af-edfa-4764-b9fc-84e51e24debb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}