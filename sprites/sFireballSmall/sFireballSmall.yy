{
    "id": "5ddbc78f-d84c-4817-8fef-fe14ac1a4335",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sFireballSmall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 3,
    "bbox_right": 25,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "463b8c3f-4c2d-449a-a485-c07100f625a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5ddbc78f-d84c-4817-8fef-fe14ac1a4335",
            "compositeImage": {
                "id": "46daf8e6-1c0e-4929-ad48-5e3fead25dea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "463b8c3f-4c2d-449a-a485-c07100f625a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "06d86a5f-aefe-47ae-9b93-f5781eea200e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "463b8c3f-4c2d-449a-a485-c07100f625a1",
                    "LayerId": "f8d86277-94d5-44e0-be44-a3967d253e27"
                }
            ]
        },
        {
            "id": "08ff280e-8441-4852-bd88-f8dbd9d1cee9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5ddbc78f-d84c-4817-8fef-fe14ac1a4335",
            "compositeImage": {
                "id": "e66eab33-de78-4f41-a01f-bb077a71510e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08ff280e-8441-4852-bd88-f8dbd9d1cee9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88a722ff-1d9c-4655-af9a-59777c007df9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08ff280e-8441-4852-bd88-f8dbd9d1cee9",
                    "LayerId": "f8d86277-94d5-44e0-be44-a3967d253e27"
                }
            ]
        },
        {
            "id": "a40569d0-078f-45cc-9f5d-a7f04717d3fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5ddbc78f-d84c-4817-8fef-fe14ac1a4335",
            "compositeImage": {
                "id": "c0727188-600a-4887-a5de-048cc913fd4d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a40569d0-078f-45cc-9f5d-a7f04717d3fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30905755-5ec4-4e85-8434-aa0e42f35253",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a40569d0-078f-45cc-9f5d-a7f04717d3fc",
                    "LayerId": "f8d86277-94d5-44e0-be44-a3967d253e27"
                }
            ]
        },
        {
            "id": "76810a01-50e8-4a63-be29-bfdfda55ec3f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5ddbc78f-d84c-4817-8fef-fe14ac1a4335",
            "compositeImage": {
                "id": "b99cc388-2e90-4806-adb9-f7da2466c411",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76810a01-50e8-4a63-be29-bfdfda55ec3f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "306903fe-24ac-43de-b551-15c311a80ad1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76810a01-50e8-4a63-be29-bfdfda55ec3f",
                    "LayerId": "f8d86277-94d5-44e0-be44-a3967d253e27"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "f8d86277-94d5-44e0-be44-a3967d253e27",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5ddbc78f-d84c-4817-8fef-fe14ac1a4335",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}