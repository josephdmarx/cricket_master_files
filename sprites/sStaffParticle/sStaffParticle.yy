{
    "id": "8fa7a23a-7cdb-4721-b45f-1a8a88463921",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sStaffParticle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 33,
    "bbox_left": 55,
    "bbox_right": 62,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bde4a5b6-f9ba-444d-8a2a-93f244ffcf98",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8fa7a23a-7cdb-4721-b45f-1a8a88463921",
            "compositeImage": {
                "id": "ea8b32c9-919f-4d5c-9a96-62212ef0013e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bde4a5b6-f9ba-444d-8a2a-93f244ffcf98",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a838185-7092-40fd-b7a6-fa072f6eed6f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bde4a5b6-f9ba-444d-8a2a-93f244ffcf98",
                    "LayerId": "69068133-cbef-44f3-a9f8-627881d8960a"
                }
            ]
        },
        {
            "id": "f0b75f48-675e-4f70-816e-dbdfa2e47d9b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8fa7a23a-7cdb-4721-b45f-1a8a88463921",
            "compositeImage": {
                "id": "0336e1d8-033a-42f3-9016-7d64dc729c90",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0b75f48-675e-4f70-816e-dbdfa2e47d9b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f994db66-eada-46b2-84eb-d85986381e4e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0b75f48-675e-4f70-816e-dbdfa2e47d9b",
                    "LayerId": "69068133-cbef-44f3-a9f8-627881d8960a"
                }
            ]
        },
        {
            "id": "30615504-3dd5-433f-9acd-65506f98455e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8fa7a23a-7cdb-4721-b45f-1a8a88463921",
            "compositeImage": {
                "id": "ce08c812-5387-487e-86bb-2669f3148948",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "30615504-3dd5-433f-9acd-65506f98455e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a235fac-5b4b-44d1-a538-b3bef43bf370",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "30615504-3dd5-433f-9acd-65506f98455e",
                    "LayerId": "69068133-cbef-44f3-a9f8-627881d8960a"
                }
            ]
        },
        {
            "id": "9a8395a1-b772-4d4a-b649-52f43295bec6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8fa7a23a-7cdb-4721-b45f-1a8a88463921",
            "compositeImage": {
                "id": "4c60e187-d6fd-4e0d-bdb7-fc14e994d1fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a8395a1-b772-4d4a-b649-52f43295bec6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7111b5e-3cdb-4777-90f1-c54058a24516",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a8395a1-b772-4d4a-b649-52f43295bec6",
                    "LayerId": "69068133-cbef-44f3-a9f8-627881d8960a"
                }
            ]
        },
        {
            "id": "5dea7833-f1ef-4502-b34e-2827023ed1cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8fa7a23a-7cdb-4721-b45f-1a8a88463921",
            "compositeImage": {
                "id": "2c152b83-c108-410f-b198-6fbbd9d6b412",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5dea7833-f1ef-4502-b34e-2827023ed1cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9e42f7e-c147-4c83-ba5a-ee0636ede812",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5dea7833-f1ef-4502-b34e-2827023ed1cf",
                    "LayerId": "69068133-cbef-44f3-a9f8-627881d8960a"
                }
            ]
        },
        {
            "id": "8ba93472-87b7-462b-9eaa-299c05db2c92",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8fa7a23a-7cdb-4721-b45f-1a8a88463921",
            "compositeImage": {
                "id": "a73c645f-4ec4-4bd7-a575-ebb12f1647a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ba93472-87b7-462b-9eaa-299c05db2c92",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "184b74d4-e4e1-441f-a128-a570f276e2bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ba93472-87b7-462b-9eaa-299c05db2c92",
                    "LayerId": "69068133-cbef-44f3-a9f8-627881d8960a"
                }
            ]
        },
        {
            "id": "8ff61755-efe3-43a5-bcc0-a10ea62a14ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8fa7a23a-7cdb-4721-b45f-1a8a88463921",
            "compositeImage": {
                "id": "776fffff-8bd4-4e15-9e14-0d803695628e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ff61755-efe3-43a5-bcc0-a10ea62a14ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d12847f6-b891-489a-a0c9-268d6aeb778e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ff61755-efe3-43a5-bcc0-a10ea62a14ae",
                    "LayerId": "69068133-cbef-44f3-a9f8-627881d8960a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "69068133-cbef-44f3-a9f8-627881d8960a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8fa7a23a-7cdb-4721-b45f-1a8a88463921",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}