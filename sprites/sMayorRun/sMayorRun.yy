{
    "id": "80a4dd11-feea-41ef-9ba1-ffc95297586e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sMayorRun",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 42,
    "bbox_right": 88,
    "bbox_top": 28,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cf4f5501-9787-4327-8963-f4745c450829",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "80a4dd11-feea-41ef-9ba1-ffc95297586e",
            "compositeImage": {
                "id": "0d560a70-ef81-4eba-ba56-7705d057b827",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf4f5501-9787-4327-8963-f4745c450829",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c493b4cf-dfbb-4aa8-aaa6-45740ebb7bbf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf4f5501-9787-4327-8963-f4745c450829",
                    "LayerId": "8cebc1aa-d637-48ab-aded-495df1d436f4"
                }
            ]
        },
        {
            "id": "38c07947-8749-472b-9c07-a801ca0a168d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "80a4dd11-feea-41ef-9ba1-ffc95297586e",
            "compositeImage": {
                "id": "46df06eb-3f43-4ab7-814d-ac0fe310bccd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38c07947-8749-472b-9c07-a801ca0a168d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b072b180-341c-4178-ae70-52376874e568",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38c07947-8749-472b-9c07-a801ca0a168d",
                    "LayerId": "8cebc1aa-d637-48ab-aded-495df1d436f4"
                }
            ]
        },
        {
            "id": "360326af-2021-4ff9-a6b9-1bfe378bfef0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "80a4dd11-feea-41ef-9ba1-ffc95297586e",
            "compositeImage": {
                "id": "3706b7fe-b653-4153-9df3-d2a36a6abbe2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "360326af-2021-4ff9-a6b9-1bfe378bfef0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b92d30c9-1e76-4c73-b527-588246fe48a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "360326af-2021-4ff9-a6b9-1bfe378bfef0",
                    "LayerId": "8cebc1aa-d637-48ab-aded-495df1d436f4"
                }
            ]
        },
        {
            "id": "3d7e3391-3481-499c-9788-033a081a6984",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "80a4dd11-feea-41ef-9ba1-ffc95297586e",
            "compositeImage": {
                "id": "09ff273c-6242-4316-b83c-bcfadedd10fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d7e3391-3481-499c-9788-033a081a6984",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89871371-343e-4018-aa40-5dc44a11b3ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d7e3391-3481-499c-9788-033a081a6984",
                    "LayerId": "8cebc1aa-d637-48ab-aded-495df1d436f4"
                }
            ]
        },
        {
            "id": "46e96c71-5b57-49cd-aa73-f51ab339d2b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "80a4dd11-feea-41ef-9ba1-ffc95297586e",
            "compositeImage": {
                "id": "31b66a68-17ab-4b08-a6b4-423d41b9d234",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46e96c71-5b57-49cd-aa73-f51ab339d2b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8410abb4-a133-495a-acaa-4cad635ebd9c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46e96c71-5b57-49cd-aa73-f51ab339d2b4",
                    "LayerId": "8cebc1aa-d637-48ab-aded-495df1d436f4"
                }
            ]
        },
        {
            "id": "1384a314-b894-49cc-84d7-caa2c27792b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "80a4dd11-feea-41ef-9ba1-ffc95297586e",
            "compositeImage": {
                "id": "2bfeb67d-07b9-4627-acfa-1b30eca92b3d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1384a314-b894-49cc-84d7-caa2c27792b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b1fd8ce-d9d8-41a2-91d5-49b667b5a231",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1384a314-b894-49cc-84d7-caa2c27792b1",
                    "LayerId": "8cebc1aa-d637-48ab-aded-495df1d436f4"
                }
            ]
        },
        {
            "id": "56cdc34b-7d1c-464d-af60-9bce74d90054",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "80a4dd11-feea-41ef-9ba1-ffc95297586e",
            "compositeImage": {
                "id": "0890e9d6-3db5-4941-984d-1d9519c333b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56cdc34b-7d1c-464d-af60-9bce74d90054",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb887961-928b-4aaa-aaf6-987acc912373",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56cdc34b-7d1c-464d-af60-9bce74d90054",
                    "LayerId": "8cebc1aa-d637-48ab-aded-495df1d436f4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "8cebc1aa-d637-48ab-aded-495df1d436f4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "80a4dd11-feea-41ef-9ba1-ffc95297586e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}