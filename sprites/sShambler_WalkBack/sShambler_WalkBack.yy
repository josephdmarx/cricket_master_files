{
    "id": "b76f42e5-7dc6-4712-84aa-1a590771dc4d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sShambler_WalkBack",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 198,
    "bbox_left": 75,
    "bbox_right": 155,
    "bbox_top": 110,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cab17aa2-d6e3-484d-b775-dc33cd19ef89",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b76f42e5-7dc6-4712-84aa-1a590771dc4d",
            "compositeImage": {
                "id": "1343a581-d466-457a-aadb-3085937fe9b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cab17aa2-d6e3-484d-b775-dc33cd19ef89",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd4846f5-c01c-4920-9c5b-66d13063a17a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cab17aa2-d6e3-484d-b775-dc33cd19ef89",
                    "LayerId": "882a67f4-e522-464b-b72b-61890d157d81"
                }
            ]
        },
        {
            "id": "a9f09160-893c-40e7-879d-0865a05ad2d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b76f42e5-7dc6-4712-84aa-1a590771dc4d",
            "compositeImage": {
                "id": "5781e975-dc36-4c99-905b-3461c1e739ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a9f09160-893c-40e7-879d-0865a05ad2d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee6b987b-1686-47ca-a39c-39d2deca2377",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a9f09160-893c-40e7-879d-0865a05ad2d7",
                    "LayerId": "882a67f4-e522-464b-b72b-61890d157d81"
                }
            ]
        },
        {
            "id": "43a1a4da-a653-4c0b-99de-f09cddafdf5c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b76f42e5-7dc6-4712-84aa-1a590771dc4d",
            "compositeImage": {
                "id": "ea15f55c-5788-40c2-8824-1609aa1e3e68",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43a1a4da-a653-4c0b-99de-f09cddafdf5c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f4b34ffc-3ec7-4511-9995-46ac180cfcdd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43a1a4da-a653-4c0b-99de-f09cddafdf5c",
                    "LayerId": "882a67f4-e522-464b-b72b-61890d157d81"
                }
            ]
        },
        {
            "id": "92417d94-dd2c-4a3f-acc2-5d855e678133",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b76f42e5-7dc6-4712-84aa-1a590771dc4d",
            "compositeImage": {
                "id": "e6e49d66-2509-4017-977a-ef144c3737db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "92417d94-dd2c-4a3f-acc2-5d855e678133",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b35e6fb-906f-450a-9d6f-8903e7c7e4fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92417d94-dd2c-4a3f-acc2-5d855e678133",
                    "LayerId": "882a67f4-e522-464b-b72b-61890d157d81"
                }
            ]
        },
        {
            "id": "74a084b7-339a-4642-8b6a-40c8bd9d889e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b76f42e5-7dc6-4712-84aa-1a590771dc4d",
            "compositeImage": {
                "id": "59eddc4d-1157-43f0-afc2-c4843fc04441",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "74a084b7-339a-4642-8b6a-40c8bd9d889e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ec77ba3-bf55-4853-adc9-9267c79001b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "74a084b7-339a-4642-8b6a-40c8bd9d889e",
                    "LayerId": "882a67f4-e522-464b-b72b-61890d157d81"
                }
            ]
        },
        {
            "id": "39fa0b31-62c1-4492-b2cd-f5b945720d5d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b76f42e5-7dc6-4712-84aa-1a590771dc4d",
            "compositeImage": {
                "id": "83ab38a9-0170-4a30-aafb-3600a888d052",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39fa0b31-62c1-4492-b2cd-f5b945720d5d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "87244d09-6b1d-4e36-badc-b4dd0f14142b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39fa0b31-62c1-4492-b2cd-f5b945720d5d",
                    "LayerId": "882a67f4-e522-464b-b72b-61890d157d81"
                }
            ]
        },
        {
            "id": "f095e64f-0212-4185-b645-693d1a4b9b26",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b76f42e5-7dc6-4712-84aa-1a590771dc4d",
            "compositeImage": {
                "id": "300c040a-4c96-46e0-910e-4cae6a7f968b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f095e64f-0212-4185-b645-693d1a4b9b26",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5856c72-9259-4988-9a75-50d0df0126e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f095e64f-0212-4185-b645-693d1a4b9b26",
                    "LayerId": "882a67f4-e522-464b-b72b-61890d157d81"
                }
            ]
        },
        {
            "id": "82b96dcb-7a4f-4416-aa1c-1e6fd2fe1592",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b76f42e5-7dc6-4712-84aa-1a590771dc4d",
            "compositeImage": {
                "id": "a75b59cc-2443-434c-aa9f-b35898ca7021",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82b96dcb-7a4f-4416-aa1c-1e6fd2fe1592",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "98abff20-38b9-4a88-9a96-a4c0d6b2c4cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82b96dcb-7a4f-4416-aa1c-1e6fd2fe1592",
                    "LayerId": "882a67f4-e522-464b-b72b-61890d157d81"
                }
            ]
        },
        {
            "id": "ee7a84dc-6ded-41b1-afbd-175660f23f24",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b76f42e5-7dc6-4712-84aa-1a590771dc4d",
            "compositeImage": {
                "id": "627cfbc3-7cf5-4063-93f7-1e4fb64ca07e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee7a84dc-6ded-41b1-afbd-175660f23f24",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e38d2fcb-fdeb-43fc-a331-4600f60930c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee7a84dc-6ded-41b1-afbd-175660f23f24",
                    "LayerId": "882a67f4-e522-464b-b72b-61890d157d81"
                }
            ]
        },
        {
            "id": "d77288f1-8677-4b8c-885d-34eac8b981dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b76f42e5-7dc6-4712-84aa-1a590771dc4d",
            "compositeImage": {
                "id": "b51d8790-4f4c-4f03-b95a-7477c79babaa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d77288f1-8677-4b8c-885d-34eac8b981dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6fccb929-f905-473a-b7d1-63e48a95b73d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d77288f1-8677-4b8c-885d-34eac8b981dc",
                    "LayerId": "882a67f4-e522-464b-b72b-61890d157d81"
                }
            ]
        },
        {
            "id": "be1f8e54-d7b0-40bc-8639-5fc681c8d845",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b76f42e5-7dc6-4712-84aa-1a590771dc4d",
            "compositeImage": {
                "id": "79273f16-4165-459e-bd4b-1e6fea225834",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be1f8e54-d7b0-40bc-8639-5fc681c8d845",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c41e86c7-88e2-40ad-8679-a802149f85b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be1f8e54-d7b0-40bc-8639-5fc681c8d845",
                    "LayerId": "882a67f4-e522-464b-b72b-61890d157d81"
                }
            ]
        },
        {
            "id": "d6a7c803-67cd-4d9b-a254-c0b8950001c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b76f42e5-7dc6-4712-84aa-1a590771dc4d",
            "compositeImage": {
                "id": "5d55ff03-3e62-49f8-8213-975645b1fddd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6a7c803-67cd-4d9b-a254-c0b8950001c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "581aa423-5ff5-4e6b-87d1-bc9e2dd849e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6a7c803-67cd-4d9b-a254-c0b8950001c2",
                    "LayerId": "882a67f4-e522-464b-b72b-61890d157d81"
                }
            ]
        },
        {
            "id": "cd2c03e5-944a-42d3-91b5-e426887f55e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b76f42e5-7dc6-4712-84aa-1a590771dc4d",
            "compositeImage": {
                "id": "c89c6a37-43c9-4976-9473-8980547d350f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd2c03e5-944a-42d3-91b5-e426887f55e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45af77b0-ef99-46e9-bc8c-713afcace763",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd2c03e5-944a-42d3-91b5-e426887f55e0",
                    "LayerId": "882a67f4-e522-464b-b72b-61890d157d81"
                }
            ]
        },
        {
            "id": "af24edfb-b1a3-4276-8878-fb7aa19daa1b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b76f42e5-7dc6-4712-84aa-1a590771dc4d",
            "compositeImage": {
                "id": "d2ab7379-f17b-4f40-a08b-164654ed5704",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af24edfb-b1a3-4276-8878-fb7aa19daa1b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9046a8ed-ba2f-4f11-a4b2-71add2756723",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af24edfb-b1a3-4276-8878-fb7aa19daa1b",
                    "LayerId": "882a67f4-e522-464b-b72b-61890d157d81"
                }
            ]
        },
        {
            "id": "9a23a567-8484-426c-be7b-037ff09f1385",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b76f42e5-7dc6-4712-84aa-1a590771dc4d",
            "compositeImage": {
                "id": "4ba349ec-1858-44a0-a4cd-77904b125156",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a23a567-8484-426c-be7b-037ff09f1385",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df7ca009-94f2-4b03-80b7-8fb5f9b58507",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a23a567-8484-426c-be7b-037ff09f1385",
                    "LayerId": "882a67f4-e522-464b-b72b-61890d157d81"
                }
            ]
        },
        {
            "id": "f443068e-28df-42c0-a164-120afe57a2f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b76f42e5-7dc6-4712-84aa-1a590771dc4d",
            "compositeImage": {
                "id": "367e7647-16f1-4ce8-a5c3-8e1053eea2ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f443068e-28df-42c0-a164-120afe57a2f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f1632adb-086e-45bb-81aa-4c5e2bd369b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f443068e-28df-42c0-a164-120afe57a2f2",
                    "LayerId": "882a67f4-e522-464b-b72b-61890d157d81"
                }
            ]
        },
        {
            "id": "e589246f-d2cd-421f-aae4-2afd94f60525",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b76f42e5-7dc6-4712-84aa-1a590771dc4d",
            "compositeImage": {
                "id": "1e76d19f-8677-4134-ac6f-dd930dd8f3ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e589246f-d2cd-421f-aae4-2afd94f60525",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48dff1d2-3a3b-4ebf-a53a-156d819a585f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e589246f-d2cd-421f-aae4-2afd94f60525",
                    "LayerId": "882a67f4-e522-464b-b72b-61890d157d81"
                }
            ]
        },
        {
            "id": "a9422987-2dd7-4c36-a2e5-046941bca2c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b76f42e5-7dc6-4712-84aa-1a590771dc4d",
            "compositeImage": {
                "id": "c184d369-7cd5-459f-9469-1830ef77be8f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a9422987-2dd7-4c36-a2e5-046941bca2c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f39731f-3313-47cb-b902-4949a5243358",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a9422987-2dd7-4c36-a2e5-046941bca2c2",
                    "LayerId": "882a67f4-e522-464b-b72b-61890d157d81"
                }
            ]
        },
        {
            "id": "8d9921b7-5f8d-485e-8083-66332eab229e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b76f42e5-7dc6-4712-84aa-1a590771dc4d",
            "compositeImage": {
                "id": "aa772d2c-7a2f-4bb6-8ee2-6c187b94bd22",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d9921b7-5f8d-485e-8083-66332eab229e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4edc2263-56a3-4d48-bbd2-cd4c3b2033cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d9921b7-5f8d-485e-8083-66332eab229e",
                    "LayerId": "882a67f4-e522-464b-b72b-61890d157d81"
                }
            ]
        },
        {
            "id": "5457c52e-d3bd-46bd-9de0-e62b930a33aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b76f42e5-7dc6-4712-84aa-1a590771dc4d",
            "compositeImage": {
                "id": "d5bdebe7-39b8-48f8-8f8a-e93a0ee94e2d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5457c52e-d3bd-46bd-9de0-e62b930a33aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e99bad69-1110-4932-a6fb-67dee4e00b1a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5457c52e-d3bd-46bd-9de0-e62b930a33aa",
                    "LayerId": "882a67f4-e522-464b-b72b-61890d157d81"
                }
            ]
        },
        {
            "id": "ef053a18-b29a-4152-a108-4efed21afbe0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b76f42e5-7dc6-4712-84aa-1a590771dc4d",
            "compositeImage": {
                "id": "3e9ccec7-9299-4a8c-970e-de19f8db1b3c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef053a18-b29a-4152-a108-4efed21afbe0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d354da91-7df3-40db-bbc8-49a508b00300",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef053a18-b29a-4152-a108-4efed21afbe0",
                    "LayerId": "882a67f4-e522-464b-b72b-61890d157d81"
                }
            ]
        },
        {
            "id": "3a1ee70d-ef06-435d-bd71-92983cc9405c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b76f42e5-7dc6-4712-84aa-1a590771dc4d",
            "compositeImage": {
                "id": "be41f4c2-df51-47b6-a16d-e2288da007ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a1ee70d-ef06-435d-bd71-92983cc9405c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4cd2493a-1b54-4754-af11-0f2b8e9eef02",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a1ee70d-ef06-435d-bd71-92983cc9405c",
                    "LayerId": "882a67f4-e522-464b-b72b-61890d157d81"
                }
            ]
        },
        {
            "id": "aa20f27f-88b0-4d50-9881-989d6e2d00a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b76f42e5-7dc6-4712-84aa-1a590771dc4d",
            "compositeImage": {
                "id": "02674e61-ddd8-476b-b174-1570980d202a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa20f27f-88b0-4d50-9881-989d6e2d00a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc6dba24-3d41-4758-9cf7-c3cda6497ac3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa20f27f-88b0-4d50-9881-989d6e2d00a9",
                    "LayerId": "882a67f4-e522-464b-b72b-61890d157d81"
                }
            ]
        },
        {
            "id": "2c730ac8-f9ec-40cc-a43a-40048494d589",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b76f42e5-7dc6-4712-84aa-1a590771dc4d",
            "compositeImage": {
                "id": "056ce897-c731-4cd9-9fe9-862259d64e93",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c730ac8-f9ec-40cc-a43a-40048494d589",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "59fa36bb-42a1-4ecd-a90e-3e8a8e528e54",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c730ac8-f9ec-40cc-a43a-40048494d589",
                    "LayerId": "882a67f4-e522-464b-b72b-61890d157d81"
                }
            ]
        },
        {
            "id": "00a0d291-607c-4f68-ab72-cbb616c83ccd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b76f42e5-7dc6-4712-84aa-1a590771dc4d",
            "compositeImage": {
                "id": "b2465380-a7d3-4075-aef3-14c7a9ab00db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00a0d291-607c-4f68-ab72-cbb616c83ccd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c7cf045-f9ac-4077-afda-520f748cc17c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00a0d291-607c-4f68-ab72-cbb616c83ccd",
                    "LayerId": "882a67f4-e522-464b-b72b-61890d157d81"
                }
            ]
        },
        {
            "id": "4ba305aa-e7b9-4661-bb34-bc567e81dd29",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b76f42e5-7dc6-4712-84aa-1a590771dc4d",
            "compositeImage": {
                "id": "f9ce5b5b-a9b3-4bed-a502-0f6079795e09",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ba305aa-e7b9-4661-bb34-bc567e81dd29",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "944661b5-f3f3-4967-931c-76d2f1f5158a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ba305aa-e7b9-4661-bb34-bc567e81dd29",
                    "LayerId": "882a67f4-e522-464b-b72b-61890d157d81"
                }
            ]
        },
        {
            "id": "b94466f9-ca38-4566-9b54-90f196942732",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b76f42e5-7dc6-4712-84aa-1a590771dc4d",
            "compositeImage": {
                "id": "3735ade6-fda1-452e-998f-dfe33c76852c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b94466f9-ca38-4566-9b54-90f196942732",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7fe5af88-ecf3-4a75-998f-14768156bbb0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b94466f9-ca38-4566-9b54-90f196942732",
                    "LayerId": "882a67f4-e522-464b-b72b-61890d157d81"
                }
            ]
        },
        {
            "id": "5236010e-8e2a-4878-a03f-a9a65fdda877",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b76f42e5-7dc6-4712-84aa-1a590771dc4d",
            "compositeImage": {
                "id": "977646a4-7152-4afd-bf71-83124f894adf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5236010e-8e2a-4878-a03f-a9a65fdda877",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32275fc6-b2dd-4882-89b7-622cc04f0d47",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5236010e-8e2a-4878-a03f-a9a65fdda877",
                    "LayerId": "882a67f4-e522-464b-b72b-61890d157d81"
                }
            ]
        },
        {
            "id": "5ffdb368-b75c-465e-94d1-3ccf4756619c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b76f42e5-7dc6-4712-84aa-1a590771dc4d",
            "compositeImage": {
                "id": "8f4b3341-4aa9-4c2c-b5ba-77519e2863ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ffdb368-b75c-465e-94d1-3ccf4756619c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4eadcd2d-3c89-4324-b00b-6312fb4ce3bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ffdb368-b75c-465e-94d1-3ccf4756619c",
                    "LayerId": "882a67f4-e522-464b-b72b-61890d157d81"
                }
            ]
        },
        {
            "id": "815e4eed-c3a7-478c-80ce-29a3845b43ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b76f42e5-7dc6-4712-84aa-1a590771dc4d",
            "compositeImage": {
                "id": "9e38aa2c-0e1a-47e4-8b47-4563bd7192eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "815e4eed-c3a7-478c-80ce-29a3845b43ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "23492fa5-2cd4-46ea-91cd-2d8b64db5859",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "815e4eed-c3a7-478c-80ce-29a3845b43ff",
                    "LayerId": "882a67f4-e522-464b-b72b-61890d157d81"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 215,
    "layers": [
        {
            "id": "882a67f4-e522-464b-b72b-61890d157d81",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b76f42e5-7dc6-4712-84aa-1a590771dc4d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 258,
    "xorig": 129,
    "yorig": 107
}