{
    "id": "024b6fd0-e6bf-43cf-bdda-ff62b4aaee13",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sShambler_Roar",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 198,
    "bbox_left": 75,
    "bbox_right": 155,
    "bbox_top": 110,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "abcd5996-1d1b-40c5-8ed6-e75cf88041e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "024b6fd0-e6bf-43cf-bdda-ff62b4aaee13",
            "compositeImage": {
                "id": "b46b2932-8e33-4a72-b959-006ec428ed9e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "abcd5996-1d1b-40c5-8ed6-e75cf88041e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ade7085d-2386-4ac8-8f5b-dd962943f4fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "abcd5996-1d1b-40c5-8ed6-e75cf88041e2",
                    "LayerId": "dd6986b0-e68a-4253-b4e5-442144cba887"
                }
            ]
        },
        {
            "id": "1490bfeb-867c-4bed-b4e6-8cfbf2e257e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "024b6fd0-e6bf-43cf-bdda-ff62b4aaee13",
            "compositeImage": {
                "id": "534dde94-ecf9-4aab-9322-e12298dc67f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1490bfeb-867c-4bed-b4e6-8cfbf2e257e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2fae6b2d-cf0c-4d9d-a600-f30b0529ff70",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1490bfeb-867c-4bed-b4e6-8cfbf2e257e4",
                    "LayerId": "dd6986b0-e68a-4253-b4e5-442144cba887"
                }
            ]
        },
        {
            "id": "d3f105b2-ad2f-4ca7-87bd-3775c587d2d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "024b6fd0-e6bf-43cf-bdda-ff62b4aaee13",
            "compositeImage": {
                "id": "9b23acdc-ab94-41e9-90cf-20ac7d1ed275",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d3f105b2-ad2f-4ca7-87bd-3775c587d2d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "304ed60d-b2ca-4331-a799-1beeaebaf4e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3f105b2-ad2f-4ca7-87bd-3775c587d2d1",
                    "LayerId": "dd6986b0-e68a-4253-b4e5-442144cba887"
                }
            ]
        },
        {
            "id": "94ef4bbd-e083-4e19-a756-d98efb164a21",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "024b6fd0-e6bf-43cf-bdda-ff62b4aaee13",
            "compositeImage": {
                "id": "8f4a3986-c931-4e37-ba70-5f31ec41125d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "94ef4bbd-e083-4e19-a756-d98efb164a21",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d1094fa-3135-4182-8b11-2ed56d4ffd83",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "94ef4bbd-e083-4e19-a756-d98efb164a21",
                    "LayerId": "dd6986b0-e68a-4253-b4e5-442144cba887"
                }
            ]
        },
        {
            "id": "78d4a4c2-0ea1-4c12-9d6c-a27406d58741",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "024b6fd0-e6bf-43cf-bdda-ff62b4aaee13",
            "compositeImage": {
                "id": "107f6191-1e78-424e-9819-35adc58fc8d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78d4a4c2-0ea1-4c12-9d6c-a27406d58741",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc0d7970-7157-4a84-8664-ed6a3551d997",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78d4a4c2-0ea1-4c12-9d6c-a27406d58741",
                    "LayerId": "dd6986b0-e68a-4253-b4e5-442144cba887"
                }
            ]
        },
        {
            "id": "15e33dcb-b21e-4d1c-b4d5-da62f3ea796b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "024b6fd0-e6bf-43cf-bdda-ff62b4aaee13",
            "compositeImage": {
                "id": "853fd1fe-07ed-44c7-81f3-a33c64a868c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15e33dcb-b21e-4d1c-b4d5-da62f3ea796b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30f8e11f-0282-4545-bf37-7b889c7df225",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15e33dcb-b21e-4d1c-b4d5-da62f3ea796b",
                    "LayerId": "dd6986b0-e68a-4253-b4e5-442144cba887"
                }
            ]
        },
        {
            "id": "823bcd19-2f9b-4765-a65f-de392449590e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "024b6fd0-e6bf-43cf-bdda-ff62b4aaee13",
            "compositeImage": {
                "id": "ae4f12d7-333a-4ba2-b069-a3f6ef4b9929",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "823bcd19-2f9b-4765-a65f-de392449590e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca479798-78fa-4a0c-8922-64461dc3ffae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "823bcd19-2f9b-4765-a65f-de392449590e",
                    "LayerId": "dd6986b0-e68a-4253-b4e5-442144cba887"
                }
            ]
        },
        {
            "id": "24306c79-6f3b-4b0a-9f79-88d199a0fb99",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "024b6fd0-e6bf-43cf-bdda-ff62b4aaee13",
            "compositeImage": {
                "id": "350b245b-5de4-4030-97b4-d8445d33ce42",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24306c79-6f3b-4b0a-9f79-88d199a0fb99",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf5934ae-1b7a-42ee-9512-6e41e699a098",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24306c79-6f3b-4b0a-9f79-88d199a0fb99",
                    "LayerId": "dd6986b0-e68a-4253-b4e5-442144cba887"
                }
            ]
        },
        {
            "id": "58f5dd52-d840-481d-87cf-a6e924ca5d13",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "024b6fd0-e6bf-43cf-bdda-ff62b4aaee13",
            "compositeImage": {
                "id": "98389f91-bfd8-4081-b1dd-0e50d0aaf535",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "58f5dd52-d840-481d-87cf-a6e924ca5d13",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c6b10dc8-0002-402f-8fb9-f35fd78e15bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "58f5dd52-d840-481d-87cf-a6e924ca5d13",
                    "LayerId": "dd6986b0-e68a-4253-b4e5-442144cba887"
                }
            ]
        },
        {
            "id": "f2ebc3c6-1125-407c-9aab-67802e65064a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "024b6fd0-e6bf-43cf-bdda-ff62b4aaee13",
            "compositeImage": {
                "id": "673186b5-5cff-49a8-9844-b8313bea7361",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f2ebc3c6-1125-407c-9aab-67802e65064a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "52e93298-d70c-47d4-b856-d167d74df464",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f2ebc3c6-1125-407c-9aab-67802e65064a",
                    "LayerId": "dd6986b0-e68a-4253-b4e5-442144cba887"
                }
            ]
        },
        {
            "id": "b03380c0-13e1-49bc-bb60-ab8c5b09346d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "024b6fd0-e6bf-43cf-bdda-ff62b4aaee13",
            "compositeImage": {
                "id": "c1bba2ae-1bf7-4c25-beaf-0adca1714405",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b03380c0-13e1-49bc-bb60-ab8c5b09346d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6ab06e6-7890-459c-891d-94d1c3b57be6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b03380c0-13e1-49bc-bb60-ab8c5b09346d",
                    "LayerId": "dd6986b0-e68a-4253-b4e5-442144cba887"
                }
            ]
        },
        {
            "id": "1260304c-72f0-452c-a729-9fdaad2d4148",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "024b6fd0-e6bf-43cf-bdda-ff62b4aaee13",
            "compositeImage": {
                "id": "f3312d32-b6cb-4a31-8b1b-08d73a0ba951",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1260304c-72f0-452c-a729-9fdaad2d4148",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f0150c71-521a-4d19-a428-572bbf98f79d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1260304c-72f0-452c-a729-9fdaad2d4148",
                    "LayerId": "dd6986b0-e68a-4253-b4e5-442144cba887"
                }
            ]
        },
        {
            "id": "08a2b5fd-5df2-4a83-b24e-4f43ed2bfabf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "024b6fd0-e6bf-43cf-bdda-ff62b4aaee13",
            "compositeImage": {
                "id": "20eace1b-8849-4dc5-aec4-1035badb6351",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08a2b5fd-5df2-4a83-b24e-4f43ed2bfabf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1c2fac6-4d6d-4cc5-8d87-632dede13169",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08a2b5fd-5df2-4a83-b24e-4f43ed2bfabf",
                    "LayerId": "dd6986b0-e68a-4253-b4e5-442144cba887"
                }
            ]
        },
        {
            "id": "388d839c-30c6-4431-bb0d-0e55ed46643b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "024b6fd0-e6bf-43cf-bdda-ff62b4aaee13",
            "compositeImage": {
                "id": "0728c22f-29be-4eb6-9d54-ca7d5aad9cab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "388d839c-30c6-4431-bb0d-0e55ed46643b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a1f5e863-6615-4c47-a7de-40e1dd09dd13",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "388d839c-30c6-4431-bb0d-0e55ed46643b",
                    "LayerId": "dd6986b0-e68a-4253-b4e5-442144cba887"
                }
            ]
        },
        {
            "id": "99a4e5f8-6b1b-46cc-8234-bcbc405c4652",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "024b6fd0-e6bf-43cf-bdda-ff62b4aaee13",
            "compositeImage": {
                "id": "ed656a4b-2bab-44aa-b893-51d0fb8a28d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99a4e5f8-6b1b-46cc-8234-bcbc405c4652",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab6007d6-a854-4719-a6b5-d9f7742c5541",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99a4e5f8-6b1b-46cc-8234-bcbc405c4652",
                    "LayerId": "dd6986b0-e68a-4253-b4e5-442144cba887"
                }
            ]
        },
        {
            "id": "3c7625c0-7e4b-4829-8457-7ec90a232194",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "024b6fd0-e6bf-43cf-bdda-ff62b4aaee13",
            "compositeImage": {
                "id": "06cf37d3-219f-47d3-bea9-50b19f1e0917",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c7625c0-7e4b-4829-8457-7ec90a232194",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad5f26a9-7167-416f-84cb-92f5a28892d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c7625c0-7e4b-4829-8457-7ec90a232194",
                    "LayerId": "dd6986b0-e68a-4253-b4e5-442144cba887"
                }
            ]
        },
        {
            "id": "47c3f04f-72bb-4fd0-b29f-9cca75410751",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "024b6fd0-e6bf-43cf-bdda-ff62b4aaee13",
            "compositeImage": {
                "id": "8faa897f-1468-42cf-9ce1-577e2a9e565d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "47c3f04f-72bb-4fd0-b29f-9cca75410751",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ccff5a7-a9b8-4247-8198-4996c4f4ac97",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47c3f04f-72bb-4fd0-b29f-9cca75410751",
                    "LayerId": "dd6986b0-e68a-4253-b4e5-442144cba887"
                }
            ]
        },
        {
            "id": "b297b2f9-437e-479d-b7e9-74fd195dbae2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "024b6fd0-e6bf-43cf-bdda-ff62b4aaee13",
            "compositeImage": {
                "id": "77677c41-910c-4674-9b61-26add99566ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b297b2f9-437e-479d-b7e9-74fd195dbae2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8556eb45-e69f-4069-88b9-6939c3624bee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b297b2f9-437e-479d-b7e9-74fd195dbae2",
                    "LayerId": "dd6986b0-e68a-4253-b4e5-442144cba887"
                }
            ]
        },
        {
            "id": "952bbf7c-7cc0-41cc-a692-e605dd2504f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "024b6fd0-e6bf-43cf-bdda-ff62b4aaee13",
            "compositeImage": {
                "id": "28b4cd4a-7aa5-4db4-857c-bbac39367467",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "952bbf7c-7cc0-41cc-a692-e605dd2504f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a5d67341-85a0-4d24-abce-a9c25d48ddfe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "952bbf7c-7cc0-41cc-a692-e605dd2504f0",
                    "LayerId": "dd6986b0-e68a-4253-b4e5-442144cba887"
                }
            ]
        },
        {
            "id": "83ada7c7-f81d-4ba1-a1b6-5f78819155a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "024b6fd0-e6bf-43cf-bdda-ff62b4aaee13",
            "compositeImage": {
                "id": "9d5ca134-fefc-43da-83a3-dd198f60d318",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "83ada7c7-f81d-4ba1-a1b6-5f78819155a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4faed01d-a8ec-46a6-90af-0abe9c235ac7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "83ada7c7-f81d-4ba1-a1b6-5f78819155a6",
                    "LayerId": "dd6986b0-e68a-4253-b4e5-442144cba887"
                }
            ]
        },
        {
            "id": "7d993a70-1bbb-4e3f-a1d5-05bfc38ba13f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "024b6fd0-e6bf-43cf-bdda-ff62b4aaee13",
            "compositeImage": {
                "id": "4fd8a2a4-4101-41cb-be88-bb20d9b48039",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d993a70-1bbb-4e3f-a1d5-05bfc38ba13f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d8ada1b4-4c42-4145-a0ea-a075d4980ff6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d993a70-1bbb-4e3f-a1d5-05bfc38ba13f",
                    "LayerId": "dd6986b0-e68a-4253-b4e5-442144cba887"
                }
            ]
        },
        {
            "id": "e9142fc7-5618-425d-beb0-bf5f5b90b454",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "024b6fd0-e6bf-43cf-bdda-ff62b4aaee13",
            "compositeImage": {
                "id": "40ce357e-6bd4-4419-a8be-7814a3996a63",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9142fc7-5618-425d-beb0-bf5f5b90b454",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86685a20-e25a-4fee-a170-ca50e8014982",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9142fc7-5618-425d-beb0-bf5f5b90b454",
                    "LayerId": "dd6986b0-e68a-4253-b4e5-442144cba887"
                }
            ]
        },
        {
            "id": "c252f8a1-6f39-4662-adbf-b833365b2227",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "024b6fd0-e6bf-43cf-bdda-ff62b4aaee13",
            "compositeImage": {
                "id": "68bf7211-32be-4e5d-988e-efe398fe08d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c252f8a1-6f39-4662-adbf-b833365b2227",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "152810fb-e8ee-417f-99e8-fd7d800e07ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c252f8a1-6f39-4662-adbf-b833365b2227",
                    "LayerId": "dd6986b0-e68a-4253-b4e5-442144cba887"
                }
            ]
        },
        {
            "id": "c9defe82-a84d-4a5e-bd5b-287f363eeb74",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "024b6fd0-e6bf-43cf-bdda-ff62b4aaee13",
            "compositeImage": {
                "id": "dd1800ef-7d4c-4215-b0a4-56daf40d8a9a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9defe82-a84d-4a5e-bd5b-287f363eeb74",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12dea720-0ffa-4378-9eab-f516cd9df272",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9defe82-a84d-4a5e-bd5b-287f363eeb74",
                    "LayerId": "dd6986b0-e68a-4253-b4e5-442144cba887"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 215,
    "layers": [
        {
            "id": "dd6986b0-e68a-4253-b4e5-442144cba887",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "024b6fd0-e6bf-43cf-bdda-ff62b4aaee13",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 258,
    "xorig": 129,
    "yorig": 107
}