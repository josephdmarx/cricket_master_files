{
    "id": "94343e71-905a-44e5-8972-f599938172ee",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSmoke_Chimney",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 99,
    "bbox_left": 35,
    "bbox_right": 67,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c8da6e94-b993-4746-bb07-8f6d8ae83b8f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94343e71-905a-44e5-8972-f599938172ee",
            "compositeImage": {
                "id": "b950cfca-4bb7-48f7-bbeb-9fa51ff24d07",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8da6e94-b993-4746-bb07-8f6d8ae83b8f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e8b6fee-75d3-436a-9f46-b99feb051942",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8da6e94-b993-4746-bb07-8f6d8ae83b8f",
                    "LayerId": "e5ab2bdc-3114-485d-928c-065fa4eb32da"
                }
            ]
        },
        {
            "id": "7412ff21-99bc-4aee-af4f-ab9b599097eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94343e71-905a-44e5-8972-f599938172ee",
            "compositeImage": {
                "id": "744ad244-7ed3-4dcb-8f6a-6167461cbb30",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7412ff21-99bc-4aee-af4f-ab9b599097eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a50bcaed-5d3e-4421-b2b2-2fce445e871e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7412ff21-99bc-4aee-af4f-ab9b599097eb",
                    "LayerId": "e5ab2bdc-3114-485d-928c-065fa4eb32da"
                }
            ]
        },
        {
            "id": "132cd327-fd09-4de4-ad13-bdbdfb4e9581",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94343e71-905a-44e5-8972-f599938172ee",
            "compositeImage": {
                "id": "a5344533-8c8d-4530-a9c3-48ceb7b48206",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "132cd327-fd09-4de4-ad13-bdbdfb4e9581",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c4175ac-3fbb-4fff-90fb-53b5e7c8061a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "132cd327-fd09-4de4-ad13-bdbdfb4e9581",
                    "LayerId": "e5ab2bdc-3114-485d-928c-065fa4eb32da"
                }
            ]
        },
        {
            "id": "fdf67f80-bf95-4688-8a73-d5256dda6944",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94343e71-905a-44e5-8972-f599938172ee",
            "compositeImage": {
                "id": "9f92f8ce-e4db-46dd-9510-4e8af4e310db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fdf67f80-bf95-4688-8a73-d5256dda6944",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5037472d-28e0-4fda-a2e5-3f8c8e15ec85",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fdf67f80-bf95-4688-8a73-d5256dda6944",
                    "LayerId": "e5ab2bdc-3114-485d-928c-065fa4eb32da"
                }
            ]
        },
        {
            "id": "95bfc382-8f0d-4722-81f8-82872717deb8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94343e71-905a-44e5-8972-f599938172ee",
            "compositeImage": {
                "id": "b148dbeb-0190-4b5b-a3cb-1e8844e8ce39",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "95bfc382-8f0d-4722-81f8-82872717deb8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9f80db2-40bd-4fe8-a990-9a995bb0a979",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "95bfc382-8f0d-4722-81f8-82872717deb8",
                    "LayerId": "e5ab2bdc-3114-485d-928c-065fa4eb32da"
                }
            ]
        },
        {
            "id": "e9411175-1128-4194-b82b-bdf1d6e97b17",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94343e71-905a-44e5-8972-f599938172ee",
            "compositeImage": {
                "id": "da445b14-6f1b-4743-a858-e0e99b1070b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9411175-1128-4194-b82b-bdf1d6e97b17",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a351d08-44ae-4c82-9648-05dc6fd07b1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9411175-1128-4194-b82b-bdf1d6e97b17",
                    "LayerId": "e5ab2bdc-3114-485d-928c-065fa4eb32da"
                }
            ]
        },
        {
            "id": "61ce8fe1-05d9-4c15-becd-8f8c32e2f6ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94343e71-905a-44e5-8972-f599938172ee",
            "compositeImage": {
                "id": "9d239427-9354-40e6-9027-16851c8f2a6e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "61ce8fe1-05d9-4c15-becd-8f8c32e2f6ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f474495-8866-412c-b520-adbcf6a7280d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "61ce8fe1-05d9-4c15-becd-8f8c32e2f6ba",
                    "LayerId": "e5ab2bdc-3114-485d-928c-065fa4eb32da"
                }
            ]
        },
        {
            "id": "7c32ec85-075f-4517-bf49-bfb50e515da5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94343e71-905a-44e5-8972-f599938172ee",
            "compositeImage": {
                "id": "a057d78f-ddea-4208-9403-9fba32a356b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c32ec85-075f-4517-bf49-bfb50e515da5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9175c26c-c010-4283-998b-038b7fe746c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c32ec85-075f-4517-bf49-bfb50e515da5",
                    "LayerId": "e5ab2bdc-3114-485d-928c-065fa4eb32da"
                }
            ]
        },
        {
            "id": "44b35811-c4d1-452f-91c2-ba28104440e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94343e71-905a-44e5-8972-f599938172ee",
            "compositeImage": {
                "id": "4cc92286-4081-40b1-9119-0c50840a84d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "44b35811-c4d1-452f-91c2-ba28104440e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5870ff0-1220-4337-b133-652acf13e634",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "44b35811-c4d1-452f-91c2-ba28104440e2",
                    "LayerId": "e5ab2bdc-3114-485d-928c-065fa4eb32da"
                }
            ]
        },
        {
            "id": "43020ab2-4b9b-47e8-9916-193771fc0de1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94343e71-905a-44e5-8972-f599938172ee",
            "compositeImage": {
                "id": "e81f3e14-ee72-42fd-8256-e67bec9aafa3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43020ab2-4b9b-47e8-9916-193771fc0de1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12caaba2-ba4d-456c-9249-dc0019f2a7f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43020ab2-4b9b-47e8-9916-193771fc0de1",
                    "LayerId": "e5ab2bdc-3114-485d-928c-065fa4eb32da"
                }
            ]
        },
        {
            "id": "bc0b009e-be0a-4a60-8cae-0f4bd5617576",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94343e71-905a-44e5-8972-f599938172ee",
            "compositeImage": {
                "id": "97bf60ec-3875-42f0-b88c-9d8e890dbd07",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc0b009e-be0a-4a60-8cae-0f4bd5617576",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12707158-8c39-4314-bb01-cbdf02b18b7c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc0b009e-be0a-4a60-8cae-0f4bd5617576",
                    "LayerId": "e5ab2bdc-3114-485d-928c-065fa4eb32da"
                }
            ]
        },
        {
            "id": "57ed3c14-3e38-44f3-a37c-64da4545fb3e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94343e71-905a-44e5-8972-f599938172ee",
            "compositeImage": {
                "id": "c1ab5183-f951-4837-8dc8-a50e2fd47b33",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57ed3c14-3e38-44f3-a37c-64da4545fb3e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e95b145-fde0-4195-8e10-7b8b79054010",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57ed3c14-3e38-44f3-a37c-64da4545fb3e",
                    "LayerId": "e5ab2bdc-3114-485d-928c-065fa4eb32da"
                }
            ]
        },
        {
            "id": "8a07e317-dbae-4e8c-82dc-aa39c5534a9f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94343e71-905a-44e5-8972-f599938172ee",
            "compositeImage": {
                "id": "4c8d634a-9876-4883-a51e-5a31d5881fd1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a07e317-dbae-4e8c-82dc-aa39c5534a9f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e91ad61-acf4-4bb0-9411-5de5c37e0f6a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a07e317-dbae-4e8c-82dc-aa39c5534a9f",
                    "LayerId": "e5ab2bdc-3114-485d-928c-065fa4eb32da"
                }
            ]
        },
        {
            "id": "52d16bcc-bb24-4ed5-ab4b-f92639b914be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94343e71-905a-44e5-8972-f599938172ee",
            "compositeImage": {
                "id": "022605b6-8c72-4274-94e6-7cbf0e62133b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52d16bcc-bb24-4ed5-ab4b-f92639b914be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3df4f3e5-92d9-41de-bd9c-73932e4f05c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52d16bcc-bb24-4ed5-ab4b-f92639b914be",
                    "LayerId": "e5ab2bdc-3114-485d-928c-065fa4eb32da"
                }
            ]
        },
        {
            "id": "03214dba-6988-49f1-8ef4-e2edabd2555c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94343e71-905a-44e5-8972-f599938172ee",
            "compositeImage": {
                "id": "31c96561-2282-437d-bf61-d3e404c904f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03214dba-6988-49f1-8ef4-e2edabd2555c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6845f910-6496-4cc5-9aab-ebb5974f2e84",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03214dba-6988-49f1-8ef4-e2edabd2555c",
                    "LayerId": "e5ab2bdc-3114-485d-928c-065fa4eb32da"
                }
            ]
        },
        {
            "id": "35aca997-8f6e-4bd6-b680-4850c148c7e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94343e71-905a-44e5-8972-f599938172ee",
            "compositeImage": {
                "id": "3293f7db-bc63-4212-b644-1f7c5b5431a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35aca997-8f6e-4bd6-b680-4850c148c7e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a5f56108-4473-41d1-9e40-ba3b12bad483",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35aca997-8f6e-4bd6-b680-4850c148c7e2",
                    "LayerId": "e5ab2bdc-3114-485d-928c-065fa4eb32da"
                }
            ]
        },
        {
            "id": "e48ba734-7269-4855-8653-13c9cf8990e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94343e71-905a-44e5-8972-f599938172ee",
            "compositeImage": {
                "id": "87b8c4e9-34bd-4615-b132-5aca4c96f5df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e48ba734-7269-4855-8653-13c9cf8990e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "74041304-2225-4205-86bd-f323da06a3c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e48ba734-7269-4855-8653-13c9cf8990e6",
                    "LayerId": "e5ab2bdc-3114-485d-928c-065fa4eb32da"
                }
            ]
        },
        {
            "id": "a49250f8-a1c8-4fbc-aecd-4f8d464fc8b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94343e71-905a-44e5-8972-f599938172ee",
            "compositeImage": {
                "id": "49493023-9cfb-4023-b321-b89f88daa7e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a49250f8-a1c8-4fbc-aecd-4f8d464fc8b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f96e4474-59dd-46ee-a499-2f12de6127d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a49250f8-a1c8-4fbc-aecd-4f8d464fc8b5",
                    "LayerId": "e5ab2bdc-3114-485d-928c-065fa4eb32da"
                }
            ]
        },
        {
            "id": "fd2e4585-f319-4abb-ac41-da185ac0dc24",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94343e71-905a-44e5-8972-f599938172ee",
            "compositeImage": {
                "id": "ae317852-6e94-468e-bf20-9b2f840e1d64",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd2e4585-f319-4abb-ac41-da185ac0dc24",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d271d8bd-cc75-4dc3-8e1f-32a390377a90",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd2e4585-f319-4abb-ac41-da185ac0dc24",
                    "LayerId": "e5ab2bdc-3114-485d-928c-065fa4eb32da"
                }
            ]
        },
        {
            "id": "9dbf78fc-19c7-47d4-aa75-292d70c1f3bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94343e71-905a-44e5-8972-f599938172ee",
            "compositeImage": {
                "id": "62eb0a76-ec3f-471a-8770-0237c886473a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9dbf78fc-19c7-47d4-aa75-292d70c1f3bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6970c4f5-4b53-4c72-8daf-b65e1f93ef49",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9dbf78fc-19c7-47d4-aa75-292d70c1f3bd",
                    "LayerId": "e5ab2bdc-3114-485d-928c-065fa4eb32da"
                }
            ]
        },
        {
            "id": "1a03dabd-edb3-48be-ad88-0435d93e54c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94343e71-905a-44e5-8972-f599938172ee",
            "compositeImage": {
                "id": "83c2a751-1e64-4ee8-ab0c-b38059b5b86e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a03dabd-edb3-48be-ad88-0435d93e54c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc281508-ae21-44ae-9deb-05175effb65e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a03dabd-edb3-48be-ad88-0435d93e54c7",
                    "LayerId": "e5ab2bdc-3114-485d-928c-065fa4eb32da"
                }
            ]
        },
        {
            "id": "f4a790bf-9396-49a8-bd36-78eef17a8696",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94343e71-905a-44e5-8972-f599938172ee",
            "compositeImage": {
                "id": "baf56782-330c-44b9-ba10-641d8b7469a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f4a790bf-9396-49a8-bd36-78eef17a8696",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "23c6489f-359e-49b7-b908-121948f85193",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4a790bf-9396-49a8-bd36-78eef17a8696",
                    "LayerId": "e5ab2bdc-3114-485d-928c-065fa4eb32da"
                }
            ]
        },
        {
            "id": "249578d6-9b04-450e-a0e9-5932e81bda5d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94343e71-905a-44e5-8972-f599938172ee",
            "compositeImage": {
                "id": "b0c3bf9b-932f-4b76-8e37-d7c952e34b0a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "249578d6-9b04-450e-a0e9-5932e81bda5d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "79eb20fd-300d-41dd-b277-2fc4c4051a57",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "249578d6-9b04-450e-a0e9-5932e81bda5d",
                    "LayerId": "e5ab2bdc-3114-485d-928c-065fa4eb32da"
                }
            ]
        },
        {
            "id": "8339f0a4-b9e4-483b-a939-9f2a0bf1b2df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94343e71-905a-44e5-8972-f599938172ee",
            "compositeImage": {
                "id": "4ab4f02c-90a8-4588-bf1f-057628f21255",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8339f0a4-b9e4-483b-a939-9f2a0bf1b2df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d538b41-7163-447f-96f3-6b8ff8fdd043",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8339f0a4-b9e4-483b-a939-9f2a0bf1b2df",
                    "LayerId": "e5ab2bdc-3114-485d-928c-065fa4eb32da"
                }
            ]
        },
        {
            "id": "ad248f9a-6616-4df1-9967-0e93ced25dc4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94343e71-905a-44e5-8972-f599938172ee",
            "compositeImage": {
                "id": "29efd8c4-6395-42c7-b22a-34180c78d58b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad248f9a-6616-4df1-9967-0e93ced25dc4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33dfbea1-5e81-44f4-994f-476d6e4a4d52",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad248f9a-6616-4df1-9967-0e93ced25dc4",
                    "LayerId": "e5ab2bdc-3114-485d-928c-065fa4eb32da"
                }
            ]
        },
        {
            "id": "048c26db-17db-4572-b398-d9dbf221633f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94343e71-905a-44e5-8972-f599938172ee",
            "compositeImage": {
                "id": "a15cc088-f593-4249-b34d-84a7bd000ce0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "048c26db-17db-4572-b398-d9dbf221633f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d2031fd1-91c7-4126-8699-4d5cf917d113",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "048c26db-17db-4572-b398-d9dbf221633f",
                    "LayerId": "e5ab2bdc-3114-485d-928c-065fa4eb32da"
                }
            ]
        },
        {
            "id": "fff9e262-edf1-4de9-8a10-6813eea6d0b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94343e71-905a-44e5-8972-f599938172ee",
            "compositeImage": {
                "id": "c23441d2-c98b-4fbe-b808-ed068c0650de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fff9e262-edf1-4de9-8a10-6813eea6d0b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f5e690b-ed20-45f0-bea9-11c164a7dc13",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fff9e262-edf1-4de9-8a10-6813eea6d0b7",
                    "LayerId": "e5ab2bdc-3114-485d-928c-065fa4eb32da"
                }
            ]
        },
        {
            "id": "c086b445-8500-4bab-a9f8-45527181df46",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94343e71-905a-44e5-8972-f599938172ee",
            "compositeImage": {
                "id": "09ea35ee-c2cc-404b-a08f-eb0562ca0ecd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c086b445-8500-4bab-a9f8-45527181df46",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ebb6f7e0-5261-45aa-bb96-3a0a5c9f97ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c086b445-8500-4bab-a9f8-45527181df46",
                    "LayerId": "e5ab2bdc-3114-485d-928c-065fa4eb32da"
                }
            ]
        },
        {
            "id": "88e89022-ba90-4cd7-89fc-568eb45bc7bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94343e71-905a-44e5-8972-f599938172ee",
            "compositeImage": {
                "id": "769633e7-ae87-42fa-9e9d-706de7de6248",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88e89022-ba90-4cd7-89fc-568eb45bc7bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "40147cd2-71fd-4886-99b3-1c5180d5af41",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88e89022-ba90-4cd7-89fc-568eb45bc7bc",
                    "LayerId": "e5ab2bdc-3114-485d-928c-065fa4eb32da"
                }
            ]
        },
        {
            "id": "ff38575a-319f-408f-adfb-a9e3d7b4a48e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94343e71-905a-44e5-8972-f599938172ee",
            "compositeImage": {
                "id": "d6dda9c2-78f4-4a0f-8310-6c83de2270d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff38575a-319f-408f-adfb-a9e3d7b4a48e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d49af3ce-c83b-4d12-b295-9fab5ebe9db3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff38575a-319f-408f-adfb-a9e3d7b4a48e",
                    "LayerId": "e5ab2bdc-3114-485d-928c-065fa4eb32da"
                }
            ]
        },
        {
            "id": "5b009943-abbd-4315-8c2b-27700b3e8d55",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94343e71-905a-44e5-8972-f599938172ee",
            "compositeImage": {
                "id": "984ed5a1-a947-4d74-a410-968a489dd8f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b009943-abbd-4315-8c2b-27700b3e8d55",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc31d7ee-d027-44bb-b012-6df26897432c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b009943-abbd-4315-8c2b-27700b3e8d55",
                    "LayerId": "e5ab2bdc-3114-485d-928c-065fa4eb32da"
                }
            ]
        },
        {
            "id": "c6f01d5e-280c-4541-aafe-57b1ee593c08",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94343e71-905a-44e5-8972-f599938172ee",
            "compositeImage": {
                "id": "7a770a10-0837-49ec-9091-c5b0132dfc61",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6f01d5e-280c-4541-aafe-57b1ee593c08",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c2f09e6-8ef5-4e81-beba-4dca21bb833f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6f01d5e-280c-4541-aafe-57b1ee593c08",
                    "LayerId": "e5ab2bdc-3114-485d-928c-065fa4eb32da"
                }
            ]
        },
        {
            "id": "274ed8db-2c3b-4146-8ee2-2e30a24dcb46",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94343e71-905a-44e5-8972-f599938172ee",
            "compositeImage": {
                "id": "4b9bb209-2c29-45f1-a761-30498e56b519",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "274ed8db-2c3b-4146-8ee2-2e30a24dcb46",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70d1acb9-19c8-4b41-95e1-b81970d2d4b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "274ed8db-2c3b-4146-8ee2-2e30a24dcb46",
                    "LayerId": "e5ab2bdc-3114-485d-928c-065fa4eb32da"
                }
            ]
        },
        {
            "id": "1260e667-56c6-4670-8e27-7d1df4675768",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94343e71-905a-44e5-8972-f599938172ee",
            "compositeImage": {
                "id": "cb08669f-6cc4-4589-8567-aa4014cfa5fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1260e667-56c6-4670-8e27-7d1df4675768",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd080e6b-132d-4af0-8948-8be8160cf4e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1260e667-56c6-4670-8e27-7d1df4675768",
                    "LayerId": "e5ab2bdc-3114-485d-928c-065fa4eb32da"
                }
            ]
        },
        {
            "id": "a41a5f1c-560e-4422-9759-9bbaaa311bc6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94343e71-905a-44e5-8972-f599938172ee",
            "compositeImage": {
                "id": "d0d14569-0f30-4b8e-b798-d265b0bc1f15",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a41a5f1c-560e-4422-9759-9bbaaa311bc6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e29998b-91c4-45a3-ac18-ec523cfebd8d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a41a5f1c-560e-4422-9759-9bbaaa311bc6",
                    "LayerId": "e5ab2bdc-3114-485d-928c-065fa4eb32da"
                }
            ]
        },
        {
            "id": "63401a6e-7fc4-4d88-ae67-4694f2e14528",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94343e71-905a-44e5-8972-f599938172ee",
            "compositeImage": {
                "id": "56a1830e-3c15-4563-b1be-69a0df326194",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63401a6e-7fc4-4d88-ae67-4694f2e14528",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8c10d13-526d-4728-9b80-5ce153b51c5c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63401a6e-7fc4-4d88-ae67-4694f2e14528",
                    "LayerId": "e5ab2bdc-3114-485d-928c-065fa4eb32da"
                }
            ]
        },
        {
            "id": "6b61c119-2119-4828-900a-da51962b9208",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94343e71-905a-44e5-8972-f599938172ee",
            "compositeImage": {
                "id": "d2a980f7-fa97-4a46-b9a1-bbfea53596cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b61c119-2119-4828-900a-da51962b9208",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a788fa0e-b25f-4946-b50a-e19bc665409b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b61c119-2119-4828-900a-da51962b9208",
                    "LayerId": "e5ab2bdc-3114-485d-928c-065fa4eb32da"
                }
            ]
        },
        {
            "id": "f12d7992-90b4-4247-88fe-fe45c7971064",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94343e71-905a-44e5-8972-f599938172ee",
            "compositeImage": {
                "id": "00a93577-e1d9-4017-95a9-48933797a936",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f12d7992-90b4-4247-88fe-fe45c7971064",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e6ff586-1a8f-4412-9371-a137e6a05b99",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f12d7992-90b4-4247-88fe-fe45c7971064",
                    "LayerId": "e5ab2bdc-3114-485d-928c-065fa4eb32da"
                }
            ]
        },
        {
            "id": "b5eccf6d-83ac-4832-bacb-7644ea65b341",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94343e71-905a-44e5-8972-f599938172ee",
            "compositeImage": {
                "id": "a3e64507-4d13-4d57-9c7e-10da33577b33",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5eccf6d-83ac-4832-bacb-7644ea65b341",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2faf0eec-e99b-4c98-b584-3d5bee1c8f63",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5eccf6d-83ac-4832-bacb-7644ea65b341",
                    "LayerId": "e5ab2bdc-3114-485d-928c-065fa4eb32da"
                }
            ]
        },
        {
            "id": "44d8ee05-fc22-4723-9b18-dbd7355af2e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94343e71-905a-44e5-8972-f599938172ee",
            "compositeImage": {
                "id": "895fc9a2-53e3-434b-bf87-ac4722fc9f6b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "44d8ee05-fc22-4723-9b18-dbd7355af2e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "90c369eb-a790-4847-9258-c9082625f945",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "44d8ee05-fc22-4723-9b18-dbd7355af2e0",
                    "LayerId": "e5ab2bdc-3114-485d-928c-065fa4eb32da"
                }
            ]
        },
        {
            "id": "9f30d415-c531-447c-be30-7b243a376f98",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94343e71-905a-44e5-8972-f599938172ee",
            "compositeImage": {
                "id": "45cc4901-4467-4b71-afe7-eb45c5c49f3c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f30d415-c531-447c-be30-7b243a376f98",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d5eb793-fbb8-4d77-8dcf-5b31ac2f9944",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f30d415-c531-447c-be30-7b243a376f98",
                    "LayerId": "e5ab2bdc-3114-485d-928c-065fa4eb32da"
                }
            ]
        },
        {
            "id": "969399bc-d670-4966-b0d6-5c87a6667340",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94343e71-905a-44e5-8972-f599938172ee",
            "compositeImage": {
                "id": "060af57a-35f4-455b-a595-36028db3b7b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "969399bc-d670-4966-b0d6-5c87a6667340",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9b80d83-617f-47d9-b7aa-50aae7762e87",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "969399bc-d670-4966-b0d6-5c87a6667340",
                    "LayerId": "e5ab2bdc-3114-485d-928c-065fa4eb32da"
                }
            ]
        },
        {
            "id": "2f85b341-fc01-489d-a3e8-194eaf3b8b0d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94343e71-905a-44e5-8972-f599938172ee",
            "compositeImage": {
                "id": "9a388346-f5e8-4e09-9239-390f59cc33bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f85b341-fc01-489d-a3e8-194eaf3b8b0d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7196026f-2864-4d43-8be6-08963f4969d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f85b341-fc01-489d-a3e8-194eaf3b8b0d",
                    "LayerId": "e5ab2bdc-3114-485d-928c-065fa4eb32da"
                }
            ]
        },
        {
            "id": "797cd901-704d-4b0d-9814-14fd1c3b66c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94343e71-905a-44e5-8972-f599938172ee",
            "compositeImage": {
                "id": "38408ad5-0e22-40b8-82f6-0821a6a4147c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "797cd901-704d-4b0d-9814-14fd1c3b66c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f98a26b-ab9e-4733-a4d4-0fb0cbb218b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "797cd901-704d-4b0d-9814-14fd1c3b66c4",
                    "LayerId": "e5ab2bdc-3114-485d-928c-065fa4eb32da"
                }
            ]
        },
        {
            "id": "0bec3ad7-2f59-46dc-965a-a3a5e485fcb4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94343e71-905a-44e5-8972-f599938172ee",
            "compositeImage": {
                "id": "7aa12541-20bd-47b5-bf9d-731903966000",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0bec3ad7-2f59-46dc-965a-a3a5e485fcb4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "218d7e56-1e0c-4bc6-9857-dd296c6da429",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0bec3ad7-2f59-46dc-965a-a3a5e485fcb4",
                    "LayerId": "e5ab2bdc-3114-485d-928c-065fa4eb32da"
                }
            ]
        },
        {
            "id": "67ba2631-adc4-4688-98d6-f707bdbc5bca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94343e71-905a-44e5-8972-f599938172ee",
            "compositeImage": {
                "id": "d6ec7da6-1068-4a94-a1b1-e4f4bb78bc64",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "67ba2631-adc4-4688-98d6-f707bdbc5bca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e607ffc2-7cb9-4653-9018-1fbda10108ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "67ba2631-adc4-4688-98d6-f707bdbc5bca",
                    "LayerId": "e5ab2bdc-3114-485d-928c-065fa4eb32da"
                }
            ]
        },
        {
            "id": "f3f451e9-f76f-4178-b8c8-662e8005c2d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94343e71-905a-44e5-8972-f599938172ee",
            "compositeImage": {
                "id": "d65595cb-f0df-4137-903a-3f15dd4d0344",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3f451e9-f76f-4178-b8c8-662e8005c2d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5e34494-faf7-4217-be32-f58e0151d13f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3f451e9-f76f-4178-b8c8-662e8005c2d4",
                    "LayerId": "e5ab2bdc-3114-485d-928c-065fa4eb32da"
                }
            ]
        },
        {
            "id": "a4a08790-da72-4243-90cb-4cce8c983718",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94343e71-905a-44e5-8972-f599938172ee",
            "compositeImage": {
                "id": "ba0056f2-2b29-4fa4-9c94-f825eb9f53b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4a08790-da72-4243-90cb-4cce8c983718",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "87a9dcd1-b504-40bb-9198-d382dcc0c8ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4a08790-da72-4243-90cb-4cce8c983718",
                    "LayerId": "e5ab2bdc-3114-485d-928c-065fa4eb32da"
                }
            ]
        },
        {
            "id": "372cef32-f122-4729-bbd8-6774165f767f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94343e71-905a-44e5-8972-f599938172ee",
            "compositeImage": {
                "id": "c55c9701-48b4-4806-8eb4-725d770b0bbf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "372cef32-f122-4729-bbd8-6774165f767f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a7168248-2036-4061-ba5d-c11c288cf15c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "372cef32-f122-4729-bbd8-6774165f767f",
                    "LayerId": "e5ab2bdc-3114-485d-928c-065fa4eb32da"
                }
            ]
        },
        {
            "id": "6e08be14-a744-432b-bcbe-31052c75f784",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94343e71-905a-44e5-8972-f599938172ee",
            "compositeImage": {
                "id": "5f18f3c8-0aab-41a7-adff-20334ee9fd9e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e08be14-a744-432b-bcbe-31052c75f784",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e7bbc44-1b40-4270-afe9-e44bda502bb2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e08be14-a744-432b-bcbe-31052c75f784",
                    "LayerId": "e5ab2bdc-3114-485d-928c-065fa4eb32da"
                }
            ]
        },
        {
            "id": "1800342f-50a3-4b78-9877-9ce135c92ae3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94343e71-905a-44e5-8972-f599938172ee",
            "compositeImage": {
                "id": "2c4843b9-0970-489e-971c-9e2fea4539bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1800342f-50a3-4b78-9877-9ce135c92ae3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f23bad8c-b90d-4538-8ba0-0c2b891bc568",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1800342f-50a3-4b78-9877-9ce135c92ae3",
                    "LayerId": "e5ab2bdc-3114-485d-928c-065fa4eb32da"
                }
            ]
        },
        {
            "id": "867fdc3b-9f49-4df0-9284-41665ae93124",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94343e71-905a-44e5-8972-f599938172ee",
            "compositeImage": {
                "id": "2ec6ad33-4ba9-4d4c-89a0-0a3391b5a8d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "867fdc3b-9f49-4df0-9284-41665ae93124",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "112172a4-9489-4ff9-8d6b-e1ef1816714d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "867fdc3b-9f49-4df0-9284-41665ae93124",
                    "LayerId": "e5ab2bdc-3114-485d-928c-065fa4eb32da"
                }
            ]
        },
        {
            "id": "54c05cc7-adf8-4796-900c-1ccd1037cd09",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94343e71-905a-44e5-8972-f599938172ee",
            "compositeImage": {
                "id": "7c7ef33d-937d-46e6-9bae-1332a43aa51a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54c05cc7-adf8-4796-900c-1ccd1037cd09",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7736ade6-57eb-4829-ada4-2291849f04f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54c05cc7-adf8-4796-900c-1ccd1037cd09",
                    "LayerId": "e5ab2bdc-3114-485d-928c-065fa4eb32da"
                }
            ]
        },
        {
            "id": "70464141-57e0-4280-9b68-85dcdfa5afd0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94343e71-905a-44e5-8972-f599938172ee",
            "compositeImage": {
                "id": "8f3bf94b-ea33-4cf4-b22f-98a4bbee093b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "70464141-57e0-4280-9b68-85dcdfa5afd0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3727588f-a782-4a0e-922d-c2af3fe396c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "70464141-57e0-4280-9b68-85dcdfa5afd0",
                    "LayerId": "e5ab2bdc-3114-485d-928c-065fa4eb32da"
                }
            ]
        },
        {
            "id": "9d969bbb-7c96-4b62-8f5f-57f20efa8af9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94343e71-905a-44e5-8972-f599938172ee",
            "compositeImage": {
                "id": "b78a6515-3f6d-415b-8ccc-6521a6e281ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d969bbb-7c96-4b62-8f5f-57f20efa8af9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e11addaf-8763-4b81-8e6d-00901359f5ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d969bbb-7c96-4b62-8f5f-57f20efa8af9",
                    "LayerId": "e5ab2bdc-3114-485d-928c-065fa4eb32da"
                }
            ]
        },
        {
            "id": "aaa2889d-9f6c-4736-acc0-0c89d39ec597",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94343e71-905a-44e5-8972-f599938172ee",
            "compositeImage": {
                "id": "1336fe3c-55ee-4c14-9963-6ac19e400f31",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aaa2889d-9f6c-4736-acc0-0c89d39ec597",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a760fdd0-608c-49b4-99b6-a22b8505a8c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aaa2889d-9f6c-4736-acc0-0c89d39ec597",
                    "LayerId": "e5ab2bdc-3114-485d-928c-065fa4eb32da"
                }
            ]
        },
        {
            "id": "d196467f-5220-4a47-be99-e12a8ee5cb65",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94343e71-905a-44e5-8972-f599938172ee",
            "compositeImage": {
                "id": "43914876-10a6-4bdf-a1a4-aa8ab5f97bd5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d196467f-5220-4a47-be99-e12a8ee5cb65",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "74c7c4c7-3652-4162-b09a-dfd25b6822fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d196467f-5220-4a47-be99-e12a8ee5cb65",
                    "LayerId": "e5ab2bdc-3114-485d-928c-065fa4eb32da"
                }
            ]
        },
        {
            "id": "a059516a-4177-432b-9646-4473897ba0de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94343e71-905a-44e5-8972-f599938172ee",
            "compositeImage": {
                "id": "065760f8-5571-45e8-a23f-848b61f97616",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a059516a-4177-432b-9646-4473897ba0de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46aaed09-430b-40c3-b8e8-11d07e830d72",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a059516a-4177-432b-9646-4473897ba0de",
                    "LayerId": "e5ab2bdc-3114-485d-928c-065fa4eb32da"
                }
            ]
        },
        {
            "id": "da68915a-4d1c-4f29-ab2a-0c534ff54f3c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94343e71-905a-44e5-8972-f599938172ee",
            "compositeImage": {
                "id": "f2a82d14-6d27-43ae-879d-5741b3dd92e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da68915a-4d1c-4f29-ab2a-0c534ff54f3c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4cc45be-2bcb-48e4-926d-67686e706d08",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da68915a-4d1c-4f29-ab2a-0c534ff54f3c",
                    "LayerId": "e5ab2bdc-3114-485d-928c-065fa4eb32da"
                }
            ]
        },
        {
            "id": "9c502433-d085-4dfa-bdf5-bf181a80816c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94343e71-905a-44e5-8972-f599938172ee",
            "compositeImage": {
                "id": "1e794b10-50f9-4253-b8b2-c75574152a91",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c502433-d085-4dfa-bdf5-bf181a80816c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd60ab3e-cf2d-44ff-9f71-4f2c2727ef06",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c502433-d085-4dfa-bdf5-bf181a80816c",
                    "LayerId": "e5ab2bdc-3114-485d-928c-065fa4eb32da"
                }
            ]
        },
        {
            "id": "7849d8a5-46a0-4388-844a-de01e2484c30",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94343e71-905a-44e5-8972-f599938172ee",
            "compositeImage": {
                "id": "8e399b5c-7ac9-444f-8e82-da1f34b4d81b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7849d8a5-46a0-4388-844a-de01e2484c30",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c79785c6-1595-427e-88f6-c2eedff33c61",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7849d8a5-46a0-4388-844a-de01e2484c30",
                    "LayerId": "e5ab2bdc-3114-485d-928c-065fa4eb32da"
                }
            ]
        },
        {
            "id": "39a6c548-449b-47c2-b3b1-8b3f09a22529",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94343e71-905a-44e5-8972-f599938172ee",
            "compositeImage": {
                "id": "df0cba72-7041-4037-9657-e27ff9c42aa3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39a6c548-449b-47c2-b3b1-8b3f09a22529",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09a30745-b477-4e39-b5b2-6bebd34b7493",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39a6c548-449b-47c2-b3b1-8b3f09a22529",
                    "LayerId": "e5ab2bdc-3114-485d-928c-065fa4eb32da"
                }
            ]
        },
        {
            "id": "6bd1e007-c5d3-4177-8bf6-6f0bd5b991aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94343e71-905a-44e5-8972-f599938172ee",
            "compositeImage": {
                "id": "41b1354d-95bf-471b-84bc-1eb47b407b1b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6bd1e007-c5d3-4177-8bf6-6f0bd5b991aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e8d1ad5-ee75-4ed4-b5ba-ae0793caa876",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6bd1e007-c5d3-4177-8bf6-6f0bd5b991aa",
                    "LayerId": "e5ab2bdc-3114-485d-928c-065fa4eb32da"
                }
            ]
        },
        {
            "id": "90b4ec44-4734-4b58-a454-fe3d2c4bd0b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94343e71-905a-44e5-8972-f599938172ee",
            "compositeImage": {
                "id": "f3ba5a1c-ee6d-44cb-baf0-22ac6c9c4042",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "90b4ec44-4734-4b58-a454-fe3d2c4bd0b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9ef2838-dffa-4ec2-a545-44e44e9acd11",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "90b4ec44-4734-4b58-a454-fe3d2c4bd0b0",
                    "LayerId": "e5ab2bdc-3114-485d-928c-065fa4eb32da"
                }
            ]
        },
        {
            "id": "1d1d5861-b79d-44b5-8d08-d7bfdef63214",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94343e71-905a-44e5-8972-f599938172ee",
            "compositeImage": {
                "id": "5f2f530e-a74e-4bb5-8bec-1e213cab6ed0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d1d5861-b79d-44b5-8d08-d7bfdef63214",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "823873f8-65df-4556-8f4a-82e5e53a43e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d1d5861-b79d-44b5-8d08-d7bfdef63214",
                    "LayerId": "e5ab2bdc-3114-485d-928c-065fa4eb32da"
                }
            ]
        },
        {
            "id": "d899b0a3-8185-4e32-a5e1-212baf4bae95",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94343e71-905a-44e5-8972-f599938172ee",
            "compositeImage": {
                "id": "b71059b9-b399-4c52-acd3-f8848d37e1f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d899b0a3-8185-4e32-a5e1-212baf4bae95",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "441cd786-569d-4e49-9c7c-9204ec4a6959",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d899b0a3-8185-4e32-a5e1-212baf4bae95",
                    "LayerId": "e5ab2bdc-3114-485d-928c-065fa4eb32da"
                }
            ]
        },
        {
            "id": "b56e6350-c46e-417a-a635-974f7aaba2a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94343e71-905a-44e5-8972-f599938172ee",
            "compositeImage": {
                "id": "775a7eef-27de-4e7e-92f7-154f65938739",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b56e6350-c46e-417a-a635-974f7aaba2a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e37fee0-e864-4f3e-b3e9-cf1c82954118",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b56e6350-c46e-417a-a635-974f7aaba2a3",
                    "LayerId": "e5ab2bdc-3114-485d-928c-065fa4eb32da"
                }
            ]
        },
        {
            "id": "64a703d1-74f5-45bd-9767-5e40b2120c39",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94343e71-905a-44e5-8972-f599938172ee",
            "compositeImage": {
                "id": "f83a2158-4dcf-477d-b204-987f7132c6ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "64a703d1-74f5-45bd-9767-5e40b2120c39",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8004b22-3b7b-48a7-abc1-eb19171e4d76",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "64a703d1-74f5-45bd-9767-5e40b2120c39",
                    "LayerId": "e5ab2bdc-3114-485d-928c-065fa4eb32da"
                }
            ]
        },
        {
            "id": "50b14c0a-088e-4426-a82f-f94998f55bc6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94343e71-905a-44e5-8972-f599938172ee",
            "compositeImage": {
                "id": "4de3e149-0465-4728-b46c-f41a66aef49e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "50b14c0a-088e-4426-a82f-f94998f55bc6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba861d0a-3628-4cbd-931e-ba2c040fd88b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "50b14c0a-088e-4426-a82f-f94998f55bc6",
                    "LayerId": "e5ab2bdc-3114-485d-928c-065fa4eb32da"
                }
            ]
        },
        {
            "id": "6533082a-5da7-4171-828b-4b417db28595",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94343e71-905a-44e5-8972-f599938172ee",
            "compositeImage": {
                "id": "4471cec4-33c2-44d5-aa8f-df8c36904f94",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6533082a-5da7-4171-828b-4b417db28595",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65afb913-cd59-4b5e-a294-e2d2cf42b662",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6533082a-5da7-4171-828b-4b417db28595",
                    "LayerId": "e5ab2bdc-3114-485d-928c-065fa4eb32da"
                }
            ]
        },
        {
            "id": "18e21355-4b42-4825-993a-f2ae88ba7dff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94343e71-905a-44e5-8972-f599938172ee",
            "compositeImage": {
                "id": "9ca2c526-d1b6-4fbf-8731-ef061697657b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "18e21355-4b42-4825-993a-f2ae88ba7dff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f4eefc67-7322-4606-90ae-d7ddefd4bddc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "18e21355-4b42-4825-993a-f2ae88ba7dff",
                    "LayerId": "e5ab2bdc-3114-485d-928c-065fa4eb32da"
                }
            ]
        },
        {
            "id": "49ebfdb4-99f3-4d3b-9259-a102279ac290",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94343e71-905a-44e5-8972-f599938172ee",
            "compositeImage": {
                "id": "39f32739-d555-44bb-9b77-3b2991d64cc5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "49ebfdb4-99f3-4d3b-9259-a102279ac290",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f87e1757-f54b-4021-b919-0d5dc7b8d370",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49ebfdb4-99f3-4d3b-9259-a102279ac290",
                    "LayerId": "e5ab2bdc-3114-485d-928c-065fa4eb32da"
                }
            ]
        },
        {
            "id": "bd6944d0-9e10-41cc-9299-52f2d725ace7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94343e71-905a-44e5-8972-f599938172ee",
            "compositeImage": {
                "id": "8e54d241-f6ea-48e2-9687-4c6deece585a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd6944d0-9e10-41cc-9299-52f2d725ace7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a0f4ed8-c691-46a4-8d00-9d546a670034",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd6944d0-9e10-41cc-9299-52f2d725ace7",
                    "LayerId": "e5ab2bdc-3114-485d-928c-065fa4eb32da"
                }
            ]
        },
        {
            "id": "a85ebc70-23e8-4c41-a123-c80b2e5d5b07",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94343e71-905a-44e5-8972-f599938172ee",
            "compositeImage": {
                "id": "f2bfd6ca-f9bf-4655-bc63-4cf9f52de800",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a85ebc70-23e8-4c41-a123-c80b2e5d5b07",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13656677-465e-4cf1-a25e-b7bd970a1d8f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a85ebc70-23e8-4c41-a123-c80b2e5d5b07",
                    "LayerId": "e5ab2bdc-3114-485d-928c-065fa4eb32da"
                }
            ]
        },
        {
            "id": "17fb8e6c-0f35-43df-8183-8db42444a979",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94343e71-905a-44e5-8972-f599938172ee",
            "compositeImage": {
                "id": "96c69e83-704c-4697-aff7-d131791c7af8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "17fb8e6c-0f35-43df-8183-8db42444a979",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39e5bb8f-03aa-4272-882d-c2bf4aeef11b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "17fb8e6c-0f35-43df-8183-8db42444a979",
                    "LayerId": "e5ab2bdc-3114-485d-928c-065fa4eb32da"
                }
            ]
        },
        {
            "id": "c8e5d8f3-e60e-42a8-96f0-eed1b847daae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94343e71-905a-44e5-8972-f599938172ee",
            "compositeImage": {
                "id": "6b432ed2-575e-4978-b1ec-ee05d16a24ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8e5d8f3-e60e-42a8-96f0-eed1b847daae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3034fe52-050e-4329-aff1-04c42f6e2290",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8e5d8f3-e60e-42a8-96f0-eed1b847daae",
                    "LayerId": "e5ab2bdc-3114-485d-928c-065fa4eb32da"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "e5ab2bdc-3114-485d-928c-065fa4eb32da",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "94343e71-905a-44e5-8972-f599938172ee",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 0,
    "yorig": 0
}