{
    "id": "b349109f-3ff3-41bb-a3f7-b28cf4709bd7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sVrock_Idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 198,
    "bbox_left": 75,
    "bbox_right": 155,
    "bbox_top": 110,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3f55a2eb-a0fa-4779-a981-1bbc3b2dbcc8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b349109f-3ff3-41bb-a3f7-b28cf4709bd7",
            "compositeImage": {
                "id": "3ff0c761-b416-48d5-b008-d8b64701077d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f55a2eb-a0fa-4779-a981-1bbc3b2dbcc8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e6e68da7-315f-4275-97ed-d90c81cc3e81",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f55a2eb-a0fa-4779-a981-1bbc3b2dbcc8",
                    "LayerId": "507663e3-63d6-4c9b-b486-18a09a432190"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "507663e3-63d6-4c9b-b486-18a09a432190",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b349109f-3ff3-41bb-a3f7-b28cf4709bd7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 154,
    "xorig": 77,
    "yorig": 64
}