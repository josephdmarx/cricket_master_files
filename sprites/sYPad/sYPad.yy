{
    "id": "31fcd331-992c-4173-8ff2-2266856474e5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sYPad",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 24,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6ba63173-93c3-4fab-bcfa-533518b93172",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "31fcd331-992c-4173-8ff2-2266856474e5",
            "compositeImage": {
                "id": "216de8fc-4068-4eaf-ba4a-f8b552f9669a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ba63173-93c3-4fab-bcfa-533518b93172",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95d5b551-16b1-4f01-a102-7b195c1e65c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ba63173-93c3-4fab-bcfa-533518b93172",
                    "LayerId": "2d214a44-27ac-4fa5-9aef-d5cda9c25d02"
                }
            ]
        },
        {
            "id": "41ce9d32-1101-47d2-8ed5-9be321cc0b70",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "31fcd331-992c-4173-8ff2-2266856474e5",
            "compositeImage": {
                "id": "4be926b0-a669-4fab-8872-f46a6dfb2f4d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "41ce9d32-1101-47d2-8ed5-9be321cc0b70",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c24ae1d1-2038-4944-9706-9283e6923d01",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "41ce9d32-1101-47d2-8ed5-9be321cc0b70",
                    "LayerId": "2d214a44-27ac-4fa5-9aef-d5cda9c25d02"
                }
            ]
        },
        {
            "id": "6cc91fca-80e0-4032-9989-b3eac38daa2a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "31fcd331-992c-4173-8ff2-2266856474e5",
            "compositeImage": {
                "id": "2d513a7c-9e44-45e2-958a-ae7f83c31ea7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6cc91fca-80e0-4032-9989-b3eac38daa2a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9173f09-2fd5-4703-98fb-f29cdd670e41",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6cc91fca-80e0-4032-9989-b3eac38daa2a",
                    "LayerId": "2d214a44-27ac-4fa5-9aef-d5cda9c25d02"
                }
            ]
        },
        {
            "id": "38a1d055-de8e-4517-bed6-41d69901864f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "31fcd331-992c-4173-8ff2-2266856474e5",
            "compositeImage": {
                "id": "0ae03c87-84bb-44fe-9c7f-12563f9a9461",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38a1d055-de8e-4517-bed6-41d69901864f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "90d08f03-1df8-428a-9b73-9315328c5187",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38a1d055-de8e-4517-bed6-41d69901864f",
                    "LayerId": "2d214a44-27ac-4fa5-9aef-d5cda9c25d02"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "2d214a44-27ac-4fa5-9aef-d5cda9c25d02",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "31fcd331-992c-4173-8ff2-2266856474e5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 15,
    "yorig": 25
}