{
    "id": "f9142827-db88-4567-97e2-05b0ec637bfa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayerChargeA",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 94,
    "bbox_left": 58,
    "bbox_right": 72,
    "bbox_top": 42,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0a17d7db-9628-46af-a3d7-3f672391876e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f9142827-db88-4567-97e2-05b0ec637bfa",
            "compositeImage": {
                "id": "73dfa908-d405-439a-a0dd-25594e931032",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a17d7db-9628-46af-a3d7-3f672391876e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "176e822c-8580-44b4-9476-66e31404e66c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a17d7db-9628-46af-a3d7-3f672391876e",
                    "LayerId": "b100f7a0-a2d4-4a38-a2e2-161e37c9e3fc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "b100f7a0-a2d4-4a38-a2e2-161e37c9e3fc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f9142827-db88-4567-97e2-05b0ec637bfa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}