{
    "id": "dce1b871-2db2-4ac4-aa6c-712a5628eb7a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sEdanaTowerWalls01",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 879,
    "bbox_left": 186,
    "bbox_right": 871,
    "bbox_top": 483,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3f1da70a-5d9c-4c09-adb4-44298a6717b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dce1b871-2db2-4ac4-aa6c-712a5628eb7a",
            "compositeImage": {
                "id": "5af40ee8-062c-43c1-b769-d48b2e72b82e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f1da70a-5d9c-4c09-adb4-44298a6717b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6f55e36-55a6-475e-9ae2-a5b3f75d876c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f1da70a-5d9c-4c09-adb4-44298a6717b5",
                    "LayerId": "57c023fa-f990-4285-bd80-3804add5afa3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1024,
    "layers": [
        {
            "id": "57c023fa-f990-4285-bd80-3804add5afa3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dce1b871-2db2-4ac4-aa6c-712a5628eb7a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1024,
    "xorig": 0,
    "yorig": 0
}