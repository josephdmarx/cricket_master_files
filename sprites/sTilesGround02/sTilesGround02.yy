{
    "id": "2dd52e6f-c568-4353-8458-904b8792991d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTilesGround02",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 495,
    "bbox_left": 16,
    "bbox_right": 975,
    "bbox_top": 16,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "db1ba6c4-66a2-42ed-b5da-30909caebf0a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2dd52e6f-c568-4353-8458-904b8792991d",
            "compositeImage": {
                "id": "b8fd6563-b4c6-42b3-8a68-6372f4e548a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db1ba6c4-66a2-42ed-b5da-30909caebf0a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c7c95b8b-a11f-429e-9068-fdbe7849be64",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db1ba6c4-66a2-42ed-b5da-30909caebf0a",
                    "LayerId": "467c0a88-08bd-4c3c-b38c-d9154c90f134"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "467c0a88-08bd-4c3c-b38c-d9154c90f134",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2dd52e6f-c568-4353-8458-904b8792991d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1024,
    "xorig": -46,
    "yorig": 94
}