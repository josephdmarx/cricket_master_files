{
    "id": "ff3377d0-2fe5-403e-a27a-c4e6355e967d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayerM1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 94,
    "bbox_left": 58,
    "bbox_right": 72,
    "bbox_top": 42,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5a6a8edd-fe9f-437f-9334-16883a8bd3d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ff3377d0-2fe5-403e-a27a-c4e6355e967d",
            "compositeImage": {
                "id": "49193e43-9865-45d5-a982-013b94cef488",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a6a8edd-fe9f-437f-9334-16883a8bd3d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "043bdde1-23f9-4b9e-a6e7-f6abe0c5d79c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a6a8edd-fe9f-437f-9334-16883a8bd3d1",
                    "LayerId": "080b9421-a642-4958-9292-dbd54206e9e9"
                }
            ]
        },
        {
            "id": "18f013cc-7b44-4a60-8b7a-3ed0cabafb0f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ff3377d0-2fe5-403e-a27a-c4e6355e967d",
            "compositeImage": {
                "id": "67f5c6dc-bae9-4680-abc8-92f12232bba3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "18f013cc-7b44-4a60-8b7a-3ed0cabafb0f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b45696f4-18f1-4bc7-8808-522c9f43e7b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "18f013cc-7b44-4a60-8b7a-3ed0cabafb0f",
                    "LayerId": "080b9421-a642-4958-9292-dbd54206e9e9"
                }
            ]
        },
        {
            "id": "0a408aac-0e1d-418d-8085-37fdd4bbf9e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ff3377d0-2fe5-403e-a27a-c4e6355e967d",
            "compositeImage": {
                "id": "3770fac9-8e0d-4385-aeef-3ea522ea92da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a408aac-0e1d-418d-8085-37fdd4bbf9e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f6eb40b-c577-4b26-9f8e-77839a1213bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a408aac-0e1d-418d-8085-37fdd4bbf9e9",
                    "LayerId": "080b9421-a642-4958-9292-dbd54206e9e9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "080b9421-a642-4958-9292-dbd54206e9e9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ff3377d0-2fe5-403e-a27a-c4e6355e967d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}