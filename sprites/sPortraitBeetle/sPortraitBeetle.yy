{
    "id": "3344c8b5-b233-407d-bcd4-2fb1310c251f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPortraitBeetle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 17,
    "bbox_right": 85,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1b08760e-2ec6-4f40-b652-2b57e7be8a07",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3344c8b5-b233-407d-bcd4-2fb1310c251f",
            "compositeImage": {
                "id": "b28c7075-e63d-4df0-909e-c5932498e4b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b08760e-2ec6-4f40-b652-2b57e7be8a07",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e9bdfcd-38a9-47af-bb35-d82cd332d652",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b08760e-2ec6-4f40-b652-2b57e7be8a07",
                    "LayerId": "c9235525-0573-41be-856b-1c3ef09f84e4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "c9235525-0573-41be-856b-1c3ef09f84e4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3344c8b5-b233-407d-bcd4-2fb1310c251f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 0,
    "yorig": 0
}