{
    "id": "4360c43a-b753-4760-89c1-8e14c51af958",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_light_cone",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 511,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "822da60a-43d9-4519-9855-e5f6f910c492",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4360c43a-b753-4760-89c1-8e14c51af958",
            "compositeImage": {
                "id": "5160e661-4148-4641-a229-57fc0030b2b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "822da60a-43d9-4519-9855-e5f6f910c492",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d5201fb-514d-47d4-a5ce-f3a621e090f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "822da60a-43d9-4519-9855-e5f6f910c492",
                    "LayerId": "604efdd4-2050-4b91-85c0-80ca7711b832"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "604efdd4-2050-4b91-85c0-80ca7711b832",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4360c43a-b753-4760-89c1-8e14c51af958",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 512,
    "xorig": 29,
    "yorig": 65
}