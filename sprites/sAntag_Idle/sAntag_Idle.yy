{
    "id": "ee9c3021-31bc-4cb9-98ae-db20c9eae1b1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sAntag_Idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 140,
    "bbox_left": 36,
    "bbox_right": 92,
    "bbox_top": 35,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fc43b415-33fe-4b99-9133-5e5f0be912dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9c3021-31bc-4cb9-98ae-db20c9eae1b1",
            "compositeImage": {
                "id": "6f86b83e-b11c-43c2-bfa0-bac124aa42b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc43b415-33fe-4b99-9133-5e5f0be912dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8aee84b0-05d7-4b61-9c0f-8d46cd8bb96c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc43b415-33fe-4b99-9133-5e5f0be912dd",
                    "LayerId": "b21125a4-6e34-487c-aa41-50a727cfc303"
                }
            ]
        },
        {
            "id": "38810190-88fc-4eb9-aeec-ec98011a18e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9c3021-31bc-4cb9-98ae-db20c9eae1b1",
            "compositeImage": {
                "id": "1df67125-dac2-4e84-9d2b-c42999afd956",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38810190-88fc-4eb9-aeec-ec98011a18e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e633b00f-c7c4-4507-aacd-b44a77d7e256",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38810190-88fc-4eb9-aeec-ec98011a18e4",
                    "LayerId": "b21125a4-6e34-487c-aa41-50a727cfc303"
                }
            ]
        },
        {
            "id": "4ac0a0cf-9e21-4f90-b5f7-7025ab1c5757",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9c3021-31bc-4cb9-98ae-db20c9eae1b1",
            "compositeImage": {
                "id": "dc1ffd4c-bbae-4175-979d-2612d7dd9807",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ac0a0cf-9e21-4f90-b5f7-7025ab1c5757",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85dd59ab-03f3-43de-a2b6-11e5e7b0b6fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ac0a0cf-9e21-4f90-b5f7-7025ab1c5757",
                    "LayerId": "b21125a4-6e34-487c-aa41-50a727cfc303"
                }
            ]
        },
        {
            "id": "f86f460e-9bb4-4feb-a905-fc779de565be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9c3021-31bc-4cb9-98ae-db20c9eae1b1",
            "compositeImage": {
                "id": "9116496a-406c-41b7-8c68-8586e818ea72",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f86f460e-9bb4-4feb-a905-fc779de565be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65edc630-a915-4fba-b1be-ce528bfd8e88",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f86f460e-9bb4-4feb-a905-fc779de565be",
                    "LayerId": "b21125a4-6e34-487c-aa41-50a727cfc303"
                }
            ]
        },
        {
            "id": "a7f00a32-b291-4a9f-b109-b7ccd4fa46f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9c3021-31bc-4cb9-98ae-db20c9eae1b1",
            "compositeImage": {
                "id": "ebbdc2c1-f2e1-4408-9cf5-4c70ded69903",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7f00a32-b291-4a9f-b109-b7ccd4fa46f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ede7f2a-2aae-47de-bea3-db48bd2e11c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7f00a32-b291-4a9f-b109-b7ccd4fa46f7",
                    "LayerId": "b21125a4-6e34-487c-aa41-50a727cfc303"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 154,
    "layers": [
        {
            "id": "b21125a4-6e34-487c-aa41-50a727cfc303",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ee9c3021-31bc-4cb9-98ae-db20c9eae1b1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 77
}