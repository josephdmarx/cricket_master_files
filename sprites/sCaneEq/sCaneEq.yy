{
    "id": "dd22498d-a074-4c7a-ac37-1af1e9c4fe09",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCaneEq",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 26,
    "bbox_left": 0,
    "bbox_right": 29,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4e8693f0-2e4d-4012-9e34-8bd7ef2f1760",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd22498d-a074-4c7a-ac37-1af1e9c4fe09",
            "compositeImage": {
                "id": "7eb77b1d-0880-418f-b203-5e5c0845144c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4e8693f0-2e4d-4012-9e34-8bd7ef2f1760",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b365e7de-055a-41cb-9632-794187920640",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e8693f0-2e4d-4012-9e34-8bd7ef2f1760",
                    "LayerId": "8e0fe43f-ec12-4c12-b125-7c50dd3f6bb3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "8e0fe43f-ec12-4c12-b125-7c50dd3f6bb3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dd22498d-a074-4c7a-ac37-1af1e9c4fe09",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}