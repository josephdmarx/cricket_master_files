{
    "id": "922cede0-c008-46ee-84ba-ee450429c775",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGremlinKnife",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 4,
    "bbox_right": 27,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "05ffd733-ee0b-41de-949c-4bcb949d142a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "922cede0-c008-46ee-84ba-ee450429c775",
            "compositeImage": {
                "id": "d355a885-faec-4d72-b3fe-3d095315a12d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05ffd733-ee0b-41de-949c-4bcb949d142a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "093afc19-b6c8-414d-9238-7f69329ef431",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05ffd733-ee0b-41de-949c-4bcb949d142a",
                    "LayerId": "27a3cbd6-dfba-4488-8471-996dd5eb47ca"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "27a3cbd6-dfba-4488-8471-996dd5eb47ca",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "922cede0-c008-46ee-84ba-ee450429c775",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}