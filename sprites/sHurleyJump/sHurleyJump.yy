{
    "id": "f1bfb760-8255-4c3f-860f-46bce25e110b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sHurleyJump",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 59,
    "bbox_left": 37,
    "bbox_right": 71,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a80cf354-99cf-407e-b91d-27dc22dd8b65",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1bfb760-8255-4c3f-860f-46bce25e110b",
            "compositeImage": {
                "id": "a51e72b6-2700-4925-a6c9-72f8ee3afdaf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a80cf354-99cf-407e-b91d-27dc22dd8b65",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5db7730-5a3a-4712-9ffe-88d876c85902",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a80cf354-99cf-407e-b91d-27dc22dd8b65",
                    "LayerId": "63370e6c-209f-4ae1-95bd-dd329cf279c1"
                }
            ]
        },
        {
            "id": "511e21a8-d2d4-4b3d-add9-cce8ad5bc8c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1bfb760-8255-4c3f-860f-46bce25e110b",
            "compositeImage": {
                "id": "86c3b374-b337-44a3-b23a-e45fdbedb649",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "511e21a8-d2d4-4b3d-add9-cce8ad5bc8c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "199defad-380d-4f3d-8d4a-c5993f6f8ff1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "511e21a8-d2d4-4b3d-add9-cce8ad5bc8c5",
                    "LayerId": "63370e6c-209f-4ae1-95bd-dd329cf279c1"
                }
            ]
        },
        {
            "id": "0ab92735-9723-416c-aace-dbd3225e44d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1bfb760-8255-4c3f-860f-46bce25e110b",
            "compositeImage": {
                "id": "7193ba00-a771-4315-82b5-d83c0edabafd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ab92735-9723-416c-aace-dbd3225e44d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f7c10232-1c8d-4688-a768-55fe5b51ff5f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ab92735-9723-416c-aace-dbd3225e44d3",
                    "LayerId": "63370e6c-209f-4ae1-95bd-dd329cf279c1"
                }
            ]
        },
        {
            "id": "25a1fa94-79aa-4777-8d1f-f77fcf696e13",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1bfb760-8255-4c3f-860f-46bce25e110b",
            "compositeImage": {
                "id": "5648983b-97bd-405a-91ad-690250d0d54a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25a1fa94-79aa-4777-8d1f-f77fcf696e13",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "980fbe72-eeee-458d-b66b-33f95cb255f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25a1fa94-79aa-4777-8d1f-f77fcf696e13",
                    "LayerId": "63370e6c-209f-4ae1-95bd-dd329cf279c1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 65,
    "layers": [
        {
            "id": "63370e6c-209f-4ae1-95bd-dd329cf279c1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f1bfb760-8255-4c3f-860f-46bce25e110b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 108,
    "xorig": 54,
    "yorig": 32
}