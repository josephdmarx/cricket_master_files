{
    "id": "6ae0cda6-20f8-4430-b353-117932d403a5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGremlinCap",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 4,
    "bbox_right": 29,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "aefd1d7f-a69d-4802-9c1a-7ef055083a0c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6ae0cda6-20f8-4430-b353-117932d403a5",
            "compositeImage": {
                "id": "5b2a2782-e7d8-4074-9ebc-fdbde1b94ad4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aefd1d7f-a69d-4802-9c1a-7ef055083a0c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3552bdd8-ba1b-4dd5-a50b-86e83f699598",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aefd1d7f-a69d-4802-9c1a-7ef055083a0c",
                    "LayerId": "a0611eeb-2d78-4958-a4b7-529ed78e76f9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "a0611eeb-2d78-4958-a4b7-529ed78e76f9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6ae0cda6-20f8-4430-b353-117932d403a5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}