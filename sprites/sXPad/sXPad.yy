{
    "id": "5cf871f7-281c-46f0-a088-e4333904a981",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sXPad",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 7,
    "bbox_right": 24,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0ffb1249-b31f-421e-9bb8-7e11e0bdd4ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5cf871f7-281c-46f0-a088-e4333904a981",
            "compositeImage": {
                "id": "16277d54-739d-4f96-823b-2cc281389568",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ffb1249-b31f-421e-9bb8-7e11e0bdd4ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6fd39dc6-b899-4391-a287-c60a8896c7c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ffb1249-b31f-421e-9bb8-7e11e0bdd4ae",
                    "LayerId": "d4bffa58-6911-4850-81ff-64eba40d2392"
                }
            ]
        },
        {
            "id": "f4b50bdb-80c5-4069-be5f-ad7ad4255a98",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5cf871f7-281c-46f0-a088-e4333904a981",
            "compositeImage": {
                "id": "9d9caa58-0b9b-4d71-b575-6cdfbfa33942",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f4b50bdb-80c5-4069-be5f-ad7ad4255a98",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d38344d8-f622-4511-bf0b-0bd799ef4c30",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4b50bdb-80c5-4069-be5f-ad7ad4255a98",
                    "LayerId": "d4bffa58-6911-4850-81ff-64eba40d2392"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "d4bffa58-6911-4850-81ff-64eba40d2392",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5cf871f7-281c-46f0-a088-e4333904a981",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 15,
    "yorig": 25
}