{
    "id": "921cccda-9f40-479c-aab5-3040e58b58c4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sWasp_Idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 76,
    "bbox_left": 41,
    "bbox_right": 82,
    "bbox_top": 29,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ad715d43-6eb0-4679-90d9-ba9f5838079f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "921cccda-9f40-479c-aab5-3040e58b58c4",
            "compositeImage": {
                "id": "3f5b7ec0-80af-485f-92d4-985366b035db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad715d43-6eb0-4679-90d9-ba9f5838079f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b16b7462-ca63-4fb9-a82b-b671a9d11e9a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad715d43-6eb0-4679-90d9-ba9f5838079f",
                    "LayerId": "1ef22908-68a5-438d-a7d7-1075f0c5dd4d"
                }
            ]
        },
        {
            "id": "fbaee515-c648-4b6a-a5cc-9048f6b64715",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "921cccda-9f40-479c-aab5-3040e58b58c4",
            "compositeImage": {
                "id": "f43f85bc-4273-40ca-9bc4-a632a8180a75",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fbaee515-c648-4b6a-a5cc-9048f6b64715",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "920ec7f3-b071-4339-b3a5-f39308be6b63",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fbaee515-c648-4b6a-a5cc-9048f6b64715",
                    "LayerId": "1ef22908-68a5-438d-a7d7-1075f0c5dd4d"
                }
            ]
        },
        {
            "id": "2a42502b-3b0d-4e49-860f-46736af2fa1a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "921cccda-9f40-479c-aab5-3040e58b58c4",
            "compositeImage": {
                "id": "8309d40a-199e-4312-aeba-365c00cc6a97",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a42502b-3b0d-4e49-860f-46736af2fa1a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "379dd9ae-fe44-45de-8178-b2b811fb3768",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a42502b-3b0d-4e49-860f-46736af2fa1a",
                    "LayerId": "1ef22908-68a5-438d-a7d7-1075f0c5dd4d"
                }
            ]
        },
        {
            "id": "fb9fb895-15ff-497a-a3ad-81275b8d6bf7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "921cccda-9f40-479c-aab5-3040e58b58c4",
            "compositeImage": {
                "id": "3ad4955f-fbb6-4559-b125-a056b0ef45eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb9fb895-15ff-497a-a3ad-81275b8d6bf7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5bb5f870-92c8-428d-b6b8-b026159341d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb9fb895-15ff-497a-a3ad-81275b8d6bf7",
                    "LayerId": "1ef22908-68a5-438d-a7d7-1075f0c5dd4d"
                }
            ]
        },
        {
            "id": "a08b0450-7e9e-4e9e-a72f-d50bb1ee4d8e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "921cccda-9f40-479c-aab5-3040e58b58c4",
            "compositeImage": {
                "id": "c5154277-6e07-4cd3-bd5e-95a7d6a7f264",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a08b0450-7e9e-4e9e-a72f-d50bb1ee4d8e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "241f59da-6d19-4d7f-a105-bf756569b7ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a08b0450-7e9e-4e9e-a72f-d50bb1ee4d8e",
                    "LayerId": "1ef22908-68a5-438d-a7d7-1075f0c5dd4d"
                }
            ]
        },
        {
            "id": "2fb80c4e-6b17-49bd-b54e-e6dd5daa3840",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "921cccda-9f40-479c-aab5-3040e58b58c4",
            "compositeImage": {
                "id": "4f4a2919-f70e-481b-a8f0-b2a3d140cbf9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2fb80c4e-6b17-49bd-b54e-e6dd5daa3840",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d08090f-93ea-426f-bc01-5a14dbc2a435",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2fb80c4e-6b17-49bd-b54e-e6dd5daa3840",
                    "LayerId": "1ef22908-68a5-438d-a7d7-1075f0c5dd4d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "1ef22908-68a5-438d-a7d7-1075f0c5dd4d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "921cccda-9f40-479c-aab5-3040e58b58c4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}