{
    "id": "962894b8-0d39-47b5-9f9f-acc0f3dab96e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSpido_Attack",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 113,
    "bbox_left": 49,
    "bbox_right": 114,
    "bbox_top": 74,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7636576c-8741-44c6-93e5-eda319edc018",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "962894b8-0d39-47b5-9f9f-acc0f3dab96e",
            "compositeImage": {
                "id": "b9f77257-506d-485c-b80f-3a1a7b64189a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7636576c-8741-44c6-93e5-eda319edc018",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "92b929dd-5023-4832-b7c9-422bcf36e6e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7636576c-8741-44c6-93e5-eda319edc018",
                    "LayerId": "462ab0ae-8e0c-4fed-981b-58f174bef4a4"
                }
            ]
        },
        {
            "id": "722fffdc-ca4c-4a42-b44a-68eaf25b3eb7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "962894b8-0d39-47b5-9f9f-acc0f3dab96e",
            "compositeImage": {
                "id": "3b18c43e-22bc-47d7-b4bd-b6478859d6b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "722fffdc-ca4c-4a42-b44a-68eaf25b3eb7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "565e565d-23cb-4504-b044-e9bcd40c7cf4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "722fffdc-ca4c-4a42-b44a-68eaf25b3eb7",
                    "LayerId": "462ab0ae-8e0c-4fed-981b-58f174bef4a4"
                }
            ]
        },
        {
            "id": "0274d36f-c969-4700-9506-c50f75fc12ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "962894b8-0d39-47b5-9f9f-acc0f3dab96e",
            "compositeImage": {
                "id": "0384d6c4-bcee-4270-8ca4-291a196b1111",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0274d36f-c969-4700-9506-c50f75fc12ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19b66638-b8bf-4dae-af0b-02f2c26b415b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0274d36f-c969-4700-9506-c50f75fc12ef",
                    "LayerId": "462ab0ae-8e0c-4fed-981b-58f174bef4a4"
                }
            ]
        },
        {
            "id": "34b339ec-e413-4e93-8682-80ca758877f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "962894b8-0d39-47b5-9f9f-acc0f3dab96e",
            "compositeImage": {
                "id": "52e387d5-fcb1-432c-9000-ce14dec6c3de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34b339ec-e413-4e93-8682-80ca758877f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "239da281-9a4b-4229-935b-26bcc0b4332e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34b339ec-e413-4e93-8682-80ca758877f2",
                    "LayerId": "462ab0ae-8e0c-4fed-981b-58f174bef4a4"
                }
            ]
        },
        {
            "id": "599c6f6a-c2cb-4143-bc3c-da2ae015ef43",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "962894b8-0d39-47b5-9f9f-acc0f3dab96e",
            "compositeImage": {
                "id": "d09b71dc-e224-49c3-8a70-0d7514bfbdb7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "599c6f6a-c2cb-4143-bc3c-da2ae015ef43",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0492af3-b7a1-4b45-8cc3-fad81ddc2fc8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "599c6f6a-c2cb-4143-bc3c-da2ae015ef43",
                    "LayerId": "462ab0ae-8e0c-4fed-981b-58f174bef4a4"
                }
            ]
        },
        {
            "id": "f6ae0b64-308c-4367-b556-a00884cdc0db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "962894b8-0d39-47b5-9f9f-acc0f3dab96e",
            "compositeImage": {
                "id": "545541ce-506b-4149-88f9-e5f9073851dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6ae0b64-308c-4367-b556-a00884cdc0db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45dbbd12-7872-4e41-b278-41196f2c54b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6ae0b64-308c-4367-b556-a00884cdc0db",
                    "LayerId": "462ab0ae-8e0c-4fed-981b-58f174bef4a4"
                }
            ]
        },
        {
            "id": "2434e5f1-71b4-4aae-91ab-99492e9082f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "962894b8-0d39-47b5-9f9f-acc0f3dab96e",
            "compositeImage": {
                "id": "5f0b94b1-57e9-45f9-a696-876817164c85",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2434e5f1-71b4-4aae-91ab-99492e9082f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f1bdf46d-82a0-4fe3-8cd1-128688340856",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2434e5f1-71b4-4aae-91ab-99492e9082f2",
                    "LayerId": "462ab0ae-8e0c-4fed-981b-58f174bef4a4"
                }
            ]
        },
        {
            "id": "a806d6d6-26b5-4e60-b565-5e5f6a30519d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "962894b8-0d39-47b5-9f9f-acc0f3dab96e",
            "compositeImage": {
                "id": "d4d9d7de-0e8b-4870-87a9-e13256729e09",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a806d6d6-26b5-4e60-b565-5e5f6a30519d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a111deac-a636-40e8-b65c-f3152cdd5ebb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a806d6d6-26b5-4e60-b565-5e5f6a30519d",
                    "LayerId": "462ab0ae-8e0c-4fed-981b-58f174bef4a4"
                }
            ]
        },
        {
            "id": "60618597-a33a-465a-88e3-9970213bb114",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "962894b8-0d39-47b5-9f9f-acc0f3dab96e",
            "compositeImage": {
                "id": "c5500261-086c-4f0b-80ec-d68d377345df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "60618597-a33a-465a-88e3-9970213bb114",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "701db083-f4a4-4668-a83d-0f88513bc84c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "60618597-a33a-465a-88e3-9970213bb114",
                    "LayerId": "462ab0ae-8e0c-4fed-981b-58f174bef4a4"
                }
            ]
        },
        {
            "id": "cbd82dac-057f-4788-b40c-0d4de5fc95ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "962894b8-0d39-47b5-9f9f-acc0f3dab96e",
            "compositeImage": {
                "id": "743d2a15-729c-4414-8e14-631a3fa21e5f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cbd82dac-057f-4788-b40c-0d4de5fc95ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eaf9784b-8b1b-4142-9ffc-04145868d1ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cbd82dac-057f-4788-b40c-0d4de5fc95ef",
                    "LayerId": "462ab0ae-8e0c-4fed-981b-58f174bef4a4"
                }
            ]
        },
        {
            "id": "ad895962-df5b-48cf-b9ac-1d50f252c4f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "962894b8-0d39-47b5-9f9f-acc0f3dab96e",
            "compositeImage": {
                "id": "75a19536-bee6-4df5-933d-c9ffa2b52059",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad895962-df5b-48cf-b9ac-1d50f252c4f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "66127ad6-e680-4940-8099-b203b660cf4e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad895962-df5b-48cf-b9ac-1d50f252c4f1",
                    "LayerId": "462ab0ae-8e0c-4fed-981b-58f174bef4a4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "462ab0ae-8e0c-4fed-981b-58f174bef4a4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "962894b8-0d39-47b5-9f9f-acc0f3dab96e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 162,
    "xorig": 20,
    "yorig": 40
}