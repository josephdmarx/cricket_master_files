{
    "id": "96681bd2-9f8a-4fba-b4fc-61a06cd40f76",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCricketHouseRafters01",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 303,
    "bbox_left": 128,
    "bbox_right": 511,
    "bbox_top": 112,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "300fef47-bb03-4be7-b953-2e29e55f716f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96681bd2-9f8a-4fba-b4fc-61a06cd40f76",
            "compositeImage": {
                "id": "13e5433f-b424-470e-afe4-3252cbef2d4e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "300fef47-bb03-4be7-b953-2e29e55f716f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5279a68-738a-4190-9128-8813387fe2f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "300fef47-bb03-4be7-b953-2e29e55f716f",
                    "LayerId": "17e6f953-aae2-4601-b08c-f32fd51bc23f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 360,
    "layers": [
        {
            "id": "17e6f953-aae2-4601-b08c-f32fd51bc23f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "96681bd2-9f8a-4fba-b4fc-61a06cd40f76",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 0,
    "yorig": 0
}