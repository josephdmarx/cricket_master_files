{
    "id": "9371f814-3165-4906-a4e2-4569f2bb237b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sInsectWing",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 6,
    "bbox_right": 25,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f6ed9ab6-0870-4ac1-8d7e-da0f79b3e4fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9371f814-3165-4906-a4e2-4569f2bb237b",
            "compositeImage": {
                "id": "39829a52-01c1-4c26-84ed-66270cf3be81",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6ed9ab6-0870-4ac1-8d7e-da0f79b3e4fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89568920-3626-41ce-90b6-c707639a3d5e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6ed9ab6-0870-4ac1-8d7e-da0f79b3e4fa",
                    "LayerId": "e5acb325-9e31-43eb-b9e5-d3075fa3a6a3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "e5acb325-9e31-43eb-b9e5-d3075fa3a6a3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9371f814-3165-4906-a4e2-4569f2bb237b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}