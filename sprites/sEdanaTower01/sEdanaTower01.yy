{
    "id": "913e4dbb-a083-48b7-9516-6b8ae805f59d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sEdanaTower01",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1023,
    "bbox_left": 0,
    "bbox_right": 1023,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "237fd713-318c-4025-b851-5686c4304da6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "913e4dbb-a083-48b7-9516-6b8ae805f59d",
            "compositeImage": {
                "id": "eb231585-a39f-4112-833c-4a8b1c1d2d04",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "237fd713-318c-4025-b851-5686c4304da6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1904b9a3-a2de-463e-856e-8ed79203461d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "237fd713-318c-4025-b851-5686c4304da6",
                    "LayerId": "728f88f4-1cee-48b3-9af2-a468b2454560"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1024,
    "layers": [
        {
            "id": "728f88f4-1cee-48b3-9af2-a468b2454560",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "913e4dbb-a083-48b7-9516-6b8ae805f59d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1024,
    "xorig": 0,
    "yorig": 0
}