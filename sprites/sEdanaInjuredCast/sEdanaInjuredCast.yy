{
    "id": "f386816c-9885-40f3-8d96-690ec8d4226d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sEdanaInjuredCast",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 110,
    "bbox_left": 14,
    "bbox_right": 79,
    "bbox_top": 23,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4193a145-5995-4fd0-a7ba-f8a58172b2c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f386816c-9885-40f3-8d96-690ec8d4226d",
            "compositeImage": {
                "id": "c7da8410-f99b-45de-b310-213f20fbc00d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4193a145-5995-4fd0-a7ba-f8a58172b2c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54690f17-6cd7-4c63-88bf-3266143e319b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4193a145-5995-4fd0-a7ba-f8a58172b2c0",
                    "LayerId": "43fbf0a9-1be2-43b0-8002-db9cf63cac46"
                }
            ]
        },
        {
            "id": "4fe9bc9f-cf02-489b-90b5-325f3cf5605a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f386816c-9885-40f3-8d96-690ec8d4226d",
            "compositeImage": {
                "id": "3cf80c14-6920-484e-b19a-0e42cf687e1a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4fe9bc9f-cf02-489b-90b5-325f3cf5605a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f655023-0415-4b89-bbf4-22a3be52a8bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4fe9bc9f-cf02-489b-90b5-325f3cf5605a",
                    "LayerId": "43fbf0a9-1be2-43b0-8002-db9cf63cac46"
                }
            ]
        },
        {
            "id": "7d253150-640c-4ce5-947a-3a9b898babde",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f386816c-9885-40f3-8d96-690ec8d4226d",
            "compositeImage": {
                "id": "3b88f963-705d-4a7d-af61-4b463baae9f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d253150-640c-4ce5-947a-3a9b898babde",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f22b37f-4d4c-4c66-b1dc-3ba19dafe828",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d253150-640c-4ce5-947a-3a9b898babde",
                    "LayerId": "43fbf0a9-1be2-43b0-8002-db9cf63cac46"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "43fbf0a9-1be2-43b0-8002-db9cf63cac46",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f386816c-9885-40f3-8d96-690ec8d4226d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}