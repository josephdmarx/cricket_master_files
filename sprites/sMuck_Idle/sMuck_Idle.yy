{
    "id": "9c113f90-cea3-403f-af4b-41b1f1a1808f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sMuck_Idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 111,
    "bbox_left": 50,
    "bbox_right": 89,
    "bbox_top": 67,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "009463e1-83b3-4323-bc4d-fd14359c2206",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c113f90-cea3-403f-af4b-41b1f1a1808f",
            "compositeImage": {
                "id": "07cb4a7a-f2a4-4b50-afcd-643b6d50013c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "009463e1-83b3-4323-bc4d-fd14359c2206",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c33210a1-8acd-4c64-9c0d-f7ad49d6f8cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "009463e1-83b3-4323-bc4d-fd14359c2206",
                    "LayerId": "619070e4-457d-4ad4-87eb-ccb03507e736"
                }
            ]
        },
        {
            "id": "812b21ad-1663-4dbf-9c14-c86d5f0b11bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c113f90-cea3-403f-af4b-41b1f1a1808f",
            "compositeImage": {
                "id": "870c6341-6ba5-461e-9815-dc94cd21611b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "812b21ad-1663-4dbf-9c14-c86d5f0b11bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc2b8408-9d1b-42f8-b93a-92e4d5b5fb13",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "812b21ad-1663-4dbf-9c14-c86d5f0b11bc",
                    "LayerId": "619070e4-457d-4ad4-87eb-ccb03507e736"
                }
            ]
        },
        {
            "id": "fa777fc6-3aa2-4d64-aff0-8c4e01a4e02e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c113f90-cea3-403f-af4b-41b1f1a1808f",
            "compositeImage": {
                "id": "c8ce71ef-c684-49a5-906a-aa94d97b1293",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa777fc6-3aa2-4d64-aff0-8c4e01a4e02e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e561e74-c822-4954-92bb-37f5292db265",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa777fc6-3aa2-4d64-aff0-8c4e01a4e02e",
                    "LayerId": "619070e4-457d-4ad4-87eb-ccb03507e736"
                }
            ]
        },
        {
            "id": "5124eb6b-73f8-4b1e-85fb-5aae14d10a0d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c113f90-cea3-403f-af4b-41b1f1a1808f",
            "compositeImage": {
                "id": "c838f647-0f5d-4168-be81-22cf3fed4ac7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5124eb6b-73f8-4b1e-85fb-5aae14d10a0d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc85cef8-877f-405d-8612-7431768d4955",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5124eb6b-73f8-4b1e-85fb-5aae14d10a0d",
                    "LayerId": "619070e4-457d-4ad4-87eb-ccb03507e736"
                }
            ]
        },
        {
            "id": "0892e59f-ecb0-4a32-ad39-abe6e771e4bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c113f90-cea3-403f-af4b-41b1f1a1808f",
            "compositeImage": {
                "id": "a3021ced-0f9c-4634-b241-2818d714b14f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0892e59f-ecb0-4a32-ad39-abe6e771e4bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca68b1dd-245d-4115-8a3c-41403051bbbb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0892e59f-ecb0-4a32-ad39-abe6e771e4bc",
                    "LayerId": "619070e4-457d-4ad4-87eb-ccb03507e736"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "619070e4-457d-4ad4-87eb-ccb03507e736",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9c113f90-cea3-403f-af4b-41b1f1a1808f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 12,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 112
}