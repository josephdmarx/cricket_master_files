{
    "id": "8c7d38ba-ede2-4ffe-b48b-4f919d969762",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sConeflower",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 6,
    "bbox_right": 25,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "10122317-89f5-46c0-b8bf-997465a6276c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8c7d38ba-ede2-4ffe-b48b-4f919d969762",
            "compositeImage": {
                "id": "72c2d2cf-e447-4e57-9937-d976d5b157ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10122317-89f5-46c0-b8bf-997465a6276c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d41df401-9b6a-4a5f-bd83-bb200f14d50b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10122317-89f5-46c0-b8bf-997465a6276c",
                    "LayerId": "152a5f9f-e769-4a9d-933c-664cf571fe93"
                }
            ]
        },
        {
            "id": "a5667357-e61a-4e66-bd53-4c65b82c4c60",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8c7d38ba-ede2-4ffe-b48b-4f919d969762",
            "compositeImage": {
                "id": "719808d0-cb09-4252-863a-d3b22f3ad295",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a5667357-e61a-4e66-bd53-4c65b82c4c60",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "511c25cd-05c1-4887-8a58-806c12988e16",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5667357-e61a-4e66-bd53-4c65b82c4c60",
                    "LayerId": "152a5f9f-e769-4a9d-933c-664cf571fe93"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "152a5f9f-e769-4a9d-933c-664cf571fe93",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8c7d38ba-ede2-4ffe-b48b-4f919d969762",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}