{
    "id": "0fbdfbc5-165a-42e5-a861-29aa0d04c39a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayerM2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 94,
    "bbox_left": 58,
    "bbox_right": 72,
    "bbox_top": 42,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "aabe4563-ab84-423c-abab-9c59bc912207",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0fbdfbc5-165a-42e5-a861-29aa0d04c39a",
            "compositeImage": {
                "id": "ddeb2803-c544-46cb-9ec3-d54fae6b40dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aabe4563-ab84-423c-abab-9c59bc912207",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f398270e-295c-4df9-9ad3-fff6fbc7f5a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aabe4563-ab84-423c-abab-9c59bc912207",
                    "LayerId": "bec2415a-66dc-4b6f-9c82-07d0c2474fa6"
                }
            ]
        },
        {
            "id": "c98853f7-ae46-48ec-a538-dd6dc2484534",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0fbdfbc5-165a-42e5-a861-29aa0d04c39a",
            "compositeImage": {
                "id": "84edb387-8a6a-4373-b4c3-d1a6fe49b1a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c98853f7-ae46-48ec-a538-dd6dc2484534",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "547455f6-764f-49e0-9c68-61cdc5b1c269",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c98853f7-ae46-48ec-a538-dd6dc2484534",
                    "LayerId": "bec2415a-66dc-4b6f-9c82-07d0c2474fa6"
                }
            ]
        },
        {
            "id": "3f5a9f20-eeb1-4770-bc32-28fc07a5b220",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0fbdfbc5-165a-42e5-a861-29aa0d04c39a",
            "compositeImage": {
                "id": "8dc9177f-ba32-4c02-8802-9b2ee0601a95",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f5a9f20-eeb1-4770-bc32-28fc07a5b220",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3fe4f77b-299b-43bf-b158-f90925accb1f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f5a9f20-eeb1-4770-bc32-28fc07a5b220",
                    "LayerId": "bec2415a-66dc-4b6f-9c82-07d0c2474fa6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "bec2415a-66dc-4b6f-9c82-07d0c2474fa6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0fbdfbc5-165a-42e5-a861-29aa0d04c39a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}