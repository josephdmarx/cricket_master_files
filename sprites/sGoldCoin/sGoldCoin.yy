{
    "id": "590f2f87-2779-4daa-8967-f372674c8ae2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGoldCoin",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 11,
    "bbox_right": 20,
    "bbox_top": 22,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5817a3ea-4533-4a4d-8a99-7b4df429de5f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "590f2f87-2779-4daa-8967-f372674c8ae2",
            "compositeImage": {
                "id": "a4b7a852-13d8-4149-9839-c372cbc7e91a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5817a3ea-4533-4a4d-8a99-7b4df429de5f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0783c75c-497b-462d-8c35-5fef6ca67166",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5817a3ea-4533-4a4d-8a99-7b4df429de5f",
                    "LayerId": "6ba6f606-80b6-4dfa-97c1-c3590de1cc44"
                }
            ]
        },
        {
            "id": "d1738ef1-9adb-4e24-bff0-3dcfa9865b9f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "590f2f87-2779-4daa-8967-f372674c8ae2",
            "compositeImage": {
                "id": "356fc8a5-f94d-4b68-bb0d-bfb2209aa4ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1738ef1-9adb-4e24-bff0-3dcfa9865b9f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a1b8c11-8050-46bd-9038-fa7582eb28ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1738ef1-9adb-4e24-bff0-3dcfa9865b9f",
                    "LayerId": "6ba6f606-80b6-4dfa-97c1-c3590de1cc44"
                }
            ]
        },
        {
            "id": "6fc7ac10-e2fd-4c73-a082-7cf779fe2932",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "590f2f87-2779-4daa-8967-f372674c8ae2",
            "compositeImage": {
                "id": "88f6a69b-25f5-4b8d-a92a-86bbf78154ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6fc7ac10-e2fd-4c73-a082-7cf779fe2932",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f2a03d34-e730-4003-9b7c-40061a69e248",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6fc7ac10-e2fd-4c73-a082-7cf779fe2932",
                    "LayerId": "6ba6f606-80b6-4dfa-97c1-c3590de1cc44"
                }
            ]
        },
        {
            "id": "fdd93c7c-cd27-45bf-8073-2db087e0e75c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "590f2f87-2779-4daa-8967-f372674c8ae2",
            "compositeImage": {
                "id": "75e16faa-09cd-4919-9cb1-0a8f007c488b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fdd93c7c-cd27-45bf-8073-2db087e0e75c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1fb7eaa8-359b-4770-8d9a-7735ee678cc6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fdd93c7c-cd27-45bf-8073-2db087e0e75c",
                    "LayerId": "6ba6f606-80b6-4dfa-97c1-c3590de1cc44"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "6ba6f606-80b6-4dfa-97c1-c3590de1cc44",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "590f2f87-2779-4daa-8967-f372674c8ae2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 31
}