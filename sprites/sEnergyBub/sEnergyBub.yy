{
    "id": "75399ee4-bf6e-4a8a-8853-0440fa1bd579",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sEnergyBub",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 94,
    "bbox_left": 16,
    "bbox_right": 111,
    "bbox_top": 74,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e48019a7-9826-4e78-b629-3cb57f32f943",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75399ee4-bf6e-4a8a-8853-0440fa1bd579",
            "compositeImage": {
                "id": "6cc91c71-fb33-4c04-ba09-846b2129fb52",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e48019a7-9826-4e78-b629-3cb57f32f943",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "320c6557-4dbe-491e-b00f-84d9ef6092a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e48019a7-9826-4e78-b629-3cb57f32f943",
                    "LayerId": "bf42d387-b4d8-4ee2-a584-22da45bcfe79"
                }
            ]
        },
        {
            "id": "f02cf47d-b289-48bf-b019-0c22f1c09ca1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75399ee4-bf6e-4a8a-8853-0440fa1bd579",
            "compositeImage": {
                "id": "996a8ce7-d4bc-4c26-a49b-773bbdda347b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f02cf47d-b289-48bf-b019-0c22f1c09ca1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "726e4843-7840-4799-a778-1230cf74a294",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f02cf47d-b289-48bf-b019-0c22f1c09ca1",
                    "LayerId": "bf42d387-b4d8-4ee2-a584-22da45bcfe79"
                }
            ]
        },
        {
            "id": "62686626-d7cc-473d-bfbd-8bca18e17a22",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75399ee4-bf6e-4a8a-8853-0440fa1bd579",
            "compositeImage": {
                "id": "5ae763fc-0fe4-435f-8398-2431c28bee6a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62686626-d7cc-473d-bfbd-8bca18e17a22",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "024dbb32-53bb-4928-abcb-6196b365c32d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62686626-d7cc-473d-bfbd-8bca18e17a22",
                    "LayerId": "bf42d387-b4d8-4ee2-a584-22da45bcfe79"
                }
            ]
        },
        {
            "id": "64f787b6-3a25-4c7f-9f8c-5a4403b77ca2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75399ee4-bf6e-4a8a-8853-0440fa1bd579",
            "compositeImage": {
                "id": "f845a792-9d84-46d4-886d-6b67105084f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "64f787b6-3a25-4c7f-9f8c-5a4403b77ca2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a13ba06-571f-460b-b8a2-41d5ccef54b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "64f787b6-3a25-4c7f-9f8c-5a4403b77ca2",
                    "LayerId": "bf42d387-b4d8-4ee2-a584-22da45bcfe79"
                }
            ]
        },
        {
            "id": "f6aa51bf-3fed-4a64-b14c-9160b5a2a756",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75399ee4-bf6e-4a8a-8853-0440fa1bd579",
            "compositeImage": {
                "id": "e4438d92-7a3f-44ce-af06-d89781e0bb58",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6aa51bf-3fed-4a64-b14c-9160b5a2a756",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "316db924-9871-4663-807c-d0f57bca7899",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6aa51bf-3fed-4a64-b14c-9160b5a2a756",
                    "LayerId": "bf42d387-b4d8-4ee2-a584-22da45bcfe79"
                }
            ]
        },
        {
            "id": "55252e09-2610-485a-bcba-2577303d0558",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75399ee4-bf6e-4a8a-8853-0440fa1bd579",
            "compositeImage": {
                "id": "e9f2efaa-8cfa-4d62-9801-24a9ea3e02a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55252e09-2610-485a-bcba-2577303d0558",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9445d900-7044-4472-8b31-4094f77bd386",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55252e09-2610-485a-bcba-2577303d0558",
                    "LayerId": "bf42d387-b4d8-4ee2-a584-22da45bcfe79"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "bf42d387-b4d8-4ee2-a584-22da45bcfe79",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "75399ee4-bf6e-4a8a-8853-0440fa1bd579",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 95
}