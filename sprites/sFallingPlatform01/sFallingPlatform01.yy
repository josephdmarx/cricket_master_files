{
    "id": "bee10a14-a349-4827-b63b-d3d9cbc48a5c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sFallingPlatform01",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 46,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 16,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b10a76db-0eb4-4fad-a1e4-71d044e760dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bee10a14-a349-4827-b63b-d3d9cbc48a5c",
            "compositeImage": {
                "id": "7be3765a-f7b4-44fc-9765-ab6c52f22df1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b10a76db-0eb4-4fad-a1e4-71d044e760dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "207ea5b5-e4bd-413e-b678-4425a614aa58",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b10a76db-0eb4-4fad-a1e4-71d044e760dd",
                    "LayerId": "22b3c862-fb70-41d0-9181-e0164ba84627"
                }
            ]
        }
    ],
    "gridX": 32,
    "gridY": 32,
    "height": 48,
    "layers": [
        {
            "id": "22b3c862-fb70-41d0-9181-e0164ba84627",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bee10a14-a349-4827-b63b-d3d9cbc48a5c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 16
}