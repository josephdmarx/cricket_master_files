{
    "id": "ef19ad65-e5b2-4e5c-883b-aa028ffe508f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayerA2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 94,
    "bbox_left": 58,
    "bbox_right": 72,
    "bbox_top": 42,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dfa363f8-1ffa-4210-b96d-eace2df1f9d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ef19ad65-e5b2-4e5c-883b-aa028ffe508f",
            "compositeImage": {
                "id": "2c9805bd-a391-467c-9c79-c5a6c285133e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dfa363f8-1ffa-4210-b96d-eace2df1f9d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa7fbb22-a1d6-4cbd-97bf-a68382f13cdc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dfa363f8-1ffa-4210-b96d-eace2df1f9d1",
                    "LayerId": "2040ba78-0c61-4196-84ed-7ebf00c91c0a"
                }
            ]
        },
        {
            "id": "007bd8de-54c5-406e-89a5-48a28fa42cda",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ef19ad65-e5b2-4e5c-883b-aa028ffe508f",
            "compositeImage": {
                "id": "131a42ff-12d7-44ae-bd7e-04f726214000",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "007bd8de-54c5-406e-89a5-48a28fa42cda",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b5e6a36-12ae-4eb9-87de-546ec1d5896e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "007bd8de-54c5-406e-89a5-48a28fa42cda",
                    "LayerId": "2040ba78-0c61-4196-84ed-7ebf00c91c0a"
                }
            ]
        },
        {
            "id": "86e50dcc-05df-4202-8652-5efbba74be87",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ef19ad65-e5b2-4e5c-883b-aa028ffe508f",
            "compositeImage": {
                "id": "b875e72e-50a9-46a4-b5b2-8603490e9eae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "86e50dcc-05df-4202-8652-5efbba74be87",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "676be50e-6cfb-4b8a-8329-cd12af609373",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86e50dcc-05df-4202-8652-5efbba74be87",
                    "LayerId": "2040ba78-0c61-4196-84ed-7ebf00c91c0a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "2040ba78-0c61-4196-84ed-7ebf00c91c0a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ef19ad65-e5b2-4e5c-883b-aa028ffe508f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": -51,
    "yorig": 67
}