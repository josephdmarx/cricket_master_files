{
    "id": "ccf373fd-e17b-4205-b9c0-92113620bdef",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBeetleLookup",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 46,
    "bbox_right": 73,
    "bbox_top": 36,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "62015f18-fb1f-4c09-8d76-eb68c2c2e5a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ccf373fd-e17b-4205-b9c0-92113620bdef",
            "compositeImage": {
                "id": "773aadf5-c865-49e7-a9fc-944d7a71e097",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62015f18-fb1f-4c09-8d76-eb68c2c2e5a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a1b742cb-c8f2-489f-8ad4-2b450894fce1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62015f18-fb1f-4c09-8d76-eb68c2c2e5a6",
                    "LayerId": "735d1ad4-0fd0-4f7a-9ea6-6b54e9ad6978"
                }
            ]
        },
        {
            "id": "0465981f-018b-4827-9757-ce8ec8bd049c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ccf373fd-e17b-4205-b9c0-92113620bdef",
            "compositeImage": {
                "id": "7b92d531-af91-44c3-9eae-a87238174c26",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0465981f-018b-4827-9757-ce8ec8bd049c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e977a3c-1c5b-4f41-8436-2987739e49fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0465981f-018b-4827-9757-ce8ec8bd049c",
                    "LayerId": "735d1ad4-0fd0-4f7a-9ea6-6b54e9ad6978"
                }
            ]
        },
        {
            "id": "abf91802-4abb-4215-8787-cf3bd647d9e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ccf373fd-e17b-4205-b9c0-92113620bdef",
            "compositeImage": {
                "id": "1a80613c-6608-4b7f-8672-c1aa517fdf87",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "abf91802-4abb-4215-8787-cf3bd647d9e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4dd2c236-ed09-45cb-b4e2-81bf8e2bdb88",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "abf91802-4abb-4215-8787-cf3bd647d9e9",
                    "LayerId": "735d1ad4-0fd0-4f7a-9ea6-6b54e9ad6978"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "735d1ad4-0fd0-4f7a-9ea6-6b54e9ad6978",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ccf373fd-e17b-4205-b9c0-92113620bdef",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}