{
    "id": "f22286bb-4167-4d08-a2a5-b8152238303a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBubbleSmall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 14,
    "bbox_right": 49,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8de94077-2953-4047-920a-1a19030d0c02",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f22286bb-4167-4d08-a2a5-b8152238303a",
            "compositeImage": {
                "id": "08e90e2c-9b42-4f5f-991c-54b5ec2a5a41",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8de94077-2953-4047-920a-1a19030d0c02",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "618e9378-e6d4-4a5b-b154-0f7e5a3d1299",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8de94077-2953-4047-920a-1a19030d0c02",
                    "LayerId": "25635298-60bd-498d-ac4f-53a9d7b3c0b4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 33,
    "layers": [
        {
            "id": "25635298-60bd-498d-ac4f-53a9d7b3c0b4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f22286bb-4167-4d08-a2a5-b8152238303a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 62,
    "xorig": 20,
    "yorig": 29
}