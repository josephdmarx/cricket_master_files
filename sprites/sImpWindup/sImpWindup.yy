{
    "id": "4315ae25-db94-4e6e-a5a0-a7c2d35148d2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sImpWindup",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 43,
    "bbox_left": 13,
    "bbox_right": 26,
    "bbox_top": 28,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f4c0f8a7-6e16-49ed-9f35-902e3512ee29",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4315ae25-db94-4e6e-a5a0-a7c2d35148d2",
            "compositeImage": {
                "id": "edcd0018-bafb-4616-b057-9aad52973be7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f4c0f8a7-6e16-49ed-9f35-902e3512ee29",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f4477929-5900-4e56-b6d8-25b1fe5782d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4c0f8a7-6e16-49ed-9f35-902e3512ee29",
                    "LayerId": "22a96421-c420-4726-86ba-b53c85f804a5"
                }
            ]
        },
        {
            "id": "ba891207-fbc1-4e52-b017-2f7de2431cbe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4315ae25-db94-4e6e-a5a0-a7c2d35148d2",
            "compositeImage": {
                "id": "4ef5c7c0-4ab3-4fc8-bcca-8d94898b3425",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba891207-fbc1-4e52-b017-2f7de2431cbe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "94001f90-79bd-465c-a0d4-d21f1e833489",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba891207-fbc1-4e52-b017-2f7de2431cbe",
                    "LayerId": "22a96421-c420-4726-86ba-b53c85f804a5"
                }
            ]
        },
        {
            "id": "bc38344f-c234-4609-812d-6daad0eccd0d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4315ae25-db94-4e6e-a5a0-a7c2d35148d2",
            "compositeImage": {
                "id": "8397f2c3-ba0a-4162-b1fe-ed17efc10e40",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc38344f-c234-4609-812d-6daad0eccd0d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5341220-bd1a-4cd7-bb20-b42c0b5b9440",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc38344f-c234-4609-812d-6daad0eccd0d",
                    "LayerId": "22a96421-c420-4726-86ba-b53c85f804a5"
                }
            ]
        },
        {
            "id": "326580ee-a205-4a41-bf97-fa3095f13ad2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4315ae25-db94-4e6e-a5a0-a7c2d35148d2",
            "compositeImage": {
                "id": "b7831cf9-b0a9-4bcf-8275-c2807b49df66",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "326580ee-a205-4a41-bf97-fa3095f13ad2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be16eb14-94f7-425e-9399-cc50fd857857",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "326580ee-a205-4a41-bf97-fa3095f13ad2",
                    "LayerId": "22a96421-c420-4726-86ba-b53c85f804a5"
                }
            ]
        },
        {
            "id": "76473d76-88ec-4baf-bdca-353a94a4c294",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4315ae25-db94-4e6e-a5a0-a7c2d35148d2",
            "compositeImage": {
                "id": "f33d312e-a01c-40f6-8213-265beec67e25",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76473d76-88ec-4baf-bdca-353a94a4c294",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "907d2968-91ea-4db3-968b-d63043884371",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76473d76-88ec-4baf-bdca-353a94a4c294",
                    "LayerId": "22a96421-c420-4726-86ba-b53c85f804a5"
                }
            ]
        },
        {
            "id": "8685c225-b1f6-45c6-9e7b-04f83b894b24",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4315ae25-db94-4e6e-a5a0-a7c2d35148d2",
            "compositeImage": {
                "id": "862e6871-7b57-4ee7-b8b7-4eba603f714e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8685c225-b1f6-45c6-9e7b-04f83b894b24",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "81ed446e-eba3-46e9-bbf9-55af21db115b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8685c225-b1f6-45c6-9e7b-04f83b894b24",
                    "LayerId": "22a96421-c420-4726-86ba-b53c85f804a5"
                }
            ]
        },
        {
            "id": "e6b5b88f-f66e-496c-aad2-891c447736a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4315ae25-db94-4e6e-a5a0-a7c2d35148d2",
            "compositeImage": {
                "id": "54110696-bd6b-420e-9d83-21d5196a6580",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6b5b88f-f66e-496c-aad2-891c447736a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6314e41d-2cac-40ac-bcad-7cebe8c3837e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6b5b88f-f66e-496c-aad2-891c447736a2",
                    "LayerId": "22a96421-c420-4726-86ba-b53c85f804a5"
                }
            ]
        },
        {
            "id": "2f0a98ce-b0dc-4d50-810e-bc72f5c2c25c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4315ae25-db94-4e6e-a5a0-a7c2d35148d2",
            "compositeImage": {
                "id": "9327e70c-00fd-4954-8592-05b76f51bd95",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f0a98ce-b0dc-4d50-810e-bc72f5c2c25c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b929e15c-3da4-4ab1-893d-fde13d1727b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f0a98ce-b0dc-4d50-810e-bc72f5c2c25c",
                    "LayerId": "22a96421-c420-4726-86ba-b53c85f804a5"
                }
            ]
        },
        {
            "id": "cae1b9fc-f8c9-44a0-be05-8ea63015e362",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4315ae25-db94-4e6e-a5a0-a7c2d35148d2",
            "compositeImage": {
                "id": "1f050d5f-4d31-4887-8c64-ee649b1aed6a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cae1b9fc-f8c9-44a0-be05-8ea63015e362",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b4bcc637-c1a6-40c3-97e7-dd370f19fdca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cae1b9fc-f8c9-44a0-be05-8ea63015e362",
                    "LayerId": "22a96421-c420-4726-86ba-b53c85f804a5"
                }
            ]
        },
        {
            "id": "53ebf15a-4dcf-41f6-a3fa-88c70fba849d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4315ae25-db94-4e6e-a5a0-a7c2d35148d2",
            "compositeImage": {
                "id": "2bc1fe68-b61b-445a-999b-3356f108f2b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "53ebf15a-4dcf-41f6-a3fa-88c70fba849d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d49ce8c7-ff4e-4244-af0d-97f0d5bbcceb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "53ebf15a-4dcf-41f6-a3fa-88c70fba849d",
                    "LayerId": "22a96421-c420-4726-86ba-b53c85f804a5"
                }
            ]
        },
        {
            "id": "13fe4b8f-c426-429a-a63c-e424954ebcb7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4315ae25-db94-4e6e-a5a0-a7c2d35148d2",
            "compositeImage": {
                "id": "d581f658-be3f-4e67-a56a-9860511aeb5d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13fe4b8f-c426-429a-a63c-e424954ebcb7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f60d153c-6b69-496f-8351-734e481bae02",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13fe4b8f-c426-429a-a63c-e424954ebcb7",
                    "LayerId": "22a96421-c420-4726-86ba-b53c85f804a5"
                }
            ]
        },
        {
            "id": "014ecb55-b3b2-46a6-85cf-812c2ed49581",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4315ae25-db94-4e6e-a5a0-a7c2d35148d2",
            "compositeImage": {
                "id": "7d507f5f-e56e-4abd-ac64-356e43ba8531",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "014ecb55-b3b2-46a6-85cf-812c2ed49581",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e7831af6-8072-4a22-b83c-09ba492264de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "014ecb55-b3b2-46a6-85cf-812c2ed49581",
                    "LayerId": "22a96421-c420-4726-86ba-b53c85f804a5"
                }
            ]
        },
        {
            "id": "f6349882-0ca5-426e-a8b7-4b6d68a645f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4315ae25-db94-4e6e-a5a0-a7c2d35148d2",
            "compositeImage": {
                "id": "b5b1ea19-649f-4125-a773-0e4fb2348764",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6349882-0ca5-426e-a8b7-4b6d68a645f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "804f8493-4f53-4307-862e-9ba65bf8a590",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6349882-0ca5-426e-a8b7-4b6d68a645f0",
                    "LayerId": "22a96421-c420-4726-86ba-b53c85f804a5"
                }
            ]
        },
        {
            "id": "2022e188-0d75-47fc-b344-1e3ebfe3c0c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4315ae25-db94-4e6e-a5a0-a7c2d35148d2",
            "compositeImage": {
                "id": "c0158cbe-2763-4a7f-95e9-3bdc6cde5c29",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2022e188-0d75-47fc-b344-1e3ebfe3c0c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd3dfa06-d09c-4d5e-9729-e1bcd08b2c16",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2022e188-0d75-47fc-b344-1e3ebfe3c0c1",
                    "LayerId": "22a96421-c420-4726-86ba-b53c85f804a5"
                }
            ]
        },
        {
            "id": "e90d1db3-2330-415b-93ae-3d0626fdffce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4315ae25-db94-4e6e-a5a0-a7c2d35148d2",
            "compositeImage": {
                "id": "9754c353-27d2-41f3-b22c-b5abc5f14068",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e90d1db3-2330-415b-93ae-3d0626fdffce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c72f5638-af2a-42a1-8e75-d550e87e1895",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e90d1db3-2330-415b-93ae-3d0626fdffce",
                    "LayerId": "22a96421-c420-4726-86ba-b53c85f804a5"
                }
            ]
        },
        {
            "id": "0ad022a5-999b-4319-85b0-0e813ef1866a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4315ae25-db94-4e6e-a5a0-a7c2d35148d2",
            "compositeImage": {
                "id": "8e1d6b2f-174a-40ac-8621-4312863208e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ad022a5-999b-4319-85b0-0e813ef1866a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14cf5d0f-79d9-4bdb-8f8c-f996fab42624",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ad022a5-999b-4319-85b0-0e813ef1866a",
                    "LayerId": "22a96421-c420-4726-86ba-b53c85f804a5"
                }
            ]
        },
        {
            "id": "d23dc52c-f3a6-4349-89ce-63b4f3b8b858",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4315ae25-db94-4e6e-a5a0-a7c2d35148d2",
            "compositeImage": {
                "id": "0e0b0884-4eae-456e-a084-fa67bf773efb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d23dc52c-f3a6-4349-89ce-63b4f3b8b858",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d156d099-71b1-44f7-9f26-9f12189a0800",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d23dc52c-f3a6-4349-89ce-63b4f3b8b858",
                    "LayerId": "22a96421-c420-4726-86ba-b53c85f804a5"
                }
            ]
        },
        {
            "id": "38a6bca9-efe2-4305-b4c2-9ab65c193037",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4315ae25-db94-4e6e-a5a0-a7c2d35148d2",
            "compositeImage": {
                "id": "27c213d8-a741-42dd-9d95-da1b193bc07f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38a6bca9-efe2-4305-b4c2-9ab65c193037",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "675b031e-7456-4a8a-aa84-5fce60db2761",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38a6bca9-efe2-4305-b4c2-9ab65c193037",
                    "LayerId": "22a96421-c420-4726-86ba-b53c85f804a5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 58,
    "layers": [
        {
            "id": "22a96421-c420-4726-86ba-b53c85f804a5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4315ae25-db94-4e6e-a5a0-a7c2d35148d2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 25,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 41,
    "xorig": 20,
    "yorig": 40
}