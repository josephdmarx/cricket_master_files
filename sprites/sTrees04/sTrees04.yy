{
    "id": "04a87101-fe09-4ac3-b499-85b6c27407da",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTrees04",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 359,
    "bbox_left": 0,
    "bbox_right": 639,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "59ea51da-fbb4-4e8c-b609-b8a99a331293",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "04a87101-fe09-4ac3-b499-85b6c27407da",
            "compositeImage": {
                "id": "d53d09fa-038a-45ed-9dc5-d8a01a5870f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59ea51da-fbb4-4e8c-b609-b8a99a331293",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb79c0c7-e6f5-4d41-b0ac-278a4df16bc8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59ea51da-fbb4-4e8c-b609-b8a99a331293",
                    "LayerId": "cb705ad7-520d-4d6f-8e2b-757d0ac4875b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 360,
    "layers": [
        {
            "id": "cb705ad7-520d-4d6f-8e2b-757d0ac4875b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "04a87101-fe09-4ac3-b499-85b6c27407da",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 0,
    "yorig": 0
}