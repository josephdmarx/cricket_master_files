{
    "id": "714d530b-af52-4f53-913a-864a9456810a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sFire01D",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 8,
    "bbox_right": 23,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3cfe5fd5-1c3f-4a97-9ea0-b7deebfc3373",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "714d530b-af52-4f53-913a-864a9456810a",
            "compositeImage": {
                "id": "d2f16e50-d5f5-4d9c-9eed-6b50cf234878",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3cfe5fd5-1c3f-4a97-9ea0-b7deebfc3373",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c5bbc09-1dae-4e45-b894-dbd3aa9e8fc9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3cfe5fd5-1c3f-4a97-9ea0-b7deebfc3373",
                    "LayerId": "3f327b80-8c20-41b9-a5d3-8d566b254dcb"
                }
            ]
        },
        {
            "id": "6619a141-cb47-4c07-b920-6e70f83bd4aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "714d530b-af52-4f53-913a-864a9456810a",
            "compositeImage": {
                "id": "9e8eeb1a-e208-4938-bcbe-bdf8eed67076",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6619a141-cb47-4c07-b920-6e70f83bd4aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "35878001-80a7-4afb-90f2-2139e6db3e05",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6619a141-cb47-4c07-b920-6e70f83bd4aa",
                    "LayerId": "3f327b80-8c20-41b9-a5d3-8d566b254dcb"
                }
            ]
        },
        {
            "id": "e4a60771-57b4-472f-b754-390831612bcf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "714d530b-af52-4f53-913a-864a9456810a",
            "compositeImage": {
                "id": "561449d8-d940-4e6b-b2b0-dcb5a13ac52f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e4a60771-57b4-472f-b754-390831612bcf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed3984b0-d579-4af7-9067-628c3f78437f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4a60771-57b4-472f-b754-390831612bcf",
                    "LayerId": "3f327b80-8c20-41b9-a5d3-8d566b254dcb"
                }
            ]
        },
        {
            "id": "9eaaed33-aba8-4879-9e5b-1fb032316100",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "714d530b-af52-4f53-913a-864a9456810a",
            "compositeImage": {
                "id": "4818c5ac-ddb2-46fd-957e-b4e5adbbcbe6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9eaaed33-aba8-4879-9e5b-1fb032316100",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46294549-730b-445e-b1ed-746d235152e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9eaaed33-aba8-4879-9e5b-1fb032316100",
                    "LayerId": "3f327b80-8c20-41b9-a5d3-8d566b254dcb"
                }
            ]
        },
        {
            "id": "729985f4-39fa-4a28-807c-815a464781fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "714d530b-af52-4f53-913a-864a9456810a",
            "compositeImage": {
                "id": "95485c9a-beca-40bf-a29c-9ebeca561cc1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "729985f4-39fa-4a28-807c-815a464781fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "350308e5-7269-4da3-84ed-0b79d2e4dab3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "729985f4-39fa-4a28-807c-815a464781fd",
                    "LayerId": "3f327b80-8c20-41b9-a5d3-8d566b254dcb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "3f327b80-8c20-41b9-a5d3-8d566b254dcb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "714d530b-af52-4f53-913a-864a9456810a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}