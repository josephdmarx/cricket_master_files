{
    "id": "7eb3d734-ca38-48f9-b5e5-6aa2bfcf42ec",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayerWLegs",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 94,
    "bbox_left": 58,
    "bbox_right": 72,
    "bbox_top": 42,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9acafac0-8eee-4a0a-99d6-6f4bad063ff1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7eb3d734-ca38-48f9-b5e5-6aa2bfcf42ec",
            "compositeImage": {
                "id": "c52c191d-ce47-47be-a518-42a109fe7a5e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9acafac0-8eee-4a0a-99d6-6f4bad063ff1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ebe89f9-1f54-4e6d-bd5a-cce0d7437071",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9acafac0-8eee-4a0a-99d6-6f4bad063ff1",
                    "LayerId": "953086eb-3d2d-47fb-bc8b-f4dbf20815b7"
                }
            ]
        },
        {
            "id": "a9153541-1041-43c6-97d2-a85e7bf70b6b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7eb3d734-ca38-48f9-b5e5-6aa2bfcf42ec",
            "compositeImage": {
                "id": "caaa83ee-30c3-4743-9724-fea431a4f69d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a9153541-1041-43c6-97d2-a85e7bf70b6b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "224c6ca8-e9b0-4f15-afc5-ad1f52fa6a54",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a9153541-1041-43c6-97d2-a85e7bf70b6b",
                    "LayerId": "953086eb-3d2d-47fb-bc8b-f4dbf20815b7"
                }
            ]
        },
        {
            "id": "c70c1dec-6214-4c05-aa56-b910146c0f8d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7eb3d734-ca38-48f9-b5e5-6aa2bfcf42ec",
            "compositeImage": {
                "id": "2b65c26f-936a-460d-883f-e31e035b1254",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c70c1dec-6214-4c05-aa56-b910146c0f8d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b80c3a1-8bbb-45a6-b642-7bfa72e666d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c70c1dec-6214-4c05-aa56-b910146c0f8d",
                    "LayerId": "953086eb-3d2d-47fb-bc8b-f4dbf20815b7"
                }
            ]
        },
        {
            "id": "3aa5b2ed-d69e-445a-974d-58e148889b0a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7eb3d734-ca38-48f9-b5e5-6aa2bfcf42ec",
            "compositeImage": {
                "id": "44b159a3-b5c9-4ceb-b96f-cccbcc0a655a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3aa5b2ed-d69e-445a-974d-58e148889b0a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9b27bd1-4d90-4782-af5c-d5c59566e1a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3aa5b2ed-d69e-445a-974d-58e148889b0a",
                    "LayerId": "953086eb-3d2d-47fb-bc8b-f4dbf20815b7"
                }
            ]
        },
        {
            "id": "f03c262d-d3f9-46e4-904a-0086c4946121",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7eb3d734-ca38-48f9-b5e5-6aa2bfcf42ec",
            "compositeImage": {
                "id": "53504533-0527-453c-9bf8-9ebad4300c31",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f03c262d-d3f9-46e4-904a-0086c4946121",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c33d152-c64f-4a07-94e6-a5c1628fe06d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f03c262d-d3f9-46e4-904a-0086c4946121",
                    "LayerId": "953086eb-3d2d-47fb-bc8b-f4dbf20815b7"
                }
            ]
        },
        {
            "id": "f482197a-2b60-4bc4-b714-2bbd10e887d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7eb3d734-ca38-48f9-b5e5-6aa2bfcf42ec",
            "compositeImage": {
                "id": "36aa58db-6cc4-4093-8da2-6e1c250be8c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f482197a-2b60-4bc4-b714-2bbd10e887d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a2903fa6-124e-4d8d-91f3-889d2b8dd5ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f482197a-2b60-4bc4-b714-2bbd10e887d2",
                    "LayerId": "953086eb-3d2d-47fb-bc8b-f4dbf20815b7"
                }
            ]
        },
        {
            "id": "1b601722-43f8-4270-abc9-870794ac2421",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7eb3d734-ca38-48f9-b5e5-6aa2bfcf42ec",
            "compositeImage": {
                "id": "f534162f-8858-483e-a1f5-6fdd68fda836",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b601722-43f8-4270-abc9-870794ac2421",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad3efb02-7095-49e8-9c0b-46dc857053c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b601722-43f8-4270-abc9-870794ac2421",
                    "LayerId": "953086eb-3d2d-47fb-bc8b-f4dbf20815b7"
                }
            ]
        },
        {
            "id": "df4b840c-eba6-4ed6-9d8c-92f747c0c46f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7eb3d734-ca38-48f9-b5e5-6aa2bfcf42ec",
            "compositeImage": {
                "id": "6f439756-7454-4b74-80b3-cea7660364a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df4b840c-eba6-4ed6-9d8c-92f747c0c46f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "369c1f0b-4178-48ee-9256-b7dfb3285203",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df4b840c-eba6-4ed6-9d8c-92f747c0c46f",
                    "LayerId": "953086eb-3d2d-47fb-bc8b-f4dbf20815b7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "953086eb-3d2d-47fb-bc8b-f4dbf20815b7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7eb3d734-ca38-48f9-b5e5-6aa2bfcf42ec",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}