{
    "id": "7404bec4-5510-43fd-a301-d1f341776e17",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sEqFrame",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 0,
    "bbox_right": 39,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8c3373d7-b1c2-429f-b0a7-4dc164937dc3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7404bec4-5510-43fd-a301-d1f341776e17",
            "compositeImage": {
                "id": "f1a61a16-edc6-4c60-adf0-ad3d2ea0b72b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c3373d7-b1c2-429f-b0a7-4dc164937dc3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6ed1a5a-59a7-41f1-ac68-90ffb6d317a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c3373d7-b1c2-429f-b0a7-4dc164937dc3",
                    "LayerId": "71835eb7-43e1-4bc3-97df-82421459f673"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 40,
    "layers": [
        {
            "id": "71835eb7-43e1-4bc3-97df-82421459f673",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7404bec4-5510-43fd-a301-d1f341776e17",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 0,
    "yorig": 0
}