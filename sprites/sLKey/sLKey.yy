{
    "id": "e0d63e9e-3880-421c-897b-b0b1799d4bc3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sLKey",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 25,
    "bbox_left": 5,
    "bbox_right": 26,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "22f2cf52-59b4-49d4-a330-c35bf749c5c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e0d63e9e-3880-421c-897b-b0b1799d4bc3",
            "compositeImage": {
                "id": "02d95577-c268-482e-a81c-7d513227b468",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22f2cf52-59b4-49d4-a330-c35bf749c5c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e037a77-f548-42ba-8975-761a94f9fde6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22f2cf52-59b4-49d4-a330-c35bf749c5c8",
                    "LayerId": "cb4e529b-5af9-4f6b-860c-a4abcfbdecec"
                }
            ]
        },
        {
            "id": "fe881461-e59d-48a7-826e-8a24ea6dadad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e0d63e9e-3880-421c-897b-b0b1799d4bc3",
            "compositeImage": {
                "id": "50990497-7000-416a-8daa-1f2baed40177",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe881461-e59d-48a7-826e-8a24ea6dadad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "771f1a40-2f7f-4ae9-aa2f-9902bc47943c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe881461-e59d-48a7-826e-8a24ea6dadad",
                    "LayerId": "cb4e529b-5af9-4f6b-860c-a4abcfbdecec"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "cb4e529b-5af9-4f6b-860c-a4abcfbdecec",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e0d63e9e-3880-421c-897b-b0b1799d4bc3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 15,
    "yorig": 26
}