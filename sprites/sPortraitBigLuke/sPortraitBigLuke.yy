{
    "id": "e9bc0f36-5aa9-4ff5-b253-96c9ab9a80ac",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPortraitBigLuke",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 95,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "864b1ddf-78a0-4846-8140-0dc3e2734d3d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9bc0f36-5aa9-4ff5-b253-96c9ab9a80ac",
            "compositeImage": {
                "id": "159b0a27-3816-4402-a42f-6e09a99b9858",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "864b1ddf-78a0-4846-8140-0dc3e2734d3d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc04fe8a-bdbc-46ed-9359-86e266c4995b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "864b1ddf-78a0-4846-8140-0dc3e2734d3d",
                    "LayerId": "b3433a97-7089-4755-85a9-6ff877919c6f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "b3433a97-7089-4755-85a9-6ff877919c6f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e9bc0f36-5aa9-4ff5-b253-96c9ab9a80ac",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 0,
    "yorig": 0
}