{
    "id": "321b7cd2-ae71-421e-8e1a-842f2eef5f45",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sEmpty",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 9,
    "bbox_right": 22,
    "bbox_top": 9,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0ace4da2-5c58-4711-8f02-8d7703a3008b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "321b7cd2-ae71-421e-8e1a-842f2eef5f45",
            "compositeImage": {
                "id": "6f16b730-cbcb-412e-8d78-938860f43c71",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ace4da2-5c58-4711-8f02-8d7703a3008b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef6ace6f-489b-451e-98df-6387f89cd261",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ace4da2-5c58-4711-8f02-8d7703a3008b",
                    "LayerId": "3f455d14-4521-4af7-8409-a00f251ce2b8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "3f455d14-4521-4af7-8409-a00f251ce2b8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "321b7cd2-ae71-421e-8e1a-842f2eef5f45",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}