{
    "id": "041b511c-1201-4695-b68e-1bf3e8960ba9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCauldron",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 222,
    "bbox_left": 9,
    "bbox_right": 119,
    "bbox_top": 34,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "102d1f43-ea5e-4aad-8b6d-8dc45d4d81af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "041b511c-1201-4695-b68e-1bf3e8960ba9",
            "compositeImage": {
                "id": "f24d943f-308d-436d-92f0-5b2c57aefba0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "102d1f43-ea5e-4aad-8b6d-8dc45d4d81af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "350e420f-aa10-4d5b-a1c0-16303ab69f91",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "102d1f43-ea5e-4aad-8b6d-8dc45d4d81af",
                    "LayerId": "01eba156-d642-4d3f-aafa-55c2ea905096"
                }
            ]
        },
        {
            "id": "c6c3e8dd-3cc2-4164-b488-52737176534d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "041b511c-1201-4695-b68e-1bf3e8960ba9",
            "compositeImage": {
                "id": "3a2ac587-3682-46f5-a79d-c223365c7c12",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6c3e8dd-3cc2-4164-b488-52737176534d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1278b3d6-ca77-4568-817b-7e9504b3719f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6c3e8dd-3cc2-4164-b488-52737176534d",
                    "LayerId": "01eba156-d642-4d3f-aafa-55c2ea905096"
                }
            ]
        },
        {
            "id": "38afba54-c075-48b6-ba96-abe68ba94495",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "041b511c-1201-4695-b68e-1bf3e8960ba9",
            "compositeImage": {
                "id": "0d685d84-d3f2-431c-a758-4cd145c85473",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38afba54-c075-48b6-ba96-abe68ba94495",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c8820de-d6ad-4207-94a1-9f424f0e5533",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38afba54-c075-48b6-ba96-abe68ba94495",
                    "LayerId": "01eba156-d642-4d3f-aafa-55c2ea905096"
                }
            ]
        },
        {
            "id": "69877cbc-3c3d-43da-a1fe-e9e144de11c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "041b511c-1201-4695-b68e-1bf3e8960ba9",
            "compositeImage": {
                "id": "cbbb6c26-a320-4a46-abfa-52a471039749",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "69877cbc-3c3d-43da-a1fe-e9e144de11c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13b20256-cfd5-4bc6-a99a-e375bc9f4ab0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69877cbc-3c3d-43da-a1fe-e9e144de11c7",
                    "LayerId": "01eba156-d642-4d3f-aafa-55c2ea905096"
                }
            ]
        },
        {
            "id": "2c8d143b-0c9f-4398-b805-3ecf7d142a89",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "041b511c-1201-4695-b68e-1bf3e8960ba9",
            "compositeImage": {
                "id": "119d0c16-b6d5-49a8-8477-595ad8b50b03",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c8d143b-0c9f-4398-b805-3ecf7d142a89",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e08644ac-2809-4a14-8022-5cc245a550b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c8d143b-0c9f-4398-b805-3ecf7d142a89",
                    "LayerId": "01eba156-d642-4d3f-aafa-55c2ea905096"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 223,
    "layers": [
        {
            "id": "01eba156-d642-4d3f-aafa-55c2ea905096",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "041b511c-1201-4695-b68e-1bf3e8960ba9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 130,
    "xorig": 234,
    "yorig": 155
}