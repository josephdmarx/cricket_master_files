{
    "id": "f69891e2-a677-4345-a3f6-3b8d406d1795",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCureFever",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 7,
    "bbox_right": 24,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "11feb94a-75fc-4ad6-b744-a4d5db2a71e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f69891e2-a677-4345-a3f6-3b8d406d1795",
            "compositeImage": {
                "id": "5630ef8a-d8d1-4611-aa87-af008088a61b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "11feb94a-75fc-4ad6-b744-a4d5db2a71e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0777c161-3be7-4c08-9a68-e67858558d83",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "11feb94a-75fc-4ad6-b744-a4d5db2a71e8",
                    "LayerId": "c274b43b-ecc7-401b-a730-24e68629d55a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "c274b43b-ecc7-401b-a730-24e68629d55a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f69891e2-a677-4345-a3f6-3b8d406d1795",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}