{
    "id": "0d2f114b-c879-4307-b73c-68f321a805b6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSky02",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 359,
    "bbox_left": 0,
    "bbox_right": 639,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c04042d5-b40d-4b56-bc3a-64947f4f2643",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d2f114b-c879-4307-b73c-68f321a805b6",
            "compositeImage": {
                "id": "3edb0da4-6b2c-4b7c-8995-c080028dba3f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c04042d5-b40d-4b56-bc3a-64947f4f2643",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff4b042a-cf20-4068-8604-7a5c3b18d28d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c04042d5-b40d-4b56-bc3a-64947f4f2643",
                    "LayerId": "13cd4e5a-a0b3-42d7-9556-48f850d6805a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 360,
    "layers": [
        {
            "id": "13cd4e5a-a0b3-42d7-9556-48f850d6805a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0d2f114b-c879-4307-b73c-68f321a805b6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 0,
    "yorig": 0
}