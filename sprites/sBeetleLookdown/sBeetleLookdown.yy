{
    "id": "128b43d0-faed-496a-b2e1-a64eb1c42ebb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBeetleLookdown",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 46,
    "bbox_right": 73,
    "bbox_top": 36,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "29efda25-8287-4db2-9c80-a9af0ee02d10",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "128b43d0-faed-496a-b2e1-a64eb1c42ebb",
            "compositeImage": {
                "id": "b6b196e5-078b-4bd0-a06d-e0e679704d19",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29efda25-8287-4db2-9c80-a9af0ee02d10",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f814dc1-be0d-421b-af95-d5e6116c4a06",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29efda25-8287-4db2-9c80-a9af0ee02d10",
                    "LayerId": "a4ced624-d855-4dfa-be30-7bde16c0b2e0"
                }
            ]
        },
        {
            "id": "a5b6fcc8-a3aa-45b1-af94-33d6cf6125ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "128b43d0-faed-496a-b2e1-a64eb1c42ebb",
            "compositeImage": {
                "id": "9cc8e6f5-c637-4d0e-9dc3-f452e6877954",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a5b6fcc8-a3aa-45b1-af94-33d6cf6125ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c8fb78a4-859b-4273-a02c-3faaa9fef975",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5b6fcc8-a3aa-45b1-af94-33d6cf6125ad",
                    "LayerId": "a4ced624-d855-4dfa-be30-7bde16c0b2e0"
                }
            ]
        },
        {
            "id": "2b2b4c10-0a19-45df-b149-7198c1dab5ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "128b43d0-faed-496a-b2e1-a64eb1c42ebb",
            "compositeImage": {
                "id": "c845b5bb-c0a2-45ac-b785-506c162b81c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b2b4c10-0a19-45df-b149-7198c1dab5ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89fbff60-191a-44ec-8fc9-cd610b29088b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b2b4c10-0a19-45df-b149-7198c1dab5ba",
                    "LayerId": "a4ced624-d855-4dfa-be30-7bde16c0b2e0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "a4ced624-d855-4dfa-be30-7bde16c0b2e0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "128b43d0-faed-496a-b2e1-a64eb1c42ebb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}