{
    "id": "0e3f61d0-f539-435c-b63e-39aca45c4812",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sInventory",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 77,
    "bbox_left": 0,
    "bbox_right": 149,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "98ff38ce-9216-431b-989b-e8485bd195d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0e3f61d0-f539-435c-b63e-39aca45c4812",
            "compositeImage": {
                "id": "5aa0bfcd-fd90-4336-b725-a04fba1dfa47",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "98ff38ce-9216-431b-989b-e8485bd195d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4199db1f-45bb-4259-b3b1-bf4441b1997f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98ff38ce-9216-431b-989b-e8485bd195d4",
                    "LayerId": "3458e650-9652-4ca4-a7c7-b1adc56a932a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 78,
    "layers": [
        {
            "id": "3458e650-9652-4ca4-a7c7-b1adc56a932a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0e3f61d0-f539-435c-b63e-39aca45c4812",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 150,
    "xorig": 0,
    "yorig": 0
}