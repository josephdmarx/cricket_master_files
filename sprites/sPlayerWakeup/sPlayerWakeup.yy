{
    "id": "3e527417-7934-466a-8177-e75d01569d0b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayerWakeup",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 94,
    "bbox_left": 44,
    "bbox_right": 58,
    "bbox_top": 42,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "34bba7a4-03b2-4392-838d-7598c5bc7d36",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e527417-7934-466a-8177-e75d01569d0b",
            "compositeImage": {
                "id": "f09be383-e420-42fb-adcd-e93246d9d04e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34bba7a4-03b2-4392-838d-7598c5bc7d36",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5aa9b5e2-8c40-4c35-8934-0b652eb38b94",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34bba7a4-03b2-4392-838d-7598c5bc7d36",
                    "LayerId": "dd3915af-4e89-4cb4-b360-7a2b2a2585c5"
                }
            ]
        },
        {
            "id": "8f62feb5-890a-47e6-a3ec-ec204564d6d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e527417-7934-466a-8177-e75d01569d0b",
            "compositeImage": {
                "id": "c2debb3d-814e-42be-b2f4-55d61ff1bf63",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f62feb5-890a-47e6-a3ec-ec204564d6d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d530d29-bd0f-4a0a-b992-99ef15770e34",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f62feb5-890a-47e6-a3ec-ec204564d6d6",
                    "LayerId": "dd3915af-4e89-4cb4-b360-7a2b2a2585c5"
                }
            ]
        },
        {
            "id": "cafac6db-9e45-4d73-94c6-35a3d4cda547",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e527417-7934-466a-8177-e75d01569d0b",
            "compositeImage": {
                "id": "19b72666-2809-4b24-bce9-3a0f5a74927f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cafac6db-9e45-4d73-94c6-35a3d4cda547",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca4ded14-1240-4fc4-aae9-f5493a0a8de8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cafac6db-9e45-4d73-94c6-35a3d4cda547",
                    "LayerId": "dd3915af-4e89-4cb4-b360-7a2b2a2585c5"
                }
            ]
        },
        {
            "id": "7eae62cc-7f48-4df3-8917-0b7c4ad7d0ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e527417-7934-466a-8177-e75d01569d0b",
            "compositeImage": {
                "id": "9562b7c7-e9b7-4bcf-872b-2c25fbf44079",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7eae62cc-7f48-4df3-8917-0b7c4ad7d0ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf17070c-d161-45b1-93be-cbd218014a67",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7eae62cc-7f48-4df3-8917-0b7c4ad7d0ae",
                    "LayerId": "dd3915af-4e89-4cb4-b360-7a2b2a2585c5"
                }
            ]
        },
        {
            "id": "ff8c37e5-6aa0-4c7a-ba3f-d423ad149ddd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e527417-7934-466a-8177-e75d01569d0b",
            "compositeImage": {
                "id": "548a6ab3-75ee-4783-9900-1fcca27abb62",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff8c37e5-6aa0-4c7a-ba3f-d423ad149ddd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e2ddb8f-3559-433b-882b-9d629e4a546f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff8c37e5-6aa0-4c7a-ba3f-d423ad149ddd",
                    "LayerId": "dd3915af-4e89-4cb4-b360-7a2b2a2585c5"
                }
            ]
        },
        {
            "id": "17f6b21e-dba0-4c5f-bcbc-13589c7f62d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e527417-7934-466a-8177-e75d01569d0b",
            "compositeImage": {
                "id": "b0a57929-7133-46bc-86c2-7bd2b1ea134b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "17f6b21e-dba0-4c5f-bcbc-13589c7f62d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44b66a06-f8c1-431b-b7a9-f4a2a689885e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "17f6b21e-dba0-4c5f-bcbc-13589c7f62d7",
                    "LayerId": "dd3915af-4e89-4cb4-b360-7a2b2a2585c5"
                }
            ]
        },
        {
            "id": "ace3c726-6cc7-4640-9ce6-cb0907db86da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e527417-7934-466a-8177-e75d01569d0b",
            "compositeImage": {
                "id": "17563d25-40c6-4826-8136-325a5b5d2d36",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ace3c726-6cc7-4640-9ce6-cb0907db86da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd8834b1-1fcb-4ba3-838f-bc6cd0207174",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ace3c726-6cc7-4640-9ce6-cb0907db86da",
                    "LayerId": "dd3915af-4e89-4cb4-b360-7a2b2a2585c5"
                }
            ]
        },
        {
            "id": "e2d31278-1383-4928-8ba7-6c511bd7c2c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e527417-7934-466a-8177-e75d01569d0b",
            "compositeImage": {
                "id": "b79a04da-e01b-4470-a2dc-30bbba2cb642",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2d31278-1383-4928-8ba7-6c511bd7c2c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d27b1fd-ce76-4093-865b-ce215edbde9f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2d31278-1383-4928-8ba7-6c511bd7c2c5",
                    "LayerId": "dd3915af-4e89-4cb4-b360-7a2b2a2585c5"
                }
            ]
        },
        {
            "id": "4c65ac60-a388-4c86-bb2e-3f23d09e1baf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e527417-7934-466a-8177-e75d01569d0b",
            "compositeImage": {
                "id": "3a9455ff-4eed-4687-8a11-02aef1953718",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c65ac60-a388-4c86-bb2e-3f23d09e1baf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95e169aa-17f1-417d-acf2-9c095659276b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c65ac60-a388-4c86-bb2e-3f23d09e1baf",
                    "LayerId": "dd3915af-4e89-4cb4-b360-7a2b2a2585c5"
                }
            ]
        },
        {
            "id": "86e59418-64f1-4375-bdb1-6ce5d34d93be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e527417-7934-466a-8177-e75d01569d0b",
            "compositeImage": {
                "id": "e1ba652f-2a4a-4417-9059-0a86072810b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "86e59418-64f1-4375-bdb1-6ce5d34d93be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a800afc-bc76-4126-bf9f-2fed23fd3010",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86e59418-64f1-4375-bdb1-6ce5d34d93be",
                    "LayerId": "dd3915af-4e89-4cb4-b360-7a2b2a2585c5"
                }
            ]
        },
        {
            "id": "2e8f2948-239a-48f5-9a96-1444ea1a08ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e527417-7934-466a-8177-e75d01569d0b",
            "compositeImage": {
                "id": "cbfd1a50-0b72-4ed0-b699-d5dfcbb941ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e8f2948-239a-48f5-9a96-1444ea1a08ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b8344a8-1061-49bd-8e16-b1759c5d0654",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e8f2948-239a-48f5-9a96-1444ea1a08ad",
                    "LayerId": "dd3915af-4e89-4cb4-b360-7a2b2a2585c5"
                }
            ]
        },
        {
            "id": "49ab4dad-d816-449a-875a-95daf082b176",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e527417-7934-466a-8177-e75d01569d0b",
            "compositeImage": {
                "id": "a691ba3e-3cd0-40f0-86bd-275d23f43aa5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "49ab4dad-d816-449a-875a-95daf082b176",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65d0b61b-da10-4c4d-8fe9-9614e8131388",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49ab4dad-d816-449a-875a-95daf082b176",
                    "LayerId": "dd3915af-4e89-4cb4-b360-7a2b2a2585c5"
                }
            ]
        },
        {
            "id": "a9403c6f-6b82-42a6-a57b-e82f48b532b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e527417-7934-466a-8177-e75d01569d0b",
            "compositeImage": {
                "id": "6b60d12e-e8a1-48b3-a67a-da70a7c7e34c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a9403c6f-6b82-42a6-a57b-e82f48b532b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "63d2ec34-6f27-41d6-a0c0-5c8d2c2f1394",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a9403c6f-6b82-42a6-a57b-e82f48b532b4",
                    "LayerId": "dd3915af-4e89-4cb4-b360-7a2b2a2585c5"
                }
            ]
        },
        {
            "id": "f85171a2-aaf4-4e64-bf6a-34b41330be6c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e527417-7934-466a-8177-e75d01569d0b",
            "compositeImage": {
                "id": "582c9af9-51ee-48e9-b60c-53770579165e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f85171a2-aaf4-4e64-bf6a-34b41330be6c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44748272-482b-4d1a-b5da-0b17bae3b409",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f85171a2-aaf4-4e64-bf6a-34b41330be6c",
                    "LayerId": "dd3915af-4e89-4cb4-b360-7a2b2a2585c5"
                }
            ]
        },
        {
            "id": "043ad62e-804a-4f35-9424-e7b1299a5a99",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e527417-7934-466a-8177-e75d01569d0b",
            "compositeImage": {
                "id": "4f65e488-cac1-415c-a089-04839a681d6d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "043ad62e-804a-4f35-9424-e7b1299a5a99",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c9e041e-73d5-4c1f-88b1-0de8fae8d9ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "043ad62e-804a-4f35-9424-e7b1299a5a99",
                    "LayerId": "dd3915af-4e89-4cb4-b360-7a2b2a2585c5"
                }
            ]
        },
        {
            "id": "7a0968b1-2f4d-4148-835e-f745c92565d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e527417-7934-466a-8177-e75d01569d0b",
            "compositeImage": {
                "id": "db62df40-a56f-44f7-a624-38a0d8ee5209",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a0968b1-2f4d-4148-835e-f745c92565d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1707a83-f8e4-4f93-a26c-e6fcc0829b84",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a0968b1-2f4d-4148-835e-f745c92565d2",
                    "LayerId": "dd3915af-4e89-4cb4-b360-7a2b2a2585c5"
                }
            ]
        },
        {
            "id": "26c6b4c7-d56b-4f5d-82d3-14f676bd0bca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e527417-7934-466a-8177-e75d01569d0b",
            "compositeImage": {
                "id": "8852048d-2986-4a4c-bf45-e3866bababde",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26c6b4c7-d56b-4f5d-82d3-14f676bd0bca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d07a3d8-bb29-4f21-8676-2ce411770ccf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26c6b4c7-d56b-4f5d-82d3-14f676bd0bca",
                    "LayerId": "dd3915af-4e89-4cb4-b360-7a2b2a2585c5"
                }
            ]
        },
        {
            "id": "1c1c27d3-4d1f-4e91-add2-63006911a841",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e527417-7934-466a-8177-e75d01569d0b",
            "compositeImage": {
                "id": "5db774ee-b883-47c7-994e-aebe2e065c0e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c1c27d3-4d1f-4e91-add2-63006911a841",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed3e2b32-5796-4fde-8e20-9472c39f36db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c1c27d3-4d1f-4e91-add2-63006911a841",
                    "LayerId": "dd3915af-4e89-4cb4-b360-7a2b2a2585c5"
                }
            ]
        },
        {
            "id": "7f31e987-1cae-482a-8dab-803e46830b80",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e527417-7934-466a-8177-e75d01569d0b",
            "compositeImage": {
                "id": "970cffe5-663f-4804-bc81-b39b2e166f69",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f31e987-1cae-482a-8dab-803e46830b80",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce642777-da80-4792-adb3-47f3cfacc97a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f31e987-1cae-482a-8dab-803e46830b80",
                    "LayerId": "dd3915af-4e89-4cb4-b360-7a2b2a2585c5"
                }
            ]
        },
        {
            "id": "a856e96c-ca89-4d3e-ac30-10eb61293ee7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e527417-7934-466a-8177-e75d01569d0b",
            "compositeImage": {
                "id": "616a882c-bc50-46df-9409-21fe694c34c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a856e96c-ca89-4d3e-ac30-10eb61293ee7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4dc55a02-456b-45bf-a527-c103de2d451d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a856e96c-ca89-4d3e-ac30-10eb61293ee7",
                    "LayerId": "dd3915af-4e89-4cb4-b360-7a2b2a2585c5"
                }
            ]
        },
        {
            "id": "af37e7c2-e390-408f-b67f-9957092862f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e527417-7934-466a-8177-e75d01569d0b",
            "compositeImage": {
                "id": "26f08afd-5ef2-40a7-b309-602356e04403",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af37e7c2-e390-408f-b67f-9957092862f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bfbfc001-5636-4a91-a53d-dc238bdf8a8c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af37e7c2-e390-408f-b67f-9957092862f9",
                    "LayerId": "dd3915af-4e89-4cb4-b360-7a2b2a2585c5"
                }
            ]
        },
        {
            "id": "09ead3e0-4c9b-499c-aac1-a24fed2a423b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e527417-7934-466a-8177-e75d01569d0b",
            "compositeImage": {
                "id": "a0120983-c71e-4ed7-91c2-0cb5ee897dcd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09ead3e0-4c9b-499c-aac1-a24fed2a423b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "240b119f-a752-45dc-bb25-fad96a75c163",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09ead3e0-4c9b-499c-aac1-a24fed2a423b",
                    "LayerId": "dd3915af-4e89-4cb4-b360-7a2b2a2585c5"
                }
            ]
        },
        {
            "id": "d3631377-972b-42ab-832f-0c0ce75bdb51",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e527417-7934-466a-8177-e75d01569d0b",
            "compositeImage": {
                "id": "27cb9355-8652-418b-b60d-24b912f1344b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d3631377-972b-42ab-832f-0c0ce75bdb51",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "28255897-f82e-4d91-a73a-992297378d8f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3631377-972b-42ab-832f-0c0ce75bdb51",
                    "LayerId": "dd3915af-4e89-4cb4-b360-7a2b2a2585c5"
                }
            ]
        },
        {
            "id": "f2a19541-28e1-4fbb-9197-58ff6b529c38",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e527417-7934-466a-8177-e75d01569d0b",
            "compositeImage": {
                "id": "1f24573d-2db4-46ea-a23a-0f82d3dada33",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f2a19541-28e1-4fbb-9197-58ff6b529c38",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32eb4cfc-1470-4973-aef3-8c91a98f73fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f2a19541-28e1-4fbb-9197-58ff6b529c38",
                    "LayerId": "dd3915af-4e89-4cb4-b360-7a2b2a2585c5"
                }
            ]
        },
        {
            "id": "5d1a63a8-b5cc-47d3-9ed2-54e20e002fe5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e527417-7934-466a-8177-e75d01569d0b",
            "compositeImage": {
                "id": "b9f5aaa5-706f-4ab7-9462-093ac2656dab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d1a63a8-b5cc-47d3-9ed2-54e20e002fe5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ca7b736-2701-4d31-8076-b6eb7c095e94",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d1a63a8-b5cc-47d3-9ed2-54e20e002fe5",
                    "LayerId": "dd3915af-4e89-4cb4-b360-7a2b2a2585c5"
                }
            ]
        },
        {
            "id": "6103eeb5-644f-46ca-949d-6ae195d62110",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e527417-7934-466a-8177-e75d01569d0b",
            "compositeImage": {
                "id": "ea25fa31-9315-4a43-b216-96e09f4c50cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6103eeb5-644f-46ca-949d-6ae195d62110",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e8ba1f8-8459-42c0-94c4-bba4af1916b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6103eeb5-644f-46ca-949d-6ae195d62110",
                    "LayerId": "dd3915af-4e89-4cb4-b360-7a2b2a2585c5"
                }
            ]
        },
        {
            "id": "a42d0632-0355-4fed-be1f-fd2c2c5d7366",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e527417-7934-466a-8177-e75d01569d0b",
            "compositeImage": {
                "id": "79516182-1a54-4b79-a394-543651bbc87e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a42d0632-0355-4fed-be1f-fd2c2c5d7366",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "edcec715-dbbc-4649-bc5a-645f702569ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a42d0632-0355-4fed-be1f-fd2c2c5d7366",
                    "LayerId": "dd3915af-4e89-4cb4-b360-7a2b2a2585c5"
                }
            ]
        },
        {
            "id": "5f5d648f-852d-4910-9471-fab2ae11cfe4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e527417-7934-466a-8177-e75d01569d0b",
            "compositeImage": {
                "id": "a11c2453-a6f4-45ea-b200-c7ccf8f0b466",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f5d648f-852d-4910-9471-fab2ae11cfe4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2bb66114-3911-44c1-91f7-00e1ac86dd7b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f5d648f-852d-4910-9471-fab2ae11cfe4",
                    "LayerId": "dd3915af-4e89-4cb4-b360-7a2b2a2585c5"
                }
            ]
        },
        {
            "id": "8382fd6c-b8b3-48f1-ab9a-fa85de96ac3e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e527417-7934-466a-8177-e75d01569d0b",
            "compositeImage": {
                "id": "d4906855-d858-45c8-ac38-b2cbfd6bf6cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8382fd6c-b8b3-48f1-ab9a-fa85de96ac3e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd0e200c-4a9b-4347-a728-8c0e1e88eb4a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8382fd6c-b8b3-48f1-ab9a-fa85de96ac3e",
                    "LayerId": "dd3915af-4e89-4cb4-b360-7a2b2a2585c5"
                }
            ]
        },
        {
            "id": "221fe974-0cdc-469f-9350-56ceaa89d8e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e527417-7934-466a-8177-e75d01569d0b",
            "compositeImage": {
                "id": "ae5af247-855e-4c0c-8b7e-71f290ab33fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "221fe974-0cdc-469f-9350-56ceaa89d8e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a84fd2f7-566b-4a46-a7f4-a2f8c97e814f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "221fe974-0cdc-469f-9350-56ceaa89d8e9",
                    "LayerId": "dd3915af-4e89-4cb4-b360-7a2b2a2585c5"
                }
            ]
        },
        {
            "id": "e073a90e-b6d7-4003-8af4-a9d43a32f69a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e527417-7934-466a-8177-e75d01569d0b",
            "compositeImage": {
                "id": "bc39457d-5f04-402b-8247-0022df414c04",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e073a90e-b6d7-4003-8af4-a9d43a32f69a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "17a4b096-e196-42de-a986-b7dd29e976bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e073a90e-b6d7-4003-8af4-a9d43a32f69a",
                    "LayerId": "dd3915af-4e89-4cb4-b360-7a2b2a2585c5"
                }
            ]
        },
        {
            "id": "f6121e5d-5e00-479e-8c8e-0c43c3f8b34a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e527417-7934-466a-8177-e75d01569d0b",
            "compositeImage": {
                "id": "067c1203-e432-4a6b-b5be-027584aee516",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6121e5d-5e00-479e-8c8e-0c43c3f8b34a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "75a67158-9654-466c-972e-6e1e12514fd8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6121e5d-5e00-479e-8c8e-0c43c3f8b34a",
                    "LayerId": "dd3915af-4e89-4cb4-b360-7a2b2a2585c5"
                }
            ]
        },
        {
            "id": "eb46120c-270f-4780-a9af-2d54b4b829c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e527417-7934-466a-8177-e75d01569d0b",
            "compositeImage": {
                "id": "33f9c4fc-7878-429e-be4d-783005e5f122",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb46120c-270f-4780-a9af-2d54b4b829c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95286fed-d379-415f-bf68-7ced66b9e399",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb46120c-270f-4780-a9af-2d54b4b829c4",
                    "LayerId": "dd3915af-4e89-4cb4-b360-7a2b2a2585c5"
                }
            ]
        },
        {
            "id": "b55cd992-4bc4-4680-b654-74e59296db72",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e527417-7934-466a-8177-e75d01569d0b",
            "compositeImage": {
                "id": "af87c1cd-04fe-463d-b62c-3dd0f4d31c58",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b55cd992-4bc4-4680-b654-74e59296db72",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6521fdcc-edd2-4eae-8de6-4c16e7e405ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b55cd992-4bc4-4680-b654-74e59296db72",
                    "LayerId": "dd3915af-4e89-4cb4-b360-7a2b2a2585c5"
                }
            ]
        },
        {
            "id": "eca92d26-e5d0-40ea-b542-a124f2a4001c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e527417-7934-466a-8177-e75d01569d0b",
            "compositeImage": {
                "id": "a6742405-2702-4ec0-8681-6b8205574ea4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eca92d26-e5d0-40ea-b542-a124f2a4001c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c02fd43-bf60-44bd-9c2d-d83e7dc360f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eca92d26-e5d0-40ea-b542-a124f2a4001c",
                    "LayerId": "dd3915af-4e89-4cb4-b360-7a2b2a2585c5"
                }
            ]
        },
        {
            "id": "3f1cf276-f3e6-4d09-bad7-e4fead3d5ee4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e527417-7934-466a-8177-e75d01569d0b",
            "compositeImage": {
                "id": "97e42763-4b1a-4453-ad82-be248c44d73a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f1cf276-f3e6-4d09-bad7-e4fead3d5ee4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53f8da32-1a35-4ac4-accb-199d0fb3ae1f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f1cf276-f3e6-4d09-bad7-e4fead3d5ee4",
                    "LayerId": "dd3915af-4e89-4cb4-b360-7a2b2a2585c5"
                }
            ]
        },
        {
            "id": "302fd52b-28ea-43f1-83a6-2f2f16835dc1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e527417-7934-466a-8177-e75d01569d0b",
            "compositeImage": {
                "id": "c35112a1-4fb6-475e-a89a-4629f8db2a11",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "302fd52b-28ea-43f1-83a6-2f2f16835dc1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ca82523-0085-4f49-a242-8f3b4582ec75",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "302fd52b-28ea-43f1-83a6-2f2f16835dc1",
                    "LayerId": "dd3915af-4e89-4cb4-b360-7a2b2a2585c5"
                }
            ]
        },
        {
            "id": "f53bf149-a907-464b-9738-9d36699929ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e527417-7934-466a-8177-e75d01569d0b",
            "compositeImage": {
                "id": "9419eaa0-6b66-4949-84ff-4c07e4326be2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f53bf149-a907-464b-9738-9d36699929ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00a3805b-a1c3-4f8c-93c4-3d725f54831d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f53bf149-a907-464b-9738-9d36699929ec",
                    "LayerId": "dd3915af-4e89-4cb4-b360-7a2b2a2585c5"
                }
            ]
        },
        {
            "id": "f21c1349-5c22-4096-aa8b-de346d1173e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e527417-7934-466a-8177-e75d01569d0b",
            "compositeImage": {
                "id": "8ae5a1bd-1309-4c1f-b233-c425d56076a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f21c1349-5c22-4096-aa8b-de346d1173e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "07b042f2-d068-4c90-9ccc-f45790264ed0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f21c1349-5c22-4096-aa8b-de346d1173e4",
                    "LayerId": "dd3915af-4e89-4cb4-b360-7a2b2a2585c5"
                }
            ]
        },
        {
            "id": "bde0b91b-3b8f-425c-b1a3-9d8a843d54d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e527417-7934-466a-8177-e75d01569d0b",
            "compositeImage": {
                "id": "e74d488d-6567-43ea-8c46-f4e14eae2a26",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bde0b91b-3b8f-425c-b1a3-9d8a843d54d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "93edcdd0-7860-4c33-89f2-42469b0945d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bde0b91b-3b8f-425c-b1a3-9d8a843d54d9",
                    "LayerId": "dd3915af-4e89-4cb4-b360-7a2b2a2585c5"
                }
            ]
        },
        {
            "id": "8cce2d8f-f376-4a43-8b8d-d23a65411f29",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e527417-7934-466a-8177-e75d01569d0b",
            "compositeImage": {
                "id": "6d0b00f4-f95a-4fed-951b-c7e68af2c81e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8cce2d8f-f376-4a43-8b8d-d23a65411f29",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c777f39-d434-47d4-a564-d8c88b063ba4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8cce2d8f-f376-4a43-8b8d-d23a65411f29",
                    "LayerId": "dd3915af-4e89-4cb4-b360-7a2b2a2585c5"
                }
            ]
        },
        {
            "id": "32add1b6-178f-4618-9f4c-e5a43f37d33f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e527417-7934-466a-8177-e75d01569d0b",
            "compositeImage": {
                "id": "addd367e-338f-4e69-b82d-10ede7bbff8d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "32add1b6-178f-4618-9f4c-e5a43f37d33f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cfa31ba3-5526-42fc-87ed-d18cefab5371",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32add1b6-178f-4618-9f4c-e5a43f37d33f",
                    "LayerId": "dd3915af-4e89-4cb4-b360-7a2b2a2585c5"
                }
            ]
        },
        {
            "id": "552d9778-5d6d-4c80-96a9-da5357b99fc6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e527417-7934-466a-8177-e75d01569d0b",
            "compositeImage": {
                "id": "f668f088-67b2-47d0-b2d1-fbf4cf7ef51c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "552d9778-5d6d-4c80-96a9-da5357b99fc6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af999b3a-0697-4733-8866-2f05893acef4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "552d9778-5d6d-4c80-96a9-da5357b99fc6",
                    "LayerId": "dd3915af-4e89-4cb4-b360-7a2b2a2585c5"
                }
            ]
        },
        {
            "id": "edea99dc-92d3-4709-ab03-126f2526d58f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e527417-7934-466a-8177-e75d01569d0b",
            "compositeImage": {
                "id": "e0a5dae2-4a93-4440-8ccc-072814c74685",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "edea99dc-92d3-4709-ab03-126f2526d58f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "52fbeb85-cc9b-4b13-8ed7-e9a6b59f7b09",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "edea99dc-92d3-4709-ab03-126f2526d58f",
                    "LayerId": "dd3915af-4e89-4cb4-b360-7a2b2a2585c5"
                }
            ]
        },
        {
            "id": "fb43b82b-f08c-46f8-ad89-5e1a9b575e45",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e527417-7934-466a-8177-e75d01569d0b",
            "compositeImage": {
                "id": "61bd2702-cb38-4802-b054-0a0f4519905b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb43b82b-f08c-46f8-ad89-5e1a9b575e45",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d9a7484-d343-45b3-8d44-5a3d624f0afc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb43b82b-f08c-46f8-ad89-5e1a9b575e45",
                    "LayerId": "dd3915af-4e89-4cb4-b360-7a2b2a2585c5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "dd3915af-4e89-4cb4-b360-7a2b2a2585c5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3e527417-7934-466a-8177-e75d01569d0b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}