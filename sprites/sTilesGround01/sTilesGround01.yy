{
    "id": "68b78aa9-4b15-4c27-96bf-f7e280cfdad1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTilesGround01",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 239,
    "bbox_left": 16,
    "bbox_right": 494,
    "bbox_top": 16,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "65469777-e9f8-41e4-8fa5-6e1640c6dde8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "68b78aa9-4b15-4c27-96bf-f7e280cfdad1",
            "compositeImage": {
                "id": "ec033d48-2456-48a5-a339-c7e95230ace5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65469777-e9f8-41e4-8fa5-6e1640c6dde8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c985f3ff-bb88-4550-be52-ce336a93390a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65469777-e9f8-41e4-8fa5-6e1640c6dde8",
                    "LayerId": "d7b56444-00e4-4cfb-93e7-0356c903cf4d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "d7b56444-00e4-4cfb-93e7-0356c903cf4d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "68b78aa9-4b15-4c27-96bf-f7e280cfdad1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 512,
    "xorig": -46,
    "yorig": 94
}