{
    "id": "f5bffe36-1287-4aa3-89a1-140ddf338ea2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sMayorIdle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 94,
    "bbox_left": 43,
    "bbox_right": 77,
    "bbox_top": 28,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "548d081b-1cb6-498b-b9a5-47f45ee27e3e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f5bffe36-1287-4aa3-89a1-140ddf338ea2",
            "compositeImage": {
                "id": "645c2602-932d-49a4-ae0c-d1678b314f31",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "548d081b-1cb6-498b-b9a5-47f45ee27e3e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d2de78bd-d395-4147-8334-ba6178f09ccd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "548d081b-1cb6-498b-b9a5-47f45ee27e3e",
                    "LayerId": "eff5c3a3-db63-478f-808d-f2d2961ef08f"
                }
            ]
        },
        {
            "id": "932c4b4e-7608-442b-a82f-ee534659fa93",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f5bffe36-1287-4aa3-89a1-140ddf338ea2",
            "compositeImage": {
                "id": "d375c9da-5953-44e4-9ece-d8308f0e40cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "932c4b4e-7608-442b-a82f-ee534659fa93",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88c27090-f307-4438-b3e0-50c567c092fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "932c4b4e-7608-442b-a82f-ee534659fa93",
                    "LayerId": "eff5c3a3-db63-478f-808d-f2d2961ef08f"
                }
            ]
        },
        {
            "id": "1f6ec823-1493-48be-aa9c-84d832ecd67f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f5bffe36-1287-4aa3-89a1-140ddf338ea2",
            "compositeImage": {
                "id": "4091e750-b7ce-4f21-9b32-9f8d442741aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f6ec823-1493-48be-aa9c-84d832ecd67f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b3b7ff3-f9d0-4b09-ba6c-0ac9006ef4a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f6ec823-1493-48be-aa9c-84d832ecd67f",
                    "LayerId": "eff5c3a3-db63-478f-808d-f2d2961ef08f"
                }
            ]
        },
        {
            "id": "2c036ee0-737a-46cb-bb28-b2914d480963",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f5bffe36-1287-4aa3-89a1-140ddf338ea2",
            "compositeImage": {
                "id": "c6a2e58d-e4fd-4dfb-8309-22ec6113a779",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c036ee0-737a-46cb-bb28-b2914d480963",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15ae5e48-5596-4b2e-8307-684bfd64cf2c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c036ee0-737a-46cb-bb28-b2914d480963",
                    "LayerId": "eff5c3a3-db63-478f-808d-f2d2961ef08f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "eff5c3a3-db63-478f-808d-f2d2961ef08f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f5bffe36-1287-4aa3-89a1-140ddf338ea2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}