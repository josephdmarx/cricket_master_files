{
    "id": "e6daee4d-09c9-4d2a-afc0-7882d3a9a647",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSky01",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 359,
    "bbox_left": 0,
    "bbox_right": 639,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4ec97220-0ccb-4b72-992c-9b0c601aa5ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6daee4d-09c9-4d2a-afc0-7882d3a9a647",
            "compositeImage": {
                "id": "c42569bd-77f8-4ccf-adee-878d9eb9a701",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ec97220-0ccb-4b72-992c-9b0c601aa5ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac615a39-408f-4aa5-b3c2-799e15e52650",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ec97220-0ccb-4b72-992c-9b0c601aa5ab",
                    "LayerId": "a9f3c5ba-34e1-462d-94b6-bb8c24bb8b78"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 360,
    "layers": [
        {
            "id": "a9f3c5ba-34e1-462d-94b6-bb8c24bb8b78",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e6daee4d-09c9-4d2a-afc0-7882d3a9a647",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 0,
    "yorig": 0
}