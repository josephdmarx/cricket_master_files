{
    "id": "43df5b12-fe3d-4cba-b902-a8494e4836fb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSplashGreen",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 6,
    "bbox_left": 1,
    "bbox_right": 6,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8125ae78-7c6c-4582-b507-99936f627d22",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "43df5b12-fe3d-4cba-b902-a8494e4836fb",
            "compositeImage": {
                "id": "4877dd75-78b6-46a4-84c3-9316637c31dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8125ae78-7c6c-4582-b507-99936f627d22",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c033cc3-a3c3-417c-bb8e-d732038e083d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8125ae78-7c6c-4582-b507-99936f627d22",
                    "LayerId": "3c6b421d-d95b-493b-a1b8-e07935c9b1d6"
                }
            ]
        },
        {
            "id": "7ddc5b05-6c98-4b9a-a419-69aca142563d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "43df5b12-fe3d-4cba-b902-a8494e4836fb",
            "compositeImage": {
                "id": "26c54487-8b9c-4a99-90b0-451756e3ab36",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ddc5b05-6c98-4b9a-a419-69aca142563d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "169f2488-a335-4e8f-b031-afecdbadd746",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ddc5b05-6c98-4b9a-a419-69aca142563d",
                    "LayerId": "3c6b421d-d95b-493b-a1b8-e07935c9b1d6"
                }
            ]
        },
        {
            "id": "4d066251-5f50-4cdb-8d85-d261e88eb017",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "43df5b12-fe3d-4cba-b902-a8494e4836fb",
            "compositeImage": {
                "id": "e5d3b8a8-b09a-4e9f-ad16-f9dc7e300b6e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d066251-5f50-4cdb-8d85-d261e88eb017",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "761e6ae6-413b-4bf8-86ac-bcb80cbafa1c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d066251-5f50-4cdb-8d85-d261e88eb017",
                    "LayerId": "3c6b421d-d95b-493b-a1b8-e07935c9b1d6"
                }
            ]
        },
        {
            "id": "54407482-0156-4068-bca5-2a476a5d6fd4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "43df5b12-fe3d-4cba-b902-a8494e4836fb",
            "compositeImage": {
                "id": "3c53a720-f12a-4fb9-a5c8-232817d7c149",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54407482-0156-4068-bca5-2a476a5d6fd4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9fe22721-2c5e-4e97-b0f7-8fe18b9b86f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54407482-0156-4068-bca5-2a476a5d6fd4",
                    "LayerId": "3c6b421d-d95b-493b-a1b8-e07935c9b1d6"
                }
            ]
        },
        {
            "id": "abb55f1d-c322-4367-9d78-7dec3e9b0451",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "43df5b12-fe3d-4cba-b902-a8494e4836fb",
            "compositeImage": {
                "id": "249e39aa-11ee-4d5c-bdc9-5f54012c3417",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "abb55f1d-c322-4367-9d78-7dec3e9b0451",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d804c172-9d31-448b-a1e9-04332c2eff03",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "abb55f1d-c322-4367-9d78-7dec3e9b0451",
                    "LayerId": "3c6b421d-d95b-493b-a1b8-e07935c9b1d6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "3c6b421d-d95b-493b-a1b8-e07935c9b1d6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "43df5b12-fe3d-4cba-b902-a8494e4836fb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}