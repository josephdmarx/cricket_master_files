{
    "id": "4ad1fd65-0450-4cac-96cb-266c47e087cf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sJKey",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 25,
    "bbox_left": 5,
    "bbox_right": 26,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "570d1d75-04b4-4819-a8ea-2ff9f03ca0c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4ad1fd65-0450-4cac-96cb-266c47e087cf",
            "compositeImage": {
                "id": "64af6589-0d3c-43af-a30c-4b05a83b6234",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "570d1d75-04b4-4819-a8ea-2ff9f03ca0c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc8e9f2a-0160-43a1-a1b9-bd016cb68d2d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "570d1d75-04b4-4819-a8ea-2ff9f03ca0c9",
                    "LayerId": "d1593f31-df89-4814-8092-25a1922f161a"
                }
            ]
        },
        {
            "id": "407cc8e3-ab0d-4722-9713-8f5aaf5057f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4ad1fd65-0450-4cac-96cb-266c47e087cf",
            "compositeImage": {
                "id": "c5957145-af68-44d1-bba8-a2b082052b62",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "407cc8e3-ab0d-4722-9713-8f5aaf5057f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1abab638-5a5c-4bba-be08-997f4f489cb8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "407cc8e3-ab0d-4722-9713-8f5aaf5057f7",
                    "LayerId": "d1593f31-df89-4814-8092-25a1922f161a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "d1593f31-df89-4814-8092-25a1922f161a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4ad1fd65-0450-4cac-96cb-266c47e087cf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 15,
    "yorig": 26
}