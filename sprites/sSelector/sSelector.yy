{
    "id": "4f534360-dc54-41d0-93d3-d7a8037734e8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSelector",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "393121fc-7589-49a1-a9ba-935c0fa81e98",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f534360-dc54-41d0-93d3-d7a8037734e8",
            "compositeImage": {
                "id": "8463037f-b4e1-4f13-875a-8cb900b2a2b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "393121fc-7589-49a1-a9ba-935c0fa81e98",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "38f7a846-da1e-4450-9a56-7dd671f8f1f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "393121fc-7589-49a1-a9ba-935c0fa81e98",
                    "LayerId": "36e03369-ea99-47a9-b44c-7b627caf3bbe"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "36e03369-ea99-47a9-b44c-7b627caf3bbe",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4f534360-dc54-41d0-93d3-d7a8037734e8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}