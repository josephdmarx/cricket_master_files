{
    "id": "be85dbe6-e527-484a-aa74-76b2c81ed1e2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sMapBogCaves",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 302,
    "bbox_left": 287,
    "bbox_right": 452,
    "bbox_top": 247,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9a9ded79-1120-47e4-9994-701e3cf6c54e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be85dbe6-e527-484a-aa74-76b2c81ed1e2",
            "compositeImage": {
                "id": "04adb252-d02b-46c5-9cda-3b88fce80cc9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a9ded79-1120-47e4-9994-701e3cf6c54e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6bb1f592-2702-4389-9314-119d944dcc46",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a9ded79-1120-47e4-9994-701e3cf6c54e",
                    "LayerId": "5a7b09c7-f2da-4bff-9cbe-6d44c1b260fd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 360,
    "layers": [
        {
            "id": "5a7b09c7-f2da-4bff-9cbe-6d44c1b260fd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "be85dbe6-e527-484a-aa74-76b2c81ed1e2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 0,
    "yorig": 0
}