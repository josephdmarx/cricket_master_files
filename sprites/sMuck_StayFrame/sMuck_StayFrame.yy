{
    "id": "7aad1605-bfd8-4d81-82dd-0af4c1356043",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sMuck_StayFrame",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 111,
    "bbox_left": 33,
    "bbox_right": 94,
    "bbox_top": 71,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6f7d7f58-cf42-4c84-bc9e-1a460c851659",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7aad1605-bfd8-4d81-82dd-0af4c1356043",
            "compositeImage": {
                "id": "15e6b760-0855-4f13-ad2e-2ea77bff5b49",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f7d7f58-cf42-4c84-bc9e-1a460c851659",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26db51df-24c6-4217-9dcf-dc4b042576da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f7d7f58-cf42-4c84-bc9e-1a460c851659",
                    "LayerId": "7b4d3cb5-fdcb-4285-a7e8-d29283412a8b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "7b4d3cb5-fdcb-4285-a7e8-d29283412a8b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7aad1605-bfd8-4d81-82dd-0af4c1356043",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 112
}