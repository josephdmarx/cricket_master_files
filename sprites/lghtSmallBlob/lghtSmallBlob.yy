{
    "id": "9e688e53-419a-4fce-b12c-d4f7e8df7bc5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "lghtSmallBlob",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 7,
    "bbox_right": 24,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d984991a-9a56-47d2-b3bd-cc5315cbaad6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e688e53-419a-4fce-b12c-d4f7e8df7bc5",
            "compositeImage": {
                "id": "c73fe0ed-61ae-4538-a5e2-59cbec55a8df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d984991a-9a56-47d2-b3bd-cc5315cbaad6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "42f3383e-2c9f-4180-ab10-e8b243db3115",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d984991a-9a56-47d2-b3bd-cc5315cbaad6",
                    "LayerId": "dccc4223-faa9-447f-8206-44de5fc01c6b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "dccc4223-faa9-447f-8206-44de5fc01c6b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9e688e53-419a-4fce-b12c-d4f7e8df7bc5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}