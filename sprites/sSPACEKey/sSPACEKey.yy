{
    "id": "b66151b8-ce5b-4f88-b9d9-894f1f02047c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSPACEKey",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 25,
    "bbox_left": 2,
    "bbox_right": 60,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d55ce02c-bbcd-4020-8606-58cfdbbcf4bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b66151b8-ce5b-4f88-b9d9-894f1f02047c",
            "compositeImage": {
                "id": "d37e47cd-1a78-4760-bb0c-9d0820c41482",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d55ce02c-bbcd-4020-8606-58cfdbbcf4bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1eef8ebb-163a-4049-8774-ebea2eff4914",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d55ce02c-bbcd-4020-8606-58cfdbbcf4bb",
                    "LayerId": "1eb320fd-e26f-4826-8cfd-a2f9e34eb7c3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "1eb320fd-e26f-4826-8cfd-a2f9e34eb7c3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b66151b8-ce5b-4f88-b9d9-894f1f02047c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 26
}