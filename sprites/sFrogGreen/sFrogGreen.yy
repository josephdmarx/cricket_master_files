{
    "id": "618b439e-aa46-4fcc-a6dc-f62e8c015b7d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sFrogGreen",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 25,
    "bbox_left": 21,
    "bbox_right": 45,
    "bbox_top": 4,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ff6ce267-5526-4f24-8c46-6c9cfdb6a37f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "618b439e-aa46-4fcc-a6dc-f62e8c015b7d",
            "compositeImage": {
                "id": "9258ee49-9ee6-4a9d-943d-60a3c17636ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff6ce267-5526-4f24-8c46-6c9cfdb6a37f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b23ad68b-a7d2-4bd3-bca3-03c6ca6f207c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff6ce267-5526-4f24-8c46-6c9cfdb6a37f",
                    "LayerId": "1df661b5-3457-4158-88a7-499003293f3e"
                }
            ]
        },
        {
            "id": "17694cea-abcc-483c-aea8-bbf54884bac4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "618b439e-aa46-4fcc-a6dc-f62e8c015b7d",
            "compositeImage": {
                "id": "dd207092-b0b6-4d93-8d55-cd87e19b2149",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "17694cea-abcc-483c-aea8-bbf54884bac4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0b3d70b-790d-404e-8205-d131a46c09ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "17694cea-abcc-483c-aea8-bbf54884bac4",
                    "LayerId": "1df661b5-3457-4158-88a7-499003293f3e"
                }
            ]
        },
        {
            "id": "6cc0f30d-ceae-4e2b-b85d-0af2c5d6a3ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "618b439e-aa46-4fcc-a6dc-f62e8c015b7d",
            "compositeImage": {
                "id": "e6339bf7-b4c2-48a7-ba94-a70bfb238a4c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6cc0f30d-ceae-4e2b-b85d-0af2c5d6a3ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c543e08a-d803-4c37-974a-d93715f3d271",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6cc0f30d-ceae-4e2b-b85d-0af2c5d6a3ce",
                    "LayerId": "1df661b5-3457-4158-88a7-499003293f3e"
                }
            ]
        },
        {
            "id": "2c3ac96e-e80e-4df8-9683-40459d581eac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "618b439e-aa46-4fcc-a6dc-f62e8c015b7d",
            "compositeImage": {
                "id": "6ab802fc-fecc-4f30-b643-da76c96dc51e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c3ac96e-e80e-4df8-9683-40459d581eac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3fcce95b-0391-475c-96ab-0c31a5ef409e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c3ac96e-e80e-4df8-9683-40459d581eac",
                    "LayerId": "1df661b5-3457-4158-88a7-499003293f3e"
                }
            ]
        },
        {
            "id": "26726e7c-12ac-4ca8-8d03-fa2253066e2d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "618b439e-aa46-4fcc-a6dc-f62e8c015b7d",
            "compositeImage": {
                "id": "8c20ea4e-8541-4fcb-b273-ebd20c9c6f81",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26726e7c-12ac-4ca8-8d03-fa2253066e2d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "73b2f2fd-8e6a-421f-9396-c9cc0cdadd67",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26726e7c-12ac-4ca8-8d03-fa2253066e2d",
                    "LayerId": "1df661b5-3457-4158-88a7-499003293f3e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "1df661b5-3457-4158-88a7-499003293f3e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "618b439e-aa46-4fcc-a6dc-f62e8c015b7d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 16
}