{
    "id": "2c2cb94e-915d-4da9-9815-8db8c8d6c623",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBlanketAir",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 49,
    "bbox_left": 23,
    "bbox_right": 86,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "497d1510-e137-41d3-9f11-18825ddc2461",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2c2cb94e-915d-4da9-9815-8db8c8d6c623",
            "compositeImage": {
                "id": "3d3adc6f-aaf7-42ad-88a7-55487c8c5f70",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "497d1510-e137-41d3-9f11-18825ddc2461",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6fae9315-c35a-48e9-84b1-a91f370acc54",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "497d1510-e137-41d3-9f11-18825ddc2461",
                    "LayerId": "6333fa8e-aafd-42f7-9cc8-4e6db87026bd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 65,
    "layers": [
        {
            "id": "6333fa8e-aafd-42f7-9cc8-4e6db87026bd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2c2cb94e-915d-4da9-9815-8db8c8d6c623",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 108,
    "xorig": 54,
    "yorig": 32
}