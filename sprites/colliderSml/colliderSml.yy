{
    "id": "240da789-6827-442b-8776-82c6c85d75a4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "colliderSml",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "618bad90-2b00-4cd1-b434-d9463a72eb46",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "240da789-6827-442b-8776-82c6c85d75a4",
            "compositeImage": {
                "id": "73135abd-d608-4394-bfde-62a0936be1eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "618bad90-2b00-4cd1-b434-d9463a72eb46",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8feb0f50-f74a-434c-bb8f-722a7c001111",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "618bad90-2b00-4cd1-b434-d9463a72eb46",
                    "LayerId": "7d6a6e7d-2132-42ae-bb59-8cc4b134db9d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "7d6a6e7d-2132-42ae-bb59-8cc4b134db9d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "240da789-6827-442b-8776-82c6c85d75a4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}