{
    "id": "1ef068ce-910c-4cbe-ad8f-890d17983928",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayerCane",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 94,
    "bbox_left": 58,
    "bbox_right": 72,
    "bbox_top": 42,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7865081c-baec-4b09-b800-b64a90caa3dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ef068ce-910c-4cbe-ad8f-890d17983928",
            "compositeImage": {
                "id": "1a47e3f4-4acb-4dab-a84f-6365cb06a693",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7865081c-baec-4b09-b800-b64a90caa3dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c997ba33-2bf6-4a74-a2b5-73403a9cdd48",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7865081c-baec-4b09-b800-b64a90caa3dc",
                    "LayerId": "afc17a78-4283-437d-8866-f20a78499115"
                }
            ]
        },
        {
            "id": "66f10c70-c577-40eb-9f76-3f14ec0918fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ef068ce-910c-4cbe-ad8f-890d17983928",
            "compositeImage": {
                "id": "7da6b974-ecb0-4dbb-adfd-c7783fc773f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "66f10c70-c577-40eb-9f76-3f14ec0918fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45a1e1ce-123f-437b-a622-1032b54fd324",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66f10c70-c577-40eb-9f76-3f14ec0918fb",
                    "LayerId": "afc17a78-4283-437d-8866-f20a78499115"
                }
            ]
        },
        {
            "id": "37d2d0e5-cc8e-4b71-94b5-59c23d1ee1c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ef068ce-910c-4cbe-ad8f-890d17983928",
            "compositeImage": {
                "id": "d1b11d20-d519-4c8f-ac0c-e00e450b46e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "37d2d0e5-cc8e-4b71-94b5-59c23d1ee1c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dae666ec-0cb8-4406-bcc7-405c9f0853c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37d2d0e5-cc8e-4b71-94b5-59c23d1ee1c2",
                    "LayerId": "afc17a78-4283-437d-8866-f20a78499115"
                }
            ]
        },
        {
            "id": "cad18466-55c7-4ca4-9c9f-fc34ed1fbaba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ef068ce-910c-4cbe-ad8f-890d17983928",
            "compositeImage": {
                "id": "e1ed0876-5781-4639-9d37-c8bede0162ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cad18466-55c7-4ca4-9c9f-fc34ed1fbaba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c7a10ecf-1069-4cfe-bac4-e527a3212514",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cad18466-55c7-4ca4-9c9f-fc34ed1fbaba",
                    "LayerId": "afc17a78-4283-437d-8866-f20a78499115"
                }
            ]
        },
        {
            "id": "ecc71d0d-a1ff-48ce-938b-309eb1b3b4be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ef068ce-910c-4cbe-ad8f-890d17983928",
            "compositeImage": {
                "id": "a6ca9e1b-f39e-4ebd-9cd8-74fd24d056aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ecc71d0d-a1ff-48ce-938b-309eb1b3b4be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48577481-7342-410e-8c31-a6e1deb57275",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ecc71d0d-a1ff-48ce-938b-309eb1b3b4be",
                    "LayerId": "afc17a78-4283-437d-8866-f20a78499115"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "afc17a78-4283-437d-8866-f20a78499115",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1ef068ce-910c-4cbe-ad8f-890d17983928",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}