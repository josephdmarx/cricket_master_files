{
    "id": "913d6ced-d333-437d-89fa-fd61ad89a906",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sDoor01",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 96,
    "bbox_left": 43,
    "bbox_right": 84,
    "bbox_top": 38,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3b1bd109-dcc2-4a43-9659-9f0d3067d5c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "913d6ced-d333-437d-89fa-fd61ad89a906",
            "compositeImage": {
                "id": "9d93a269-9654-4878-9c61-693c38ed68e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b1bd109-dcc2-4a43-9659-9f0d3067d5c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "237f18cb-8862-4f0b-95b3-05830bd02ead",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b1bd109-dcc2-4a43-9659-9f0d3067d5c4",
                    "LayerId": "07919a27-f637-467f-8e89-254ae9cea9f1"
                }
            ]
        },
        {
            "id": "a73ea546-4be2-4ca2-af01-3d6d090bc9fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "913d6ced-d333-437d-89fa-fd61ad89a906",
            "compositeImage": {
                "id": "ac0166e9-a5ac-48df-9f36-2ecef82ba09d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a73ea546-4be2-4ca2-af01-3d6d090bc9fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95d373f4-81a4-4ae2-b3e7-00ab491e1fcc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a73ea546-4be2-4ca2-af01-3d6d090bc9fa",
                    "LayerId": "07919a27-f637-467f-8e89-254ae9cea9f1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "07919a27-f637-467f-8e89-254ae9cea9f1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "913d6ced-d333-437d-89fa-fd61ad89a906",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 43,
    "yorig": 94
}