{
    "id": "608a8ea7-ab67-4c06-91e0-5366d59b2f28",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sDust",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 6,
    "bbox_left": 1,
    "bbox_right": 6,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fb3bae2a-287e-492b-9dca-e26d5594f476",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "608a8ea7-ab67-4c06-91e0-5366d59b2f28",
            "compositeImage": {
                "id": "e9bf6dda-9b43-4be9-ba23-3a27e1fa2f42",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb3bae2a-287e-492b-9dca-e26d5594f476",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "96682129-6eec-4b79-a217-8bf26626fd74",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb3bae2a-287e-492b-9dca-e26d5594f476",
                    "LayerId": "cc4d0480-b427-43d3-a3a7-1cc00eae41d7"
                }
            ]
        },
        {
            "id": "23dd8e97-7533-4286-afdb-e266a24a65d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "608a8ea7-ab67-4c06-91e0-5366d59b2f28",
            "compositeImage": {
                "id": "c7997c66-4479-4aab-aab3-6afcb1e865b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "23dd8e97-7533-4286-afdb-e266a24a65d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "976c71b0-ccfd-4761-bb3f-ee9dedab1c82",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "23dd8e97-7533-4286-afdb-e266a24a65d6",
                    "LayerId": "cc4d0480-b427-43d3-a3a7-1cc00eae41d7"
                }
            ]
        },
        {
            "id": "052e0e97-da76-49c9-9f24-6df6b34f45c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "608a8ea7-ab67-4c06-91e0-5366d59b2f28",
            "compositeImage": {
                "id": "08f2b89b-08eb-4193-a864-bd67c69a5a70",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "052e0e97-da76-49c9-9f24-6df6b34f45c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "afa8276a-af91-4e35-976f-eeb7bac8ee8c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "052e0e97-da76-49c9-9f24-6df6b34f45c2",
                    "LayerId": "cc4d0480-b427-43d3-a3a7-1cc00eae41d7"
                }
            ]
        },
        {
            "id": "d99e5b2e-5412-47d2-aab5-99dc9e32853f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "608a8ea7-ab67-4c06-91e0-5366d59b2f28",
            "compositeImage": {
                "id": "dcbc34f4-0795-4360-94e1-16968c2c445c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d99e5b2e-5412-47d2-aab5-99dc9e32853f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b1102624-0783-4da2-82fc-d57991eaae68",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d99e5b2e-5412-47d2-aab5-99dc9e32853f",
                    "LayerId": "cc4d0480-b427-43d3-a3a7-1cc00eae41d7"
                }
            ]
        },
        {
            "id": "ececaa14-ee9d-4e08-a4a6-eb3b3cd62689",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "608a8ea7-ab67-4c06-91e0-5366d59b2f28",
            "compositeImage": {
                "id": "dec8480f-5f57-400b-b4f1-1f87072437a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ececaa14-ee9d-4e08-a4a6-eb3b3cd62689",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a76b0616-b61b-429e-9d57-b9c7d31922f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ececaa14-ee9d-4e08-a4a6-eb3b3cd62689",
                    "LayerId": "cc4d0480-b427-43d3-a3a7-1cc00eae41d7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "cc4d0480-b427-43d3-a3a7-1cc00eae41d7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "608a8ea7-ab67-4c06-91e0-5366d59b2f28",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}