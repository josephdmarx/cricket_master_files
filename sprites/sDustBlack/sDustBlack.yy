{
    "id": "31c1969d-5686-4cd5-9b0f-445c4588a5e7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sDustBlack",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 6,
    "bbox_left": 1,
    "bbox_right": 6,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fb4d62a6-ee0f-4b65-8725-ad08c87815e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "31c1969d-5686-4cd5-9b0f-445c4588a5e7",
            "compositeImage": {
                "id": "abbe3520-fc2c-48f6-8e8d-32bbf2528599",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb4d62a6-ee0f-4b65-8725-ad08c87815e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da590f4f-090e-42f2-9c04-9dcecd1e049d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb4d62a6-ee0f-4b65-8725-ad08c87815e7",
                    "LayerId": "028b9251-99f3-4c48-82a7-12171c4f5b24"
                }
            ]
        },
        {
            "id": "a93781d8-8714-44e0-a666-5e14f2bc9f46",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "31c1969d-5686-4cd5-9b0f-445c4588a5e7",
            "compositeImage": {
                "id": "4f17a878-8870-4dc9-bdba-6a2ca2a5bcc5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a93781d8-8714-44e0-a666-5e14f2bc9f46",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a593d85-9d77-4f34-bc33-7bce10ad15dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a93781d8-8714-44e0-a666-5e14f2bc9f46",
                    "LayerId": "028b9251-99f3-4c48-82a7-12171c4f5b24"
                }
            ]
        },
        {
            "id": "5130db77-4002-4733-9b38-874eb96a6d5a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "31c1969d-5686-4cd5-9b0f-445c4588a5e7",
            "compositeImage": {
                "id": "7bf41194-fc8d-4516-a665-1105c8d76eef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5130db77-4002-4733-9b38-874eb96a6d5a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e59e155-1981-4175-949e-a8f5749f219f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5130db77-4002-4733-9b38-874eb96a6d5a",
                    "LayerId": "028b9251-99f3-4c48-82a7-12171c4f5b24"
                }
            ]
        },
        {
            "id": "7cfa8bfb-b45d-4db7-9e5e-3ba2ed57afa5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "31c1969d-5686-4cd5-9b0f-445c4588a5e7",
            "compositeImage": {
                "id": "b74d7c5c-4966-4eeb-8598-c08de08d0096",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7cfa8bfb-b45d-4db7-9e5e-3ba2ed57afa5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a37933a3-3bd7-4fa6-8416-496d8e21d97b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7cfa8bfb-b45d-4db7-9e5e-3ba2ed57afa5",
                    "LayerId": "028b9251-99f3-4c48-82a7-12171c4f5b24"
                }
            ]
        },
        {
            "id": "910d4b28-144f-4476-9666-cf5adedf015f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "31c1969d-5686-4cd5-9b0f-445c4588a5e7",
            "compositeImage": {
                "id": "aa6a8e2d-272e-438b-a7a1-106e7f84d224",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "910d4b28-144f-4476-9666-cf5adedf015f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cab37012-e837-4626-a9a0-06712b054bd4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "910d4b28-144f-4476-9666-cf5adedf015f",
                    "LayerId": "028b9251-99f3-4c48-82a7-12171c4f5b24"
                }
            ]
        },
        {
            "id": "8f54b71b-9744-4509-aa33-ebd7c8b48d09",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "31c1969d-5686-4cd5-9b0f-445c4588a5e7",
            "compositeImage": {
                "id": "1bda8179-46f3-4336-831c-10456c74533c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f54b71b-9744-4509-aa33-ebd7c8b48d09",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4995fdbd-7529-41b5-a09c-8646f10a7e7f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f54b71b-9744-4509-aa33-ebd7c8b48d09",
                    "LayerId": "028b9251-99f3-4c48-82a7-12171c4f5b24"
                }
            ]
        },
        {
            "id": "a85b206d-ce3f-4cf6-a255-d2aeb5eb65ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "31c1969d-5686-4cd5-9b0f-445c4588a5e7",
            "compositeImage": {
                "id": "69c4cefe-b01a-4e42-970c-4309c12996b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a85b206d-ce3f-4cf6-a255-d2aeb5eb65ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26baaa1d-b074-412d-8cdc-84e480fca761",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a85b206d-ce3f-4cf6-a255-d2aeb5eb65ad",
                    "LayerId": "028b9251-99f3-4c48-82a7-12171c4f5b24"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "028b9251-99f3-4c48-82a7-12171c4f5b24",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "31c1969d-5686-4cd5-9b0f-445c4588a5e7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}