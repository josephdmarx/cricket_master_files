{
    "id": "43cbee62-d518-4e24-af96-94bc76cd865d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sAntag_Cast",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 140,
    "bbox_left": 21,
    "bbox_right": 97,
    "bbox_top": 35,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6c5fcff9-d9c0-4b17-83fb-195658b989b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "43cbee62-d518-4e24-af96-94bc76cd865d",
            "compositeImage": {
                "id": "7c7f9bba-972d-4ffa-8a74-4d0be8a2eb12",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c5fcff9-d9c0-4b17-83fb-195658b989b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a13a99a-fe1f-4971-a122-cb8cd205463a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c5fcff9-d9c0-4b17-83fb-195658b989b3",
                    "LayerId": "9d8ea3e9-439b-4cba-bc6e-199e98e4eb52"
                }
            ]
        },
        {
            "id": "e451ff99-2c1d-466c-85b2-c00309499df4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "43cbee62-d518-4e24-af96-94bc76cd865d",
            "compositeImage": {
                "id": "04841c2e-9150-487a-866b-01697e38215c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e451ff99-2c1d-466c-85b2-c00309499df4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14fd9e65-9252-4083-a71a-33d318fdc560",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e451ff99-2c1d-466c-85b2-c00309499df4",
                    "LayerId": "9d8ea3e9-439b-4cba-bc6e-199e98e4eb52"
                }
            ]
        },
        {
            "id": "80b391f1-314e-4bf8-9f74-b05a1f9de565",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "43cbee62-d518-4e24-af96-94bc76cd865d",
            "compositeImage": {
                "id": "9ec60464-1e99-4277-9874-8c55737ef989",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "80b391f1-314e-4bf8-9f74-b05a1f9de565",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "63665af6-3f08-4d36-bde1-9078b9624a04",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80b391f1-314e-4bf8-9f74-b05a1f9de565",
                    "LayerId": "9d8ea3e9-439b-4cba-bc6e-199e98e4eb52"
                }
            ]
        },
        {
            "id": "40ba5e17-c7e6-4f36-9957-92691823cba6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "43cbee62-d518-4e24-af96-94bc76cd865d",
            "compositeImage": {
                "id": "1ff2fd62-8970-4969-9b89-fcc7088d7773",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "40ba5e17-c7e6-4f36-9957-92691823cba6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a4de0e9-265f-4178-ac43-e758296abb68",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "40ba5e17-c7e6-4f36-9957-92691823cba6",
                    "LayerId": "9d8ea3e9-439b-4cba-bc6e-199e98e4eb52"
                }
            ]
        },
        {
            "id": "032559a6-9326-4518-b2fe-f0d102e548a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "43cbee62-d518-4e24-af96-94bc76cd865d",
            "compositeImage": {
                "id": "5a30f672-488f-4885-8789-f324ef4e5614",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "032559a6-9326-4518-b2fe-f0d102e548a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64db4049-9097-4f34-9118-fffe3fadc369",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "032559a6-9326-4518-b2fe-f0d102e548a2",
                    "LayerId": "9d8ea3e9-439b-4cba-bc6e-199e98e4eb52"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 154,
    "layers": [
        {
            "id": "9d8ea3e9-439b-4cba-bc6e-199e98e4eb52",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "43cbee62-d518-4e24-af96-94bc76cd865d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 77
}