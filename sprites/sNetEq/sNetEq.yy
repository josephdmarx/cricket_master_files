{
    "id": "1ffc3908-d0ee-4c9d-b33e-b9340f57bda8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sNetEq",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 26,
    "bbox_left": 0,
    "bbox_right": 30,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "920ec91d-de49-40ef-bb95-1b6326bb4534",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ffc3908-d0ee-4c9d-b33e-b9340f57bda8",
            "compositeImage": {
                "id": "9d3043d0-6806-40a5-afb6-c3dbee4cace9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "920ec91d-de49-40ef-bb95-1b6326bb4534",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82a95ddb-271a-4d84-96f8-eda993ccd18c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "920ec91d-de49-40ef-bb95-1b6326bb4534",
                    "LayerId": "4267507d-6b5b-4575-a105-81616266bfb1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "4267507d-6b5b-4575-a105-81616266bfb1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1ffc3908-d0ee-4c9d-b33e-b9340f57bda8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}