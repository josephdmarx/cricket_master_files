{
    "id": "12a82432-93a5-44e2-b498-93bd53b42086",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sWasp_D",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 78,
    "bbox_left": 48,
    "bbox_right": 81,
    "bbox_top": 45,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a3ef0a9e-fadf-4ce9-9fdd-2bdb8eb45bb5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "12a82432-93a5-44e2-b498-93bd53b42086",
            "compositeImage": {
                "id": "24dbcbac-38be-4e50-b865-9a1f4d4014bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3ef0a9e-fadf-4ce9-9fdd-2bdb8eb45bb5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c7b6832c-3acb-497d-97b9-df0481bf98cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3ef0a9e-fadf-4ce9-9fdd-2bdb8eb45bb5",
                    "LayerId": "97911e26-8ca4-41e9-a65f-9cc730f04993"
                }
            ]
        },
        {
            "id": "d6a870c2-93e9-480e-87f0-026541789642",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "12a82432-93a5-44e2-b498-93bd53b42086",
            "compositeImage": {
                "id": "38f06706-0fae-4353-af81-07785452f837",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6a870c2-93e9-480e-87f0-026541789642",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f1d4634e-e6ab-4f7d-9ccc-5c987d7fc049",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6a870c2-93e9-480e-87f0-026541789642",
                    "LayerId": "97911e26-8ca4-41e9-a65f-9cc730f04993"
                }
            ]
        },
        {
            "id": "dd93917b-d90b-43d7-adde-6b0c42a53eea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "12a82432-93a5-44e2-b498-93bd53b42086",
            "compositeImage": {
                "id": "a6621be6-bda6-4a76-a040-79768e8701d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd93917b-d90b-43d7-adde-6b0c42a53eea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e83dfb1-eb3b-4c38-906a-0d686c62abc3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd93917b-d90b-43d7-adde-6b0c42a53eea",
                    "LayerId": "97911e26-8ca4-41e9-a65f-9cc730f04993"
                }
            ]
        },
        {
            "id": "f1441edb-294e-449b-93c0-1d6d4698ade7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "12a82432-93a5-44e2-b498-93bd53b42086",
            "compositeImage": {
                "id": "840e481b-3168-454f-8a3b-a953c5364920",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1441edb-294e-449b-93c0-1d6d4698ade7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa85ae1c-e597-408a-88ee-232624ef6b8a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1441edb-294e-449b-93c0-1d6d4698ade7",
                    "LayerId": "97911e26-8ca4-41e9-a65f-9cc730f04993"
                }
            ]
        },
        {
            "id": "95127d54-1beb-4da1-b78a-1bdf18a69bf0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "12a82432-93a5-44e2-b498-93bd53b42086",
            "compositeImage": {
                "id": "3c47d863-8b21-4c41-ba0c-4671a18915bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "95127d54-1beb-4da1-b78a-1bdf18a69bf0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a66db408-6416-455c-a974-d15df1456f6c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "95127d54-1beb-4da1-b78a-1bdf18a69bf0",
                    "LayerId": "97911e26-8ca4-41e9-a65f-9cc730f04993"
                }
            ]
        },
        {
            "id": "1a90a0d1-925e-4616-ad3e-1578cf452537",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "12a82432-93a5-44e2-b498-93bd53b42086",
            "compositeImage": {
                "id": "1bfb29c8-db03-45ca-8914-5fc1d259e723",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a90a0d1-925e-4616-ad3e-1578cf452537",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95684793-3746-42cf-9686-613e87c86932",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a90a0d1-925e-4616-ad3e-1578cf452537",
                    "LayerId": "97911e26-8ca4-41e9-a65f-9cc730f04993"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "97911e26-8ca4-41e9-a65f-9cc730f04993",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "12a82432-93a5-44e2-b498-93bd53b42086",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}