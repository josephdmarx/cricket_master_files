{
    "id": "87d644de-5b8a-42d3-beeb-4ecbf0a690a9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayerW",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 94,
    "bbox_left": 58,
    "bbox_right": 72,
    "bbox_top": 42,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e469aa5a-4d57-4940-892f-8c1bfb0d5bbe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "87d644de-5b8a-42d3-beeb-4ecbf0a690a9",
            "compositeImage": {
                "id": "80b72354-9669-47b9-a298-579325e51871",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e469aa5a-4d57-4940-892f-8c1bfb0d5bbe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1874ae23-01eb-480f-ace0-f29f241e6da6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e469aa5a-4d57-4940-892f-8c1bfb0d5bbe",
                    "LayerId": "f3e0a797-1ef2-49f9-9848-cad41c632e54"
                }
            ]
        },
        {
            "id": "a7a68174-a601-4fdc-8937-922fe902e95b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "87d644de-5b8a-42d3-beeb-4ecbf0a690a9",
            "compositeImage": {
                "id": "151c9a4d-7b11-4bd6-8822-8909cc04455a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7a68174-a601-4fdc-8937-922fe902e95b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea87ac68-a699-405d-8ea5-99c2843eb860",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7a68174-a601-4fdc-8937-922fe902e95b",
                    "LayerId": "f3e0a797-1ef2-49f9-9848-cad41c632e54"
                }
            ]
        },
        {
            "id": "01a092b1-a6b1-4aef-a039-ef2ddf1f80f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "87d644de-5b8a-42d3-beeb-4ecbf0a690a9",
            "compositeImage": {
                "id": "779e274d-fe5e-4863-a74e-31de6bb1d3a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01a092b1-a6b1-4aef-a039-ef2ddf1f80f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eaf6a0ac-d0cc-4886-a7b1-50dd74d244e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01a092b1-a6b1-4aef-a039-ef2ddf1f80f8",
                    "LayerId": "f3e0a797-1ef2-49f9-9848-cad41c632e54"
                }
            ]
        },
        {
            "id": "758eac4b-9bd1-457c-b2fb-8f66f592eb0e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "87d644de-5b8a-42d3-beeb-4ecbf0a690a9",
            "compositeImage": {
                "id": "813adb47-11f2-4fd0-bc80-7cb6ff0502ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "758eac4b-9bd1-457c-b2fb-8f66f592eb0e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9751e11e-f148-49ec-9490-8e36a21c1da9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "758eac4b-9bd1-457c-b2fb-8f66f592eb0e",
                    "LayerId": "f3e0a797-1ef2-49f9-9848-cad41c632e54"
                }
            ]
        },
        {
            "id": "c9b1cd26-005d-4748-a3fa-e5d560b28546",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "87d644de-5b8a-42d3-beeb-4ecbf0a690a9",
            "compositeImage": {
                "id": "6385be84-2470-4341-8bc8-ac67084d3882",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9b1cd26-005d-4748-a3fa-e5d560b28546",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "119a8dd9-d1bb-4803-b831-999edd758fc6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9b1cd26-005d-4748-a3fa-e5d560b28546",
                    "LayerId": "f3e0a797-1ef2-49f9-9848-cad41c632e54"
                }
            ]
        },
        {
            "id": "26179198-1109-424b-b1ac-89eaa297f6a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "87d644de-5b8a-42d3-beeb-4ecbf0a690a9",
            "compositeImage": {
                "id": "8deb8583-6670-49b8-ae70-35700eb82ebc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26179198-1109-424b-b1ac-89eaa297f6a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d06e2ee-89bb-4fc6-bc5e-0a500d0fb119",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26179198-1109-424b-b1ac-89eaa297f6a8",
                    "LayerId": "f3e0a797-1ef2-49f9-9848-cad41c632e54"
                }
            ]
        },
        {
            "id": "6915d864-4c23-4643-999b-09cfb2ec2125",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "87d644de-5b8a-42d3-beeb-4ecbf0a690a9",
            "compositeImage": {
                "id": "09f5cc5a-3c8f-43e7-9ded-89f5529864c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6915d864-4c23-4643-999b-09cfb2ec2125",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ab1c604-e931-458d-9dc5-5ad72001e6fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6915d864-4c23-4643-999b-09cfb2ec2125",
                    "LayerId": "f3e0a797-1ef2-49f9-9848-cad41c632e54"
                }
            ]
        },
        {
            "id": "8a840887-9a30-46b2-a453-1c05fffaf326",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "87d644de-5b8a-42d3-beeb-4ecbf0a690a9",
            "compositeImage": {
                "id": "baf89d2c-bab6-419b-ab88-23adcb393c89",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a840887-9a30-46b2-a453-1c05fffaf326",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1460adfe-2f4a-4875-b1f0-9be4db23d5c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a840887-9a30-46b2-a453-1c05fffaf326",
                    "LayerId": "f3e0a797-1ef2-49f9-9848-cad41c632e54"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "f3e0a797-1ef2-49f9-9848-cad41c632e54",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "87d644de-5b8a-42d3-beeb-4ecbf0a690a9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}