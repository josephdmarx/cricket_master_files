{
    "id": "9d8a56db-8824-4404-82dd-ec8101f6dc21",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sHurleyIdle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 58,
    "bbox_left": 39,
    "bbox_right": 66,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e1559693-442a-4cfb-af0a-5d13a83b99fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9d8a56db-8824-4404-82dd-ec8101f6dc21",
            "compositeImage": {
                "id": "fb5c8f94-cf14-4fb3-93f9-7fe81e0af8e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1559693-442a-4cfb-af0a-5d13a83b99fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d9cdec8-e7e2-4d75-9656-9d814eaf7d4b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1559693-442a-4cfb-af0a-5d13a83b99fd",
                    "LayerId": "642e3e67-3f53-44bf-b5f1-9e8791db95d8"
                }
            ]
        },
        {
            "id": "38591353-0549-4ebf-80e2-3277fca383a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9d8a56db-8824-4404-82dd-ec8101f6dc21",
            "compositeImage": {
                "id": "35c49e6a-2bb6-4680-8823-4a8ddae0a3a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38591353-0549-4ebf-80e2-3277fca383a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf835fe5-3a0a-4a10-be2a-fd66e64e148a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38591353-0549-4ebf-80e2-3277fca383a9",
                    "LayerId": "642e3e67-3f53-44bf-b5f1-9e8791db95d8"
                }
            ]
        },
        {
            "id": "66f33df2-7d8c-4a2a-a376-e358c1c2749c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9d8a56db-8824-4404-82dd-ec8101f6dc21",
            "compositeImage": {
                "id": "2adef0ca-0ee9-4fb9-a74e-95d3bc221379",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "66f33df2-7d8c-4a2a-a376-e358c1c2749c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "24d8dca1-2b0e-4cfb-8786-831d74d0910a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66f33df2-7d8c-4a2a-a376-e358c1c2749c",
                    "LayerId": "642e3e67-3f53-44bf-b5f1-9e8791db95d8"
                }
            ]
        },
        {
            "id": "095ed2b0-b1bb-45e2-8219-8ca660179a9f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9d8a56db-8824-4404-82dd-ec8101f6dc21",
            "compositeImage": {
                "id": "89b2880b-5f0f-45c6-b0e1-58230c9c2fd6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "095ed2b0-b1bb-45e2-8219-8ca660179a9f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5271d79b-7895-4699-9be1-42da838f847f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "095ed2b0-b1bb-45e2-8219-8ca660179a9f",
                    "LayerId": "642e3e67-3f53-44bf-b5f1-9e8791db95d8"
                }
            ]
        },
        {
            "id": "b9b278f4-fc2b-4997-8ae8-6872361c804a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9d8a56db-8824-4404-82dd-ec8101f6dc21",
            "compositeImage": {
                "id": "e7f8396e-caaa-4f5b-b881-4e505a24e3af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b9b278f4-fc2b-4997-8ae8-6872361c804a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c50a306-9c1f-43c4-bdd2-3bccff920762",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9b278f4-fc2b-4997-8ae8-6872361c804a",
                    "LayerId": "642e3e67-3f53-44bf-b5f1-9e8791db95d8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 65,
    "layers": [
        {
            "id": "642e3e67-3f53-44bf-b5f1-9e8791db95d8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9d8a56db-8824-4404-82dd-ec8101f6dc21",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 108,
    "xorig": 54,
    "yorig": 32
}