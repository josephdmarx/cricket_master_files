{
    "id": "19db53bd-0bad-4bc9-aef5-0b55e06f365f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSpido_Walk",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 113,
    "bbox_left": 49,
    "bbox_right": 114,
    "bbox_top": 74,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "36ee96dd-23c1-4f56-950e-0091628e7d4b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19db53bd-0bad-4bc9-aef5-0b55e06f365f",
            "compositeImage": {
                "id": "d1c0a4f0-dac8-4f80-bd70-eaa5805bd02b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "36ee96dd-23c1-4f56-950e-0091628e7d4b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "68437f42-7761-4b39-9d99-efccc6543d5f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36ee96dd-23c1-4f56-950e-0091628e7d4b",
                    "LayerId": "6bed79a7-0473-43b8-8201-76e0b587c47a"
                }
            ]
        },
        {
            "id": "cebabec7-e8a8-4e04-94e0-58fda6774755",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19db53bd-0bad-4bc9-aef5-0b55e06f365f",
            "compositeImage": {
                "id": "e9eb2f69-5d97-45ef-841e-f4ad3a93f98a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cebabec7-e8a8-4e04-94e0-58fda6774755",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95512678-51fe-471e-8758-b21e4974062d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cebabec7-e8a8-4e04-94e0-58fda6774755",
                    "LayerId": "6bed79a7-0473-43b8-8201-76e0b587c47a"
                }
            ]
        },
        {
            "id": "9938d8f4-a6ad-48eb-9870-98f07e856a28",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19db53bd-0bad-4bc9-aef5-0b55e06f365f",
            "compositeImage": {
                "id": "c5d57b23-d7e7-441f-89d5-d00afc724713",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9938d8f4-a6ad-48eb-9870-98f07e856a28",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca043d17-4767-4d26-80fd-2ac72990628c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9938d8f4-a6ad-48eb-9870-98f07e856a28",
                    "LayerId": "6bed79a7-0473-43b8-8201-76e0b587c47a"
                }
            ]
        },
        {
            "id": "0d577557-2897-495a-bf9b-274dfc01073b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19db53bd-0bad-4bc9-aef5-0b55e06f365f",
            "compositeImage": {
                "id": "32e2f085-3389-422b-9fc6-a242228f762b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d577557-2897-495a-bf9b-274dfc01073b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a645457-7c2a-4d51-aa6f-37be383a0c3b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d577557-2897-495a-bf9b-274dfc01073b",
                    "LayerId": "6bed79a7-0473-43b8-8201-76e0b587c47a"
                }
            ]
        },
        {
            "id": "592c9bc0-ff97-4cb7-8e30-5c5f6e704c2a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19db53bd-0bad-4bc9-aef5-0b55e06f365f",
            "compositeImage": {
                "id": "1bdc7683-2760-45f2-823a-88caf58fa31e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "592c9bc0-ff97-4cb7-8e30-5c5f6e704c2a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f368667-9956-4cc8-be7a-f55b1015c1a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "592c9bc0-ff97-4cb7-8e30-5c5f6e704c2a",
                    "LayerId": "6bed79a7-0473-43b8-8201-76e0b587c47a"
                }
            ]
        },
        {
            "id": "f1d23f03-1523-482f-9cb0-22185e77622e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19db53bd-0bad-4bc9-aef5-0b55e06f365f",
            "compositeImage": {
                "id": "e3d6077a-54e3-4195-94a4-4429b556d76e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1d23f03-1523-482f-9cb0-22185e77622e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b9ddf791-9d0e-4adc-9475-581fa836814b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1d23f03-1523-482f-9cb0-22185e77622e",
                    "LayerId": "6bed79a7-0473-43b8-8201-76e0b587c47a"
                }
            ]
        },
        {
            "id": "0647a131-ca71-4152-88d9-b61843516863",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19db53bd-0bad-4bc9-aef5-0b55e06f365f",
            "compositeImage": {
                "id": "532aac73-9e99-4ff1-a1ce-4a7885a2e658",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0647a131-ca71-4152-88d9-b61843516863",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da142537-5040-40cc-ba6d-6cdea0866775",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0647a131-ca71-4152-88d9-b61843516863",
                    "LayerId": "6bed79a7-0473-43b8-8201-76e0b587c47a"
                }
            ]
        },
        {
            "id": "c2e0e909-a4cc-409f-a16e-efa690c9cdae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19db53bd-0bad-4bc9-aef5-0b55e06f365f",
            "compositeImage": {
                "id": "eb56d5d7-92b1-4b40-b721-63cde8892c3b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2e0e909-a4cc-409f-a16e-efa690c9cdae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "601ef224-4e0b-4d0f-94e0-0b6d81521509",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2e0e909-a4cc-409f-a16e-efa690c9cdae",
                    "LayerId": "6bed79a7-0473-43b8-8201-76e0b587c47a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "6bed79a7-0473-43b8-8201-76e0b587c47a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "19db53bd-0bad-4bc9-aef5-0b55e06f365f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 162,
    "xorig": 20,
    "yorig": 40
}