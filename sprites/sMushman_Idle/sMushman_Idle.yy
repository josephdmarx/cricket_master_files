{
    "id": "d7dc2e58-5841-4b1c-878c-f697ed2a2ef5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sMushman_Idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 94,
    "bbox_left": 45,
    "bbox_right": 83,
    "bbox_top": 40,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ca7d86c5-7ff2-4e0a-8d10-443e45b9e3bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7dc2e58-5841-4b1c-878c-f697ed2a2ef5",
            "compositeImage": {
                "id": "27ab8551-999a-44d1-8a15-0485081d62de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca7d86c5-7ff2-4e0a-8d10-443e45b9e3bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b6ff5bd7-17e2-4ed9-9785-c7898c83385c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca7d86c5-7ff2-4e0a-8d10-443e45b9e3bb",
                    "LayerId": "9342c691-3c19-4047-9d8e-0693a3cfb9bc"
                }
            ]
        },
        {
            "id": "f29a035f-0db0-4b2a-a353-7341d989528a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7dc2e58-5841-4b1c-878c-f697ed2a2ef5",
            "compositeImage": {
                "id": "363c0504-f27f-441d-b2fc-47aca128d058",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f29a035f-0db0-4b2a-a353-7341d989528a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c787759-c966-4a0a-886a-714ea8dd7e98",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f29a035f-0db0-4b2a-a353-7341d989528a",
                    "LayerId": "9342c691-3c19-4047-9d8e-0693a3cfb9bc"
                }
            ]
        },
        {
            "id": "c4715d95-7650-404f-b551-a8c456599083",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7dc2e58-5841-4b1c-878c-f697ed2a2ef5",
            "compositeImage": {
                "id": "2a00cb64-af99-4c7c-9883-4683255e72a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c4715d95-7650-404f-b551-a8c456599083",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e5ee70a-520e-41f6-a67c-a97283367a57",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c4715d95-7650-404f-b551-a8c456599083",
                    "LayerId": "9342c691-3c19-4047-9d8e-0693a3cfb9bc"
                }
            ]
        },
        {
            "id": "d287ab86-3f34-4073-9681-2e9317f9ec8e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7dc2e58-5841-4b1c-878c-f697ed2a2ef5",
            "compositeImage": {
                "id": "ca3b0480-f9e4-46d5-aff0-b71c09b967d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d287ab86-3f34-4073-9681-2e9317f9ec8e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "03f2fffe-f0c8-4599-8683-550fcf7f5b28",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d287ab86-3f34-4073-9681-2e9317f9ec8e",
                    "LayerId": "9342c691-3c19-4047-9d8e-0693a3cfb9bc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "9342c691-3c19-4047-9d8e-0693a3cfb9bc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d7dc2e58-5841-4b1c-878c-f697ed2a2ef5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}