{
    "id": "37290d53-c487-4812-8fa8-a6a7a4be4fcf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sRightBumperPad",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 6,
    "bbox_right": 25,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6172b35e-867f-46f0-92c1-edf60591998f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37290d53-c487-4812-8fa8-a6a7a4be4fcf",
            "compositeImage": {
                "id": "cff5c874-160e-4560-a343-a875f90208e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6172b35e-867f-46f0-92c1-edf60591998f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d58db68a-d03e-43f4-a6bc-9e11644c15db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6172b35e-867f-46f0-92c1-edf60591998f",
                    "LayerId": "30b8b241-d93d-49a4-a597-9949811a946e"
                }
            ]
        },
        {
            "id": "b04706a2-a891-431c-9a99-e94125213833",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37290d53-c487-4812-8fa8-a6a7a4be4fcf",
            "compositeImage": {
                "id": "b6cd71d0-699c-49e7-b3c8-35a13c6650a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b04706a2-a891-431c-9a99-e94125213833",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd85dc39-92fa-45cb-a674-21344b0fdbac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b04706a2-a891-431c-9a99-e94125213833",
                    "LayerId": "30b8b241-d93d-49a4-a597-9949811a946e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "30b8b241-d93d-49a4-a597-9949811a946e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "37290d53-c487-4812-8fa8-a6a7a4be4fcf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 15,
    "yorig": 25
}