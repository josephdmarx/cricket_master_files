{
    "id": "86472daa-4aa3-46f4-a2ae-1cb92edff45e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sDoor02",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 93,
    "bbox_left": 40,
    "bbox_right": 83,
    "bbox_top": 35,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "016cdc03-4d44-4fe1-95d6-7c675a5a17a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "86472daa-4aa3-46f4-a2ae-1cb92edff45e",
            "compositeImage": {
                "id": "a7be4418-417b-4fe2-a71c-df2acb526dec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "016cdc03-4d44-4fe1-95d6-7c675a5a17a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2149fde4-7834-46f3-94da-cc38c327dac2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "016cdc03-4d44-4fe1-95d6-7c675a5a17a7",
                    "LayerId": "8050ce0c-5d67-4007-a70d-aee2e1e6da9d"
                }
            ]
        },
        {
            "id": "7d3fbd3e-eccc-43be-af00-0c4e0769c1a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "86472daa-4aa3-46f4-a2ae-1cb92edff45e",
            "compositeImage": {
                "id": "4aa9806b-636a-4f3b-a528-750f587b81ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d3fbd3e-eccc-43be-af00-0c4e0769c1a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "31e95e7d-cbb8-49fc-a100-9a1b0c15d39a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d3fbd3e-eccc-43be-af00-0c4e0769c1a2",
                    "LayerId": "8050ce0c-5d67-4007-a70d-aee2e1e6da9d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "8050ce0c-5d67-4007-a70d-aee2e1e6da9d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "86472daa-4aa3-46f4-a2ae-1cb92edff45e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 42,
    "yorig": 92
}