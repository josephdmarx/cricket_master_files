{
    "id": "a5bda1d6-6803-49d2-a893-e3cbd4d7507f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSpido_Idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 113,
    "bbox_left": 49,
    "bbox_right": 114,
    "bbox_top": 74,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9abf8d34-a00b-4984-b8c6-98ed754b4029",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5bda1d6-6803-49d2-a893-e3cbd4d7507f",
            "compositeImage": {
                "id": "42566b4e-dd56-4a0f-893c-20e99656f18e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9abf8d34-a00b-4984-b8c6-98ed754b4029",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "baa7dd3d-74d5-405d-b8da-4b759e777fd4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9abf8d34-a00b-4984-b8c6-98ed754b4029",
                    "LayerId": "77bf56da-0db3-488b-a2b4-9a45e3dcae18"
                }
            ]
        },
        {
            "id": "ec6b0033-50a9-4f05-a8b1-eba1d4211c0c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5bda1d6-6803-49d2-a893-e3cbd4d7507f",
            "compositeImage": {
                "id": "39623e0e-92bf-4b05-8b7d-7747bea34fe3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec6b0033-50a9-4f05-a8b1-eba1d4211c0c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c2ad188-0e93-420f-be5d-9f48ff6e27c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec6b0033-50a9-4f05-a8b1-eba1d4211c0c",
                    "LayerId": "77bf56da-0db3-488b-a2b4-9a45e3dcae18"
                }
            ]
        },
        {
            "id": "b04c91ef-629a-467f-aa3b-641da9932a82",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5bda1d6-6803-49d2-a893-e3cbd4d7507f",
            "compositeImage": {
                "id": "27f23938-9225-4611-b350-ee3d64b866f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b04c91ef-629a-467f-aa3b-641da9932a82",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9746dce9-fa51-41b4-8a7b-0d7624c601fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b04c91ef-629a-467f-aa3b-641da9932a82",
                    "LayerId": "77bf56da-0db3-488b-a2b4-9a45e3dcae18"
                }
            ]
        },
        {
            "id": "e74dda51-f515-4f37-9433-34d6dc2e0cba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5bda1d6-6803-49d2-a893-e3cbd4d7507f",
            "compositeImage": {
                "id": "0b40c956-18f1-4044-9657-9f5c3a217bbe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e74dda51-f515-4f37-9433-34d6dc2e0cba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f498fe0-4d94-4f93-ad74-5ec58ffd6cae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e74dda51-f515-4f37-9433-34d6dc2e0cba",
                    "LayerId": "77bf56da-0db3-488b-a2b4-9a45e3dcae18"
                }
            ]
        },
        {
            "id": "8089be0f-8eb6-49f1-a992-2c757fc35635",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5bda1d6-6803-49d2-a893-e3cbd4d7507f",
            "compositeImage": {
                "id": "e58a6a79-7a0d-40a4-9beb-5395e4ed05f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8089be0f-8eb6-49f1-a992-2c757fc35635",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d55fb987-e048-4dae-9c8a-a3da5c770539",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8089be0f-8eb6-49f1-a992-2c757fc35635",
                    "LayerId": "77bf56da-0db3-488b-a2b4-9a45e3dcae18"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "77bf56da-0db3-488b-a2b4-9a45e3dcae18",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a5bda1d6-6803-49d2-a893-e3cbd4d7507f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 162,
    "xorig": 20,
    "yorig": 40
}