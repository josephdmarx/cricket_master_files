{
    "id": "a8953ab6-c208-4fe7-885c-30b81b8b8b55",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Ground02_Water",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 271,
    "bbox_left": 43,
    "bbox_right": 788,
    "bbox_top": 32,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f9452b00-b573-40ff-999e-1e8f77bb698a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8953ab6-c208-4fe7-885c-30b81b8b8b55",
            "compositeImage": {
                "id": "ebf55304-3c24-4830-a79f-106d8f9ee9c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f9452b00-b573-40ff-999e-1e8f77bb698a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "76cdc869-d062-4549-b51f-a09e040f7dee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9452b00-b573-40ff-999e-1e8f77bb698a",
                    "LayerId": "2b1b96fb-7f3b-478d-83cf-a88983595f43"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "2b1b96fb-7f3b-478d-83cf-a88983595f43",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a8953ab6-c208-4fe7-885c-30b81b8b8b55",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1024,
    "xorig": -46,
    "yorig": 94
}