{
    "id": "a25c6947-6f99-469a-8899-463b38cc2b3d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sMapWorld",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 341,
    "bbox_left": 21,
    "bbox_right": 615,
    "bbox_top": 78,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a4c3413e-ead1-4402-af66-bf4528d636c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a25c6947-6f99-469a-8899-463b38cc2b3d",
            "compositeImage": {
                "id": "7805ec78-d1e0-44c0-a02f-8e9b058f201d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4c3413e-ead1-4402-af66-bf4528d636c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae39e30a-31d2-409f-a3a5-637dd3cf5021",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4c3413e-ead1-4402-af66-bf4528d636c4",
                    "LayerId": "5bd22026-6816-4c99-a457-17f2701abb7e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 360,
    "layers": [
        {
            "id": "5bd22026-6816-4c99-a457-17f2701abb7e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a25c6947-6f99-469a-8899-463b38cc2b3d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 0,
    "yorig": 0
}