{
    "id": "76e0ae00-46f9-464a-af1a-2992cf8f1298",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayerCast01",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 94,
    "bbox_left": 58,
    "bbox_right": 72,
    "bbox_top": 42,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5fbd3ed8-0ea1-4275-af07-3787d12bbbbb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76e0ae00-46f9-464a-af1a-2992cf8f1298",
            "compositeImage": {
                "id": "6ba88599-2a38-409f-9cd6-9d64b62282a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5fbd3ed8-0ea1-4275-af07-3787d12bbbbb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6cc94213-55f1-4c91-bdd9-1604ec7e3963",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5fbd3ed8-0ea1-4275-af07-3787d12bbbbb",
                    "LayerId": "d51eb8d3-3981-4b0d-9ca7-c9ddd4f23776"
                }
            ]
        },
        {
            "id": "bbe9779a-f08e-41e6-8101-040080531dc0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76e0ae00-46f9-464a-af1a-2992cf8f1298",
            "compositeImage": {
                "id": "6d1bac7f-20f7-4383-b5ef-2f8d75afd226",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bbe9779a-f08e-41e6-8101-040080531dc0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b2e7aeb-bf6b-49c4-b620-4fe7bb9928c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bbe9779a-f08e-41e6-8101-040080531dc0",
                    "LayerId": "d51eb8d3-3981-4b0d-9ca7-c9ddd4f23776"
                }
            ]
        },
        {
            "id": "79f41643-6cbf-403d-adb8-1f46008f8c7d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76e0ae00-46f9-464a-af1a-2992cf8f1298",
            "compositeImage": {
                "id": "3bae6b69-a602-4b68-b4a0-24a6189d9d3b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "79f41643-6cbf-403d-adb8-1f46008f8c7d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d9ff0ce-8108-4fde-819d-9b41c5a49471",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79f41643-6cbf-403d-adb8-1f46008f8c7d",
                    "LayerId": "d51eb8d3-3981-4b0d-9ca7-c9ddd4f23776"
                }
            ]
        },
        {
            "id": "b727e9dd-75a2-4229-9c7b-12632c96e1a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76e0ae00-46f9-464a-af1a-2992cf8f1298",
            "compositeImage": {
                "id": "05fd6dd6-edbb-41c4-ae8a-029a99c9816c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b727e9dd-75a2-4229-9c7b-12632c96e1a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d91d1f44-3f63-400b-9c11-cdc64db00982",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b727e9dd-75a2-4229-9c7b-12632c96e1a6",
                    "LayerId": "d51eb8d3-3981-4b0d-9ca7-c9ddd4f23776"
                }
            ]
        },
        {
            "id": "53d96366-47d0-4d5f-8625-b71dc2da3e8a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76e0ae00-46f9-464a-af1a-2992cf8f1298",
            "compositeImage": {
                "id": "fef7517f-b92d-47b2-89c4-a65f4301e89e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "53d96366-47d0-4d5f-8625-b71dc2da3e8a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f1fe5a17-cdbe-4d0e-8bbc-fff3f60aa21a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "53d96366-47d0-4d5f-8625-b71dc2da3e8a",
                    "LayerId": "d51eb8d3-3981-4b0d-9ca7-c9ddd4f23776"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "d51eb8d3-3981-4b0d-9ca7-c9ddd4f23776",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "76e0ae00-46f9-464a-af1a-2992cf8f1298",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}