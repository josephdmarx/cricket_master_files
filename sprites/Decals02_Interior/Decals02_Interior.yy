{
    "id": "ef5b639e-5942-4eae-9192-5b656586326b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Decals02_Interior",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 447,
    "bbox_left": 16,
    "bbox_right": 491,
    "bbox_top": 19,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "835211fb-249c-4049-bc37-d873f0d15b8d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ef5b639e-5942-4eae-9192-5b656586326b",
            "compositeImage": {
                "id": "05cf69ce-c955-4429-ba61-596893ff7003",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "835211fb-249c-4049-bc37-d873f0d15b8d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "687fcf70-0ba2-4e00-9b04-63b02f5a910f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "835211fb-249c-4049-bc37-d873f0d15b8d",
                    "LayerId": "1155dcfb-2268-4123-af72-3ba379389c04"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "1155dcfb-2268-4123-af72-3ba379389c04",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ef5b639e-5942-4eae-9192-5b656586326b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 512,
    "xorig": 0,
    "yorig": 0
}