{
    "id": "ccb81f2c-27d0-4d9c-bb2c-e0b913017a13",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sDialogueBox",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 103,
    "bbox_left": 0,
    "bbox_right": 239,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "20a435f6-4610-4d9f-853e-3ece30596ce1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ccb81f2c-27d0-4d9c-bb2c-e0b913017a13",
            "compositeImage": {
                "id": "604269d5-0393-4672-96dc-3a0fb75bf90e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "20a435f6-4610-4d9f-853e-3ece30596ce1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9865f96-1e9e-40a4-ba05-0c03c69705c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "20a435f6-4610-4d9f-853e-3ece30596ce1",
                    "LayerId": "d2053403-8fd6-4bbf-9cb4-f50d67d9811b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 104,
    "layers": [
        {
            "id": "d2053403-8fd6-4bbf-9cb4-f50d67d9811b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ccb81f2c-27d0-4d9c-bb2c-e0b913017a13",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 240,
    "xorig": 0,
    "yorig": 41
}