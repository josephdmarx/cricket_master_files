{
    "id": "e0d44475-03fe-4cc9-a038-a9724944449c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sHealPot01",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 12,
    "bbox_right": 19,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7e461134-29b1-459f-84f2-2a16ced87e0d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e0d44475-03fe-4cc9-a038-a9724944449c",
            "compositeImage": {
                "id": "410d60b4-0391-4ee7-9ece-30b0cdf835c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e461134-29b1-459f-84f2-2a16ced87e0d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2cc50ded-b831-46ed-9e1e-791e29441f65",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e461134-29b1-459f-84f2-2a16ced87e0d",
                    "LayerId": "646b292f-b359-4371-b6ad-2667168865a0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "646b292f-b359-4371-b6ad-2667168865a0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e0d44475-03fe-4cc9-a038-a9724944449c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}