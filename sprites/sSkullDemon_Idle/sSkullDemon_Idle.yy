{
    "id": "caa31944-0ea7-469b-8ec4-5ff707e7f7ec",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSkullDemon_Idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 48,
    "bbox_right": 80,
    "bbox_top": 65,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "863d268d-7c6d-4b47-b828-eb4814d4184f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "caa31944-0ea7-469b-8ec4-5ff707e7f7ec",
            "compositeImage": {
                "id": "d5df0dda-893d-4b04-8dff-7d38d7e85e88",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "863d268d-7c6d-4b47-b828-eb4814d4184f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f3bbae43-e81d-4d82-bea1-09f44319cb95",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "863d268d-7c6d-4b47-b828-eb4814d4184f",
                    "LayerId": "6f696144-4094-422a-9a9a-3d2259896776"
                }
            ]
        },
        {
            "id": "76c4fed1-9ae2-43fc-bcaf-2a460cca24bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "caa31944-0ea7-469b-8ec4-5ff707e7f7ec",
            "compositeImage": {
                "id": "ac2a52a0-fc9a-4f56-956a-f0a669b28741",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76c4fed1-9ae2-43fc-bcaf-2a460cca24bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f69bb68-2956-4a5a-9abb-baeddb352506",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76c4fed1-9ae2-43fc-bcaf-2a460cca24bd",
                    "LayerId": "6f696144-4094-422a-9a9a-3d2259896776"
                }
            ]
        },
        {
            "id": "49e6ed0e-c07d-4f66-b9d2-daba08ad6be1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "caa31944-0ea7-469b-8ec4-5ff707e7f7ec",
            "compositeImage": {
                "id": "2917ddbb-2b06-4aa5-9505-802398590d6e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "49e6ed0e-c07d-4f66-b9d2-daba08ad6be1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0605c92d-62e1-4dab-aa0d-94340e8faf43",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49e6ed0e-c07d-4f66-b9d2-daba08ad6be1",
                    "LayerId": "6f696144-4094-422a-9a9a-3d2259896776"
                }
            ]
        },
        {
            "id": "456b16b1-9c7c-4298-84d9-b7c37da2f2fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "caa31944-0ea7-469b-8ec4-5ff707e7f7ec",
            "compositeImage": {
                "id": "558ae456-e08c-4e8c-8784-d28176444da9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "456b16b1-9c7c-4298-84d9-b7c37da2f2fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd6fa6cb-bfb3-432e-b23f-f0d41cdd73cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "456b16b1-9c7c-4298-84d9-b7c37da2f2fc",
                    "LayerId": "6f696144-4094-422a-9a9a-3d2259896776"
                }
            ]
        },
        {
            "id": "b6103720-ac75-4b8c-a185-35bd254db450",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "caa31944-0ea7-469b-8ec4-5ff707e7f7ec",
            "compositeImage": {
                "id": "72b8f403-8852-45b3-ab8b-079f16785150",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6103720-ac75-4b8c-a185-35bd254db450",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d49a1ed3-7543-4fe0-94e1-73e2c00ca833",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6103720-ac75-4b8c-a185-35bd254db450",
                    "LayerId": "6f696144-4094-422a-9a9a-3d2259896776"
                }
            ]
        },
        {
            "id": "eab65c6a-32d2-41b2-8636-9d8d5fc494bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "caa31944-0ea7-469b-8ec4-5ff707e7f7ec",
            "compositeImage": {
                "id": "9019b361-658c-4eb1-903c-ed055cc3cb46",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eab65c6a-32d2-41b2-8636-9d8d5fc494bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "656045cd-f227-4965-b190-50a89aed61eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eab65c6a-32d2-41b2-8636-9d8d5fc494bb",
                    "LayerId": "6f696144-4094-422a-9a9a-3d2259896776"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "6f696144-4094-422a-9a9a-3d2259896776",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "caa31944-0ea7-469b-8ec4-5ff707e7f7ec",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}