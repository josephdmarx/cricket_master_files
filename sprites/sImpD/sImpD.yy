{
    "id": "bc1ab8c2-039b-4ad8-b929-3c38779558eb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sImpD",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 43,
    "bbox_left": 13,
    "bbox_right": 26,
    "bbox_top": 28,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "78ca6080-6730-413e-8afd-a4ab22d21984",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bc1ab8c2-039b-4ad8-b929-3c38779558eb",
            "compositeImage": {
                "id": "2f909dad-8a46-4417-97fa-97f98b4571cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78ca6080-6730-413e-8afd-a4ab22d21984",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c67fd84e-1726-4157-8511-45c156843a3c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78ca6080-6730-413e-8afd-a4ab22d21984",
                    "LayerId": "6e68c212-d617-4a54-917d-42939b272e30"
                }
            ]
        },
        {
            "id": "83d1b369-7a0f-4a30-8d4c-3209be4173ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bc1ab8c2-039b-4ad8-b929-3c38779558eb",
            "compositeImage": {
                "id": "a104f4e3-4a85-4e80-b4fe-c2e557dd1773",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "83d1b369-7a0f-4a30-8d4c-3209be4173ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a9bbd64-bf81-4ac6-b0fa-8782cb37b0ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "83d1b369-7a0f-4a30-8d4c-3209be4173ea",
                    "LayerId": "6e68c212-d617-4a54-917d-42939b272e30"
                }
            ]
        },
        {
            "id": "44543b0c-44b8-431e-88a5-2b98dffa5367",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bc1ab8c2-039b-4ad8-b929-3c38779558eb",
            "compositeImage": {
                "id": "21eee372-e393-4d49-b17a-52115a51f23a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "44543b0c-44b8-431e-88a5-2b98dffa5367",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "043d55a6-451b-4316-a456-f871310365b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "44543b0c-44b8-431e-88a5-2b98dffa5367",
                    "LayerId": "6e68c212-d617-4a54-917d-42939b272e30"
                }
            ]
        },
        {
            "id": "7a85f070-6ba4-479b-a64d-b8e8a0292db1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bc1ab8c2-039b-4ad8-b929-3c38779558eb",
            "compositeImage": {
                "id": "9456b470-e0f9-474e-b3c5-c7331f8ba417",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a85f070-6ba4-479b-a64d-b8e8a0292db1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e7239dc6-8cd9-472d-a044-6f04286a6f69",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a85f070-6ba4-479b-a64d-b8e8a0292db1",
                    "LayerId": "6e68c212-d617-4a54-917d-42939b272e30"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "6e68c212-d617-4a54-917d-42939b272e30",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bc1ab8c2-039b-4ad8-b929-3c38779558eb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 31,
    "yorig": 40
}