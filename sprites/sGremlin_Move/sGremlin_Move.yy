{
    "id": "c580a0f7-4476-4346-9505-8684fda3c978",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGremlin_Move",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 13,
    "bbox_right": 43,
    "bbox_top": 7,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a8706151-59c3-4029-bbaa-59662f79f486",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c580a0f7-4476-4346-9505-8684fda3c978",
            "compositeImage": {
                "id": "c3331b83-03c8-4f06-8021-064ea6a26f94",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a8706151-59c3-4029-bbaa-59662f79f486",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "845d64bd-ae27-4c26-bbcb-ee1bf261b516",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8706151-59c3-4029-bbaa-59662f79f486",
                    "LayerId": "352a03ad-3a04-411b-9264-e43a4a4e0510"
                }
            ]
        },
        {
            "id": "5cffbe50-66e8-43af-8556-796f7061211d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c580a0f7-4476-4346-9505-8684fda3c978",
            "compositeImage": {
                "id": "b9c77ffa-0479-44d1-a483-6717aec39775",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5cffbe50-66e8-43af-8556-796f7061211d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a2a00a91-f0b8-4444-a40e-23c8fba657fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5cffbe50-66e8-43af-8556-796f7061211d",
                    "LayerId": "352a03ad-3a04-411b-9264-e43a4a4e0510"
                }
            ]
        },
        {
            "id": "f5d6cb4a-2964-427e-bb65-c942dc1ed88f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c580a0f7-4476-4346-9505-8684fda3c978",
            "compositeImage": {
                "id": "99908c08-e672-4ab7-9fad-f64a9a9b8dcb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5d6cb4a-2964-427e-bb65-c942dc1ed88f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "90f4afbe-9770-421e-888b-af8885d8619a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5d6cb4a-2964-427e-bb65-c942dc1ed88f",
                    "LayerId": "352a03ad-3a04-411b-9264-e43a4a4e0510"
                }
            ]
        },
        {
            "id": "ac52cdef-3b69-4bf3-9151-7122b2eb543a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c580a0f7-4476-4346-9505-8684fda3c978",
            "compositeImage": {
                "id": "03bfc5ca-d46c-4402-81a6-84643fed9867",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac52cdef-3b69-4bf3-9151-7122b2eb543a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a68f796e-bb8d-4ae9-b470-4098dbfd9ec1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac52cdef-3b69-4bf3-9151-7122b2eb543a",
                    "LayerId": "352a03ad-3a04-411b-9264-e43a4a4e0510"
                }
            ]
        },
        {
            "id": "fe897cd7-0549-4a0d-aad1-82d2fb4e0b0f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c580a0f7-4476-4346-9505-8684fda3c978",
            "compositeImage": {
                "id": "6bd87830-a988-4129-8f0d-e169363ccc77",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe897cd7-0549-4a0d-aad1-82d2fb4e0b0f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "686b8722-1f60-4340-8da4-4679addea166",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe897cd7-0549-4a0d-aad1-82d2fb4e0b0f",
                    "LayerId": "352a03ad-3a04-411b-9264-e43a4a4e0510"
                }
            ]
        },
        {
            "id": "32bf9529-fdd8-4ef9-96b4-c4d71fc8a8f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c580a0f7-4476-4346-9505-8684fda3c978",
            "compositeImage": {
                "id": "4d259fb7-7573-4f2e-a0a7-c2b0452c9d6e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "32bf9529-fdd8-4ef9-96b4-c4d71fc8a8f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5eeb235b-8e71-43dd-b255-6534a8a954c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32bf9529-fdd8-4ef9-96b4-c4d71fc8a8f0",
                    "LayerId": "352a03ad-3a04-411b-9264-e43a4a4e0510"
                }
            ]
        },
        {
            "id": "75e560bb-4133-4f2e-a8bf-fcd9c088615f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c580a0f7-4476-4346-9505-8684fda3c978",
            "compositeImage": {
                "id": "a262aab1-4a41-4443-9b8c-3a5ab18eb3c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "75e560bb-4133-4f2e-a8bf-fcd9c088615f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fdc9ba74-8682-4d46-ba95-1afedc023cd0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "75e560bb-4133-4f2e-a8bf-fcd9c088615f",
                    "LayerId": "352a03ad-3a04-411b-9264-e43a4a4e0510"
                }
            ]
        },
        {
            "id": "669e8176-7eee-4612-98c6-49964caeb599",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c580a0f7-4476-4346-9505-8684fda3c978",
            "compositeImage": {
                "id": "646c1f08-a985-4cae-ac62-f737936a5ea3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "669e8176-7eee-4612-98c6-49964caeb599",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c7f3c4c-30dc-49a0-b2ed-245320d71197",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "669e8176-7eee-4612-98c6-49964caeb599",
                    "LayerId": "352a03ad-3a04-411b-9264-e43a4a4e0510"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "352a03ad-3a04-411b-9264-e43a4a4e0510",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c580a0f7-4476-4346-9505-8684fda3c978",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 77,
    "xorig": 30,
    "yorig": 25
}