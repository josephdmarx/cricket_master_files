{
    "id": "e8ea25d4-ec32-44b5-a6c4-8b86af109e74",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCricketPort",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 3,
    "bbox_right": 89,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": true,
    "frames": [
        {
            "id": "cb8809c4-01ad-4d36-9e1b-733fe92b3605",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8ea25d4-ec32-44b5-a6c4-8b86af109e74",
            "compositeImage": {
                "id": "4d599ec0-1613-41e2-bf05-fd2ff203f284",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb8809c4-01ad-4d36-9e1b-733fe92b3605",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "804fafcb-6e12-485b-bd14-d94ceaa11e7c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb8809c4-01ad-4d36-9e1b-733fe92b3605",
                    "LayerId": "7b6121ef-5968-437d-9209-b2298e5db5b8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "7b6121ef-5968-437d-9209-b2298e5db5b8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e8ea25d4-ec32-44b5-a6c4-8b86af109e74",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": true,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 0,
    "yorig": 0
}