{
    "id": "609f408e-380e-40aa-a352-ae82c044ef17",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sImp",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 43,
    "bbox_left": 13,
    "bbox_right": 26,
    "bbox_top": 28,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f9f79bdb-a7b2-4b56-b2b4-427b8c126bab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "609f408e-380e-40aa-a352-ae82c044ef17",
            "compositeImage": {
                "id": "b4ebc782-c990-4620-8f55-01bf2fa433c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f9f79bdb-a7b2-4b56-b2b4-427b8c126bab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f832de4-f82a-44d5-a06a-e0a5777afbb5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9f79bdb-a7b2-4b56-b2b4-427b8c126bab",
                    "LayerId": "00f847bb-1c9a-41e1-b842-c0ef7f55e4ab"
                }
            ]
        },
        {
            "id": "fba02b44-7448-4be8-95a5-f70e113e3917",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "609f408e-380e-40aa-a352-ae82c044ef17",
            "compositeImage": {
                "id": "7c6e5efb-bc04-47ff-92a9-a693166db7ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fba02b44-7448-4be8-95a5-f70e113e3917",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "561baea5-355c-40eb-8bf7-e62cad9bda8c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fba02b44-7448-4be8-95a5-f70e113e3917",
                    "LayerId": "00f847bb-1c9a-41e1-b842-c0ef7f55e4ab"
                }
            ]
        },
        {
            "id": "23faa49d-7e3d-4358-9e62-d6264752116e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "609f408e-380e-40aa-a352-ae82c044ef17",
            "compositeImage": {
                "id": "5fd4e749-70e1-4127-b029-c47b743b2d3b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "23faa49d-7e3d-4358-9e62-d6264752116e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4edee995-4b01-4f5c-88f7-e245fbd4bf17",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "23faa49d-7e3d-4358-9e62-d6264752116e",
                    "LayerId": "00f847bb-1c9a-41e1-b842-c0ef7f55e4ab"
                }
            ]
        },
        {
            "id": "2b46c8d4-0c03-4309-a534-d6e9306e8828",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "609f408e-380e-40aa-a352-ae82c044ef17",
            "compositeImage": {
                "id": "c6956215-4de2-41bf-b19b-b80e8db81997",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b46c8d4-0c03-4309-a534-d6e9306e8828",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f03898b-e852-4452-b9ad-a83faf3572b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b46c8d4-0c03-4309-a534-d6e9306e8828",
                    "LayerId": "00f847bb-1c9a-41e1-b842-c0ef7f55e4ab"
                }
            ]
        },
        {
            "id": "d476bee6-57c9-4e72-9a12-dbd0da412269",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "609f408e-380e-40aa-a352-ae82c044ef17",
            "compositeImage": {
                "id": "cdfbc09f-9d7b-4e18-9170-4d95a876be47",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d476bee6-57c9-4e72-9a12-dbd0da412269",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57c453a0-a710-473e-b131-af116c297eca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d476bee6-57c9-4e72-9a12-dbd0da412269",
                    "LayerId": "00f847bb-1c9a-41e1-b842-c0ef7f55e4ab"
                }
            ]
        },
        {
            "id": "01c4110e-b526-4511-815f-fef4b8ae677b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "609f408e-380e-40aa-a352-ae82c044ef17",
            "compositeImage": {
                "id": "1bb756f6-6781-476f-a935-fbdb3ddb573a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01c4110e-b526-4511-815f-fef4b8ae677b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d0a7749c-647f-4af2-99f4-1ebc80b4cf06",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01c4110e-b526-4511-815f-fef4b8ae677b",
                    "LayerId": "00f847bb-1c9a-41e1-b842-c0ef7f55e4ab"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 58,
    "layers": [
        {
            "id": "00f847bb-1c9a-41e1-b842-c0ef7f55e4ab",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "609f408e-380e-40aa-a352-ae82c044ef17",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 41,
    "xorig": 20,
    "yorig": 40
}