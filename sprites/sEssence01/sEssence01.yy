{
    "id": "b084fcb0-2841-4df5-92e5-3425b86550f7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sEssence01",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 56,
    "bbox_left": 5,
    "bbox_right": 24,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f2f1d3da-58ea-4214-81da-d81a4ebe4833",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b084fcb0-2841-4df5-92e5-3425b86550f7",
            "compositeImage": {
                "id": "a7779c7d-8ebc-4fdd-92ea-8ca732f506c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f2f1d3da-58ea-4214-81da-d81a4ebe4833",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df3ba8da-ea57-44f6-86b7-15441b142899",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f2f1d3da-58ea-4214-81da-d81a4ebe4833",
                    "LayerId": "698dc6c2-a38e-4185-bd16-1992ffb5b08e"
                }
            ]
        },
        {
            "id": "ca67751a-1d40-447c-9e0b-147967a69f38",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b084fcb0-2841-4df5-92e5-3425b86550f7",
            "compositeImage": {
                "id": "6d33437a-8448-4538-bbad-f658cdbb1418",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca67751a-1d40-447c-9e0b-147967a69f38",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "460f892b-daa6-49c5-b95c-417f4788f74b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca67751a-1d40-447c-9e0b-147967a69f38",
                    "LayerId": "698dc6c2-a38e-4185-bd16-1992ffb5b08e"
                }
            ]
        },
        {
            "id": "3e532bcb-038d-4eff-ac22-1288b43ff47a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b084fcb0-2841-4df5-92e5-3425b86550f7",
            "compositeImage": {
                "id": "f704b8fd-77fa-4a22-bd8d-826f74cca457",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e532bcb-038d-4eff-ac22-1288b43ff47a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "78b3b68f-f979-48c6-8072-82bcccc71f01",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e532bcb-038d-4eff-ac22-1288b43ff47a",
                    "LayerId": "698dc6c2-a38e-4185-bd16-1992ffb5b08e"
                }
            ]
        },
        {
            "id": "4224947e-1be8-4607-9572-8aacf6678900",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b084fcb0-2841-4df5-92e5-3425b86550f7",
            "compositeImage": {
                "id": "6368ea41-cf7b-4edc-b90c-a221dcf32ede",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4224947e-1be8-4607-9572-8aacf6678900",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c223c901-1571-4826-8431-c818a5ab926a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4224947e-1be8-4607-9572-8aacf6678900",
                    "LayerId": "698dc6c2-a38e-4185-bd16-1992ffb5b08e"
                }
            ]
        },
        {
            "id": "2935a46d-5a72-4a4b-b9e5-b27d9625f29b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b084fcb0-2841-4df5-92e5-3425b86550f7",
            "compositeImage": {
                "id": "aaca79e5-b655-42d7-852a-986d6936498f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2935a46d-5a72-4a4b-b9e5-b27d9625f29b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3702140-6df9-4ac2-af11-8e714e20d071",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2935a46d-5a72-4a4b-b9e5-b27d9625f29b",
                    "LayerId": "698dc6c2-a38e-4185-bd16-1992ffb5b08e"
                }
            ]
        },
        {
            "id": "9dded4a4-dd92-4829-899a-21fbc2f923e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b084fcb0-2841-4df5-92e5-3425b86550f7",
            "compositeImage": {
                "id": "cd14f31f-337f-4c69-bfeb-5c902eba5809",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9dded4a4-dd92-4829-899a-21fbc2f923e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca215332-6db9-4eac-9318-37e56f04c3c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9dded4a4-dd92-4829-899a-21fbc2f923e2",
                    "LayerId": "698dc6c2-a38e-4185-bd16-1992ffb5b08e"
                }
            ]
        },
        {
            "id": "f08e9c0f-4dda-4c9c-ab77-dfe7c665a3bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b084fcb0-2841-4df5-92e5-3425b86550f7",
            "compositeImage": {
                "id": "3b4e275b-5a0c-430e-9d50-4f8986282dcc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f08e9c0f-4dda-4c9c-ab77-dfe7c665a3bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be073437-e6e0-4408-b493-968bff410aa2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f08e9c0f-4dda-4c9c-ab77-dfe7c665a3bc",
                    "LayerId": "698dc6c2-a38e-4185-bd16-1992ffb5b08e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "698dc6c2-a38e-4185-bd16-1992ffb5b08e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b084fcb0-2841-4df5-92e5-3425b86550f7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 43
}