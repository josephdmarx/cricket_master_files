{
    "id": "0bd4e305-e3fa-431a-b60e-4bd4cfa4b817",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sMenuBackground",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 341,
    "bbox_left": 21,
    "bbox_right": 615,
    "bbox_top": 78,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8e9f3977-a871-4a39-ab50-8702e716fd64",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bd4e305-e3fa-431a-b60e-4bd4cfa4b817",
            "compositeImage": {
                "id": "d7769dee-aa5c-4bdd-b4f4-d3c579ecc597",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e9f3977-a871-4a39-ab50-8702e716fd64",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "31f38b11-85cd-4171-a47a-029d0cba5755",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e9f3977-a871-4a39-ab50-8702e716fd64",
                    "LayerId": "9414d8b0-ada9-4596-a5d5-43eef9833748"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 360,
    "layers": [
        {
            "id": "9414d8b0-ada9-4596-a5d5-43eef9833748",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0bd4e305-e3fa-431a-b60e-4bd4cfa4b817",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 0,
    "yorig": 0
}