{
    "id": "ed8ba7a5-b892-41dc-a957-b969a62c8f7d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "background_example",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1079,
    "bbox_left": 0,
    "bbox_right": 1919,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "aaf37786-f516-4544-a578-ecca68096fbb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ed8ba7a5-b892-41dc-a957-b969a62c8f7d",
            "compositeImage": {
                "id": "02525524-6768-4bd0-90b3-93b3e343a1c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aaf37786-f516-4544-a578-ecca68096fbb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "408a9462-2ba8-494d-affd-606de10e827e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aaf37786-f516-4544-a578-ecca68096fbb",
                    "LayerId": "572d2b4b-244d-412c-b6b1-32be3dcb3efa"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1080,
    "layers": [
        {
            "id": "572d2b4b-244d-412c-b6b1-32be3dcb3efa",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ed8ba7a5-b892-41dc-a957-b969a62c8f7d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1920,
    "xorig": 0,
    "yorig": 0
}