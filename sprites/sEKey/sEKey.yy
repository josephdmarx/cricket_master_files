{
    "id": "b41051b7-c405-4300-bca4-77376f56558a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sEKey",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 25,
    "bbox_left": 5,
    "bbox_right": 26,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "50028491-942f-4fb5-9325-c7d1c42607ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b41051b7-c405-4300-bca4-77376f56558a",
            "compositeImage": {
                "id": "4843e06d-eb40-4be7-ac55-ac2e3171f214",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "50028491-942f-4fb5-9325-c7d1c42607ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc9a4adb-72b0-4f32-b5ba-93b7847bf1dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "50028491-942f-4fb5-9325-c7d1c42607ad",
                    "LayerId": "20600aca-57c9-4569-b17e-9e51ed894349"
                }
            ]
        },
        {
            "id": "6c182369-539d-4ff6-a7bd-0497bd9ad15f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b41051b7-c405-4300-bca4-77376f56558a",
            "compositeImage": {
                "id": "160572f9-af16-4139-b90f-5a04d0b20831",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c182369-539d-4ff6-a7bd-0497bd9ad15f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "312b8524-a48e-4467-ae4d-f9aaf12df9dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c182369-539d-4ff6-a7bd-0497bd9ad15f",
                    "LayerId": "20600aca-57c9-4569-b17e-9e51ed894349"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "20600aca-57c9-4569-b17e-9e51ed894349",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b41051b7-c405-4300-bca4-77376f56558a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 15,
    "yorig": 26
}