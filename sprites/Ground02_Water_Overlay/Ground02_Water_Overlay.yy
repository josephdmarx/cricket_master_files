{
    "id": "e67ac908-9078-4d20-a03a-d1d6932192e2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Ground02_Water_Overlay",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 271,
    "bbox_left": 43,
    "bbox_right": 788,
    "bbox_top": 32,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f1f9ab10-b523-4c3c-95be-6be2146fe71d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e67ac908-9078-4d20-a03a-d1d6932192e2",
            "compositeImage": {
                "id": "43b394b7-586b-44ee-8eee-4278f9b7861a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1f9ab10-b523-4c3c-95be-6be2146fe71d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c826b3ae-fc31-49a0-a567-8c2fa24b0a58",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1f9ab10-b523-4c3c-95be-6be2146fe71d",
                    "LayerId": "d302eb1f-ea8e-48c0-9792-150c7743c3ed"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "d302eb1f-ea8e-48c0-9792-150c7743c3ed",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e67ac908-9078-4d20-a03a-d1d6932192e2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1024,
    "xorig": -46,
    "yorig": 94
}