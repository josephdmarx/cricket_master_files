{
    "id": "9d9e4378-acc5-459c-bd35-9e3cfea0fa02",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayerAbsorb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 38,
    "bbox_right": 79,
    "bbox_top": 35,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d9328439-8ea4-4f80-ab56-cabb476d00e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9d9e4378-acc5-459c-bd35-9e3cfea0fa02",
            "compositeImage": {
                "id": "cb711982-c497-40e9-8e07-8f07185de358",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d9328439-8ea4-4f80-ab56-cabb476d00e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "23636206-fed5-463b-9c22-a0b62a249406",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9328439-8ea4-4f80-ab56-cabb476d00e3",
                    "LayerId": "8ed98e7d-5e95-4cb9-8f31-9303b0e57985"
                }
            ]
        },
        {
            "id": "2527b96e-374d-4116-9c18-76e5b2ad7d18",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9d9e4378-acc5-459c-bd35-9e3cfea0fa02",
            "compositeImage": {
                "id": "f7241d77-00c9-4394-ba1b-9ee0c51e5221",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2527b96e-374d-4116-9c18-76e5b2ad7d18",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25a1901e-d1ce-4a3d-84a1-8e4c572be60e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2527b96e-374d-4116-9c18-76e5b2ad7d18",
                    "LayerId": "8ed98e7d-5e95-4cb9-8f31-9303b0e57985"
                }
            ]
        },
        {
            "id": "6e052114-2f40-4ffa-afa3-08fef34db87f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9d9e4378-acc5-459c-bd35-9e3cfea0fa02",
            "compositeImage": {
                "id": "d3d997bd-9260-4679-83e5-9247ecd97ef9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e052114-2f40-4ffa-afa3-08fef34db87f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c4afa4b-cf8f-4278-ad26-9117d446dfbc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e052114-2f40-4ffa-afa3-08fef34db87f",
                    "LayerId": "8ed98e7d-5e95-4cb9-8f31-9303b0e57985"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "8ed98e7d-5e95-4cb9-8f31-9303b0e57985",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9d9e4378-acc5-459c-bd35-9e3cfea0fa02",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}