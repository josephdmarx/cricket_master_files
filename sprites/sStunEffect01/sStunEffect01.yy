{
    "id": "48433fae-67f8-42d3-b2f7-72118fc079cc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sStunEffect01",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 1,
    "bbox_right": 30,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f1d68da8-136a-46e0-9233-0de0c1015bad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48433fae-67f8-42d3-b2f7-72118fc079cc",
            "compositeImage": {
                "id": "ad33034e-6a98-467d-85eb-823f66602617",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1d68da8-136a-46e0-9233-0de0c1015bad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1bfb91b2-b037-4670-b116-3b16e713c5c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1d68da8-136a-46e0-9233-0de0c1015bad",
                    "LayerId": "a2962da2-7ff0-4663-9d65-fcb04a8c2d94"
                }
            ]
        },
        {
            "id": "cd997104-faa9-4eeb-98c5-f952d6770144",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48433fae-67f8-42d3-b2f7-72118fc079cc",
            "compositeImage": {
                "id": "a4e40616-527b-49ab-b2b8-023f7ebeee91",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd997104-faa9-4eeb-98c5-f952d6770144",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7efbdc2b-c8bc-4458-be45-31453966988b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd997104-faa9-4eeb-98c5-f952d6770144",
                    "LayerId": "a2962da2-7ff0-4663-9d65-fcb04a8c2d94"
                }
            ]
        },
        {
            "id": "f2a139f8-58c6-4eb2-aa82-41f4abb6db50",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48433fae-67f8-42d3-b2f7-72118fc079cc",
            "compositeImage": {
                "id": "4f7ee499-931c-472c-b7bf-a64d32b19099",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f2a139f8-58c6-4eb2-aa82-41f4abb6db50",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2709636e-d714-4595-b155-5fafa6bcd2db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f2a139f8-58c6-4eb2-aa82-41f4abb6db50",
                    "LayerId": "a2962da2-7ff0-4663-9d65-fcb04a8c2d94"
                }
            ]
        },
        {
            "id": "43b1ba16-0f74-49f3-92d0-16060a8f3c31",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48433fae-67f8-42d3-b2f7-72118fc079cc",
            "compositeImage": {
                "id": "74ef6a8a-c4cb-4da0-be87-94b131f34dc6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43b1ba16-0f74-49f3-92d0-16060a8f3c31",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e176702c-7e1a-4c0a-a417-2967d8f7eddd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43b1ba16-0f74-49f3-92d0-16060a8f3c31",
                    "LayerId": "a2962da2-7ff0-4663-9d65-fcb04a8c2d94"
                }
            ]
        },
        {
            "id": "87075cfc-113b-4b71-9022-b3ef73fbb343",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48433fae-67f8-42d3-b2f7-72118fc079cc",
            "compositeImage": {
                "id": "7b9f49ca-515b-434b-840d-eb06dc9a3b9c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87075cfc-113b-4b71-9022-b3ef73fbb343",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b14f20ce-15b2-40db-b171-afd5f5100cb5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87075cfc-113b-4b71-9022-b3ef73fbb343",
                    "LayerId": "a2962da2-7ff0-4663-9d65-fcb04a8c2d94"
                }
            ]
        },
        {
            "id": "0f04da63-9083-4e86-a7e8-9ba390448451",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48433fae-67f8-42d3-b2f7-72118fc079cc",
            "compositeImage": {
                "id": "dccf5284-7d9d-462b-af3f-2f5c0d8af49b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f04da63-9083-4e86-a7e8-9ba390448451",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d60b935-fad4-4313-862c-064b73086fa1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f04da63-9083-4e86-a7e8-9ba390448451",
                    "LayerId": "a2962da2-7ff0-4663-9d65-fcb04a8c2d94"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "a2962da2-7ff0-4663-9d65-fcb04a8c2d94",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "48433fae-67f8-42d3-b2f7-72118fc079cc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}