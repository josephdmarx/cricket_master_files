{
    "id": "d01f282a-5cdc-4f43-a1bb-ec278015fdb8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sWaspStinger",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 3,
    "bbox_right": 29,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9723dea3-47fb-4dbf-bc0e-21f2a2edca3e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d01f282a-5cdc-4f43-a1bb-ec278015fdb8",
            "compositeImage": {
                "id": "13ccf29c-002a-484c-ba49-a8bc7806dabf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9723dea3-47fb-4dbf-bc0e-21f2a2edca3e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d7fee33-ebe5-479f-9dfa-73373671b8a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9723dea3-47fb-4dbf-bc0e-21f2a2edca3e",
                    "LayerId": "3f7d5da4-4aae-44db-a727-ac7123c8d3e6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "3f7d5da4-4aae-44db-a727-ac7123c8d3e6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d01f282a-5cdc-4f43-a1bb-ec278015fdb8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}