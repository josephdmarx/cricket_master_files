{
    "id": "4f5d5632-5228-4757-b850-19f179a3db5b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Ground01_Overlays",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 143,
    "bbox_left": 43,
    "bbox_right": 84,
    "bbox_top": 64,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "16f81f0d-b32b-4636-bd41-ba8e4fa4ac4e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f5d5632-5228-4757-b850-19f179a3db5b",
            "compositeImage": {
                "id": "7cd1dec3-be71-4459-aecb-30c8591e0a66",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "16f81f0d-b32b-4636-bd41-ba8e4fa4ac4e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee452150-a688-464a-b8c6-05604ff76101",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "16f81f0d-b32b-4636-bd41-ba8e4fa4ac4e",
                    "LayerId": "a9d30cf1-4633-4318-8d5c-def7762c568d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "a9d30cf1-4633-4318-8d5c-def7762c568d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4f5d5632-5228-4757-b850-19f179a3db5b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 512,
    "xorig": -46,
    "yorig": 94
}