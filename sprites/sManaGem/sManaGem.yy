{
    "id": "798c4d6f-4a22-4d0d-8f44-11e705cae995",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sManaGem",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 10,
    "bbox_right": 21,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8f7747f8-227c-4541-93ef-862cb88a0e04",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "798c4d6f-4a22-4d0d-8f44-11e705cae995",
            "compositeImage": {
                "id": "a8e00d2d-a997-413b-bdb1-d75b7033789a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f7747f8-227c-4541-93ef-862cb88a0e04",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d318004-f38a-452f-ba00-a7bacea00d06",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f7747f8-227c-4541-93ef-862cb88a0e04",
                    "LayerId": "d33c6e73-e639-4097-92c5-7fa51201550c"
                }
            ]
        },
        {
            "id": "fd46e96f-0748-4045-9192-705e4f84c9ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "798c4d6f-4a22-4d0d-8f44-11e705cae995",
            "compositeImage": {
                "id": "b453104c-466d-4fe1-b2e6-2e9c81a5a1e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd46e96f-0748-4045-9192-705e4f84c9ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0934ebb4-d2d7-494d-8dd5-b7d54fe825ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd46e96f-0748-4045-9192-705e4f84c9ae",
                    "LayerId": "d33c6e73-e639-4097-92c5-7fa51201550c"
                }
            ]
        },
        {
            "id": "a1f2138c-a03c-4b68-b88a-5be8a1ce3901",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "798c4d6f-4a22-4d0d-8f44-11e705cae995",
            "compositeImage": {
                "id": "8712080a-54e1-4f89-bc53-bcdf177deb04",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1f2138c-a03c-4b68-b88a-5be8a1ce3901",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25f63afe-8620-484c-8118-d73b034f2d5e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1f2138c-a03c-4b68-b88a-5be8a1ce3901",
                    "LayerId": "d33c6e73-e639-4097-92c5-7fa51201550c"
                }
            ]
        },
        {
            "id": "71b48a3b-cdeb-4451-8b03-37e6e9687220",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "798c4d6f-4a22-4d0d-8f44-11e705cae995",
            "compositeImage": {
                "id": "ae1b0468-9304-4ce0-958d-11cdcff018dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "71b48a3b-cdeb-4451-8b03-37e6e9687220",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f691225-1106-4100-9a19-7438eed0ff56",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71b48a3b-cdeb-4451-8b03-37e6e9687220",
                    "LayerId": "d33c6e73-e639-4097-92c5-7fa51201550c"
                }
            ]
        },
        {
            "id": "ba80593e-3dbc-45b5-9316-60cf16ad7688",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "798c4d6f-4a22-4d0d-8f44-11e705cae995",
            "compositeImage": {
                "id": "1c13f956-6482-475f-b8a0-f33829d9f00d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba80593e-3dbc-45b5-9316-60cf16ad7688",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "98a31763-a46d-44a6-a2e0-bad4ef2e7bdb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba80593e-3dbc-45b5-9316-60cf16ad7688",
                    "LayerId": "d33c6e73-e639-4097-92c5-7fa51201550c"
                }
            ]
        },
        {
            "id": "520cc837-e7b2-4f2a-afc2-7b1027b8bd84",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "798c4d6f-4a22-4d0d-8f44-11e705cae995",
            "compositeImage": {
                "id": "791a5145-bd34-4273-a62b-5c3bd6245ab8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "520cc837-e7b2-4f2a-afc2-7b1027b8bd84",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f028c54-4990-441e-85b1-2e630b19eab5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "520cc837-e7b2-4f2a-afc2-7b1027b8bd84",
                    "LayerId": "d33c6e73-e639-4097-92c5-7fa51201550c"
                }
            ]
        },
        {
            "id": "89bdb90a-d8eb-43e2-b81e-c2a419daa628",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "798c4d6f-4a22-4d0d-8f44-11e705cae995",
            "compositeImage": {
                "id": "605a9537-d3c6-4612-bdba-a5ccaebbffe0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89bdb90a-d8eb-43e2-b81e-c2a419daa628",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c847003-bdc4-4ed0-8184-1d1c37704768",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89bdb90a-d8eb-43e2-b81e-c2a419daa628",
                    "LayerId": "d33c6e73-e639-4097-92c5-7fa51201550c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "d33c6e73-e639-4097-92c5-7fa51201550c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "798c4d6f-4a22-4d0d-8f44-11e705cae995",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}