{
    "id": "76b9043c-7632-4bc5-9616-d33ceb4fe47d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayerCrouchHurt",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 94,
    "bbox_left": 58,
    "bbox_right": 72,
    "bbox_top": 59,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "baf05d6e-f1d0-4973-aa2b-ec021ebb0a01",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76b9043c-7632-4bc5-9616-d33ceb4fe47d",
            "compositeImage": {
                "id": "6684e8a8-fdf4-4657-8338-619aad1b1533",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "baf05d6e-f1d0-4973-aa2b-ec021ebb0a01",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "afc0e044-d6bb-488b-a1e9-d3a1de773332",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "baf05d6e-f1d0-4973-aa2b-ec021ebb0a01",
                    "LayerId": "41763448-cb59-4d7a-98ad-d1c06ca590b9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "41763448-cb59-4d7a-98ad-d1c06ca590b9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "76b9043c-7632-4bc5-9616-d33ceb4fe47d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}