{
    "id": "62cc90c6-586e-4849-ba65-74f2f82885ef",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGremlin_Stun",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 14,
    "bbox_right": 45,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "464387c1-6b9f-4223-b2aa-f3e551e516d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "62cc90c6-586e-4849-ba65-74f2f82885ef",
            "compositeImage": {
                "id": "fe10815f-195a-4788-b445-a8fc0bbc8865",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "464387c1-6b9f-4223-b2aa-f3e551e516d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf6a70cf-6aa4-435b-9c41-fed0d8cfad12",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "464387c1-6b9f-4223-b2aa-f3e551e516d4",
                    "LayerId": "71021a18-f7f3-449d-aab5-93c4c57967a7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "71021a18-f7f3-449d-aab5-93c4c57967a7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "62cc90c6-586e-4849-ba65-74f2f82885ef",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 77,
    "xorig": 30,
    "yorig": 25
}