{
    "id": "d43b1906-389d-479a-a4ab-296b251ed015",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTilesInterior01",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 335,
    "bbox_left": 32,
    "bbox_right": 571,
    "bbox_top": 31,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "78a75665-6bed-4627-aea8-6666d8496aae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d43b1906-389d-479a-a4ab-296b251ed015",
            "compositeImage": {
                "id": "d6bd6e83-d753-494d-ae97-1c9909aa2930",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78a75665-6bed-4627-aea8-6666d8496aae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3a6010e-aa33-4609-9e24-2ff5106ae152",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78a75665-6bed-4627-aea8-6666d8496aae",
                    "LayerId": "4b48e55c-2817-422a-9451-41ae29814904"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 360,
    "layers": [
        {
            "id": "4b48e55c-2817-422a-9451-41ae29814904",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d43b1906-389d-479a-a4ab-296b251ed015",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 0,
    "yorig": 0
}