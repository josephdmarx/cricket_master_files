{
    "id": "9b984abd-b95b-4fc4-b2c5-0dd1e72c7ecc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sLeftBumperPad",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 7,
    "bbox_right": 26,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3d351782-24d8-4af6-9f44-82a92969f0ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9b984abd-b95b-4fc4-b2c5-0dd1e72c7ecc",
            "compositeImage": {
                "id": "8cc25250-70ff-4ab8-9dad-123232964f34",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d351782-24d8-4af6-9f44-82a92969f0ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fce9899f-af7b-4f1d-8277-71a517f40c39",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d351782-24d8-4af6-9f44-82a92969f0ff",
                    "LayerId": "fd862a60-4271-4939-91bc-d7f4c42b4e81"
                }
            ]
        },
        {
            "id": "528a6e80-9ea5-4fa7-947b-23c5aaa54ed7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9b984abd-b95b-4fc4-b2c5-0dd1e72c7ecc",
            "compositeImage": {
                "id": "7fa35e2d-2db3-42d2-8234-84c1fe9d7c24",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "528a6e80-9ea5-4fa7-947b-23c5aaa54ed7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d2f00628-94ea-4f31-ac38-77d9e7265878",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "528a6e80-9ea5-4fa7-947b-23c5aaa54ed7",
                    "LayerId": "fd862a60-4271-4939-91bc-d7f4c42b4e81"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "fd862a60-4271-4939-91bc-d7f4c42b4e81",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9b984abd-b95b-4fc4-b2c5-0dd1e72c7ecc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 25
}