{
    "id": "99b6868d-d82a-4e05-aa5f-8dc1c354f60e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSlotHolder",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 221,
    "bbox_left": 0,
    "bbox_right": 256,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c322973d-b138-41fa-8cdc-3aa656ba34f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "99b6868d-d82a-4e05-aa5f-8dc1c354f60e",
            "compositeImage": {
                "id": "6ce7578c-177c-4b4c-adc7-39e7e45f7a6b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c322973d-b138-41fa-8cdc-3aa656ba34f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2bae6df3-314a-45e6-8b7a-aa14a9b7df88",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c322973d-b138-41fa-8cdc-3aa656ba34f9",
                    "LayerId": "ee0e9d0f-8353-4bb9-9815-6f44d676c6b4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 222,
    "layers": [
        {
            "id": "ee0e9d0f-8353-4bb9-9815-6f44d676c6b4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "99b6868d-d82a-4e05-aa5f-8dc1c354f60e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 257,
    "xorig": 0,
    "yorig": 0
}