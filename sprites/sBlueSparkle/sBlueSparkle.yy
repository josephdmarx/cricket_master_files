{
    "id": "8b149d3f-5513-4039-827f-8ce4ff4243d1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBlueSparkle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 6,
    "bbox_left": 1,
    "bbox_right": 6,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dc936726-7940-499d-a096-8a03afec8a91",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8b149d3f-5513-4039-827f-8ce4ff4243d1",
            "compositeImage": {
                "id": "b57f8831-b13e-465f-a4a3-e96589dc353f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dc936726-7940-499d-a096-8a03afec8a91",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a030012-d593-4515-8393-7ad7d3d5d168",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc936726-7940-499d-a096-8a03afec8a91",
                    "LayerId": "3e2b21e1-6f8a-4a1f-825a-14015925e035"
                }
            ]
        },
        {
            "id": "a66196b4-5754-40aa-b41b-45b4ff97359c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8b149d3f-5513-4039-827f-8ce4ff4243d1",
            "compositeImage": {
                "id": "8f3bfb48-857c-4fe6-bc69-0bc976637b76",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a66196b4-5754-40aa-b41b-45b4ff97359c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64742797-208c-43ed-b66f-7943f2b1e16d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a66196b4-5754-40aa-b41b-45b4ff97359c",
                    "LayerId": "3e2b21e1-6f8a-4a1f-825a-14015925e035"
                }
            ]
        },
        {
            "id": "eb7bdfe3-dc64-47e6-8789-8a8724643062",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8b149d3f-5513-4039-827f-8ce4ff4243d1",
            "compositeImage": {
                "id": "c5b87af7-5efd-4713-83ae-f2dc6262772c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb7bdfe3-dc64-47e6-8789-8a8724643062",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f1fa480e-0246-403e-baa0-cfe4d75d6211",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb7bdfe3-dc64-47e6-8789-8a8724643062",
                    "LayerId": "3e2b21e1-6f8a-4a1f-825a-14015925e035"
                }
            ]
        },
        {
            "id": "0efba321-e211-4042-9159-7fd88f944174",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8b149d3f-5513-4039-827f-8ce4ff4243d1",
            "compositeImage": {
                "id": "e50e840a-f08f-47f3-9b48-71e35a2eefb7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0efba321-e211-4042-9159-7fd88f944174",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55cfebf3-6e1e-468c-8ea8-f2c04f578554",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0efba321-e211-4042-9159-7fd88f944174",
                    "LayerId": "3e2b21e1-6f8a-4a1f-825a-14015925e035"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "3e2b21e1-6f8a-4a1f-825a-14015925e035",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8b149d3f-5513-4039-827f-8ce4ff4243d1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}