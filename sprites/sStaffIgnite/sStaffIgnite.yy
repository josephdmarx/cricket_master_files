{
    "id": "dc4b7e93-63a0-4151-a4d9-544d3402b43d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sStaffIgnite",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 48,
    "bbox_left": 49,
    "bbox_right": 68,
    "bbox_top": 23,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2b1a2586-6714-455c-b8e1-583c759a3276",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dc4b7e93-63a0-4151-a4d9-544d3402b43d",
            "compositeImage": {
                "id": "ed911ed8-afa5-4673-a442-2d5502c11f8e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b1a2586-6714-455c-b8e1-583c759a3276",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "151597b8-623b-499d-aa20-60b7e18edc7d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b1a2586-6714-455c-b8e1-583c759a3276",
                    "LayerId": "767704ba-2817-4ad8-b3d2-9a493d463b9c"
                }
            ]
        },
        {
            "id": "827f584b-3034-4ca9-a644-f6ef9d97a42b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dc4b7e93-63a0-4151-a4d9-544d3402b43d",
            "compositeImage": {
                "id": "bfb47e1a-cd9f-493f-a7fe-2e6b94918185",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "827f584b-3034-4ca9-a644-f6ef9d97a42b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b258cea1-e99f-4936-842f-db617de23a39",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "827f584b-3034-4ca9-a644-f6ef9d97a42b",
                    "LayerId": "767704ba-2817-4ad8-b3d2-9a493d463b9c"
                }
            ]
        },
        {
            "id": "3b0bca19-6449-4183-8dc2-cc29658d311c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dc4b7e93-63a0-4151-a4d9-544d3402b43d",
            "compositeImage": {
                "id": "dfeb6c7e-e758-4bf7-8367-7c427928e72f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b0bca19-6449-4183-8dc2-cc29658d311c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fac6583b-4f4c-4f5e-a2c8-5134edd7b9ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b0bca19-6449-4183-8dc2-cc29658d311c",
                    "LayerId": "767704ba-2817-4ad8-b3d2-9a493d463b9c"
                }
            ]
        },
        {
            "id": "9df084ad-a02e-4620-918d-af89986ab5c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dc4b7e93-63a0-4151-a4d9-544d3402b43d",
            "compositeImage": {
                "id": "64c6cd5c-8b48-4b83-86e9-0dd11ec39aa8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9df084ad-a02e-4620-918d-af89986ab5c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7cf842bc-b0f5-47f0-8714-77b13d39416d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9df084ad-a02e-4620-918d-af89986ab5c6",
                    "LayerId": "767704ba-2817-4ad8-b3d2-9a493d463b9c"
                }
            ]
        },
        {
            "id": "0cfbd4b7-365b-4d4b-bd7e-0b94d5d61a44",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dc4b7e93-63a0-4151-a4d9-544d3402b43d",
            "compositeImage": {
                "id": "651441dd-f7b8-42bc-9eba-95054331dc74",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0cfbd4b7-365b-4d4b-bd7e-0b94d5d61a44",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a83a36dc-262a-46dd-95b1-8626c737a90e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0cfbd4b7-365b-4d4b-bd7e-0b94d5d61a44",
                    "LayerId": "767704ba-2817-4ad8-b3d2-9a493d463b9c"
                }
            ]
        },
        {
            "id": "39e1de90-e0d2-4f15-ae82-073732ebf947",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dc4b7e93-63a0-4151-a4d9-544d3402b43d",
            "compositeImage": {
                "id": "37e10bf7-7a94-4c93-b825-51c7ecd7e6c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39e1de90-e0d2-4f15-ae82-073732ebf947",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0786a1ad-857e-4c38-9ae0-dd7ab1b0827d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39e1de90-e0d2-4f15-ae82-073732ebf947",
                    "LayerId": "767704ba-2817-4ad8-b3d2-9a493d463b9c"
                }
            ]
        },
        {
            "id": "6ddce6c8-fe5f-42c3-a853-7eef5bbbfa6d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dc4b7e93-63a0-4151-a4d9-544d3402b43d",
            "compositeImage": {
                "id": "ccac2762-338e-488b-94e6-c7983a366c38",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ddce6c8-fe5f-42c3-a853-7eef5bbbfa6d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2dda8adb-9ab2-4071-9b23-e1476a3baa0e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ddce6c8-fe5f-42c3-a853-7eef5bbbfa6d",
                    "LayerId": "767704ba-2817-4ad8-b3d2-9a493d463b9c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "767704ba-2817-4ad8-b3d2-9a493d463b9c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dc4b7e93-63a0-4151-a4d9-544d3402b43d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}