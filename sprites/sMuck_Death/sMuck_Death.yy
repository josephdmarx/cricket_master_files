{
    "id": "a81f6f8c-ccca-4a1c-8c24-23c07ea97bfb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sMuck_Death",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 112,
    "bbox_left": 33,
    "bbox_right": 104,
    "bbox_top": 49,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a9e04ef2-3908-4d9e-bd3c-37b54d4af4bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a81f6f8c-ccca-4a1c-8c24-23c07ea97bfb",
            "compositeImage": {
                "id": "e0e1d9b8-3379-4ef4-8fec-337421aecb29",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a9e04ef2-3908-4d9e-bd3c-37b54d4af4bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea667d5f-d2d6-4054-8361-b74d42d79969",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a9e04ef2-3908-4d9e-bd3c-37b54d4af4bc",
                    "LayerId": "7fce3172-8e78-4aa0-9cdc-83d7a904861d"
                }
            ]
        },
        {
            "id": "705ef27e-62b0-4377-953e-e4e1dc4e363e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a81f6f8c-ccca-4a1c-8c24-23c07ea97bfb",
            "compositeImage": {
                "id": "1ea0cf59-52c2-4145-a75c-cbc2e83861e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "705ef27e-62b0-4377-953e-e4e1dc4e363e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec0a42ce-1997-442b-98fe-db50d72d3dd4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "705ef27e-62b0-4377-953e-e4e1dc4e363e",
                    "LayerId": "7fce3172-8e78-4aa0-9cdc-83d7a904861d"
                }
            ]
        },
        {
            "id": "0606cbcc-9198-40f7-b6fe-925233546523",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a81f6f8c-ccca-4a1c-8c24-23c07ea97bfb",
            "compositeImage": {
                "id": "7027d887-97b7-477b-bee6-d71eba0e184a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0606cbcc-9198-40f7-b6fe-925233546523",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5848786-bda8-4953-8f98-b1a47e3d0f8b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0606cbcc-9198-40f7-b6fe-925233546523",
                    "LayerId": "7fce3172-8e78-4aa0-9cdc-83d7a904861d"
                }
            ]
        },
        {
            "id": "2155e557-2490-4a04-bf69-fb6345013bfd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a81f6f8c-ccca-4a1c-8c24-23c07ea97bfb",
            "compositeImage": {
                "id": "b9c44c6f-fd35-49fc-b39b-20ddf993e0a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2155e557-2490-4a04-bf69-fb6345013bfd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "79875d47-ee02-4148-9795-e3c7e1ad6ce9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2155e557-2490-4a04-bf69-fb6345013bfd",
                    "LayerId": "7fce3172-8e78-4aa0-9cdc-83d7a904861d"
                }
            ]
        },
        {
            "id": "c992dc56-bee8-4d20-b2c6-2bc55491799d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a81f6f8c-ccca-4a1c-8c24-23c07ea97bfb",
            "compositeImage": {
                "id": "8c1e8082-d156-4dd4-94c2-8dd8187cf208",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c992dc56-bee8-4d20-b2c6-2bc55491799d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ebffae6c-6f52-40fa-ae6d-22aab45d5dda",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c992dc56-bee8-4d20-b2c6-2bc55491799d",
                    "LayerId": "7fce3172-8e78-4aa0-9cdc-83d7a904861d"
                }
            ]
        },
        {
            "id": "554a590c-aeb4-40bc-bdab-aa9fc306bce2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a81f6f8c-ccca-4a1c-8c24-23c07ea97bfb",
            "compositeImage": {
                "id": "5e3b8c1c-4b63-4395-a49d-a41dfa29e345",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "554a590c-aeb4-40bc-bdab-aa9fc306bce2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "41066d4c-5a26-46b9-86c4-91728abfc2ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "554a590c-aeb4-40bc-bdab-aa9fc306bce2",
                    "LayerId": "7fce3172-8e78-4aa0-9cdc-83d7a904861d"
                }
            ]
        },
        {
            "id": "eb21d47e-f69f-4de0-ac2d-0ea587f0c550",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a81f6f8c-ccca-4a1c-8c24-23c07ea97bfb",
            "compositeImage": {
                "id": "8974ec9d-9bd1-4d9c-b5d6-907687e89862",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb21d47e-f69f-4de0-ac2d-0ea587f0c550",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ffb818ce-1de5-4aad-be33-e03e494d5fdb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb21d47e-f69f-4de0-ac2d-0ea587f0c550",
                    "LayerId": "7fce3172-8e78-4aa0-9cdc-83d7a904861d"
                }
            ]
        },
        {
            "id": "996fbb3e-f7fc-4da3-95eb-8ca0af74ccb0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a81f6f8c-ccca-4a1c-8c24-23c07ea97bfb",
            "compositeImage": {
                "id": "0e0b6734-e1a1-47b6-8f57-cf1072b987f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "996fbb3e-f7fc-4da3-95eb-8ca0af74ccb0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "18c2963c-33b8-4d70-a8cb-800a5e1aefd7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "996fbb3e-f7fc-4da3-95eb-8ca0af74ccb0",
                    "LayerId": "7fce3172-8e78-4aa0-9cdc-83d7a904861d"
                }
            ]
        },
        {
            "id": "6946bec7-76d3-42df-b620-6f47d8f0701a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a81f6f8c-ccca-4a1c-8c24-23c07ea97bfb",
            "compositeImage": {
                "id": "c5a3d15b-5c04-412f-b678-1a8da8306411",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6946bec7-76d3-42df-b620-6f47d8f0701a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d9fbc4a-06b4-45ef-9ef6-2d1423edeeee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6946bec7-76d3-42df-b620-6f47d8f0701a",
                    "LayerId": "7fce3172-8e78-4aa0-9cdc-83d7a904861d"
                }
            ]
        },
        {
            "id": "314c1b41-899c-4c09-8a96-1dc2e5cf1435",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a81f6f8c-ccca-4a1c-8c24-23c07ea97bfb",
            "compositeImage": {
                "id": "97c4441f-c82a-4cdc-aa9c-653c19743ea6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "314c1b41-899c-4c09-8a96-1dc2e5cf1435",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "473982fd-39bb-4343-a40f-dbb4f000433b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "314c1b41-899c-4c09-8a96-1dc2e5cf1435",
                    "LayerId": "7fce3172-8e78-4aa0-9cdc-83d7a904861d"
                }
            ]
        },
        {
            "id": "d29a0eab-67f3-4472-9e20-97fa99c70666",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a81f6f8c-ccca-4a1c-8c24-23c07ea97bfb",
            "compositeImage": {
                "id": "02f51e3c-cf6b-4929-ba15-2635e15bf01f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d29a0eab-67f3-4472-9e20-97fa99c70666",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7891bda-2911-4cdb-878f-342d14cec0db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d29a0eab-67f3-4472-9e20-97fa99c70666",
                    "LayerId": "7fce3172-8e78-4aa0-9cdc-83d7a904861d"
                }
            ]
        },
        {
            "id": "8d5fee7a-7871-45d1-b401-b157fb925467",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a81f6f8c-ccca-4a1c-8c24-23c07ea97bfb",
            "compositeImage": {
                "id": "464ca754-3584-43c7-bfb8-cd34412dc652",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d5fee7a-7871-45d1-b401-b157fb925467",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f2b784dc-1fba-4cd2-99c1-4116dd9d4e69",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d5fee7a-7871-45d1-b401-b157fb925467",
                    "LayerId": "7fce3172-8e78-4aa0-9cdc-83d7a904861d"
                }
            ]
        },
        {
            "id": "f9c69dc5-ef8a-43a5-acb7-611760bdc4f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a81f6f8c-ccca-4a1c-8c24-23c07ea97bfb",
            "compositeImage": {
                "id": "4d72dd7e-d80e-4e27-8d2b-2997a5cd9ff0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f9c69dc5-ef8a-43a5-acb7-611760bdc4f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e55d5a0f-210b-456e-b1c7-39bfa241acf5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9c69dc5-ef8a-43a5-acb7-611760bdc4f1",
                    "LayerId": "7fce3172-8e78-4aa0-9cdc-83d7a904861d"
                }
            ]
        },
        {
            "id": "50ff877b-5d16-4c79-9c33-b43f2ab23532",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a81f6f8c-ccca-4a1c-8c24-23c07ea97bfb",
            "compositeImage": {
                "id": "7f629f0a-ad15-46c9-8783-4f620ff22d77",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "50ff877b-5d16-4c79-9c33-b43f2ab23532",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf4700e0-0d7e-455e-b082-f50d027584cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "50ff877b-5d16-4c79-9c33-b43f2ab23532",
                    "LayerId": "7fce3172-8e78-4aa0-9cdc-83d7a904861d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "7fce3172-8e78-4aa0-9cdc-83d7a904861d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a81f6f8c-ccca-4a1c-8c24-23c07ea97bfb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 112
}