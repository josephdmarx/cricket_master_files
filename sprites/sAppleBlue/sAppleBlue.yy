{
    "id": "c66c3466-99fc-46c1-9f7c-4e6e9033b957",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sAppleBlue",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 9,
    "bbox_right": 22,
    "bbox_top": 9,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "01b70fec-fdf4-4d31-93d9-2a0a94d96d6e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c66c3466-99fc-46c1-9f7c-4e6e9033b957",
            "compositeImage": {
                "id": "d0fce8f0-c78e-4441-9fd0-f29736dd0bb3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01b70fec-fdf4-4d31-93d9-2a0a94d96d6e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd119f9b-6926-45d5-907b-f110c1ca467e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01b70fec-fdf4-4d31-93d9-2a0a94d96d6e",
                    "LayerId": "83ba3804-9076-40f4-89a6-821f66b1fac8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "83ba3804-9076-40f4-89a6-821f66b1fac8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c66c3466-99fc-46c1-9f7c-4e6e9033b957",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}