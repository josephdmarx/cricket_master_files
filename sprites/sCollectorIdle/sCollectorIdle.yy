{
    "id": "f8c0816c-e7c3-40e8-94c1-f3a617834a02",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCollectorIdle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 111,
    "bbox_left": 29,
    "bbox_right": 89,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4ef4f80a-fa76-46c5-95bf-5a8883d4fc6f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8c0816c-e7c3-40e8-94c1-f3a617834a02",
            "compositeImage": {
                "id": "e1b4cff7-3f45-4053-b1d2-f617ec14f823",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ef4f80a-fa76-46c5-95bf-5a8883d4fc6f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "085d5f2c-a476-40f8-b3a7-e4313f07f423",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ef4f80a-fa76-46c5-95bf-5a8883d4fc6f",
                    "LayerId": "6cafccd2-b897-492e-8b2e-aca4d35128b5"
                }
            ]
        },
        {
            "id": "75c534e1-589c-40ce-8adf-feb5370fc17d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8c0816c-e7c3-40e8-94c1-f3a617834a02",
            "compositeImage": {
                "id": "0716b49e-4766-492d-a92a-58173057c2df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "75c534e1-589c-40ce-8adf-feb5370fc17d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cea0d79c-81ac-4e31-b95d-e344bc69e07c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "75c534e1-589c-40ce-8adf-feb5370fc17d",
                    "LayerId": "6cafccd2-b897-492e-8b2e-aca4d35128b5"
                }
            ]
        },
        {
            "id": "687fdca8-a372-423d-850f-7f8f6f3626eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8c0816c-e7c3-40e8-94c1-f3a617834a02",
            "compositeImage": {
                "id": "a93ee909-7a7a-4594-b015-583b9b20928a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "687fdca8-a372-423d-850f-7f8f6f3626eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ac9f355-451e-4500-b7de-6a08c356997e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "687fdca8-a372-423d-850f-7f8f6f3626eb",
                    "LayerId": "6cafccd2-b897-492e-8b2e-aca4d35128b5"
                }
            ]
        },
        {
            "id": "9fd3dd3c-a513-4b6e-81d2-cc5e62aecf29",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8c0816c-e7c3-40e8-94c1-f3a617834a02",
            "compositeImage": {
                "id": "4cc22790-3106-4977-b632-78725f882e25",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9fd3dd3c-a513-4b6e-81d2-cc5e62aecf29",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d9362b3-f0ba-45d1-9b43-c196a25f31cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9fd3dd3c-a513-4b6e-81d2-cc5e62aecf29",
                    "LayerId": "6cafccd2-b897-492e-8b2e-aca4d35128b5"
                }
            ]
        },
        {
            "id": "f6a2283e-6535-42b8-8801-740e5e98c1d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8c0816c-e7c3-40e8-94c1-f3a617834a02",
            "compositeImage": {
                "id": "4473a44d-47cc-41ff-86d4-f2e864fa04c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6a2283e-6535-42b8-8801-740e5e98c1d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c4e6ea5-d1ca-4937-8e89-c4d70d467d27",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6a2283e-6535-42b8-8801-740e5e98c1d8",
                    "LayerId": "6cafccd2-b897-492e-8b2e-aca4d35128b5"
                }
            ]
        },
        {
            "id": "e782fe2a-a8e9-4a8b-8ff8-8093213b3640",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8c0816c-e7c3-40e8-94c1-f3a617834a02",
            "compositeImage": {
                "id": "3caa5299-d4f3-4b96-9225-0f7d6f7b24d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e782fe2a-a8e9-4a8b-8ff8-8093213b3640",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f9f94df-9fb9-487a-b0c6-1a6cf4ceb8c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e782fe2a-a8e9-4a8b-8ff8-8093213b3640",
                    "LayerId": "6cafccd2-b897-492e-8b2e-aca4d35128b5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "6cafccd2-b897-492e-8b2e-aca4d35128b5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f8c0816c-e7c3-40e8-94c1-f3a617834a02",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}