{
    "id": "0892bc96-de5c-42b1-833f-7cffb7a0e1d2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBedSickHurley",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 55,
    "bbox_left": 4,
    "bbox_right": 90,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6ebe4ca6-ae82-4e41-92ea-fcdf4b3908cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0892bc96-de5c-42b1-833f-7cffb7a0e1d2",
            "compositeImage": {
                "id": "440fdff4-8ee2-4454-96bb-dac87606bdf0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ebe4ca6-ae82-4e41-92ea-fcdf4b3908cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29e0aa06-077c-4368-b0e5-e1d15844979d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ebe4ca6-ae82-4e41-92ea-fcdf4b3908cf",
                    "LayerId": "d4b43d9b-e34b-4bfc-8b1e-cc5a749d3752"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "d4b43d9b-e34b-4bfc-8b1e-cc5a749d3752",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0892bc96-de5c-42b1-833f-7cffb7a0e1d2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 4,
    "yorig": 55
}