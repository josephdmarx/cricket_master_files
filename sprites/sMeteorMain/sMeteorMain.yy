{
    "id": "f88f5a4e-edde-4fa6-83d0-d67a1dfa58c7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sMeteorMain",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 84,
    "bbox_left": 37,
    "bbox_right": 67,
    "bbox_top": 26,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "28cc4d1c-720b-45e9-b7f0-e899e0db0b25",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f88f5a4e-edde-4fa6-83d0-d67a1dfa58c7",
            "compositeImage": {
                "id": "6ae4e607-886c-426b-879f-db118e0ddb01",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "28cc4d1c-720b-45e9-b7f0-e899e0db0b25",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e121d45e-bd6d-4b60-bcc1-473036ffc42a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "28cc4d1c-720b-45e9-b7f0-e899e0db0b25",
                    "LayerId": "7d55b8d8-9481-4fe3-b0d7-f95b2f4ed208"
                }
            ]
        },
        {
            "id": "c8e87bc6-bf8e-48cb-8f6f-07ae843590ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f88f5a4e-edde-4fa6-83d0-d67a1dfa58c7",
            "compositeImage": {
                "id": "cfe8fc84-2387-4a34-9ac1-1f246094d245",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8e87bc6-bf8e-48cb-8f6f-07ae843590ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "535f4cc8-4815-46f8-8416-8b06bac2a07f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8e87bc6-bf8e-48cb-8f6f-07ae843590ab",
                    "LayerId": "7d55b8d8-9481-4fe3-b0d7-f95b2f4ed208"
                }
            ]
        },
        {
            "id": "d75a6abc-2005-4334-8338-5b9d53dbb02d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f88f5a4e-edde-4fa6-83d0-d67a1dfa58c7",
            "compositeImage": {
                "id": "8bc68161-925c-406a-ace1-2074a2679444",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d75a6abc-2005-4334-8338-5b9d53dbb02d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e77d54b3-c47b-41a4-a0cd-8e12dc6f8e17",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d75a6abc-2005-4334-8338-5b9d53dbb02d",
                    "LayerId": "7d55b8d8-9481-4fe3-b0d7-f95b2f4ed208"
                }
            ]
        },
        {
            "id": "91d04b19-4ae3-4cd7-984b-c935a166f2af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f88f5a4e-edde-4fa6-83d0-d67a1dfa58c7",
            "compositeImage": {
                "id": "707cba81-0582-42a3-b33d-5d878b2eab30",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91d04b19-4ae3-4cd7-984b-c935a166f2af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8e25c4e-f6f6-48b1-b96b-659e7d3889bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91d04b19-4ae3-4cd7-984b-c935a166f2af",
                    "LayerId": "7d55b8d8-9481-4fe3-b0d7-f95b2f4ed208"
                }
            ]
        },
        {
            "id": "a8f19754-cb61-45b8-8852-9b9e0e429efe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f88f5a4e-edde-4fa6-83d0-d67a1dfa58c7",
            "compositeImage": {
                "id": "ebef51bc-59d1-4d66-83d8-ca2dbd18329b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a8f19754-cb61-45b8-8852-9b9e0e429efe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "241a7810-18dc-401d-9969-7021a0e4ab25",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8f19754-cb61-45b8-8852-9b9e0e429efe",
                    "LayerId": "7d55b8d8-9481-4fe3-b0d7-f95b2f4ed208"
                }
            ]
        },
        {
            "id": "ccf76ee4-65f9-4162-828b-0c5e48e3cc7e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f88f5a4e-edde-4fa6-83d0-d67a1dfa58c7",
            "compositeImage": {
                "id": "608371c4-8f20-45f6-b613-6fb51adc8b02",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ccf76ee4-65f9-4162-828b-0c5e48e3cc7e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "17e8e1f3-2cbb-4b10-a805-f7c6e2158d13",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ccf76ee4-65f9-4162-828b-0c5e48e3cc7e",
                    "LayerId": "7d55b8d8-9481-4fe3-b0d7-f95b2f4ed208"
                }
            ]
        },
        {
            "id": "a021ec56-25ff-4786-b7b3-83403d20a990",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f88f5a4e-edde-4fa6-83d0-d67a1dfa58c7",
            "compositeImage": {
                "id": "580d9661-33b1-4863-858f-f57b10bbac9a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a021ec56-25ff-4786-b7b3-83403d20a990",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae4e3975-f518-4560-ba6e-4219197ccec8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a021ec56-25ff-4786-b7b3-83403d20a990",
                    "LayerId": "7d55b8d8-9481-4fe3-b0d7-f95b2f4ed208"
                }
            ]
        },
        {
            "id": "d9d45004-9ae5-44bb-9632-e03fb1d6978c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f88f5a4e-edde-4fa6-83d0-d67a1dfa58c7",
            "compositeImage": {
                "id": "26acfe54-a5ba-494f-802a-6647512c4cf8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d9d45004-9ae5-44bb-9632-e03fb1d6978c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f360718e-598e-437b-ba92-02e8d83c3fb3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9d45004-9ae5-44bb-9632-e03fb1d6978c",
                    "LayerId": "7d55b8d8-9481-4fe3-b0d7-f95b2f4ed208"
                }
            ]
        },
        {
            "id": "57d358f2-ecc4-41be-8f89-c5a7636e2d36",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f88f5a4e-edde-4fa6-83d0-d67a1dfa58c7",
            "compositeImage": {
                "id": "0b82b488-4513-4d88-a456-43469c1d42d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57d358f2-ecc4-41be-8f89-c5a7636e2d36",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12695fdb-2dbe-47a8-9e05-50152d8d36bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57d358f2-ecc4-41be-8f89-c5a7636e2d36",
                    "LayerId": "7d55b8d8-9481-4fe3-b0d7-f95b2f4ed208"
                }
            ]
        },
        {
            "id": "25412b6c-2519-4dda-8b00-2502e85447e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f88f5a4e-edde-4fa6-83d0-d67a1dfa58c7",
            "compositeImage": {
                "id": "5c407612-e0b8-4e94-8e0d-e5c778eb3524",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25412b6c-2519-4dda-8b00-2502e85447e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8898a511-aa81-48d1-aa92-3f1aa4e30f99",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25412b6c-2519-4dda-8b00-2502e85447e8",
                    "LayerId": "7d55b8d8-9481-4fe3-b0d7-f95b2f4ed208"
                }
            ]
        },
        {
            "id": "a969b9c7-1268-4049-85cc-76210d3fc391",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f88f5a4e-edde-4fa6-83d0-d67a1dfa58c7",
            "compositeImage": {
                "id": "286352ca-70db-4667-8320-13e53312215d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a969b9c7-1268-4049-85cc-76210d3fc391",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a409cd8b-b8ee-4905-8ecd-249ce881b94e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a969b9c7-1268-4049-85cc-76210d3fc391",
                    "LayerId": "7d55b8d8-9481-4fe3-b0d7-f95b2f4ed208"
                }
            ]
        },
        {
            "id": "59a5e40e-52ae-41aa-aecd-c63afceff747",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f88f5a4e-edde-4fa6-83d0-d67a1dfa58c7",
            "compositeImage": {
                "id": "782c3756-77bd-42d3-a6be-775a03a0b3e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59a5e40e-52ae-41aa-aecd-c63afceff747",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b9e07c8-c687-4fdd-a598-51f45ca9398b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59a5e40e-52ae-41aa-aecd-c63afceff747",
                    "LayerId": "7d55b8d8-9481-4fe3-b0d7-f95b2f4ed208"
                }
            ]
        },
        {
            "id": "b93cdc0d-0df6-4a81-ad64-e7ed913637c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f88f5a4e-edde-4fa6-83d0-d67a1dfa58c7",
            "compositeImage": {
                "id": "271c0671-2e98-4752-9b68-d53c1225fba5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b93cdc0d-0df6-4a81-ad64-e7ed913637c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a448875c-667f-474f-875c-b94be6a90fec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b93cdc0d-0df6-4a81-ad64-e7ed913637c9",
                    "LayerId": "7d55b8d8-9481-4fe3-b0d7-f95b2f4ed208"
                }
            ]
        },
        {
            "id": "37d9012a-7ef7-4b46-958c-fe2b71c2bdc3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f88f5a4e-edde-4fa6-83d0-d67a1dfa58c7",
            "compositeImage": {
                "id": "1bbaaf02-eb86-4c24-aa58-e251c0473875",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "37d9012a-7ef7-4b46-958c-fe2b71c2bdc3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f993fc4-2c3f-4f5e-8134-8cd841695419",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37d9012a-7ef7-4b46-958c-fe2b71c2bdc3",
                    "LayerId": "7d55b8d8-9481-4fe3-b0d7-f95b2f4ed208"
                }
            ]
        },
        {
            "id": "fcc96c0d-755d-41d7-bacc-f6d4326587f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f88f5a4e-edde-4fa6-83d0-d67a1dfa58c7",
            "compositeImage": {
                "id": "150ea7fd-3af4-443b-9884-70df419953df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fcc96c0d-755d-41d7-bacc-f6d4326587f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d8016bfb-e164-41a9-8bb7-2775c290d4f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fcc96c0d-755d-41d7-bacc-f6d4326587f0",
                    "LayerId": "7d55b8d8-9481-4fe3-b0d7-f95b2f4ed208"
                }
            ]
        },
        {
            "id": "1a4b4085-e965-4a28-8f3d-89413167ca61",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f88f5a4e-edde-4fa6-83d0-d67a1dfa58c7",
            "compositeImage": {
                "id": "0990811e-76f0-43da-8fe3-c79d227220f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a4b4085-e965-4a28-8f3d-89413167ca61",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b20790b0-3ca0-47e5-9593-2a80510c1faa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a4b4085-e965-4a28-8f3d-89413167ca61",
                    "LayerId": "7d55b8d8-9481-4fe3-b0d7-f95b2f4ed208"
                }
            ]
        },
        {
            "id": "4fd4c827-8e63-4d04-b62c-3aa3c53affeb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f88f5a4e-edde-4fa6-83d0-d67a1dfa58c7",
            "compositeImage": {
                "id": "84a65178-b105-43e6-9307-f90ab7a50f5a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4fd4c827-8e63-4d04-b62c-3aa3c53affeb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d105bb7-5156-44aa-be87-0dc07bafb910",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4fd4c827-8e63-4d04-b62c-3aa3c53affeb",
                    "LayerId": "7d55b8d8-9481-4fe3-b0d7-f95b2f4ed208"
                }
            ]
        },
        {
            "id": "8a28789a-63be-4ded-9ece-b4f72a8ff39f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f88f5a4e-edde-4fa6-83d0-d67a1dfa58c7",
            "compositeImage": {
                "id": "e9bc68a7-b1e2-4f61-b076-958933acfe06",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a28789a-63be-4ded-9ece-b4f72a8ff39f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "67abe212-64a9-4dee-aab4-ca588e3231f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a28789a-63be-4ded-9ece-b4f72a8ff39f",
                    "LayerId": "7d55b8d8-9481-4fe3-b0d7-f95b2f4ed208"
                }
            ]
        },
        {
            "id": "8db8d210-27af-4cda-b893-e2b2acc5cbdf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f88f5a4e-edde-4fa6-83d0-d67a1dfa58c7",
            "compositeImage": {
                "id": "e73828d3-96e8-4f3c-b7c3-4e7d4a42e8a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8db8d210-27af-4cda-b893-e2b2acc5cbdf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f2c8d2c7-11a2-4a64-97be-9e381f7143be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8db8d210-27af-4cda-b893-e2b2acc5cbdf",
                    "LayerId": "7d55b8d8-9481-4fe3-b0d7-f95b2f4ed208"
                }
            ]
        },
        {
            "id": "a0838653-85e6-4de9-966f-b4be59d34cac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f88f5a4e-edde-4fa6-83d0-d67a1dfa58c7",
            "compositeImage": {
                "id": "4f855ae9-a18d-4dc6-a41d-c51fb9948a22",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a0838653-85e6-4de9-966f-b4be59d34cac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f284d46f-1b8d-4598-9c1d-352c1d9e657d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0838653-85e6-4de9-966f-b4be59d34cac",
                    "LayerId": "7d55b8d8-9481-4fe3-b0d7-f95b2f4ed208"
                }
            ]
        },
        {
            "id": "f3c6b86d-32c8-4c88-a54e-87598552943e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f88f5a4e-edde-4fa6-83d0-d67a1dfa58c7",
            "compositeImage": {
                "id": "a239c8e1-676d-4cb8-849e-449dfa01efdf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3c6b86d-32c8-4c88-a54e-87598552943e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1e56580-908a-4dce-9274-a7efcb7a63d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3c6b86d-32c8-4c88-a54e-87598552943e",
                    "LayerId": "7d55b8d8-9481-4fe3-b0d7-f95b2f4ed208"
                }
            ]
        },
        {
            "id": "f6b4f872-df0d-43ea-a958-2a68fe5e264c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f88f5a4e-edde-4fa6-83d0-d67a1dfa58c7",
            "compositeImage": {
                "id": "d2d8c41c-52c0-437b-93e9-e142963e0edb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6b4f872-df0d-43ea-a958-2a68fe5e264c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5930933a-b30c-434a-94f5-e00b7964f0b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6b4f872-df0d-43ea-a958-2a68fe5e264c",
                    "LayerId": "7d55b8d8-9481-4fe3-b0d7-f95b2f4ed208"
                }
            ]
        },
        {
            "id": "e3fab4fd-75bb-4f25-8d73-2dd0f44fa201",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f88f5a4e-edde-4fa6-83d0-d67a1dfa58c7",
            "compositeImage": {
                "id": "cccf2caf-475b-4e4b-91c2-9a62916803e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3fab4fd-75bb-4f25-8d73-2dd0f44fa201",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15a6f62c-a062-4536-80b9-f0d7e1f19089",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3fab4fd-75bb-4f25-8d73-2dd0f44fa201",
                    "LayerId": "7d55b8d8-9481-4fe3-b0d7-f95b2f4ed208"
                }
            ]
        },
        {
            "id": "3265eaf3-68c6-48f0-b04f-6abd1e00b947",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f88f5a4e-edde-4fa6-83d0-d67a1dfa58c7",
            "compositeImage": {
                "id": "abdd3a61-1ff3-43b8-ba3e-cb572809eea1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3265eaf3-68c6-48f0-b04f-6abd1e00b947",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19468d37-f13b-45f9-ab85-1ff0c7c9d89f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3265eaf3-68c6-48f0-b04f-6abd1e00b947",
                    "LayerId": "7d55b8d8-9481-4fe3-b0d7-f95b2f4ed208"
                }
            ]
        },
        {
            "id": "0683b066-df31-4fbd-899e-e52904b71336",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f88f5a4e-edde-4fa6-83d0-d67a1dfa58c7",
            "compositeImage": {
                "id": "d48b3fc0-7fc4-46a1-a77b-8b3d9dbde16b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0683b066-df31-4fbd-899e-e52904b71336",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1bbb7187-3d8b-48be-9403-e139a2dc2c2d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0683b066-df31-4fbd-899e-e52904b71336",
                    "LayerId": "7d55b8d8-9481-4fe3-b0d7-f95b2f4ed208"
                }
            ]
        },
        {
            "id": "d6d319ef-966c-4fe0-ab4a-1059c68b7e7f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f88f5a4e-edde-4fa6-83d0-d67a1dfa58c7",
            "compositeImage": {
                "id": "0cd2d940-f31a-4efc-8cc3-9bd5fc842694",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6d319ef-966c-4fe0-ab4a-1059c68b7e7f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64ebd877-c012-4062-8de0-049b83c9b151",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6d319ef-966c-4fe0-ab4a-1059c68b7e7f",
                    "LayerId": "7d55b8d8-9481-4fe3-b0d7-f95b2f4ed208"
                }
            ]
        },
        {
            "id": "26158376-23e7-4c1a-916d-36df06763adb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f88f5a4e-edde-4fa6-83d0-d67a1dfa58c7",
            "compositeImage": {
                "id": "c38a6f02-400f-4501-be93-1a9fc187d7dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26158376-23e7-4c1a-916d-36df06763adb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "960f2b13-b618-41d7-97a7-74b045d09e08",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26158376-23e7-4c1a-916d-36df06763adb",
                    "LayerId": "7d55b8d8-9481-4fe3-b0d7-f95b2f4ed208"
                }
            ]
        },
        {
            "id": "9e814c98-8633-4466-a3ab-57db923abb9a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f88f5a4e-edde-4fa6-83d0-d67a1dfa58c7",
            "compositeImage": {
                "id": "a7da472d-9ab1-4099-b71c-e79106a46be7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e814c98-8633-4466-a3ab-57db923abb9a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5010b9a-7b54-4ad9-9f97-725a81c57602",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e814c98-8633-4466-a3ab-57db923abb9a",
                    "LayerId": "7d55b8d8-9481-4fe3-b0d7-f95b2f4ed208"
                }
            ]
        },
        {
            "id": "56d3795e-7c7c-4ac4-891b-d883235ad4ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f88f5a4e-edde-4fa6-83d0-d67a1dfa58c7",
            "compositeImage": {
                "id": "8d3fe81c-b2c5-465a-b9a9-d85b5ee76288",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56d3795e-7c7c-4ac4-891b-d883235ad4ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9282fac0-0711-4669-89f3-57dbc3b12fe6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56d3795e-7c7c-4ac4-891b-d883235ad4ee",
                    "LayerId": "7d55b8d8-9481-4fe3-b0d7-f95b2f4ed208"
                }
            ]
        },
        {
            "id": "0ca66e56-729c-4d97-b31a-046d3cb4a964",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f88f5a4e-edde-4fa6-83d0-d67a1dfa58c7",
            "compositeImage": {
                "id": "e02d65cc-5d1e-44c4-8c10-f16a1e88aa86",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ca66e56-729c-4d97-b31a-046d3cb4a964",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b2671cc0-c27d-47a0-842e-018239c41f74",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ca66e56-729c-4d97-b31a-046d3cb4a964",
                    "LayerId": "7d55b8d8-9481-4fe3-b0d7-f95b2f4ed208"
                }
            ]
        },
        {
            "id": "80c44765-ac70-4052-b041-e471d10216ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f88f5a4e-edde-4fa6-83d0-d67a1dfa58c7",
            "compositeImage": {
                "id": "73627423-cd4a-410e-89d0-e0dbc147bd07",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "80c44765-ac70-4052-b041-e471d10216ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "03df9cec-18a7-45bc-a52e-a7519d761140",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80c44765-ac70-4052-b041-e471d10216ec",
                    "LayerId": "7d55b8d8-9481-4fe3-b0d7-f95b2f4ed208"
                }
            ]
        },
        {
            "id": "ae90dc47-d87e-4a8d-9ed3-6417b16db5b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f88f5a4e-edde-4fa6-83d0-d67a1dfa58c7",
            "compositeImage": {
                "id": "9ea99e97-cf30-4f75-bed6-68ebcccf5373",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae90dc47-d87e-4a8d-9ed3-6417b16db5b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d5dd875-6115-4c55-92fd-1d7238ec3bbc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae90dc47-d87e-4a8d-9ed3-6417b16db5b2",
                    "LayerId": "7d55b8d8-9481-4fe3-b0d7-f95b2f4ed208"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "7d55b8d8-9481-4fe3-b0d7-f95b2f4ed208",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f88f5a4e-edde-4fa6-83d0-d67a1dfa58c7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 52,
    "yorig": 48
}