{
    "id": "0d108167-37f5-41b4-a773-6708b21051ef",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPortraitFrame",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 249,
    "bbox_left": 0,
    "bbox_right": 249,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ae6f431f-5f1a-494c-b7b4-e7569b983a2d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d108167-37f5-41b4-a773-6708b21051ef",
            "compositeImage": {
                "id": "3affe555-55fd-4a48-9d7f-309899a62069",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae6f431f-5f1a-494c-b7b4-e7569b983a2d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0e57c4c-c665-4658-9ccc-8fbe8620bf97",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae6f431f-5f1a-494c-b7b4-e7569b983a2d",
                    "LayerId": "4686bca5-d9a4-471c-9712-ec2675135d8d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 250,
    "layers": [
        {
            "id": "4686bca5-d9a4-471c-9712-ec2675135d8d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0d108167-37f5-41b4-a773-6708b21051ef",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 250,
    "xorig": 0,
    "yorig": 0
}