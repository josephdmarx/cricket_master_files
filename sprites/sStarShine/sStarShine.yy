{
    "id": "6ac9e69d-a8b0-4f81-9157-529d1775335c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sStarShine",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 0,
    "bbox_right": 21,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "11a450e9-3d0e-4df7-9def-11e2a79ec456",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6ac9e69d-a8b0-4f81-9157-529d1775335c",
            "compositeImage": {
                "id": "2fe78314-5b58-45cc-b20f-c6ec57474e53",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "11a450e9-3d0e-4df7-9def-11e2a79ec456",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf3e9c88-5018-426f-a3d3-32eb85377801",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "11a450e9-3d0e-4df7-9def-11e2a79ec456",
                    "LayerId": "b340a874-992b-4e2d-8282-88fdfcb9b303"
                }
            ]
        },
        {
            "id": "770cf4a2-dcf4-4350-8b66-2560122f606a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6ac9e69d-a8b0-4f81-9157-529d1775335c",
            "compositeImage": {
                "id": "04af8787-7b8e-4637-8516-cd015b308662",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "770cf4a2-dcf4-4350-8b66-2560122f606a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64d2bb87-cd31-440d-81b7-03b2b7ba94f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "770cf4a2-dcf4-4350-8b66-2560122f606a",
                    "LayerId": "b340a874-992b-4e2d-8282-88fdfcb9b303"
                }
            ]
        },
        {
            "id": "6db93a86-98ac-479c-842b-dbd3178fa00b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6ac9e69d-a8b0-4f81-9157-529d1775335c",
            "compositeImage": {
                "id": "68d07bcc-5bd7-4732-97ce-074ca2755b89",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6db93a86-98ac-479c-842b-dbd3178fa00b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "abc9c538-007f-470e-a421-4b0a9723ff39",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6db93a86-98ac-479c-842b-dbd3178fa00b",
                    "LayerId": "b340a874-992b-4e2d-8282-88fdfcb9b303"
                }
            ]
        },
        {
            "id": "ed4bbc57-7f0b-43ba-88b7-869808d8cb0a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6ac9e69d-a8b0-4f81-9157-529d1775335c",
            "compositeImage": {
                "id": "6314e2e3-995d-449d-9c55-96596d51ca06",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed4bbc57-7f0b-43ba-88b7-869808d8cb0a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6480cee1-b34e-4eab-87de-906256d864b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed4bbc57-7f0b-43ba-88b7-869808d8cb0a",
                    "LayerId": "b340a874-992b-4e2d-8282-88fdfcb9b303"
                }
            ]
        },
        {
            "id": "2bd2cb68-9f9f-4c84-ad0e-e02dbddab169",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6ac9e69d-a8b0-4f81-9157-529d1775335c",
            "compositeImage": {
                "id": "204b7b05-7a72-4ef0-a98d-1ce802115ad1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2bd2cb68-9f9f-4c84-ad0e-e02dbddab169",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4953d7fb-8701-4850-98ac-e5146e52bb10",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2bd2cb68-9f9f-4c84-ad0e-e02dbddab169",
                    "LayerId": "b340a874-992b-4e2d-8282-88fdfcb9b303"
                }
            ]
        },
        {
            "id": "552bbe44-0f08-48f0-bcde-43c4e8c869cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6ac9e69d-a8b0-4f81-9157-529d1775335c",
            "compositeImage": {
                "id": "7b8c1f6a-c3b3-4289-b6dd-b87a2b2bd5b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "552bbe44-0f08-48f0-bcde-43c4e8c869cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a334d2f1-4258-4c3e-a516-c025ab934c27",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "552bbe44-0f08-48f0-bcde-43c4e8c869cb",
                    "LayerId": "b340a874-992b-4e2d-8282-88fdfcb9b303"
                }
            ]
        },
        {
            "id": "70bf4810-bd42-402f-a965-b1b85e690a80",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6ac9e69d-a8b0-4f81-9157-529d1775335c",
            "compositeImage": {
                "id": "b570b9f6-1790-497b-a625-a9bd7b989cba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "70bf4810-bd42-402f-a965-b1b85e690a80",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "507814dd-6e87-47a2-be40-75e6927c2894",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "70bf4810-bd42-402f-a965-b1b85e690a80",
                    "LayerId": "b340a874-992b-4e2d-8282-88fdfcb9b303"
                }
            ]
        },
        {
            "id": "56c2dfaa-0cbe-4a61-a00d-c59501f910f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6ac9e69d-a8b0-4f81-9157-529d1775335c",
            "compositeImage": {
                "id": "612dcb3b-b3cf-4a49-8254-9c10d025848f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56c2dfaa-0cbe-4a61-a00d-c59501f910f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e826cb12-a457-4796-863a-17415a75486f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56c2dfaa-0cbe-4a61-a00d-c59501f910f0",
                    "LayerId": "b340a874-992b-4e2d-8282-88fdfcb9b303"
                }
            ]
        },
        {
            "id": "fc4d305e-bee4-4818-a0f1-fa7bdb796e51",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6ac9e69d-a8b0-4f81-9157-529d1775335c",
            "compositeImage": {
                "id": "61499fa1-6397-4c64-91c1-15f7b519069c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc4d305e-bee4-4818-a0f1-fa7bdb796e51",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2dfc7eb4-2d4c-4ead-a9ec-bda482221bc9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc4d305e-bee4-4818-a0f1-fa7bdb796e51",
                    "LayerId": "b340a874-992b-4e2d-8282-88fdfcb9b303"
                }
            ]
        },
        {
            "id": "ddd19733-0fe2-4016-a539-482dda91b954",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6ac9e69d-a8b0-4f81-9157-529d1775335c",
            "compositeImage": {
                "id": "2d624801-6c84-45ed-a2fa-fc83666b9c6f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ddd19733-0fe2-4016-a539-482dda91b954",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf3e5488-0168-494c-ad99-6cccd1ea71ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ddd19733-0fe2-4016-a539-482dda91b954",
                    "LayerId": "b340a874-992b-4e2d-8282-88fdfcb9b303"
                }
            ]
        },
        {
            "id": "a2358e12-6290-4691-9c7c-ef76497070f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6ac9e69d-a8b0-4f81-9157-529d1775335c",
            "compositeImage": {
                "id": "f9f27d69-ad6b-45e2-b60c-a5b2767c3421",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a2358e12-6290-4691-9c7c-ef76497070f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "151d8c94-50ab-449a-8360-7956d84f65a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2358e12-6290-4691-9c7c-ef76497070f9",
                    "LayerId": "b340a874-992b-4e2d-8282-88fdfcb9b303"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "b340a874-992b-4e2d-8282-88fdfcb9b303",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6ac9e69d-a8b0-4f81-9157-529d1775335c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}