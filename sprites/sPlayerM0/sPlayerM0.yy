{
    "id": "acfb9562-b076-44e9-a395-036b9dc29bd2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayerM0",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 94,
    "bbox_left": 58,
    "bbox_right": 72,
    "bbox_top": 42,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a70a3e3b-65b6-48bb-8650-af9ea5967209",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "acfb9562-b076-44e9-a395-036b9dc29bd2",
            "compositeImage": {
                "id": "3b25dfb8-3fb7-46bc-864b-203965a5ad5a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a70a3e3b-65b6-48bb-8650-af9ea5967209",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df64b793-f14b-48dd-8d00-1e4bd319d611",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a70a3e3b-65b6-48bb-8650-af9ea5967209",
                    "LayerId": "b9785e5f-b28a-4978-8f0d-32f67d2b77e6"
                }
            ]
        },
        {
            "id": "cfb9f23f-907a-48c0-b5c7-52317e642842",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "acfb9562-b076-44e9-a395-036b9dc29bd2",
            "compositeImage": {
                "id": "6aaeff45-6052-4e36-bb0d-124414ece9b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cfb9f23f-907a-48c0-b5c7-52317e642842",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "78ab0e47-c401-4f41-83b4-67ae702fc192",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cfb9f23f-907a-48c0-b5c7-52317e642842",
                    "LayerId": "b9785e5f-b28a-4978-8f0d-32f67d2b77e6"
                }
            ]
        },
        {
            "id": "e82586e3-3780-45ba-af6e-f31eeb58459c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "acfb9562-b076-44e9-a395-036b9dc29bd2",
            "compositeImage": {
                "id": "3b37014c-fe0a-4fa5-8f88-d1fe01732b50",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e82586e3-3780-45ba-af6e-f31eeb58459c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58aa16be-2429-4d9f-ae31-c715226ada3b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e82586e3-3780-45ba-af6e-f31eeb58459c",
                    "LayerId": "b9785e5f-b28a-4978-8f0d-32f67d2b77e6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "b9785e5f-b28a-4978-8f0d-32f67d2b77e6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "acfb9562-b076-44e9-a395-036b9dc29bd2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}