{
    "id": "ae82bc85-070f-4545-badf-f862855d7d2d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTrees02",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 359,
    "bbox_left": 0,
    "bbox_right": 639,
    "bbox_top": 120,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0fdb9adf-9247-4e1b-910a-1c478e3212d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae82bc85-070f-4545-badf-f862855d7d2d",
            "compositeImage": {
                "id": "346f53ce-4957-418e-915b-63e65b607e9f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0fdb9adf-9247-4e1b-910a-1c478e3212d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7438901d-94b5-471d-83e8-3cac251c9fba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0fdb9adf-9247-4e1b-910a-1c478e3212d3",
                    "LayerId": "7dea1921-ac52-4174-b2f7-cb472658873c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 360,
    "layers": [
        {
            "id": "7dea1921-ac52-4174-b2f7-cb472658873c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ae82bc85-070f-4545-badf-f862855d7d2d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 0,
    "yorig": 0
}