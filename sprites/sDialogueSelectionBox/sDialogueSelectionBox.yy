{
    "id": "2321990e-b85c-4b13-9639-13ab7a06a093",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sDialogueSelectionBox",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 0,
    "bbox_right": 239,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "eb8c7a8a-4bc1-4393-aac4-e53fb1131da7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2321990e-b85c-4b13-9639-13ab7a06a093",
            "compositeImage": {
                "id": "53dc92d6-4b4d-4e0b-bcf2-f3bee30125b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb8c7a8a-4bc1-4393-aac4-e53fb1131da7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "676e36c6-1d27-44fa-b897-9faf9022d203",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb8c7a8a-4bc1-4393-aac4-e53fb1131da7",
                    "LayerId": "f12830d6-93cb-450a-be28-7f1c1b169135"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 30,
    "layers": [
        {
            "id": "f12830d6-93cb-450a-be28-7f1c1b169135",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2321990e-b85c-4b13-9639-13ab7a06a093",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 240,
    "xorig": 0,
    "yorig": 0
}