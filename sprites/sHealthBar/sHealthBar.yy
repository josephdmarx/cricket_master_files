{
    "id": "56c7c67e-c688-4c7a-8d51-1b97c339264a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sHealthBar",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 16,
    "bbox_left": 0,
    "bbox_right": 93,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f952a7a4-88cd-4f08-b755-95ba575eb8ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "56c7c67e-c688-4c7a-8d51-1b97c339264a",
            "compositeImage": {
                "id": "d0668b08-b3b1-4cdf-9b0b-808b4302042d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f952a7a4-88cd-4f08-b755-95ba575eb8ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7350b3e8-1c3a-468a-a1c4-ccf146a6ccac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f952a7a4-88cd-4f08-b755-95ba575eb8ec",
                    "LayerId": "01c9f6a5-db1d-4d0a-aeb4-ed5f3dad7b19"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 17,
    "layers": [
        {
            "id": "01c9f6a5-db1d-4d0a-aeb4-ed5f3dad7b19",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "56c7c67e-c688-4c7a-8d51-1b97c339264a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 94,
    "xorig": 0,
    "yorig": 0
}