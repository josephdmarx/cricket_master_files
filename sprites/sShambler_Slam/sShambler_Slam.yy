{
    "id": "ddfe4b8d-03ea-45b6-88f9-19506e3cb2ec",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sShambler_Slam",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 198,
    "bbox_left": 75,
    "bbox_right": 155,
    "bbox_top": 110,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cea4652a-19c2-4091-94d8-d5139a6892e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ddfe4b8d-03ea-45b6-88f9-19506e3cb2ec",
            "compositeImage": {
                "id": "546551b7-84e9-480c-add2-b15ff2ad5167",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cea4652a-19c2-4091-94d8-d5139a6892e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c5deb99-57fc-45a8-b928-105d0a63422c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cea4652a-19c2-4091-94d8-d5139a6892e2",
                    "LayerId": "e40fc4bb-f8b5-4402-ab74-20c102f566c5"
                }
            ]
        },
        {
            "id": "fdef6ebd-fad2-4193-bf37-0602c737d351",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ddfe4b8d-03ea-45b6-88f9-19506e3cb2ec",
            "compositeImage": {
                "id": "708630b1-e707-4b26-b588-155723b0572b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fdef6ebd-fad2-4193-bf37-0602c737d351",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3cb65042-22a7-4c76-91f4-543e6b703885",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fdef6ebd-fad2-4193-bf37-0602c737d351",
                    "LayerId": "e40fc4bb-f8b5-4402-ab74-20c102f566c5"
                }
            ]
        },
        {
            "id": "48712c41-019a-46cb-843a-c79565bcd6a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ddfe4b8d-03ea-45b6-88f9-19506e3cb2ec",
            "compositeImage": {
                "id": "4550d71a-d10e-4658-80e9-472e6e2adc6c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "48712c41-019a-46cb-843a-c79565bcd6a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd4613d0-0513-4d87-9d40-357b8df1f56c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48712c41-019a-46cb-843a-c79565bcd6a8",
                    "LayerId": "e40fc4bb-f8b5-4402-ab74-20c102f566c5"
                }
            ]
        },
        {
            "id": "d90f7f20-b4b5-4d2a-b19c-4e7b1c2a8bc7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ddfe4b8d-03ea-45b6-88f9-19506e3cb2ec",
            "compositeImage": {
                "id": "7415b980-3b73-4642-85de-d2bfabd6a8ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d90f7f20-b4b5-4d2a-b19c-4e7b1c2a8bc7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21d298af-d7d2-439c-84d5-e9e0084b68ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d90f7f20-b4b5-4d2a-b19c-4e7b1c2a8bc7",
                    "LayerId": "e40fc4bb-f8b5-4402-ab74-20c102f566c5"
                }
            ]
        },
        {
            "id": "e58c37a3-bd02-4395-aaa9-1a05917c2cc3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ddfe4b8d-03ea-45b6-88f9-19506e3cb2ec",
            "compositeImage": {
                "id": "8dfa1765-c978-4171-b772-a8d56f4b0cfb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e58c37a3-bd02-4395-aaa9-1a05917c2cc3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "454adb2d-018a-4e4b-b076-89536adf60dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e58c37a3-bd02-4395-aaa9-1a05917c2cc3",
                    "LayerId": "e40fc4bb-f8b5-4402-ab74-20c102f566c5"
                }
            ]
        },
        {
            "id": "45d9c27f-7a04-4034-bbbf-36dc326282b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ddfe4b8d-03ea-45b6-88f9-19506e3cb2ec",
            "compositeImage": {
                "id": "fbac4900-eb2a-429c-b55c-c898d72eb515",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "45d9c27f-7a04-4034-bbbf-36dc326282b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3cd79038-aeec-43bb-9451-801575ae6c5e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "45d9c27f-7a04-4034-bbbf-36dc326282b3",
                    "LayerId": "e40fc4bb-f8b5-4402-ab74-20c102f566c5"
                }
            ]
        },
        {
            "id": "176b97e9-66e7-41e6-8068-523881f5fb05",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ddfe4b8d-03ea-45b6-88f9-19506e3cb2ec",
            "compositeImage": {
                "id": "1a6a3258-1b07-4cf4-a137-128fe8ac2e80",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "176b97e9-66e7-41e6-8068-523881f5fb05",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20bb5250-0981-4df1-af21-bd30f8f2a4e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "176b97e9-66e7-41e6-8068-523881f5fb05",
                    "LayerId": "e40fc4bb-f8b5-4402-ab74-20c102f566c5"
                }
            ]
        },
        {
            "id": "7336a8c6-106f-4096-8127-895a1b137023",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ddfe4b8d-03ea-45b6-88f9-19506e3cb2ec",
            "compositeImage": {
                "id": "e3ad3a33-1d31-44b3-a925-9bf6725cc928",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7336a8c6-106f-4096-8127-895a1b137023",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c660385-5cdd-4857-b4f2-95f065dfdac4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7336a8c6-106f-4096-8127-895a1b137023",
                    "LayerId": "e40fc4bb-f8b5-4402-ab74-20c102f566c5"
                }
            ]
        },
        {
            "id": "04c96330-db21-468a-b54f-b01dd0f91996",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ddfe4b8d-03ea-45b6-88f9-19506e3cb2ec",
            "compositeImage": {
                "id": "a41f9c41-b50b-43a5-bbbf-47fb99c3d4ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "04c96330-db21-468a-b54f-b01dd0f91996",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ed6a6ab-ed0e-40af-baac-af00e5408e4c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "04c96330-db21-468a-b54f-b01dd0f91996",
                    "LayerId": "e40fc4bb-f8b5-4402-ab74-20c102f566c5"
                }
            ]
        },
        {
            "id": "441b1764-c1c9-43ab-a928-532494665bf9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ddfe4b8d-03ea-45b6-88f9-19506e3cb2ec",
            "compositeImage": {
                "id": "d6ae3660-9edc-4d58-803e-ad83f03fc8c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "441b1764-c1c9-43ab-a928-532494665bf9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c6288784-f5a9-4ec4-9c77-3520a7bcfadb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "441b1764-c1c9-43ab-a928-532494665bf9",
                    "LayerId": "e40fc4bb-f8b5-4402-ab74-20c102f566c5"
                }
            ]
        },
        {
            "id": "22dd7d52-9043-4495-9d26-1b5b69e11f57",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ddfe4b8d-03ea-45b6-88f9-19506e3cb2ec",
            "compositeImage": {
                "id": "920749c4-ab33-46c7-9784-b1fea473d3d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22dd7d52-9043-4495-9d26-1b5b69e11f57",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b5f4a99-59f3-4d01-8b9d-6208c53f489a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22dd7d52-9043-4495-9d26-1b5b69e11f57",
                    "LayerId": "e40fc4bb-f8b5-4402-ab74-20c102f566c5"
                }
            ]
        },
        {
            "id": "4a455bd6-6ce3-4c37-a875-1a4ef4f2c413",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ddfe4b8d-03ea-45b6-88f9-19506e3cb2ec",
            "compositeImage": {
                "id": "c16f1e10-b3ad-4def-b22c-78d3aa206c89",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a455bd6-6ce3-4c37-a875-1a4ef4f2c413",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ed4c42e-4d02-4323-b7b2-09e2ce42ca98",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a455bd6-6ce3-4c37-a875-1a4ef4f2c413",
                    "LayerId": "e40fc4bb-f8b5-4402-ab74-20c102f566c5"
                }
            ]
        },
        {
            "id": "4d7a4477-a700-4a92-8911-52b42da217ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ddfe4b8d-03ea-45b6-88f9-19506e3cb2ec",
            "compositeImage": {
                "id": "2ed1f0eb-0b72-4dcb-813d-80ee26efbc4c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d7a4477-a700-4a92-8911-52b42da217ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "67c4f659-7f69-4c59-bbf2-c55c47a7a260",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d7a4477-a700-4a92-8911-52b42da217ac",
                    "LayerId": "e40fc4bb-f8b5-4402-ab74-20c102f566c5"
                }
            ]
        },
        {
            "id": "f27e5eb6-427e-423a-986c-239f172fe620",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ddfe4b8d-03ea-45b6-88f9-19506e3cb2ec",
            "compositeImage": {
                "id": "2992e781-74cc-40a0-b48f-e5c1e42e992b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f27e5eb6-427e-423a-986c-239f172fe620",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e1fad22-ba11-4f6a-953d-4efb6c16d8f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f27e5eb6-427e-423a-986c-239f172fe620",
                    "LayerId": "e40fc4bb-f8b5-4402-ab74-20c102f566c5"
                }
            ]
        },
        {
            "id": "9d7de8fd-1b0e-4455-bc13-9783627d73e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ddfe4b8d-03ea-45b6-88f9-19506e3cb2ec",
            "compositeImage": {
                "id": "0e9c0ab3-d921-42b1-8b76-7677d1b2d396",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d7de8fd-1b0e-4455-bc13-9783627d73e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57fdb11a-52ea-4dbb-b385-379e1c752c4d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d7de8fd-1b0e-4455-bc13-9783627d73e1",
                    "LayerId": "e40fc4bb-f8b5-4402-ab74-20c102f566c5"
                }
            ]
        },
        {
            "id": "29f2dcd5-0361-476a-8a5a-73b2fa040e35",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ddfe4b8d-03ea-45b6-88f9-19506e3cb2ec",
            "compositeImage": {
                "id": "4e78b257-2084-461c-9bc5-d32280a41f4c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29f2dcd5-0361-476a-8a5a-73b2fa040e35",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2af8a5ca-cf17-4050-84b6-42dd1ee041a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29f2dcd5-0361-476a-8a5a-73b2fa040e35",
                    "LayerId": "e40fc4bb-f8b5-4402-ab74-20c102f566c5"
                }
            ]
        },
        {
            "id": "fd41a569-aa2e-4b87-932e-97187b497814",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ddfe4b8d-03ea-45b6-88f9-19506e3cb2ec",
            "compositeImage": {
                "id": "a87c1c8a-b35b-4601-926a-9617c584c0c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd41a569-aa2e-4b87-932e-97187b497814",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef1c1678-147e-443b-b28a-ece8909d40b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd41a569-aa2e-4b87-932e-97187b497814",
                    "LayerId": "e40fc4bb-f8b5-4402-ab74-20c102f566c5"
                }
            ]
        },
        {
            "id": "db222275-c84d-41f0-888e-9b9654f44cee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ddfe4b8d-03ea-45b6-88f9-19506e3cb2ec",
            "compositeImage": {
                "id": "21b3073e-b530-4752-90dc-20ceb9b339fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db222275-c84d-41f0-888e-9b9654f44cee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b2ac970-64c7-4acd-b3f0-1d147f0b5db9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db222275-c84d-41f0-888e-9b9654f44cee",
                    "LayerId": "e40fc4bb-f8b5-4402-ab74-20c102f566c5"
                }
            ]
        },
        {
            "id": "e73ea342-8ae4-4552-923a-20d479f9e043",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ddfe4b8d-03ea-45b6-88f9-19506e3cb2ec",
            "compositeImage": {
                "id": "5b08767c-6e18-443a-9e02-facb95e1e0f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e73ea342-8ae4-4552-923a-20d479f9e043",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58ab5063-efeb-4c1d-90ab-2aee9830c23b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e73ea342-8ae4-4552-923a-20d479f9e043",
                    "LayerId": "e40fc4bb-f8b5-4402-ab74-20c102f566c5"
                }
            ]
        },
        {
            "id": "d96e6c7d-81b7-4d22-8c40-713a151b1519",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ddfe4b8d-03ea-45b6-88f9-19506e3cb2ec",
            "compositeImage": {
                "id": "f8a3eb19-7880-4560-a35b-14046c1564ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d96e6c7d-81b7-4d22-8c40-713a151b1519",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "317c40cd-19d6-4f3a-bdb5-7f806b96048a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d96e6c7d-81b7-4d22-8c40-713a151b1519",
                    "LayerId": "e40fc4bb-f8b5-4402-ab74-20c102f566c5"
                }
            ]
        },
        {
            "id": "49bbce0d-72e6-4881-afab-2e54aacc21c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ddfe4b8d-03ea-45b6-88f9-19506e3cb2ec",
            "compositeImage": {
                "id": "22b45b03-fe2b-4bdf-9bfd-dccbba3f9bb9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "49bbce0d-72e6-4881-afab-2e54aacc21c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f8b773b-1349-45fe-ab81-d710ee31013d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49bbce0d-72e6-4881-afab-2e54aacc21c0",
                    "LayerId": "e40fc4bb-f8b5-4402-ab74-20c102f566c5"
                }
            ]
        },
        {
            "id": "7fc71946-d89d-44fd-8e1e-a80f53921a26",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ddfe4b8d-03ea-45b6-88f9-19506e3cb2ec",
            "compositeImage": {
                "id": "6d5abf75-5288-4db9-82e8-515cb4d125fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7fc71946-d89d-44fd-8e1e-a80f53921a26",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "43cd1644-a381-410e-9d85-dcafb4ec411e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7fc71946-d89d-44fd-8e1e-a80f53921a26",
                    "LayerId": "e40fc4bb-f8b5-4402-ab74-20c102f566c5"
                }
            ]
        },
        {
            "id": "1a9432e4-165f-4fa5-a4c3-9bea0dcbde53",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ddfe4b8d-03ea-45b6-88f9-19506e3cb2ec",
            "compositeImage": {
                "id": "1a8d2853-5921-4f0d-9ba1-933879da88db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a9432e4-165f-4fa5-a4c3-9bea0dcbde53",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a821d0d9-217e-4b4a-b919-7000970811cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a9432e4-165f-4fa5-a4c3-9bea0dcbde53",
                    "LayerId": "e40fc4bb-f8b5-4402-ab74-20c102f566c5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 215,
    "layers": [
        {
            "id": "e40fc4bb-f8b5-4402-ab74-20c102f566c5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ddfe4b8d-03ea-45b6-88f9-19506e3cb2ec",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 258,
    "xorig": 129,
    "yorig": 107
}