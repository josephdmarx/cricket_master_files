{
    "id": "6ff411b9-ac76-49ed-aa03-b8b358b832da",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCaveBlackout",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1fba4fb8-2187-4938-a4a4-fb03f254339b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6ff411b9-ac76-49ed-aa03-b8b358b832da",
            "compositeImage": {
                "id": "697ed5cc-8787-4e04-93b5-c8bdd17b85cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1fba4fb8-2187-4938-a4a4-fb03f254339b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1843bed-de5c-419e-86d0-6c5a12f228bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1fba4fb8-2187-4938-a4a4-fb03f254339b",
                    "LayerId": "7b6108b3-a4c1-4f1e-b6c4-9725eeae12ef"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "7b6108b3-a4c1-4f1e-b6c4-9725eeae12ef",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6ff411b9-ac76-49ed-aa03-b8b358b832da",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}