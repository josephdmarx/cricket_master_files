{
    "id": "c3ce15a6-9a5e-4f74-a8c7-1f83776a178a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSplash",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 6,
    "bbox_left": 1,
    "bbox_right": 6,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8a501cc9-45b8-499a-84ea-daa24b744995",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c3ce15a6-9a5e-4f74-a8c7-1f83776a178a",
            "compositeImage": {
                "id": "dc958e90-197a-49f1-9e04-f210b7e45aec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a501cc9-45b8-499a-84ea-daa24b744995",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6bdd224f-41e6-4e4e-bfe3-b1c276b54aa9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a501cc9-45b8-499a-84ea-daa24b744995",
                    "LayerId": "167e76f8-6595-4cf0-8246-5f71562e0e5e"
                }
            ]
        },
        {
            "id": "1cd3b311-2f5f-42e0-bee8-039d0b19bce6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c3ce15a6-9a5e-4f74-a8c7-1f83776a178a",
            "compositeImage": {
                "id": "777f25df-210d-4d51-99ba-4a42297835bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1cd3b311-2f5f-42e0-bee8-039d0b19bce6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ddfb598-d9d0-4e24-a0a0-a26f1a215260",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1cd3b311-2f5f-42e0-bee8-039d0b19bce6",
                    "LayerId": "167e76f8-6595-4cf0-8246-5f71562e0e5e"
                }
            ]
        },
        {
            "id": "77e83f8e-4176-479f-a751-4028acaf045e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c3ce15a6-9a5e-4f74-a8c7-1f83776a178a",
            "compositeImage": {
                "id": "b53ba125-f2b5-4518-9306-0ced6235152e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77e83f8e-4176-479f-a751-4028acaf045e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d55e6cfa-7434-419d-855f-18f02a5b7b43",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77e83f8e-4176-479f-a751-4028acaf045e",
                    "LayerId": "167e76f8-6595-4cf0-8246-5f71562e0e5e"
                }
            ]
        },
        {
            "id": "aa193001-e9fd-4eef-8ee6-157b72636eab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c3ce15a6-9a5e-4f74-a8c7-1f83776a178a",
            "compositeImage": {
                "id": "c917c78e-2777-472a-9796-6947c133a5ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa193001-e9fd-4eef-8ee6-157b72636eab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec59b059-2af3-442e-8a43-aa5178ab703f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa193001-e9fd-4eef-8ee6-157b72636eab",
                    "LayerId": "167e76f8-6595-4cf0-8246-5f71562e0e5e"
                }
            ]
        },
        {
            "id": "e95101f2-4d95-4dbd-979b-765bc79e223c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c3ce15a6-9a5e-4f74-a8c7-1f83776a178a",
            "compositeImage": {
                "id": "4aab4129-fc08-4cb4-934e-b9a4e7a85ffc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e95101f2-4d95-4dbd-979b-765bc79e223c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eafca95e-03bb-4ea2-94ff-2c4701e38ab5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e95101f2-4d95-4dbd-979b-765bc79e223c",
                    "LayerId": "167e76f8-6595-4cf0-8246-5f71562e0e5e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "167e76f8-6595-4cf0-8246-5f71562e0e5e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c3ce15a6-9a5e-4f74-a8c7-1f83776a178a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}