{
    "id": "64444592-9784-4c18-9c8d-f3cbee1a36f4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSpell01F",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 12,
    "bbox_right": 19,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "53b42bb6-a2df-48ed-8204-0dfd432d2279",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "64444592-9784-4c18-9c8d-f3cbee1a36f4",
            "compositeImage": {
                "id": "a882429f-6834-4d93-83dc-28066b823c0b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "53b42bb6-a2df-48ed-8204-0dfd432d2279",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2aa8bcee-7c09-4f65-9362-856ab30d849d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "53b42bb6-a2df-48ed-8204-0dfd432d2279",
                    "LayerId": "a9849c9b-6e3f-43ba-be05-d20156fdeeec"
                }
            ]
        },
        {
            "id": "1a6024c4-559c-4129-b5c7-888fbe84dbd8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "64444592-9784-4c18-9c8d-f3cbee1a36f4",
            "compositeImage": {
                "id": "2f027680-ddee-4dc6-b889-cac87eb754cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a6024c4-559c-4129-b5c7-888fbe84dbd8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f76c1b0-bd55-4a0b-81e6-30777c7df32f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a6024c4-559c-4129-b5c7-888fbe84dbd8",
                    "LayerId": "a9849c9b-6e3f-43ba-be05-d20156fdeeec"
                }
            ]
        },
        {
            "id": "131a0edd-36b1-4247-b70f-ed226a7d8d34",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "64444592-9784-4c18-9c8d-f3cbee1a36f4",
            "compositeImage": {
                "id": "6bccdef1-392f-4cb9-afb0-76c474153f9c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "131a0edd-36b1-4247-b70f-ed226a7d8d34",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32258a33-96ec-4611-88be-9868173c65f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "131a0edd-36b1-4247-b70f-ed226a7d8d34",
                    "LayerId": "a9849c9b-6e3f-43ba-be05-d20156fdeeec"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "a9849c9b-6e3f-43ba-be05-d20156fdeeec",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "64444592-9784-4c18-9c8d-f3cbee1a36f4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}