{
    "id": "a442355a-873b-4a8b-bd48-87239a4c1458",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite52",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 359,
    "bbox_left": 0,
    "bbox_right": 639,
    "bbox_top": 226,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6ec7ce15-4503-4b8b-94fb-feef4f1c021c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a442355a-873b-4a8b-bd48-87239a4c1458",
            "compositeImage": {
                "id": "9d476b39-bccb-4d0b-b4e4-f1014834323b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ec7ce15-4503-4b8b-94fb-feef4f1c021c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b85d31f9-158f-470c-a41b-e64bcb2423f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ec7ce15-4503-4b8b-94fb-feef4f1c021c",
                    "LayerId": "7cdc4553-c346-469c-9d0d-aca47aa23085"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 360,
    "layers": [
        {
            "id": "7cdc4553-c346-469c-9d0d-aca47aa23085",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a442355a-873b-4a8b-bd48-87239a4c1458",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 0,
    "yorig": 0
}