{
    "id": "39fe1548-73c2-4610-8c2d-99a4e7f5b341",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayerNet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 94,
    "bbox_left": 58,
    "bbox_right": 72,
    "bbox_top": 42,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3af23b2b-6569-46ef-ae60-0200c76b5782",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "39fe1548-73c2-4610-8c2d-99a4e7f5b341",
            "compositeImage": {
                "id": "928e23b2-d351-4b27-8ccb-51af1b20e2b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3af23b2b-6569-46ef-ae60-0200c76b5782",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec64e61c-789b-4256-9271-5623d45fab8a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3af23b2b-6569-46ef-ae60-0200c76b5782",
                    "LayerId": "35624885-4634-4331-8415-c7f2d71df08b"
                }
            ]
        },
        {
            "id": "6d63af47-c0c5-4d58-8063-52ba2ecbdb88",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "39fe1548-73c2-4610-8c2d-99a4e7f5b341",
            "compositeImage": {
                "id": "f42bdf8e-c66a-40b3-a7e0-3349cf090ba7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d63af47-c0c5-4d58-8063-52ba2ecbdb88",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4334b87-f908-4c8c-ac16-86b7ca49b834",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d63af47-c0c5-4d58-8063-52ba2ecbdb88",
                    "LayerId": "35624885-4634-4331-8415-c7f2d71df08b"
                }
            ]
        },
        {
            "id": "7ef94afa-02dc-47d8-b056-d8e1c72fba2f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "39fe1548-73c2-4610-8c2d-99a4e7f5b341",
            "compositeImage": {
                "id": "3ccec4a1-76e6-4ff3-a841-f3ba5d8fdca0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ef94afa-02dc-47d8-b056-d8e1c72fba2f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ac633af-b656-4be6-b3d0-94fc99515558",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ef94afa-02dc-47d8-b056-d8e1c72fba2f",
                    "LayerId": "35624885-4634-4331-8415-c7f2d71df08b"
                }
            ]
        },
        {
            "id": "ab000f46-2cf1-4616-9fd9-bbeb515fcd84",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "39fe1548-73c2-4610-8c2d-99a4e7f5b341",
            "compositeImage": {
                "id": "8c8c75c4-dd76-4624-a0c7-f9d17bdf6462",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab000f46-2cf1-4616-9fd9-bbeb515fcd84",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa97d28d-6280-4eba-bd24-3994a410e02f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab000f46-2cf1-4616-9fd9-bbeb515fcd84",
                    "LayerId": "35624885-4634-4331-8415-c7f2d71df08b"
                }
            ]
        },
        {
            "id": "203e9f68-a732-4c6a-9700-6b9c8b411977",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "39fe1548-73c2-4610-8c2d-99a4e7f5b341",
            "compositeImage": {
                "id": "b50b08d3-4823-4ae5-851e-d9ad5b816861",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "203e9f68-a732-4c6a-9700-6b9c8b411977",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a74457a9-6625-4b7f-8b0f-d6d0e39481e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "203e9f68-a732-4c6a-9700-6b9c8b411977",
                    "LayerId": "35624885-4634-4331-8415-c7f2d71df08b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "35624885-4634-4331-8415-c7f2d71df08b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "39fe1548-73c2-4610-8c2d-99a4e7f5b341",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}