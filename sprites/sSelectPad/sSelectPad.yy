{
    "id": "006dcc85-2740-40ef-a990-ac758e15419b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSelectPad",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 6,
    "bbox_right": 25,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d4406e84-3830-4566-b649-fb93cf3808f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "006dcc85-2740-40ef-a990-ac758e15419b",
            "compositeImage": {
                "id": "b00ff62c-1928-4e18-a457-67eefa2455d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4406e84-3830-4566-b649-fb93cf3808f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "61880949-b11a-4be3-a579-811da53374be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4406e84-3830-4566-b649-fb93cf3808f1",
                    "LayerId": "618e09f5-207f-4239-8e05-8617283cb7e7"
                }
            ]
        },
        {
            "id": "a03f914b-da77-48f1-bba0-40e0c270cbef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "006dcc85-2740-40ef-a990-ac758e15419b",
            "compositeImage": {
                "id": "27a33925-cc24-43ee-ad8b-b8f3c740f422",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a03f914b-da77-48f1-bba0-40e0c270cbef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8dff1ec3-76b2-4c00-9cba-a76a32ce724c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a03f914b-da77-48f1-bba0-40e0c270cbef",
                    "LayerId": "618e09f5-207f-4239-8e05-8617283cb7e7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "618e09f5-207f-4239-8e05-8617283cb7e7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "006dcc85-2740-40ef-a990-ac758e15419b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 15,
    "yorig": 25
}