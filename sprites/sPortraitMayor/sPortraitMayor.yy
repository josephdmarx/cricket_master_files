{
    "id": "4c165403-63d2-4c52-a6cd-424114609ae2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPortraitMayor",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 9,
    "bbox_right": 95,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1f5cb928-80f1-48a6-8f25-6637fbfe8e58",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4c165403-63d2-4c52-a6cd-424114609ae2",
            "compositeImage": {
                "id": "2f98ba92-9a28-4638-af93-eb0937ccc526",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f5cb928-80f1-48a6-8f25-6637fbfe8e58",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc7ca74b-acf9-4929-a033-13610269a6fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f5cb928-80f1-48a6-8f25-6637fbfe8e58",
                    "LayerId": "9fed5207-d66d-44e6-b2af-0f171302f0ce"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "9fed5207-d66d-44e6-b2af-0f171302f0ce",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4c165403-63d2-4c52-a6cd-424114609ae2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 0,
    "yorig": 0
}