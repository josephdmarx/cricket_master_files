{
    "id": "c3885661-43f3-453c-9099-d4eb38b1667f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sWaterFallSection",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3e0402d1-b483-4cc8-b554-35de8d3780ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c3885661-43f3-453c-9099-d4eb38b1667f",
            "compositeImage": {
                "id": "e547ebb2-24da-4efd-953b-155cbb07bc99",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e0402d1-b483-4cc8-b554-35de8d3780ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c1494cb-9bfe-43af-802f-dca4cfcd0ca5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e0402d1-b483-4cc8-b554-35de8d3780ca",
                    "LayerId": "eee0ee0b-68ab-4d52-9c38-88f893f30ca4"
                }
            ]
        },
        {
            "id": "c4c63074-82b1-44ec-b047-794658bd889f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c3885661-43f3-453c-9099-d4eb38b1667f",
            "compositeImage": {
                "id": "7d6435c0-4744-4a28-a828-02731fb60335",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c4c63074-82b1-44ec-b047-794658bd889f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "709e39af-cc0e-4110-a2cd-33de4c347972",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c4c63074-82b1-44ec-b047-794658bd889f",
                    "LayerId": "eee0ee0b-68ab-4d52-9c38-88f893f30ca4"
                }
            ]
        },
        {
            "id": "dda174af-01a2-4db5-a340-6fb638f29b4c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c3885661-43f3-453c-9099-d4eb38b1667f",
            "compositeImage": {
                "id": "2a9f78ac-03de-4406-866a-07c6ccdd0650",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dda174af-01a2-4db5-a340-6fb638f29b4c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d5b7943-1307-46f0-9cb5-6dfe512aeda4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dda174af-01a2-4db5-a340-6fb638f29b4c",
                    "LayerId": "eee0ee0b-68ab-4d52-9c38-88f893f30ca4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "eee0ee0b-68ab-4d52-9c38-88f893f30ca4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c3885661-43f3-453c-9099-d4eb38b1667f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}