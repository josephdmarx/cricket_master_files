{
    "id": "163c1124-02e9-454a-acf2-a5d498b93b23",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sMushman_Wake",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 94,
    "bbox_left": 44,
    "bbox_right": 84,
    "bbox_top": 30,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2c51d110-7900-4e07-93ac-434ff0ff3482",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "163c1124-02e9-454a-acf2-a5d498b93b23",
            "compositeImage": {
                "id": "7958e9da-39d2-41ac-88b8-8fb17bd6a40e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c51d110-7900-4e07-93ac-434ff0ff3482",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1bae5d5-7599-47c1-b394-a9ff6de3af0c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c51d110-7900-4e07-93ac-434ff0ff3482",
                    "LayerId": "b38d9adb-15a8-4757-bbc9-0c14d6dac0a0"
                }
            ]
        },
        {
            "id": "58a641da-efb7-4d99-b6c7-b45996d50402",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "163c1124-02e9-454a-acf2-a5d498b93b23",
            "compositeImage": {
                "id": "b8cd7e4f-b36f-4a58-b23f-9b49698aa1de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "58a641da-efb7-4d99-b6c7-b45996d50402",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b17f446-a525-430e-9c03-fa7a88994f80",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "58a641da-efb7-4d99-b6c7-b45996d50402",
                    "LayerId": "b38d9adb-15a8-4757-bbc9-0c14d6dac0a0"
                }
            ]
        },
        {
            "id": "ec97ee1b-ba2c-4a04-9432-9e50a5f50cc7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "163c1124-02e9-454a-acf2-a5d498b93b23",
            "compositeImage": {
                "id": "12910742-76de-4e76-9b9e-fab8677b7825",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec97ee1b-ba2c-4a04-9432-9e50a5f50cc7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea627204-c440-4d0a-8e8e-75c08fa483f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec97ee1b-ba2c-4a04-9432-9e50a5f50cc7",
                    "LayerId": "b38d9adb-15a8-4757-bbc9-0c14d6dac0a0"
                }
            ]
        },
        {
            "id": "c30edcf0-630b-4b31-b885-b963e3497e09",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "163c1124-02e9-454a-acf2-a5d498b93b23",
            "compositeImage": {
                "id": "df020358-dea1-4d02-b219-1b044cdcd113",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c30edcf0-630b-4b31-b885-b963e3497e09",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a4a0c3a-b441-4e3b-8232-e4229681399f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c30edcf0-630b-4b31-b885-b963e3497e09",
                    "LayerId": "b38d9adb-15a8-4757-bbc9-0c14d6dac0a0"
                }
            ]
        },
        {
            "id": "8b37cfc9-358d-4d27-994e-920ee0f0928b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "163c1124-02e9-454a-acf2-a5d498b93b23",
            "compositeImage": {
                "id": "38f4f19f-d05a-43af-88ce-35b3b0d65b89",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b37cfc9-358d-4d27-994e-920ee0f0928b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6dca5c2-cabd-4237-a198-11e535a95d6e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b37cfc9-358d-4d27-994e-920ee0f0928b",
                    "LayerId": "b38d9adb-15a8-4757-bbc9-0c14d6dac0a0"
                }
            ]
        },
        {
            "id": "bf48231d-0274-45b7-9adf-ab099168a069",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "163c1124-02e9-454a-acf2-a5d498b93b23",
            "compositeImage": {
                "id": "16622534-27db-414d-aa5d-ba9ff20d92b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf48231d-0274-45b7-9adf-ab099168a069",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2be98ae-afef-464f-b9ab-fc2edcc66041",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf48231d-0274-45b7-9adf-ab099168a069",
                    "LayerId": "b38d9adb-15a8-4757-bbc9-0c14d6dac0a0"
                }
            ]
        },
        {
            "id": "a1f63598-d6b9-44ba-b3a3-6db81ee53222",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "163c1124-02e9-454a-acf2-a5d498b93b23",
            "compositeImage": {
                "id": "8e702286-08b7-46fe-aad1-e197690fefb1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1f63598-d6b9-44ba-b3a3-6db81ee53222",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a1114c16-986d-46dc-abb7-be321aebbffa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1f63598-d6b9-44ba-b3a3-6db81ee53222",
                    "LayerId": "b38d9adb-15a8-4757-bbc9-0c14d6dac0a0"
                }
            ]
        },
        {
            "id": "e80a9139-c787-4c7f-9c71-a87ca23db0b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "163c1124-02e9-454a-acf2-a5d498b93b23",
            "compositeImage": {
                "id": "de106609-8279-4aa2-bbcd-f5bf24d37fe7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e80a9139-c787-4c7f-9c71-a87ca23db0b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b405d84b-d394-4625-ac67-b95e9138ded8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e80a9139-c787-4c7f-9c71-a87ca23db0b1",
                    "LayerId": "b38d9adb-15a8-4757-bbc9-0c14d6dac0a0"
                }
            ]
        },
        {
            "id": "4a1ac698-0e35-4572-8844-6b5183554178",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "163c1124-02e9-454a-acf2-a5d498b93b23",
            "compositeImage": {
                "id": "389e5191-ed64-43c0-b5c0-e34862af646c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a1ac698-0e35-4572-8844-6b5183554178",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d62c4d5-340e-468a-81d1-5aca2d88162f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a1ac698-0e35-4572-8844-6b5183554178",
                    "LayerId": "b38d9adb-15a8-4757-bbc9-0c14d6dac0a0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "b38d9adb-15a8-4757-bbc9-0c14d6dac0a0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "163c1124-02e9-454a-acf2-a5d498b93b23",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}