{
    "id": "9ef076df-e2d4-4c0d-8d64-4340e468d024",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sWater",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1d33abbb-a58a-4eba-8859-735c65e6d9df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9ef076df-e2d4-4c0d-8d64-4340e468d024",
            "compositeImage": {
                "id": "b1a3672a-19e6-4579-8f96-9ab0b214f9bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d33abbb-a58a-4eba-8859-735c65e6d9df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b1696e1-4f8b-4bc9-858d-172f633bb942",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d33abbb-a58a-4eba-8859-735c65e6d9df",
                    "LayerId": "e98adc78-5cf6-42f6-93d1-9230d5ac1609"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "e98adc78-5cf6-42f6-93d1-9230d5ac1609",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9ef076df-e2d4-4c0d-8d64-4340e468d024",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 3,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 16
}