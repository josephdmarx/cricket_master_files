{
    "id": "f3f9d9d7-1e6f-4379-922a-9f1737423724",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sFloorOneWay",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cec3df18-411b-40fb-b75d-b96c522a53a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3f9d9d7-1e6f-4379-922a-9f1737423724",
            "compositeImage": {
                "id": "4f50700c-874a-4ab9-aa38-ba609236105b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cec3df18-411b-40fb-b75d-b96c522a53a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "81aa2122-2c1c-47da-8f08-1758baaddc52",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cec3df18-411b-40fb-b75d-b96c522a53a4",
                    "LayerId": "af993597-9395-4f50-8774-ef9cf5b46573"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "af993597-9395-4f50-8774-ef9cf5b46573",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f3f9d9d7-1e6f-4379-922a-9f1737423724",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 1,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 0
}