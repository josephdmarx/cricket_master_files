{
    "id": "d7561f40-c0b0-4a95-86d7-c961be5e88c0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sThornWallCollider",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7dd412ad-186a-40b0-8868-88eeb4783ac1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7561f40-c0b0-4a95-86d7-c961be5e88c0",
            "compositeImage": {
                "id": "b9e4045f-9a95-4787-849f-2ed1d7753db5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7dd412ad-186a-40b0-8868-88eeb4783ac1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d9cdd27-0c4c-48b3-82ba-1e54bbc0069b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7dd412ad-186a-40b0-8868-88eeb4783ac1",
                    "LayerId": "cb9fcf5a-6a36-4caf-a146-f411966603ca"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "cb9fcf5a-6a36-4caf-a146-f411966603ca",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d7561f40-c0b0-4a95-86d7-c961be5e88c0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 128
}