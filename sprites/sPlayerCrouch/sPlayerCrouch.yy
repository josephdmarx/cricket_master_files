{
    "id": "56f2b143-0afa-435c-981f-6d3978a27ebd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayerCrouch",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 94,
    "bbox_left": 58,
    "bbox_right": 72,
    "bbox_top": 59,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "59abe825-3438-49cf-b1fb-f2e852581b1d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "56f2b143-0afa-435c-981f-6d3978a27ebd",
            "compositeImage": {
                "id": "7f9f55bf-a424-4960-a136-770266cc445b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59abe825-3438-49cf-b1fb-f2e852581b1d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c7226087-3b11-422b-abfe-5836dc36dcbd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59abe825-3438-49cf-b1fb-f2e852581b1d",
                    "LayerId": "4d615309-d29b-43cd-9336-c7a6261642ee"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "4d615309-d29b-43cd-9336-c7a6261642ee",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "56f2b143-0afa-435c-981f-6d3978a27ebd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}