{
    "id": "1f0ca915-960d-46dc-b98e-0cbb0681ba67",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGremlin_Attack",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 13,
    "bbox_right": 63,
    "bbox_top": 6,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "89b5a7c5-4a60-4f81-af1d-98a80a8d6fc7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1f0ca915-960d-46dc-b98e-0cbb0681ba67",
            "compositeImage": {
                "id": "f30bf45c-116e-4f11-8f5d-be64873fadb2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89b5a7c5-4a60-4f81-af1d-98a80a8d6fc7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64cef59e-4ccd-47db-9d5b-ea9ab011c6d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89b5a7c5-4a60-4f81-af1d-98a80a8d6fc7",
                    "LayerId": "3b15d101-5831-42f5-89f6-afe4214f4ab7"
                }
            ]
        },
        {
            "id": "e512149c-26ca-40db-9457-2b7358e47092",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1f0ca915-960d-46dc-b98e-0cbb0681ba67",
            "compositeImage": {
                "id": "1b7f553f-939b-4c8a-a27b-14ac5aeba676",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e512149c-26ca-40db-9457-2b7358e47092",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d020b6a8-32ae-4e18-9ba5-a2cb24a9abdf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e512149c-26ca-40db-9457-2b7358e47092",
                    "LayerId": "3b15d101-5831-42f5-89f6-afe4214f4ab7"
                }
            ]
        },
        {
            "id": "ac4c81e8-7a7e-410e-9d77-483fba70c871",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1f0ca915-960d-46dc-b98e-0cbb0681ba67",
            "compositeImage": {
                "id": "75fd3c69-8692-4d7d-a0a8-f7fd6b97b90c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac4c81e8-7a7e-410e-9d77-483fba70c871",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a010802-efea-4c7d-966e-ece979cc5037",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac4c81e8-7a7e-410e-9d77-483fba70c871",
                    "LayerId": "3b15d101-5831-42f5-89f6-afe4214f4ab7"
                }
            ]
        },
        {
            "id": "008ef363-6994-458a-8fd1-32701fa278ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1f0ca915-960d-46dc-b98e-0cbb0681ba67",
            "compositeImage": {
                "id": "df1e887d-7678-4799-a4e5-b5d5080e5c54",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "008ef363-6994-458a-8fd1-32701fa278ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a700817c-f830-4d8b-85f7-dcce10a0425b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "008ef363-6994-458a-8fd1-32701fa278ae",
                    "LayerId": "3b15d101-5831-42f5-89f6-afe4214f4ab7"
                }
            ]
        },
        {
            "id": "374b253a-d2b0-446b-b8cb-f5c8113bb421",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1f0ca915-960d-46dc-b98e-0cbb0681ba67",
            "compositeImage": {
                "id": "92c7b07f-99f7-43b8-8eec-ae8afc9634dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "374b253a-d2b0-446b-b8cb-f5c8113bb421",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56551e61-157e-4524-9e98-8315b2b2bc9c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "374b253a-d2b0-446b-b8cb-f5c8113bb421",
                    "LayerId": "3b15d101-5831-42f5-89f6-afe4214f4ab7"
                }
            ]
        },
        {
            "id": "d795b8b5-5d45-4bdf-97e6-b88564d09d30",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1f0ca915-960d-46dc-b98e-0cbb0681ba67",
            "compositeImage": {
                "id": "326bcb03-2c4c-42e9-81c2-fe2a676e91c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d795b8b5-5d45-4bdf-97e6-b88564d09d30",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "916144b5-3fa0-458a-9f7b-6d72c601b337",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d795b8b5-5d45-4bdf-97e6-b88564d09d30",
                    "LayerId": "3b15d101-5831-42f5-89f6-afe4214f4ab7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "3b15d101-5831-42f5-89f6-afe4214f4ab7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1f0ca915-960d-46dc-b98e-0cbb0681ba67",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 77,
    "xorig": 30,
    "yorig": 25
}