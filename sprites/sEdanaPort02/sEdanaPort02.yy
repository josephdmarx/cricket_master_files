{
    "id": "60933066-875a-4c15-9da2-8f2175e49d12",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sEdanaPort02",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 8,
    "bbox_right": 89,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": true,
    "frames": [
        {
            "id": "69f530a6-7cf3-40c6-bebd-23e076aea6aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60933066-875a-4c15-9da2-8f2175e49d12",
            "compositeImage": {
                "id": "00c00b55-7d73-4cc4-aff7-3f94c1ea943d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "69f530a6-7cf3-40c6-bebd-23e076aea6aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a3fd42a-74f6-4113-b99c-f549b473f670",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69f530a6-7cf3-40c6-bebd-23e076aea6aa",
                    "LayerId": "01f656e4-82cc-4b43-b48d-55d4ba9375ba"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "01f656e4-82cc-4b43-b48d-55d4ba9375ba",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "60933066-875a-4c15-9da2-8f2175e49d12",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": true,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 0,
    "yorig": 0
}