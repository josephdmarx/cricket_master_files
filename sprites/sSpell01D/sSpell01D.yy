{
    "id": "68d563a8-4037-46ac-9540-d7a2f6983d15",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSpell01D",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 8,
    "bbox_right": 23,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "90af9da1-85de-4f6a-843a-af6f0188fb88",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "68d563a8-4037-46ac-9540-d7a2f6983d15",
            "compositeImage": {
                "id": "b2713ee0-5d0a-4d62-aa9c-e99a2b03cfdb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "90af9da1-85de-4f6a-843a-af6f0188fb88",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec678635-d4c8-4e76-aa61-795ebb03db60",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "90af9da1-85de-4f6a-843a-af6f0188fb88",
                    "LayerId": "837ff8fe-be15-49e1-b4da-2acf5dc580ad"
                }
            ]
        },
        {
            "id": "e406a0f2-dcd4-44f7-a432-4622e79755fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "68d563a8-4037-46ac-9540-d7a2f6983d15",
            "compositeImage": {
                "id": "6d6658b0-bb0e-405b-8623-9d00ed566127",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e406a0f2-dcd4-44f7-a432-4622e79755fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "298a35e7-52f3-4627-a475-e7eebde0aad4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e406a0f2-dcd4-44f7-a432-4622e79755fb",
                    "LayerId": "837ff8fe-be15-49e1-b4da-2acf5dc580ad"
                }
            ]
        },
        {
            "id": "419be30f-4a90-4467-a5d7-db7743981fde",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "68d563a8-4037-46ac-9540-d7a2f6983d15",
            "compositeImage": {
                "id": "eac2e447-e43b-4347-a78b-b2a13a6fd0fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "419be30f-4a90-4467-a5d7-db7743981fde",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91d49fcb-c7c4-4f8d-ba32-babb1afdc6ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "419be30f-4a90-4467-a5d7-db7743981fde",
                    "LayerId": "837ff8fe-be15-49e1-b4da-2acf5dc580ad"
                }
            ]
        },
        {
            "id": "49c1cbe5-57e6-4ab9-8edc-30ae9a281ec4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "68d563a8-4037-46ac-9540-d7a2f6983d15",
            "compositeImage": {
                "id": "a9a92b1f-7123-4fa6-a7f9-4481dd33f6f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "49c1cbe5-57e6-4ab9-8edc-30ae9a281ec4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1be5179a-ebee-403d-80d1-ae1e7fdc62ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49c1cbe5-57e6-4ab9-8edc-30ae9a281ec4",
                    "LayerId": "837ff8fe-be15-49e1-b4da-2acf5dc580ad"
                }
            ]
        },
        {
            "id": "28dd865e-2561-4345-8587-590cce31631e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "68d563a8-4037-46ac-9540-d7a2f6983d15",
            "compositeImage": {
                "id": "ce66430d-34fe-4ba3-b2b6-25a8c7e328b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "28dd865e-2561-4345-8587-590cce31631e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4825775e-bbeb-49f9-8212-c6717716d23c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "28dd865e-2561-4345-8587-590cce31631e",
                    "LayerId": "837ff8fe-be15-49e1-b4da-2acf5dc580ad"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "837ff8fe-be15-49e1-b4da-2acf5dc580ad",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "68d563a8-4037-46ac-9540-d7a2f6983d15",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}