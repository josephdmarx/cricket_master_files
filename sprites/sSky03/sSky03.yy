{
    "id": "6d4b787e-299e-47b9-af9e-31f21bd5dd06",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSky03",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 359,
    "bbox_left": 0,
    "bbox_right": 639,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4ab9ff5b-3b10-49fc-b3e6-c1e483861465",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6d4b787e-299e-47b9-af9e-31f21bd5dd06",
            "compositeImage": {
                "id": "8212a522-5250-4186-a2a5-aa97338e98bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ab9ff5b-3b10-49fc-b3e6-c1e483861465",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "50891391-bbcf-49fa-acac-9d1bc5ad0531",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ab9ff5b-3b10-49fc-b3e6-c1e483861465",
                    "LayerId": "4b8e2ac2-f77a-41d0-ae3f-4588f7358a6e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 360,
    "layers": [
        {
            "id": "4b8e2ac2-f77a-41d0-ae3f-4588f7358a6e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6d4b787e-299e-47b9-af9e-31f21bd5dd06",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 0,
    "yorig": 0
}