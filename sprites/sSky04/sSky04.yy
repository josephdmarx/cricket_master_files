{
    "id": "061ced53-96b3-462f-8b99-0be8c1b5b5f9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSky04",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 359,
    "bbox_left": 0,
    "bbox_right": 639,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "51350d8b-7a79-4895-a9a1-f0cd610aba2f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "061ced53-96b3-462f-8b99-0be8c1b5b5f9",
            "compositeImage": {
                "id": "16cc3bfb-917f-45da-998f-ce61dde019e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51350d8b-7a79-4895-a9a1-f0cd610aba2f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "667be1fa-20b7-41af-b4c3-1022cbcca150",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51350d8b-7a79-4895-a9a1-f0cd610aba2f",
                    "LayerId": "34ddd935-9b5b-485b-b404-41a5e2e53347"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 360,
    "layers": [
        {
            "id": "34ddd935-9b5b-485b-b404-41a5e2e53347",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "061ced53-96b3-462f-8b99-0be8c1b5b5f9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 0,
    "yorig": 0
}