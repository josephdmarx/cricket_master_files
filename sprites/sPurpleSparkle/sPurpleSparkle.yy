{
    "id": "c601a00b-4579-48f7-8587-2e7509f1e71d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPurpleSparkle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 60,
    "bbox_left": 45,
    "bbox_right": 56,
    "bbox_top": 49,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ea449582-af4b-42a5-b377-960c83732937",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c601a00b-4579-48f7-8587-2e7509f1e71d",
            "compositeImage": {
                "id": "51d8ff68-29c0-46b9-a388-3c242caec3bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea449582-af4b-42a5-b377-960c83732937",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f4101a9b-4aaf-4449-b163-072555541539",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea449582-af4b-42a5-b377-960c83732937",
                    "LayerId": "bd9a2b3e-026e-4da6-9974-69c412472a71"
                }
            ]
        },
        {
            "id": "36f9d73e-2ad9-4cc7-af07-71172a8adb8a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c601a00b-4579-48f7-8587-2e7509f1e71d",
            "compositeImage": {
                "id": "7172c9de-7d0a-406a-9e3b-ba2c926ac682",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "36f9d73e-2ad9-4cc7-af07-71172a8adb8a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "907e64da-c381-4076-be3f-1d68523fe542",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36f9d73e-2ad9-4cc7-af07-71172a8adb8a",
                    "LayerId": "bd9a2b3e-026e-4da6-9974-69c412472a71"
                }
            ]
        },
        {
            "id": "1c34f171-fb67-43c3-ae88-0f1bb4160055",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c601a00b-4579-48f7-8587-2e7509f1e71d",
            "compositeImage": {
                "id": "59427756-68ed-4987-b13e-a1a0899cded9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c34f171-fb67-43c3-ae88-0f1bb4160055",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "efcc6879-7a89-4ba8-b5af-e7f9191ac1ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c34f171-fb67-43c3-ae88-0f1bb4160055",
                    "LayerId": "bd9a2b3e-026e-4da6-9974-69c412472a71"
                }
            ]
        },
        {
            "id": "0f067053-f193-41b9-aa7c-b2d04adbaef6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c601a00b-4579-48f7-8587-2e7509f1e71d",
            "compositeImage": {
                "id": "68f16313-6f96-4436-ada1-331666988d7e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f067053-f193-41b9-aa7c-b2d04adbaef6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39b161c5-bbe0-406f-915c-cecdb56bd42a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f067053-f193-41b9-aa7c-b2d04adbaef6",
                    "LayerId": "bd9a2b3e-026e-4da6-9974-69c412472a71"
                }
            ]
        },
        {
            "id": "0cfdcdec-70da-4cc5-80ce-c2b8d7476405",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c601a00b-4579-48f7-8587-2e7509f1e71d",
            "compositeImage": {
                "id": "0f64aaaf-7257-4ba7-9689-9c46bcabdea2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0cfdcdec-70da-4cc5-80ce-c2b8d7476405",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e5031fd-5a4d-4f4f-9de1-8bbbf38d713b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0cfdcdec-70da-4cc5-80ce-c2b8d7476405",
                    "LayerId": "bd9a2b3e-026e-4da6-9974-69c412472a71"
                }
            ]
        },
        {
            "id": "8806a042-ba98-46fd-94ff-dd6ca8b20b9f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c601a00b-4579-48f7-8587-2e7509f1e71d",
            "compositeImage": {
                "id": "361db92f-4c4c-4e53-b496-73c41f8477de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8806a042-ba98-46fd-94ff-dd6ca8b20b9f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "887f3e2f-3f6b-4261-af6d-da92c5ee37fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8806a042-ba98-46fd-94ff-dd6ca8b20b9f",
                    "LayerId": "bd9a2b3e-026e-4da6-9974-69c412472a71"
                }
            ]
        },
        {
            "id": "f1beeed7-258c-45b1-bf10-0630608d8bf7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c601a00b-4579-48f7-8587-2e7509f1e71d",
            "compositeImage": {
                "id": "9c997c99-389a-4966-8e5d-7636d5642ae6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1beeed7-258c-45b1-bf10-0630608d8bf7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "428de5b2-1773-46df-92e0-8a624cac8405",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1beeed7-258c-45b1-bf10-0630608d8bf7",
                    "LayerId": "bd9a2b3e-026e-4da6-9974-69c412472a71"
                }
            ]
        },
        {
            "id": "890ee239-ee7c-4376-9747-e840c6eda4ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c601a00b-4579-48f7-8587-2e7509f1e71d",
            "compositeImage": {
                "id": "70424ead-e6e6-4005-813d-d6ed09f6d0f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "890ee239-ee7c-4376-9747-e840c6eda4ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c7d93e5-1094-4275-b43d-7864af2e976f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "890ee239-ee7c-4376-9747-e840c6eda4ee",
                    "LayerId": "bd9a2b3e-026e-4da6-9974-69c412472a71"
                }
            ]
        },
        {
            "id": "9b857845-509c-40bf-a775-02fcfc943598",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c601a00b-4579-48f7-8587-2e7509f1e71d",
            "compositeImage": {
                "id": "d65db080-9ed1-4ab0-91e4-ba5e05eb6967",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b857845-509c-40bf-a775-02fcfc943598",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04748967-c4ab-478e-b4ac-26520b39cdd9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b857845-509c-40bf-a775-02fcfc943598",
                    "LayerId": "bd9a2b3e-026e-4da6-9974-69c412472a71"
                }
            ]
        },
        {
            "id": "2f018695-cb27-460f-a420-8888aec28e49",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c601a00b-4579-48f7-8587-2e7509f1e71d",
            "compositeImage": {
                "id": "93b24fa5-91c7-4e36-abee-aecfa54f7c09",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f018695-cb27-460f-a420-8888aec28e49",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46bc35b5-38cd-42b1-8017-ffe963c6515d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f018695-cb27-460f-a420-8888aec28e49",
                    "LayerId": "bd9a2b3e-026e-4da6-9974-69c412472a71"
                }
            ]
        },
        {
            "id": "aa24bf30-b344-43cb-ad92-436667b2848c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c601a00b-4579-48f7-8587-2e7509f1e71d",
            "compositeImage": {
                "id": "378ce051-4058-4278-bf3f-c841a79bc487",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa24bf30-b344-43cb-ad92-436667b2848c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d2b1f0ec-c62c-4a2d-be21-25d15d23f955",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa24bf30-b344-43cb-ad92-436667b2848c",
                    "LayerId": "bd9a2b3e-026e-4da6-9974-69c412472a71"
                }
            ]
        },
        {
            "id": "514cb30c-e03b-4dad-8a8d-05bd6c73771c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c601a00b-4579-48f7-8587-2e7509f1e71d",
            "compositeImage": {
                "id": "e960dae9-3b2c-4758-a885-f191ab656104",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "514cb30c-e03b-4dad-8a8d-05bd6c73771c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46e8f7c6-a420-4a43-9dcd-57c2018a063a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "514cb30c-e03b-4dad-8a8d-05bd6c73771c",
                    "LayerId": "bd9a2b3e-026e-4da6-9974-69c412472a71"
                }
            ]
        },
        {
            "id": "a8f5bdc0-fb9b-4bb7-af7b-994d073cebc9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c601a00b-4579-48f7-8587-2e7509f1e71d",
            "compositeImage": {
                "id": "d26d6be1-6ffd-4ba4-bc6b-2399b1c05613",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a8f5bdc0-fb9b-4bb7-af7b-994d073cebc9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86a43092-c185-48a7-9eab-56750252d707",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8f5bdc0-fb9b-4bb7-af7b-994d073cebc9",
                    "LayerId": "bd9a2b3e-026e-4da6-9974-69c412472a71"
                }
            ]
        },
        {
            "id": "1fbce138-7f15-43f0-b194-109cdd523d51",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c601a00b-4579-48f7-8587-2e7509f1e71d",
            "compositeImage": {
                "id": "fdd26073-732f-4639-be52-7aa3b4ca258a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1fbce138-7f15-43f0-b194-109cdd523d51",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "350e30df-f254-4970-9e14-951f48d786c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1fbce138-7f15-43f0-b194-109cdd523d51",
                    "LayerId": "bd9a2b3e-026e-4da6-9974-69c412472a71"
                }
            ]
        },
        {
            "id": "e6f951f9-5abb-4d3c-813e-a7df8d0846aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c601a00b-4579-48f7-8587-2e7509f1e71d",
            "compositeImage": {
                "id": "27235189-1978-4273-872c-9b9d20d8db91",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6f951f9-5abb-4d3c-813e-a7df8d0846aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "526e86f6-7f9a-428d-b36b-12d62613d2c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6f951f9-5abb-4d3c-813e-a7df8d0846aa",
                    "LayerId": "bd9a2b3e-026e-4da6-9974-69c412472a71"
                }
            ]
        },
        {
            "id": "2ce6c589-4e8d-42b8-aeb5-28c6ee2ce0a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c601a00b-4579-48f7-8587-2e7509f1e71d",
            "compositeImage": {
                "id": "9cd495f5-904b-47f4-984f-167d0d8f9653",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ce6c589-4e8d-42b8-aeb5-28c6ee2ce0a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5e0a6fc-1b8a-4f67-8253-d4287582a3b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ce6c589-4e8d-42b8-aeb5-28c6ee2ce0a3",
                    "LayerId": "bd9a2b3e-026e-4da6-9974-69c412472a71"
                }
            ]
        },
        {
            "id": "4a3ce46d-0b40-4c57-888a-b89c2ed5d4a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c601a00b-4579-48f7-8587-2e7509f1e71d",
            "compositeImage": {
                "id": "8b3c19ec-20f9-4f50-985f-7e3f0dc76b7a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a3ce46d-0b40-4c57-888a-b89c2ed5d4a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c93d417f-f3be-4ba5-909e-e7137da7cdfc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a3ce46d-0b40-4c57-888a-b89c2ed5d4a0",
                    "LayerId": "bd9a2b3e-026e-4da6-9974-69c412472a71"
                }
            ]
        },
        {
            "id": "8d1affa1-7461-4607-96c9-34ef1aca4758",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c601a00b-4579-48f7-8587-2e7509f1e71d",
            "compositeImage": {
                "id": "8173f059-f91c-40f0-8fc8-ecd81cd4498a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d1affa1-7461-4607-96c9-34ef1aca4758",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec67e3a3-6454-4d69-a508-9c23a8bbdbd4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d1affa1-7461-4607-96c9-34ef1aca4758",
                    "LayerId": "bd9a2b3e-026e-4da6-9974-69c412472a71"
                }
            ]
        },
        {
            "id": "d90e1af6-1d51-430d-9818-8f226bd7cb46",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c601a00b-4579-48f7-8587-2e7509f1e71d",
            "compositeImage": {
                "id": "99c5b9fe-a833-425c-8d6b-e0b1771c8e33",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d90e1af6-1d51-430d-9818-8f226bd7cb46",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8910ce4-bf73-412a-b910-ec0b1fe464b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d90e1af6-1d51-430d-9818-8f226bd7cb46",
                    "LayerId": "bd9a2b3e-026e-4da6-9974-69c412472a71"
                }
            ]
        },
        {
            "id": "df7bee2d-0ea5-4bc9-9646-b307924e164a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c601a00b-4579-48f7-8587-2e7509f1e71d",
            "compositeImage": {
                "id": "b59ff791-9bcc-4fb1-b15c-81c956e356df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df7bee2d-0ea5-4bc9-9646-b307924e164a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "712194ed-9ac5-40e6-b0bc-e48107e45c93",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df7bee2d-0ea5-4bc9-9646-b307924e164a",
                    "LayerId": "bd9a2b3e-026e-4da6-9974-69c412472a71"
                }
            ]
        },
        {
            "id": "5f450043-9aa8-4747-a111-dcb565a8f49a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c601a00b-4579-48f7-8587-2e7509f1e71d",
            "compositeImage": {
                "id": "34e08264-bc72-4685-86df-4d2e410b3da2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f450043-9aa8-4747-a111-dcb565a8f49a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c7a7c09-d26b-4625-829e-147f7c22144e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f450043-9aa8-4747-a111-dcb565a8f49a",
                    "LayerId": "bd9a2b3e-026e-4da6-9974-69c412472a71"
                }
            ]
        },
        {
            "id": "3df00040-686a-49c4-8dbf-6031e770e0a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c601a00b-4579-48f7-8587-2e7509f1e71d",
            "compositeImage": {
                "id": "4eef379f-8ffa-40e6-93ab-10b3cc16ed36",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3df00040-686a-49c4-8dbf-6031e770e0a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "243d1c0b-1f27-4320-8ecc-d6dee8dbead2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3df00040-686a-49c4-8dbf-6031e770e0a5",
                    "LayerId": "bd9a2b3e-026e-4da6-9974-69c412472a71"
                }
            ]
        },
        {
            "id": "638006c8-c1f6-4cf3-9980-7ccc2e4415f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c601a00b-4579-48f7-8587-2e7509f1e71d",
            "compositeImage": {
                "id": "7fab6a3b-9a5f-402c-94cd-591de3795dfe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "638006c8-c1f6-4cf3-9980-7ccc2e4415f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20b6f7f3-f4ae-4a00-b3b9-7a49c3d9a854",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "638006c8-c1f6-4cf3-9980-7ccc2e4415f9",
                    "LayerId": "bd9a2b3e-026e-4da6-9974-69c412472a71"
                }
            ]
        },
        {
            "id": "6ee8941a-c9a6-4acc-b5cc-9697c3bc0527",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c601a00b-4579-48f7-8587-2e7509f1e71d",
            "compositeImage": {
                "id": "35870575-effe-41f7-9ab2-a1177ef9fbd0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ee8941a-c9a6-4acc-b5cc-9697c3bc0527",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f0b8109d-d537-465c-ac9d-4bdf475d9fd3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ee8941a-c9a6-4acc-b5cc-9697c3bc0527",
                    "LayerId": "bd9a2b3e-026e-4da6-9974-69c412472a71"
                }
            ]
        },
        {
            "id": "04b82190-e2b8-4f9b-a42b-0cd405053f50",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c601a00b-4579-48f7-8587-2e7509f1e71d",
            "compositeImage": {
                "id": "0adfde1d-8d76-48ad-a7f0-345822abf7b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "04b82190-e2b8-4f9b-a42b-0cd405053f50",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "101e2b3d-6d28-41b8-b77e-e9769b42ea21",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "04b82190-e2b8-4f9b-a42b-0cd405053f50",
                    "LayerId": "bd9a2b3e-026e-4da6-9974-69c412472a71"
                }
            ]
        },
        {
            "id": "f1d97217-7579-4a21-a3b3-9b12e949dc9c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c601a00b-4579-48f7-8587-2e7509f1e71d",
            "compositeImage": {
                "id": "a22876f8-16c8-4d38-af87-77959a7bb082",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1d97217-7579-4a21-a3b3-9b12e949dc9c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ee0a20b-8821-466b-9398-3469581fbd62",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1d97217-7579-4a21-a3b3-9b12e949dc9c",
                    "LayerId": "bd9a2b3e-026e-4da6-9974-69c412472a71"
                }
            ]
        },
        {
            "id": "58f73caf-cbe4-4407-9d69-554617cdd452",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c601a00b-4579-48f7-8587-2e7509f1e71d",
            "compositeImage": {
                "id": "a2c09497-43a3-421d-a690-6b35778d5362",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "58f73caf-cbe4-4407-9d69-554617cdd452",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "38e6c2da-927f-440a-8732-20c4a8368948",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "58f73caf-cbe4-4407-9d69-554617cdd452",
                    "LayerId": "bd9a2b3e-026e-4da6-9974-69c412472a71"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "bd9a2b3e-026e-4da6-9974-69c412472a71",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c601a00b-4579-48f7-8587-2e7509f1e71d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 50,
    "yorig": 55
}