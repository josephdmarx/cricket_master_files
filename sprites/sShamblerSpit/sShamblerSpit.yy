{
    "id": "b1079b03-3ff9-48e9-bbb9-9a148279e851",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sShamblerSpit",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3593a313-a0be-4fb3-96bf-e86895e7936c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b1079b03-3ff9-48e9-bbb9-9a148279e851",
            "compositeImage": {
                "id": "8b447b59-2033-4c10-b56c-6e1a86b9a8b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3593a313-a0be-4fb3-96bf-e86895e7936c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bdac111c-b4d1-4ea6-b7d9-c3118dfb2282",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3593a313-a0be-4fb3-96bf-e86895e7936c",
                    "LayerId": "bc535ec6-7b30-4325-95d7-84e9cc7e719f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "bc535ec6-7b30-4325-95d7-84e9cc7e719f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b1079b03-3ff9-48e9-bbb9-9a148279e851",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}