{
    "id": "98c8dcd6-5f6a-49fe-906f-2de7aba5aa9b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sDoorTransition",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 35,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3206c888-2d30-4da9-81b3-9e1cb30c8258",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "98c8dcd6-5f6a-49fe-906f-2de7aba5aa9b",
            "compositeImage": {
                "id": "06d590ca-b6ff-4c6a-871e-24bcb45e9a51",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3206c888-2d30-4da9-81b3-9e1cb30c8258",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ca984fd-4bbc-47dc-92d9-7fc52fc3acce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3206c888-2d30-4da9-81b3-9e1cb30c8258",
                    "LayerId": "c0fc115c-c8ee-4864-b982-aa856749ca61"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "c0fc115c-c8ee-4864-b982-aa856749ca61",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "98c8dcd6-5f6a-49fe-906f-2de7aba5aa9b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 36,
    "xorig": 18,
    "yorig": 32
}