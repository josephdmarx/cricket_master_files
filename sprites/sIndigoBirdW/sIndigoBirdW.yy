{
    "id": "d34a34ac-11be-4d41-9650-3357f561885b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sIndigoBirdW",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 26,
    "bbox_left": 7,
    "bbox_right": 25,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "81c3a38b-a7da-4a30-9e0e-98b828ea4b0b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d34a34ac-11be-4d41-9650-3357f561885b",
            "compositeImage": {
                "id": "12d194a4-e933-47f1-b3f6-6e451e9aa8e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81c3a38b-a7da-4a30-9e0e-98b828ea4b0b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "acecde3f-d232-479c-9dce-07ef5127cbf5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81c3a38b-a7da-4a30-9e0e-98b828ea4b0b",
                    "LayerId": "b44de2eb-2bf5-41ce-9337-0a4ae84efa0a"
                }
            ]
        },
        {
            "id": "cab0338f-fb51-497c-9a6b-7597faaf0408",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d34a34ac-11be-4d41-9650-3357f561885b",
            "compositeImage": {
                "id": "77055624-c2a5-4977-9c0a-3214cea5ef8e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cab0338f-fb51-497c-9a6b-7597faaf0408",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05f92f70-426d-4536-940a-e1c00c86abc9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cab0338f-fb51-497c-9a6b-7597faaf0408",
                    "LayerId": "b44de2eb-2bf5-41ce-9337-0a4ae84efa0a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "b44de2eb-2bf5-41ce-9337-0a4ae84efa0a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d34a34ac-11be-4d41-9650-3357f561885b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 15,
    "yorig": 25
}