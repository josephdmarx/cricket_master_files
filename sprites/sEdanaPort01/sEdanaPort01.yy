{
    "id": "6e597cc4-8c17-4fe2-9a14-0a68d1da6848",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sEdanaPort01",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 8,
    "bbox_right": 89,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": true,
    "frames": [
        {
            "id": "2e1ac390-5297-4f14-93ea-dab03cde9447",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6e597cc4-8c17-4fe2-9a14-0a68d1da6848",
            "compositeImage": {
                "id": "61078cda-d536-404f-9185-4f006fb20abc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e1ac390-5297-4f14-93ea-dab03cde9447",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "73ed9b33-acc1-45cd-b883-2168dde5b9ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e1ac390-5297-4f14-93ea-dab03cde9447",
                    "LayerId": "804eeeaf-c925-44ca-be86-cdf264118fe1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "804eeeaf-c925-44ca-be86-cdf264118fe1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6e597cc4-8c17-4fe2-9a14-0a68d1da6848",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": true,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 0,
    "yorig": 0
}