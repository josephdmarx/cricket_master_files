{
    "id": "39d44a64-4287-46e0-83b0-2bf8fd5bb47a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSkullDemon_Leap",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 48,
    "bbox_right": 80,
    "bbox_top": 65,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bf73d291-c34e-444f-abf9-afe8ce509b63",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "39d44a64-4287-46e0-83b0-2bf8fd5bb47a",
            "compositeImage": {
                "id": "eff25d27-0816-45ca-a210-d5ece3050849",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf73d291-c34e-444f-abf9-afe8ce509b63",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6501f8fd-f3f9-4f34-8031-e538a2cffc39",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf73d291-c34e-444f-abf9-afe8ce509b63",
                    "LayerId": "8c746b92-2473-4259-ac28-e147f7b7f05c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "8c746b92-2473-4259-ac28-e147f7b7f05c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "39d44a64-4287-46e0-83b0-2bf8fd5bb47a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}