{
    "id": "6ee92c1e-d7a9-47a6-b93d-785bbd9432f6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Decals02_Interior_Destroyed",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 367,
    "bbox_left": 16,
    "bbox_right": 491,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "48f88c3b-5136-4424-b09b-de3a714fc74d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6ee92c1e-d7a9-47a6-b93d-785bbd9432f6",
            "compositeImage": {
                "id": "ff2d06e6-90db-41a7-ba5a-658e2feb5b77",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "48f88c3b-5136-4424-b09b-de3a714fc74d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8121b386-5998-4cb3-9ba8-b31d6ffc94d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48f88c3b-5136-4424-b09b-de3a714fc74d",
                    "LayerId": "8d4a2aeb-b6c8-4202-a0f1-3d44c4784420"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "8d4a2aeb-b6c8-4202-a0f1-3d44c4784420",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6ee92c1e-d7a9-47a6-b93d-785bbd9432f6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 512,
    "xorig": 0,
    "yorig": 0
}