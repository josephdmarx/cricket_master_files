{
    "id": "376cf730-ef3d-4aa4-b38e-edd6749608c9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayerA0",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 94,
    "bbox_left": 58,
    "bbox_right": 72,
    "bbox_top": 42,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e72a4c7c-1d31-4313-b0a6-e224f8f3452b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "376cf730-ef3d-4aa4-b38e-edd6749608c9",
            "compositeImage": {
                "id": "d1736215-4cfd-40e4-9d56-da99795bfe82",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e72a4c7c-1d31-4313-b0a6-e224f8f3452b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c102c6e3-7d2f-4c7d-b286-e602e97a9b57",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e72a4c7c-1d31-4313-b0a6-e224f8f3452b",
                    "LayerId": "749a4399-b651-41d3-9c64-d272f05de6be"
                }
            ]
        },
        {
            "id": "367db224-e1eb-4a14-9bcd-c3578ab074fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "376cf730-ef3d-4aa4-b38e-edd6749608c9",
            "compositeImage": {
                "id": "3597b829-6494-419a-92e5-cc9e05fdb930",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "367db224-e1eb-4a14-9bcd-c3578ab074fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f2e6d7e-152a-4add-87e0-87c95b1d43a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "367db224-e1eb-4a14-9bcd-c3578ab074fd",
                    "LayerId": "749a4399-b651-41d3-9c64-d272f05de6be"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "749a4399-b651-41d3-9c64-d272f05de6be",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "376cf730-ef3d-4aa4-b38e-edd6749608c9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}