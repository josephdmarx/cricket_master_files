{
    "id": "8f29740d-edc8-4e96-809d-b38efdb3d835",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayerM1Arms",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 94,
    "bbox_left": 58,
    "bbox_right": 72,
    "bbox_top": 42,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0b29b985-be9f-480b-9c83-2810fe21ead5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8f29740d-edc8-4e96-809d-b38efdb3d835",
            "compositeImage": {
                "id": "ad4a401f-4176-40be-9231-da23ae6214fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b29b985-be9f-480b-9c83-2810fe21ead5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "636c82ec-48c3-4bee-a21a-6fd4929beb3b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b29b985-be9f-480b-9c83-2810fe21ead5",
                    "LayerId": "e78d1619-3dc5-41c7-ad5c-f0762952832f"
                }
            ]
        },
        {
            "id": "47616c9d-9d96-4fce-b66c-e40649f31386",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8f29740d-edc8-4e96-809d-b38efdb3d835",
            "compositeImage": {
                "id": "c631ad2a-2e66-4777-b89b-727189d25982",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "47616c9d-9d96-4fce-b66c-e40649f31386",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56372a17-bf4a-404c-901e-5de5d7aa1d0a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47616c9d-9d96-4fce-b66c-e40649f31386",
                    "LayerId": "e78d1619-3dc5-41c7-ad5c-f0762952832f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "e78d1619-3dc5-41c7-ad5c-f0762952832f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8f29740d-edc8-4e96-809d-b38efdb3d835",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}