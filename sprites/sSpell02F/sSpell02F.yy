{
    "id": "9c76e071-3e57-4abb-9ddb-ea47badc0a73",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSpell02F",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 41,
    "bbox_left": 0,
    "bbox_right": 18,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bb01d480-294b-45be-9c5f-814a92aa9084",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c76e071-3e57-4abb-9ddb-ea47badc0a73",
            "compositeImage": {
                "id": "e3df82f3-d2f1-412f-9599-8f591989658b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb01d480-294b-45be-9c5f-814a92aa9084",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "40901f56-98b4-45cc-acdb-515f96bab820",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb01d480-294b-45be-9c5f-814a92aa9084",
                    "LayerId": "61eda2e1-05b9-47e3-89a4-ca9b04965257"
                }
            ]
        },
        {
            "id": "93dab7cd-8023-4ac5-93a2-3264be01c29b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c76e071-3e57-4abb-9ddb-ea47badc0a73",
            "compositeImage": {
                "id": "ca46859c-65c2-4627-9568-b5a02d99b0ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93dab7cd-8023-4ac5-93a2-3264be01c29b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f35299d-abc9-460c-bf7d-29da4bfd498e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93dab7cd-8023-4ac5-93a2-3264be01c29b",
                    "LayerId": "61eda2e1-05b9-47e3-89a4-ca9b04965257"
                }
            ]
        },
        {
            "id": "1ae674e9-d0ca-48a5-8aef-2ed80391ff52",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c76e071-3e57-4abb-9ddb-ea47badc0a73",
            "compositeImage": {
                "id": "145ca515-64a9-40f2-b2e2-8a3fff60cfa7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ae674e9-d0ca-48a5-8aef-2ed80391ff52",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f4b14f5a-095d-4ab5-a28a-02afd03bf2f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ae674e9-d0ca-48a5-8aef-2ed80391ff52",
                    "LayerId": "61eda2e1-05b9-47e3-89a4-ca9b04965257"
                }
            ]
        },
        {
            "id": "3770f98f-1bdf-4d1f-951f-41208e58fa76",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c76e071-3e57-4abb-9ddb-ea47badc0a73",
            "compositeImage": {
                "id": "aa682ed3-6923-455a-b9a0-264c14696227",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3770f98f-1bdf-4d1f-951f-41208e58fa76",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cba60342-45c2-4594-b1ad-6de4e74c50da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3770f98f-1bdf-4d1f-951f-41208e58fa76",
                    "LayerId": "61eda2e1-05b9-47e3-89a4-ca9b04965257"
                }
            ]
        },
        {
            "id": "73fb2591-7739-4a59-a8fe-f86860e06625",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c76e071-3e57-4abb-9ddb-ea47badc0a73",
            "compositeImage": {
                "id": "6b762844-a3d1-4d23-9abf-0e8057f6a4ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "73fb2591-7739-4a59-a8fe-f86860e06625",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab27cd96-f51f-4df5-85bc-26ca1fe50770",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73fb2591-7739-4a59-a8fe-f86860e06625",
                    "LayerId": "61eda2e1-05b9-47e3-89a4-ca9b04965257"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 42,
    "layers": [
        {
            "id": "61eda2e1-05b9-47e3-89a4-ca9b04965257",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9c76e071-3e57-4abb-9ddb-ea47badc0a73",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 19,
    "xorig": 9,
    "yorig": 21
}