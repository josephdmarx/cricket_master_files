{
    "id": "76c57b56-289e-4193-ba82-a441f5064e9b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sRedSparkle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 6,
    "bbox_left": 1,
    "bbox_right": 6,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d4755213-0b45-4dfe-951c-e04959654124",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76c57b56-289e-4193-ba82-a441f5064e9b",
            "compositeImage": {
                "id": "fe4610ce-d456-4f8f-aef6-ab0666253cc5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4755213-0b45-4dfe-951c-e04959654124",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "976c8aba-ee44-4899-941a-453958de6f09",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4755213-0b45-4dfe-951c-e04959654124",
                    "LayerId": "9e836251-5874-4889-9c14-13be1b707a5d"
                }
            ]
        },
        {
            "id": "f984045e-cb4f-4580-a435-491de9241673",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76c57b56-289e-4193-ba82-a441f5064e9b",
            "compositeImage": {
                "id": "c2c66257-c7ba-42c3-bbb3-d05d7f2f3508",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f984045e-cb4f-4580-a435-491de9241673",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "36bcc071-6f03-4ac2-8de2-a76836b83b4e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f984045e-cb4f-4580-a435-491de9241673",
                    "LayerId": "9e836251-5874-4889-9c14-13be1b707a5d"
                }
            ]
        },
        {
            "id": "f189d102-5118-46d9-a963-e5019cdcbfcd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76c57b56-289e-4193-ba82-a441f5064e9b",
            "compositeImage": {
                "id": "919403f6-dea2-4783-bd0f-c61a7e0cb288",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f189d102-5118-46d9-a963-e5019cdcbfcd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "521d48eb-25ab-4619-b11a-3d2958abaa7f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f189d102-5118-46d9-a963-e5019cdcbfcd",
                    "LayerId": "9e836251-5874-4889-9c14-13be1b707a5d"
                }
            ]
        },
        {
            "id": "32c591a2-3b4f-429f-98c6-defc4d339ced",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76c57b56-289e-4193-ba82-a441f5064e9b",
            "compositeImage": {
                "id": "9bbf5072-0d6a-43d4-93ce-501110495878",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "32c591a2-3b4f-429f-98c6-defc4d339ced",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce1dc67d-09b1-4cda-b043-22b30350426e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32c591a2-3b4f-429f-98c6-defc4d339ced",
                    "LayerId": "9e836251-5874-4889-9c14-13be1b707a5d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "9e836251-5874-4889-9c14-13be1b707a5d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "76c57b56-289e-4193-ba82-a441f5064e9b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 4
}