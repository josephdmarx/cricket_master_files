{
    "id": "cf973f73-1252-4bc1-867c-daad6ee441da",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sShadow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 18,
    "bbox_left": 10,
    "bbox_right": 20,
    "bbox_top": 16,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6cf8f480-9e50-4002-97a9-385d32c2ddea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cf973f73-1252-4bc1-867c-daad6ee441da",
            "compositeImage": {
                "id": "e814f129-b7aa-49b4-9b15-9a26856aa948",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6cf8f480-9e50-4002-97a9-385d32c2ddea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6807f183-ea0e-4118-94ed-30c921457e5a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6cf8f480-9e50-4002-97a9-385d32c2ddea",
                    "LayerId": "f14a74d3-cafe-487b-9163-67aebc48839c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "f14a74d3-cafe-487b-9163-67aebc48839c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cf973f73-1252-4bc1-867c-daad6ee441da",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}