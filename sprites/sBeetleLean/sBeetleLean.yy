{
    "id": "ed4e48d1-6143-46ea-b847-cd3a27f528b1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBeetleLean",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 46,
    "bbox_right": 73,
    "bbox_top": 36,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9b5f2d3c-bec6-4e7e-b060-56ba1014d0c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ed4e48d1-6143-46ea-b847-cd3a27f528b1",
            "compositeImage": {
                "id": "d40188a4-e1e9-41a9-ae36-6fc71f18ab78",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b5f2d3c-bec6-4e7e-b060-56ba1014d0c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a8c508a9-76bb-44e2-8521-affdd2525480",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b5f2d3c-bec6-4e7e-b060-56ba1014d0c9",
                    "LayerId": "0b71f0c3-5009-40ec-81e1-a84277bc8225"
                }
            ]
        },
        {
            "id": "a67b110c-92c4-431a-842f-75157c051677",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ed4e48d1-6143-46ea-b847-cd3a27f528b1",
            "compositeImage": {
                "id": "8e7986c6-98d0-4600-ad91-66c854ddefa6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a67b110c-92c4-431a-842f-75157c051677",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "242e9b14-6696-42ba-bf29-39419ea40a94",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a67b110c-92c4-431a-842f-75157c051677",
                    "LayerId": "0b71f0c3-5009-40ec-81e1-a84277bc8225"
                }
            ]
        },
        {
            "id": "92ce06cb-3643-4ab4-ac16-c5f858853ed4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ed4e48d1-6143-46ea-b847-cd3a27f528b1",
            "compositeImage": {
                "id": "40c06321-a591-4634-8853-ef6990cf25de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "92ce06cb-3643-4ab4-ac16-c5f858853ed4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "257714a6-4d3a-40ed-8da0-c05c87a471af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92ce06cb-3643-4ab4-ac16-c5f858853ed4",
                    "LayerId": "0b71f0c3-5009-40ec-81e1-a84277bc8225"
                }
            ]
        },
        {
            "id": "1f41f667-acc6-4be5-92ea-e146a43a6162",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ed4e48d1-6143-46ea-b847-cd3a27f528b1",
            "compositeImage": {
                "id": "ffb48651-333f-4500-ae15-49d08e215c3d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f41f667-acc6-4be5-92ea-e146a43a6162",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f4238ab-e9c3-472f-90af-f544d5d20a69",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f41f667-acc6-4be5-92ea-e146a43a6162",
                    "LayerId": "0b71f0c3-5009-40ec-81e1-a84277bc8225"
                }
            ]
        },
        {
            "id": "50627d27-bb4d-46d9-ac61-e822fea6d6c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ed4e48d1-6143-46ea-b847-cd3a27f528b1",
            "compositeImage": {
                "id": "8ac2788f-c338-44e7-9115-a2218b41ac21",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "50627d27-bb4d-46d9-ac61-e822fea6d6c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "07a2a1f4-0ee3-45ea-9ba6-ac4d70b69a1d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "50627d27-bb4d-46d9-ac61-e822fea6d6c0",
                    "LayerId": "0b71f0c3-5009-40ec-81e1-a84277bc8225"
                }
            ]
        },
        {
            "id": "614a1da6-d42a-46a5-8c03-53e02a4f600c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ed4e48d1-6143-46ea-b847-cd3a27f528b1",
            "compositeImage": {
                "id": "47803d62-0fa7-4b9d-b318-af038e77c8f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "614a1da6-d42a-46a5-8c03-53e02a4f600c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4e229e05-456e-4ac7-8e28-c9b495ae74a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "614a1da6-d42a-46a5-8c03-53e02a4f600c",
                    "LayerId": "0b71f0c3-5009-40ec-81e1-a84277bc8225"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "0b71f0c3-5009-40ec-81e1-a84277bc8225",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ed4e48d1-6143-46ea-b847-cd3a27f528b1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}