{
    "id": "e9cf9f17-7512-4b7c-bb5a-dd6146d6c2f5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sThornWall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c56b1083-bd7d-4532-8b44-630adab80f17",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9cf9f17-7512-4b7c-bb5a-dd6146d6c2f5",
            "compositeImage": {
                "id": "0de6d289-d8cf-42df-8a06-1233f729dcff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c56b1083-bd7d-4532-8b44-630adab80f17",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60db3412-144e-4ab1-aea6-001e57830dc9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c56b1083-bd7d-4532-8b44-630adab80f17",
                    "LayerId": "e03f459b-31c6-4049-aee8-399560777613"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "e03f459b-31c6-4049-aee8-399560777613",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e9cf9f17-7512-4b7c-bb5a-dd6146d6c2f5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 128
}