{
    "id": "f7ec87e2-1de5-41c1-85b2-472b05480db5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sManaPot01",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 9,
    "bbox_right": 22,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1b3d5b1e-413b-4454-bbde-e1dc0069cbfd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f7ec87e2-1de5-41c1-85b2-472b05480db5",
            "compositeImage": {
                "id": "b339e0b2-d35a-4df3-9f7c-39ad4559b05b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b3d5b1e-413b-4454-bbde-e1dc0069cbfd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "982cc22e-ef73-449d-a549-d9b757dd1145",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b3d5b1e-413b-4454-bbde-e1dc0069cbfd",
                    "LayerId": "e8741cdc-1c38-4b65-a0b9-a14610da08f9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "e8741cdc-1c38-4b65-a0b9-a14610da08f9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f7ec87e2-1de5-41c1-85b2-472b05480db5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}