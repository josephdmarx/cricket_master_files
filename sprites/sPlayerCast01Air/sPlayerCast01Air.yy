{
    "id": "e3ccdbc9-53b9-4634-89ea-f9cb107b00e4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayerCast01Air",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 94,
    "bbox_left": 58,
    "bbox_right": 72,
    "bbox_top": 42,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d8449805-fbeb-4b83-a839-ecc41d45bb00",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3ccdbc9-53b9-4634-89ea-f9cb107b00e4",
            "compositeImage": {
                "id": "939ba51a-af07-478e-a2d9-7c514184d608",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8449805-fbeb-4b83-a839-ecc41d45bb00",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a58e20e-8d8f-42dc-9995-dcb41cebc8ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8449805-fbeb-4b83-a839-ecc41d45bb00",
                    "LayerId": "a80a8f16-942d-44b1-b8e4-9d9fac9747f4"
                }
            ]
        },
        {
            "id": "b0b23eb6-995b-4103-bd81-361f4aadbb41",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3ccdbc9-53b9-4634-89ea-f9cb107b00e4",
            "compositeImage": {
                "id": "141e3bc5-de76-462f-b90f-70060e4da584",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b0b23eb6-995b-4103-bd81-361f4aadbb41",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e821ea2-6bcb-4dd5-9d28-b27df35f0b6f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0b23eb6-995b-4103-bd81-361f4aadbb41",
                    "LayerId": "a80a8f16-942d-44b1-b8e4-9d9fac9747f4"
                }
            ]
        },
        {
            "id": "7ca52417-7f0d-4137-9cf5-8c4176821101",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3ccdbc9-53b9-4634-89ea-f9cb107b00e4",
            "compositeImage": {
                "id": "d2efc0ad-e885-40a6-9ecc-82459826b961",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ca52417-7f0d-4137-9cf5-8c4176821101",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "27eab846-65f2-420b-9f8e-061b0689ff0b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ca52417-7f0d-4137-9cf5-8c4176821101",
                    "LayerId": "a80a8f16-942d-44b1-b8e4-9d9fac9747f4"
                }
            ]
        },
        {
            "id": "c8dfda30-7235-4183-a03c-214e12497ad2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3ccdbc9-53b9-4634-89ea-f9cb107b00e4",
            "compositeImage": {
                "id": "cf860eeb-aa34-4c79-aac8-0c9f0562a62c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8dfda30-7235-4183-a03c-214e12497ad2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df8febbd-a2f5-4b3c-b90e-604ebb162cbd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8dfda30-7235-4183-a03c-214e12497ad2",
                    "LayerId": "a80a8f16-942d-44b1-b8e4-9d9fac9747f4"
                }
            ]
        },
        {
            "id": "34ea7fc7-525b-4fe7-b4b0-a9259ee5b850",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3ccdbc9-53b9-4634-89ea-f9cb107b00e4",
            "compositeImage": {
                "id": "6a3e5c0a-b53e-4581-ace9-af3b001d60fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34ea7fc7-525b-4fe7-b4b0-a9259ee5b850",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ec1be5c-6c30-44f6-a88a-51fff030bb7a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34ea7fc7-525b-4fe7-b4b0-a9259ee5b850",
                    "LayerId": "a80a8f16-942d-44b1-b8e4-9d9fac9747f4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "a80a8f16-942d-44b1-b8e4-9d9fac9747f4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e3ccdbc9-53b9-4634-89ea-f9cb107b00e4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}