{
    "id": "516ff03d-5532-46cd-b918-a0f5b4b33588",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGremlin_Idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 13,
    "bbox_right": 43,
    "bbox_top": 7,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7ed312ce-773a-4f2f-a0f2-4bb45372dfc6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "516ff03d-5532-46cd-b918-a0f5b4b33588",
            "compositeImage": {
                "id": "9d7ef0e3-ab1c-496e-ac75-c7d7955d490f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ed312ce-773a-4f2f-a0f2-4bb45372dfc6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b13cb89d-85e7-47e8-88ac-efe0f7a78272",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ed312ce-773a-4f2f-a0f2-4bb45372dfc6",
                    "LayerId": "ac81d3b1-dd8c-427e-9e13-ec35a09b2332"
                }
            ]
        },
        {
            "id": "bdfedf92-c702-4d1b-8b87-79641ca06248",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "516ff03d-5532-46cd-b918-a0f5b4b33588",
            "compositeImage": {
                "id": "77273012-63b2-4133-ad42-7e83a6aeec77",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bdfedf92-c702-4d1b-8b87-79641ca06248",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9167a7f4-059c-40be-8343-5ca44aab9f35",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bdfedf92-c702-4d1b-8b87-79641ca06248",
                    "LayerId": "ac81d3b1-dd8c-427e-9e13-ec35a09b2332"
                }
            ]
        },
        {
            "id": "0a9237de-be10-41a1-bc48-c111dc23d06f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "516ff03d-5532-46cd-b918-a0f5b4b33588",
            "compositeImage": {
                "id": "c266aaf1-43cf-480d-8b80-93a465a5c65f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a9237de-be10-41a1-bc48-c111dc23d06f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b2622b5-566a-4808-8253-85067a81892b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a9237de-be10-41a1-bc48-c111dc23d06f",
                    "LayerId": "ac81d3b1-dd8c-427e-9e13-ec35a09b2332"
                }
            ]
        },
        {
            "id": "7b40df2b-7f5d-4f5b-98d9-ec77d4e91736",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "516ff03d-5532-46cd-b918-a0f5b4b33588",
            "compositeImage": {
                "id": "4b51f655-86ff-48a7-b58d-d1e4556aa30f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b40df2b-7f5d-4f5b-98d9-ec77d4e91736",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "618a5bb8-52e4-49e4-8b93-355bac0aea44",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b40df2b-7f5d-4f5b-98d9-ec77d4e91736",
                    "LayerId": "ac81d3b1-dd8c-427e-9e13-ec35a09b2332"
                }
            ]
        },
        {
            "id": "a87eb08c-6c95-4f02-9173-42c4fe466ceb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "516ff03d-5532-46cd-b918-a0f5b4b33588",
            "compositeImage": {
                "id": "2475b65a-8e1c-40d0-bb31-0db8b82a7fba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a87eb08c-6c95-4f02-9173-42c4fe466ceb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3db3ebef-e7f9-4c19-84c5-186c0e5cee85",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a87eb08c-6c95-4f02-9173-42c4fe466ceb",
                    "LayerId": "ac81d3b1-dd8c-427e-9e13-ec35a09b2332"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "ac81d3b1-dd8c-427e-9e13-ec35a09b2332",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "516ff03d-5532-46cd-b918-a0f5b4b33588",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 77,
    "xorig": 30,
    "yorig": 25
}