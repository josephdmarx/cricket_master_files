{
    "id": "9d9f26df-471a-4979-88d8-4b024688dbce",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayerCharge",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 94,
    "bbox_left": 58,
    "bbox_right": 72,
    "bbox_top": 42,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ad89b021-34f4-4819-9dab-6ad1ee76bb7d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9d9f26df-471a-4979-88d8-4b024688dbce",
            "compositeImage": {
                "id": "13752455-238b-4065-b295-89c3564a6059",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad89b021-34f4-4819-9dab-6ad1ee76bb7d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8158d396-0cea-4d62-858a-da9c7e1552f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad89b021-34f4-4819-9dab-6ad1ee76bb7d",
                    "LayerId": "57066654-019b-4188-8f2c-6e9a9f695317"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "57066654-019b-4188-8f2c-6e9a9f695317",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9d9f26df-471a-4979-88d8-4b024688dbce",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 13,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}