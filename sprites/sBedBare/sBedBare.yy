{
    "id": "7c63d4c8-cb27-4447-81ca-001e571a3406",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBedBare",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 57,
    "bbox_left": 10,
    "bbox_right": 96,
    "bbox_top": 22,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "60639c9a-933a-48d5-8e63-1399fa02e036",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c63d4c8-cb27-4447-81ca-001e571a3406",
            "compositeImage": {
                "id": "127170f1-f52e-4c67-adcd-200c2844e432",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "60639c9a-933a-48d5-8e63-1399fa02e036",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db0d110e-563d-4a15-b957-2cb982a18362",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "60639c9a-933a-48d5-8e63-1399fa02e036",
                    "LayerId": "aca53868-a20e-42ef-82b7-04c57fb40187"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 65,
    "layers": [
        {
            "id": "aca53868-a20e-42ef-82b7-04c57fb40187",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7c63d4c8-cb27-4447-81ca-001e571a3406",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 108,
    "xorig": 10,
    "yorig": 57
}