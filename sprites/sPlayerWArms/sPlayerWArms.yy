{
    "id": "0791070c-026f-40ab-a547-63056c4ebcb4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayerWArms",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 94,
    "bbox_left": 58,
    "bbox_right": 72,
    "bbox_top": 42,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e684ac3d-f7f3-40d2-96cb-ee8b59774474",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0791070c-026f-40ab-a547-63056c4ebcb4",
            "compositeImage": {
                "id": "5e448e9f-eb7e-4039-b0ea-fc1e0ffe1414",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e684ac3d-f7f3-40d2-96cb-ee8b59774474",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d8c568ec-36d6-489c-9dc0-fcef11d6a5f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e684ac3d-f7f3-40d2-96cb-ee8b59774474",
                    "LayerId": "d64b9df6-18d4-4d22-b3c5-b2ba91395d7a"
                }
            ]
        },
        {
            "id": "ab54bed2-7667-4cc0-b87d-2c8566daafac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0791070c-026f-40ab-a547-63056c4ebcb4",
            "compositeImage": {
                "id": "e423cd87-f267-4149-b370-c3b324b7e119",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab54bed2-7667-4cc0-b87d-2c8566daafac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be96f433-298c-47ed-8ca4-badf36454610",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab54bed2-7667-4cc0-b87d-2c8566daafac",
                    "LayerId": "d64b9df6-18d4-4d22-b3c5-b2ba91395d7a"
                }
            ]
        },
        {
            "id": "edb329fa-74e5-45d5-b015-8c6266aff07b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0791070c-026f-40ab-a547-63056c4ebcb4",
            "compositeImage": {
                "id": "59e5cd82-ce01-4e2b-8a6d-881b9d102d90",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "edb329fa-74e5-45d5-b015-8c6266aff07b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "860ba009-4144-4cff-8b3f-84658eb40ea2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "edb329fa-74e5-45d5-b015-8c6266aff07b",
                    "LayerId": "d64b9df6-18d4-4d22-b3c5-b2ba91395d7a"
                }
            ]
        },
        {
            "id": "0d5e0d76-01d3-4dc8-8a71-e786a4229938",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0791070c-026f-40ab-a547-63056c4ebcb4",
            "compositeImage": {
                "id": "ee3c7514-a220-4cf8-ad3d-ec33c70d8715",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d5e0d76-01d3-4dc8-8a71-e786a4229938",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d23fda4b-cd2d-4760-9eeb-ce3831ad2f5a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d5e0d76-01d3-4dc8-8a71-e786a4229938",
                    "LayerId": "d64b9df6-18d4-4d22-b3c5-b2ba91395d7a"
                }
            ]
        },
        {
            "id": "a6e9eaab-03ec-4685-abcf-667037c1a8b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0791070c-026f-40ab-a547-63056c4ebcb4",
            "compositeImage": {
                "id": "636dc330-4e86-41fe-8a24-bc4c92cc3a48",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a6e9eaab-03ec-4685-abcf-667037c1a8b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "361d662d-aca4-412e-a181-e8e5a03793ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6e9eaab-03ec-4685-abcf-667037c1a8b2",
                    "LayerId": "d64b9df6-18d4-4d22-b3c5-b2ba91395d7a"
                }
            ]
        },
        {
            "id": "a8e01198-d09b-411c-b61a-12c296db215a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0791070c-026f-40ab-a547-63056c4ebcb4",
            "compositeImage": {
                "id": "fc7ce4f7-3ad8-465b-9078-c7229226fd2f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a8e01198-d09b-411c-b61a-12c296db215a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "67f3e7c5-3833-434f-987b-2425d3ba1a07",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8e01198-d09b-411c-b61a-12c296db215a",
                    "LayerId": "d64b9df6-18d4-4d22-b3c5-b2ba91395d7a"
                }
            ]
        },
        {
            "id": "4df0548a-d2ab-436f-b0c3-344feab2f544",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0791070c-026f-40ab-a547-63056c4ebcb4",
            "compositeImage": {
                "id": "2d51a6d7-9357-49d6-bec7-95b74e77e92a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4df0548a-d2ab-436f-b0c3-344feab2f544",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5c8e221-0a12-47e3-a858-0da8bc983352",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4df0548a-d2ab-436f-b0c3-344feab2f544",
                    "LayerId": "d64b9df6-18d4-4d22-b3c5-b2ba91395d7a"
                }
            ]
        },
        {
            "id": "87bb7552-51de-40de-888b-1096de1ec699",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0791070c-026f-40ab-a547-63056c4ebcb4",
            "compositeImage": {
                "id": "aac618e4-2742-4a82-ad8e-9854ddb10bfe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87bb7552-51de-40de-888b-1096de1ec699",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91403f01-af11-4771-ab94-609647d98cac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87bb7552-51de-40de-888b-1096de1ec699",
                    "LayerId": "d64b9df6-18d4-4d22-b3c5-b2ba91395d7a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "d64b9df6-18d4-4d22-b3c5-b2ba91395d7a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0791070c-026f-40ab-a547-63056c4ebcb4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}