{
    "id": "0ba17d79-9b7a-4542-b47e-647c48d9c31d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayerM0Arms",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 94,
    "bbox_left": 58,
    "bbox_right": 72,
    "bbox_top": 42,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "de39c784-a0d5-445d-b690-8042cfe9bdc1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0ba17d79-9b7a-4542-b47e-647c48d9c31d",
            "compositeImage": {
                "id": "56f0a088-064f-43a5-ab87-aa47bf7dff5c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de39c784-a0d5-445d-b690-8042cfe9bdc1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b90f90cf-d12c-462c-93f8-c7b6cb720c95",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de39c784-a0d5-445d-b690-8042cfe9bdc1",
                    "LayerId": "705afda3-0546-4a17-a3f2-ea35755f04db"
                }
            ]
        },
        {
            "id": "9e0d86d1-3ffc-4bbc-b41a-d536300dbfb4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0ba17d79-9b7a-4542-b47e-647c48d9c31d",
            "compositeImage": {
                "id": "d0ae96ce-ee9d-4344-9274-afc4c2b8c024",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e0d86d1-3ffc-4bbc-b41a-d536300dbfb4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30dd4d9d-b1d0-4591-9485-577156383b49",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e0d86d1-3ffc-4bbc-b41a-d536300dbfb4",
                    "LayerId": "705afda3-0546-4a17-a3f2-ea35755f04db"
                }
            ]
        },
        {
            "id": "565c4eff-dbd7-4d3c-a0b4-37ab16984c69",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0ba17d79-9b7a-4542-b47e-647c48d9c31d",
            "compositeImage": {
                "id": "8537329a-b189-4362-9519-92d8e283db79",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "565c4eff-dbd7-4d3c-a0b4-37ab16984c69",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48ffc807-a692-473a-bd6c-55b1ca406ab6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "565c4eff-dbd7-4d3c-a0b4-37ab16984c69",
                    "LayerId": "705afda3-0546-4a17-a3f2-ea35755f04db"
                }
            ]
        },
        {
            "id": "76ee08f0-57f7-4cbf-a7f2-f0d2c3b663fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0ba17d79-9b7a-4542-b47e-647c48d9c31d",
            "compositeImage": {
                "id": "d5dceef1-40a9-4450-9626-7230dff116d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76ee08f0-57f7-4cbf-a7f2-f0d2c3b663fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4155fd6f-fdcf-47d0-8b58-8b1fd60b6b64",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76ee08f0-57f7-4cbf-a7f2-f0d2c3b663fe",
                    "LayerId": "705afda3-0546-4a17-a3f2-ea35755f04db"
                }
            ]
        },
        {
            "id": "ea892124-8ca1-49ce-ae7f-e067dfbeee72",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0ba17d79-9b7a-4542-b47e-647c48d9c31d",
            "compositeImage": {
                "id": "c110e2e4-f3ea-4163-b700-940c312680f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea892124-8ca1-49ce-ae7f-e067dfbeee72",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e5a01b0-7dcb-4b27-879f-e7df9d560946",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea892124-8ca1-49ce-ae7f-e067dfbeee72",
                    "LayerId": "705afda3-0546-4a17-a3f2-ea35755f04db"
                }
            ]
        },
        {
            "id": "b203e671-f569-4674-8bfc-d8a63959a048",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0ba17d79-9b7a-4542-b47e-647c48d9c31d",
            "compositeImage": {
                "id": "b9996116-d557-49fe-9b92-c0ad0b0b0f02",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b203e671-f569-4674-8bfc-d8a63959a048",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "106e1a30-7e1f-4569-9a5f-7648f5bb8632",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b203e671-f569-4674-8bfc-d8a63959a048",
                    "LayerId": "705afda3-0546-4a17-a3f2-ea35755f04db"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "705afda3-0546-4a17-a3f2-ea35755f04db",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0ba17d79-9b7a-4542-b47e-647c48d9c31d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}