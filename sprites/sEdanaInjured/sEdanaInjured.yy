{
    "id": "14ef877e-2b7f-45ee-a95b-cc4d684c5671",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sEdanaInjured",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 110,
    "bbox_left": 35,
    "bbox_right": 79,
    "bbox_top": 23,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c9cc5364-9b68-45a8-8ffc-b571d8e60d5b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "14ef877e-2b7f-45ee-a95b-cc4d684c5671",
            "compositeImage": {
                "id": "16114d01-7ac2-40f6-afd5-e6f9eace8fde",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9cc5364-9b68-45a8-8ffc-b571d8e60d5b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f25661f9-e6aa-4cd8-af24-1f39bb479839",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9cc5364-9b68-45a8-8ffc-b571d8e60d5b",
                    "LayerId": "10975f09-eddb-4dcf-afb5-a5caea7de40b"
                }
            ]
        },
        {
            "id": "a9b1f1df-6c0a-4fde-a51d-c16fb47c215c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "14ef877e-2b7f-45ee-a95b-cc4d684c5671",
            "compositeImage": {
                "id": "238c2154-bc0b-4565-8d44-9c7a46725d9f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a9b1f1df-6c0a-4fde-a51d-c16fb47c215c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "69e14997-0cb9-423a-ac3b-89af309a0086",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a9b1f1df-6c0a-4fde-a51d-c16fb47c215c",
                    "LayerId": "10975f09-eddb-4dcf-afb5-a5caea7de40b"
                }
            ]
        },
        {
            "id": "680995fc-5d87-4a5b-8096-7a8869291373",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "14ef877e-2b7f-45ee-a95b-cc4d684c5671",
            "compositeImage": {
                "id": "95293b7d-5050-48e4-9f96-fc8f4a3268eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "680995fc-5d87-4a5b-8096-7a8869291373",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "856be95b-fadd-47ce-a440-9ed226cc2c0f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "680995fc-5d87-4a5b-8096-7a8869291373",
                    "LayerId": "10975f09-eddb-4dcf-afb5-a5caea7de40b"
                }
            ]
        },
        {
            "id": "b7e700af-11ce-4a90-8e3b-b296a869b527",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "14ef877e-2b7f-45ee-a95b-cc4d684c5671",
            "compositeImage": {
                "id": "481ab7f4-a354-4966-895f-ebf10dd26737",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7e700af-11ce-4a90-8e3b-b296a869b527",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21a57d0e-088a-4c03-aba2-ab06f81e92b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7e700af-11ce-4a90-8e3b-b296a869b527",
                    "LayerId": "10975f09-eddb-4dcf-afb5-a5caea7de40b"
                }
            ]
        },
        {
            "id": "7f321239-cbf8-42f1-bc49-e24314c68402",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "14ef877e-2b7f-45ee-a95b-cc4d684c5671",
            "compositeImage": {
                "id": "74a949c3-c334-4a35-9e6e-a853c26f73a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f321239-cbf8-42f1-bc49-e24314c68402",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6edb0b68-055d-405e-9704-23fbee73c430",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f321239-cbf8-42f1-bc49-e24314c68402",
                    "LayerId": "10975f09-eddb-4dcf-afb5-a5caea7de40b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "10975f09-eddb-4dcf-afb5-a5caea7de40b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "14ef877e-2b7f-45ee-a95b-cc4d684c5671",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}