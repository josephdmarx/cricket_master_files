{
    "id": "868428b9-3b74-491f-87cf-87497f119190",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sApple",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 9,
    "bbox_right": 22,
    "bbox_top": 9,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "43d18a9e-a138-4afb-8c0a-0d9c08987284",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "868428b9-3b74-491f-87cf-87497f119190",
            "compositeImage": {
                "id": "ed1bfd9b-cd27-4ec7-a150-6f7e0c94e3d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43d18a9e-a138-4afb-8c0a-0d9c08987284",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7aa6b93e-5a50-46fd-9d5b-2cc885a926b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43d18a9e-a138-4afb-8c0a-0d9c08987284",
                    "LayerId": "e4303ba6-aecb-4aba-b7bf-7f590a8dc00c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "e4303ba6-aecb-4aba-b7bf-7f590a8dc00c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "868428b9-3b74-491f-87cf-87497f119190",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}