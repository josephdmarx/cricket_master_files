{
    "id": "ef743377-cd08-49be-bf11-240563d6b1bd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayerWalk",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 94,
    "bbox_left": 58,
    "bbox_right": 72,
    "bbox_top": 42,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "899c2adb-d029-4534-b76f-0c3cad26278d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ef743377-cd08-49be-bf11-240563d6b1bd",
            "compositeImage": {
                "id": "a2e4947b-4f2a-494e-acde-dc070f30d354",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "899c2adb-d029-4534-b76f-0c3cad26278d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7587c317-4fea-4a9f-890b-486c881b2ee1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "899c2adb-d029-4534-b76f-0c3cad26278d",
                    "LayerId": "afbb39e4-8e1a-4e1f-9268-00ce9b11555f"
                }
            ]
        },
        {
            "id": "6ce76818-efc4-4ffa-8960-335a37d6ad92",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ef743377-cd08-49be-bf11-240563d6b1bd",
            "compositeImage": {
                "id": "6867ebd6-7f64-45c1-afa4-78236b69e01e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ce76818-efc4-4ffa-8960-335a37d6ad92",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac276879-b599-44ce-9bec-4b9e4f55e858",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ce76818-efc4-4ffa-8960-335a37d6ad92",
                    "LayerId": "afbb39e4-8e1a-4e1f-9268-00ce9b11555f"
                }
            ]
        },
        {
            "id": "33ab0e23-483a-4122-9374-fc333558a968",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ef743377-cd08-49be-bf11-240563d6b1bd",
            "compositeImage": {
                "id": "a046ef4e-8caa-4fcf-a2c8-ae8112961598",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33ab0e23-483a-4122-9374-fc333558a968",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ec02282-2e07-4a74-8777-1294d5e957b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33ab0e23-483a-4122-9374-fc333558a968",
                    "LayerId": "afbb39e4-8e1a-4e1f-9268-00ce9b11555f"
                }
            ]
        },
        {
            "id": "bc1b160a-6e22-465f-b472-7a39bcd457cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ef743377-cd08-49be-bf11-240563d6b1bd",
            "compositeImage": {
                "id": "4d5e2d13-ecdd-4205-8db4-305b0708c324",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc1b160a-6e22-465f-b472-7a39bcd457cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c750d831-bee0-4d6f-b389-2eb8953f7dd9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc1b160a-6e22-465f-b472-7a39bcd457cd",
                    "LayerId": "afbb39e4-8e1a-4e1f-9268-00ce9b11555f"
                }
            ]
        },
        {
            "id": "9515d177-cb45-4384-9dc1-e32f81501914",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ef743377-cd08-49be-bf11-240563d6b1bd",
            "compositeImage": {
                "id": "176fc528-3452-4ca8-8346-30cae32b215c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9515d177-cb45-4384-9dc1-e32f81501914",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6cc26323-237b-40b8-801b-9a25a42335df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9515d177-cb45-4384-9dc1-e32f81501914",
                    "LayerId": "afbb39e4-8e1a-4e1f-9268-00ce9b11555f"
                }
            ]
        },
        {
            "id": "eebb483a-00be-4598-a003-dd1ff0166504",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ef743377-cd08-49be-bf11-240563d6b1bd",
            "compositeImage": {
                "id": "e77a9856-5269-497a-80ac-45d88336d1d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eebb483a-00be-4598-a003-dd1ff0166504",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c873e29a-9c60-4e8c-abe6-e1adf83db7d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eebb483a-00be-4598-a003-dd1ff0166504",
                    "LayerId": "afbb39e4-8e1a-4e1f-9268-00ce9b11555f"
                }
            ]
        },
        {
            "id": "ac53eb60-c561-4d21-ac68-fca1338369c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ef743377-cd08-49be-bf11-240563d6b1bd",
            "compositeImage": {
                "id": "fb518630-26e5-4e5e-9f0a-c63b4a285126",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac53eb60-c561-4d21-ac68-fca1338369c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "137b4821-7bfc-42d4-ab8c-2dd82c04b8ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac53eb60-c561-4d21-ac68-fca1338369c5",
                    "LayerId": "afbb39e4-8e1a-4e1f-9268-00ce9b11555f"
                }
            ]
        },
        {
            "id": "13b579fa-4cb6-474b-8c7d-2714d4f20127",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ef743377-cd08-49be-bf11-240563d6b1bd",
            "compositeImage": {
                "id": "56a1a062-0803-454e-9a5e-863e665909b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13b579fa-4cb6-474b-8c7d-2714d4f20127",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "78670a20-335c-47e7-aa72-aedaa4a2d1b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13b579fa-4cb6-474b-8c7d-2714d4f20127",
                    "LayerId": "afbb39e4-8e1a-4e1f-9268-00ce9b11555f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "afbb39e4-8e1a-4e1f-9268-00ce9b11555f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ef743377-cd08-49be-bf11-240563d6b1bd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}