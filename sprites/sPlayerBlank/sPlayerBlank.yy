{
    "id": "8034abc1-5a0d-49f2-b1f8-7edf269b5e1b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayerBlank",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 94,
    "bbox_left": 58,
    "bbox_right": 72,
    "bbox_top": 42,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5c11fc37-1716-4e77-af73-874ba578be59",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8034abc1-5a0d-49f2-b1f8-7edf269b5e1b",
            "compositeImage": {
                "id": "7d4887b8-0ae0-467c-b748-90ef5636dc14",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c11fc37-1716-4e77-af73-874ba578be59",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f58b475a-a35c-4d63-ba2a-b921cd741668",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c11fc37-1716-4e77-af73-874ba578be59",
                    "LayerId": "b93a5ecf-1ffa-44e3-b8a6-874e82c89994"
                }
            ]
        },
        {
            "id": "4ba181fb-d20c-47dd-89e0-6d3762042173",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8034abc1-5a0d-49f2-b1f8-7edf269b5e1b",
            "compositeImage": {
                "id": "4b060bf4-360e-47ab-85db-373d6af23f6b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ba181fb-d20c-47dd-89e0-6d3762042173",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4670a9c-c7b1-4dc6-9678-a0445f04bd0c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ba181fb-d20c-47dd-89e0-6d3762042173",
                    "LayerId": "b93a5ecf-1ffa-44e3-b8a6-874e82c89994"
                }
            ]
        },
        {
            "id": "4ebb5ec0-d187-42b4-94cd-2bb372cbc242",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8034abc1-5a0d-49f2-b1f8-7edf269b5e1b",
            "compositeImage": {
                "id": "37717aad-1835-4fe2-848d-e9d38a27bb37",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ebb5ec0-d187-42b4-94cd-2bb372cbc242",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2def148e-3b75-4806-a83a-73f9cd03876a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ebb5ec0-d187-42b4-94cd-2bb372cbc242",
                    "LayerId": "b93a5ecf-1ffa-44e3-b8a6-874e82c89994"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "b93a5ecf-1ffa-44e3-b8a6-874e82c89994",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8034abc1-5a0d-49f2-b1f8-7edf269b5e1b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}