{
    "id": "c0a7cead-9e47-474d-9f8e-29907bd6e9dc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Decals01_Meadow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 504,
    "bbox_left": 17,
    "bbox_right": 825,
    "bbox_top": 19,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6b9297c3-6da7-4bc7-9bfa-9965149b500d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0a7cead-9e47-474d-9f8e-29907bd6e9dc",
            "compositeImage": {
                "id": "38dd2347-d1f9-4d87-9e5e-a96af6099216",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b9297c3-6da7-4bc7-9bfa-9965149b500d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "215622bb-8f68-4a17-ae36-8a729dc3df6b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b9297c3-6da7-4bc7-9bfa-9965149b500d",
                    "LayerId": "d01ba46a-a830-4b26-bdb6-2f3da4276523"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "d01ba46a-a830-4b26-bdb6-2f3da4276523",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c0a7cead-9e47-474d-9f8e-29907bd6e9dc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 986,
    "xorig": 0,
    "yorig": 0
}