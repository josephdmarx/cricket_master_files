{
    "id": "729117ab-0ab2-44bb-aab5-4487af6544c0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTrees03",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 359,
    "bbox_left": 0,
    "bbox_right": 639,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ee8cf60a-7e48-402f-bb0d-6f44e71ae4ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "729117ab-0ab2-44bb-aab5-4487af6544c0",
            "compositeImage": {
                "id": "dc44b555-7834-4fe3-ab92-a4c53b156ab3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee8cf60a-7e48-402f-bb0d-6f44e71ae4ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c94fec8d-1b8b-4128-9efc-ae05e04d8c74",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee8cf60a-7e48-402f-bb0d-6f44e71ae4ff",
                    "LayerId": "cc1ce5eb-71e6-46e6-a5e7-e1cf0d4d3012"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 360,
    "layers": [
        {
            "id": "cc1ce5eb-71e6-46e6-a5e7-e1cf0d4d3012",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "729117ab-0ab2-44bb-aab5-4487af6544c0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 0,
    "yorig": 0
}