{
    "id": "0cd4a353-254d-4ab6-b812-4c56e4daf369",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayerA1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 94,
    "bbox_left": 58,
    "bbox_right": 72,
    "bbox_top": 42,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0e1477cc-5ef8-46db-85f8-984131daef36",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0cd4a353-254d-4ab6-b812-4c56e4daf369",
            "compositeImage": {
                "id": "8e2069a0-8fd6-4a21-8bc2-96ea64c50336",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e1477cc-5ef8-46db-85f8-984131daef36",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89ea0500-9b5f-4172-a968-dcb69f4f9e92",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e1477cc-5ef8-46db-85f8-984131daef36",
                    "LayerId": "f4bd320b-1069-4dab-9223-15af0f277151"
                }
            ]
        },
        {
            "id": "3cf7673d-7b11-45c3-b737-9d5974a0c9eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0cd4a353-254d-4ab6-b812-4c56e4daf369",
            "compositeImage": {
                "id": "e524bf2c-41fb-4f07-9e26-d0801d9afed3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3cf7673d-7b11-45c3-b737-9d5974a0c9eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "17596050-7211-47d7-aa5f-56cbc3aa0cb8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3cf7673d-7b11-45c3-b737-9d5974a0c9eb",
                    "LayerId": "f4bd320b-1069-4dab-9223-15af0f277151"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "f4bd320b-1069-4dab-9223-15af0f277151",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0cd4a353-254d-4ab6-b812-4c56e4daf369",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}