{
    "id": "61e98ca8-d6fe-41d2-9ba4-5429d0b0c396",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sMainMenuBG",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 359,
    "bbox_left": 0,
    "bbox_right": 639,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0a55dc8b-8c9d-453d-995c-066acde6d4ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61e98ca8-d6fe-41d2-9ba4-5429d0b0c396",
            "compositeImage": {
                "id": "00ea73a6-3c64-4250-a29b-d532bd338ee3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a55dc8b-8c9d-453d-995c-066acde6d4ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a4dd252-aac6-447b-9329-7056362d8337",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a55dc8b-8c9d-453d-995c-066acde6d4ad",
                    "LayerId": "dcb6715e-d3f3-4648-8540-16d8b65a6235"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 360,
    "layers": [
        {
            "id": "dcb6715e-d3f3-4648-8540-16d8b65a6235",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "61e98ca8-d6fe-41d2-9ba4-5429d0b0c396",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 0,
    "yorig": 0
}