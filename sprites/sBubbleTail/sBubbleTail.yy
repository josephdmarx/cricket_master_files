{
    "id": "f9cab155-6162-42a4-aca3-acb6fa88e487",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBubbleTail",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 8,
    "bbox_left": 0,
    "bbox_right": 8,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0a37adb6-699e-4222-9a19-38323ad7f1e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f9cab155-6162-42a4-aca3-acb6fa88e487",
            "compositeImage": {
                "id": "1087d326-415a-47a5-9bda-f6459946dc62",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a37adb6-699e-4222-9a19-38323ad7f1e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7fc36035-24c4-4718-88a5-069ccb019a61",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a37adb6-699e-4222-9a19-38323ad7f1e7",
                    "LayerId": "5a3f0020-a541-4760-86b5-c3fb509f8512"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 9,
    "layers": [
        {
            "id": "5a3f0020-a541-4760-86b5-c3fb509f8512",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f9cab155-6162-42a4-aca3-acb6fa88e487",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 1,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 9,
    "xorig": 4,
    "yorig": 0
}