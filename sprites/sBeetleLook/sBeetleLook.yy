{
    "id": "90533dca-6a9a-4fe9-842f-395af5959d54",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBeetleLook",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 46,
    "bbox_right": 73,
    "bbox_top": 36,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1f64b6a7-a8de-4961-a26d-a2d1c1900bfa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "90533dca-6a9a-4fe9-842f-395af5959d54",
            "compositeImage": {
                "id": "ba7c59ee-5e3a-4e32-bd85-ccd4a37ee446",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f64b6a7-a8de-4961-a26d-a2d1c1900bfa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "adecd574-424a-40e0-9a5e-570f5caf89e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f64b6a7-a8de-4961-a26d-a2d1c1900bfa",
                    "LayerId": "327df049-472c-48e8-8445-059e0623375f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "327df049-472c-48e8-8445-059e0623375f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "90533dca-6a9a-4fe9-842f-395af5959d54",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}