{
    "id": "85e3bbea-8c67-4dfc-87cb-5d787bf16d87",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sEssence00",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 56,
    "bbox_left": 5,
    "bbox_right": 24,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "beb9f955-e62c-4cfe-8228-8893219d3060",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "85e3bbea-8c67-4dfc-87cb-5d787bf16d87",
            "compositeImage": {
                "id": "bec5f1c3-b447-4eb0-9c3a-0e6921eecd2b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "beb9f955-e62c-4cfe-8228-8893219d3060",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c22dd1a8-6685-4e71-88f7-9d0a19f4f933",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "beb9f955-e62c-4cfe-8228-8893219d3060",
                    "LayerId": "31308b90-6a3c-4b7e-a461-2f5aa36953bf"
                }
            ]
        },
        {
            "id": "386f6aac-11b5-498f-bb28-8aa0a7b45a30",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "85e3bbea-8c67-4dfc-87cb-5d787bf16d87",
            "compositeImage": {
                "id": "fac3f1df-d39d-456d-8a8a-aa48aa00a900",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "386f6aac-11b5-498f-bb28-8aa0a7b45a30",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b4082687-5a69-4a10-bf36-7381cdf3195e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "386f6aac-11b5-498f-bb28-8aa0a7b45a30",
                    "LayerId": "31308b90-6a3c-4b7e-a461-2f5aa36953bf"
                }
            ]
        },
        {
            "id": "9cf4b6e9-3148-40d5-bd67-3dced2dadcb2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "85e3bbea-8c67-4dfc-87cb-5d787bf16d87",
            "compositeImage": {
                "id": "ae93428b-b486-4616-9daa-c8de997489ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9cf4b6e9-3148-40d5-bd67-3dced2dadcb2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c566fc56-7c3f-4e74-8173-f7330adc67e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9cf4b6e9-3148-40d5-bd67-3dced2dadcb2",
                    "LayerId": "31308b90-6a3c-4b7e-a461-2f5aa36953bf"
                }
            ]
        },
        {
            "id": "cda536e2-422b-4ca8-a765-d68431d897d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "85e3bbea-8c67-4dfc-87cb-5d787bf16d87",
            "compositeImage": {
                "id": "4e6713a5-3318-457c-8068-ae6162c3eef7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cda536e2-422b-4ca8-a765-d68431d897d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c23015fc-4ccd-4535-ae5a-0f86a0b9325d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cda536e2-422b-4ca8-a765-d68431d897d3",
                    "LayerId": "31308b90-6a3c-4b7e-a461-2f5aa36953bf"
                }
            ]
        },
        {
            "id": "99eea16e-7e84-4780-aebe-4564ce0d26dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "85e3bbea-8c67-4dfc-87cb-5d787bf16d87",
            "compositeImage": {
                "id": "15273baf-5dbf-49e7-902e-5e7d71e68643",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99eea16e-7e84-4780-aebe-4564ce0d26dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca462c4a-6e73-4caa-83de-25c5c8d3d89a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99eea16e-7e84-4780-aebe-4564ce0d26dc",
                    "LayerId": "31308b90-6a3c-4b7e-a461-2f5aa36953bf"
                }
            ]
        },
        {
            "id": "2086ecc9-44a3-46b8-85c9-d53ad00e6fba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "85e3bbea-8c67-4dfc-87cb-5d787bf16d87",
            "compositeImage": {
                "id": "21462c67-6a58-44cc-aefd-478710527cc7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2086ecc9-44a3-46b8-85c9-d53ad00e6fba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7062cf7-1966-4aa0-9e82-142fb4506c72",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2086ecc9-44a3-46b8-85c9-d53ad00e6fba",
                    "LayerId": "31308b90-6a3c-4b7e-a461-2f5aa36953bf"
                }
            ]
        },
        {
            "id": "5c19a179-2e2c-4fc6-a513-c97a84bbd17e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "85e3bbea-8c67-4dfc-87cb-5d787bf16d87",
            "compositeImage": {
                "id": "0cc1e53b-476e-43fe-856f-1de04ab2487c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c19a179-2e2c-4fc6-a513-c97a84bbd17e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf05912c-904a-4df6-8066-a6b59ec7be13",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c19a179-2e2c-4fc6-a513-c97a84bbd17e",
                    "LayerId": "31308b90-6a3c-4b7e-a461-2f5aa36953bf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "31308b90-6a3c-4b7e-a461-2f5aa36953bf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "85e3bbea-8c67-4dfc-87cb-5d787bf16d87",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 43
}