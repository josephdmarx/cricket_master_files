{
    "id": "a4c6e877-f34d-4d74-85de-c1bacc8bf275",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sAppleYellow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 9,
    "bbox_right": 22,
    "bbox_top": 9,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4dede301-5b66-4d8f-83d6-a5edacd14c13",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4c6e877-f34d-4d74-85de-c1bacc8bf275",
            "compositeImage": {
                "id": "9c736538-59b7-4fe8-9eca-8736d450276e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4dede301-5b66-4d8f-83d6-a5edacd14c13",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "966b5c59-b5e6-4baf-bbb8-9fd0ff822054",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4dede301-5b66-4d8f-83d6-a5edacd14c13",
                    "LayerId": "5003cbe3-379e-4b04-b636-a6af8a43743f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "5003cbe3-379e-4b04-b636-a6af8a43743f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a4c6e877-f34d-4d74-85de-c1bacc8bf275",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}