{
    "id": "c38a45c6-1741-41a2-adc4-a23283fa1c0e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sEnergyDark_Cast01",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 117,
    "bbox_left": 72,
    "bbox_right": 119,
    "bbox_top": 52,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3b636a39-0b34-4d55-b5ed-48bf777314c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c38a45c6-1741-41a2-adc4-a23283fa1c0e",
            "compositeImage": {
                "id": "f3326a42-18ed-4f19-81a2-ebc996a7e5c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b636a39-0b34-4d55-b5ed-48bf777314c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ba089f2-a223-4d18-b373-f7a283a43485",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b636a39-0b34-4d55-b5ed-48bf777314c2",
                    "LayerId": "c45d531a-75b8-4fc3-a442-d36b85d6e6fa"
                }
            ]
        },
        {
            "id": "be02ffaa-1591-431d-a823-34f1bb50ece0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c38a45c6-1741-41a2-adc4-a23283fa1c0e",
            "compositeImage": {
                "id": "6cabd1c0-83e2-4020-aeb0-9ca819e2af7a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be02ffaa-1591-431d-a823-34f1bb50ece0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7859224a-77b6-478f-9c5a-253d03aa25fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be02ffaa-1591-431d-a823-34f1bb50ece0",
                    "LayerId": "c45d531a-75b8-4fc3-a442-d36b85d6e6fa"
                }
            ]
        },
        {
            "id": "3661b160-be15-4929-aa42-ab8f84d1b4b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c38a45c6-1741-41a2-adc4-a23283fa1c0e",
            "compositeImage": {
                "id": "e666b43c-adc9-4658-8797-66c08e4de7c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3661b160-be15-4929-aa42-ab8f84d1b4b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1b414ba-8719-446d-8ac7-04bd549fae60",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3661b160-be15-4929-aa42-ab8f84d1b4b0",
                    "LayerId": "c45d531a-75b8-4fc3-a442-d36b85d6e6fa"
                }
            ]
        },
        {
            "id": "615125bb-ae48-43ad-a1fd-64aef2cf47b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c38a45c6-1741-41a2-adc4-a23283fa1c0e",
            "compositeImage": {
                "id": "56b2e16f-97d1-45c8-b325-72e0924e5811",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "615125bb-ae48-43ad-a1fd-64aef2cf47b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45cae831-c9ef-4d4f-9384-4f44425c535b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "615125bb-ae48-43ad-a1fd-64aef2cf47b3",
                    "LayerId": "c45d531a-75b8-4fc3-a442-d36b85d6e6fa"
                }
            ]
        },
        {
            "id": "59533cad-70a3-4421-aa38-ad993c636cc4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c38a45c6-1741-41a2-adc4-a23283fa1c0e",
            "compositeImage": {
                "id": "bca16594-1eeb-4a71-8b22-84810a216842",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59533cad-70a3-4421-aa38-ad993c636cc4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb2970f5-e55d-4a3d-8429-f9e1541dad52",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59533cad-70a3-4421-aa38-ad993c636cc4",
                    "LayerId": "c45d531a-75b8-4fc3-a442-d36b85d6e6fa"
                }
            ]
        },
        {
            "id": "aa0d6509-3129-4362-8ed7-36be647020f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c38a45c6-1741-41a2-adc4-a23283fa1c0e",
            "compositeImage": {
                "id": "d5abb017-656e-4aa3-a7fd-db1234b8b8f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa0d6509-3129-4362-8ed7-36be647020f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e28662e-bcb1-43d9-9d96-cd53218e1624",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa0d6509-3129-4362-8ed7-36be647020f7",
                    "LayerId": "c45d531a-75b8-4fc3-a442-d36b85d6e6fa"
                }
            ]
        },
        {
            "id": "8f31507f-7053-48d1-9eef-49a4ed52ee86",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c38a45c6-1741-41a2-adc4-a23283fa1c0e",
            "compositeImage": {
                "id": "83ef9cc9-91f2-4681-90e6-794123f8cbf2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f31507f-7053-48d1-9eef-49a4ed52ee86",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8cd581f1-3062-4d3c-a292-4e0c9098bb2d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f31507f-7053-48d1-9eef-49a4ed52ee86",
                    "LayerId": "c45d531a-75b8-4fc3-a442-d36b85d6e6fa"
                }
            ]
        },
        {
            "id": "b616f221-edfb-4318-b799-8538e6f92a7b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c38a45c6-1741-41a2-adc4-a23283fa1c0e",
            "compositeImage": {
                "id": "53946a0f-157a-4855-9dd5-85f6c6779595",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b616f221-edfb-4318-b799-8538e6f92a7b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8e92883-d27d-4ee0-b032-656d3a29e1cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b616f221-edfb-4318-b799-8538e6f92a7b",
                    "LayerId": "c45d531a-75b8-4fc3-a442-d36b85d6e6fa"
                }
            ]
        },
        {
            "id": "3b792584-40d8-410e-b798-8ace049abb87",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c38a45c6-1741-41a2-adc4-a23283fa1c0e",
            "compositeImage": {
                "id": "1c77b076-7273-4c19-ad19-0c962f1ecedd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b792584-40d8-410e-b798-8ace049abb87",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ea28b41-169a-4b57-a0f4-bafabcf2edaf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b792584-40d8-410e-b798-8ace049abb87",
                    "LayerId": "c45d531a-75b8-4fc3-a442-d36b85d6e6fa"
                }
            ]
        },
        {
            "id": "83111a8a-1c3e-4263-bea3-2a00f5ef7eab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c38a45c6-1741-41a2-adc4-a23283fa1c0e",
            "compositeImage": {
                "id": "ee56ae03-9343-42ed-ab0a-441f404cef9f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "83111a8a-1c3e-4263-bea3-2a00f5ef7eab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9a5cb57-97c0-40ae-9166-a8baf5983b32",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "83111a8a-1c3e-4263-bea3-2a00f5ef7eab",
                    "LayerId": "c45d531a-75b8-4fc3-a442-d36b85d6e6fa"
                }
            ]
        },
        {
            "id": "1bb2d472-b552-4027-b6cd-f1159677894b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c38a45c6-1741-41a2-adc4-a23283fa1c0e",
            "compositeImage": {
                "id": "303d4a35-6b17-4d82-a856-2e8636dd1a2c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1bb2d472-b552-4027-b6cd-f1159677894b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "179749a9-ddb9-428c-964f-55339c7e2d4a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1bb2d472-b552-4027-b6cd-f1159677894b",
                    "LayerId": "c45d531a-75b8-4fc3-a442-d36b85d6e6fa"
                }
            ]
        },
        {
            "id": "37dd1c46-0353-4b45-b6c2-e4f11754037c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c38a45c6-1741-41a2-adc4-a23283fa1c0e",
            "compositeImage": {
                "id": "048efaf6-a9bc-4494-9535-cd8f223b94e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "37dd1c46-0353-4b45-b6c2-e4f11754037c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae15f9e0-e8a2-49fb-8ed4-10d71becc028",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37dd1c46-0353-4b45-b6c2-e4f11754037c",
                    "LayerId": "c45d531a-75b8-4fc3-a442-d36b85d6e6fa"
                }
            ]
        },
        {
            "id": "1f8886e5-1d76-454d-b154-208396b82234",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c38a45c6-1741-41a2-adc4-a23283fa1c0e",
            "compositeImage": {
                "id": "e8fe0c05-941a-4397-acef-74b392907440",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f8886e5-1d76-454d-b154-208396b82234",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1481466a-d328-425e-872e-2d51d24978b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f8886e5-1d76-454d-b154-208396b82234",
                    "LayerId": "c45d531a-75b8-4fc3-a442-d36b85d6e6fa"
                }
            ]
        },
        {
            "id": "bcd6c7e5-6455-468d-9dd0-2bc495297b03",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c38a45c6-1741-41a2-adc4-a23283fa1c0e",
            "compositeImage": {
                "id": "47074621-d3a5-485f-a2fb-c66ac6493c94",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bcd6c7e5-6455-468d-9dd0-2bc495297b03",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "63538888-9b2a-46ad-8053-29aa2a6a17f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bcd6c7e5-6455-468d-9dd0-2bc495297b03",
                    "LayerId": "c45d531a-75b8-4fc3-a442-d36b85d6e6fa"
                }
            ]
        },
        {
            "id": "d4ca9413-ea0b-4415-b156-b4ebc2f13d2d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c38a45c6-1741-41a2-adc4-a23283fa1c0e",
            "compositeImage": {
                "id": "6308525b-a731-402e-beb5-7a430c02eb8c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4ca9413-ea0b-4415-b156-b4ebc2f13d2d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1331be8f-7a5c-4524-8dac-5bd8395c6882",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4ca9413-ea0b-4415-b156-b4ebc2f13d2d",
                    "LayerId": "c45d531a-75b8-4fc3-a442-d36b85d6e6fa"
                }
            ]
        },
        {
            "id": "f908f222-167e-4d58-8e28-bbbc6bb9a23c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c38a45c6-1741-41a2-adc4-a23283fa1c0e",
            "compositeImage": {
                "id": "b76c2ef6-ae0d-490e-bd37-36e8abeacacf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f908f222-167e-4d58-8e28-bbbc6bb9a23c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd85d58a-4e24-46e8-832f-a345d8713cc6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f908f222-167e-4d58-8e28-bbbc6bb9a23c",
                    "LayerId": "c45d531a-75b8-4fc3-a442-d36b85d6e6fa"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 200,
    "layers": [
        {
            "id": "c45d531a-75b8-4fc3-a442-d36b85d6e6fa",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c38a45c6-1741-41a2-adc4-a23283fa1c0e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 200,
    "xorig": 109,
    "yorig": 61
}