{
    "id": "739aa8c2-caac-4117-ba8d-13cc71474be9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sIndigoBird",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 7,
    "bbox_right": 25,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "77de8416-682d-4e64-baee-f00785235ccf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "739aa8c2-caac-4117-ba8d-13cc71474be9",
            "compositeImage": {
                "id": "491a825d-dac8-43c4-b1df-1dd4631b84e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77de8416-682d-4e64-baee-f00785235ccf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "41418c6e-b852-48cd-82f8-079e45557a55",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77de8416-682d-4e64-baee-f00785235ccf",
                    "LayerId": "976972bc-ed8c-4f87-b2d2-57f9c7841144"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "976972bc-ed8c-4f87-b2d2-57f9c7841144",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "739aa8c2-caac-4117-ba8d-13cc71474be9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 15,
    "yorig": 25
}