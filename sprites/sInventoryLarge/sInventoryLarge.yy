{
    "id": "343c179d-3f55-4042-ae49-ba69a948bc5f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sInventoryLarge",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 198,
    "bbox_left": 1,
    "bbox_right": 438,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1fdff75d-7c09-4de9-add5-37faa4e463e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "343c179d-3f55-4042-ae49-ba69a948bc5f",
            "compositeImage": {
                "id": "f10bf859-736d-43f7-99ce-ec6814151f40",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1fdff75d-7c09-4de9-add5-37faa4e463e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39130735-b8b9-419c-9f6f-4b57abb7dd39",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1fdff75d-7c09-4de9-add5-37faa4e463e0",
                    "LayerId": "d451e9e5-b595-4294-bfa9-4c25d508a123"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 235,
    "layers": [
        {
            "id": "d451e9e5-b595-4294-bfa9-4c25d508a123",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "343c179d-3f55-4042-ae49-ba69a948bc5f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 439,
    "xorig": 1,
    "yorig": 1
}