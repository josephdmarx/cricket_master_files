{
    "id": "d08cce73-3ab9-46de-abf8-b3a977c70211",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayerHurt",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 94,
    "bbox_left": 58,
    "bbox_right": 72,
    "bbox_top": 42,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "176d1ea4-b3c2-438d-92ef-d10819d15f68",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d08cce73-3ab9-46de-abf8-b3a977c70211",
            "compositeImage": {
                "id": "351d8790-2e65-4f8b-ae9d-bb2cd53fa82d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "176d1ea4-b3c2-438d-92ef-d10819d15f68",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7150c49b-be01-483b-9e93-192617306fc5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "176d1ea4-b3c2-438d-92ef-d10819d15f68",
                    "LayerId": "b955362a-3e61-43b1-b084-4672ab3edcc6"
                }
            ]
        },
        {
            "id": "bed211c4-9e2a-4cf3-a5b1-47b6795cfd18",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d08cce73-3ab9-46de-abf8-b3a977c70211",
            "compositeImage": {
                "id": "6db4edb0-0a13-46d0-986f-1d1bc749cbc3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bed211c4-9e2a-4cf3-a5b1-47b6795cfd18",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c981090-cf98-4481-8f7e-0daa7e8be8a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bed211c4-9e2a-4cf3-a5b1-47b6795cfd18",
                    "LayerId": "b955362a-3e61-43b1-b084-4672ab3edcc6"
                }
            ]
        },
        {
            "id": "8aabea28-7799-493d-884f-3c9f29e05910",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d08cce73-3ab9-46de-abf8-b3a977c70211",
            "compositeImage": {
                "id": "7c563a3d-03c4-4706-af7b-03ea958884b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8aabea28-7799-493d-884f-3c9f29e05910",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8679e0e9-ca59-4ea8-9062-251dba1eac4e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8aabea28-7799-493d-884f-3c9f29e05910",
                    "LayerId": "b955362a-3e61-43b1-b084-4672ab3edcc6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "b955362a-3e61-43b1-b084-4672ab3edcc6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d08cce73-3ab9-46de-abf8-b3a977c70211",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}