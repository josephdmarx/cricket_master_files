{
    "id": "571d2c3b-bc23-4ff7-921f-c6269a95dc15",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayerSleep",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 94,
    "bbox_left": 44,
    "bbox_right": 58,
    "bbox_top": 42,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "405beffe-01cb-49b8-91b1-bf08a3d52f2e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "571d2c3b-bc23-4ff7-921f-c6269a95dc15",
            "compositeImage": {
                "id": "ac9b27e2-557b-4799-877b-c9747e450d33",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "405beffe-01cb-49b8-91b1-bf08a3d52f2e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "774a6ae0-ab1b-4147-8a99-46078cd0ecf4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "405beffe-01cb-49b8-91b1-bf08a3d52f2e",
                    "LayerId": "ae07d1b8-4edb-458b-9586-1f7381fc3ec1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "ae07d1b8-4edb-458b-9586-1f7381fc3ec1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "571d2c3b-bc23-4ff7-921f-c6269a95dc15",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}