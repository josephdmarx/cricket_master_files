{
    "id": "771c3d65-b176-4f24-aca7-d3302be75519",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCricketHouse01",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 359,
    "bbox_left": 0,
    "bbox_right": 639,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f28942bc-1c66-462d-8f64-765f1df9b2f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "771c3d65-b176-4f24-aca7-d3302be75519",
            "compositeImage": {
                "id": "1476667b-62f3-4188-99cb-ec1aec25fbc2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f28942bc-1c66-462d-8f64-765f1df9b2f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4da56e04-e84e-4a64-a817-4b106b76fe6d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f28942bc-1c66-462d-8f64-765f1df9b2f9",
                    "LayerId": "43bbef1f-41b7-41d0-baa8-49475aa3e9c8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 360,
    "layers": [
        {
            "id": "43bbef1f-41b7-41d0-baa8-49475aa3e9c8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "771c3d65-b176-4f24-aca7-d3302be75519",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 0,
    "yorig": 0
}