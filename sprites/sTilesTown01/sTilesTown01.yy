{
    "id": "84bb898a-3d49-470b-8da7-d9f72bc4a45a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTilesTown01",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 479,
    "bbox_left": 14,
    "bbox_right": 958,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fbb44c4b-162b-4d83-b1aa-ffa67007b00d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "84bb898a-3d49-470b-8da7-d9f72bc4a45a",
            "compositeImage": {
                "id": "a5bcdee6-76d7-413e-a4a6-e57b51e004e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fbb44c4b-162b-4d83-b1aa-ffa67007b00d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf9dd438-2ddf-45c9-a215-e62d810182a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fbb44c4b-162b-4d83-b1aa-ffa67007b00d",
                    "LayerId": "29c0750e-88c3-4822-96ee-42a73687ab99"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "29c0750e-88c3-4822-96ee-42a73687ab99",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "84bb898a-3d49-470b-8da7-d9f72bc4a45a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1024,
    "xorig": 0,
    "yorig": 0
}