{
    "id": "e1094ae7-dfc5-4aba-89de-ad5d28c691d9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGremlin_WindUpAttack",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 13,
    "bbox_right": 43,
    "bbox_top": 7,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "aecf4bf0-0a51-495b-bd7a-d57fb392b65d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1094ae7-dfc5-4aba-89de-ad5d28c691d9",
            "compositeImage": {
                "id": "f04044fd-b2d3-43d7-a08c-b4731ad28ede",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aecf4bf0-0a51-495b-bd7a-d57fb392b65d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ccc71bd6-4270-4557-b01f-c06d15470206",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aecf4bf0-0a51-495b-bd7a-d57fb392b65d",
                    "LayerId": "84da49de-7e85-4ada-a609-624dc37c22a2"
                }
            ]
        },
        {
            "id": "2d1718aa-0c64-4a45-ac89-017ba335005e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1094ae7-dfc5-4aba-89de-ad5d28c691d9",
            "compositeImage": {
                "id": "e17051ed-e6af-4b16-9f55-416fad823ab3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d1718aa-0c64-4a45-ac89-017ba335005e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4213cc0-ae89-4d8c-b4be-f68baa18e672",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d1718aa-0c64-4a45-ac89-017ba335005e",
                    "LayerId": "84da49de-7e85-4ada-a609-624dc37c22a2"
                }
            ]
        },
        {
            "id": "00315507-facd-432a-850b-2d35a547cb80",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1094ae7-dfc5-4aba-89de-ad5d28c691d9",
            "compositeImage": {
                "id": "61de567d-4a87-4800-baf8-2a42a3402bda",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00315507-facd-432a-850b-2d35a547cb80",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62aafe4b-5ba3-45ba-83dc-b47ebbda1056",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00315507-facd-432a-850b-2d35a547cb80",
                    "LayerId": "84da49de-7e85-4ada-a609-624dc37c22a2"
                }
            ]
        },
        {
            "id": "02c067c5-dc3b-4305-ae5c-fa76f72171f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1094ae7-dfc5-4aba-89de-ad5d28c691d9",
            "compositeImage": {
                "id": "7bc89128-1590-4458-86a3-a9b6a41fb308",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "02c067c5-dc3b-4305-ae5c-fa76f72171f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e286a66-51a4-4ef0-a4dd-4ad7da5e00ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "02c067c5-dc3b-4305-ae5c-fa76f72171f7",
                    "LayerId": "84da49de-7e85-4ada-a609-624dc37c22a2"
                }
            ]
        },
        {
            "id": "b66f9e12-272b-4f5f-a9cc-6340089bc337",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1094ae7-dfc5-4aba-89de-ad5d28c691d9",
            "compositeImage": {
                "id": "6f3d7462-253c-4a5e-a4b8-a9c1b09ad8a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b66f9e12-272b-4f5f-a9cc-6340089bc337",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20477a18-d95b-45fb-a609-4aab4226b244",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b66f9e12-272b-4f5f-a9cc-6340089bc337",
                    "LayerId": "84da49de-7e85-4ada-a609-624dc37c22a2"
                }
            ]
        },
        {
            "id": "ea70e890-4fd4-435b-bd33-79ce9d1f2296",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1094ae7-dfc5-4aba-89de-ad5d28c691d9",
            "compositeImage": {
                "id": "6aa07350-a921-48ac-9728-44dc35ec2ec9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea70e890-4fd4-435b-bd33-79ce9d1f2296",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "439ff129-24a6-48e9-9213-e4d067e16fd3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea70e890-4fd4-435b-bd33-79ce9d1f2296",
                    "LayerId": "84da49de-7e85-4ada-a609-624dc37c22a2"
                }
            ]
        },
        {
            "id": "b7b56b2f-15c2-4e87-86b3-ee6f08661f55",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1094ae7-dfc5-4aba-89de-ad5d28c691d9",
            "compositeImage": {
                "id": "766bf2d7-1f9f-411a-95a2-7e93e49e4661",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7b56b2f-15c2-4e87-86b3-ee6f08661f55",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "74cf99c6-70eb-4a51-b240-5f749f563c7d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7b56b2f-15c2-4e87-86b3-ee6f08661f55",
                    "LayerId": "84da49de-7e85-4ada-a609-624dc37c22a2"
                }
            ]
        },
        {
            "id": "fa0de1d0-c0e0-4f8d-80f2-fc875fb2f7dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1094ae7-dfc5-4aba-89de-ad5d28c691d9",
            "compositeImage": {
                "id": "759877c7-7be2-4adb-81f4-7143782acd84",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa0de1d0-c0e0-4f8d-80f2-fc875fb2f7dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9901e0e2-b2cb-4603-9de5-ea540ba8b775",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa0de1d0-c0e0-4f8d-80f2-fc875fb2f7dd",
                    "LayerId": "84da49de-7e85-4ada-a609-624dc37c22a2"
                }
            ]
        },
        {
            "id": "b1015e15-c875-4e09-bab8-78840ead490f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1094ae7-dfc5-4aba-89de-ad5d28c691d9",
            "compositeImage": {
                "id": "6829b04f-4bb5-42d5-953b-3c47abe0b03c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b1015e15-c875-4e09-bab8-78840ead490f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4e2e82b2-8df5-452f-8863-3b024554c537",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b1015e15-c875-4e09-bab8-78840ead490f",
                    "LayerId": "84da49de-7e85-4ada-a609-624dc37c22a2"
                }
            ]
        },
        {
            "id": "d0a194bd-7b82-42a6-8b9f-02f45295cbf6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1094ae7-dfc5-4aba-89de-ad5d28c691d9",
            "compositeImage": {
                "id": "1532b31e-e750-4d43-95c8-234c53b2faac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d0a194bd-7b82-42a6-8b9f-02f45295cbf6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57e8ef94-7bda-4ccf-bf79-b4fa73efd5db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d0a194bd-7b82-42a6-8b9f-02f45295cbf6",
                    "LayerId": "84da49de-7e85-4ada-a609-624dc37c22a2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "84da49de-7e85-4ada-a609-624dc37c22a2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e1094ae7-dfc5-4aba-89de-ad5d28c691d9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 77,
    "xorig": 30,
    "yorig": 25
}