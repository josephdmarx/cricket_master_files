{
    "id": "88cae362-8372-493e-a06b-9f60f1a4a7cb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGremlin_Hurt",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 12,
    "bbox_right": 47,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "29ad61bd-8945-4065-a5df-93b6bb642a2a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "88cae362-8372-493e-a06b-9f60f1a4a7cb",
            "compositeImage": {
                "id": "432959a1-c7b1-44fe-afb1-414220daa4a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29ad61bd-8945-4065-a5df-93b6bb642a2a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc9c70e7-eda9-4738-9206-87558720236b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29ad61bd-8945-4065-a5df-93b6bb642a2a",
                    "LayerId": "87b08678-81bd-48dd-aa97-1932cc75e566"
                }
            ]
        },
        {
            "id": "11cfc438-a758-453b-9535-77af2c48e462",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "88cae362-8372-493e-a06b-9f60f1a4a7cb",
            "compositeImage": {
                "id": "91acf6e2-5d24-45ca-899d-6e1ae7edb62a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "11cfc438-a758-453b-9535-77af2c48e462",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "399eb55e-f219-485d-9b7f-ff83370b2416",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "11cfc438-a758-453b-9535-77af2c48e462",
                    "LayerId": "87b08678-81bd-48dd-aa97-1932cc75e566"
                }
            ]
        },
        {
            "id": "f76088d2-c5d1-4937-a045-8058d81bce94",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "88cae362-8372-493e-a06b-9f60f1a4a7cb",
            "compositeImage": {
                "id": "c7ad400f-c798-4274-bb83-a3f7d778a5af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f76088d2-c5d1-4937-a045-8058d81bce94",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "77cd5a4f-6b2a-4db5-b8aa-4a10d65ddbe0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f76088d2-c5d1-4937-a045-8058d81bce94",
                    "LayerId": "87b08678-81bd-48dd-aa97-1932cc75e566"
                }
            ]
        },
        {
            "id": "0771e7d3-81f1-4c68-bf0c-9864c52435cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "88cae362-8372-493e-a06b-9f60f1a4a7cb",
            "compositeImage": {
                "id": "95bdc89c-2f61-48de-9c85-667b105c0a35",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0771e7d3-81f1-4c68-bf0c-9864c52435cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9c15d0b-7f5f-4a40-9d3c-e085fb538b03",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0771e7d3-81f1-4c68-bf0c-9864c52435cb",
                    "LayerId": "87b08678-81bd-48dd-aa97-1932cc75e566"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "87b08678-81bd-48dd-aa97-1932cc75e566",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "88cae362-8372-493e-a06b-9f60f1a4a7cb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 77,
    "xorig": 30,
    "yorig": 25
}