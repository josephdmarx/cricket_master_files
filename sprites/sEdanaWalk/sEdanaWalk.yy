{
    "id": "56f553e6-df4f-4f72-a83c-490069b6f1f8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sEdanaWalk",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 110,
    "bbox_left": 45,
    "bbox_right": 79,
    "bbox_top": 23,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c7bf05c9-b209-4746-b0b7-c8fe32e85763",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "56f553e6-df4f-4f72-a83c-490069b6f1f8",
            "compositeImage": {
                "id": "705674ce-0a64-4f44-b650-2c6b780ea7be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7bf05c9-b209-4746-b0b7-c8fe32e85763",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "43199850-f3a0-4f7b-9aa2-ebb0c79ebad3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7bf05c9-b209-4746-b0b7-c8fe32e85763",
                    "LayerId": "d40d8567-0c81-4b5c-8fed-93430e34bfb9"
                }
            ]
        },
        {
            "id": "25b117c8-057d-4ab8-95d8-36efd61e9d75",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "56f553e6-df4f-4f72-a83c-490069b6f1f8",
            "compositeImage": {
                "id": "7b32f18a-d6c3-48d7-89b5-c508bec1dba0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25b117c8-057d-4ab8-95d8-36efd61e9d75",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a82f207-786b-484f-8c86-ea2b43585c20",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25b117c8-057d-4ab8-95d8-36efd61e9d75",
                    "LayerId": "d40d8567-0c81-4b5c-8fed-93430e34bfb9"
                }
            ]
        },
        {
            "id": "b63a1ee9-5625-41a8-b893-1f0be6fd1411",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "56f553e6-df4f-4f72-a83c-490069b6f1f8",
            "compositeImage": {
                "id": "b5b21816-c512-47c4-9473-b9127b43500a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b63a1ee9-5625-41a8-b893-1f0be6fd1411",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71fc8365-888b-44f8-925b-5d8bf9cb3284",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b63a1ee9-5625-41a8-b893-1f0be6fd1411",
                    "LayerId": "d40d8567-0c81-4b5c-8fed-93430e34bfb9"
                }
            ]
        },
        {
            "id": "6748a7cd-d493-4656-b85c-3795bb37b13b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "56f553e6-df4f-4f72-a83c-490069b6f1f8",
            "compositeImage": {
                "id": "83aaf696-8567-4acb-a889-a27d2964ea90",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6748a7cd-d493-4656-b85c-3795bb37b13b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd9ce984-2c59-4235-9051-dc4efb64719a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6748a7cd-d493-4656-b85c-3795bb37b13b",
                    "LayerId": "d40d8567-0c81-4b5c-8fed-93430e34bfb9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "d40d8567-0c81-4b5c-8fed-93430e34bfb9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "56f553e6-df4f-4f72-a83c-490069b6f1f8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}