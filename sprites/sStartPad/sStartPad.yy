{
    "id": "2b8ff957-1ad0-48ce-9b38-1d138aeff0a7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sStartPad",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 6,
    "bbox_right": 25,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c9b50a37-9791-4529-864f-96af75b09a02",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2b8ff957-1ad0-48ce-9b38-1d138aeff0a7",
            "compositeImage": {
                "id": "1d4120a1-d54d-4def-bf3e-80a4e6a97dfd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9b50a37-9791-4529-864f-96af75b09a02",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "94d84ace-a265-4bcb-aa61-98388580e59e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9b50a37-9791-4529-864f-96af75b09a02",
                    "LayerId": "d6705dd0-6c2e-40a0-adf6-919c960a6e87"
                }
            ]
        },
        {
            "id": "31f81d88-de12-4c01-b447-fb5e43244455",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2b8ff957-1ad0-48ce-9b38-1d138aeff0a7",
            "compositeImage": {
                "id": "c76e5bd5-b081-4dc9-9ed0-4d8f35195b24",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31f81d88-de12-4c01-b447-fb5e43244455",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4bc0a9bf-8aff-4edb-b2cf-834ca3ea98bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31f81d88-de12-4c01-b447-fb5e43244455",
                    "LayerId": "d6705dd0-6c2e-40a0-adf6-919c960a6e87"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "d6705dd0-6c2e-40a0-adf6-919c960a6e87",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2b8ff957-1ad0-48ce-9b38-1d138aeff0a7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 15,
    "yorig": 25
}