{
    "id": "95872664-1a35-46dd-a71f-19005c92d2e4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sShopMenu",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 191,
    "bbox_left": 0,
    "bbox_right": 319,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b3b159bf-40ff-488e-a432-c2840b3ce07b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95872664-1a35-46dd-a71f-19005c92d2e4",
            "compositeImage": {
                "id": "7446abe0-829f-423e-a9c6-b04f888b477e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b3b159bf-40ff-488e-a432-c2840b3ce07b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fcee9ff4-910b-4562-97cf-45aa0b3e22ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3b159bf-40ff-488e-a432-c2840b3ce07b",
                    "LayerId": "b215d960-d8ce-4dbf-ae82-c8584b819731"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 192,
    "layers": [
        {
            "id": "b215d960-d8ce-4dbf-ae82-c8584b819731",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "95872664-1a35-46dd-a71f-19005c92d2e4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 320,
    "xorig": 0,
    "yorig": 0
}