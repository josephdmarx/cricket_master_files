{
    "id": "29d362df-98e6-4732-8705-588429403239",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCaveBreakWallLeft",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 16,
    "bbox_right": 47,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3361d7e5-2a2c-4a5f-ad81-b26f9c43abe0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29d362df-98e6-4732-8705-588429403239",
            "compositeImage": {
                "id": "b0249388-f545-466c-82c3-25615e4ec368",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3361d7e5-2a2c-4a5f-ad81-b26f9c43abe0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b00e6f9e-ae47-4390-81e9-eeaf697c9562",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3361d7e5-2a2c-4a5f-ad81-b26f9c43abe0",
                    "LayerId": "6f5f5234-1944-48dc-886c-0d365a1a800a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "6f5f5234-1944-48dc-886c-0d365a1a800a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "29d362df-98e6-4732-8705-588429403239",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 16,
    "yorig": 127
}