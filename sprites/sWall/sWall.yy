{
    "id": "0ea93cc1-617d-462e-b090-d305a68082dc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sWall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6156e20e-26a9-4817-9cc7-2dded06929f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0ea93cc1-617d-462e-b090-d305a68082dc",
            "compositeImage": {
                "id": "0800fd60-2c31-4067-bac8-d6d3be99828b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6156e20e-26a9-4817-9cc7-2dded06929f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe866d08-5437-4558-946f-0bcdafd723b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6156e20e-26a9-4817-9cc7-2dded06929f8",
                    "LayerId": "aec07b34-a08b-48a8-a7ad-29d8c490a433"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "aec07b34-a08b-48a8-a7ad-29d8c490a433",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0ea93cc1-617d-462e-b090-d305a68082dc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 3,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 16
}