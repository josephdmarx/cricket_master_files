{
    "id": "fcfd09c1-3ced-449d-980c-f4553d98a0c0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sInventoryLarge2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 269,
    "bbox_left": 1,
    "bbox_right": 438,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b5593f8b-485d-43b2-96c9-f5ee99956033",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fcfd09c1-3ced-449d-980c-f4553d98a0c0",
            "compositeImage": {
                "id": "b68883c8-dc01-49b9-9c1d-84f7ef239d14",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5593f8b-485d-43b2-96c9-f5ee99956033",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "16070d00-788f-4644-ba99-88af5ee81331",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5593f8b-485d-43b2-96c9-f5ee99956033",
                    "LayerId": "ee1cd97a-a42c-40b9-95ea-9aaed80c214a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 300,
    "layers": [
        {
            "id": "ee1cd97a-a42c-40b9-95ea-9aaed80c214a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fcfd09c1-3ced-449d-980c-f4553d98a0c0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 439,
    "xorig": 1,
    "yorig": 1
}