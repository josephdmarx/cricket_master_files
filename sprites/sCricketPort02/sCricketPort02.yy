{
    "id": "b4c53937-c291-4c72-8303-3d6d8dddd58d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCricketPort02",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 3,
    "bbox_right": 89,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": true,
    "frames": [
        {
            "id": "0b8857b3-81d9-4935-b4d7-3843fa28f772",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b4c53937-c291-4c72-8303-3d6d8dddd58d",
            "compositeImage": {
                "id": "5084212b-fce1-41d6-85b8-7f34f6c66553",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b8857b3-81d9-4935-b4d7-3843fa28f772",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7a065a1-56d8-4348-812f-97e94d11995b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b8857b3-81d9-4935-b4d7-3843fa28f772",
                    "LayerId": "1c5d32c0-b835-421e-a6af-02f88e64dbf2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "1c5d32c0-b835-421e-a6af-02f88e64dbf2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b4c53937-c291-4c72-8303-3d6d8dddd58d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": true,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 0,
    "yorig": 0
}