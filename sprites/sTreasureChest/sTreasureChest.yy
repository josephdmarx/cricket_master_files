{
    "id": "fc99f2f2-ed73-416c-98fc-fc240d40f017",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTreasureChest",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 54,
    "bbox_left": 11,
    "bbox_right": 58,
    "bbox_top": 17,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "defcf790-2405-4ec4-b190-94d736ba666a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fc99f2f2-ed73-416c-98fc-fc240d40f017",
            "compositeImage": {
                "id": "ff74447b-52cd-41b6-bc59-0f8c0adb7387",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "defcf790-2405-4ec4-b190-94d736ba666a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a105323-a984-4cdd-9d27-ea99e9d5a420",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "defcf790-2405-4ec4-b190-94d736ba666a",
                    "LayerId": "0bb1ce7d-e8e1-4fe2-930e-533db5e83a7b"
                }
            ]
        },
        {
            "id": "345425d8-e2e7-427f-b264-473f92ef839d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fc99f2f2-ed73-416c-98fc-fc240d40f017",
            "compositeImage": {
                "id": "fa97a504-c72b-4126-a7ea-6a2f01e2e226",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "345425d8-e2e7-427f-b264-473f92ef839d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a30dfa2-8da8-4eed-921b-3389a2c2e809",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "345425d8-e2e7-427f-b264-473f92ef839d",
                    "LayerId": "0bb1ce7d-e8e1-4fe2-930e-533db5e83a7b"
                }
            ]
        },
        {
            "id": "673d01e9-065e-4429-a149-caf4e7c8bb11",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fc99f2f2-ed73-416c-98fc-fc240d40f017",
            "compositeImage": {
                "id": "ea2b6c6a-10ea-4f05-b846-ca5983298cd7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "673d01e9-065e-4429-a149-caf4e7c8bb11",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d22a453f-a5f0-4444-9c35-3ba71fba08f3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "673d01e9-065e-4429-a149-caf4e7c8bb11",
                    "LayerId": "0bb1ce7d-e8e1-4fe2-930e-533db5e83a7b"
                }
            ]
        },
        {
            "id": "65cb066e-ade0-4a0c-b2e5-93c4131f5c1f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fc99f2f2-ed73-416c-98fc-fc240d40f017",
            "compositeImage": {
                "id": "4e6164a4-08c3-4a5d-a3f8-4fa0a220cdd6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65cb066e-ade0-4a0c-b2e5-93c4131f5c1f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c271c3ea-4ad7-438f-a34f-6dac5150227b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65cb066e-ade0-4a0c-b2e5-93c4131f5c1f",
                    "LayerId": "0bb1ce7d-e8e1-4fe2-930e-533db5e83a7b"
                }
            ]
        },
        {
            "id": "70b93fb0-a57f-4c37-b10c-841e2d13f494",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fc99f2f2-ed73-416c-98fc-fc240d40f017",
            "compositeImage": {
                "id": "1de7f5dd-4b0a-4457-9569-e2f0621c2e92",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "70b93fb0-a57f-4c37-b10c-841e2d13f494",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b684d436-b4cd-4e75-aabb-fed25b19ed5d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "70b93fb0-a57f-4c37-b10c-841e2d13f494",
                    "LayerId": "0bb1ce7d-e8e1-4fe2-930e-533db5e83a7b"
                }
            ]
        },
        {
            "id": "4db8e0eb-5fd0-4e3f-ac44-ae6dc686f905",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fc99f2f2-ed73-416c-98fc-fc240d40f017",
            "compositeImage": {
                "id": "40756f56-9e66-416e-a493-74394a098aec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4db8e0eb-5fd0-4e3f-ac44-ae6dc686f905",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a5299c03-10ae-41c9-9d81-d681346013de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4db8e0eb-5fd0-4e3f-ac44-ae6dc686f905",
                    "LayerId": "0bb1ce7d-e8e1-4fe2-930e-533db5e83a7b"
                }
            ]
        },
        {
            "id": "c078e195-730b-45f0-8e68-03c6d70ffeef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fc99f2f2-ed73-416c-98fc-fc240d40f017",
            "compositeImage": {
                "id": "56468c01-179f-4985-9857-2497685632cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c078e195-730b-45f0-8e68-03c6d70ffeef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ddac354e-a2a0-466c-9eed-67b326725752",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c078e195-730b-45f0-8e68-03c6d70ffeef",
                    "LayerId": "0bb1ce7d-e8e1-4fe2-930e-533db5e83a7b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "0bb1ce7d-e8e1-4fe2-930e-533db5e83a7b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fc99f2f2-ed73-416c-98fc-fc240d40f017",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 30,
    "yorig": 54
}