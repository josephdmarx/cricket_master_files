{
    "id": "5443204f-2b63-4811-bcd3-2c1d330c3da9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sEdanaIdle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 110,
    "bbox_left": 45,
    "bbox_right": 79,
    "bbox_top": 23,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "940798f0-d1a7-4206-8a23-a2715141223a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5443204f-2b63-4811-bcd3-2c1d330c3da9",
            "compositeImage": {
                "id": "b62dc646-2d26-4d5c-94e2-ba38f7f93cc4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "940798f0-d1a7-4206-8a23-a2715141223a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95e45380-ecef-444d-83b7-a5e9c788c1af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "940798f0-d1a7-4206-8a23-a2715141223a",
                    "LayerId": "3211a92e-8386-4e57-9931-12e4e75429e7"
                }
            ]
        },
        {
            "id": "088c504e-0f40-4be4-8de6-883188a67ec0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5443204f-2b63-4811-bcd3-2c1d330c3da9",
            "compositeImage": {
                "id": "e7373aec-fc68-4342-b504-2b10e94481b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "088c504e-0f40-4be4-8de6-883188a67ec0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "257e8af0-77e0-40c9-952c-be04c2c1fa91",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "088c504e-0f40-4be4-8de6-883188a67ec0",
                    "LayerId": "3211a92e-8386-4e57-9931-12e4e75429e7"
                }
            ]
        },
        {
            "id": "e87761b7-3d50-4cf0-9ca1-e89c5d94d8ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5443204f-2b63-4811-bcd3-2c1d330c3da9",
            "compositeImage": {
                "id": "2eb1a97b-22c7-49ab-96ab-2d68724a986e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e87761b7-3d50-4cf0-9ca1-e89c5d94d8ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3a4b86e-324f-4190-9fa6-84d4f7961ebd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e87761b7-3d50-4cf0-9ca1-e89c5d94d8ee",
                    "LayerId": "3211a92e-8386-4e57-9931-12e4e75429e7"
                }
            ]
        },
        {
            "id": "0316ba82-0ec4-4214-94fb-22291faabc8a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5443204f-2b63-4811-bcd3-2c1d330c3da9",
            "compositeImage": {
                "id": "d4736c44-27ec-42c3-bd64-9069daf1dc7d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0316ba82-0ec4-4214-94fb-22291faabc8a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "922b2c68-9524-4365-a72a-3d79fea8312b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0316ba82-0ec4-4214-94fb-22291faabc8a",
                    "LayerId": "3211a92e-8386-4e57-9931-12e4e75429e7"
                }
            ]
        },
        {
            "id": "55fdca67-4df2-40ff-b205-17088283b690",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5443204f-2b63-4811-bcd3-2c1d330c3da9",
            "compositeImage": {
                "id": "26d7ecc0-d4c2-4b14-9a67-c84822ba1442",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55fdca67-4df2-40ff-b205-17088283b690",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9ee3acf-cff2-45bf-be19-a126ff500ffb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55fdca67-4df2-40ff-b205-17088283b690",
                    "LayerId": "3211a92e-8386-4e57-9931-12e4e75429e7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "3211a92e-8386-4e57-9931-12e4e75429e7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5443204f-2b63-4811-bcd3-2c1d330c3da9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}