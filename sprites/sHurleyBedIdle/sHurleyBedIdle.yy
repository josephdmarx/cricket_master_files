{
    "id": "58bb1187-67c8-4547-bac3-202ffd520e89",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sHurleyBedIdle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 57,
    "bbox_left": 10,
    "bbox_right": 96,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f5f01b88-53bc-4fee-8c98-067bdf5c608f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58bb1187-67c8-4547-bac3-202ffd520e89",
            "compositeImage": {
                "id": "d293e7cb-99be-4006-a936-86ef49939cf9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5f01b88-53bc-4fee-8c98-067bdf5c608f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11416fcd-24c7-41e8-8dd8-4b0124519042",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5f01b88-53bc-4fee-8c98-067bdf5c608f",
                    "LayerId": "5dec5b5e-e762-40b0-9734-61f38b97c4ac"
                }
            ]
        },
        {
            "id": "63de641a-a94b-40b2-ad7b-6126ce62f8c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58bb1187-67c8-4547-bac3-202ffd520e89",
            "compositeImage": {
                "id": "93753c9e-2c1f-49d5-9a05-5b4f257a5d9d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63de641a-a94b-40b2-ad7b-6126ce62f8c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3bcd380-8cc3-4d44-b999-261b337005e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63de641a-a94b-40b2-ad7b-6126ce62f8c0",
                    "LayerId": "5dec5b5e-e762-40b0-9734-61f38b97c4ac"
                }
            ]
        },
        {
            "id": "2853001f-82fa-48bf-966d-9a630a61cb51",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58bb1187-67c8-4547-bac3-202ffd520e89",
            "compositeImage": {
                "id": "33931ec8-8417-419a-8268-7d168dc3dfd5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2853001f-82fa-48bf-966d-9a630a61cb51",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4430265a-03ef-4a85-a979-2c5f735424ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2853001f-82fa-48bf-966d-9a630a61cb51",
                    "LayerId": "5dec5b5e-e762-40b0-9734-61f38b97c4ac"
                }
            ]
        },
        {
            "id": "4bc6328b-cb45-4581-b39a-d489d0ada12c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58bb1187-67c8-4547-bac3-202ffd520e89",
            "compositeImage": {
                "id": "ffe831c8-1535-41ab-9546-68236185e3a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4bc6328b-cb45-4581-b39a-d489d0ada12c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9cc0dd69-3a93-4c9e-98df-5b77383aa2e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4bc6328b-cb45-4581-b39a-d489d0ada12c",
                    "LayerId": "5dec5b5e-e762-40b0-9734-61f38b97c4ac"
                }
            ]
        },
        {
            "id": "09b6672e-ae70-4f99-afa4-288f737ea819",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58bb1187-67c8-4547-bac3-202ffd520e89",
            "compositeImage": {
                "id": "3b6ce403-ef8a-4748-80ab-63c06f3b7871",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09b6672e-ae70-4f99-afa4-288f737ea819",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "41107ed2-d763-44ad-ada4-219c53cdd7de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09b6672e-ae70-4f99-afa4-288f737ea819",
                    "LayerId": "5dec5b5e-e762-40b0-9734-61f38b97c4ac"
                }
            ]
        },
        {
            "id": "04caffed-cb56-416c-bbca-050d456624a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58bb1187-67c8-4547-bac3-202ffd520e89",
            "compositeImage": {
                "id": "d05075e9-995d-476a-bdac-07ce9b51fb68",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "04caffed-cb56-416c-bbca-050d456624a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a2421b65-9e3f-44b4-adc1-df56f1bcbdf1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "04caffed-cb56-416c-bbca-050d456624a2",
                    "LayerId": "5dec5b5e-e762-40b0-9734-61f38b97c4ac"
                }
            ]
        },
        {
            "id": "21fedd39-f09c-4350-916e-1f93880dc67b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58bb1187-67c8-4547-bac3-202ffd520e89",
            "compositeImage": {
                "id": "a7f4e9d6-6066-4608-88ec-20170ba4e73c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21fedd39-f09c-4350-916e-1f93880dc67b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84f96a0e-2e20-4c5e-8210-1e2c26a67acf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21fedd39-f09c-4350-916e-1f93880dc67b",
                    "LayerId": "5dec5b5e-e762-40b0-9734-61f38b97c4ac"
                }
            ]
        },
        {
            "id": "e0425ff6-98c3-418a-9372-9dd252b6277a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58bb1187-67c8-4547-bac3-202ffd520e89",
            "compositeImage": {
                "id": "dd48dd30-1462-4f68-800d-ea581d0da468",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e0425ff6-98c3-418a-9372-9dd252b6277a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dfe499e9-e2d2-477e-9ab9-9431f5a58cbe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0425ff6-98c3-418a-9372-9dd252b6277a",
                    "LayerId": "5dec5b5e-e762-40b0-9734-61f38b97c4ac"
                }
            ]
        },
        {
            "id": "89b75e97-db26-46c1-8cd2-fc88524f574f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58bb1187-67c8-4547-bac3-202ffd520e89",
            "compositeImage": {
                "id": "5355d90e-cdf2-45ce-bb54-a4501c2c6eb2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89b75e97-db26-46c1-8cd2-fc88524f574f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "75d640b7-babb-48aa-b733-8e075b482711",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89b75e97-db26-46c1-8cd2-fc88524f574f",
                    "LayerId": "5dec5b5e-e762-40b0-9734-61f38b97c4ac"
                }
            ]
        },
        {
            "id": "1da057de-ee67-4fcc-ae87-2620ed922930",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58bb1187-67c8-4547-bac3-202ffd520e89",
            "compositeImage": {
                "id": "bbee9be5-6e52-495f-93ad-7bfea21f375d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1da057de-ee67-4fcc-ae87-2620ed922930",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c8b4a49-7fe0-43d5-b6ca-80928a314462",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1da057de-ee67-4fcc-ae87-2620ed922930",
                    "LayerId": "5dec5b5e-e762-40b0-9734-61f38b97c4ac"
                }
            ]
        },
        {
            "id": "9042ebaf-f90b-414b-853d-7ed309238f8d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58bb1187-67c8-4547-bac3-202ffd520e89",
            "compositeImage": {
                "id": "abe07fe2-5093-4f6a-be95-ba53067d9076",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9042ebaf-f90b-414b-853d-7ed309238f8d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33afb8d0-c75b-4d78-8d26-9a17e4bec3ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9042ebaf-f90b-414b-853d-7ed309238f8d",
                    "LayerId": "5dec5b5e-e762-40b0-9734-61f38b97c4ac"
                }
            ]
        },
        {
            "id": "378e792b-9c1a-46b8-a54b-32228c665b58",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58bb1187-67c8-4547-bac3-202ffd520e89",
            "compositeImage": {
                "id": "42e0b151-4d2b-4dbb-87b7-6c85d1e7794e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "378e792b-9c1a-46b8-a54b-32228c665b58",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c748abd-4e97-4b0f-91c6-40a74e90c60a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "378e792b-9c1a-46b8-a54b-32228c665b58",
                    "LayerId": "5dec5b5e-e762-40b0-9734-61f38b97c4ac"
                }
            ]
        },
        {
            "id": "a17ac3f0-b692-4f39-8d9a-c6f173051a70",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58bb1187-67c8-4547-bac3-202ffd520e89",
            "compositeImage": {
                "id": "1925ac34-c3f0-4445-b965-d8f7afc66907",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a17ac3f0-b692-4f39-8d9a-c6f173051a70",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1bdffbaf-6f59-4fa1-afc7-d59f5bc02b7f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a17ac3f0-b692-4f39-8d9a-c6f173051a70",
                    "LayerId": "5dec5b5e-e762-40b0-9734-61f38b97c4ac"
                }
            ]
        },
        {
            "id": "70b70c42-e3c9-437d-a215-f7f6d065f8f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58bb1187-67c8-4547-bac3-202ffd520e89",
            "compositeImage": {
                "id": "1e8e1b85-4f92-48b3-b277-0e6f78081879",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "70b70c42-e3c9-437d-a215-f7f6d065f8f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "28bf4c7e-7d85-4fd8-9973-b624bea9cff2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "70b70c42-e3c9-437d-a215-f7f6d065f8f3",
                    "LayerId": "5dec5b5e-e762-40b0-9734-61f38b97c4ac"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 65,
    "layers": [
        {
            "id": "5dec5b5e-e762-40b0-9734-61f38b97c4ac",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "58bb1187-67c8-4547-bac3-202ffd520e89",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 108,
    "xorig": 10,
    "yorig": 57
}