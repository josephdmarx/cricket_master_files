{
    "id": "c8fcc3c9-b4e9-4c05-8e62-3c584144e36c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sDissolve1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b6d2a66f-5cf5-484b-ab13-eb3e7e5301e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c8fcc3c9-b4e9-4c05-8e62-3c584144e36c",
            "compositeImage": {
                "id": "bfdd62f4-5110-4f93-a226-ddf2d8ec7e2e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6d2a66f-5cf5-484b-ab13-eb3e7e5301e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82087bb7-9e91-4c0a-ba33-3840d8b1e84f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6d2a66f-5cf5-484b-ab13-eb3e7e5301e1",
                    "LayerId": "7bea5ff9-6e61-4269-98c1-608eaabcdbf4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "7bea5ff9-6e61-4269-98c1-608eaabcdbf4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c8fcc3c9-b4e9-4c05-8e62-3c584144e36c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 153,
    "yorig": 27
}