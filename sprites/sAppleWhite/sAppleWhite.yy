{
    "id": "72ff1bb3-262b-468c-a18d-3b192c725f91",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sAppleWhite",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 9,
    "bbox_right": 22,
    "bbox_top": 9,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c4a13eb4-ae5a-4398-953c-19da3ef616ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72ff1bb3-262b-468c-a18d-3b192c725f91",
            "compositeImage": {
                "id": "cb244c4c-520c-4ebf-941e-4d7b92fc9f6a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c4a13eb4-ae5a-4398-953c-19da3ef616ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "063f5d82-9262-4df9-8b25-a97a083ccb5d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c4a13eb4-ae5a-4398-953c-19da3ef616ef",
                    "LayerId": "e1a695a0-aa7f-4c48-ae85-bd2f11daac63"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "e1a695a0-aa7f-4c48-ae85-bd2f11daac63",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "72ff1bb3-262b-468c-a18d-3b192c725f91",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}