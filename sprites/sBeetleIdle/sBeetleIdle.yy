{
    "id": "422c1b90-ca1d-496d-be01-01fc4970193d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBeetleIdle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 49,
    "bbox_right": 77,
    "bbox_top": 35,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b92948bb-17cb-439e-a659-d9ee91622890",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "422c1b90-ca1d-496d-be01-01fc4970193d",
            "compositeImage": {
                "id": "d08720eb-110f-4e72-a578-7e6de2b30315",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b92948bb-17cb-439e-a659-d9ee91622890",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "888f1a67-858c-4456-ab72-56d6e96eb000",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b92948bb-17cb-439e-a659-d9ee91622890",
                    "LayerId": "3ecc86d7-5f41-4e1f-9be0-50428c7c297f"
                }
            ]
        },
        {
            "id": "ecc05be7-0578-4a26-8275-243ffce2a771",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "422c1b90-ca1d-496d-be01-01fc4970193d",
            "compositeImage": {
                "id": "cbdd6e49-8330-4b9d-8eb7-4827e369fc24",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ecc05be7-0578-4a26-8275-243ffce2a771",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3dd64814-62cb-463f-9f26-fcf9d8877d1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ecc05be7-0578-4a26-8275-243ffce2a771",
                    "LayerId": "3ecc86d7-5f41-4e1f-9be0-50428c7c297f"
                }
            ]
        },
        {
            "id": "94bbdce4-457b-4b1b-801c-4e8a03560512",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "422c1b90-ca1d-496d-be01-01fc4970193d",
            "compositeImage": {
                "id": "39d6e7c4-3534-4e33-a8b6-e140d4a88f4f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "94bbdce4-457b-4b1b-801c-4e8a03560512",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c506aa64-fc2c-4e20-a59a-52ee960d4ec1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "94bbdce4-457b-4b1b-801c-4e8a03560512",
                    "LayerId": "3ecc86d7-5f41-4e1f-9be0-50428c7c297f"
                }
            ]
        },
        {
            "id": "b5d86a90-bab6-43e8-b3fd-7998f4082364",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "422c1b90-ca1d-496d-be01-01fc4970193d",
            "compositeImage": {
                "id": "69846c68-e054-4ff4-93eb-c542b787d9a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5d86a90-bab6-43e8-b3fd-7998f4082364",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b774703-e3e5-4273-92e8-903867aacf39",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5d86a90-bab6-43e8-b3fd-7998f4082364",
                    "LayerId": "3ecc86d7-5f41-4e1f-9be0-50428c7c297f"
                }
            ]
        },
        {
            "id": "bd024d33-d271-42c3-9c08-f0649adc63ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "422c1b90-ca1d-496d-be01-01fc4970193d",
            "compositeImage": {
                "id": "a1cb126f-bf55-474d-8377-3bae9b41e05d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd024d33-d271-42c3-9c08-f0649adc63ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eec11919-e668-44f9-860e-e2d2341bbf6a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd024d33-d271-42c3-9c08-f0649adc63ed",
                    "LayerId": "3ecc86d7-5f41-4e1f-9be0-50428c7c297f"
                }
            ]
        },
        {
            "id": "bf89eac9-b206-4623-b33b-065ae7a51db5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "422c1b90-ca1d-496d-be01-01fc4970193d",
            "compositeImage": {
                "id": "d1251e71-e9f0-47a8-ae22-728458ccfafb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf89eac9-b206-4623-b33b-065ae7a51db5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b4f74e95-619c-475b-9289-1ce2aeb899e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf89eac9-b206-4623-b33b-065ae7a51db5",
                    "LayerId": "3ecc86d7-5f41-4e1f-9be0-50428c7c297f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "3ecc86d7-5f41-4e1f-9be0-50428c7c297f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "422c1b90-ca1d-496d-be01-01fc4970193d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}