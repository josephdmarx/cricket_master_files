{
    "id": "0c607c75-d122-40d6-b6ba-ec8738d2827c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayerA",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 94,
    "bbox_left": 58,
    "bbox_right": 72,
    "bbox_top": 42,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f87d18ba-5cf6-4eea-8c65-dc6ab5d9fad8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0c607c75-d122-40d6-b6ba-ec8738d2827c",
            "compositeImage": {
                "id": "4110d186-8184-413e-acb1-f9a4b21cdac2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f87d18ba-5cf6-4eea-8c65-dc6ab5d9fad8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bbaee661-2161-488a-82e1-8def8ebf71a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f87d18ba-5cf6-4eea-8c65-dc6ab5d9fad8",
                    "LayerId": "d499b478-d5c9-41be-90ff-5511f89ca922"
                }
            ]
        },
        {
            "id": "d905107f-e90f-45a8-9e4d-e868176f9fba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0c607c75-d122-40d6-b6ba-ec8738d2827c",
            "compositeImage": {
                "id": "2f4737b5-5a8d-4f94-bcc6-357bd2f23433",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d905107f-e90f-45a8-9e4d-e868176f9fba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "107415e4-c134-4339-9243-1a0951574bfc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d905107f-e90f-45a8-9e4d-e868176f9fba",
                    "LayerId": "d499b478-d5c9-41be-90ff-5511f89ca922"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "d499b478-d5c9-41be-90ff-5511f89ca922",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0c607c75-d122-40d6-b6ba-ec8738d2827c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}