{
    "id": "d06561d3-3f59-491e-9c58-22ec7940847e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sMushman_Wiggle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 94,
    "bbox_left": 43,
    "bbox_right": 83,
    "bbox_top": 50,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b8f6db91-510e-4b28-a43f-af599c75cfd5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d06561d3-3f59-491e-9c58-22ec7940847e",
            "compositeImage": {
                "id": "4d8e9416-925f-4185-ae84-02a7e47978a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b8f6db91-510e-4b28-a43f-af599c75cfd5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c6dbed5-6922-4fca-962f-2b8284ba69a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b8f6db91-510e-4b28-a43f-af599c75cfd5",
                    "LayerId": "0a25632a-cdc8-4103-af73-fc5008cffd39"
                }
            ]
        },
        {
            "id": "22ffa044-534a-4104-9d49-ddc0a9901ca9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d06561d3-3f59-491e-9c58-22ec7940847e",
            "compositeImage": {
                "id": "6b1757e6-cf97-4f1c-b65a-6992ccd59d76",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22ffa044-534a-4104-9d49-ddc0a9901ca9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c02e598a-7450-401b-8b87-f404a416365e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22ffa044-534a-4104-9d49-ddc0a9901ca9",
                    "LayerId": "0a25632a-cdc8-4103-af73-fc5008cffd39"
                }
            ]
        },
        {
            "id": "e6b0ea05-761c-4da0-a778-391f3e7a8d18",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d06561d3-3f59-491e-9c58-22ec7940847e",
            "compositeImage": {
                "id": "a1c4d96d-c850-49f1-9777-6dcb247b6644",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6b0ea05-761c-4da0-a778-391f3e7a8d18",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "59ff43f8-f92e-4eec-a40c-147872f5ee03",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6b0ea05-761c-4da0-a778-391f3e7a8d18",
                    "LayerId": "0a25632a-cdc8-4103-af73-fc5008cffd39"
                }
            ]
        },
        {
            "id": "7089cd1e-6556-4ba6-8500-960e5c550e11",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d06561d3-3f59-491e-9c58-22ec7940847e",
            "compositeImage": {
                "id": "8a70920f-84bc-4a3e-8941-1fa730804265",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7089cd1e-6556-4ba6-8500-960e5c550e11",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32ff41f2-cf7f-4f05-b51f-211e23e9fc38",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7089cd1e-6556-4ba6-8500-960e5c550e11",
                    "LayerId": "0a25632a-cdc8-4103-af73-fc5008cffd39"
                }
            ]
        },
        {
            "id": "6b2e6646-7764-4a39-aaa0-6877a5d08ee0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d06561d3-3f59-491e-9c58-22ec7940847e",
            "compositeImage": {
                "id": "d9432d83-1f1a-407d-ade0-9cdea2cb061f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b2e6646-7764-4a39-aaa0-6877a5d08ee0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b4064a30-3134-4e5b-a183-1c10bcca5cfa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b2e6646-7764-4a39-aaa0-6877a5d08ee0",
                    "LayerId": "0a25632a-cdc8-4103-af73-fc5008cffd39"
                }
            ]
        },
        {
            "id": "368b85be-44f4-43f4-ae32-fbd580453651",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d06561d3-3f59-491e-9c58-22ec7940847e",
            "compositeImage": {
                "id": "5de19601-3a1b-479f-a417-5edc04757209",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "368b85be-44f4-43f4-ae32-fbd580453651",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce448bca-7944-4bc4-8eaa-027b40bbe369",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "368b85be-44f4-43f4-ae32-fbd580453651",
                    "LayerId": "0a25632a-cdc8-4103-af73-fc5008cffd39"
                }
            ]
        },
        {
            "id": "522b7ce2-f0ca-499e-a27c-be10f0331bb5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d06561d3-3f59-491e-9c58-22ec7940847e",
            "compositeImage": {
                "id": "aa0fdc59-a66c-4827-abe0-f47b372ec96e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "522b7ce2-f0ca-499e-a27c-be10f0331bb5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "17868917-7411-43a5-9f37-c5f9e1ed268b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "522b7ce2-f0ca-499e-a27c-be10f0331bb5",
                    "LayerId": "0a25632a-cdc8-4103-af73-fc5008cffd39"
                }
            ]
        },
        {
            "id": "09833ada-8444-4200-b773-d9a4fe85db26",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d06561d3-3f59-491e-9c58-22ec7940847e",
            "compositeImage": {
                "id": "7b01633a-8bd2-4e76-8f1b-d0a9a62b181d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09833ada-8444-4200-b773-d9a4fe85db26",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "74d497ce-c0dd-4b84-a31f-09fa78602ce6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09833ada-8444-4200-b773-d9a4fe85db26",
                    "LayerId": "0a25632a-cdc8-4103-af73-fc5008cffd39"
                }
            ]
        },
        {
            "id": "a90b1153-8423-4826-ae61-516ea935190a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d06561d3-3f59-491e-9c58-22ec7940847e",
            "compositeImage": {
                "id": "587b1792-17ac-41b1-a039-a76c66da27c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a90b1153-8423-4826-ae61-516ea935190a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "123927ca-3568-4a8b-8627-aa9e3b8e3766",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a90b1153-8423-4826-ae61-516ea935190a",
                    "LayerId": "0a25632a-cdc8-4103-af73-fc5008cffd39"
                }
            ]
        },
        {
            "id": "20fd709a-43d6-48cf-8420-de98b6a00b31",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d06561d3-3f59-491e-9c58-22ec7940847e",
            "compositeImage": {
                "id": "7856d5af-647a-48f1-be47-77e1c91fcd3d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "20fd709a-43d6-48cf-8420-de98b6a00b31",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09ed217a-0743-4a9f-8172-5206b17b502e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "20fd709a-43d6-48cf-8420-de98b6a00b31",
                    "LayerId": "0a25632a-cdc8-4103-af73-fc5008cffd39"
                }
            ]
        },
        {
            "id": "102010f7-0378-463b-af25-39fb77217858",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d06561d3-3f59-491e-9c58-22ec7940847e",
            "compositeImage": {
                "id": "825b9a5e-efed-45a7-877d-bb1fa918be61",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "102010f7-0378-463b-af25-39fb77217858",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b26b3f09-54cf-4992-8491-edeabddaaebf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "102010f7-0378-463b-af25-39fb77217858",
                    "LayerId": "0a25632a-cdc8-4103-af73-fc5008cffd39"
                }
            ]
        },
        {
            "id": "5da3dbc9-30cf-4817-8cfc-588b5ee677b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d06561d3-3f59-491e-9c58-22ec7940847e",
            "compositeImage": {
                "id": "38147d0a-8fe8-42a8-966a-cf75b683a5fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5da3dbc9-30cf-4817-8cfc-588b5ee677b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "343be8fb-16a0-4092-8b29-6addf1e84386",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5da3dbc9-30cf-4817-8cfc-588b5ee677b6",
                    "LayerId": "0a25632a-cdc8-4103-af73-fc5008cffd39"
                }
            ]
        },
        {
            "id": "4c7959ec-c811-4a01-b472-f14c3dd6e181",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d06561d3-3f59-491e-9c58-22ec7940847e",
            "compositeImage": {
                "id": "896b5b16-a357-4b30-9165-a6e574bce20e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c7959ec-c811-4a01-b472-f14c3dd6e181",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04914ce5-696e-452d-955c-98b895b74581",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c7959ec-c811-4a01-b472-f14c3dd6e181",
                    "LayerId": "0a25632a-cdc8-4103-af73-fc5008cffd39"
                }
            ]
        },
        {
            "id": "04de8fc0-3994-42a4-9c25-f69339b01b0e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d06561d3-3f59-491e-9c58-22ec7940847e",
            "compositeImage": {
                "id": "8be5a0b9-db6e-4f7a-b657-7cc7ad6313fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "04de8fc0-3994-42a4-9c25-f69339b01b0e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee6ca9c9-7fd3-4c0e-a939-dabd3a3f21f3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "04de8fc0-3994-42a4-9c25-f69339b01b0e",
                    "LayerId": "0a25632a-cdc8-4103-af73-fc5008cffd39"
                }
            ]
        },
        {
            "id": "6b07e195-c664-4726-be04-bc75a07e523c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d06561d3-3f59-491e-9c58-22ec7940847e",
            "compositeImage": {
                "id": "a1139c78-ff90-422b-8d70-3786d68f401f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b07e195-c664-4726-be04-bc75a07e523c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "686eeecc-736f-4b17-910d-455ed2e06c5d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b07e195-c664-4726-be04-bc75a07e523c",
                    "LayerId": "0a25632a-cdc8-4103-af73-fc5008cffd39"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "0a25632a-cdc8-4103-af73-fc5008cffd39",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d06561d3-3f59-491e-9c58-22ec7940847e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}