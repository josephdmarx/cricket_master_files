{
    "id": "271e9ea0-f725-4c4a-9bd5-a73a4fbe6d5d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayer",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 94,
    "bbox_left": 58,
    "bbox_right": 72,
    "bbox_top": 42,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8f26c1aa-c077-4b17-9b14-de483a82d992",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "271e9ea0-f725-4c4a-9bd5-a73a4fbe6d5d",
            "compositeImage": {
                "id": "e38aa957-29c3-4476-99ff-b01a4fbc6397",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f26c1aa-c077-4b17-9b14-de483a82d992",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a79b8e6-6150-4a3d-99aa-8213acc7f689",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f26c1aa-c077-4b17-9b14-de483a82d992",
                    "LayerId": "084e95b7-46e7-49ea-a96c-b0a2004e42c8"
                }
            ]
        },
        {
            "id": "850bb6ab-b2fd-4214-bde5-2d4d295061a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "271e9ea0-f725-4c4a-9bd5-a73a4fbe6d5d",
            "compositeImage": {
                "id": "020d069f-f77b-4698-90ae-fa93bf2fc75a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "850bb6ab-b2fd-4214-bde5-2d4d295061a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7acf79b2-610c-4b82-9f6d-649724f7ac2a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "850bb6ab-b2fd-4214-bde5-2d4d295061a2",
                    "LayerId": "084e95b7-46e7-49ea-a96c-b0a2004e42c8"
                }
            ]
        },
        {
            "id": "57eef0d7-8c55-47eb-8ec5-53c88d208322",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "271e9ea0-f725-4c4a-9bd5-a73a4fbe6d5d",
            "compositeImage": {
                "id": "b6133a6d-fd4d-4cde-ae59-1a99197955e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57eef0d7-8c55-47eb-8ec5-53c88d208322",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "77f768c0-5a30-4093-990a-f420f08351c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57eef0d7-8c55-47eb-8ec5-53c88d208322",
                    "LayerId": "084e95b7-46e7-49ea-a96c-b0a2004e42c8"
                }
            ]
        },
        {
            "id": "9c9add5a-9cd4-41b6-8590-cc85f37b10af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "271e9ea0-f725-4c4a-9bd5-a73a4fbe6d5d",
            "compositeImage": {
                "id": "379a0f93-690f-43cc-a1cb-0b0e13139ce0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c9add5a-9cd4-41b6-8590-cc85f37b10af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53de4a5e-8027-46d1-85a0-880dc926e242",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c9add5a-9cd4-41b6-8590-cc85f37b10af",
                    "LayerId": "084e95b7-46e7-49ea-a96c-b0a2004e42c8"
                }
            ]
        },
        {
            "id": "4022f34c-ef3a-4f73-a691-505e39a53b43",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "271e9ea0-f725-4c4a-9bd5-a73a4fbe6d5d",
            "compositeImage": {
                "id": "70c6c4af-0f1f-4367-9f15-bb1f58867680",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4022f34c-ef3a-4f73-a691-505e39a53b43",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6fd07edc-69cb-4de8-9948-e6c00857d467",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4022f34c-ef3a-4f73-a691-505e39a53b43",
                    "LayerId": "084e95b7-46e7-49ea-a96c-b0a2004e42c8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "084e95b7-46e7-49ea-a96c-b0a2004e42c8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "271e9ea0-f725-4c4a-9bd5-a73a4fbe6d5d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}