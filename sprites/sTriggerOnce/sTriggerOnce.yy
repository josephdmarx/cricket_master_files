{
    "id": "47123730-a81d-43e3-8eeb-f8e082aaeac6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTriggerOnce",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d05a96ff-6696-4252-b0ed-2385ccfed0eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "47123730-a81d-43e3-8eeb-f8e082aaeac6",
            "compositeImage": {
                "id": "e4c4c1ac-512b-4a97-aee2-583e4e1517fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d05a96ff-6696-4252-b0ed-2385ccfed0eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3aeec98c-e302-473e-a4cc-2b6e9946e6e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d05a96ff-6696-4252-b0ed-2385ccfed0eb",
                    "LayerId": "143fbabf-8f9a-4859-9d29-cbac6707c9cd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "143fbabf-8f9a-4859-9d29-cbac6707c9cd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "47123730-a81d-43e3-8eeb-f8e082aaeac6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}