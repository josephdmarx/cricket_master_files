{
    "id": "b79416c7-413b-4c85-b378-458fca7e9fa1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sWaterSpray",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 339,
    "bbox_left": 116,
    "bbox_right": 311,
    "bbox_top": 252,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8279b795-728b-4041-a64c-7ee6d1dd7fa0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b79416c7-413b-4c85-b378-458fca7e9fa1",
            "compositeImage": {
                "id": "02510c74-84c4-418b-b8a3-eba9bb9bc19a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8279b795-728b-4041-a64c-7ee6d1dd7fa0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d937f16-30ec-4a2a-aa12-4a65c904a75d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8279b795-728b-4041-a64c-7ee6d1dd7fa0",
                    "LayerId": "ecd558f4-72ac-4fdb-9a26-2de492497dd5"
                }
            ]
        },
        {
            "id": "c497928d-326e-4dec-82db-0993e7771310",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b79416c7-413b-4c85-b378-458fca7e9fa1",
            "compositeImage": {
                "id": "5b494e55-8193-4a2b-84c1-34513be486d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c497928d-326e-4dec-82db-0993e7771310",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60efc1d8-0eae-4b50-88cc-83e243d7bbc6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c497928d-326e-4dec-82db-0993e7771310",
                    "LayerId": "ecd558f4-72ac-4fdb-9a26-2de492497dd5"
                }
            ]
        },
        {
            "id": "a25ba2bf-5d88-41e3-bdab-ce1ebc4c07b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b79416c7-413b-4c85-b378-458fca7e9fa1",
            "compositeImage": {
                "id": "711fc261-74c7-4b7d-82ad-d4cf1f67b454",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a25ba2bf-5d88-41e3-bdab-ce1ebc4c07b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6429e10c-6dc3-4d30-8548-f83be38e3593",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a25ba2bf-5d88-41e3-bdab-ce1ebc4c07b2",
                    "LayerId": "ecd558f4-72ac-4fdb-9a26-2de492497dd5"
                }
            ]
        },
        {
            "id": "9b68fdef-2831-4b21-b494-040ecd8533f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b79416c7-413b-4c85-b378-458fca7e9fa1",
            "compositeImage": {
                "id": "7f770af7-63ba-4cfb-a3b1-215851b3f75f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b68fdef-2831-4b21-b494-040ecd8533f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9a4ebe3-5b65-460d-b860-75ae993050e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b68fdef-2831-4b21-b494-040ecd8533f9",
                    "LayerId": "ecd558f4-72ac-4fdb-9a26-2de492497dd5"
                }
            ]
        },
        {
            "id": "c0d2a6bd-f28b-49fb-8a89-9fbec88508de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b79416c7-413b-4c85-b378-458fca7e9fa1",
            "compositeImage": {
                "id": "885f4ae7-3dea-4d97-b0ba-95c28dd75650",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0d2a6bd-f28b-49fb-8a89-9fbec88508de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57bc5c65-6841-4ddc-9ec4-47107b7e5921",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0d2a6bd-f28b-49fb-8a89-9fbec88508de",
                    "LayerId": "ecd558f4-72ac-4fdb-9a26-2de492497dd5"
                }
            ]
        },
        {
            "id": "70a3d1ec-ca43-4088-9d94-6822069e8b8d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b79416c7-413b-4c85-b378-458fca7e9fa1",
            "compositeImage": {
                "id": "49b8dce2-35df-4fd8-ab1f-9d13f43c6cb4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "70a3d1ec-ca43-4088-9d94-6822069e8b8d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e991ce7-651d-4580-b79b-129e83f8a041",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "70a3d1ec-ca43-4088-9d94-6822069e8b8d",
                    "LayerId": "ecd558f4-72ac-4fdb-9a26-2de492497dd5"
                }
            ]
        },
        {
            "id": "d8ea6bc2-3e02-440c-8d66-47dac88fef22",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b79416c7-413b-4c85-b378-458fca7e9fa1",
            "compositeImage": {
                "id": "cec03e1e-c237-4603-aae8-c34b69302181",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8ea6bc2-3e02-440c-8d66-47dac88fef22",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2df71922-a162-457c-b8b0-4ff2327e7836",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8ea6bc2-3e02-440c-8d66-47dac88fef22",
                    "LayerId": "ecd558f4-72ac-4fdb-9a26-2de492497dd5"
                }
            ]
        },
        {
            "id": "eb8f4691-8499-480d-846f-4f6adc4abff2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b79416c7-413b-4c85-b378-458fca7e9fa1",
            "compositeImage": {
                "id": "15a2a6ef-c7d9-4e7b-b307-87fdb336208e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb8f4691-8499-480d-846f-4f6adc4abff2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39609f91-eb6f-4b73-9fce-6dc71d5376d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb8f4691-8499-480d-846f-4f6adc4abff2",
                    "LayerId": "ecd558f4-72ac-4fdb-9a26-2de492497dd5"
                }
            ]
        },
        {
            "id": "bd9a0029-c29c-4559-b1ee-7f996c7b5416",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b79416c7-413b-4c85-b378-458fca7e9fa1",
            "compositeImage": {
                "id": "2d3f9731-035e-4232-9df8-7a93f1c032b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd9a0029-c29c-4559-b1ee-7f996c7b5416",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f620abbe-4c3c-471d-a81b-bf061a48cda7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd9a0029-c29c-4559-b1ee-7f996c7b5416",
                    "LayerId": "ecd558f4-72ac-4fdb-9a26-2de492497dd5"
                }
            ]
        },
        {
            "id": "589f00e2-c644-4910-a547-e59d18fdb788",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b79416c7-413b-4c85-b378-458fca7e9fa1",
            "compositeImage": {
                "id": "b24fb3da-435a-4f5d-bd2f-63098fa0469a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "589f00e2-c644-4910-a547-e59d18fdb788",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60ea74e3-7430-4f76-a36c-626ee13e2352",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "589f00e2-c644-4910-a547-e59d18fdb788",
                    "LayerId": "ecd558f4-72ac-4fdb-9a26-2de492497dd5"
                }
            ]
        },
        {
            "id": "317b1587-a9f5-45d1-87ba-470ee46e63ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b79416c7-413b-4c85-b378-458fca7e9fa1",
            "compositeImage": {
                "id": "d9a519e5-570d-4bc4-b019-e9ab81c0ae2b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "317b1587-a9f5-45d1-87ba-470ee46e63ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "532ed28d-a625-43f2-bdb9-7e69a6743ce6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "317b1587-a9f5-45d1-87ba-470ee46e63ff",
                    "LayerId": "ecd558f4-72ac-4fdb-9a26-2de492497dd5"
                }
            ]
        },
        {
            "id": "f9a19232-0e8c-4568-bbf7-0102b90efbc8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b79416c7-413b-4c85-b378-458fca7e9fa1",
            "compositeImage": {
                "id": "5e8a422e-f77e-4d56-86eb-9aa0df6fa497",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f9a19232-0e8c-4568-bbf7-0102b90efbc8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7cb32e6-5fbd-4534-83e9-d762be8f6e8c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9a19232-0e8c-4568-bbf7-0102b90efbc8",
                    "LayerId": "ecd558f4-72ac-4fdb-9a26-2de492497dd5"
                }
            ]
        },
        {
            "id": "6b6e34ee-36fc-486c-918e-099dca5d5eaf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b79416c7-413b-4c85-b378-458fca7e9fa1",
            "compositeImage": {
                "id": "6bf6e508-859c-4fe6-97d2-ebb3c0e33ac0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b6e34ee-36fc-486c-918e-099dca5d5eaf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ac700cc-330a-4345-99ea-896d24952e35",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b6e34ee-36fc-486c-918e-099dca5d5eaf",
                    "LayerId": "ecd558f4-72ac-4fdb-9a26-2de492497dd5"
                }
            ]
        },
        {
            "id": "76ad9dce-859c-4220-9834-f36af9b0d067",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b79416c7-413b-4c85-b378-458fca7e9fa1",
            "compositeImage": {
                "id": "97287347-b5a7-44ea-9c34-3f3d4a33236b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76ad9dce-859c-4220-9834-f36af9b0d067",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6edfee73-62d2-4822-913c-edfe73c3b624",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76ad9dce-859c-4220-9834-f36af9b0d067",
                    "LayerId": "ecd558f4-72ac-4fdb-9a26-2de492497dd5"
                }
            ]
        },
        {
            "id": "90492aa3-2a70-41ea-88f1-cd009ffd05b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b79416c7-413b-4c85-b378-458fca7e9fa1",
            "compositeImage": {
                "id": "3e68bc53-858e-4ba0-92e8-f750b39cc424",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "90492aa3-2a70-41ea-88f1-cd009ffd05b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0868df8e-46a7-412b-a5cb-e447175ac064",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "90492aa3-2a70-41ea-88f1-cd009ffd05b7",
                    "LayerId": "ecd558f4-72ac-4fdb-9a26-2de492497dd5"
                }
            ]
        },
        {
            "id": "cf70b0e6-8c5c-4e02-bf99-6a4800e4a800",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b79416c7-413b-4c85-b378-458fca7e9fa1",
            "compositeImage": {
                "id": "3f5b612c-0608-497d-b1f3-0a8de87fa519",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf70b0e6-8c5c-4e02-bf99-6a4800e4a800",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86382695-6f1b-4d55-bb1d-134d01e9c11a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf70b0e6-8c5c-4e02-bf99-6a4800e4a800",
                    "LayerId": "ecd558f4-72ac-4fdb-9a26-2de492497dd5"
                }
            ]
        },
        {
            "id": "1010204f-2bef-45e9-b83b-70ac00d95445",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b79416c7-413b-4c85-b378-458fca7e9fa1",
            "compositeImage": {
                "id": "94c7cd66-7941-4c5b-a935-c812a0a12b1d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1010204f-2bef-45e9-b83b-70ac00d95445",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "355b3995-d90a-49a8-845e-599f97283d86",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1010204f-2bef-45e9-b83b-70ac00d95445",
                    "LayerId": "ecd558f4-72ac-4fdb-9a26-2de492497dd5"
                }
            ]
        },
        {
            "id": "ccd58d52-b620-4500-bc7f-fcbd9b9dbc63",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b79416c7-413b-4c85-b378-458fca7e9fa1",
            "compositeImage": {
                "id": "830b7d70-caa7-4469-a71c-2aaa0604ef6d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ccd58d52-b620-4500-bc7f-fcbd9b9dbc63",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a36fd6fb-910f-4bc8-81f8-502c85b53bae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ccd58d52-b620-4500-bc7f-fcbd9b9dbc63",
                    "LayerId": "ecd558f4-72ac-4fdb-9a26-2de492497dd5"
                }
            ]
        },
        {
            "id": "88dd804b-6f9f-44f3-9ae5-21a7a6f43de5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b79416c7-413b-4c85-b378-458fca7e9fa1",
            "compositeImage": {
                "id": "89cfcfe9-6cb8-4e36-8c47-334f4e8f0c36",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88dd804b-6f9f-44f3-9ae5-21a7a6f43de5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8b0f028-331c-4a1c-a3a7-312cd3676a83",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88dd804b-6f9f-44f3-9ae5-21a7a6f43de5",
                    "LayerId": "ecd558f4-72ac-4fdb-9a26-2de492497dd5"
                }
            ]
        },
        {
            "id": "b4d8d4b2-5889-4557-87c1-09bff94788e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b79416c7-413b-4c85-b378-458fca7e9fa1",
            "compositeImage": {
                "id": "8571eab2-9a23-4e61-97fe-8637af3396f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4d8d4b2-5889-4557-87c1-09bff94788e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "715f8364-d30c-44fa-ae40-b73e77d62f00",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4d8d4b2-5889-4557-87c1-09bff94788e1",
                    "LayerId": "ecd558f4-72ac-4fdb-9a26-2de492497dd5"
                }
            ]
        },
        {
            "id": "32dfa88a-a71d-4ce9-8837-004daa15db54",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b79416c7-413b-4c85-b378-458fca7e9fa1",
            "compositeImage": {
                "id": "3fd842b1-83f3-457f-a58a-3c3f1f84d72f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "32dfa88a-a71d-4ce9-8837-004daa15db54",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8bd07ea2-ccb4-45ee-b720-5ae833662f18",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32dfa88a-a71d-4ce9-8837-004daa15db54",
                    "LayerId": "ecd558f4-72ac-4fdb-9a26-2de492497dd5"
                }
            ]
        },
        {
            "id": "643ad54c-a3ef-4f00-b793-6dc074733432",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b79416c7-413b-4c85-b378-458fca7e9fa1",
            "compositeImage": {
                "id": "e33ec98b-1b86-4c94-af1e-86b8984cda15",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "643ad54c-a3ef-4f00-b793-6dc074733432",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c2454f5-b46d-4456-a548-ce42500a2926",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "643ad54c-a3ef-4f00-b793-6dc074733432",
                    "LayerId": "ecd558f4-72ac-4fdb-9a26-2de492497dd5"
                }
            ]
        },
        {
            "id": "3110b02d-e36c-441a-8067-c8c829c4cf91",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b79416c7-413b-4c85-b378-458fca7e9fa1",
            "compositeImage": {
                "id": "73b5aea3-8334-4894-b6d2-368c53405b5b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3110b02d-e36c-441a-8067-c8c829c4cf91",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c1c50d6-c3f6-4af1-b147-db6352ad0522",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3110b02d-e36c-441a-8067-c8c829c4cf91",
                    "LayerId": "ecd558f4-72ac-4fdb-9a26-2de492497dd5"
                }
            ]
        },
        {
            "id": "655db7c1-a622-4025-957c-633a24741665",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b79416c7-413b-4c85-b378-458fca7e9fa1",
            "compositeImage": {
                "id": "c3b26997-c1c9-47b4-a408-c12ca7c29aa4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "655db7c1-a622-4025-957c-633a24741665",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf538299-44c0-4943-89e3-f4ca769fd264",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "655db7c1-a622-4025-957c-633a24741665",
                    "LayerId": "ecd558f4-72ac-4fdb-9a26-2de492497dd5"
                }
            ]
        },
        {
            "id": "03acd6a3-fc08-40df-9a0b-295b230c7332",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b79416c7-413b-4c85-b378-458fca7e9fa1",
            "compositeImage": {
                "id": "35a86cdb-ae9c-4f46-92cc-0a733a90f2dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03acd6a3-fc08-40df-9a0b-295b230c7332",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9de14cd7-2685-4249-b47c-42fda4b9c2d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03acd6a3-fc08-40df-9a0b-295b230c7332",
                    "LayerId": "ecd558f4-72ac-4fdb-9a26-2de492497dd5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 400,
    "layers": [
        {
            "id": "ecd558f4-72ac-4fdb-9a26-2de492497dd5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b79416c7-413b-4c85-b378-458fca7e9fa1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 400,
    "xorig": 0,
    "yorig": 0
}