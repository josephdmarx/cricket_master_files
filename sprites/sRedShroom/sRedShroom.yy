{
    "id": "4b4920dc-5d81-44e7-9a10-fe0f388a69ab",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sRedShroom",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 26,
    "bbox_left": 4,
    "bbox_right": 27,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "92640f6a-9f95-4c2b-ae74-3f16ab3b459b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4b4920dc-5d81-44e7-9a10-fe0f388a69ab",
            "compositeImage": {
                "id": "8b5d1bc1-1766-4699-8457-76a3d0795e56",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "92640f6a-9f95-4c2b-ae74-3f16ab3b459b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "568a4125-7a04-49be-b437-c639a144bb41",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92640f6a-9f95-4c2b-ae74-3f16ab3b459b",
                    "LayerId": "252f2eac-c619-4fbf-91ea-a3dc3d8ee949"
                }
            ]
        },
        {
            "id": "49a2a2ff-0d9c-4f45-9485-eee224ff1c56",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4b4920dc-5d81-44e7-9a10-fe0f388a69ab",
            "compositeImage": {
                "id": "377f58f4-1919-4f9d-afb7-136441fe29de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "49a2a2ff-0d9c-4f45-9485-eee224ff1c56",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ddccfde0-1304-42c2-86e1-d4da98288028",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49a2a2ff-0d9c-4f45-9485-eee224ff1c56",
                    "LayerId": "252f2eac-c619-4fbf-91ea-a3dc3d8ee949"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "252f2eac-c619-4fbf-91ea-a3dc3d8ee949",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4b4920dc-5d81-44e7-9a10-fe0f388a69ab",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}