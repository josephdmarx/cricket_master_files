{
    "id": "f435b0ca-46e2-4f77-bfe9-833178d31b38",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSpell01",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 7,
    "bbox_right": 22,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "417cb4a7-f6ef-477a-95c7-a06bd1e99b09",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f435b0ca-46e2-4f77-bfe9-833178d31b38",
            "compositeImage": {
                "id": "717b5123-8a9c-46d9-8045-918de5f9f91a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "417cb4a7-f6ef-477a-95c7-a06bd1e99b09",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f40a00bc-f3a5-407b-909e-16a93a5a9868",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "417cb4a7-f6ef-477a-95c7-a06bd1e99b09",
                    "LayerId": "bf4ff0da-6950-40da-acf5-bcaed8e98ae3"
                }
            ]
        },
        {
            "id": "65ff62e4-db6d-4e04-94ed-ef884f480139",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f435b0ca-46e2-4f77-bfe9-833178d31b38",
            "compositeImage": {
                "id": "89923248-85e5-4d5a-9be4-7805d4602454",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65ff62e4-db6d-4e04-94ed-ef884f480139",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b99e4ed-2e12-4adc-8499-529476b4df32",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65ff62e4-db6d-4e04-94ed-ef884f480139",
                    "LayerId": "bf4ff0da-6950-40da-acf5-bcaed8e98ae3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "bf4ff0da-6950-40da-acf5-bcaed8e98ae3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f435b0ca-46e2-4f77-bfe9-833178d31b38",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}