{
    "id": "0d3a537a-2e84-45d0-834b-ad2b3e325628",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sMushman_Attack",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 94,
    "bbox_left": 36,
    "bbox_right": 97,
    "bbox_top": 40,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "123030f2-133e-420e-96cf-432e038a1b57",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d3a537a-2e84-45d0-834b-ad2b3e325628",
            "compositeImage": {
                "id": "8f680395-95c6-4185-9561-866a4b26506e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "123030f2-133e-420e-96cf-432e038a1b57",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15b71b11-5075-4348-84fb-0b5b18ba6537",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "123030f2-133e-420e-96cf-432e038a1b57",
                    "LayerId": "85448f77-4344-4809-8c43-20deafa5905d"
                }
            ]
        },
        {
            "id": "c479a1b0-2872-4296-ba42-e203972bc238",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d3a537a-2e84-45d0-834b-ad2b3e325628",
            "compositeImage": {
                "id": "15f232c2-6b96-4b39-80c7-2cb5465c0677",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c479a1b0-2872-4296-ba42-e203972bc238",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cac92f21-fc6c-44cd-bd8d-0109a8d59b68",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c479a1b0-2872-4296-ba42-e203972bc238",
                    "LayerId": "85448f77-4344-4809-8c43-20deafa5905d"
                }
            ]
        },
        {
            "id": "351163f9-a1ce-48d3-9ea6-1d5ed63c1358",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d3a537a-2e84-45d0-834b-ad2b3e325628",
            "compositeImage": {
                "id": "468adac9-f519-45d5-b7cc-87ce1ae5ac52",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "351163f9-a1ce-48d3-9ea6-1d5ed63c1358",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08d0c441-26a6-48ff-af79-a788473bbd66",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "351163f9-a1ce-48d3-9ea6-1d5ed63c1358",
                    "LayerId": "85448f77-4344-4809-8c43-20deafa5905d"
                }
            ]
        },
        {
            "id": "0a97f53b-61d0-47e3-8e9f-156e66599a9c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d3a537a-2e84-45d0-834b-ad2b3e325628",
            "compositeImage": {
                "id": "672aba08-6428-4d5e-9896-a62ff06e4398",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a97f53b-61d0-47e3-8e9f-156e66599a9c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "49c3b3be-76c5-422a-96a9-e439f6fc1d08",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a97f53b-61d0-47e3-8e9f-156e66599a9c",
                    "LayerId": "85448f77-4344-4809-8c43-20deafa5905d"
                }
            ]
        },
        {
            "id": "35d409df-9785-4fcb-bf24-62da2c4ddca3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d3a537a-2e84-45d0-834b-ad2b3e325628",
            "compositeImage": {
                "id": "eb869564-67d6-4c5b-873a-945cf61ed3d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35d409df-9785-4fcb-bf24-62da2c4ddca3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd8fa269-06ef-453b-94e8-6e1c4c85f122",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35d409df-9785-4fcb-bf24-62da2c4ddca3",
                    "LayerId": "85448f77-4344-4809-8c43-20deafa5905d"
                }
            ]
        },
        {
            "id": "ecc3cd1e-563c-43f9-b30a-60f693425398",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d3a537a-2e84-45d0-834b-ad2b3e325628",
            "compositeImage": {
                "id": "246b7533-3c99-4d30-8956-66bb5e6786f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ecc3cd1e-563c-43f9-b30a-60f693425398",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3a8922e-96cf-4d99-bd8a-5a7b0c65e2bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ecc3cd1e-563c-43f9-b30a-60f693425398",
                    "LayerId": "85448f77-4344-4809-8c43-20deafa5905d"
                }
            ]
        },
        {
            "id": "c466522d-d673-4e40-93bc-08b469b6e7b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d3a537a-2e84-45d0-834b-ad2b3e325628",
            "compositeImage": {
                "id": "28a7c60c-bc48-4d61-bc22-e963e45d3d32",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c466522d-d673-4e40-93bc-08b469b6e7b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1bdd485-e0c0-4e7e-9353-e37ae6576b8a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c466522d-d673-4e40-93bc-08b469b6e7b3",
                    "LayerId": "85448f77-4344-4809-8c43-20deafa5905d"
                }
            ]
        },
        {
            "id": "8d53abd0-cb93-430e-9f18-069b58a78da0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d3a537a-2e84-45d0-834b-ad2b3e325628",
            "compositeImage": {
                "id": "5674ceec-8de4-4853-807c-bc6856a03819",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d53abd0-cb93-430e-9f18-069b58a78da0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f81b6be7-cdd4-48ec-a69c-2a2fcc3d01c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d53abd0-cb93-430e-9f18-069b58a78da0",
                    "LayerId": "85448f77-4344-4809-8c43-20deafa5905d"
                }
            ]
        },
        {
            "id": "31bdda57-f02c-4be9-892e-6b9a7086f3f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d3a537a-2e84-45d0-834b-ad2b3e325628",
            "compositeImage": {
                "id": "1eb72ecd-8bc0-4f47-a422-a49638e3378e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31bdda57-f02c-4be9-892e-6b9a7086f3f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70ccd7d6-f316-4cce-b113-69ce75ed27d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31bdda57-f02c-4be9-892e-6b9a7086f3f3",
                    "LayerId": "85448f77-4344-4809-8c43-20deafa5905d"
                }
            ]
        },
        {
            "id": "ae19e0aa-7b2e-427c-bf5c-06bafecf4c5a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d3a537a-2e84-45d0-834b-ad2b3e325628",
            "compositeImage": {
                "id": "1b55e2b0-9e09-45f4-a94b-11a110b21811",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae19e0aa-7b2e-427c-bf5c-06bafecf4c5a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "701b5f45-a06b-4c94-8c0d-672aae34299b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae19e0aa-7b2e-427c-bf5c-06bafecf4c5a",
                    "LayerId": "85448f77-4344-4809-8c43-20deafa5905d"
                }
            ]
        },
        {
            "id": "1cde9ba9-85f1-47e3-b2d8-ad2dce71497e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d3a537a-2e84-45d0-834b-ad2b3e325628",
            "compositeImage": {
                "id": "a38e1ac8-9ce6-408b-bce1-ab2071d85780",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1cde9ba9-85f1-47e3-b2d8-ad2dce71497e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b41bf99a-3a11-4307-9dc5-97b954e2204b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1cde9ba9-85f1-47e3-b2d8-ad2dce71497e",
                    "LayerId": "85448f77-4344-4809-8c43-20deafa5905d"
                }
            ]
        },
        {
            "id": "87ac87b7-ade4-4ae6-ab5e-b2d5dbebc8c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d3a537a-2e84-45d0-834b-ad2b3e325628",
            "compositeImage": {
                "id": "c4b0bcd5-a3e0-4f79-bfa1-43827e3a8422",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87ac87b7-ade4-4ae6-ab5e-b2d5dbebc8c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e7457ef8-0bbc-485d-a709-eeb617ebcd79",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87ac87b7-ade4-4ae6-ab5e-b2d5dbebc8c4",
                    "LayerId": "85448f77-4344-4809-8c43-20deafa5905d"
                }
            ]
        },
        {
            "id": "bcdb52e7-adc1-4582-994f-54fa4fb6d79e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d3a537a-2e84-45d0-834b-ad2b3e325628",
            "compositeImage": {
                "id": "cf25d68e-3fe5-441a-bcbc-72606b52f558",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bcdb52e7-adc1-4582-994f-54fa4fb6d79e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa93f592-c097-427b-bee8-c7dc2d31bfdf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bcdb52e7-adc1-4582-994f-54fa4fb6d79e",
                    "LayerId": "85448f77-4344-4809-8c43-20deafa5905d"
                }
            ]
        },
        {
            "id": "d8715b5f-2e63-4e15-a0b0-8b90d1cf7995",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d3a537a-2e84-45d0-834b-ad2b3e325628",
            "compositeImage": {
                "id": "4d5c23bf-a651-4258-970f-05ca3bc70f86",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8715b5f-2e63-4e15-a0b0-8b90d1cf7995",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fdd80b7b-8b9c-42bb-a4a3-8bbab581d19a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8715b5f-2e63-4e15-a0b0-8b90d1cf7995",
                    "LayerId": "85448f77-4344-4809-8c43-20deafa5905d"
                }
            ]
        },
        {
            "id": "c1936501-29f1-4977-8885-b941546ed6ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d3a537a-2e84-45d0-834b-ad2b3e325628",
            "compositeImage": {
                "id": "5e1ac8a5-3009-4b14-8065-9f35280f37b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1936501-29f1-4977-8885-b941546ed6ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf43ad5f-ee46-41d7-a8ff-f3e5510617ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1936501-29f1-4977-8885-b941546ed6ed",
                    "LayerId": "85448f77-4344-4809-8c43-20deafa5905d"
                }
            ]
        },
        {
            "id": "4307f45d-8469-454c-819b-3a95544793c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d3a537a-2e84-45d0-834b-ad2b3e325628",
            "compositeImage": {
                "id": "d48a8642-001e-42f9-9b87-593a3d9d747b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4307f45d-8469-454c-819b-3a95544793c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c86abc2-5d67-4886-9344-4add800539ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4307f45d-8469-454c-819b-3a95544793c9",
                    "LayerId": "85448f77-4344-4809-8c43-20deafa5905d"
                }
            ]
        },
        {
            "id": "519b03af-90b8-4e80-aa95-6c0b0a7de630",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d3a537a-2e84-45d0-834b-ad2b3e325628",
            "compositeImage": {
                "id": "ebb58202-f682-4e7d-af11-9d9d13d7feb3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "519b03af-90b8-4e80-aa95-6c0b0a7de630",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d31c770-3e63-46df-89ac-9cf55f88a540",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "519b03af-90b8-4e80-aa95-6c0b0a7de630",
                    "LayerId": "85448f77-4344-4809-8c43-20deafa5905d"
                }
            ]
        },
        {
            "id": "6634546c-7075-4321-ab02-f318f399e136",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d3a537a-2e84-45d0-834b-ad2b3e325628",
            "compositeImage": {
                "id": "cf2b15b7-8210-48f7-85f9-9f07d9c2cdbf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6634546c-7075-4321-ab02-f318f399e136",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4399e9a7-435f-4339-b531-b052b0481eab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6634546c-7075-4321-ab02-f318f399e136",
                    "LayerId": "85448f77-4344-4809-8c43-20deafa5905d"
                }
            ]
        },
        {
            "id": "7c5edf57-066f-4d38-aff2-7ef43cbdd63e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d3a537a-2e84-45d0-834b-ad2b3e325628",
            "compositeImage": {
                "id": "df1ff726-80a3-49ef-9dc7-7ef5821e8f94",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c5edf57-066f-4d38-aff2-7ef43cbdd63e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "380f34db-fb7b-4e4c-ae99-deca06acbc0f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c5edf57-066f-4d38-aff2-7ef43cbdd63e",
                    "LayerId": "85448f77-4344-4809-8c43-20deafa5905d"
                }
            ]
        },
        {
            "id": "06b28670-b178-4221-a951-2bcd406f7574",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d3a537a-2e84-45d0-834b-ad2b3e325628",
            "compositeImage": {
                "id": "3369e52f-a908-4be7-9bb8-610825f4d45b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06b28670-b178-4221-a951-2bcd406f7574",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "059ea312-3b53-42b8-ac4c-44e848cfb47f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06b28670-b178-4221-a951-2bcd406f7574",
                    "LayerId": "85448f77-4344-4809-8c43-20deafa5905d"
                }
            ]
        },
        {
            "id": "9d05a12b-8b20-4945-a434-bba3d142eca4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d3a537a-2e84-45d0-834b-ad2b3e325628",
            "compositeImage": {
                "id": "9223008f-8b62-48c8-a0bb-915ea3fc4303",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d05a12b-8b20-4945-a434-bba3d142eca4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8df52e5-f3be-4b25-b7ab-af80c3dd96f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d05a12b-8b20-4945-a434-bba3d142eca4",
                    "LayerId": "85448f77-4344-4809-8c43-20deafa5905d"
                }
            ]
        },
        {
            "id": "55caeb94-b04d-4478-8ecf-bf043cf9517f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d3a537a-2e84-45d0-834b-ad2b3e325628",
            "compositeImage": {
                "id": "ce59911a-1374-4233-9f82-ccdf3289efe4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55caeb94-b04d-4478-8ecf-bf043cf9517f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c3f12bc-6307-4cdc-9d59-bbbb59b72fa8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55caeb94-b04d-4478-8ecf-bf043cf9517f",
                    "LayerId": "85448f77-4344-4809-8c43-20deafa5905d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "85448f77-4344-4809-8c43-20deafa5905d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0d3a537a-2e84-45d0-834b-ad2b3e325628",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}