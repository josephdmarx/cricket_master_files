{
    "id": "fc767ae7-e10b-48a8-9e83-033bb6865b06",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sStatusStun",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 1,
    "bbox_right": 30,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7d598aeb-dcc8-4bb0-a84c-6f3e705772f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fc767ae7-e10b-48a8-9e83-033bb6865b06",
            "compositeImage": {
                "id": "fa2a9302-0e6e-4768-ac90-fa2f91bb874e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d598aeb-dcc8-4bb0-a84c-6f3e705772f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "68886588-59ad-4f2a-925c-cb94bf945886",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d598aeb-dcc8-4bb0-a84c-6f3e705772f1",
                    "LayerId": "c6dbb692-95b3-4386-ade1-09140d93b8db"
                }
            ]
        },
        {
            "id": "696bd8d8-81b7-4314-8910-d16d0a7a57d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fc767ae7-e10b-48a8-9e83-033bb6865b06",
            "compositeImage": {
                "id": "76867151-fbb6-45d7-9b4b-020073b17967",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "696bd8d8-81b7-4314-8910-d16d0a7a57d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ced751b6-180a-46b6-8d19-3e701769762b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "696bd8d8-81b7-4314-8910-d16d0a7a57d1",
                    "LayerId": "c6dbb692-95b3-4386-ade1-09140d93b8db"
                }
            ]
        },
        {
            "id": "ecf8f8f5-333b-48a2-b599-ce39ef52501d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fc767ae7-e10b-48a8-9e83-033bb6865b06",
            "compositeImage": {
                "id": "71ab0fe1-b602-453f-bd49-51556c5786fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ecf8f8f5-333b-48a2-b599-ce39ef52501d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8cda2845-f57e-42b3-9fd6-3dee6c662110",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ecf8f8f5-333b-48a2-b599-ce39ef52501d",
                    "LayerId": "c6dbb692-95b3-4386-ade1-09140d93b8db"
                }
            ]
        },
        {
            "id": "a16b4553-7310-4433-9456-1e0e1a516f16",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fc767ae7-e10b-48a8-9e83-033bb6865b06",
            "compositeImage": {
                "id": "2ffbda4f-2a30-4659-b32c-0db8bb2d4cfb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a16b4553-7310-4433-9456-1e0e1a516f16",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54a3a6f9-726a-489f-bcc7-e6a1272380ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a16b4553-7310-4433-9456-1e0e1a516f16",
                    "LayerId": "c6dbb692-95b3-4386-ade1-09140d93b8db"
                }
            ]
        },
        {
            "id": "720eaa00-f805-4555-8c0d-b2a11b08ea89",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fc767ae7-e10b-48a8-9e83-033bb6865b06",
            "compositeImage": {
                "id": "035327d8-fb43-4d7e-b9b1-e26b80f5fad3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "720eaa00-f805-4555-8c0d-b2a11b08ea89",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "49fb0e2d-e57b-4b5f-91b2-333e3114c77b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "720eaa00-f805-4555-8c0d-b2a11b08ea89",
                    "LayerId": "c6dbb692-95b3-4386-ade1-09140d93b8db"
                }
            ]
        },
        {
            "id": "46764442-c016-4437-add0-8e85ed9398df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fc767ae7-e10b-48a8-9e83-033bb6865b06",
            "compositeImage": {
                "id": "be04d050-0e1f-4268-8384-439489937c39",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46764442-c016-4437-add0-8e85ed9398df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60f8b5d4-5421-4185-824e-6186e960c00d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46764442-c016-4437-add0-8e85ed9398df",
                    "LayerId": "c6dbb692-95b3-4386-ade1-09140d93b8db"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "c6dbb692-95b3-4386-ade1-09140d93b8db",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fc767ae7-e10b-48a8-9e83-033bb6865b06",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 13,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}