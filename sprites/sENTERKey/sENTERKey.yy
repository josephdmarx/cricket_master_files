{
    "id": "1293e521-f9c2-4ca0-bb5a-a1dec4be16bc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sENTERKey",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 25,
    "bbox_left": 1,
    "bbox_right": 61,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "173c6e0b-8bfd-4564-8e5e-dadab4b622a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1293e521-f9c2-4ca0-bb5a-a1dec4be16bc",
            "compositeImage": {
                "id": "364732a8-ab98-4ab0-aa19-e778a191483d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "173c6e0b-8bfd-4564-8e5e-dadab4b622a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dbb28564-4683-42e1-80c6-eaf1d4ba6b6e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "173c6e0b-8bfd-4564-8e5e-dadab4b622a8",
                    "LayerId": "138ab5aa-dd13-4a50-823b-bb7d7ac1f8a8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "138ab5aa-dd13-4a50-823b-bb7d7ac1f8a8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1293e521-f9c2-4ca0-bb5a-a1dec4be16bc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 26
}