{
    "id": "36fe6a24-128c-4c05-bd48-ee74fb8a022f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sFrogGreenPU",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 25,
    "bbox_left": 21,
    "bbox_right": 45,
    "bbox_top": 4,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "99bc8380-b281-4bed-8d3b-e12da91630dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36fe6a24-128c-4c05-bd48-ee74fb8a022f",
            "compositeImage": {
                "id": "55f8ac21-0ec6-4ac8-a9bb-c48ce46a607c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99bc8380-b281-4bed-8d3b-e12da91630dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa5bef15-52fc-40da-9959-ab0a6355ba5a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99bc8380-b281-4bed-8d3b-e12da91630dd",
                    "LayerId": "999a0126-2ecc-447e-a32e-a96164b0161d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "999a0126-2ecc-447e-a32e-a96164b0161d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "36fe6a24-128c-4c05-bd48-ee74fb8a022f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 14,
    "yorig": 0
}