{
    "id": "b492bfbf-6716-4045-b33f-423236de529b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTrees01",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 359,
    "bbox_left": 0,
    "bbox_right": 639,
    "bbox_top": 98,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ca16987e-de0b-4b9b-ae02-053cb0aa0ead",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b492bfbf-6716-4045-b33f-423236de529b",
            "compositeImage": {
                "id": "4197686f-0e7f-4b86-95ad-986e97d2b2a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca16987e-de0b-4b9b-ae02-053cb0aa0ead",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bdab196a-02e5-4782-874a-267128700ade",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca16987e-de0b-4b9b-ae02-053cb0aa0ead",
                    "LayerId": "bcb02183-75bb-4baf-87e4-b1526a122b2f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 360,
    "layers": [
        {
            "id": "bcb02183-75bb-4baf-87e4-b1526a122b2f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b492bfbf-6716-4045-b33f-423236de529b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 0,
    "yorig": 0
}