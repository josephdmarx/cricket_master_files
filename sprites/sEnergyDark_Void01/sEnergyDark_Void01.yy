{
    "id": "2c920475-3176-47b9-a474-54fcb6532e52",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sEnergyDark_Void01",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 161,
    "bbox_left": 50,
    "bbox_right": 159,
    "bbox_top": 46,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "54419a25-9312-412e-a508-aa548cd5f6ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2c920475-3176-47b9-a474-54fcb6532e52",
            "compositeImage": {
                "id": "47183997-8fe1-422e-986a-5fdbcbd6928d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54419a25-9312-412e-a508-aa548cd5f6ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3df60c38-c26f-4861-8a81-f3ae62685b3b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54419a25-9312-412e-a508-aa548cd5f6ac",
                    "LayerId": "5bdc0a1a-216d-4f24-a5b9-12af97117726"
                }
            ]
        },
        {
            "id": "b301bff1-33aa-4256-a461-7217a32bba1b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2c920475-3176-47b9-a474-54fcb6532e52",
            "compositeImage": {
                "id": "f2adce26-9b5a-41dd-a53e-d6d535b7a0f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b301bff1-33aa-4256-a461-7217a32bba1b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f60bc16e-6488-4f14-85a9-5a15b9b885e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b301bff1-33aa-4256-a461-7217a32bba1b",
                    "LayerId": "5bdc0a1a-216d-4f24-a5b9-12af97117726"
                }
            ]
        },
        {
            "id": "62024652-55c2-4e56-bff4-07ee9d9d28d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2c920475-3176-47b9-a474-54fcb6532e52",
            "compositeImage": {
                "id": "60b696d5-2016-45e6-8528-ec231863f916",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62024652-55c2-4e56-bff4-07ee9d9d28d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "75df30db-3940-4a8f-9bdb-237e0081d86f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62024652-55c2-4e56-bff4-07ee9d9d28d4",
                    "LayerId": "5bdc0a1a-216d-4f24-a5b9-12af97117726"
                }
            ]
        },
        {
            "id": "4dc64bea-0381-4b6a-8abb-0a454d3f62a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2c920475-3176-47b9-a474-54fcb6532e52",
            "compositeImage": {
                "id": "00235c4e-a2b2-445d-ba68-b40991ebd1aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4dc64bea-0381-4b6a-8abb-0a454d3f62a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3fc560ff-fa94-4f15-bdb5-33c22b8a4d7b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4dc64bea-0381-4b6a-8abb-0a454d3f62a0",
                    "LayerId": "5bdc0a1a-216d-4f24-a5b9-12af97117726"
                }
            ]
        },
        {
            "id": "4c431a13-de99-4a60-bdf8-c09c520e6f7c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2c920475-3176-47b9-a474-54fcb6532e52",
            "compositeImage": {
                "id": "f322a375-b0ca-4e02-8663-eaaebfa2def0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c431a13-de99-4a60-bdf8-c09c520e6f7c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6986b8ad-4119-4861-9d85-8aa1d0c316ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c431a13-de99-4a60-bdf8-c09c520e6f7c",
                    "LayerId": "5bdc0a1a-216d-4f24-a5b9-12af97117726"
                }
            ]
        },
        {
            "id": "fdacab42-d1dc-4e4d-831b-deec7a6da579",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2c920475-3176-47b9-a474-54fcb6532e52",
            "compositeImage": {
                "id": "43942a5b-4bb4-4c0f-ad0c-8f8de2c2debf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fdacab42-d1dc-4e4d-831b-deec7a6da579",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bdddfa76-3d22-4d75-844e-549df4b62dfc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fdacab42-d1dc-4e4d-831b-deec7a6da579",
                    "LayerId": "5bdc0a1a-216d-4f24-a5b9-12af97117726"
                }
            ]
        },
        {
            "id": "f3ea960c-6b99-460b-ab83-d3c0220964d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2c920475-3176-47b9-a474-54fcb6532e52",
            "compositeImage": {
                "id": "65eda459-d621-408d-8216-1d2381244daf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3ea960c-6b99-460b-ab83-d3c0220964d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2758799a-c945-4839-b62d-1a90823011f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3ea960c-6b99-460b-ab83-d3c0220964d6",
                    "LayerId": "5bdc0a1a-216d-4f24-a5b9-12af97117726"
                }
            ]
        },
        {
            "id": "50be0cb5-4713-44a3-b269-b997a1514d41",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2c920475-3176-47b9-a474-54fcb6532e52",
            "compositeImage": {
                "id": "a99649d9-fb70-40fe-9aa5-80bec19e85f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "50be0cb5-4713-44a3-b269-b997a1514d41",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f1cb14b-ed3d-415a-82a2-2675b6f4e682",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "50be0cb5-4713-44a3-b269-b997a1514d41",
                    "LayerId": "5bdc0a1a-216d-4f24-a5b9-12af97117726"
                }
            ]
        },
        {
            "id": "d843ec72-6862-4928-892e-fbfcb872cabd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2c920475-3176-47b9-a474-54fcb6532e52",
            "compositeImage": {
                "id": "efe5a7d3-a278-432b-9a9e-e0322a365ace",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d843ec72-6862-4928-892e-fbfcb872cabd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "81a80554-0c35-49cd-813a-4793aac52807",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d843ec72-6862-4928-892e-fbfcb872cabd",
                    "LayerId": "5bdc0a1a-216d-4f24-a5b9-12af97117726"
                }
            ]
        },
        {
            "id": "31e6ce82-f332-41bc-ae27-62f1d35b9828",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2c920475-3176-47b9-a474-54fcb6532e52",
            "compositeImage": {
                "id": "55abb48e-18c2-4cca-8c98-3de4fb5217a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31e6ce82-f332-41bc-ae27-62f1d35b9828",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "36b262b2-14f2-4f2b-a015-b84af178cae2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31e6ce82-f332-41bc-ae27-62f1d35b9828",
                    "LayerId": "5bdc0a1a-216d-4f24-a5b9-12af97117726"
                }
            ]
        },
        {
            "id": "49ff0391-da7c-45f9-8198-2919cd07fbed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2c920475-3176-47b9-a474-54fcb6532e52",
            "compositeImage": {
                "id": "a4ddccfe-301f-4075-aba1-959b33ed78e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "49ff0391-da7c-45f9-8198-2919cd07fbed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f6ae2ba-f673-4904-a5af-00f0a658a952",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49ff0391-da7c-45f9-8198-2919cd07fbed",
                    "LayerId": "5bdc0a1a-216d-4f24-a5b9-12af97117726"
                }
            ]
        },
        {
            "id": "c8ffc1ff-2b96-4c20-b020-f9abbb96f63e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2c920475-3176-47b9-a474-54fcb6532e52",
            "compositeImage": {
                "id": "0356e2e5-2e25-4d6b-8f35-f89d8d8ab3b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8ffc1ff-2b96-4c20-b020-f9abbb96f63e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "52e3701a-d16b-4211-b86c-f93e86f47de5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8ffc1ff-2b96-4c20-b020-f9abbb96f63e",
                    "LayerId": "5bdc0a1a-216d-4f24-a5b9-12af97117726"
                }
            ]
        },
        {
            "id": "f3823ec9-0706-419a-b82c-5e27c1ed3420",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2c920475-3176-47b9-a474-54fcb6532e52",
            "compositeImage": {
                "id": "2def446c-f1d8-4e4f-82a0-848fcf23b0f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3823ec9-0706-419a-b82c-5e27c1ed3420",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4767fe2e-2b5a-4498-8b8a-cc695a5969ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3823ec9-0706-419a-b82c-5e27c1ed3420",
                    "LayerId": "5bdc0a1a-216d-4f24-a5b9-12af97117726"
                }
            ]
        },
        {
            "id": "d03c41f7-af15-4f05-9c7e-bd5f7d354c7e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2c920475-3176-47b9-a474-54fcb6532e52",
            "compositeImage": {
                "id": "e30396d0-bf1a-4fe0-8be9-702a86134358",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d03c41f7-af15-4f05-9c7e-bd5f7d354c7e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "17ee57b8-0f34-428d-ae70-d86f203988be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d03c41f7-af15-4f05-9c7e-bd5f7d354c7e",
                    "LayerId": "5bdc0a1a-216d-4f24-a5b9-12af97117726"
                }
            ]
        },
        {
            "id": "ea1af588-2be9-46ea-a2f1-7a8e6cc5b494",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2c920475-3176-47b9-a474-54fcb6532e52",
            "compositeImage": {
                "id": "71f94ec8-2d89-4054-8507-26ee99c85055",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea1af588-2be9-46ea-a2f1-7a8e6cc5b494",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "faf26474-c866-491f-b625-47f8021156af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea1af588-2be9-46ea-a2f1-7a8e6cc5b494",
                    "LayerId": "5bdc0a1a-216d-4f24-a5b9-12af97117726"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 200,
    "layers": [
        {
            "id": "5bdc0a1a-216d-4f24-a5b9-12af97117726",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2c920475-3176-47b9-a474-54fcb6532e52",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 200,
    "xorig": 100,
    "yorig": 100
}