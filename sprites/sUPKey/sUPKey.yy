{
    "id": "adc01b08-0dfe-41cb-b1ca-9b1b42887784",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sUPKey",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 25,
    "bbox_left": 5,
    "bbox_right": 26,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e004214b-b293-41d5-8911-7b920051269e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "adc01b08-0dfe-41cb-b1ca-9b1b42887784",
            "compositeImage": {
                "id": "fa7a70e4-92d2-4390-9452-75ff26714e45",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e004214b-b293-41d5-8911-7b920051269e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a933acb8-27d0-40d6-a077-559b33b71669",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e004214b-b293-41d5-8911-7b920051269e",
                    "LayerId": "bea9daad-546d-441a-b67e-d559a7bcbe3f"
                }
            ]
        },
        {
            "id": "4faa95c9-f9b7-4119-b329-0ccf6440c122",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "adc01b08-0dfe-41cb-b1ca-9b1b42887784",
            "compositeImage": {
                "id": "cb701b18-6732-485f-9da6-a8d6c03661c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4faa95c9-f9b7-4119-b329-0ccf6440c122",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d34fa87-05e6-4c73-8343-5ad9aeceddaf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4faa95c9-f9b7-4119-b329-0ccf6440c122",
                    "LayerId": "bea9daad-546d-441a-b67e-d559a7bcbe3f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "bea9daad-546d-441a-b67e-d559a7bcbe3f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "adc01b08-0dfe-41cb-b1ca-9b1b42887784",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 15,
    "yorig": 26
}