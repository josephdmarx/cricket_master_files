{
    "id": "7491854b-5d5b-4555-b0cb-57b9db729954",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Ground01_Water",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 143,
    "bbox_left": 43,
    "bbox_right": 84,
    "bbox_top": 64,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c5699461-9106-42a6-a290-36ff08ed08ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7491854b-5d5b-4555-b0cb-57b9db729954",
            "compositeImage": {
                "id": "f5aca3a3-268c-436c-b7cb-6dde27157571",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5699461-9106-42a6-a290-36ff08ed08ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8841d501-501f-4832-8d03-08c15ed8a980",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5699461-9106-42a6-a290-36ff08ed08ec",
                    "LayerId": "ebac4f26-edb2-42ba-9b89-9b855029b826"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "ebac4f26-edb2-42ba-9b89-9b855029b826",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7491854b-5d5b-4555-b0cb-57b9db729954",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 512,
    "xorig": -46,
    "yorig": 94
}