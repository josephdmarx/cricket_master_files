if (flying)
{
	sprite_index = IdleSprite;

	var _lungeforce = -4;
	
	var _direction = point_direction(oPlayer.x, oPlayer.y, x, y);

	var _xforce = (lengthdir_x(_lungeforce, _direction));
	var _yforce = (lengthdir_y(_lungeforce, _direction));
	
	hsp = _xforce * 1;
	vsp = _yforce * 1;
	
	attacking = true;
}