//@desc adds the specified equipment to the equipment list

var _type = argument0;  //RECIPETYPE.CUREFEVER for example
var indexAddedAt = -2;

if (ArrayFindIndex(oRecipeManager.recipe, _type) == -1)  //if the item type is NOT already in our equipment list
{
	indexAddedAt = ArrayReplaceValue(oRecipeManager.recipe, RECIPETYPE.NONE, _type);  //finds a none value, and changes it to the new value}
}
	