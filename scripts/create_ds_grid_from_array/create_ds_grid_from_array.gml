///@desc create_ds_grid_from_array
///@arg array

var ds_grid;
var array = argument[0];
var array_num_quests =  array_length_1d(array);
var array_w = array_length_1d(array[0]);

ds_grid = ds_grid_create(array_w, array_num_quests);

var yy  = 0; 
repeat(array_num_quests)
{
	var quest_array = array[yy];   //this is grabbing an entire quest array (with it's own sub arrays)
		
	var xx = 0;	
	repeat(array_w)
	{
		ds_grid[# xx, yy] = quest_array[xx];  //begins filling the grid with the info from the quests' array
		xx++;
	}
		
	yy++;

}
	
return ds_grid;  //this return will save the value of ds_grid from this script

