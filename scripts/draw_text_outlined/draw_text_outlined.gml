///@desc draw text with an outline 
///@arg x
///@arg y
///@arg text

var _x = argument0;
var _y = argument1;
var _text = argument2;

draw_set_halign(fa_left);
draw_set_font(font01);

draw_set_color(c_black);
draw_text_transformed(_x+1,_y,_text,.5,.5,0);
draw_text_transformed(_x-1,_y,_text,.5,.5,0);
draw_text_transformed(_x,_y+1,_text,.5,.5,0);
draw_text_transformed(_x,_y-1,_text,.5,.5,0);

draw_set_color(c_white);
draw_text_transformed(_x,_y,_text,.5,.5,0);