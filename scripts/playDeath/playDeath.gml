///@desc death animation player
///@arg	deathanimation
///@arg	x
///@arg	y


with (instance_create_layer(argument1, argument2,"Effects", oDeathPlayer))
	{
		image_xscale = other.image_xscale;
		sprite_index = argument0;
	}