///@desc draw text with an outline 
///@arg x
///@arg y
///@arg text

var _x = argument0;
var _y = argument1;
var _text = argument2;

draw_set_halign(fa_left);
draw_set_font(font04);

draw_set_color(c_black);

//draw_text_ext_transformed(100+border, 106+border, text_current, 22, 250, .5, .5, 0);


//draw_text_ext_transformed(_x+1, _y, _text, 22, 250, 0.5, 0.5, 0);
//draw_text_ext_transformed(_x-1, _y, _text, 22, 250, 0.5, 0.5, 0);
//draw_text_ext_transformed(_x, _y+1, _text, 22, 250, 0.5, 0.5, 0);
//draw_text_ext_transformed(_x, _y-1, _text, 22, 250, 0.5, 0.5, 0);

//draw_text_transformed(_x+1,_y,_text,.5,.5,0);
//draw_text_transformed(_x-1,_y,_text,.5,.5,0);
//draw_text_transformed(_x,_y+1,_text,.5,.5,0);
//draw_text_transformed(_x,_y-1,_text,.5,.5,0);

draw_set_color(c_black);
draw_text_ext_transformed(_x,_y,_text, 22, 250, 0.5, 0.5,0);