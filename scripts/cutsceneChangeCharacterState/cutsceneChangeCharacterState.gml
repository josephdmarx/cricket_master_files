///@arg target object
///@arg xscale
///@arg state
with (argument0)
{
	image_speed = 1;
	image_index = 0;
	image_xscale = argument1;
	State = argument2;
}

cutsceneEndAction();