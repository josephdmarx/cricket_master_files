
magicDefinitions[MAGICTYPE.LENGTH, itemproperties.length] = noone

magicAddMagicDef(MAGICTYPE.NONE, sEmpty, "", MGC_Empty, "Empty"); //calling a new script, passing in the values of the sprite, name, amount, and useScript

magicAddMagicDef(MAGICTYPE.FIRE, sBookFire, "Fire", MGC_Fire, "Hurl a small fireball.");

magicAddMagicDef(MAGICTYPE.ICE, sEmpty, "Ice", MGC_Ice, "Freeze an enemy in place.");