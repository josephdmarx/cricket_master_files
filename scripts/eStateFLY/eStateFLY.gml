var _targetx = sign(TargetX)*spd;
var _targety = sign(TargetY)*spd;

if (abs(TargetY) <= 1) _targety = 0;
		
EnemyCollide(_targetx,_targety,oPlayer,false);

if (_targetx > 0)
{
	image_xscale = 1;
}

else if (_targetx < 0)
{
	image_xscale = -1;
}

else
{
	image_xscale = 1;
}