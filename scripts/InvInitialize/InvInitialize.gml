//set up some macros

#macro MAX_ACTIVE_INV_ITEMS 48
#macro MAX_INV_ITEMS 48

inventory[MAX_INV_ITEMS] = itemtype.none;     //initialize to NO items

InvDeclareItemDefinitions();