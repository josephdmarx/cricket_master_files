///@desc dialogue popup

///@arg portrait
///@arg string

with (instance_create_layer(x,y, "GUI", oPortraitDialogueCutscenePlayer))
{
	character = argument0;
	text = argument1;
}

if (oPortraitDialogueCutscenePlayer.alreadyplayed = true)
	{
		instance_destroy(oPortraitDialogueCutscenePlayer);
		cutsceneEndAction();
	}