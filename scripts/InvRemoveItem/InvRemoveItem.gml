// @desc remove an item

var _type = argument0;
var _indexOfGivenType = ArrayFindIndex(inventory, _type);

if (_indexOfGivenType != -1)
{
	itemDefinitions[_type, itemproperties.amount] -= 1;   //remove 1 of the given index
	
	if (itemDefinitions[_type, itemproperties.amount] <= 0)   //if there are zero, remove the index completely
		inventory[_indexOfGivenType] = itemtype.none;	
	
}