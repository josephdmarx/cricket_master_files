

scene++;

if(scene > array_length_1d(scene_info)-1)  //if the scene value is greater than the number of scenes, destroy the cutscene object
{
	instance_destroy();	
	exit;
}

event_perform(ev_other, ev_user0);