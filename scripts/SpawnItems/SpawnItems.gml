///@ desc use for multiple item drops
///@ arg itemtype
///@ arg itemamount


var _item = argument[0];
var _amount = argument[1]

for (var i = 0; i < _amount; i += 1)
{
	
	instance_create_layer(x+irandom_range(-50,50),y-50+irandom_range(-40,10),"Pickups",_item);
	
}