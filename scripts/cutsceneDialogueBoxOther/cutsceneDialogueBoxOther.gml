///@desc dialogue popup

///@arg portrait
///@arg string

with (instance_create_layer(x,y, "GUI", oPortraitDialogueCutscene))
{
	character = argument0;
	text = argument1;
}

if (oPortraitDialogueCutscene.alreadyplayed = true)
	{
		instance_destroy(oPortraitDialogueCutscene);
		cutsceneEndAction();
	}