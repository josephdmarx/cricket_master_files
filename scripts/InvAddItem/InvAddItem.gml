// @desc pickup item
// Attempt to add an item to inventory
// @param type item type

var _type = argument0;
var indexAddedAt = -2;

if (ArrayFindIndex(oInventoryManager.inventory, _type) == -1)  //if the item type is NOT already in our inventory
{
	indexAddedAt = ArrayReplaceValue(oInventoryManager.inventory, itemtype.none, _type);//finds a none value, and changes it to the new value}
}

if (indexAddedAt != -1)
{
	oInventoryManager.itemDefinitions[_type, itemproperties.amount] += 1;
}

return (indexAddedAt != -1);
	