///@desc smallparticleburst
///@arg xpos
///@arg ypos
///@arg particle_to_fire


var _x = argument[0];
var _y = argument[1];
var particle = argument[2];

var _offsetx = random_range(-5,5);
var _offsety = random_range(-25,25);

repeat(1)
{
		instance_create_layer(_x+_offsetx,_y+_offsety-10,"Effects_BG",particle);
}