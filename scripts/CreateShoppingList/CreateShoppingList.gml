//this will create a list based on what Merchant calls it. 
//@ arg text from vendor
//@ arg shopping list from vendor
//@ arg prices from vendor


var _shoppingmenu = instance_create_layer(0,0,"GUI",oShopManager);

with (_shoppingmenu)
	{
		text = argument[0];
		items = argument[1];
		price = argument[2];
	
		var len = array_length_1d(items);
		var i = 0;
		
		numberOfSaleItems = len;		
		
		repeat(len)
		{
			
			saleItemTypes[i] = items[i];
			
			saleItemNames[i] = oInventoryManager.itemDefinitions[items[i], itemproperties.name];
			saleItemSprites[i] = oInventoryManager.itemDefinitions[items[i], itemproperties.sprite];
			salePrices[i] = price[i];
			
			
			i++;
		}

	}
	
return (_shoppingmenu);