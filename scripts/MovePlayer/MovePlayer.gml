//The Player's full movement script

var _obj = argument[0];  // the collision object

Horiz = (key_right - key_left);
hsp = Horiz * WalkSpeed;
vsp = vsp + grv;



if (key_down) and (grounded)
{	
	hsp = 0;
	CanAttack = false;
	
	if (key_jump) and (place_meeting(x,y+1,oFloorOneWay))
	{
		grounded = false;
	}
	
	else if (key_jump) and !(place_meeting(x,y+1,oFloorOneWay))
	{
		if !(charging)
			{
				grounded = false; 
				audio_play_sound(sfxJump01,1,0);
				vsp = -8;
			}
	}
}

if (underwater)
	hsp = ((Horiz * WalkSpeed) * 0.7);

if (place_meeting(x,y+1,_obj)) and (key_jump)  // if we are on the ground and we press jump
{
	if !(charging) and !(key_down)				//  if we are not charging
		{
			audio_play_sound(sfxJump01,1,0);
			vsp = -8;
		}
}

//Collision and Movement
if (place_meeting(x+hsp,y,_obj))  //stop moving when you hit a wall
{			
	while(!place_meeting(x+sign(hsp),y,_obj))
	{
		x = x + sign(hsp);
	}
	hsp = 0;	
}

if (place_meeting(x,y+1,_obj)) and (State = StateATTACK)  //attack while moving - comment this out to allow.
{
	hsp = 0;
}

if (place_meeting(x,y+1,_obj)) and (State = StateFIRE)  //attack while moving - comment this out to allow.
{
	hsp = 0;
}


if (place_meeting(x,y+1,_obj)) and (MeleeAttack)  //stop moving while attacking on the ground melee
{
	hsp = 0;
}


if (stun)
	{
		hsp = 0;
	}
	

if (charging) and (!place_meeting(x,y+1,_obj))   // if we are charging an attack and we are not on the ground
	x = x + hsp;									// we continue moving horizontally


if !(charging) and (Horiz != 0)					// if we are not charging and have a key pressed we move horizontally
	x = x + hsp;  //move
	

if (place_meeting(x,y+vsp,_obj))  //stop moving vertically when you hit the ground  
{
	while(!place_meeting(x,y+sign(vsp),_obj))
	{
		y = y + sign(vsp);
	}
	vsp = 0;
}

	y = y + vsp;  //move vertical
