/// @param x
/// @param y
/// @param collission object
/// @param can be destroyed?


var _x = argument[0];
var _y = argument[1];
var _obj = argument[2];
var _destroy = argument[3];


_y = _y + 0.4; //grav

if (place_meeting(x+_x,y,_obj))
{
	while(!place_meeting(x+sign(_x),y,_obj))
	{
		x += sign(_x);
	}
	_x = 0;	
	
	if (_destroy)	///if the instance is declared destroyable, destroy it!
	{
		ParticleBurstSmall(x,y,particle);
		instance_change(deathAnim, true);
		//instance_destroy();
	}
}

x += _x;

if (place_meeting(x,y+_y,_obj))                    
{
	while(!place_meeting(x,y+sign(_y),_obj))
	{
		y += sign(_y);
	}
	_y = 0;
	
	if (_destroy)
	{
		ParticleBurstSmall(x,y,particle);
		instance_change(deathAnim, true);
		//instance_destroy();
	}
}

y += _y;

