Action = SPELL;   //referencing back to the macros

var _x = XOffset;
var _y = YOffset;




if (CanAttack) and (_x > 0)
	{
		
		CanAttack = false;
		
		ParticleBurstSmall(x+_x, y+_y, oRedSparkle);
		audio_play_sound(sfxFire01,1,0);
		with (instance_create_layer(x+_x, y+_y,"Effects",oFireballSmall))
		{
			
			image_xscale = 1;
			xdir = _x;
			ydir = 0;
		}
		
	}

else if (CanAttack) and (_x < 0)
	{
		
		CanAttack = false;
		
		ParticleBurstSmall(x+_x, y+_y, oRedSparkle);
		audio_play_sound(sfxFire01,1,0);
		
		with (instance_create_layer(x+_x, y+_y,"Effects",oFireballSmall))
		{
			
			image_xscale = -1;
			xdir = _x;
			ydir = 0;
		}
		
	}
	



