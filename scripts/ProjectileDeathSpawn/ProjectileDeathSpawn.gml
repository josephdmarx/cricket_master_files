var _amount = argument0;
var _projectile = argument1;

repeat (_amount)
{
	with (instance_create_layer(x, y, "Enemies", _projectile))
	{
		hsp = random_range(1 * (-1 * image_xscale), 3 * (-1 * image_xscale));
		vsp = random_range(-2, -5);
	}
}


