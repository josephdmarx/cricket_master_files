/// @desc SlideTransition(mode, targetroom, targetX, targetY)
/// @arg Mode sets transition mode between next, restart, and goto
/// @arg Target sets target room when using the goto mode
/// @arg TargetX sets X position of player upon arrival in new room
/// @arg TargetY sets Y position of player upon arrival in new room

with (oTransition)
{ 
	
	
	mode = argument[0];
	
	if (argument_count > 1) 
		{
			target = argument[1];
			targetX = argument[2];
			targetY = argument[3];
		}
	
	
	
}