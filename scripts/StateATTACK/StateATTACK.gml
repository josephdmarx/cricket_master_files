Action = ATTACK;   //referencing back to the macros

var _x = XOffset;
var _y = YOffset;

if (CanAttack) and (_x > 0)
	{
		
		CanAttack = false;
				
		audio_play_sound(sfxSpell01,1,0);
		with (instance_create_layer(x+_x, y+_y,"Effects",oSpell01))
		{
		
		}
		with (instance_create_layer(x+_x, y+_y,"Effects",parProjectile))
		{
			image_xscale = 1;
			xdir = _x;
			ydir = 0;
		}
	}

else if (CanAttack) and (_x < 0)
	{
		
		CanAttack = false;
		
		audio_play_sound(sfxSpell01,1,0);
		with (instance_create_layer(x+_x, y+_y,"Effects",oSpell01))
		{
			
		}
		
		with (instance_create_layer(x+_x, y+_y,"Effects",parProjectile))
		{
			image_xscale = -1;
			xdir = _x;
			ydir = 0;
		}
	}

