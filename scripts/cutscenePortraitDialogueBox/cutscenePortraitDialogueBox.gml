///@desc dialogue popup

///@arg portrait
///@arg string
with (instance_create_layer(x,y, "GUI", oPortraitDialogueCutscene))
{
	text = argument0;
	speakers = argument1;
	names = argument2;
	portraits = argument3;
}

if (oPortraitDialogueCutscene.alreadyplayed = true)
	{
		instance_destroy(oPortraitDialogueCutscene);
		cutsceneEndAction();
	}

