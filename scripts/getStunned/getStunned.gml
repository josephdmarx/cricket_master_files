var _time = (argument0 * room_speed);

sprite_index = stunSprite;

whackable = false;
stunned = true;

stuntimer = _time;

audio_play_sound(sfxCaneSmack01,1,0);
instance_create_layer(x,y-32,"Effects",oStatusStun);

hsp = 0;
vsp = 0;
hurt = true;
