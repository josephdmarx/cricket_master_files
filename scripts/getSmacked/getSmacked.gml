if (flying)
{
	sprite_index = IdleSprite;

	var _knockbackforce = 6;

	whackable = false;
	
	audio_play_sound(sfxCaneSmack01,1,0);
	
	var _direction = point_direction(oPlayer.x, oPlayer.y, x, y);

	var _xforce = (lengthdir_x(_knockbackforce, _direction));
	var _yforce = (lengthdir_y(_knockbackforce, _direction));
	
	hsp = _xforce * 1;
	vsp = _yforce * 1;
	hurt = true;
}

else
{
	sprite_index = IdleSprite;
	image_speed = 0;

	var _knockbackforce = 6;

	whackable = false;
	
	audio_play_sound(sfxCaneSmack01,1,0);
	
	var _direction = point_direction(oPlayer.x, oPlayer.y, x, y);

	var _xforce = (lengthdir_x(_knockbackforce, _direction));
	var _yforce = (lengthdir_y(_knockbackforce, _direction));
	
	hsp = _xforce;
	vsp = -5;
	hurt = true;
}
