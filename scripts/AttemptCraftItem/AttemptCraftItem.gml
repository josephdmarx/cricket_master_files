var _ingamount0 = oInventoryManager.itemDefinitions[argument1, itemproperties.amount];
var _ingamount1 = oInventoryManager.itemDefinitions[argument2, itemproperties.amount];
var _ingamount2 = oInventoryManager.itemDefinitions[argument3, itemproperties.amount];


if (_ingamount0 >= 1)
{
	if (_ingamount1 >= 1)
	{
		if (_ingamount2 >= 1)
		{
				with (oInventoryManager)
				{
					InvRemoveItem(argument1);
					InvRemoveItem(argument2);
					InvRemoveItem(argument3);
				}
			audio_play_sound(sfxPickup01,1,0);
			InvAddItem(argument0);
		}
			
		
			
	}
}

else 
{
	audio_play_sound(sfxMenuCancel,1,0);
	exit;
}