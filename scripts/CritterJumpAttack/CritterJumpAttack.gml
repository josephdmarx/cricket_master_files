/// @ param x
/// @ param y
/// @ param collission object
/// @ param can be destroyed?

var _x = argument[0];
var _obj = argument[1];
var _destroy = argument[2];

vsp = vsp + 0.4;

if (_x > 0)
{
	image_xscale = 1;
	
}

else if (_x < 0)
{
	image_xscale = -1;
	
}

else
{
	image_xscale = 1;
}

if (place_meeting(x+_x,y,_obj))
{
	while(!place_meeting(x+sign(_x),y,_obj))
	{
		x += sign(_x);
	}
	
	_x = 0;	
	
	if (_destroy)	///if the instance is declared destroyable, destroy it!
	{
		ParticleBurstSmall(x,y,particle);
		instance_change(deathAnim, true);
		instance_destroy();
	}
}

x += _x;

if (place_meeting(x,y+vsp,_obj))                    
{
	while(!place_meeting(x,y+vsp,_obj))
	{
		y = y + sign(vsp);
	}
	
	vsp = 0;
	
	if (_destroy)
	{
		ParticleBurstSmall(x,y,particle);
		instance_change(deathAnim, true);
		instance_destroy();
	}
}


y = y + vsp;


if (place_meeting(x,y+1, oWall)) grounded = true;