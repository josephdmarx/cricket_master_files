var _targetx = sign(TargetX)*spd;
var _targety = sign(TargetY)*spd;

switch (attacktype)
{

	case LUNGE:
	{
		if (distance_to_object(oPlayer)	< attackrange)
		{
			State = eStateWINDUP;
		}
		
		else 
		{
			State = eStateFLY;
		}
	break;
	}

	case NOTHING:
	{
		if (turret)
			{
				image_speed = 1;
				image_xscale = sign(TargetX);
				sprite_index = IdleSprite;

				if (distance_to_object(oPlayer) < range)
				{
					image_index = 0;
					State = eStateATTACK;
				}
			}
		
		
		else if (flying)
		{
			State = eStateFLY;	
		}

		else if (jumper)
			{
				State = eStateWINDUP;
			}

		//if (distance_to_object(oPlayer) > range) and (grounded)
		//	State = eStateIDLE;

		else if (hostile)
			{
		
				image_speed = 1;
				sprite_index = MoveSprite;
			
		
				CritterMoveCollide(_targetx, oWall, false)  //runs toward the player
				if (distance_to_object(oPlayer) < attackrange) and (grounded)
					{
						State = eStateATTACK;
					}
			
				else if (distance_to_object(oPlayer) > range) and (grounded)
				State = eStateIDLE;
		
			}

		else if !(hostile)
			{	
				if (distance_to_object(oPlayer) > range) and (grounded)
				State = eStateIDLE;
		
				else
				{
					image_speed = 1;
					sprite_index = MoveSprite;
					CritterMoveCollide((_targetx* -1), oWall, false)  //runs AWAY from the player
				}
			}
	}
	
}

