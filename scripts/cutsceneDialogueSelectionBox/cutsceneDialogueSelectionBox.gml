///@desc dialogue popup


///@arg portrait
///@arg string
///@arg selection_option1_as_string
///@arg selection_option2_as_string
///@arg quest_to_consider_and_possibly_advance


with (instance_create_layer(x,y, "GUI", oPortraitDialogueCutsceneSelection))
	{
		character = argument0;
		text = argument1;
		option1 = argument2;
		option2 = argument3;
		quest = argument4;	
	}


cutsceneEndAction();


if (oPortraitDialogueCutsceneSelection.alreadyplayed = true)
	{
		instance_destroy(oPortraitDialogueCutsceneSelection);	
	}
