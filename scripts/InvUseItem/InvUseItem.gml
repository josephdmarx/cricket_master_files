var _type = argument0;
var _itemscript = oInventoryManager.itemDefinitions[_type, itemproperties.useScript];
var _usable = oInventoryManager.itemDefinitions[_type, itemproperties.usable];


if (_usable)
	{
		script_execute(_itemscript);
		InvRemoveItem(_type);
	}