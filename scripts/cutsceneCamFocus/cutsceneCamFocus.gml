///@desc cutscene move camera

///@arg hascontrol
///@arg camera_speed
///@arg camera_target
///@arg camera_snap

var _control = argument0;
var _spd = argument1;
var _target = argument2;
var snap = argument3;

if (snap)
{
	camGame.x = _target.x;
	camGame.y = _target.y;
	snap = false;
}
	
if (instance_exists(oPlayer))
{
	oPlayer.hascontrol = false;
}

//oPlayer.hascontrol = _control;
camGame.camspd = _spd;
camGame.follow = _target;

cutsceneEndAction();
	






