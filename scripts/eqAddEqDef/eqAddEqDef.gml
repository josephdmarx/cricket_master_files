/// @desc add item definition to itemdefinitions array based on the inherited values from when the script is called
/// @param type Type Of Item Enu
/// @param spriteIndex Index of Sprite to Show in GUI
/// @param script Script to execute on use

var _type      = argument0;
var _sprite    = argument1;
var _name      = argument2;
var _useScript = argument3;
var _description = argument4;


eqDefinitions[_type,WEAPONPROPERTIES.SPRITE] = _sprite;   
eqDefinitions[_type,WEAPONPROPERTIES.NAME] = _name;
eqDefinitions[_type,WEAPONPROPERTIES.USESCRIPT] = _useScript;
eqDefinitions[_type,WEAPONPROPERTIES.DESCRIPTION] = _description;