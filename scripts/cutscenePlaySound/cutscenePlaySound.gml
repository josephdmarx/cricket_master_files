///@desc play a sound
///@arg sound id
///@arg priority
///@arg loops

audio_play_sound(argument[0], argument[1], argument[2]);

cutsceneEndAction();