// 

//eqDefinitions[itemtype.length,itemproperties.length] = noone;  //OK SO.  This creates a 2d array with a number of x and y entries equeal to how many item types exist in the enums. 

eqAddEqDef(WEAPONTYPE.NONE, sEmpty, "", WPN_Empty, "Empty"); //calling a new script, passing in the values of the sprite, name, amount, and useScript

eqAddEqDef(WEAPONTYPE.CRITTERNET, sNetEq, "Net", WPN_Net, "A net for catching small critters.");

eqAddEqDef(WEAPONTYPE.WPNCANE, sCaneEq, "Wooden Cane", WPN_Cane, "A sturdy cane. Good for smacking things!");