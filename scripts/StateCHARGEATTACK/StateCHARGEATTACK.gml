Action = ATTACK;   //referencing back to the macros

var _x = XOffset;
var _y = YOffset;

if (CanAttack) and (_x > 0)
	{
		
		CanAttack = false;
		
		with (instance_create_layer(x+_x, y+_y,"Effects",oSpell01))
		{
			image_xscale = 2;
			image_yscale = 2;

		}
		with (instance_create_layer(x+_x, y+_y,"Effects",oChargeAttack))
		{
			
			image_xscale = 2;
			image_yscale = 2;
			xdir = _x;
			ydir = _y;
		}

	}

else if (CanAttack) and (_x < 0)
	{
		
		CanAttack = false;
		
		with (instance_create_layer(x+_x, y+_y,"Effects",oSpell01))
		{
			image_xscale = 2;
			image_yscale = 2;
		}
		
		with (instance_create_layer(x+_x, y+_y,"Effects",parProjectile))
		{
			image_xscale = -2;
			image_yscale = 2;
			xdir = _x;
			ydir = _y;
			
		}
	}
	



