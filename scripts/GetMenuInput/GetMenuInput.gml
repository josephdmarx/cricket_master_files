var _jump			= keyboard_check_pressed(vk_space) or gamepad_button_check_pressed(0, gp_face1);
var _cast			= keyboard_check_pressed(ord("K")) or gamepad_button_check_pressed(0, gp_face3);
var _melee			= keyboard_check_pressed(ord("L")) or gamepad_button_check_pressed(0, gp_shoulderr);
var _down			= keyboard_check_pressed(ord("S")) or gamepad_button_check_pressed(0, gp_padd);
var _spell			= keyboard_check_pressed(ord("J")) or gamepad_button_check_pressed(0, gp_face2);
var _interact		= keyboard_check(ord("E")) or gamepad_button_check(0, gp_face4);
var _right			= keyboard_check_pressed(ord("D")) or gamepad_button_check_pressed(0, gp_padr);
var _left			= keyboard_check_pressed(ord("A")) or gamepad_button_check_pressed(0, gp_padl);
var _charge			= keyboard_check(ord("K")) or gamepad_button_check(0, gp_face3);
var _up				= keyboard_check_pressed(ord("W")) or gamepad_button_check_pressed(0, gp_padu);
var _start			= keyboard_check_pressed(vk_enter) or gamepad_button_check_pressed(0, gp_start);

if (menu_active)
{
	key_up			= _up;
	key_down		= _down;
	key_back		= _spell;
	key_interact	= _interact;
	key_right		= _right
	key_left		= _left;
	key_accept		= _jump;
	key_cast		= _cast;
	key_melee		= _melee;
	key_charge		= _charge;
	key_start		= _start;
}

else if (!menu_active)
{
	key_up			= 0;
	key_down		= 0;
	key_back		= 0;
	key_interact	= 0;
	key_right		= 0;
	key_left		= 0;
	key_accept		= 0;
	key_cast		= 0;
	key_melee		= 0;
	key_charge		= 0;
	key_start		= 0;
}
	







