var _x = argument[0];
var _y = argument[1];
var _obj = argument[2];
var _destroy = argument[3];

//this should check to see if the enemy is 1 pixel inside of the player object to trigger the Hurt State

if (place_meeting(x-_x,y,_obj)) 
{
	if (_destroy)	///if the instance is declared destroyable, destroy it!
	{
		ParticleBurstSmall(x,y,particle);
		instance_change(deathAnim, true);
		//instance_destroy();
	}
	
	_x = 0;
}

x += _x;

if (place_meeting(x,y-_y,_obj))
{
	if (_destroy)
	{
		ParticleBurstSmall(x,y,particle);
		instance_change(deathAnim, true);
		//instance_destroy();
	}
	
	_y = 0;
}

y += _y;



