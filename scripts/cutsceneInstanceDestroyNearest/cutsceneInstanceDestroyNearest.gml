///@desc  destroy nearest instance to x and y coords
///@arg x
///@arg y
///@arg obj

var inst = instance_nearest(argument0, argument1, argument2);

cutsceneInstanceDestroy(inst);