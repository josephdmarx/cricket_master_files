Action = NET;   //referencing back to the macros

var _x = XOffset;
var _y = YOffset;


if (CanAttack)
	{
		MeleeAttack = true;
				
		if (image_index >= 3)
		{
			with(instance_create_layer(x + _x, y+10, "Effects", oMHitBox))
			{
				image_xscale = other.image_xscale;
				
				with(instance_place(x,y,parCritter))
				{
					audio_play_sound(sfxCatch01,0,false);
					InvAddItem(myItemType);
					infodisplay = true;
					DrawInfoBox(myItemType);
					instance_destroy();
				}
			}
		}
		
	}
	


