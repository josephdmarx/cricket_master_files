///@desc
///@arg x
///@arg y
///@arg layer id
///@arg obj

var inst = instance_create_layer(argument[0], argument[1], argument[2], argument[3]);

cutsceneEndAction();

return inst;

