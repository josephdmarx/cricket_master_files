var _shotcount = instance_number(parProjectile);
Action = IDLE;

//GetInput();

//Movement Animation Handling


#region INTERACT KEY

if(key_interact)
{
	var inst = collision_rectangle(x-radius, y-radius, x+radius, y+radius, oNPCMaster, false, false);  //returns the id of an instance if it finds it within this radius
	
	if (inst != noone)
		{
			with(inst)
			{
				if (interactable = true)
					create_textbox(text,speakers);
			}
		}
}

if(key_interact)
{
	var inst = collision_rectangle(x-radius, y-radius, x+radius, y+radius, oMerchantMaster, false, false);  //returns the id of an instance if it finds it within this radius
	
	if (inst != noone)
		{
			with(inst)
			{
				if (interactable = true)
					CreateShoppingList(text,items,price);
			}
		}
}

#endregion

if (key_down) and (place_meeting(x,y+1,oWall))
{
	CanAttack = false;
	State = StateCROUCH;
}


#region MOVEMENT STUFF


if (Horiz > 0)  and !(charging) //if horizontal movement is greater than 0 and we are not charging
{
	
		image_index = floor(legframe);
		image_xscale = 1;
		XOffset = CELL;
	
		if (!place_meeting(x,y+1,oWall)) //if we are not on the ground
			{
				
				grounded = false;
				
				if (vsp <= 0)
				{
					Direction = UP;     //We are moving in the air up and to the right
				}
				else if (vsp > 0)
				{
					Direction = DOWN;     //We are moving in the air down and to the right
				}
			}
	
		else if (place_meeting(x,y+1,oWall))
			{	
				if (grounded = false)  //if we have just landed
					{
						ParticleBurstSmall(x,bbox_bottom,parDust); //particle dust
						grounded = true;
					}
				
				Direction = HORIZONTAL;
			
			}
}
else if (Horiz < 0) and !(charging) //if we moving to the left and NOT charging
{
	image_index = floor(legframe);
	image_xscale = -1;
	XOffset = -CELL;
	
	if (!place_meeting(x,y+1,oWall))  //if we are not on the ground
	{
		
		grounded = false;
		
				if (vsp <= 0)
				{					
					Direction = UP;     //We are moving in the air up and to the right
				}
				else if (vsp > 0)
				{
					Direction = DOWN;     //We are moving in the air down and to the right
				}
	
	}
	
	else if (place_meeting(x,y+1,oWall))
	{	
		if (grounded = false)   // if we just hit the ground
			{	
				
				ParticleBurstSmall(x,bbox_bottom,parDust);
					grounded = true;
			}
	
		Direction = HORIZONTAL;

	}
}
else
{
	if (!place_meeting(x,y+1,oWall)) and !(charging) //if we are NOT on the ground and NOT charging
	{
			
		grounded = false;
			
			if (vsp <= 0)
				{
					Direction = UP;     //We are moving in the air up and to the right
				}
				else if (vsp > 0)
				{
					Direction = DOWN;     //We are moving in the air down and to the right
				}
	}
	else if (place_meeting(x,y+1,oWall))
		{	
		if (grounded = false)  //if we have just landed
			{
			
				ParticleBurstSmall(x,bbox_bottom,parDust);
				grounded = true;
			}
	
		Direction = NOKEY;
	
	}
}

#endregion

#region CHARGE ATTACK

//if (cancast)
//{
//	if (key_charge) and (charge_count <= room_speed)  //if we hold key_charge and our count is below 1 second
//		{
//			if (Horiz != 0)
//				{
//					charging = false;
//					charge_count = 0;
				
//					Direction = HORIZONTAL;
//					if (!place_meeting(x,y+1,oWall))   //if we are in the air
//							{
//								if (vsp < 0)
//									Direction = UP;
//								else if (vsp >= 0)
//									Direction = DOWN;
//							}
//					else Action = IDLE;
//				}
			
//			else if (Horiz = 0) and (grounded) // you can only charge while standing still on the ground
//			{
//			charge_count++;
//			charging = true;
			
//			if (charge_count > 10) //set threshold to prevent charge from happening on initial key press
//				{
//					image_index = 0;
//					Direction = NOKEY;
//						if (!place_meeting(x,y+1,oWall))   //if we are in the air
//							{
//								Direction = AIR;
//							}
//				    Action = CHARGING; 
//					ParticleBurstSmallBG(x,y,oBlueSparkle);
//				}
	
//			}

//		}
//	else if (key_charge) and (charge_count >= room_speed)  //if our charge count is greater that 1 second we are fully charged
//		{
//			charging = false;
		
//			Action = IDLE;

//		}
//	else if !(key_charge) and (charge_count < room_speed)  //if we release the charge key and are not fully charged
//	{
//		charging = false;
//		charge_count = 0;
//		Action = IDLE;

//	}
//	else if !(key_charge) and (charge_count >= room_speed)  //if we release the charge key and are fully charged
//	{	
//		charging = false;
//		stun = true;
//		image_index = 0;  //reset the animation	
//		Sprite[@ AIR, ATTACK] = sPlayerM2;
//		Sprite[@ NOKEY, ATTACK] = sPlayerM2;
//		Sprite[@ HORIZONTAL, ATTACK] = sPlayerM2;
//		Sprite[@ UP, ATTACK] = sPlayerM2;
//		Sprite[@ DOWN, ATTACK] = sPlayerM2;
		
//		State = StateCHARGEATTACK;
//	}
//}

#endregion

#region WEAPONS

var _weapon = oEquipmentManager.equippedweapon;

if (key_melee) and (oEquipmentManager.equipment[_weapon] == WEAPONTYPE.CRITTERNET)
{
	if (Horiz > 0)
		XOffset = CELL;
		
	else if (Horiz < 0)
		XOffset = -CELL;
	
	audio_play_sound(sfx_netswish,1,0);
	image_index = 0;  //reset the animation	
	
	Sprite[@ AIR, ATTACK] = sPlayerNet;
	Sprite[@ NOKEY, ATTACK] = sPlayerNet;
	Sprite[@ HORIZONTAL, ATTACK] = sPlayerNet;
	Sprite[@ UP, ATTACK] = sPlayerNet;
	Sprite[@ DOWN, ATTACK] = sPlayerNet;
	
	
	State = StateNET;
}

if (key_melee) and (oEquipmentManager.equipment[_weapon] == WEAPONTYPE.WPNCANE)
{
	if (Horiz > 0)
		XOffset = CELL;
		
	else if (Horiz < 0)
		XOffset = -CELL;
		
	audio_play_sound(sfx_caneswish,1,0);
	image_index = 0;  //reset the animation	
	
	Sprite[@ AIR, ATTACK] = sPlayerCane;
	Sprite[@ NOKEY, ATTACK] = sPlayerCane;
	Sprite[@ HORIZONTAL, ATTACK] = sPlayerCane;
	Sprite[@ UP, ATTACK] = sPlayerCane;
	Sprite[@ DOWN, ATTACK] = sPlayerCane;
	
	
	State = StateCANE;
}

#endregion

#region MAGIC SPELLS

var _spell = oMagicManager.activemagic;

if (key_spell) and (oMagicManager.magic[_spell] == MAGICTYPE.FIRE)
{	
	if (oPlayer.mana >= 2)
	{
		
		oPlayer.mana -= 2;
		
		if (Horiz > 0)
			XOffset = CELL;
		
		else if (Horiz < 0)
			XOffset = -CELL;
	
		image_index = 0;  //reset the animation	
	
		Sprite[@ AIR, SPELL] = sPlayerCast01Air;
		Sprite[@ NOKEY, SPELL] = sPlayerCast01;
		Sprite[@ HORIZONTAL, SPELL] = sPlayerCast01;
		Sprite[@ UP, SPELL] = sPlayerCast01Air;
		Sprite[@ DOWN, SPELL] = sPlayerCast01Air;
				
	
		
		State = StateFIRE
	}
}






#endregion