///@desc
///@arg x
///@arg y
///@arg layer id
///@arg obj
///@arg sound

var inst = instance_create_layer(argument[0], argument[1], argument[2], argument[3]);
var playing = false;
var sound = argument[4];


if (!playing)
{
	playing = true
	audio_play_sound(sound,0,false);
}

cutsceneEndAction();

return inst;

