///@use for enemy item drops
///@arg item
var _item = argument[0];
var _offset = 12;

	with (instance_create_layer(x-_offset,y-45,"Pickups",_item))
	{
		hsp = 0;
		vsp = -5;
	}

