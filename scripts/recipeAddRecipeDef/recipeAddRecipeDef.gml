/// @desc add item definition to itemdefinitions array based on the inherited values from when the script is called
/// @param type_of_item
/// @param sprite_of_item
/// @param name_of_item
/// @param ingredient1
/// @param ingredient2
/// @param ingredient3
/// @param description
/// @param item_to_craft

var _type		 = argument0;
var _sprite		 = argument1;
var _name		 = argument2;

var _ingsprite0  = oInventoryManager.itemDefinitions[argument3, itemproperties.sprite];
var _ingsprite1  = oInventoryManager.itemDefinitions[argument4, itemproperties.sprite];
var _ingsprite2  = oInventoryManager.itemDefinitions[argument5, itemproperties.sprite];
						
var _ingname0    =	oInventoryManager.itemDefinitions[argument3, itemproperties.name];
var _ingname1    =	oInventoryManager.itemDefinitions[argument4, itemproperties.name];
var _ingname2    =	oInventoryManager.itemDefinitions[argument5, itemproperties.name];

var _ingamount0    =	oInventoryManager.itemDefinitions[argument3, itemproperties.amount];
var _ingamount1    =	oInventoryManager.itemDefinitions[argument4, itemproperties.amount];
var _ingamount2    =	oInventoryManager.itemDefinitions[argument5, itemproperties.amount];

//var _ingredienttype0 = argument3;
//var _ingredienttype1 = argument4;
//var _ingredienttype2 = argument5;


var _description = argument6;
var _itemtocraft = argument7;

recipeIngredient0 = argument3;
recipeIngredient1 = argument4;
recipeIngredient2 = argument5;

recipeDefinitions[_type,RECIPEPROPERTIES.SPRITE] = _sprite;
recipeDefinitions[_type,RECIPEPROPERTIES.NAME] = _name;
recipeDefinitions[_type,RECIPEPROPERTIES.INGSPRITE0] = _ingsprite0;  
recipeDefinitions[_type,RECIPEPROPERTIES.INGSPRITE1] = _ingsprite1;  
recipeDefinitions[_type,RECIPEPROPERTIES.INGSPRITE2] = _ingsprite2;  
recipeDefinitions[_type,RECIPEPROPERTIES.INGNAME0] = _ingname0;
recipeDefinitions[_type,RECIPEPROPERTIES.INGNAME1] = _ingname1;
recipeDefinitions[_type,RECIPEPROPERTIES.INGNAME2] = _ingname2;
recipeDefinitions[_type,RECIPEPROPERTIES.INGTYPE0] = argument3;
recipeDefinitions[_type,RECIPEPROPERTIES.INGTYPE1] = argument4;
recipeDefinitions[_type,RECIPEPROPERTIES.INGTYPE2] = argument5;
recipeDefinitions[_type,RECIPEPROPERTIES.DESCRIPTION] = _description;
recipeDefinitions[_type,RECIPEPROPERTIES.CRAFTEDITEM] = _itemtocraft;