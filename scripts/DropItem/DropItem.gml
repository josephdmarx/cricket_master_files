///@use for enemy item drops

var _item = argument[0];
var _rare = argument[1];

var i = irandom(10);


if (i == 10)
{
	with (instance_create_layer(x,y,"Pickups",_rare))
	{
		hsp = random_range(-2,2);
		vsp = random_range(-2,-3);
	}
}

else if (i > 4)
{
	with (instance_create_layer(x,y,"Pickups",_item))
	{
		hsp = random_range(-2,2);
		vsp = random_range(-2,-3);
	}
}

else exit;
