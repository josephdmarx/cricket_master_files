///@desc
///@arg obj_id
///@arg var_name_as_string
///@arg value

with(argument0)  //point to the target object ID
{
	variable_instance_set(id, argument1, argument2);   //change the given variable to the new value
}


cutsceneEndAction();