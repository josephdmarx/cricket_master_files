enum MAGICTYPE
	{
		NONE			=0,
		FIRE			=1,
		ICE				=2,
		LENGTH
	}
	
enum MAGICPROPERTIES
	{
		SPRITE			=0,
		NAME			=1,
		USESCRIPT		=2,
		DESCRIPTION		=3,
		LENGTH
	}