var _splashnumber = random_range(20,25)


var _pitch = (random_range(0.8, 1.2));
audio_sound_pitch(sfxSplash, _pitch);
audio_play_sound(sfxSplash, 1, 0);


for (var i = 0; i < _splashnumber; i ++)
{
	instance_create_layer(x, y, "Effects", parSplash);
}