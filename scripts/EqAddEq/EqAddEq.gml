//@desc adds the specified equipment to the equipment list
//@desc adds the specified equipment to the equipment list

var _type = argument0;
var indexAddedAt = -2;

if (ArrayFindIndex(oEquipmentManager.equipment, _type) == -1)  //if the item type is NOT already in our equipment list
{
	indexAddedAt = ArrayReplaceValue(oEquipmentManager.equipment, WEAPONTYPE.NONE, _type);  //finds a none value, and changes it to the new value}
}

//return (indexAddedAt != -1);
	