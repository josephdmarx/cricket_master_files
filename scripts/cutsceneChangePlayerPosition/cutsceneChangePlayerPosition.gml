///@desc move the player instantly
///@arg x
///@arg y

with (oPlayer)
{
	x = argument0;
	y = argument1;
}

cutsceneEndAction();