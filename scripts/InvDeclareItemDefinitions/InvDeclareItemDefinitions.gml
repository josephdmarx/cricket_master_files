// 

itemDefinitions[itemtype.length,itemproperties.length] = noone;  //OK SO.  This creates a 2d array with a number of x and y entries equeal to how many item types exist in the enums. 

InvAddItemDef(itemtype.none, sEmpty, "", 0, PU_empty, "", false); //calling a new script, passing in the values of the sprite, name, amount, and useScript

InvAddItemDef(itemtype.redapple, sApple, "Red Apple", 0, PU_apple, "A shiny red apple. Restores a small amount of health", true);

InvAddItemDef(itemtype.greenapple, sAppleGreen, "Green Apple", 0, PU_greenapple, "A sour green apple. Restores a small amount of health", true);

InvAddItemDef(itemtype.yellowapple, sAppleYellow, "Yellow Apple", 0, PU_yellowapple, "A sweet yellow apple. Restores a small amount of health", true);

InvAddItemDef(itemtype.blueapple, sAppleBlue, "Blue Apple", 0, PU_blueapple, "A curious blue apple. Restores a small amount of magik", true);

InvAddItemDef(itemtype.whiteapple, sAppleWhite, "White Apple", 0, PU_whiteapple, "A poisonous white apple. Eat at your own risk!", true);

InvAddItemDef(itemtype.greenfrog, sFrogGreenPU, "Green Frog", 0, PU_frog, "A plump green frog", false);

InvAddItemDef(itemtype.indigobird, sIndigoBirdPU, "Indigo Bird", 0, PU_indigobird, "A tiny purplish bird", false);

InvAddItemDef(itemtype.coneflower, sConeflower, "Coneflower", 0, PU_empty, "A small red coneflower", false);

InvAddItemDef(itemtype.jellycap, sRedShroom, "Jellycap", 0, PU_empty, "A mushroom with a glossy red cap", false);

InvAddItemDef(itemtype.curefever, sCureFever, "Cure Fever", 0, PU_empty, "This potion will cure almsot any fever", false);

InvAddItemDef(itemtype.healpot01, sHealPot01, "Small Health Potion", 0, PU_healpot01, "This potion will retore 5 health", true);

InvAddItemDef(itemtype.manapot01, sManaPot01, "Small Mana Potion", 0, PU_manapot01, "This potion will retore 5 mana", true);

InvAddItemDef(itemtype.insectwing, sInsectWing, "Insect Wing", 0, PU_empty, "A transparent wing of a flying insect", false);

InvAddItemDef(itemtype.waspstinger, sWaspStinger, "Wasp Stinger", 0, PU_empty, "A sharp stinger from a venomous insect", false);

InvAddItemDef(itemtype.gremlinknife, sGremlinKnife, "Small Knife", 0, PU_empty, "A knife dropped by a murderous gremlin", false);

InvAddItemDef(itemtype.gremlincap, sGremlinCap, "Gremlin Cap", 0, PU_empty, "A cap typically worn by murderous gremlins", false);