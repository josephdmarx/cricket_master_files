///@desc cutscene_wait
///@arg seconds

timer++;

if (instance_exists(oPlayer))
{
	oPlayer.hascontrol = false;
}


if (timer >= argument[0] * room_speed)
	{
		timer = 0;
		
		cutsceneEndAction();  //advance the scene
	}