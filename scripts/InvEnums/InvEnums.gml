enum itemtype 
	{
		none			=0,
		redapple		=1,
		greenapple		=2,
		yellowapple		=3,
		blueapple		=4,
		whiteapple		=5,
		greenfrog		=6,
		indigobird		=8,
		coneflower		=9,
		jellycap		=10,
		curefever		=11,
		healpot01		=12,
		manapot01		=13,
		insectwing		=14,
		waspstinger		=15,
		gremlinknife	=16,
		gremlincap		=17,
		length			    //used to reference back to so we can know how many enums are here
	}


enum itemproperties 
	{
		sprite			=0,
		name			=1,
		amount			=2,
		useScript		=3,
		details			=4,
		usable			=5,
		length			
		
	}
	
