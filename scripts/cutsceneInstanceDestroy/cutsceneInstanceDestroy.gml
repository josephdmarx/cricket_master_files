///@desc destroys the given instance ID
///@arg obj id

with(argument[0])
{
	instance_destroy();
}

cutsceneEndAction();



