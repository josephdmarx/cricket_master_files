// @desc Looks for an old value, and replaces with a new one

var _array  = argument0;
var _oldVal = argument1;
var _newVal = argument2;

var _indexToReplaceAt = ArrayFindIndex(_array, _oldVal);

if (_indexToReplaceAt == -1)
	return -1;
	
	
_array[@ _indexToReplaceAt] = _newVal;      //the @ sign is to prevent this from creating a duplicate array, and instead changing the original array



