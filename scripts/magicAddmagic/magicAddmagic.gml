//@ desc add a spell
//@ spellnametoadd


var _type = argument0;
var indexAddedAt = -2;

if (ArrayFindIndex(oMagicManager.magic, _type) == -1)  //if the item type is NOT already in our equipment list
{
	indexAddedAt = ArrayReplaceValue(oMagicManager.magic, MAGICTYPE.NONE, _type);  //finds a none value, and changes it to the new value}
}
