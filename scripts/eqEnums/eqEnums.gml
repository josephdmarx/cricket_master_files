enum WEAPONTYPE
	{
		NONE			=0,
		CRITTERNET		=1,
		WPNCANE			=2,
		LENGTH
	}
	
enum WEAPONPROPERTIES
	{
		SPRITE			=0,
		NAME			=1,
		USESCRIPT		=2,
		DESCRIPTION		=3,
		LENGTH
	}