//Direction
#macro NOKEY 0
#macro HORIZONTAL 1
#macro AIR 2
#macro UP 3
#macro DOWN 4
#macro CROUCH 5

//Action
#macro IDLE 0
#macro ATTACK 1
#macro CHARGING 2
#macro NET 3
#macro CANE 4
#macro SLEEPTREE 5
#macro ANIMATE 6
#macro WAKEUP1 7
#macro SPELL 8
#macro FIRESPELL 9
#macro WALK 10

//Attack Types
#macro NOTHING 0
#macro LUNGE 1
#macro SPIT 2

//Misc
#macro CELL 24 //offset 

//Locations
#macro MEADOWLANDS 0
#macro MEADOWTOWN 1
#macro BOGCAVES 2

//Menu
#macro ITEMS 0
#macro EQUIPMENT 1
#macro SPELLS 2
#macro BESTIARY 3
#macro RECIPES 4
#macro MAP 5
#macro PETS	6
#macro EXIT 7
