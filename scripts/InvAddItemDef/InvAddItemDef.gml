/// @desc add item definition to itemdefinitions array based on the inherited values from when the script is called
/// @param type Type Of Item Enum
/// @param spriteIndex Index of Sprite to Show in GUI
/// @param amount Amount of item of this type we currently have
/// @param script Script to execute on use

var _type      = argument0;
var _sprite    = argument1;
var _name      = argument2
var _amount    = argument3;
var _useScript = argument4;
var _details   = argument5;
var _usable	   = argument6;

itemDefinitions[_type,itemproperties.sprite]		= _sprite;
itemDefinitions[_type,itemproperties.name]			= _name;
itemDefinitions[_type,itemproperties.amount]		= _amount;
itemDefinitions[_type,itemproperties.useScript]		= _useScript;
itemDefinitions[_type,itemproperties.details]		= _details;
itemDefinitions[_type,itemproperties.usable]		= _usable;





