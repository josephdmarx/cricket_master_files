recipeDefinitions[RECIPETYPE.LENGTH,RECIPEPROPERTIES.LENGTH] = noone;  //OK SO.  This creates a 2d array with a number of x and y entries equeal to how many item types exist in the enums. 

recipeAddRecipeDef(RECIPETYPE.NONE, sEmpty, "", itemtype.none, itemtype.none, itemtype.none,"", itemtype.none); //calling a new script, passing in the values of the sprite, name, amount, and useScript

recipeAddRecipeDef(RECIPETYPE.CUREFEVER, sCureFever, "Cure Fever", itemtype.redapple, itemtype.coneflower, itemtype.jellycap, "This potion will cure almost any fever", itemtype.curefever);

recipeAddRecipeDef(RECIPETYPE.HEALTHPOTION, sHealPot01, "S. Health Potion", itemtype.redapple, itemtype.coneflower, itemtype.insectwing, "Restores 5 health", itemtype.healpot01);

recipeAddRecipeDef(RECIPETYPE.MANAPOTION, sManaPot01, "S. Mana Potion", itemtype.blueapple, itemtype.coneflower, itemtype.insectwing, "Restores 5 mana", itemtype.manapot01);

